using Advocar.Controls;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Import
{
	public class Status : Form
	{
		private ColumnHeader Description;

		public System.Windows.Forms.ProgressBar ProgressBar;

		public Advocar.Controls.ListView ListView;

		public Label LabelCount;

		private System.ComponentModel.Container components;

		public Status()
		{
			this.InitializeComponent();
		}

		public void ClearLog()
		{
			this.ListView.Items.Clear();
		}

		public void ClearProgress()
		{
			this.ProgressBar.Value = 0;
			this.LabelCount.Text = "";
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.ListView = new Advocar.Controls.ListView();
			this.Description = new ColumnHeader();
			this.ProgressBar = new System.Windows.Forms.ProgressBar();
			this.LabelCount = new Label();
			base.SuspendLayout();
			this.ListView.AllowColumnReorder = true;
			this.ListView.Columns.AddRange(new ColumnHeader[] { this.Description });
			this.ListView.FullRowSelect = true;
			this.ListView.Location = new Point(8, 28);
			this.ListView.MultiSelect = false;
			this.ListView.Name = "ListView";
			this.ListView.Size = new System.Drawing.Size(608, 292);
			this.ListView.TabIndex = 0;
			this.ListView.UseCompatibleStateImageBehavior = false;
			this.ListView.View = View.Details;
			this.Description.Text = "Description";
			this.Description.Width = 588;
			this.ProgressBar.Location = new Point(8, 4);
			this.ProgressBar.Name = "ProgressBar";
			this.ProgressBar.Size = new System.Drawing.Size(435, 20);
			this.ProgressBar.TabIndex = 1;
			this.LabelCount.Location = new Point(449, 8);
			this.LabelCount.Name = "LabelCount";
			this.LabelCount.Size = new System.Drawing.Size(167, 16);
			this.LabelCount.TabIndex = 2;
			this.LabelCount.Text = "0";
			this.LabelCount.TextAlign = ContentAlignment.MiddleRight;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			base.ClientSize = new System.Drawing.Size(624, 329);
			base.Controls.Add(this.LabelCount);
			base.Controls.Add(this.ProgressBar);
			base.Controls.Add(this.ListView);
			base.Name = "Status";
			base.Opacity = 0.8;
			this.Text = "Application Progress and Status";
			base.TopMost = true;
			base.Closed += new EventHandler(this.Status_Closed);
			base.ResumeLayout(false);
		}

		public string Open(string path, string file)
		{
			string fileName = string.Concat(path, file);
			if (!Directory.Exists(path))
			{
				Directory.CreateDirectory(path);
			}
			if (!File.Exists(fileName))
			{
				File.CreateText(fileName).Close();
			}
			GC.Collect();
			return fileName;
		}

		private void Status_Closed(object sender, EventArgs e)
		{
			Environment.Exit(Environment.ExitCode);
		}

		public void UpdateProgress(int progress, int maximum)
		{
			this.ProgressBar.Maximum = maximum;
			this.ProgressBar.Value = (progress > maximum ? maximum : progress);
			this.ProgressBar.Refresh();
			this.LabelCount.Text = string.Concat(progress.ToString(), " / ", maximum.ToString());
			this.Refresh();
			Application.DoEvents();
			GC.Collect();
		}

		public void UpdateStatus(string status, float progress)
		{
			if (status.Length > 0)
			{
				this.ListView.Items.Insert(0, status);
				this.ListView.Refresh();
			}
			int value = this.ProgressBar.Value;
			float progressCalc = progress + float.Parse(value.ToString());
			this.ProgressBar.Value = (progressCalc > 100f ? 100 : (int)progressCalc);
			this.ProgressBar.Refresh();
			Application.DoEvents();
			GC.Collect();
		}

		public void UpdateStatus(string status)
		{
			if (status.Length > 0)
			{
				this.ListView.Items.Insert(0, status);
				this.ListView.Refresh();
			}
			Application.DoEvents();
			GC.Collect();
		}

		public void WriteStatusToFile(string logFileName)
		{
			StreamWriter stream = File.AppendText(logFileName);
			for (int index = this.ListView.Items.Count; index > 0; index--)
			{
				ListViewItem item = this.ListView.Items[index - 1];
				stream.WriteLine(item.Text);
			}
			stream.Close();
			this.ListView.Items.Clear();
			this.ClearProgress();
			this.Refresh();
			Application.DoEvents();
			GC.Collect();
		}

		public void WriteToFile(string logFileName, string writeText)
		{
			StreamWriter stream = File.AppendText(logFileName);
			stream.WriteLine(writeText);
			stream.Close();
			this.Refresh();
			Application.DoEvents();
			GC.Collect();
		}

		public void WriteToFile(string logFileName, Exception err)
		{
			StreamWriter stream = File.AppendText(logFileName);
			stream.WriteLine("\n\n");
			stream.WriteLine("***********************************************************************************\n");
			stream.WriteLine(string.Concat("OS Version:\t\t", Environment.OSVersion.VersionString));
			stream.WriteLine(string.Concat("Program Files:\t", Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)));
			stream.WriteLine(string.Concat("Error Message:\t", err.Message));
			stream.WriteLine(string.Concat("Error Source:\t\t", err.Source));
			stream.WriteLine(string.Concat("Stack Trace:\t\t", err.StackTrace));
			stream.WriteLine("***********************************************************************************");
			stream.WriteLine("\n\n");
			stream.Close();
			this.Refresh();
			Application.DoEvents();
			GC.Collect();
		}
	}
}