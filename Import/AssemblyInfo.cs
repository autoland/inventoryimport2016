﻿using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security.Permissions;

[assembly: AssemblyCompany("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyDescription("")]
[assembly: AssemblyKeyFile("")]
[assembly: AssemblyKeyName("")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyTitle("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("1.0.5706.33187")]
[assembly: CompilationRelaxations(8)]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.Default | DebuggableAttribute.DebuggingModes.DisableOptimizations | DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints | DebuggableAttribute.DebuggingModes.EnableEditAndContinue)]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows=true)]
[assembly: SecurityPermission(SecurityAction.RequestMinimum, SkipVerification=true)]
