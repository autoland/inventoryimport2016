using Advocar.Data;
using Advocar.Interface;
using Import.Execute.Mapping;
using Import.Execute.Raw;
using System;
using System.Windows.Forms;

namespace Import
{
    public class Startup
    {
        public static Import.Status Status;

        public static string Server;

        public static string User;

        public static string Password;

        public static string SaPassword;

        public static string dbInventoryImport;

        public static string dbAime;

        public static string dbCars;

        public static string dbConfigurator;

        public static string Media;

        static Startup()
        {
            Startup.Status = new Import.Status();
            Startup.Server = "";
            Startup.User = "";
            Startup.Password = "";
            Startup.SaPassword = "";
            Startup.dbInventoryImport = "";
            Startup.dbAime = "";
            Startup.dbCars = "";
            Startup.dbConfigurator = "";
            Startup.Media = "";
        }

        public Startup()
        {
        }

        public static void DisplayError(string logFile, Exception err)
        {
            Import.Status status = Startup.Status;
            string str = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            DateTime now = DateTime.Now;
            status.Open(str, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Startup.Status.WriteToFile(logFile, err);
        }

        [STAThread]
        private static void Main()
        {
            Exception err;
            Environment.GetCommandLineArgs();
            Startup.Server = "10.1.1.147";
            Startup.User = "autolandDb";
            Startup.Password = "au&ol1nd#b";
            Startup.Media = "10.1.1.200";
            Startup.SaPassword = (Startup.Server == "10.1.1.147" ? "yellowcorvette" : "flipper");
            Startup.Status.Show();
            Job job = null;
            Import.Status status = Startup.Status;
            string str = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            DateTime now = DateTime.Now;
            string mainLogFile = status.Open(str, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            try
            {
                Import.Status statu = Startup.Status;
                now = DateTime.Now;
                statu.UpdateStatus(string.Concat("BEGIN PROCESSING - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
                Startup.Status.WriteStatusToFile(mainLogFile);
            }
            catch (Exception exception)
            {
                err = exception;
                Startup.DisplayError(mainLogFile, err);
            }
            Startup.User = "sa";
            Startup.Password = "yellowcorvette";
            Startup.dbInventoryImport = DataAccess.GetConnectionString(Startup.Server, "INVENTORY_IMPORT", Startup.User, Startup.Password);
            Startup.dbAime = DataAccess.GetConnectionString(Startup.Server, "AIME", Startup.User, Startup.Password);
            Startup.dbCars = DataAccess.GetConnectionString(Startup.Server, "CARS", Startup.User, Startup.Password);
            Startup.dbConfigurator = DataAccess.GetConnectionString(Startup.Server, "CONFIGURATOR", Startup.User, Startup.Password);
            Import.Status status1 = Startup.Status;
            now = DateTime.Now;
            status1.UpdateStatus(string.Concat("LIQUID MOTORS STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new LiquidMotors()).Execute();
                job = null;
            }
            catch (Exception exception1)
            {
                err = exception1;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status statu1 = Startup.Status;
            string str1 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            statu1.Open(str1, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status status2 = Startup.Status;
            now = DateTime.Now;
            status2.UpdateStatus(string.Concat("LIQUID MOTORS COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));

            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status statu2 = Startup.Status;
            now = DateTime.Now;
            statu2.UpdateStatus(string.Concat("AUTO FUSION STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new AutoFusion()).Execute();
                job = null;
            }
            catch (Exception exception2)
            {
                err = exception2;
                Startup.DisplayError(mainLogFile, err);
            }
            Import.Status status3 = Startup.Status;
            string str2 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            status3.Open(str2, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status statu3 = Startup.Status;
            now = DateTime.Now;
            statu3.UpdateStatus(string.Concat("AUTO FUSION COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status status4 = Startup.Status;
            now = DateTime.Now;
            status4.UpdateStatus(string.Concat("AUTO UPLINK USA STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new AutoUplinkUsa()).Execute();
                job = null;
            }
            catch (Exception exception3)
            {
                err = exception3;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status statu4 = Startup.Status;
            string str3 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            statu4.Open(str3, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status status5 = Startup.Status;
            now = DateTime.Now;
            status5.UpdateStatus(string.Concat("AUTO UPLINK USA COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status statu5 = Startup.Status;
            now = DateTime.Now;
            statu5.UpdateStatus(string.Concat("FEX - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new Fex()).Execute();
                job = null;
            }
            catch (Exception exception4)
            {
                err = exception4;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status status6 = Startup.Status;
            string str4 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            status6.Open(str4, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status statu6 = Startup.Status;
            now = DateTime.Now;
            statu6.UpdateStatus(string.Concat("FEX COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status status7 = Startup.Status;
            now = DateTime.Now;
            status7.UpdateStatus(string.Concat("KARPOWER STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new Karpower()).Execute();
                job = null;
            }
            catch (Exception exception5)
            {
                err = exception5;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status statu7 = Startup.Status;
            string str5 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            statu7.Open(str5, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status status8 = Startup.Status;
            now = DateTime.Now;
            status8.UpdateStatus(string.Concat("KARPOWER COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status statu8 = Startup.Status;
            now = DateTime.Now;
            statu8.UpdateStatus(string.Concat("KARPOWERWEB STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new KarpowerWeb()).Execute();
                job = null;
            }
            catch (Exception exception6)
            {
                err = exception6;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status status9 = Startup.Status;
            string str6 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            status9.Open(str6, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status statu9 = Startup.Status;
            now = DateTime.Now;
            statu9.UpdateStatus(string.Concat("KARPOWERWEB COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status status10 = Startup.Status;
            now = DateTime.Now;
            status10.UpdateStatus(string.Concat("DEALER SPECIALTIES STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new DealerSpecialties()).Execute();
                job = null;
            }
            catch (Exception exception7)
            {
                err = exception7;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status statu10 = Startup.Status;
            string str7 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            statu10.Open(str7, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status status11 = Startup.Status;
            now = DateTime.Now;
            status11.UpdateStatus(string.Concat("DEALER SPECIALTIES COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status statu11 = Startup.Status;
            now = DateTime.Now;
            statu11.UpdateStatus(string.Concat("CDM DATA STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new CdmData()).Execute();
                job = null;
            }
            catch (Exception exception8)
            {
                err = exception8;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status status12 = Startup.Status;
            string str8 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            status12.Open(str8, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status statu12 = Startup.Status;
            now = DateTime.Now;
            statu12.UpdateStatus(string.Concat("CDM DATA COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status status13 = Startup.Status;
            now = DateTime.Now;
            status13.UpdateStatus(string.Concat("NET LOOK STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new NetLook()).Execute();
                job = null;
            }
            catch (Exception exception9)
            {
                err = exception9;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status statu13 = Startup.Status;
            string str9 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            statu13.Open(str9, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status status14 = Startup.Status;
            now = DateTime.Now;
            status14.UpdateStatus(string.Concat("NET LOOK COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status statu14 = Startup.Status;
            now = DateTime.Now;
            statu14.UpdateStatus(string.Concat("EBIZAUTOS STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new EbizAutos()).Execute();
                job = null;
            }
            catch (Exception exception10)
            {
                err = exception10;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status status15 = Startup.Status;
            string str10 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            status15.Open(str10, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status statu15 = Startup.Status;
            now = DateTime.Now;
            statu15.UpdateStatus(string.Concat("EBIZAUTOS COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status status16 = Startup.Status;
            now = DateTime.Now;
            status16.UpdateStatus(string.Concat("VIRTUAL DEALER STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new VirtualDealer()).Execute();
                job = null;
            }
            catch (Exception exception11)
            {
                err = exception11;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status statu16 = Startup.Status;
            string str11 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            statu16.Open(str11, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status status17 = Startup.Status;
            now = DateTime.Now;
            status17.UpdateStatus(string.Concat("VIRTUAL DEALER COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status statu17 = Startup.Status;
            now = DateTime.Now;
            statu17.UpdateStatus(string.Concat("HOME NET STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new HomeNet()).Execute();
                job = null;
            }
            catch (Exception exception12)
            {
                err = exception12;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status status18 = Startup.Status;
            string str12 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            status18.Open(str12, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status statu18 = Startup.Status;
            now = DateTime.Now;
            statu18.UpdateStatus(string.Concat("HOME NET COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status status19 = Startup.Status;
            now = DateTime.Now;
            status19.UpdateStatus(string.Concat("AUTONEXUS STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new Autonexus()).Execute();
                job = null;
            }
            catch (Exception exception13)
            {
                err = exception13;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status statu19 = Startup.Status;
            string str13 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            statu19.Open(str13, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status status20 = Startup.Status;
            now = DateTime.Now;
            status20.UpdateStatus(string.Concat("AUTONEXUS COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status statu20 = Startup.Status;
            now = DateTime.Now;
            statu20.UpdateStatus(string.Concat("DEALER.COM STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new DealerCom()).Execute();
                job = null;
            }
            catch (Exception exception14)
            {
                err = exception14;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status status21 = Startup.Status;
            string str14 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            status21.Open(str14, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status statu21 = Startup.Status;
            now = DateTime.Now;
            statu21.UpdateStatus(string.Concat("DEALER.COM COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status status22 = Startup.Status;
            now = DateTime.Now;
            status22.UpdateStatus(string.Concat("VAUTO STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new Vauto()).Execute();
                job = null;
            }
            catch (Exception exception15)
            {
                err = exception15;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status statu22 = Startup.Status;
            string str15 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            statu22.Open(str15, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status status23 = Startup.Status;
            now = DateTime.Now;
            status23.UpdateStatus(string.Concat("VAUTO COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status statu23 = Startup.Status;
            now = DateTime.Now;
            statu23.UpdateStatus(string.Concat("DEALER FRONT STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new DealerFront()).Execute();
                job = null;
            }
            catch (Exception exception16)
            {
                err = exception16;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status status24 = Startup.Status;
            string str16 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            status24.Open(str16, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status statu24 = Startup.Status;
            now = DateTime.Now;
            statu24.UpdateStatus(string.Concat("DEALER FRONT COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status status25 = Startup.Status;
            now = DateTime.Now;
            status25.UpdateStatus(string.Concat("MOSS BUICK GMC STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new VinSolutions("mossbuickgmc")).Execute();
                job = null;
            }
            catch (Exception exception17)
            {
                err = exception17;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status statu25 = Startup.Status;
            string str17 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            statu25.Open(str17, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status status26 = Startup.Status;
            now = DateTime.Now;
            status26.UpdateStatus(string.Concat("MOSS BUICK GMC COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status statu26 = Startup.Status;
            now = DateTime.Now;
            statu26.UpdateStatus(string.Concat("MOSS CHEVY STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new VinSolutions("mosschevy")).Execute();
                job = null;
            }
            catch (Exception exception18)
            {
                err = exception18;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status status27 = Startup.Status;
            string str18 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            status27.Open(str18, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status statu27 = Startup.Status;
            now = DateTime.Now;
            statu27.UpdateStatus(string.Concat("MOSS CHEVY COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status status28 = Startup.Status;
            now = DateTime.Now;
            status28.UpdateStatus(string.Concat("MOSS CJD MV STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new VinSolutions("mosscjdmv")).Execute();
                job = null;
            }
            catch (Exception exception19)
            {
                err = exception19;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status statu28 = Startup.Status;
            string str19 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            statu28.Open(str19, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status status29 = Startup.Status;
            now = DateTime.Now;
            status29.UpdateStatus(string.Concat("MOSS CJD MV COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status statu29 = Startup.Status;
            now = DateTime.Now;
            statu29.UpdateStatus(string.Concat("MOSS CJD RIV STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new VinSolutions("mosscjdriv")).Execute();
                job = null;
            }
            catch (Exception exception20)
            {
                err = exception20;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status status30 = Startup.Status;
            string str20 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            status30.Open(str20, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status statu30 = Startup.Status;
            now = DateTime.Now;
            statu30.UpdateStatus(string.Concat("MOSS CJD RIV COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status status31 = Startup.Status;
            now = DateTime.Now;
            status31.UpdateStatus(string.Concat("MOSS CJD SB STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new VinSolutions("mosscjdsb")).Execute();
                job = null;
            }
            catch (Exception exception21)
            {
                err = exception21;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status statu31 = Startup.Status;
            string str21 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            statu31.Open(str21, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status status32 = Startup.Status;
            now = DateTime.Now;
            status32.UpdateStatus(string.Concat("MOSS CJD SB COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status statu32 = Startup.Status;
            now = DateTime.Now;
            statu32.UpdateStatus(string.Concat("MOSS HONDA STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new VinSolutions("mosshonda")).Execute();
                job = null;
            }
            catch (Exception exception22)
            {
                err = exception22;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status status33 = Startup.Status;
            string str22 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            status33.Open(str22, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status statu33 = Startup.Status;
            now = DateTime.Now;
            statu33.UpdateStatus(string.Concat("MOSS HONDA COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status status34 = Startup.Status;
            now = DateTime.Now;
            status34.UpdateStatus(string.Concat("MOSS TOYOTA STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new VinSolutions("mosstoyota")).Execute();
                job = null;
            }
            catch (Exception exception23)
            {
                err = exception23;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status statu34 = Startup.Status;
            string str23 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            statu34.Open(str23, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status status35 = Startup.Status;
            now = DateTime.Now;
            status35.UpdateStatus(string.Concat("MOSS TOYOTA COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status statu35 = Startup.Status;
            now = DateTime.Now;
            statu35.UpdateStatus(string.Concat("SOUTHBAY FORD STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new VinSolutions("southbayford")).Execute();
                job = null;
            }
            catch (Exception exception24)
            {
                err = exception24;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status status36 = Startup.Status;
            string str24 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            status36.Open(str24, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status statu36 = Startup.Status;
            now = DateTime.Now;
            statu36.UpdateStatus(string.Concat("SOUTHBAY FORD COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status status37 = Startup.Status;
            now = DateTime.Now;
            status37.UpdateStatus(string.Concat("RIVERSIDE METRO STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new VinSolutions("riversidemetro")).Execute();
                job = null;
            }
            catch (Exception exception25)
            {
                err = exception25;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status statu37 = Startup.Status;
            string str25 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            statu37.Open(str25, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status status38 = Startup.Status;
            now = DateTime.Now;
            status38.UpdateStatus(string.Concat("RIVERSIDE METRO COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status statu38 = Startup.Status;
            now = DateTime.Now;
            statu38.UpdateStatus(string.Concat("SOUTH BAY BMW MINI STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new VinSolutions("southbaybmwmini")).Execute();
                job = null;
            }
            catch (Exception exception26)
            {
                err = exception26;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status status39 = Startup.Status;
            string str26 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            status39.Open(str26, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status statu39 = Startup.Status;
            now = DateTime.Now;
            statu39.UpdateStatus(string.Concat("SOUTH BAY BMW MINI COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status status40 = Startup.Status;
            now = DateTime.Now;
            status40.UpdateStatus(string.Concat("KEYES LEXUS STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new VinSolutions("keyeslexus")).Execute();
                job = null;
            }
            catch (Exception exception27)
            {
                err = exception27;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status statu40 = Startup.Status;
            string str27 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            statu40.Open(str27, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status status41 = Startup.Status;
            now = DateTime.Now;
            status41.UpdateStatus(string.Concat("KEYES LEXUS COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status statu41 = Startup.Status;
            now = DateTime.Now;

            statu41.UpdateStatus(string.Concat("Pacific Honda - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new VinSolutions("PacificHonda")).Execute();
                job = null;
            }
            catch (Exception exception27)
            {
                err = exception27;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status status101 = Startup.Status;
            string str201 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            status101.Open(str2, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status statu101 = Startup.Status;
            now = DateTime.Now;
            statu101.UpdateStatus(string.Concat("Pacific Honda COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));



            statu41.UpdateStatus(string.Concat("PGIAUTO STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new PgiAuto()).Execute();
                job = null;
            }
            catch (Exception exception28)
            {
                err = exception28;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status status100 = Startup.Status;
            string str200 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            status100.Open(str2, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status statu100 = Startup.Status;
            now = DateTime.Now;
            statu100.UpdateStatus(string.Concat("PGIAUTO COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));


            statu41.UpdateStatus(string.Concat("VINLIST STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new Vinlist()).Execute();
                job = null;
            }
            catch (Exception exception28)
            {
                err = exception28;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status status42 = Startup.Status;
            string str28 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            status42.Open(str28, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status statu42 = Startup.Status;
            now = DateTime.Now;
            statu42.UpdateStatus(string.Concat("VINLIST COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status status43 = Startup.Status;
            now = DateTime.Now;
            status43.UpdateStatus(string.Concat("ECARLIST STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new ECarlist()).Execute();
                job = null;
            }
            catch (Exception exception29)
            {
                err = exception29;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status statu43 = Startup.Status;
            string str29 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            statu43.Open(str29, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status status44 = Startup.Status;
            now = DateTime.Now;
            status44.UpdateStatus(string.Concat("ECARLIST COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status statu44 = Startup.Status;
            now = DateTime.Now;
            statu44.UpdateStatus(string.Concat("DEALER CAR SEARCH STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new DealerCarSearch()).Execute();
                job = null;
            }
            catch (Exception exception30)
            {
                err = exception30;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status status45 = Startup.Status;
            string str30 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            status45.Open(str30, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status statu45 = Startup.Status;
            now = DateTime.Now;
            statu45.UpdateStatus(string.Concat("DEALER CAR SEARCH COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status status46 = Startup.Status;
            now = DateTime.Now;
            status46.UpdateStatus(string.Concat("MAX SYSTEMS STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new MaxSystems()).Execute();
                job = null;
            }
            catch (Exception exception31)
            {
                err = exception31;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status statu46 = Startup.Status;
            string str31 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            statu46.Open(str31, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status status47 = Startup.Status;
            now = DateTime.Now;
            status47.UpdateStatus(string.Concat("MAX SYSTEMS COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status statu47 = Startup.Status;
            now = DateTime.Now;
            statu47.UpdateStatus(string.Concat("VIN CONTROL STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new VinControl()).Execute();
                job = null;
            }
            catch (Exception exception32)
            {
                err = exception32;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status status48 = Startup.Status;
            string str32 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            status48.Open(str32, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status statu48 = Startup.Status;
            now = DateTime.Now;
            statu48.UpdateStatus(string.Concat("VIN CONTROL COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status status49 = Startup.Status;
            now = DateTime.Now;
            status49.UpdateStatus(string.Concat("Auto Revo - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new AutoRevo()).Execute();
                job = null;
            }
            catch (Exception exception33)
            {
                err = exception33;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status statu49 = Startup.Status;
            string str33 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            statu49.Open(str33, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status status50 = Startup.Status;
            now = DateTime.Now;
            status50.UpdateStatus(string.Concat("Auto Revo COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status statu50 = Startup.Status;
            now = DateTime.Now;

            status50.UpdateStatus(string.Concat("AtuoManager - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new AutoManager()).Execute();
                job = null;
            }
            catch (Exception exception33)
            {
                err = exception33;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status statu51 = Startup.Status;
            string str34 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            statu49.Open(str33, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status status52 = Startup.Status;
            now = DateTime.Now;
            status50.UpdateStatus(string.Concat("Auto Manager COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status statu53 = Startup.Status;
            now = DateTime.Now;

            status50.UpdateStatus(string.Concat("DealerSync - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new DealerSync()).Execute();
                job = null;
            }
            catch (Exception exception33)
            {
                err = exception33;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status statuas105 = Startup.Status;
            string str205 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            statu49.Open(str205, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status status105 = Startup.Status;
            now = DateTime.Now;
            status50.UpdateStatus(string.Concat("DealerSync COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status statu54 = Startup.Status;
            now = DateTime.Now;


            status50.UpdateStatus(string.Concat("CarWeek - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new Carweek()).Execute();
                job = null;
            }
            catch (Exception exception33)
            {
                err = exception33;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status statu102 = Startup.Status;
            string str300 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            statu49.Open(str33, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status status102 = Startup.Status;
            now = DateTime.Now;
            status50.UpdateStatus(string.Concat("Carweek COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status statu202 = Startup.Status;
            now = DateTime.Now;


            statu50.UpdateStatus(string.Concat("MAPPING STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new TransferToMapping()).Execute();
                job = null;
            }
            catch (Exception exception34)
            {
                err = exception34;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status status51 = Startup.Status;
            string str35 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            status51.Open(str35, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status statu52 = Startup.Status;
            now = DateTime.Now;
            statu51.UpdateStatus(string.Concat("MAPPING COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status status53 = Startup.Status;
            now = DateTime.Now;
            status52.UpdateStatus(string.Concat("VEHICLE IMAGE PROCESSING STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            try
            {
                (new VehicleImageProcessing(mainLogFile)).Execute();
                job = null;
            }
            catch (Exception exception35)
            {
                err = exception35;
                Startup.DisplayError(mainLogFile, err);
                MailNotification.Email(Convert.ToString(err));
            }
            Import.Status statu001 = Startup.Status;
            string str001 = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
            now = DateTime.Now;
            statu52.Open(str35, string.Concat(now.ToString("yyyyMMdd"), ".log"));
            Import.Status status002 = Startup.Status;
            now = DateTime.Now;
            status53.UpdateStatus(string.Concat("VEHICLE IMAGE PROCESSING COMPLETED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.WriteStatusToFile(mainLogFile);
            Import.Status statu003 = Startup.Status;
            now = DateTime.Now;
            statu53.UpdateStatus(string.Concat("COMPLETED PROCESSING - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
            Startup.Status.Close();
            Environment.Exit(Environment.ExitCode);
        }
    }
}