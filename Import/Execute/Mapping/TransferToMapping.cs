using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;

namespace Import.Execute.Mapping
{
	public class TransferToMapping : Job
	{
		public TransferToMapping()
		{
		}

		public override void Execute()
		{
			int transferCount;
			int jobRawId = JobExecutionRaw.GetRecordId(Startup.dbInventoryImport);
			JobExecutionRaw job = new JobExecutionRaw(Startup.dbInventoryImport, "IMPORT.EXE", jobRawId);
			Status status = Startup.Status;
			string str = string.Concat(Environment.CurrentDirectory, "\\Logs\\");
			DateTime now = DateTime.Now;
			string logFile = status.Open(str, string.Concat(now.ToString("yyyyMMdd"), ".log"));
			Status statu = Startup.Status;
			now = DateTime.Now;
			statu.UpdateStatus(string.Concat("MAPPING STARTED - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
			int count = 0;
			int maxCount = job.NoOfRawNotMapped;
			while (jobRawId > 0)
			{
				count++;
				jobRawId = JobExecutionRaw.GetRecordId(Startup.dbInventoryImport);
				job = new JobExecutionRaw(Startup.dbInventoryImport, "IMPORT.EXE", jobRawId);
				Lookup dataProvider = new Lookup(Startup.dbAime, "IMPORT.EXE", job.LookupId_DataProvider);
				if (jobRawId == 0)
				{
					break;
				}
				if (dataProvider.Code == "")
				{
					job.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
					job.Save();
				}
				else
				{
					try
					{
						DataAccess data = new DataAccess(Startup.dbInventoryImport)
						{
							Timeout = 12000
						};
						data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, job.JobExecutionRawId.ToString());
						data.ExecuteProcedure(string.Concat(dataProvider.OtherValue1, "_TransferToMapping"));
						try
						{
							transferCount = int.Parse(data["JobExecutionCount"]);
						}
						catch
						{
							transferCount = 0;
						}
						Status status1 = Startup.Status;
						string[] strArrays = new string[] { dataProvider.OtherValue1, " - Transfer To Mapping (", transferCount.ToString(), ") - ", null };
						string[] otherValue1 = strArrays;
						now = DateTime.Now;
						otherValue1[4] = now.ToString("MM/dd/yyyy hh:mm:ss");
						status1.UpdateStatus(string.Concat(otherValue1));
						int jobMappingId = int.Parse(data["JobExecutionMappingId"]);
						data.ClearParams();
						data.AddParam("@JobExecutionMappingId", DataAccessParameterType.Numeric, jobMappingId.ToString());
						data.ExecuteProcedure("MAPPING_VEHICLE_TransferUpdate");
						Status statu1 = Startup.Status;
						string otherValue11 = dataProvider.OtherValue1;
						now = DateTime.Now;
						statu1.UpdateStatus(string.Concat(otherValue11, " - Update Mapping Records - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
						data.ClearParams();
						data.AddParam("@JobExecutionMappingId", DataAccessParameterType.Numeric, jobMappingId.ToString());
						try
						{
							data.ExecuteProcedure("MAPPING_VEHICLE_TransferToAime");
						}
						catch (Exception exception)
						{
							Startup.Status.WriteToFile(logFile, exception);
							Startup.Status.WriteToFile(logFile, jobRawId.ToString());
						}
						Status status2 = Startup.Status;
						string str1 = dataProvider.OtherValue1;
						now = DateTime.Now;
						status2.UpdateStatus(string.Concat(str1, " - Transfer To Aime - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
						Startup.Status.UpdateProgress(count, maxCount);
					}
					catch (Exception exception1)
					{
						Startup.Status.WriteToFile(logFile, exception1);
						Startup.Status.WriteToFile(logFile, jobRawId.ToString());
					}
					GC.Collect();
				}
			}
			Startup.Status.WriteStatusToFile(logFile);
		}
	}
}