using Advocar.Data;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Advocar.Tools;
using Import;
using System;
using System.IO;

namespace Import.Execute.Mapping
{
	public class VehicleImageProcessing : Job
	{
		private string logFile = "";

		public VehicleImageProcessing(string logFile)
		{
			this.logFile = logFile;
		}

		public override void Execute()
		{
			string path = string.Concat(Environment.CurrentDirectory, "\\Images\\");
			DataAccess data = new DataAccess(Startup.dbInventoryImport);
			data.AddParam("@OrderBy", DataAccessParameterType.Text, "MappingVehicleId DESC");
			data.AddParam("@GetPendingTransferOnly", DataAccessParameterType.Numeric, "1");
			data.Timeout = 2400;
			data.ExecuteProcedure("MAPPING_VEHICLE_IMAGE_GetList");
			int vehicleID = 0;
			int imageCount = 0;
			int row = 0;
			MappingVehicleImage image = new MappingVehicleImage();
			while (!data.EOF)
			{
				row++;
				image = new MappingVehicleImage(Startup.dbInventoryImport, "IMPORT.EXE", int.Parse(data["MappingVehicleImageId"]));
				if (vehicleID == image.MappingVehicleId)
				{
					imageCount++;
				}
				else
				{
					imageCount = 1;
					vehicleID = image.MappingVehicleId;
				}
				if (imageCount > 20)
				{
					image.Delete();
					image.IsDeleted = false;
				}
				try
				{
					if (!image.IsDeleted)
					{
						image.ImageDestinationPath = string.Concat(path, data["Vin"], (imageCount > 1 ? string.Concat("(", imageCount.ToString(), ")") : ""), ".jpg");
						if (File.Exists(image.ImageDestinationPath))
						{
							image.IsImageUploadedToDestination = true;
						}
						if ((image.IsImageUploadedToDestination ? false : image.ImageSourceUrlOrFile.ToLower() == "u") && API.URLDownloadToFile(image.ImageSourcePath, image.ImageDestinationPath) != 0)
						{
							image.Delete();
							image.IsDeleted = true;
						}
					}
					if ((!File.Exists(image.ImageDestinationPath) ? false : !image.IsDeleted))
					{
						image.ImageDestinationPath = image.ImageDestinationPath.Replace(path, "http://aime.advocar.com/mediacom/inventory/");
						image.ImageDestinationPath = image.ImageDestinationPath.Replace("\\", "/");
						image.IsImageUploadedToDestination = true;
						image.Save();
						string[] strArrays = new string[] { "Transferred Mapping Vehicle ID: ", null, null, null, null, null, null, null };
						string[] str = strArrays;
						int mappingVehicleId = image.MappingVehicleId;
						str[1] = mappingVehicleId.ToString();
						str[2] = " Image ID: ";
						mappingVehicleId = image.MappingVehicleImageId;
						str[3] = mappingVehicleId.ToString();
						str[4] = " Source: ";
						str[5] = image.ImageSourcePath;
						str[6] = " Destination: ";
						str[7] = image.ImageDestinationPath;
						string status = string.Concat(str);
						Startup.Status.UpdateStatus(status);
					}
					if (row % 100 == 0)
					{
						Startup.Status.WriteStatusToFile(this.logFile);
					}
				}
				catch (System.Exception exception)
				{
					System.Exception err = exception;
					Startup.Status.WriteToFile(this.logFile, err);
					MailNotification.Email(Convert.ToString(err));
				}
				Startup.Status.UpdateProgress(row, data.RecordCount);
				data.MoveNext();
			}
			GC.Collect();
			Startup.Status.WriteStatusToFile(this.logFile);
		}
	}
}