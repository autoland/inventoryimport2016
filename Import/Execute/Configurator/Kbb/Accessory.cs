using System;

namespace Import.Execute.Configurator.Kbb
{
	public class Accessory
	{
		public string Code = "";

		public string Value = "";

		public Accessory(string code, string value)
		{
			this.Code = code;
			this.Value = value;
		}
	}
}