using Advocar.Tools;
using System;

namespace Import.Execute.Configurator.Kbb
{
	public class Common
	{
		public Common()
		{
		}

		public static Accessory[] GetAccessoryAvailability(string accessories)
		{
			int index;
			string[] accessoryList = accessories.Split(",".ToCharArray());
			Accessory[] list = new Accessory[33];
			int count = -1;
			for (index = 0; (int)accessoryList.Length > index; index++)
			{
				string lower = accessoryList[index].ToLower();
				if (lower != null)
				{
					if (lower == "standard equipment")
					{
						count++;
						list[count] = new Accessory(index.ToString(), Common.GetAccessoryName(index));
					}
					else if (lower == "available as an option or part of a package")
					{
						count++;
						list[count] = new Accessory(index.ToString(), Common.GetAccessoryName(index));
					}
				}
			}
			Accessory[] returnList = new Accessory[count + 1];
			for (index = 0; (int)returnList.Length > index; index++)
			{
				returnList[index] = list[index];
			}
			return returnList;
		}

		private static string GetAccessoryName(int index)
		{
			string optionName = "";
			switch (index)
			{
				case 0:
				{
					optionName = "2 Wheel Anti-Lock Braking System";
					break;
				}
				case 1:
				{
					optionName = "4 Wheel Anti-Lock Braking System";
					break;
				}
				case 2:
				{
					optionName = "Front Air Conditioning";
					break;
				}
				case 3:
				{
					optionName = "Dual Air Conditioning";
					break;
				}
				case 4:
				{
					optionName = "Cruise Control";
					break;
				}
				case 5:
				{
					optionName = "AM/FM Radio";
					break;
				}
				case 6:
				{
					optionName = "Cassette";
					break;
				}
				case 7:
				{
					optionName = "Compact Disc";
					break;
				}
				case 8:
				{
					optionName = "Compact Disc Changer";
					break;
				}
				case 9:
				{
					optionName = "Premium Radio";
					break;
				}
				case 10:
				{
					optionName = "Cup Holder";
					break;
				}
				case 11:
				{
					optionName = "Leather";
					break;
				}
				case 12:
				{
					optionName = "Navigation System";
					break;
				}
				case 13:
				{
					optionName = "Privacy Glass";
					break;
				}
				case 14:
				{
					optionName = "Hands Off Phone";
					break;
				}
				case 15:
				{
					optionName = "Power Locks";
					break;
				}
				case 16:
				{
					optionName = "Power Outlets";
					break;
				}
				case 17:
				{
					optionName = "Power Mirrors";
					break;
				}
				case 18:
				{
					optionName = "Remote Mirrors";
					break;
				}
				case 19:
				{
					optionName = "Power Seat";
					break;
				}
				case 20:
				{
					optionName = "Dual Power Seats";
					break;
				}
				case 21:
				{
					optionName = "Folding Rear Seat";
					break;
				}
				case 22:
				{
					optionName = "Moon Roof";
					break;
				}
				case 23:
				{
					optionName = "Sun Roof";
					break;
				}
				case 24:
				{
					optionName = "Keyless Remote Entry";
					break;
				}
				case 25:
				{
					optionName = "Luggage Rack";
					break;
				}
				case 26:
				{
					optionName = "Rear Window Defroster";
					break;
				}
				case 27:
				{
					optionName = "Tilt Wheel";
					break;
				}
				case 28:
				{
					optionName = "Tachometer";
					break;
				}
				case 29:
				{
					optionName = "Traction Control";
					break;
				}
				case 30:
				{
					optionName = "Power Windows";
					break;
				}
				case 31:
				{
					optionName = "Wheel Upgrade";
					break;
				}
				case 32:
				{
					optionName = "No Power Steering";
					break;
				}
				case 33:
				{
					optionName = "DVD Entertainment System";
					break;
				}
			}
			return optionName;
		}

		public static Specification GetSpecification(string fieldName, string fieldValue)
		{
			string[] temp;
			string optionName = "";
			string optionValue = fieldValue;
			float numericValue = 0f;
			string str = fieldName;
			if (str != null)
			{
				string str1 = str;
				string str2 = str1;
				if (str1 != null)
				{
					switch (str2)
					{
						case "SpecSeating":
						{
							optionName = "Seating";
							optionValue = (fieldValue.Length >= 1 ? fieldValue.Substring(0, 1) : "");
							break;
						}
						case "SpecAxleRatio":
						{
							optionName = "Axle Ratio";
							if (Validation.IsNumeric(fieldValue))
							{
								numericValue = float.Parse(fieldValue) / 100f;
								optionValue = string.Concat(numericValue.ToString("0.00"), ":1");
								break;
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
						}
						case "SpecWheelbase":
						{
							optionName = "Wheelbase";
							if (Validation.IsNumeric(fieldValue))
							{
								numericValue = float.Parse(fieldValue) / 10f;
								optionValue = string.Concat(numericValue.ToString("0.0"), " In.");
								break;
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
						}
						case "SpecLength":
						{
							optionName = "Length";
							if (Validation.IsNumeric(fieldValue))
							{
								numericValue = float.Parse(fieldValue) / 10f;
								optionValue = string.Concat(numericValue.ToString("0.0"), " In.");
								break;
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
						}
						case "SpecWidth":
						{
							optionName = "Width";
							if (Validation.IsNumeric(fieldValue))
							{
								numericValue = float.Parse(fieldValue) / 10f;
								optionValue = string.Concat(numericValue.ToString("0.0"), " In.");
								break;
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
						}
						case "SpecHeight":
						{
							optionName = "Height";
							if (Validation.IsNumeric(fieldValue))
							{
								numericValue = float.Parse(fieldValue) / 10f;
								optionValue = string.Concat(numericValue.ToString("0.0"), " In.");
								break;
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
						}
						case "SpecCurbWeight":
						{
							optionName = "Curb Weight";
							if (Validation.IsNumeric(fieldValue))
							{
								numericValue = float.Parse(fieldValue);
								optionValue = string.Concat(numericValue.ToString("0.0"), " Lbs.");
								break;
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
						}
						case "SpecTireSize":
						{
							optionName = "Tire Wheel Size";
							break;
						}
						case "SepcTireSizeRear":
						{
							optionName = "Rear Tire Wheel Size";
							break;
						}
						case "SpecSpareTire":
						{
							optionName = "Spare Tire";
							break;
						}
						case "SpecWheelType":
						{
							optionName = "Wheel Type";
							break;
						}
						case "SpecAirbagDriver":
						{
							optionName = "Driver Side Airbag";
							break;
						}
						case "SpecAirbagPassenger":
						{
							optionName = "Passenger Side Airbag";
							break;
						}
						case "SpecAirbagSideImpact":
						{
							optionName = "Side Impact Airbag";
							break;
						}
						case "SpecAirbagHeadProtection":
						{
							optionName = "Head Protection Airbag";
							break;
						}
						case "SpecAirbagKneeProtection":
						{
							optionName = "Knee Protection Airbag";
							break;
						}
						case "SpecAirbagRearProtection":
						{
							optionName = "Rear Protection Airbag";
							break;
						}
						case "SpecFrontBrakes":
						{
							optionName = "Front Brakes";
							break;
						}
						case "SpecRearBrakes":
						{
							optionName = "Rear Brakes";
							break;
						}
						case "SpecTowingCapacity":
						{
							optionName = "Towing Capacity";
							if (Validation.IsNumeric(fieldValue))
							{
								numericValue = float.Parse(fieldValue);
								optionValue = string.Concat(numericValue.ToString(), " Lbs.");
								break;
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
						}
						case "SpecTurningRadius":
						{
							optionName = "Turning Radius";
							if (Validation.IsNumeric(fieldValue))
							{
								numericValue = float.Parse(fieldValue) / 10f;
								optionValue = numericValue.ToString("0.0");
								break;
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
						}
						case "SpecHeadRoom":
						{
							optionName = "Head Room";
							if (fieldValue.IndexOf(",") >= 0)
							{
								temp = fieldValue.Split(",".ToCharArray());
								if ((!Validation.IsNumeric(temp[0]) ? false : Validation.IsNumeric(temp[1])))
								{
									numericValue = float.Parse(temp[0]) / 10f;
									optionValue = string.Concat("Front: ", numericValue.ToString("0.0"), " In., ");
									numericValue = float.Parse(temp[1]) / 10f;
									optionValue = string.Concat(optionValue, "Rear: ", numericValue.ToString("0.0"), " In.");
									break;
								}
								else
								{
									optionValue = fieldValue;
									break;
								}
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
						}
						case "SpecLegRoom":
						{
							optionName = "Leg Room";
							if (fieldValue.IndexOf(",") >= 0)
							{
								temp = fieldValue.Split(",".ToCharArray());
								if ((!Validation.IsNumeric(temp[0]) ? false : Validation.IsNumeric(temp[1])))
								{
									numericValue = float.Parse(temp[0]) / 10f;
									optionValue = string.Concat("Front: ", numericValue.ToString("0.0"), " In., ");
									numericValue = float.Parse(temp[1]) / 10f;
									optionValue = string.Concat(optionValue, "Rear: ", numericValue.ToString("0.0"), " In.");
									break;
								}
								else
								{
									optionValue = fieldValue;
									break;
								}
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
						}
						case "SpecShoulderRoom":
						{
							optionName = "Shoulder Room";
							if (fieldValue.IndexOf(",") >= 0)
							{
								temp = fieldValue.Split(",".ToCharArray());
								if ((!Validation.IsNumeric(temp[0]) ? false : Validation.IsNumeric(temp[1])))
								{
									numericValue = float.Parse(temp[0]) / 10f;
									optionValue = string.Concat("Front: ", numericValue.ToString("0.0"), " In., ");
									numericValue = float.Parse(temp[1]) / 10f;
									optionValue = string.Concat(optionValue, "Rear: ", numericValue.ToString("0.0"), " In.");
									break;
								}
								else
								{
									optionValue = fieldValue;
									break;
								}
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
						}
						case "SpecPassengerFeet":
						{
							optionName = "EPA Passenger Feet";
							if (Validation.IsNumeric(fieldValue))
							{
								numericValue = float.Parse(fieldValue) / 10f;
								optionValue = string.Concat(numericValue.ToString("0.0"), " Cubic Feet");
								break;
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
						}
						case "SpecTrunkFeet":
						{
							optionName = "EPA Trunk Feet";
							if (Validation.IsNumeric(fieldValue))
							{
								numericValue = float.Parse(fieldValue) / 10f;
								optionValue = string.Concat(numericValue.ToString("0.0"), " Cubic Feet");
								break;
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
						}
						case "SpecInteriorFeet":
						{
							optionName = "EPA Interior Feet";
							if (Validation.IsNumeric(fieldValue))
							{
								numericValue = float.Parse(fieldValue) / 10f;
								optionValue = string.Concat(numericValue.ToString("0.0"), " Cubic Feet");
								break;
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
						}
						case "SpecFuelCapacity":
						{
							optionName = "Fuel Capacity";
							if (Validation.IsNumeric(fieldValue))
							{
								numericValue = float.Parse(fieldValue) / 10f;
								optionValue = string.Concat(numericValue.ToString("0.0"), " Gallons");
								break;
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
						}
						case "SpecSafetyChildLock":
						{
							optionName = "Safety - Child Locks";
							break;
						}
						case "SpecSafetyChildSeatAnchors":
						{
							optionName = "Safety - Child Seat Anchor";
							break;
						}
						case "SpecSafetyIntegratedChildSeat":
						{
							optionName = "Safety - Integrated Child Seat";
							break;
						}
						case "SpecSafetyAlarm":
						{
							optionName = "Safety - Alarm System";
							break;
						}
						case "SpecSafetyEngineBlocking":
						{
							optionName = "Safety - Engine Blocking";
							break;
						}
						case "SpecTruckGroundClearance":
						{
							optionName = "Ground Clearance";
							if (Validation.IsNumeric(fieldValue))
							{
								numericValue = float.Parse(fieldValue) / 10f;
								optionValue = string.Concat(numericValue.ToString("0.0"), " In.");
								break;
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
						}
						case "SpecTruckGVWR":
						{
							optionName = "GVWR";
							if (Validation.IsNumeric(fieldValue))
							{
								numericValue = float.Parse(fieldValue) / 10f;
								optionValue = string.Concat(numericValue.ToString("0.0"), " Lbs.");
								break;
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
						}
						case "SpecTruckPayload":
						{
							optionName = "Truck - Payload";
							if (Validation.IsNumeric(fieldValue))
							{
								numericValue = float.Parse(fieldValue);
								optionValue = string.Concat(numericValue.ToString(), " Lbs.");
								break;
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
						}
						case "SpecTruckBedVolume":
						{
							optionName = "Truck - Bed Volume";
							if (Validation.IsNumeric(fieldValue))
							{
								numericValue = float.Parse(fieldValue);
								optionValue = string.Concat(numericValue.ToString(), " Cubic Feet");
								break;
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
						}
						case "SpecEngineCylinders":
						{
							optionName = "Engine - # Of Cylinders";
							break;
						}
						case "SpecEngineFuelInduction":
						{
							optionName = "Engine - Fuel Induction";
							break;
						}
						case "SpecEngineDisplacement":
						{
							optionName = "Engine - Displacement";
							if (Validation.IsNumeric(fieldValue))
							{
								numericValue = float.Parse(fieldValue) / 10f;
								optionValue = numericValue.ToString("0.0");
								break;
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
						}
						case "SpecEngineBoreStroke":
						{
							optionName = "Engine - Bore Times Stroke";
							if (fieldValue.IndexOf(",") >= 0)
							{
								temp = fieldValue.Split(",".ToCharArray());
								if ((!Validation.IsNumeric(temp[0]) ? false : Validation.IsNumeric(temp[1])))
								{
									numericValue = float.Parse(temp[0]) / 100f;
									optionValue = string.Concat("Bore: ", numericValue.ToString("0.00"), ", ");
									numericValue = float.Parse(temp[1]) / 100f;
									optionValue = string.Concat(optionValue, "Stroke: ", numericValue.ToString("0.00"));
									break;
								}
								else
								{
									optionValue = fieldValue;
									break;
								}
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
						}
						case "SpecEngineCompressionRatio":
						{
							optionName = "Engine - Compression Ratio";
							if (Validation.IsNumeric(fieldValue))
							{
								numericValue = float.Parse(fieldValue) / 10f;
								optionValue = string.Concat(numericValue.ToString("0.0"), " to 1");
								break;
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
						}
						case "SpecEngineValves":
						{
							optionName = "Engine - # of Valves";
							break;
						}
						case "SpecEngineHorsepower":
						{
							optionName = "Horsepower @ Rpm";
							if (fieldValue.IndexOf(",") >= 0)
							{
								temp = fieldValue.Split(",".ToCharArray());
								if ((!Validation.IsNumeric(temp[0]) ? false : Validation.IsNumeric(temp[1])))
								{
									numericValue = float.Parse(temp[0]);
									optionValue = string.Concat(numericValue.ToString("0.00"), " @ ");
									numericValue = float.Parse(temp[1]);
									optionValue = string.Concat(optionValue, numericValue.ToString("0.00"));
									break;
								}
								else
								{
									optionValue = fieldValue;
									break;
								}
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
						}
						case "SpecEngineTorque":
						{
							optionName = "Torque @ Rpm";
							if (fieldValue.IndexOf(",") >= 0)
							{
								temp = fieldValue.Split(",".ToCharArray());
								if ((!Validation.IsNumeric(temp[0]) ? false : Validation.IsNumeric(temp[1])))
								{
									numericValue = float.Parse(temp[0]);
									optionValue = string.Concat(numericValue.ToString("0.00"), " @ ");
									numericValue = float.Parse(temp[1]);
									optionValue = string.Concat(optionValue, numericValue.ToString("0.00"));
									break;
								}
								else
								{
									optionValue = fieldValue;
									break;
								}
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
						}
						case "SpecEngineMaxSpeed":
						{
							optionName = "Maximum Engine Speed (Rpm)";
							if (Validation.IsNumeric(fieldValue))
							{
								numericValue = float.Parse(fieldValue);
								optionValue = string.Concat(numericValue.ToString("0.0"), " (RPM)");
								break;
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
						}
						case "SpecMilesPerGallonCity":
						{
							optionName = "City Mpg.";
							if (Validation.IsNumeric(fieldValue))
							{
								numericValue = float.Parse(fieldValue);
								optionValue = string.Concat(numericValue.ToString("0.0"), " Mpg.");
								break;
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
						}
						case "SpecMilesPerGallonHwy":
						{
							optionName = "Highway Mpg.";
							if (Validation.IsNumeric(fieldValue))
							{
								numericValue = float.Parse(fieldValue);
								optionValue = string.Concat(numericValue.ToString("0.0"), " Mpg.");
								break;
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
						}
						case "SpecRatingFrontDriver":
						{
							optionName = "Rating - Front Driver";
							if (fieldValue.IndexOf(",") >= 0)
							{
								temp = fieldValue.Split(",".ToCharArray());
								if (Validation.IsNumeric(temp[0]))
								{
									numericValue = float.Parse(temp[0]);
									optionValue = string.Concat(numericValue.ToString(), " ", temp[1].ToString());
									break;
								}
								else
								{
									optionValue = temp[0];
									try
									{
										optionValue = string.Concat(optionValue, " ", temp[1]);
										break;
									}
									catch
									{
										break;
									}
								}
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
							break;
						}
						case "SpecRatingFrontPassenger":
						{
							optionName = "Rating - Front Passenger";
							if (fieldValue.IndexOf(",") >= 0)
							{
								temp = fieldValue.Split(",".ToCharArray());
								if (Validation.IsNumeric(temp[0]))
								{
									numericValue = float.Parse(temp[0]);
									optionValue = string.Concat(numericValue.ToString(), " ", temp[1].ToString());
									break;
								}
								else
								{
									optionValue = temp[0];
									try
									{
										optionValue = string.Concat(optionValue, " ", temp[1]);
										break;
									}
									catch
									{
										break;
									}
								}
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
							break;
						}
						case "SpecRatingFrontSide":
						{
							optionName = "Rating - Front Side";
							if (fieldValue.IndexOf(",") >= 0)
							{
								temp = fieldValue.Split(",".ToCharArray());
								if (Validation.IsNumeric(temp[0]))
								{
									numericValue = float.Parse(temp[0]);
									optionValue = string.Concat(numericValue.ToString(), " ", temp[1].ToString());
									break;
								}
								else
								{
									optionValue = temp[0];
									try
									{
										optionValue = string.Concat(optionValue, " ", temp[1]);
										break;
									}
									catch
									{
										break;
									}
								}
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
							break;
						}
						case "SpecRatingRearSide":
						{
							optionName = "Rating - Rear Side";
							if (fieldValue.IndexOf(",") >= 0)
							{
								temp = fieldValue.Split(",".ToCharArray());
								if (Validation.IsNumeric(temp[0]))
								{
									numericValue = float.Parse(temp[0]);
									optionValue = string.Concat(numericValue.ToString(), " ", temp[1].ToString());
									break;
								}
								else
								{
									optionValue = temp[0];
									try
									{
										optionValue = string.Concat(optionValue, " ", temp[1]);
										break;
									}
									catch
									{
										break;
									}
								}
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
							break;
						}
						case "SpecRatingRollover":
						{
							optionName = "Rating - Rollover";
							if (fieldValue.IndexOf(",") >= 0)
							{
								temp = fieldValue.Split(",".ToCharArray());
								if (Validation.IsNumeric(temp[0]))
								{
									numericValue = float.Parse(temp[0]);
									optionValue = string.Concat(numericValue.ToString(), " ", temp[1].ToString());
									break;
								}
								else
								{
									optionValue = temp[0];
									try
									{
										optionValue = string.Concat(optionValue, " ", temp[1]);
										break;
									}
									catch
									{
										break;
									}
								}
							}
							else
							{
								optionValue = fieldValue;
								break;
							}
							break;
						}
					}
				}
			}
			return new Specification()
			{
				Name = optionName,
				Value = optionValue
			};
		}
	}
}