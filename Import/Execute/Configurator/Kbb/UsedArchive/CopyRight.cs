using System;
using System.Xml.Serialization;

namespace Import.Execute.Configurator.Kbb.UsedArchive
{
	[XmlRoot(Namespace="", IsNullable=false, ElementName="Vehicle_Raw_DataCopyRight")]
	public class CopyRight
	{
		public string Text;

		public string Data_Format;

		public CopyRight()
		{
		}
	}
}