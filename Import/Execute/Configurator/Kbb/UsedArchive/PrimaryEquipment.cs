using System;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Import.Execute.Configurator.Kbb.UsedArchive
{
	[XmlRoot(Namespace="", IsNullable=false, ElementName="Vehicle_Raw_DataPrimaryEquipmentPricing_TradeIn")]
	public class PrimaryEquipment
	{
		public string Class;

		public string Year;

		public string Code;

		[XmlElement("Trade-In_Excellent", Form=XmlSchemaForm.Unqualified)]
		public string TradeIn_Excellent;

		[XmlElement("Trade-In_Good", Form=XmlSchemaForm.Unqualified)]
		public string TradeIn_Good;

		[XmlElement("Trade-In_Fair", Form=XmlSchemaForm.Unqualified)]
		public string TradeIn_Fair;

		public string Retail;

		public string PrivateParty_Excellent;

		public string PrivateParty_Good;

		public string PrivateParty_Fair;

		public PrimaryEquipment()
		{
		}
	}
}