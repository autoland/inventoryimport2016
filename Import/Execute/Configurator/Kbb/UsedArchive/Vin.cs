using System;
using System.Xml.Serialization;

namespace Import.Execute.Configurator.Kbb.UsedArchive
{
	[XmlRoot(Namespace="", IsNullable=false, ElementName="Vehicle_Raw_DataVIN")]
	public class Vin
	{
		public string Pattern;

		public string Model_Ids;

		public Vin()
		{
		}
	}
}