using System;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Import.Execute.Configurator.Kbb.UsedArchive
{
	[XmlRoot(Namespace="", IsNullable=false, ElementName="Vehicle_Raw_DataModel")]
	public class Model
	{
		public string ModelId;

		public string Equip_Class;

		public string Sequence_Key;

		[XmlElement("Model")]
		public string ModelName;

		public string Trim;

		public string VIN_Seq;

		public string WheelBase;

		public string Equip_Overrides;

		public string Category;

		public string List;

		[XmlElement("Region1_Trade-In_Excellent", Form=XmlSchemaForm.Unqualified)]
		public string Region1_TradeIn_Excellent;

		[XmlElement("Region1_Trade-In_Good", Form=XmlSchemaForm.Unqualified)]
		public string Region1_TradeIn_Good;

		[XmlElement("Region1_Trade-In_Fair", Form=XmlSchemaForm.Unqualified)]
		public string Region1_TradeIn_Fair;

		public string Region1_Wholesale;

		public string Region1_Retail;

		public string Region1_PrivateParty_Excellent;

		public string Region1_PrivateParty_Good;

		public string Region1_PrivateParty_Fair;

		[XmlElement("Region2_Trade-In_Excellent", Form=XmlSchemaForm.Unqualified)]
		public string Region2_TradeIn_Excellent;

		[XmlElement("Region2_Trade-In_Good", Form=XmlSchemaForm.Unqualified)]
		public string Region2_TradeIn_Good;

		[XmlElement("Region2_Trade-In_Fair", Form=XmlSchemaForm.Unqualified)]
		public string Region2_TradeIn_Fair;

		public string Region2_Wholesale;

		public string Region2_Retail;

		public string Region2_PrivateParty_Excellent;

		public string Region2_PrivateParty_Good;

		public string Region2_PrivateParty_Fair;

		[XmlElement("Region3_Trade-In_Excellent", Form=XmlSchemaForm.Unqualified)]
		public string Region3_TradeIn_Excellent;

		[XmlElement("Region3_Trade-In_Good", Form=XmlSchemaForm.Unqualified)]
		public string Region3_TradeIn_Good;

		[XmlElement("Region3_Trade-In_Fair", Form=XmlSchemaForm.Unqualified)]
		public string Region3_TradeIn_Fair;

		public string Region3_Wholesale;

		public string Region3_Retail;

		public string Region3_PrivateParty_Excellent;

		public string Region3_PrivateParty_Good;

		public string Region3_PrivateParty_Fair;

		[XmlElement("Ranged_Low_Trade-In", Form=XmlSchemaForm.Unqualified)]
		public string Ranged_Low_TradeIn;

		[XmlElement("Ranged_High_Trade-In", Form=XmlSchemaForm.Unqualified)]
		public string Ranged_High_TradeIn;

		public string Ranged_Low_Retail;

		public string Ranged_High_Retail;

		public string Ranged_Low_PrivateParty;

		public string Ranged_High_PrivateParty;

		public string Region1_Classic_Wholesale;

		public string Region2_Classic_Wholesale;

		public string Region3_Classic_Wholesale;

		public Model()
		{
		}
	}
}