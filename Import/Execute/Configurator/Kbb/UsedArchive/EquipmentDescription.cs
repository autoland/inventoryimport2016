using System;
using System.Xml.Serialization;

namespace Import.Execute.Configurator.Kbb.UsedArchive
{
	[XmlRoot(Namespace="", IsNullable=false, ElementName="Vehicle_Raw_DataEquipmentDescription")]
	public class EquipmentDescription
	{
		public string Code;

		public string Name;

		public string Compatibility;

		public string OptionGroup;

		public EquipmentDescription()
		{
		}
	}
}