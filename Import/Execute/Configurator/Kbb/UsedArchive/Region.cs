using System;
using System.Xml.Serialization;

namespace Import.Execute.Configurator.Kbb.UsedArchive
{
	[XmlRoot(Namespace="", IsNullable=false, ElementName="Vehicle_Raw_DataRegion")]
	public class Region
	{
		public string State;

		[XmlElement("Region")]
		public int RegionNo;

		public Region()
		{
		}
	}
}