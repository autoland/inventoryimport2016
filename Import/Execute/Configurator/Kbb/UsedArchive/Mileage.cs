using System;
using System.Xml.Serialization;

namespace Import.Execute.Configurator.Kbb.UsedArchive
{
	[XmlRoot(Namespace="", IsNullable=false, ElementName="Vehicle_Raw_DataMileage")]
	public class Mileage
	{
		public string Miles;

		public string Year;

		public string Price1;

		public string Adjust1;

		public string Price2;

		public string Adjust2;

		public string Price3;

		public string Adjust3;

		public string Price4;

		public string Adjust4;

		public string Price5;

		public string Adjust5;

		public string Price6;

		public string Adjust6;

		public string Price7;

		public string Adjust7;

		public string Price8;

		public string Adjust8;

		public Mileage()
		{
		}
	}
}