using System;
using System.Xml.Serialization;

namespace Import.Execute.Configurator.Kbb.UsedArchive
{
	[XmlRoot(Namespace="", IsNullable=false, ElementName="Vehicle_Raw_DataControl_Record")]
	public class ControlRecord
	{
		public string Latest_Year;

		public string Used_Car_Publication_Dates;

		public string Older_Car_Publication_Dates;

		public ControlRecord()
		{
		}
	}
}