using System;
using System.Xml.Serialization;

namespace Import.Execute.Configurator.Kbb.UsedArchive
{
	[XmlRoot(Namespace="", IsNullable=false, ElementName="Vehicle_Raw_DataVIN_Equip_Codes")]
	public class VinEquipmentCode
	{
		public string Model_id;

		public string Pattern;

		public string Codes;

		public VinEquipmentCode()
		{
		}
	}
}