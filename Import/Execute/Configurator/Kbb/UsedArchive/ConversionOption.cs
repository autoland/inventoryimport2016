using System;
using System.Xml.Serialization;

namespace Import.Execute.Configurator.Kbb.UsedArchive
{
	[XmlRoot(Namespace="", IsNullable=false, ElementName="Vehicle_Raw_DataConversionOption")]
	public class ConversionOption
	{
		public string Value;

		public ConversionOption()
		{
		}
	}
}