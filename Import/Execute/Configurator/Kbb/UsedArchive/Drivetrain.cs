using System;
using System.Xml.Serialization;

namespace Import.Execute.Configurator.Kbb.UsedArchive
{
	[XmlRoot(Namespace="", IsNullable=false, ElementName="Vehicle_Raw_DataModel_SpecificationsDriveTrain")]
	public class Drivetrain
	{
		public string Code;

		public string Name;

		public Drivetrain()
		{
		}
	}
}