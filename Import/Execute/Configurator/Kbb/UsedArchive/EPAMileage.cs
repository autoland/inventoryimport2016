using System;
using System.Xml.Serialization;

namespace Import.Execute.Configurator.Kbb.UsedArchive
{
	[XmlRoot(Namespace="", IsNullable=false, ElementName="Vehicle_Raw_DataModel_SpecificationsEPA_Mileage")]
	public class EPAMileage
	{
		public string Engine_Code;

		public string Transmission_code;

		public string Drivetrain_code;

		public string EPA_Group_Code;

		public string City_Mpg;

		public string Highway_Mpg;

		public EPAMileage()
		{
		}
	}
}