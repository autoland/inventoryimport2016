using System;
using System.Xml.Serialization;

namespace Import.Execute.Configurator.Kbb.UsedArchive
{
	[XmlRoot(Namespace="", IsNullable=false, ElementName="Vehicle_Raw_DataConversion")]
	public class Conversion
	{
		public string DatabaseId;

		public string ModelId;

		public string EngineCode;

		public string TransmissionId;

		public string DrivetrainCode;

		[XmlElement("Options")]
		public ConversionOption[] Options;

		[XmlElement("Actions")]
		public ConversionAction[] Actions;

		public Conversion()
		{
		}
	}
}