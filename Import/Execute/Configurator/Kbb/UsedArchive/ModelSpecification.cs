using System;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Import.Execute.Configurator.Kbb.UsedArchive
{
	[XmlRoot(Namespace="", IsNullable=false, ElementName="Vehicle_Raw_DataModel_Specifications")]
	public class ModelSpecification
	{
		public string ModelId;

		public string Manufacturer_Code;

		public string Trim;

		public string BodyStyle;

		public string Doors;

		public string EPA_Class;

		public string Seating;

		public string AxleRatio;

		public string WheelBase;

		public string CurbWeight;

		public string TireWheelSize;

		public string TowingCapacity;

		public string HeadRoom;

		public string LegRoom;

		public string ShoulderRoom;

		public string Basic_Warranty;

		public string PowerTrain_Warranty;

		public string CorrosionRust_Warranty;

		public string Performance_Braking;

		public string Safety_EngineImmobilization;

		public string Rating_Front_Driver;

		public string Rating_Front_Passenger;

		public string Rating_Front_Side;

		public string Trucks_GroundClearance;

		public string DealerHoldback;

		public string CountryOfOrigin;

		public string CountryOfAssembly;

		public string Length;

		public string Width;

		public string Height;

		public string SpareTire;

		public string WheelType;

		public string FrontBrakes;

		public string RearBrakes;

		public string TurningRadius;

		public string EPA_Passenger_Feet;

		public string EPA_Trunk_Feet;

		public string EPA_Interior_Feet;

		public string FuelCapacity;

		public string AccessoryAvailability;

		public string Safety_ChildLocks;

		public string Safety_ChildSeatAnchors;

		public string Safety_IntegratedChildSeat;

		public string Safety_Alarm;

		public string Rating_Rear_Side;

		public string Trucks_GVWR;

		public string AirBag;

		public string Performance_0to60;

		public string Performance_TopSpeed;

		public string Performance_QuarterMile;

		public string TireWheelSize_Rear;

		public string Trucks_Payload;

		public string Trucks_BedVolume;

		public string TransferCase;

		public string Rating_Rollover;

		public string IntroductionDate;

		[XmlElement("Transmission", Form=XmlSchemaForm.Unqualified)]
		public Import.Execute.Configurator.Kbb.UsedArchive.Transmission[] Transmission;

		[XmlElement("DriveTrain", Form=XmlSchemaForm.Unqualified)]
		public Drivetrain[] DriveTrain;

		[XmlElement("Engine", Form=XmlSchemaForm.Unqualified)]
		public Import.Execute.Configurator.Kbb.UsedArchive.Engine[] Engine;

		[XmlElement("EPA_Mileage", Form=XmlSchemaForm.Unqualified)]
		public EPAMileage[] EPA_Mileage;

		public ModelSpecification()
		{
		}
	}
}