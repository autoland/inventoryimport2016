using System;
using System.Xml.Serialization;

namespace Import.Execute.Configurator.Kbb.UsedArchive
{
	[XmlRoot(Namespace="", IsNullable=false, ElementName="Vehicle_Raw_DataConversionAction")]
	public class ConversionAction
	{
		public string Value;

		public ConversionAction()
		{
		}
	}
}