using Advocar.Interface;
using System;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Import.Execute.Configurator.Kbb.UsedArchive
{
	[XmlRoot(Namespace="", IsNullable=false, ElementName="Vehicle_Raw_Data")]
	public class RawData : Transaction
	{
		[XmlElement("Control_Record", typeof(ControlRecord), Form=XmlSchemaForm.Unqualified)]
		[XmlElement("Conversion", typeof(Conversion), Form=XmlSchemaForm.Unqualified)]
		[XmlElement("CopyRight", typeof(CopyRight), Form=XmlSchemaForm.Unqualified)]
		[XmlElement("EquipmentDescription", typeof(EquipmentDescription), Form=XmlSchemaForm.Unqualified)]
		[XmlElement("Make", typeof(Make), Form=XmlSchemaForm.Unqualified)]
		[XmlElement("Mileage", typeof(Mileage), Form=XmlSchemaForm.Unqualified)]
		[XmlElement("Model", typeof(Model), Form=XmlSchemaForm.Unqualified)]
		[XmlElement("Model_Specifications", typeof(ModelSpecification), Form=XmlSchemaForm.Unqualified)]
		[XmlElement("PrimaryEquipmentPricing_TradeIn", typeof(PrimaryEquipment), Form=XmlSchemaForm.Unqualified)]
		[XmlElement("Region", typeof(Region), Form=XmlSchemaForm.Unqualified)]
		[XmlElement("Special", typeof(SpecialEquipment), Form=XmlSchemaForm.Unqualified)]
		[XmlElement("VIN", typeof(Vin), Form=XmlSchemaForm.Unqualified)]
		[XmlElement("VIN_Equip_Codes", typeof(VinEquipmentCode), Form=XmlSchemaForm.Unqualified)]
		public object[] Items;

		public RawData()
		{
		}

		public override void Delete()
		{
		}

		protected override void retrievedata()
		{
		}

		public override void Save()
		{
		}
	}
}