using System;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Import.Execute.Configurator.Kbb.UsedArchive
{
	[XmlRoot(Namespace="", IsNullable=false, ElementName="Vehicle_Raw_DataSpecial")]
	public class SpecialEquipment
	{
		public string EquipId;

		public string Code;

		public string Name;

		[XmlElement("Trade-In_Excellent", Form=XmlSchemaForm.Unqualified)]
		public string TradeIn_Excellent;

		[XmlElement("Trade-In_Good", Form=XmlSchemaForm.Unqualified)]
		public string TradeIn_Good;

		[XmlElement("Trade-In_Fair", Form=XmlSchemaForm.Unqualified)]
		public string TradeIn_Fair;

		public string Retail;

		public string PrivateParty_Excellent;

		public string PrivateParty_Good;

		public string PrivateParty_Fair;

		public SpecialEquipment()
		{
		}
	}
}