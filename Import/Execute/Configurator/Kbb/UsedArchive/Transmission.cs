using System;
using System.Xml.Serialization;

namespace Import.Execute.Configurator.Kbb.UsedArchive
{
	[XmlRoot(Namespace="", IsNullable=false, ElementName="Vehicle_Raw_DataModel_SpecificationsTransmission")]
	public class Transmission
	{
		public string Code;

		public string Name;

		public Transmission()
		{
		}
	}
}