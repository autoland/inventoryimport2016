using System;
using System.Xml.Serialization;

namespace Import.Execute.Configurator.Kbb.UsedArchive
{
	[XmlRoot(Namespace="", IsNullable=false, ElementName="Vehicle_Raw_DataModel_SpecificationsEngine")]
	public class Engine
	{
		public string Code;

		public string Name;

		public string ManufacturerCode;

		public string Cylinders;

		public string Displacement;

		public string Bore_Stroke;

		public string CompressionRatio;

		public string Fuel_Type;

		public string Fuel_Induction;

		public string ValveTrainCode;

		public string Valves;

		public string Horsepower_Rpm;

		public string Torque_Rpm;

		public string MaximumEngineSpeed;

		public string VinCode;

		public Engine()
		{
		}
	}
}