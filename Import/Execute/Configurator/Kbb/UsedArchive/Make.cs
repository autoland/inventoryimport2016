using System;
using System.Xml.Serialization;

namespace Import.Execute.Configurator.Kbb.UsedArchive
{
	[XmlRoot(Namespace="", IsNullable=false, ElementName="Vehicle_Raw_DataMake")]
	public class Make
	{
		public string Code;

		public string Name;

		public string Truck_Code;

		public Make()
		{
		}
	}
}