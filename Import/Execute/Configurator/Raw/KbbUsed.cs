using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using Import;
using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace Import.Execute.Configurator.Raw
{
	public class KbbUsed : Job
	{
		private string logFile = "";

		private string path = string.Concat(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "Download\\");

		private string connection = DataAccess.GetConnectionString(Startup.Server, "RAW_KBB_USED", "sa", Startup.SaPassword);

		private string kbbUrl = "ftp://autolandftp:jx#4b8t@ftp.kbb.com/KBBUsedVehiclesXMLFormat-2009-W18.zip";

		public KbbUsed(string logFile)
		{
			this.logFile = logFile;
		}

		private void createDatabase()
		{
			System.Exception er;
			DataAccess data = new DataAccess(Startup.Server, "MASTER", "sa", Startup.SaPassword);
			try
			{
				data.ExecuteStatement("DROP DATABASE RAW_KBB_USED");
			}
			catch
			{
			}
			string query = "CREATE DATABASE RAW_KBB_USED ON PRIMARY (NAME = 'RAW_KBB_USED', FILENAME = 'D:\\MSSQL\\Data\\RAW_KBB_USED.MDF')";
			data.ExecuteStatement(query);
			data = new DataAccess(this.connection);
			try
			{
				data.ExecuteProcedure("sp_adduser @loginame = 'autolandDb', @grpname = 'db_owner'");
			}
			catch
			{
			}
			query = "IF EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'[Make]') AND OBJECTPROPERTY(ID, N'IsUserTable') = 1) DROP TABLE [Make]";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception)
			{
				er = exception;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE TABLE Make (MakeId int NOT NULL, DisplayName varchar(200) NULL)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch
			{
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", query));
			}
			query = "IF EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'[Model]') AND OBJECTPROPERTY(ID, N'IsUserTable') = 1) DROP TABLE [Model]";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception1)
			{
				er = exception1;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE TABLE Model (ModelId int NOT NULL, DisplayName varchar(200) NULL, Description Varchar(200) NULL, MakeId Int NULL, SortOrder Int NULL)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch
			{
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", query));
			}
			query = "IF EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'[ModelYear]') AND OBJECTPROPERTY(ID, N'IsUserTable') = 1) DROP TABLE [ModelYear]";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception2)
			{
				er = exception2;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE TABLE ModelYear (ModelYearId int NOT NULL, ModelId int NULL, YearId Int NULL, Note VarChar(1000) NULL, EffectiveDate varchar(100) NULL, EffectiveDateMissingReason varchar(200) NULL, RevisedFlag Varchar(200) NULL)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch
			{
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", query));
			}
			query = "IF EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'[Trim]') AND OBJECTPROPERTY(ID, N'IsUserTable') = 1) DROP TABLE [Trim]";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception3)
			{
				er = exception3;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE TABLE Trim (TrimId int NOT NULL, ModelId int NULL, YearId Int NULL, DisplayName VarChar(200) NULL, SortOrder Int NULL, TrimName varchar(200) NULL)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch
			{
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", query));
			}
			query = "IF EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'[Vehicle]') AND OBJECTPROPERTY(ID, N'IsUserTable') = 1) DROP TABLE [Vehicle]";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception4)
			{
				er = exception4;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE TABLE Vehicle (VehicleId int NOT NULL, TrimId int NULL, ManufacturerAssignedModelCode varchar(100) NULL, SubTrim VarChar(200) NULL, DisplayName VarChar(200) NULL, VehicleTypeId Int NULL, VehicleTypeDisplayName VarChar(100), SortOrder Int NULL, DisplayNameAdditionalData VarChar(200) NULL, KBBSortOrder Int NULL, AvailabilityStatus VarChar(100) NULL, AvailabilityStatusEndDate VarChar(100) NULL, MarketName VarChar(200) NULL, RelatedVehicleId Int NULL, AvailabilityStatusStartDate VarChar(100) NULL, LowVolume Bit NULL)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch
			{
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", query));
			}
			query = "IF EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'[VINMakePattern]') AND OBJECTPROPERTY(ID, N'IsUserTable') = 1) DROP TABLE [VINMakePattern]";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception5)
			{
				er = exception5;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE TABLE VINMakePattern (MakeId int NULL, Pattern varchar(100) NULL, YearId Int NULL)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch
			{
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", query));
			}
			query = "IF EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'[VINVehiclePattern]') AND OBJECTPROPERTY(ID, N'IsUserTable') = 1) DROP TABLE [VINVehiclePattern]";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception6)
			{
				er = exception6;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE TABLE VINVehiclePattern (YearId int NULL, MakeId int NULL, Pattern varchar(100) NULL, VehicleId Int NULL)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch
			{
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", query));
			}
			query = "IF EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'[Year]') AND OBJECTPROPERTY(ID, N'IsUserTable') = 1) DROP TABLE [Year]";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception7)
			{
				er = exception7;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE TABLE Year (YearId int NOT NULL, DisplayName VarChar(100) NULL, VINCode varchar(200) NULL, MileageZeroPoint int NULL, MaximumDeductionPercentage float NULL)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch
			{
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", query));
			}
			query = "IF EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'[VehicleOption]') AND OBJECTPROPERTY(ID, N'IsUserTable') = 1) DROP TABLE [VehicleOption]";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception8)
			{
				er = exception8;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE TABLE VehicleOption (VehicleOptionId int NOT NULL, OptionTypeId int NULL, OptionTypeDisplayName varchar(200) NULL, DisplayName varchar(200) NULL, DisplayNameAdditionalData varchar(200) NULL, ManufacturerAssignedOptionCode varchar(100) NULL, OptionAvailabilityId int NULL, OptionAvailabilityDisplayName varchar(200) NULL, OptionAvailabilityCode varchar(100) NULL, IsDefaultConfiguration bit NULL, VehicleId int NULL, DetailName varchar(200) NULL, NonBoldName varchar(200) NULL, Footer varchar(200) NULL, SortOrder int NULL, NCBBAdjustmentTypeId int NULL, NCBBAdjustmentTypeDisplayName varchar(200) NULL, NCBBAdjustmentValue float NULL, StartDate datetime NULL, EndDate datetime NULL)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch
			{
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", query));
			}
			query = "IF EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'[VehicleOptionCategory]') AND OBJECTPROPERTY(ID, N'IsUserTable') = 1) DROP TABLE [VehicleOptionCategory]";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception9)
			{
				er = exception9;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE TABLE VehicleOptionCategory (VehicleOptionId Int NULL, CategoryId Int NULL, VehicleOptionCategorySequence Int)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch
			{
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", query));
			}
			query = "IF EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'[Category]') AND OBJECTPROPERTY(ID, N'IsUserTable') = 1) DROP TABLE [Category]";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception10)
			{
				er = exception10;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE TABLE Category (CategoryId Int NOT NULL, CategoryTypeId Int NULL, CategoryDisplayName VarChar(200) NULL, DisplayName VarChar(200) NULL, SortOrder Int NULL, Notes VarChar(1000) NULL, ExclusivityFlag VarChar(100) NULL)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch
			{
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", query));
			}
			query = "IF EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'[VehicleCategory]') AND OBJECTPROPERTY(ID, N'IsUserTable') = 1) DROP TABLE [VehicleCategory]";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception11)
			{
				er = exception11;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE TABLE VehicleCategory (CategoryId Int NOT NULL, VehicleId Int NOT NULL, VehicleCategorySequence Int NULL)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch
			{
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", query));
			}
			query = "IF EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'[Specification]') AND OBJECTPROPERTY(ID, N'IsUserTable') = 1) DROP TABLE [Specification]";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception12)
			{
				er = exception12;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE TABLE Specification (SpecificationId Int NOT NULL, SpecificationTypeId Int NOT NULL, SpecificationTypeDisplayName VarChar(255) NULL, DisplayName VarChar(100) NULL, Units VarChar(100) NULL)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch
			{
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", query));
			}
			query = "IF EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'[SpecificationValue]') AND OBJECTPROPERTY(ID, N'IsUserTable') = 1) DROP TABLE [SpecificationValue]";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception13)
			{
				er = exception13;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE TABLE SpecificationValue (VehicleId Int NOT NULL, SpecificationId Int NOT NULL, ValueIndex Int NOT NULL, Value VarChar(255) NULL)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch
			{
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", query));
			}
			query = "IF EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'[OptionSpecificationValue]') AND OBJECTPROPERTY(ID, N'IsUserTable') = 1) DROP TABLE [OptionSpecificationValue]";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception14)
			{
				er = exception14;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE TABLE OptionSpecificationValue (VehicleOptionId Int NOT NULL, SpecificationId Int NOT NULL, ValueIndex Int NOT NULL, Value VarChar(255) NULL)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch
			{
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", query));
			}
			query = "ALTER TABLE dbo.Category ADD CONSTRAINT PK_Category PRIMARY KEY CLUSTERED (CategoryId)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception15)
			{
				er = exception15;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "ALTER TABLE dbo.Make ADD CONSTRAINT PK_Make PRIMARY KEY CLUSTERED (MakeId)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception16)
			{
				er = exception16;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "ALTER TABLE dbo.Model ADD CONSTRAINT PK_Model PRIMARY KEY CLUSTERED (ModelId)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception17)
			{
				er = exception17;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "ALTER TABLE dbo.ModelYear ADD CONSTRAINT PK_ModelYear PRIMARY KEY CLUSTERED (ModelYearId)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception18)
			{
				er = exception18;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "ALTER TABLE dbo.Trim ADD CONSTRAINT PK_Trim PRIMARY KEY CLUSTERED (TrimId)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception19)
			{
				er = exception19;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "ALTER TABLE dbo.Vehicle ADD CONSTRAINT PK_Vehicle PRIMARY KEY CLUSTERED (VehicleId)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception20)
			{
				er = exception20;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "ALTER TABLE dbo.VehicleOption ADD CONSTRAINT PK_VehicleOption PRIMARY KEY CLUSTERED (VehicleOptionId)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception21)
			{
				er = exception21;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "ALTER TABLE dbo.[Year] ADD CONSTRAINT PK_Year PRIMARY KEY CLUSTERED (YearId)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception22)
			{
				er = exception22;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "ALTER TABLE dbo.Specification ADD CONSTRAINT PK_Specification PRIMARY KEY CLUSTERED (SpecificationId)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception23)
			{
				er = exception23;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE NONCLUSTERED INDEX IX_Category_CategoryTypeId ON Category(CategoryTypeId ASC) INCLUDE (DisplayName)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception24)
			{
				er = exception24;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE NONCLUSTERED INDEX IX_Model_MakeId ON Model(MakeId ASC) INCLUDE (DisplayName)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception25)
			{
				er = exception25;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE NONCLUSTERED INDEX IX_ModelYear_ModelIdYearId ON ModelYear(ModelId ASC, YearId ASC)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception26)
			{
				er = exception26;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE NONCLUSTERED INDEX IX_Trim_ModelIdYearId ON Trim(ModelId ASC, YearId ASC)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception27)
			{
				er = exception27;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE NONCLUSTERED INDEX IX_Vehicle_TrimId ON Vehicle(TrimId ASC)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception28)
			{
				er = exception28;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE NONCLUSTERED INDEX IX_VehicleOption_VehicleId ON VehicleOption(VehicleId ASC)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception29)
			{
				er = exception29;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE NONCLUSTERED INDEX IX_VehicleOption_OptionTypeId ON VehicleOption(OptionTypeId ASC)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception30)
			{
				er = exception30;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE NONCLUSTERED INDEX IX_VehicleOptionCategory_VehicleOptionIdCategoryId ON VehicleOptionCategory(CategoryId ASC, VehicleOptionId ASC)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception31)
			{
				er = exception31;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE NONCLUSTERED INDEX IX_VINMakePattern_Pattern ON VINMakePattern(Pattern ASC)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception32)
			{
				er = exception32;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE NONCLUSTERED INDEX IX_VINMakePattern_YearIdMakeId ON VINMakePattern(YearId ASC, MakeId ASC)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception33)
			{
				er = exception33;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE NONCLUSTERED INDEX IX_VINVehiclePattern_Pattern ON VINVehiclePattern(Pattern ASC)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception34)
			{
				er = exception34;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE NONCLUSTERED INDEX IX_VINVehiclePattern_YearIdMakeId ON VINVehiclePattern(YearId ASC, MakeId ASC)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception35)
			{
				er = exception35;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE NONCLUSTERED INDEX IX_VINVehiclePattern_VehicleId ON VINVehiclePattern(VehicleId ASC)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception36)
			{
				er = exception36;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE NONCLUSTERED INDEX IX_VehicleCategory_CategoryIdVehicleId ON VehicleCategory (CategoryId ASC, VehicleId ASC)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception37)
			{
				er = exception37;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE NONCLUSTERED INDEX IX_SpecificationValue_VehicleId ON SpecificationValue(VehicleId ASC, SpecificationId ASC) INCLUDE (ValueIndex)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception38)
			{
				er = exception38;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
			query = "CREATE NONCLUSTERED INDEX IX_OptionSpecificationValue_VehicleId ON OptionSpecificationValue(VehicleOptionId ASC, SpecificationId ASC) INCLUDE (ValueIndex)";
			try
			{
				data.ExecuteStatement(query);
			}
			catch (System.Exception exception39)
			{
				er = exception39;
				Startup.Status.WriteToFile(this.logFile, string.Concat("Error on executing: ", er.Message, query));
			}
		}

		private string downloadFile()
		{
			string zipFileName = "kbb.zip";
			try
			{
				API.URLDownloadToFile(this.kbbUrl, string.Concat(this.path, zipFileName));
			}
			catch (System.Exception exception)
			{
				Startup.Status.WriteToFile(this.logFile, exception);
			}
			return zipFileName;
		}

		public override void Execute()
		{
			int recordCount;
			Status status = Startup.Status;
			DateTime now = DateTime.Now;
			status.UpdateStatus(string.Concat("Begin KBB Used Raw Data Processing ", now.ToString("MM/dd/yyyy hh:mm:ss")), 0f);
			try
			{
				this.createDatabase();
			}
			catch (SystemException systemException)
			{
				SystemException e = systemException;
				Startup.Status.WriteToFile(this.logFile, "ERRORED on this.createDatabase()");
				Startup.Status.WriteToFile(this.logFile, e);
			}
			string query = "";
			int index = 0;
			DataAccess data = new DataAccess(this.connection);
			while (true)
			{
				string fileName = "";
				index++;
				if (index > 15)
				{
					break;
				}
				if (index == 1)
				{
					fileName = "Make.txt";
				}
				if (index == 2)
				{
					fileName = "Model.txt";
				}
				if (index == 3)
				{
					fileName = "ModelYear.txt";
				}
				if (index == 4)
				{
					fileName = "Trim.txt";
				}
				if (index == 5)
				{
					fileName = "Vehicle.txt";
				}
				if (index == 6)
				{
					fileName = "VINMakePattern.txt";
				}
				if (index == 7)
				{
					fileName = "VINVehiclePattern.txt";
				}
				if (index == 8)
				{
					fileName = "Year.txt";
				}
				if (index == 9)
				{
					fileName = "VehicleOption.txt";
				}
				if (index == 10)
				{
					fileName = "VehicleOptionCategory.txt";
				}
				if (index == 11)
				{
					fileName = "Category.txt";
				}
				if (index == 12)
				{
					fileName = "VehicleCategory.txt";
				}
				if (index == 13)
				{
					fileName = "Specification.txt";
				}
				if (index == 14)
				{
					fileName = "OptionSpecificationValue.txt";
				}
				if (index == 15)
				{
					fileName = "SpecificationValue.txt";
				}
				int count = 0;
				DataFlatFile file = new DataFlatFile(string.Concat(this.path, fileName), "\t", false);
				while (!file.EOF)
				{
					count++;
					if (index == 1)
					{
						query = string.Concat(query, this.insertMake(file[0], file[1]));
					}
					if (index == 2)
					{
						query = string.Concat(query, this.insertModel(file[0], file[1], file[2], file[3], file[4]));
					}
					if (index == 3)
					{
						query = string.Concat(query, this.insertModelYear(file[0], file[1], file[2], file[3], file[4], file[5], file[6]));
					}
					if (index == 4)
					{
						query = string.Concat(query, this.insertTrim(file[0], file[1], file[2], file[3], file[4], file[5]));
					}
					if (index == 5)
					{
						query = string.Concat(query, this.insertVehicle(file[0], file[1], file[2], file[3], file[4], file[5], file[6], file[7], file[8], file[9], file[10], file[11], file[12], file[13], file[14], file[15]));
					}
					if (index == 6)
					{
						query = string.Concat(query, this.insertVINMakePattern(file[0], file[1], file[2]));
					}
					if (index == 7)
					{
						query = string.Concat(query, this.insertVINVehiclePattern(file[0], file[1], file[2], file[3]));
					}
					if (index == 8)
					{
						query = string.Concat(query, this.insertYear(file[0], file[1], file[2], file[3], file[4]));
					}
					if (index == 9)
					{
						query = string.Concat(query, this.insertVehicleOption(file[0], file[1], file[2], file[3], file[4], file[5], file[6], file[7], file[8], file[9], file[10], file[11], file[12], file[13], file[14], file[15], file[16], file[17], file[18], file[19]));
					}
					if (index == 10)
					{
						query = string.Concat(query, this.insertVehicleOptionCategory(file[0], file[1], file[2]));
					}
					if (index == 11)
					{
						query = string.Concat(query, this.insertCategory(file[0], file[1], file[2], file[3], file[4], file[5], file[6]));
					}
					if (index == 12)
					{
						query = string.Concat(query, this.insertVehicleCategory(file[0], file[1], file[2]));
					}
					if (index == 13)
					{
						query = string.Concat(query, this.insertSpecification(file[0], file[1], file[2], file[3], file[4]));
					}
					if (index == 14)
					{
						query = string.Concat(query, this.insertOptionSpecificationValue(file[0], file[1], file[2], file[3]));
					}
					if (index == 15)
					{
						query = string.Concat(query, this.insertSpecificationValue(file[0], file[1], file[2], file[3]));
					}
					if ((count % 500 != 0 ? false : query.Length > 0))
					{
						try
						{
							data.ExecuteStatement(query);
						}
						catch
						{
							Startup.Status.WriteToFile(this.logFile, "FAILED at data.ExecuteStatement(query);");
						}
						query = "";
						Label labelCount = Startup.Status.LabelCount;
						string str = count.ToString();
						recordCount = file.RecordCount;
						labelCount.Text = string.Concat(str, "/", recordCount.ToString());
						Startup.Status.UpdateStatus(string.Concat(fileName, " - UPDATING - ", count.ToString()));
						Startup.Status.UpdateProgress(count, file.RecordCount);
						GC.Collect();
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					try
					{
						data.ExecuteStatement(query);
					}
					catch
					{
						Startup.Status.WriteToFile(this.logFile, "FAILED at data.ExecuteStatement(query);");
					}
					Label label = Startup.Status.LabelCount;
					string str1 = count.ToString();
					recordCount = file.RecordCount;
					label.Text = string.Concat(str1, "/", recordCount.ToString());
					Startup.Status.UpdateStatus(string.Concat(fileName, " - COMPLETED - ", count.ToString()));
					Startup.Status.UpdateProgress(count, file.RecordCount);
					GC.Collect();
					query = "";
				}
			}
			Status statu = Startup.Status;
			now = DateTime.Now;
			statu.UpdateStatus(string.Concat("End KBB Used Raw Data Processing ", now.ToString("MM/dd/yyyy hh:mm:ss")), 0f);
			Status status1 = Startup.Status;
			string str2 = this.logFile;
			now = DateTime.Now;
			status1.WriteToFile(str2, string.Concat("End KBB Used Raw Data Processing ", now.ToString("MM/dd/yyyy hh:mm:ss")));
			Startup.Status.WriteStatusToFile(this.logFile);
		}

		private string insertCategory(string categoryId, string categoryTypeId, string categoryDisplayName, string displayName, string sortOrder, string note, string exclusivityFlag)
		{
			string str;
			if (categoryId == "")
			{
				str = "";
			}
			else if (categoryTypeId == "")
			{
				str = "";
			}
			else
			{
				categoryDisplayName = categoryDisplayName.Replace("'", "''");
				if (sortOrder == "")
				{
					sortOrder = "NULL";
				}
				note = note.Replace("'", "''");
				if (exclusivityFlag == "")
				{
					exclusivityFlag = "NULL";
				}
				else
				{
					exclusivityFlag = string.Concat("'", exclusivityFlag, "'");
				}
				string[] strArrays = new string[] { "INSERT INTO Category VALUES (", categoryId, ", ", categoryTypeId, ", '", categoryDisplayName, "', '", displayName, "', ", sortOrder, ", '", note, "', ", exclusivityFlag, ")\n" };
				str = string.Concat(strArrays);
			}
			return str;
		}

		private string insertMake(string makeId, string makeName)
		{
			string str;
			if (makeId == "")
			{
				str = "";
			}
			else
			{
				string[] strArrays = new string[] { "INSERT INTO Make VALUES (", makeId, ", '", makeName, "')\n" };
				str = string.Concat(strArrays);
			}
			return str;
		}

		private string insertModel(string modelId, string model, string description, string makeId, string sortOrder)
		{
			string str;
			if (modelId == "")
			{
				str = "";
			}
			else
			{
				model = model.Replace("'", "''");
				description = description.Replace("'", "''");
				string[] strArrays = new string[] { "INSERT INTO Model VALUES (", modelId, ", '", model, "', '", description, "', ", makeId, ", ", sortOrder, ")\n" };
				str = string.Concat(strArrays);
			}
			return str;
		}

		private string insertModelYear(string modelYearId, string modelId, string yearId, string note, string effectiveDate, string missingReaseon, string revisedFlag)
		{
			string str;
			if (modelYearId == "")
			{
				str = "";
			}
			else if (modelId == "")
			{
				str = "";
			}
			else if (yearId == "")
			{
				str = "";
			}
			else
			{
				note = note.Replace("'", "''");
				try
				{
					effectiveDate = DateTime.Parse(effectiveDate).ToString("MM/dd/yyyy");
				}
				catch
				{
					effectiveDate = "";
				}
				missingReaseon = missingReaseon.Replace("'", "''");
				string[] strArrays = new string[] { "INSERT INTO ModelYear VALUES (", modelYearId, ", ", modelId, ", ", yearId, ", '", note, "', '", effectiveDate, "', '", missingReaseon, "', '", revisedFlag, "')\n" };
				str = string.Concat(strArrays);
			}
			return str;
		}

		private string insertOptionSpecificationValue(string vehicleOptionId, string specificationId, string valueIndex, string value)
		{
			string str;
			if (vehicleOptionId == "")
			{
				str = "";
			}
			else if (specificationId == "")
			{
				str = "";
			}
			else if (valueIndex == "")
			{
				str = "";
			}
			else if (value == "")
			{
				str = "";
			}
			else
			{
				string[] strArrays = new string[] { "INSERT INTO OptionSpecificationValue VALUES (", vehicleOptionId, ", ", specificationId, ", ", valueIndex, ", '", value, "')\n" };
				str = string.Concat(strArrays);
			}
			return str;
		}

		private string insertSpecification(string specificationId, string specificationTypeId, string specificationTypeDesc, string displayName, string units)
		{
			string str;
			if (specificationId == "")
			{
				str = "";
			}
			else if (specificationTypeId == "")
			{
				str = "";
			}
			else if (displayName == "")
			{
				str = "";
			}
			else
			{
				string[] strArrays = new string[] { "INSERT INTO Specification VALUES (", specificationId, ", ", specificationTypeId, ", '", specificationTypeDesc, "', '", displayName, "', '", units, "')\n" };
				str = string.Concat(strArrays);
			}
			return str;
		}

		private string insertSpecificationValue(string vehicleId, string specificationId, string valueIndex, string value)
		{
			string str;
			if (vehicleId == "")
			{
				str = "";
			}
			else if (specificationId == "")
			{
				str = "";
			}
			else if (valueIndex == "")
			{
				str = "";
			}
			else if (value == "")
			{
				str = "";
			}
			else
			{
				string[] strArrays = new string[] { "INSERT INTO SpecificationValue VALUES (", vehicleId, ", ", specificationId, ", ", valueIndex, ", '", value, "')\n" };
				str = string.Concat(strArrays);
			}
			return str;
		}

		private string insertTrim(string trimId, string modelId, string yearId, string displayName, string sortOrder, string trimName)
		{
			string str;
			if (trimId == "")
			{
				str = "";
			}
			else if (modelId == "")
			{
				str = "";
			}
			else if (yearId == "")
			{
				str = "";
			}
			else
			{
				displayName = displayName.Replace("'", "''");
				trimName = trimName.Replace("'", "''");
				string[] strArrays = new string[] { "INSERT INTO Trim VALUES (", trimId, ", ", modelId, ", ", yearId, ", '", displayName, "', ", sortOrder, ", '", trimName, "')\n" };
				str = string.Concat(strArrays);
			}
			return str;
		}

		private string insertVehicle(string vehicleId, string trimId, string modelCode, string subTrim, string displayName, string vehicleTypeId, string vehicleType, string sortOrder, string additionalData, string kbbSortOrder, string availabilityStatus, string availabilityEndDate, string marketName, string relatedVehicleId, string availabilityStartDate, string lowVolume)
		{
			string str;
			DateTime dateTime;
			if (vehicleId == "")
			{
				str = "";
			}
			else if (trimId == "")
			{
				str = "";
			}
			else
			{
				modelCode = modelCode.Replace("'", "''");
				subTrim = subTrim.Replace("'", "''");
				displayName = displayName.Replace("'", "''");
				if (vehicleTypeId == "")
				{
					vehicleTypeId = "0";
				}
				additionalData = additionalData.Replace("'", "''");
				availabilityStatus = availabilityStatus.Replace("'", "''");
				try
				{
					dateTime = DateTime.Parse(availabilityEndDate);
					availabilityEndDate = dateTime.ToString("MM/dd/yyyy");
				}
				catch
				{
					availabilityEndDate = "";
				}
				try
				{
					dateTime = DateTime.Parse(availabilityStartDate);
					availabilityStartDate = dateTime.ToString("MM/dd/yyyy");
				}
				catch
				{
					availabilityStartDate = "";
				}
				marketName = marketName.Replace("'", "''");
				if (relatedVehicleId == "")
				{
					relatedVehicleId = "0";
				}
				lowVolume = "0";
				string[] strArrays = new string[] { "INSERT INTO Vehicle VALUES (", vehicleId, ", ", trimId, ", '", modelCode, "', '", subTrim, "', '", displayName, "', ", vehicleTypeId, ", '", vehicleType, "', ", sortOrder, ", '", additionalData, "', ", kbbSortOrder, ", '", availabilityStatus, "', '", availabilityEndDate, "', '", marketName, "', ", relatedVehicleId, ", '", availabilityStartDate, "', 0)\n" };
				str = string.Concat(strArrays);
			}
			return str;
		}

		private string insertVehicleCategory(string categoryId, string vehicleId, string sequence)
		{
			string str;
			if (categoryId == "")
			{
				str = "";
			}
			else if (vehicleId == "")
			{
				str = "";
			}
			else
			{
				if (sequence == "")
				{
					sequence = "NULL";
				}
				string[] strArrays = new string[] { "INSERT INTO VehicleCategory VALUES (", categoryId, ", ", vehicleId, ", ", sequence, ")\n" };
				str = string.Concat(strArrays);
			}
			return str;
		}

		private string insertVehicleOption(string vehicleOptionId, string optionTypeId, string optionTypeDisplayName, string displayName, string displayNameAdditionalData, string mfgOptionCode, string optionAvailabilityId, string optionAvailabilityDisplayName, string optionAvailabilityCode, string isDefaultConfig, string vehicleId, string detailName, string nonBoldName, string footer, string sortOrder, string ncbbAdjustmentTypeId, string ncbbAdjustmentTypeDisplayName, string ncbbAdjustmentValue, string startDate, string endDate)
		{
			string str;
			if (vehicleOptionId == "")
			{
				str = "";
			}
			else if (optionTypeId == "")
			{
				str = "";
			}
			else
			{
				optionTypeDisplayName = optionTypeDisplayName.Replace("'", "''");
				displayName = displayName.Replace("'", "''");
				displayNameAdditionalData = displayNameAdditionalData.Replace("'", "''");
				mfgOptionCode = mfgOptionCode.Replace("'", "''");
				if (optionAvailabilityId == "")
				{
					optionAvailabilityId = "NULL";
				}
				optionAvailabilityDisplayName = optionAvailabilityDisplayName.Replace("'", "''");
				optionAvailabilityCode = optionAvailabilityCode.Replace("'", "''");
				if (isDefaultConfig == "")
				{
					isDefaultConfig = "0";
				}
				if (vehicleId == "")
				{
					str = "";
				}
				else
				{
					detailName = detailName.Replace("'", "''");
					nonBoldName = nonBoldName.Replace("'", "''");
					footer = footer.Replace("'", "''");
					if (sortOrder == "")
					{
						sortOrder = "NULL";
					}
					if (ncbbAdjustmentTypeId == "")
					{
						ncbbAdjustmentTypeId = "NULL";
					}
					ncbbAdjustmentTypeDisplayName = ncbbAdjustmentTypeDisplayName.Replace("'", "''");
					if (ncbbAdjustmentValue == "")
					{
						ncbbAdjustmentValue = "NULL";
					}
					if (!Validation.IsDate(startDate))
					{
						startDate = "NULL";
					}
					else
					{
						startDate = string.Concat("'", startDate, "'");
					}
					if (!Validation.IsDate(endDate))
					{
						endDate = "NULL";
					}
					else
					{
						endDate = string.Concat("'", endDate, "'");
					}
					string[] strArrays = new string[] { "INSERT INTO VehicleOption VALUES (", vehicleOptionId, ", ", optionTypeId, ", '", optionTypeDisplayName, "', '", displayName, "', '", displayNameAdditionalData, "', '", mfgOptionCode, "', ", optionAvailabilityId, ", '", optionAvailabilityDisplayName, "', '", optionAvailabilityCode, "', ", isDefaultConfig, ", ", vehicleId, ", '", detailName, "', '", nonBoldName, "', '", footer, "', ", sortOrder, ", ", ncbbAdjustmentTypeId, ", '", ncbbAdjustmentTypeDisplayName, "', ", ncbbAdjustmentValue, ", ", startDate, ", ", endDate, ")\n" };
					str = string.Concat(strArrays);
				}
			}
			return str;
		}

		private string insertVehicleOptionCategory(string vehicleOptionId, string categoryId, string categorySequence)
		{
			string str;
			if (vehicleOptionId == "")
			{
				str = "";
			}
			else if (categoryId == "")
			{
				str = "";
			}
			else
			{
				if (categorySequence == "")
				{
					categorySequence = "NULL";
				}
				string[] strArrays = new string[] { "INSERT INTO VehicleOptionCategory VALUES (", vehicleOptionId, ", ", categoryId, ", ", categorySequence, ")\n" };
				str = string.Concat(strArrays);
			}
			return str;
		}

		private string insertVINMakePattern(string yearId, string pattern, string makeId)
		{
			string str;
			if (yearId == "")
			{
				str = "";
			}
			else if (makeId == "")
			{
				str = "";
			}
			else if (pattern == "")
			{
				str = "";
			}
			else
			{
				string[] strArrays = new string[] { "INSERT INTO VINMakePattern VALUES (", makeId, ", '", pattern, "', ", yearId, ")\n" };
				str = string.Concat(strArrays);
			}
			return str;
		}

		private string insertVINVehiclePattern(string yearId, string makeId, string pattern, string vehicleId)
		{
			string str;
			if (yearId == "")
			{
				str = "";
			}
			else if (makeId == "")
			{
				str = "";
			}
			else if (vehicleId == "")
			{
				str = "";
			}
			else if (pattern == "")
			{
				str = "";
			}
			else
			{
				string[] strArrays = new string[] { "INSERT INTO VINVehiclePattern VALUES (", yearId, ", ", makeId, ", '", pattern, "', ", vehicleId, ")\n" };
				str = string.Concat(strArrays);
			}
			return str;
		}

		private string insertYear(string yearId, string displayName, string vinCode, string mileage, string deduction)
		{
			string str;
			if (yearId == "")
			{
				str = "";
			}
			else
			{
				if (mileage == "")
				{
					mileage = "0";
				}
				if (deduction == "")
				{
					deduction = "0";
				}
				string[] strArrays = new string[] { "INSERT INTO Year VALUES (", yearId, ", '", displayName, "', '", vinCode, "', ", mileage, ", ", deduction, ")\n" };
				str = string.Concat(strArrays);
			}
			return str;
		}

		private void unzipDownloadFile(string zipFilename)
		{
			string winZipPath = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "\\winzip\\winzip32.exe");
			string[] strArrays = new string[] { " -e -o ", this.path, zipFilename, " ", this.path };
			string winZipCommand = string.Concat(strArrays);
			Startup.Status.WriteToFile(this.logFile, winZipCommand);
			try
			{
				Process process = new Process();
				process.StartInfo.FileName = winZipPath;
				process.StartInfo.Arguments = winZipCommand;
				process.Start();
				while (!process.HasExited)
				{
				}
			}
			catch (System.Exception exception)
			{
				Startup.Status.WriteToFile(this.logFile, exception);
			}
			GC.Collect();
		}
	}
}