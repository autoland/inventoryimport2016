using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.Configurator;
using Advocar.Interface;
using Advocar.Tools;
using Import;
using System;

namespace Import.Execute.Configurator.Mapping
{
	public class KbbUsed : Job
	{
		private string logFile = "";

		public int CodeID_ConfiguratorSource;

		public KbbUsed(string logFile)
		{
			this.logFile = logFile;
		}

		private void ConfiguratorImportMakes()
		{
			Startup.Status.UpdateStatus("ConfiguratorImporting Makes", 0f);
			DataAccess data = new DataAccess(Startup.dbConfigurator);
			data.AddParam("@OrderBy", DataAccessParameterType.Text, "MakeName");
			data.ExecuteProcedure("External_KbbUsedMakes");
			VehicleMake make = null;
			int count = 0;
			while (!data.EOF)
			{
				count++;
				VehicleMake vehicleMake = new VehicleMake(Startup.dbConfigurator, "ConfiguratorImport.EXE", 0)
				{
					VehicleMakeId = VehicleMake.GetRecordId(this.CodeID_ConfiguratorSource.ToString(), data["ConfiguratorSourceId"], Startup.dbConfigurator),
					LookupId_ConfiguratorDataSource = this.CodeID_ConfiguratorSource,
					DataSourceId = data["ConfiguratorSourceID"],
					MakeName = data["MakeName"].Trim(),
					LogoFile = "",
					IsDeleted = false
				};
				make = vehicleMake;
				try
				{
					make.Save();
				}
				catch (System.Exception exception)
				{
					Startup.Status.WriteToFile(this.logFile, exception);
				}
				data.MoveNext();
				Startup.Status.UpdateProgress(count, data.RecordCount);
				Startup.Status.UpdateStatus(make.MakeName);
				GC.Collect();
			}
			Startup.Status.ClearProgress();
			Startup.Status.ClearLog();
		}

		private void ConfiguratorImportModels()
		{
			Startup.Status.UpdateStatus("ConfiguratorImporting Models");
			DataAccess data = new DataAccess(Startup.dbConfigurator);
			data.AddParam("@OrderBy", DataAccessParameterType.Text, "CodeID_Year, MakeName, ModelName");
			data.ExecuteProcedure("External_KbbUsedModels");
			VehicleModelYear model = new VehicleModelYear();
			int count = 0;
			while (!data.EOF)
			{
				count++;
				model.VehicleModelYearId = VehicleModelYear.GetRecordId(this.CodeID_ConfiguratorSource.ToString(), data["ConfiguratorSourceID"], Startup.dbConfigurator);
				model = new VehicleModelYear(Startup.dbConfigurator, "ConfiguratorImport.EXE", model.VehicleModelYearId);
				if (model.VehicleModelYearId <= 0)
				{
					model.VehicleMakeId = VehicleMake.GetRecordId(this.CodeID_ConfiguratorSource.ToString(), data["ConfiguratorSourceID_Make"], Startup.dbConfigurator);
					model.LookupId_ConfiguratorDataSource = this.CodeID_ConfiguratorSource;
					model.VehicleYearId = int.Parse(data["CodeID_Year"]);
					model.DataSourceId = data["ConfiguratorSourceID"];
					model.IsDeleted = false;
					model.ModelName = data["ModelName"];
					try
					{
						model.Save();
					}
					catch (System.Exception exception)
					{
						Startup.Status.WriteToFile(this.logFile, exception);
					}
				}
				Startup.Status.UpdateProgress(count, data.RecordCount);
				Status status = Startup.Status;
				object[] vehicleYearId = new object[] { model.VehicleYearId, " ", data["MakeName"], " - ", model.ModelName };
				status.UpdateStatus(string.Concat(vehicleYearId));
				GC.Collect();
				data.MoveNext();
			}
			Startup.Status.ClearProgress();
			Startup.Status.ClearLog();
		}

		private void ConfiguratorImportTrims()
		{
			Startup.Status.UpdateStatus("ConfiguratorImporting Trims");
			DataAccess data = new DataAccess(Startup.dbConfigurator)
			{
				Timeout = 300
			};
			data.ExecuteProcedure("External_KbbUsedTrims");
			VehicleTrim trim = null;
			int count = 0;
			while (!data.EOF)
			{
				count++;
				trim = new VehicleTrim(Startup.dbConfigurator, "ConfiguratorImport.EXE", 0);
				try
				{
					trim.VehicleTrimId = int.Parse(data["VehicleTrimId"]);
				}
				catch
				{
				}
				if (trim.VehicleTrimId > 0)
				{
					VehicleTrim vehicleTrim = new VehicleTrim(Startup.dbConfigurator, "ConfiguratorImport.EXE", trim.VehicleTrimId)
					{
						IsDeleted = false
					};
					trim = vehicleTrim;
				}
				try
				{
					trim.VehicleMakeId = int.Parse(data["VehicleMakeId"]);
				}
				catch
				{
					continue;
				}
				try
				{
					trim.VehicleModelId = int.Parse(data["VehicleModelId"]);
				}
				catch
				{
					continue;
				}
				try
				{
					trim.VehicleModelYearId = int.Parse(data["VehicleModelId"]);
				}
				catch
				{
					continue;
				}
				trim.IsDeleted = false;
				trim.LookupId_ConfiguratorDataSource = this.CodeID_ConfiguratorSource;
				try
				{
					trim.DataSourceId = data["ConfiguratorSourceID"];
				}
				catch
				{
					continue;
				}
				try
				{
					trim.VehicleYearId = int.Parse(data["YearId"]);
				}
				catch
				{
					continue;
				}
				trim.TrimName = data["TrimName"];
				trim.TrimOtherInfo = data["TrimName"];
				trim.Doors = int.Parse((Validation.IsNumeric(data["Doors"]) ? data["Doors"] : "0"));
				try
				{
					trim.LookupId_Style = int.Parse(data["StyleId"]);
				}
				catch
				{
				}
				trim.Style = data["StyleDesc"];
				try
				{
					trim.LookupId_Drivetrain = int.Parse(data["DrivetrainId"]);
				}
				catch
				{
				}
				trim.Drivetrain = data["DrivetrainDesc"];
				try
				{
					trim.LookupId_Fuel = int.Parse(data["FuelId"]);
				}
				catch
				{
				}
				trim.Fuel = data["FuelDesc"];
				trim.IsTurbo = (data["TurboYN"] == "Y" ? true : false);
				try
				{
					trim.LookupId_Engine = int.Parse(data["EngineId"]);
				}
				catch
				{
				}
				trim.Engine = data["EngineDesc"];
				try
				{
					trim.LookupId_Transmission = int.Parse(data["TransmissionId"]);
				}
				catch
				{
				}
				trim.Transmission = data["TransmissionDesc"];
				try
				{
					trim.Save();
				}
				catch (System.Exception exception)
				{
					Startup.Status.WriteStatusToFile(exception.Message);
					data.MoveNext();
					continue;
				}
				Status status = Startup.Status;
				string[] strArrays = new string[] { "Completed Trim ConfiguratorImport: ", null, null, null, null, null, null, null };
				string[] str = strArrays;
				str[1] = trim.VehicleYearId.ToString();
				str[2] = " ";
				str[3] = data["MakeName"];
				str[4] = " - ";
				str[5] = data["ModelName"];
				str[6] = " ";
				str[7] = trim.TrimName;
				status.UpdateStatus(string.Concat(str));
				GC.Collect();
				data.MoveNext();
				if (count % 100 == 0)
				{
					Startup.Status.ClearLog();
				}
				Startup.Status.UpdateProgress(count, data.RecordCount);
			}
			Startup.Status.ClearProgress();
			Startup.Status.ClearLog();
		}

		private void ConfiguratorImportVins()
		{
			Startup.Status.UpdateStatus("ConfiguratorImporting Vins", 0f);
			DataAccess dataAccess = new DataAccess(Startup.dbConfigurator)
			{
				Timeout = 600
			};
			dataAccess.ExecuteProcedure("External_KbbImportVins");
			DataAccess dataAccess1 = new DataAccess(Startup.dbConfigurator)
			{
				Timeout = 600
			};
			dataAccess1.ExecuteProcedure("External_KbbUsedMakesCleanup");
			Startup.Status.ClearLog();
		}

		public override void Execute()
		{
			SystemException e;
			Status status = Startup.Status;
			DateTime now = DateTime.Now;
			status.UpdateStatus(string.Concat("BEGIN KBB USED CONFIGURATOR ConfiguratorImport - ", now.ToString("MM/dd/yyyy hh:mm:ss")), 0f);
			this.CodeID_ConfiguratorSource = Lookup.GetRecordId("KbbUsed", LookupType.ConfiguratorDataSource, Startup.dbAime);
			try
			{
				this.ConfiguratorImportMakes();
				Startup.Status.WriteToFile(this.logFile, "Imported Makes");
			}
			catch (SystemException systemException)
			{
				e = systemException;
				Startup.Status.WriteToFile(this.logFile, "FAILED at this.ConfiguratorImportMakes();");
				Startup.Status.WriteToFile(this.logFile, e);
			}
			try
			{
				this.ConfiguratorImportModels();
				Startup.Status.WriteToFile(this.logFile, "Imported Models");
			}
			catch (SystemException systemException1)
			{
				e = systemException1;
				Startup.Status.WriteToFile(this.logFile, "FAILED at this.ConfiguratorImportModels();");
				Startup.Status.WriteToFile(this.logFile, e);
			}
			try
			{
				this.ConfiguratorImportTrims();
				Startup.Status.WriteToFile(this.logFile, "Imported Trims");
			}
			catch (SystemException systemException2)
			{
				e = systemException2;
				Startup.Status.WriteToFile(this.logFile, "FAILED at this.ConfiguratorImportTrims();");
				Startup.Status.WriteToFile(this.logFile, e);
			}
			try
			{
				this.ConfiguratorImportVins();
				Startup.Status.WriteToFile(this.logFile, "Imported Vins");
			}
			catch (SystemException systemException3)
			{
				e = systemException3;
				Startup.Status.WriteToFile(this.logFile, "FAILED at this.ConfiguratorImportTrims();");
				Startup.Status.WriteToFile(this.logFile, e);
			}
			Status statu = Startup.Status;
			string str = this.logFile;
			now = DateTime.Now;
			statu.WriteToFile(str, string.Concat("COMPLETED KBB USED CONFIGURATOR ConfiguratorImport - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
			Startup.Status.WriteStatusToFile(this.logFile);
		}
	}
}