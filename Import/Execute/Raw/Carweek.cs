using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;

namespace Import.Execute.Raw
{
	public class Carweek : JobRaw
	{
		public Carweek()
		{
		}

		public override void Execute()
		{
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\Carweek\\");
			this.SourceFile = "cwinventory.txt";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("CARWEEK", LookupType.DataProvider, Startup.dbAime);
			if (base.Start())
			{
				this.import();
				base.Complete();
			}
		}

		private void import()
		{
			Exception err;
			string fileName = string.Concat(this.Directory, this.SourceFile);
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				DataFlatFile file = new DataFlatFile(fileName, ",", true);
				RawCarweek vehicle = new RawCarweek();
				file.MoveNext();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						if (!base.IsDealerPresent(file[0]))
						{
							file.MoveNext();
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							continue;
						}
						else
						{
							JobExecutionRaw noOfRecords = this.Raw;
							noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
							RawCarweek rawCarweek = new RawCarweek(Startup.dbInventoryImport, "IMPORT.EXE", 0)
							{
								JobExecutionRawID = this.Raw.JobExecutionRawId,
								DealerId = int.Parse((file[0] == "" ? "0" : file[0])),
								NewUsed = file[1],
								Vin = file[2],
								StockNumber = file[3],
								Make = file[4],
								Model = file[5],
								ModelYear = int.Parse((file[6] == "" ? "0" : file[6])),
								Trim = file[7],
								BodyStyle = file[8],
								Mileage = int.Parse((file[9] == "" ? "0" : file[9])),
								EngineDescription = file[10],
								Cylinders = int.Parse((file[11] == "" ? "0" : file[11])),
								FuelType = file[12],
								Transmission = file[13],
								Price = float.Parse((file[14] == "" ? "0" : file[14])),
								SpecialPrice = float.Parse((file[15] == "" ? "0" : file[15])),
								ExteriorColor = file[16],
								InteriorColor = file[17],
								Options = file[18],
								Description = file[19],
								Images = file[20]
							};
							query = string.Concat(query, rawCarweek.SaveQuery(), "\n");
							if (this.Raw.NoOfRecords % this.Interval == 0)
							{
								batch.ExecuteStatement(query);
								query = "";
							}
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							if (this.Raw.NoOfRecords % 100 == 0)
							{
								Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
							}
							GC.Collect();
						}
					}
					catch (Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}