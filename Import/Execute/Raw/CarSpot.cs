using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;

namespace Import.Execute.Raw
{
	public class CarSpot : JobRaw
	{
		public CarSpot()
		{
		}

		public override void Execute()
		{
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\CarSpot\\");
			this.SourceFile = "carspot.txt";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("Carspot", LookupType.DataProvider, Startup.dbAime);
			if (base.Start())
			{
				this.importCarSpot();
				base.Complete();
			}
		}

		private void importCarSpot()
		{
			Exception err;
			string fileName = string.Concat(this.Directory, this.SourceFile);
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				DataFlatFile file = new DataFlatFile(fileName, "|", true);
				RawCarSpot carspot = new RawCarSpot();
				file.MoveNext();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						if (!base.IsDealerPresent(file[0]))
						{
							file.MoveNext();
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							continue;
						}
						else
						{
							JobExecutionRaw noOfRecords = this.Raw;
							noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
							RawCarSpot rawCarSpot = new RawCarSpot(Startup.dbInventoryImport, "IMPORT.EXE", 0)
							{
								JobExecutionRawId = this.Raw.JobExecutionRawId,
								DealerID = file[0],
								StockID = file[1],
								Year = int.Parse((file[2].Trim() == "" ? "0000" : file[2])),
								VIN = file[3],
								NewUsed = file[4],
								Make = file[5],
								Model = file[6],
								Trim = file[7],
								ModelNumber = file[8],
								BodyStyle = file[9],
								Mileage = int.Parse((file[10].Trim() == "" ? "0" : file[10])),
								ExtColor = file[11],
								IntColor = file[12],
								Transmission = file[13],
								Doors = int.Parse((file[14].Trim() == "" ? "0" : file[14])),
								SellingPrice = float.Parse((file[15].Trim() == "" ? "0" : file[15])),
								MSRPPrice = float.Parse((file[16].Trim() == "" ? "0" : file[16])),
								InvoicePrice = float.Parse((file[17].Trim() == "" ? "0" : file[17])),
								DateEntered = file[18],
								CertifiedYN = file[19],
								Engine = file[20],
								FuelCode = file[21],
								DriveTrain = file[22],
								Restraint = file[23],
								DealerComment = file[24],
								VehicleComment = file[25],
								ImageUrls = file[26],
								Packages = file[27],
								Options = file[28]
							};
							carspot = rawCarSpot;
							query = string.Concat(query, carspot.SaveQuery(), "\n");
							if (this.Raw.NoOfRecords % this.Interval == 0)
							{
								batch.ExecuteStatement(query);
								query = "";
							}
							Status status = Startup.Status;
							object[] upper = new object[] { "CARSPOT - ", carspot.VIN.ToUpper(), " - ", carspot.Year, " ", carspot.Make, " ", carspot.Model };
							status.UpdateStatus(string.Concat(upper));
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							if (this.Raw.NoOfRecords % 100 == 0)
							{
								Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
							}
							GC.Collect();
						}
					}
					catch (Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}