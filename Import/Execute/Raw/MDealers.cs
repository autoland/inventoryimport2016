using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;

namespace Import.Execute.Raw
{
	public class MDealers : JobRaw
	{
		public MDealers()
		{
		}

		public override void Execute()
		{
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\MDealers\\");
			this.SourceFile = "inventory.txt";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("MDealers", LookupType.DataProvider, Startup.dbAime);
			if (base.Start())
			{
				this.importMDealers();
				base.Complete();
			}
		}

		private void importMDealers()
		{
			Exception err;
			string fileName = string.Concat(this.Directory, this.SourceFile);
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				DataFlatFile file = new DataFlatFile(fileName, "|", true);
				RawMdealers mdealers = new RawMdealers();
				file.MoveNext();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						if (!base.IsDealerPresent(file[0]))
						{
							file.MoveNext();
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							continue;
						}
						else
						{
							JobExecutionRaw noOfRecords = this.Raw;
							noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
							RawMdealers rawMdealer = new RawMdealers(Startup.dbInventoryImport, "IMPORT.EXE", 0)
							{
								JobExecutionRawId = this.Raw.JobExecutionRawId,
								DealerID = file[0],
								NewUsed = file[1],
								StockID = file[2],
								VehicleDescription = file[3],
								Year = int.Parse((file[4] == "" ? "0" : file[4])),
								Make = file[5],
								Model = file[6],
								Trim = file[7],
								VIN = file[8],
								Style = file[9],
								Engine = file[10],
								DriveTrain = file[11],
								Transmission = file[12],
								ExtColor = file[13],
								IntColor = file[14],
								Options = file[15],
								Mileage = int.Parse((file[16] == "" ? "0" : file[16])),
								AcquisitionPrice = float.Parse((file[17] == "" ? "0" : file[17])),
								InvoicePrice = float.Parse((file[18] == "" ? "0" : file[18])),
								SellingPrice = float.Parse((file[19] == "" ? "0" : file[19])),
								RetailPrice = float.Parse((file[20] == "" ? "0" : file[20])),
								MarketIndex = file[21],
								MSRPPrice = float.Parse((file[22] == "" ? "0" : file[22])),
								ImageUrls = file[23]
							};
							mdealers = rawMdealer;
							query = string.Concat(query, mdealers.SaveQuery(), "\n");
							if (this.Raw.NoOfRecords % this.Interval == 0)
							{
								batch.ExecuteStatement(query);
								query = "";
							}
							Status status = Startup.Status;
							object[] upper = new object[] { "MDEALERS - ", mdealers.VIN.ToUpper(), " - ", mdealers.Year, " ", mdealers.Make, " ", mdealers.Model };
							status.UpdateStatus(string.Concat(upper));
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							if (this.Raw.NoOfRecords % 100 == 0)
							{
								Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
							}
							GC.Collect();
						}
					}
					catch (Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}