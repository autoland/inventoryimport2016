using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;

namespace Import.Execute.Raw
{
	public class TreisterDs : JobRaw
	{
		public TreisterDs()
		{
		}

		public override void Execute()
		{
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\TreisterDs\\");
			this.SourceFile = "treisterds.csv";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("TreisterDs", LookupType.DataProvider, Startup.dbAime);
			if (base.Start())
			{
				this.import();
				base.Complete();
			}
		}

		private void import()
		{
			Exception err;
			string fileName = string.Concat(this.Directory, this.SourceFile);
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				DataFlatFile file = new DataFlatFile(fileName, ",", false);
				RawTreisterDs treister = new RawTreisterDs();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						if (!base.IsDealerPresent(file[0]))
						{
							file.MoveNext();
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							continue;
						}
						else
						{
							JobExecutionRaw noOfRecords = this.Raw;
							noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
							RawTreisterDs rawTreisterD = new RawTreisterDs(Startup.dbInventoryImport, "IMPORT.EXE", 0)
							{
								JobExecutionRawId = this.Raw.JobExecutionRawId,
								DealerId = file[0],
								StockNo = file[1],
								Year = int.Parse((file[2] == "" ? "0" : file[2])),
								Make = file[3],
								Model = file[4],
								Trim = file[5],
								Vin = file[6],
								Mileage = int.Parse((file[7] == "" ? "0" : file[7])),
								SellingPrice = float.Parse((file[8] == "" ? "0" : file[8])),
								ExteriorColor = file[9],
								InteriorColor = file[10],
								Transmission = file[11],
								ImageUrls = file[12],
								Options = file[13]
							};
							treister = rawTreisterD;
							query = string.Concat(query, treister.SaveQuery(), "\n");
							if (this.Raw.NoOfRecords % this.Interval == 0)
							{
								batch.ExecuteStatement(query);
								query = "";
							}
							Status status = Startup.Status;
							object[] upper = new object[] { "TREISTER - ", treister.Vin.ToUpper(), " - ", treister.Year, " ", treister.Make, " ", treister.Model };
							status.UpdateStatus(string.Concat(upper));
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							if (this.Raw.NoOfRecords % 100 == 0)
							{
								Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
							}
							GC.Collect();
						}
					}
					catch (Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}