using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;

namespace Import.Execute.Raw
{
	public class AutoRevolution : JobRaw
	{
		public AutoRevolution()
		{
		}

		public override void Execute()
		{
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\AutoRevolution\\");
			this.SourceFile = "AutoRevolution.txt";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("AutoRevolution", LookupType.DataProvider, Startup.dbAime);
			if (base.Start())
			{
				this.importAutoRevolution();
				base.Complete();
			}
		}

		private void importAutoRevolution()
		{
			Exception err;
			string fileName = string.Concat(this.Directory, this.SourceFile);
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				DataFlatFile file = new DataFlatFile(fileName, "|", true);
				RawAutoRevolution autorev = new RawAutoRevolution();
				file.MoveNext();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						if (!base.IsDealerPresent(file[1]))
						{
							file.MoveNext();
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							continue;
						}
						else
						{
							JobExecutionRaw noOfRecords = this.Raw;
							noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
							RawAutoRevolution rawAutoRevolution = new RawAutoRevolution(Startup.dbInventoryImport, "IMPORT.EXE", 0)
							{
								JobExecutionRawId = this.Raw.JobExecutionRawId,
								MatchID = file[0],
								DealerID = file[1],
								NewUsed = (file[2] == "1" ? 1 : 0),
								VIN = file[3],
								StockID = file[4],
								Category = file[5],
								Type = file[6],
								Year = int.Parse((file[7].Trim() == "" ? "0" : file[7])),
								Make = file[8],
								Model = file[9],
								Body = file[10],
								Doors = int.Parse((file[11].Trim() == "" ? "0" : file[11])),
								ExtColor = file[12],
								IntColor = file[13],
								Engine = file[14],
								Fuel = file[15],
								Transmission = file[16],
								Cylinders = file[17]
							};
							autorev = rawAutoRevolution;
							autorev.Type = file[18];
							autorev.Mileage = int.Parse((file[19].Trim() == "" ? "0" : file[19]));
							autorev.RetailPrice = float.Parse((file[20].Trim() == "" ? "0" : file[20]));
							autorev.WholesalePrice = float.Parse((file[21].Trim() == "" ? "0" : file[21]));
							autorev.Certified = int.Parse((file[22].Trim() == "" ? "0" : file[22]));
							autorev.Special = int.Parse((file[23].Trim() == "" ? "0" : file[23]));
							autorev.Options = file[25];
							autorev.ImageURLs = file[26];
							query = string.Concat(query, autorev.SaveQuery(), "\n");
							if (this.Raw.NoOfRecords % this.Interval == 0)
							{
								batch.ExecuteStatement(query);
								query = "";
							}
							Status status = Startup.Status;
							object[] upper = new object[] { "AUTO REVOLUTION - ", autorev.VIN.ToUpper(), " - ", autorev.Year, " ", autorev.Make, " ", autorev.Model };
							status.UpdateStatus(string.Concat(upper));
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							if (this.Raw.NoOfRecords % 100 == 0)
							{
								Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
							}
							GC.Collect();
						}
					}
					catch (Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}