using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;

namespace Import.Execute.Raw
{
	public class VirtualDealer : JobRaw
	{
		public VirtualDealer()
		{
		}

		public override void Execute()
		{
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\VirtualDealer\\");
			this.SourceFile = "autofusion.csv";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("VirtualDealer", LookupType.DataProvider, Startup.dbAime);
			if (base.Start())
			{
				this.importVirtualDealer();
				base.Complete();
			}
		}

		private void importVirtualDealer()
		{
			Exception err;
			string fileName = string.Concat(this.Directory, this.SourceFile);
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				DataFlatFile file = new DataFlatFile(fileName, ",", true);
				RawVirtualDealer vdealer = new RawVirtualDealer();
				file.MoveNext();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						if (!base.IsDealerPresent(file[0]))
						{
							file.MoveNext();
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							continue;
						}
						else
						{
							JobExecutionRaw noOfRecords = this.Raw;
							noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
							RawVirtualDealer rawVirtualDealer = new RawVirtualDealer(Startup.dbInventoryImport, "IMPORT.EXE", 0)
							{
								JobExecutionRawId = this.Raw.JobExecutionRawId,
								DealerId = file[0],
								NewUsed = file[1],
								Vin = file[2],
								StockNum = file[3],
								VehicleYear = file[4],
								Make = file[5],
								Model = file[6],
								Trim = file[7],
								Engine = file[8],
								Transmission = file[9],
								BodyStyle = file[10],
								Exterior = file[11],
								Interior = file[12],
								Mileage = int.Parse((file[13].Trim() == "" ? "0" : file[13])),
								Price = float.Parse((file[14].Trim() == "" ? "0" : file[14])),
								SpecialPrice = float.Parse((file[15].Trim() == "" ? "0" : file[15])),
								Certified = file[16],
								Images = file[17],
								Options = file[18]
							};
							vdealer = rawVirtualDealer;
							query = string.Concat(query, vdealer.SaveQuery(), "\n");
							if (this.Raw.NoOfRecords % this.Interval == 0)
							{
								batch.ExecuteStatement(query);
								query = "";
							}
							Status status = Startup.Status;
							string[] upper = new string[] { "VIRTUAL DEALER - ", vdealer.Vin.ToUpper(), " - ", vdealer.VehicleYear, " ", vdealer.Make, " ", vdealer.Model };
							status.UpdateStatus(string.Concat(upper));
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							if (this.Raw.NoOfRecords % 100 == 0)
							{
								Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
							}
							GC.Collect();
						}
					}
					catch (Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}