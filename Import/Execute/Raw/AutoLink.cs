using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;

namespace Import.Execute.Raw
{
	public class AutoLink : JobRaw
	{
		public AutoLink()
		{
		}

		public override void Execute()
		{
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\Autolink\\");
			this.SourceFile = "inventory.csv";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("Autolink", LookupType.DataProvider, Startup.dbAime);
			if (base.Start())
			{
				this.importAutoLink();
				base.Complete();
			}
		}

		private void importAutoLink()
		{
			Exception err;
			string fileName = string.Concat(this.Directory, this.SourceFile);
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				DataFlatFile file = new DataFlatFile(fileName, ",", true);
				RawAutoLink autolink = new RawAutoLink();
				file.MoveNext();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						if (!base.IsDealerPresent(file[0]))
						{
							file.MoveNext();
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							continue;
						}
						else
						{
							JobExecutionRaw noOfRecords = this.Raw;
							noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
							RawAutoLink rawAutoLink = new RawAutoLink(Startup.dbInventoryImport, "IMPORT.EXE", 0)
							{
								JobExecutionRawId = this.Raw.JobExecutionRawId,
								DealerID = file[0],
								StockID = file[1],
								Year = int.Parse(file[2]),
								VIN = file[3],
								NewUsed = file[4],
								Make = file[5],
								Model = file[6],
								Series = file[7],
								ModelNumber = file[8],
								Body = file[9],
								Mileage = int.Parse((file[10] == "" ? "0" : file[10])),
								ExtColor = file[11],
								IntColor = file[12],
								Transmission = file[13],
								Doors = int.Parse((file[14] == "" ? "0" : file[14])),
								AskingPrice = float.Parse((file[15] == "" ? "0" : file[15])),
								MSRP = float.Parse((file[16] == "" ? "0" : file[16])),
								Invoice = float.Parse((file[17] == "" ? "0" : file[17])),
								DateEntered = file[18],
								CertifiedYesNo = file[19],
								Engine = file[20],
								FuelCode = file[21],
								DriveTrain = file[22],
								Restraint = file[23],
								DealerComments = file[24],
								VehicleComments = file[25],
								ImageUrls = file[26],
								PackageCode = file[27],
								Options = file[28]
							};
							autolink = rawAutoLink;
							query = string.Concat(query, autolink.SaveQuery(), "\n");
							if (this.Raw.NoOfRecords % this.Interval == 0)
							{
								batch.ExecuteStatement(query);
								query = "";
							}
							Status status = Startup.Status;
							object[] upper = new object[] { "AUTOLINK - ", autolink.VIN.ToUpper(), " - ", autolink.Year, " ", autolink.Make, " ", autolink.Model };
							status.UpdateStatus(string.Concat(upper));
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							if (this.Raw.NoOfRecords % 100 == 0)
							{
								Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
							}
							GC.Collect();
						}
					}
					catch (Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}