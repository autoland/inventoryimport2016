using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;

namespace Import.Execute.Raw
{
	public class VinControl : JobRaw
	{
		public VinControl()
		{
		}

		public override void Execute()
		{
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\VinControl\\");
			this.SourceFile = "37375.txt";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("VinControl", LookupType.DataProvider, Startup.dbAime);
			if (base.Start())
			{
				this.VinControlImport();
				base.Complete();
			}
		}

		private void VinControlImport()
		{
			Exception err;
			string fileName = string.Concat(this.Directory, this.SourceFile);
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				DataFlatFile file = new DataFlatFile(fileName, ",", true);
				RawVinControl vehicle = new RawVinControl();
				file.MoveNext();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						if (!base.IsDealerPresent(file[0]))
						{
							file.MoveNext();
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							continue;
						}
						else
						{
							JobExecutionRaw noOfRecords = this.Raw;
							noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
							RawVinControl rawVinControl = new RawVinControl(Startup.dbInventoryImport, "IMPORT.EXE", 0)
							{
								JobExecutionRawId = this.Raw.JobExecutionRawId,
								DealerId = file[0],
								Vin = file[1],
								Stock = file[2],
								NewUsed = file[3],
								VehicleYear = int.Parse((file[4] == "" ? "0" : file[4])),
								Make = file[5],
								Model = file[6],
								ModelNumber = file[7],
								Body = file[8],
								Transmission = file[9],
								Trim = file[10],
								Doors = int.Parse((file[11] == "" ? "0" : file[11])),
								Miles = int.Parse((file[12] == "" ? "0" : file[12])),
								EngineCylinders = int.Parse((file[13] == "" ? "0" : file[13])),
								EngineDisplacement = file[14],
								Drivetrain = file[15],
								ExteriorColor = file[16],
								InteriorColor = file[17],
								Invoice = file[18],
								Msrp = float.Parse((file[19] == "" ? "0" : file[19])),
								BookValue = file[20],
								SellingPrice = float.Parse((file[21] == "" ? "0" : file[21])),
								DateInStock = file[22],
								Certified = file[23],
								Description = file[24],
								Options = file[25],
								PhotoUrLs = file[26],
								PriorRental = file[27],
								DealerDemonstrator = file[28],
								Unwind = file[29]
							};
							query = string.Concat(query, rawVinControl.SaveQuery(), "\n");
							if (this.Raw.NoOfRecords % this.Interval == 0)
							{
								batch.ExecuteStatement(query);
								query = "";
							}
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							if (this.Raw.NoOfRecords % 100 == 0)
							{
								Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
							}
							GC.Collect();
						}
					}
					catch (Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}