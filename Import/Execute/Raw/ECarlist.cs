using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;
using System.Text;

namespace Import.Execute.Raw
{
	internal class ECarlist : JobRaw
	{
		public ECarlist()
		{
		}

		public override void Execute()
		{
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\ECarlist\\");
			this.SourceFile = "3726.txt";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("ECarlist", LookupType.DataProvider, Startup.dbAime);
			if (base.Start())
			{
				this.ImportECarlist();
				base.Complete();
			}
		}

		private void ImportECarlist()
		{
			Exception err;
			string fileName = string.Concat(this.Directory, this.SourceFile);
			StringBuilder query = new StringBuilder();
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				DataFlatFile file = new DataFlatFile(fileName, ",", true);
				while (!file.EOF)
				{
					try
					{
						file.MoveNext();
						RawECarlist rawECarlist = new RawECarlist(Startup.dbInventoryImport, "IMPORT.EXE", 0);
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecords = raw.NoOfRecords + 1;
						JobExecutionRaw noOfRecordsInFile = this.Raw;
						noOfRecordsInFile.NoOfRecordsInFile = noOfRecordsInFile.NoOfRecordsInFile + 1;
						this.Raw.NoOfDealers = 1;
						rawECarlist.JobExecutionRawId = this.Raw.JobExecutionRawId;
						rawECarlist.DealerId = (!string.IsNullOrEmpty(file[0]) ? Convert.ToInt32(file[0]) : 0);
						rawECarlist.FeedId = (!string.IsNullOrEmpty(file[1]) ? Convert.ToInt32(file[1]) : 0);
						rawECarlist.Vin = file[2];
						rawECarlist.Stock = file[3];
						rawECarlist.Mileage = (!string.IsNullOrEmpty(file[4]) ? Convert.ToInt32(file[4]) : 0);
						rawECarlist.Year = (!string.IsNullOrEmpty(file[5]) ? Convert.ToInt32(file[5]) : 0);
						rawECarlist.Make = file[6];
						rawECarlist.Model = file[7];
						rawECarlist.Trim = file[8];
						rawECarlist.Body = file[9];
						rawECarlist.Transmission = file[10];
						rawECarlist.Engine = file[11];
						rawECarlist.InteriorColor = file[12];
						rawECarlist.ExteriorColor = file[13];
						rawECarlist.GenericInteriorColor = file[14];
						rawECarlist.GenericExteriorColor = file[15];
						rawECarlist.Price = (!string.IsNullOrEmpty(file[16]) ? Convert.ToDouble(file[16]) : 0);
						rawECarlist.RetailPrice = (!string.IsNullOrEmpty(file[17]) ? Convert.ToDouble(file[17]) : 0);
						rawECarlist.DealerCost = (!string.IsNullOrEmpty(file[18]) ? Convert.ToDouble(file[18]) : 0);
						rawECarlist.ModelCode = file[19];
						rawECarlist.ExteriorColorCode = file[20];
						rawECarlist.InteriorColorCode = file[21];
						rawECarlist.Certified = Convert.ToInt32(file[22]) > 0;
						string certified = file[22];
						if (!string.IsNullOrEmpty(certified))
						{
							rawECarlist.Certified = Convert.ToInt32(certified) > 0;
						}
						else
						{
							rawECarlist.Certified = false;
						}
						rawECarlist.Condition = file[23];
						rawECarlist.InDate = (!string.IsNullOrEmpty(file[24]) ? Convert.ToDateTime(file[24]) : DateTime.MinValue);
						rawECarlist.Comments = file[25];
						rawECarlist.ImageUrls = file[26];
						rawECarlist.Options = file[28];
						rawECarlist.StandardEquipment = file[28];
						rawECarlist.OptionCodes = file[29];
						rawECarlist.MpgCity = (!string.IsNullOrEmpty(file[30]) ? Convert.ToInt32(file[30]) : 0);
						rawECarlist.MpgHighway = (!string.IsNullOrEmpty(file[31]) ? Convert.ToInt32(file[31]) : 0);
						rawECarlist.Doors = (!string.IsNullOrEmpty(file[32]) ? Convert.ToInt32(file[32]) : 0);
						query.Append(rawECarlist.SaveQuery());
						query.Append(Environment.NewLine);
						if (this.Raw.NoOfRecords % this.Interval == 0)
						{
							batch.ExecuteStatement(query.ToString());
							query.Remove(0, query.Length);
						}
						Status status = Startup.Status;
						object[] upper = new object[] { "ECARLIST - ", rawECarlist.Vin.ToUpper(), " - ", rawECarlist.Year, " ", rawECarlist.Make, " ", rawECarlist.Model };
						status.UpdateStatus(string.Concat(upper));
						Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
						if (this.Raw.NoOfRecords % 100 == 0)
						{
							Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
						}
					}
					catch (Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
				}
			}
			catch (Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}