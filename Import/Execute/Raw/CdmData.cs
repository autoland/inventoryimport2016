using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;

namespace Import.Execute.Raw
{
	public class CdmData : JobRaw
	{
		public CdmData()
		{
		}

		public override void Execute()
		{
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\CDMData\\");
			this.SourceFile = "cdmdata.csv";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("CDMData", LookupType.DataProvider, Startup.dbAime);
			if (base.Start())
			{
				this.importCdmData();
				base.Complete();
			}
		}

		private void importCdmData()
		{
			Exception err;
			string fileName = string.Concat(this.Directory, this.SourceFile);
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				DataFlatFile file = new DataFlatFile(fileName, ",", true);
				RawCdmData cdmdata = new RawCdmData();
				file.MoveNext();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						if (!base.IsDealerPresent(file[0]))
						{
							file.MoveNext();
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							continue;
						}
						else
						{
							JobExecutionRaw noOfRecords = this.Raw;
							noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
							RawCdmData rawCdmDatum = new RawCdmData(Startup.dbInventoryImport, "IMPORT.EXE", 0)
							{
								JobExecutionRawId = this.Raw.JobExecutionRawId,
								DealerID = file[0],
								VIN = file[1],
								StockID = file[2],
								Mileage = int.Parse((file[3] == "" ? "0" : file[3])),
								Year = int.Parse((file[4] == "" ? "0" : file[4])),
								Make = file[5],
								Model = file[6],
								Body = file[7],
								Transmission = file[8],
								Engine = file[9],
								DriveTrain = file[10],
								IntColor = file[11],
								ExtColor = file[12],
								WebPrice = float.Parse((file[13] == "" ? "0" : file[13])),
								AskingPrice = float.Parse((file[14] == "" ? "0" : file[14])),
								CurrentCostPrice = float.Parse((file[15] == "" ? "0" : file[15])),
								Certified = file[16],
								IsUsed = file[17],
								BookInDate = file[18],
								Comments = file[19],
								Features = file[20],
								ImageURLs = file[21]
							};
							cdmdata = rawCdmDatum;
							query = string.Concat(query, cdmdata.SaveQuery(), "\n");
							if (this.Raw.NoOfRecords % this.Interval == 0)
							{
								batch.ExecuteStatement(query);
								query = "";
							}
							Status status = Startup.Status;
							object[] upper = new object[] { "CDMDATA - ", cdmdata.VIN.ToUpper(), " - ", cdmdata.Year, " ", cdmdata.Make, " ", cdmdata.Model };
							status.UpdateStatus(string.Concat(upper));
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							if (this.Raw.NoOfRecords % 100 == 0)
							{
								Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
							}
							GC.Collect();
						}
					}
					catch (Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}