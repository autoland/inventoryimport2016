using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Advocar.Tools;
using Import;
using System;
using System.Diagnostics;
using System.IO;

namespace Import.Execute.Raw
{
	public class DealerFusion : JobRaw
	{
		public DealerFusion()
		{
		}

		public override void Execute()
		{
			string source = "http://content.dealerfusion.com/gen_feed/502/data_feed.csv";
			string destination = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\DealerFusion\\data_feed.csv");
			API.URLDownloadToFile(source, destination);
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\DealerFusion\\");
			this.SourceFile = "data_feed.csv";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("DealerFusion", LookupType.DataProvider, Startup.dbAime);
			string winzip = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "\\WinZip\\wzunzip.exe");
			string imageSource = "http://content.dealerfusion.com/gen_feed/502/images.zip";
			string imageDestination = "D:\\Inventory\\images.zip";
			API.URLDownloadToFile(imageSource, imageDestination);
			bool winzipExists = true;
			if (!File.Exists(winzip))
			{
				winzip = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), " (x86)\\WinZip\\wzunzip.exe");
				if (!File.Exists(winzip))
				{
					winzipExists = false;
				}
			}
			if (!File.Exists(imageDestination))
			{
				winzipExists = false;
			}
			if (winzipExists)
			{
				Process process = new Process();
				process.StartInfo.FileName = winzip;
				process.StartInfo.Arguments = string.Concat(imageDestination, " \\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\DealerFusion\\Images\\ -o");
				process.Start();
				while (!process.HasExited)
				{
					winzipExists = true;
				}
			}
			if (base.Start())
			{
				this.importDealerFusion();
				base.Complete();
				source = "D:\\Inventory\\images.zip";
				string directory = this.Directory;
				DateTime now = DateTime.Now;
				destination = string.Concat(directory, "Archive\\images_", now.ToString("yyyyMMdd_mmhhss"), ".zip");
				if (File.Exists(source))
				{
					File.Copy(source, destination);
					File.Delete(source);
				}
			}
		}

		private void importDealerFusion()
		{
			System.Exception err;
			string fileName = string.Concat(this.Directory, this.SourceFile);
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				DataFlatFile file = new DataFlatFile(fileName, ",", true);
				RawDealerFusion dealerfusion = new RawDealerFusion();
				file.MoveNext();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						if (!base.IsDealerPresent(file[1]))
						{
							file.MoveNext();
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							continue;
						}
						else
						{
							JobExecutionRaw noOfRecords = this.Raw;
							noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
							RawDealerFusion rawDealerFusion = new RawDealerFusion(Startup.dbInventoryImport, "IMPORT.EXE", 0)
							{
								JobExecutionRawId = this.Raw.JobExecutionRawId,
								DealerName = file[0],
								DealerID = file[1],
								Year = int.Parse((file[2] == "" ? "0000" : file[2])),
								Make = file[3],
								Model = file[4],
								VIN = file[5],
								StockID = file[6],
								Engine = file[7],
								Transmission = file[8],
								Description = file[9],
								Mileage = int.Parse((file[10] == "" ? "0" : file[10])),
								Price = float.Parse((file[11] == "" ? "0" : file[11])),
								ExtColor = file[12],
								DealerAddress = file[13],
								DealerCity = file[14],
								DealerState = file[15],
								DealerZip = file[16],
								DealerPhone = file[17],
								EmailLeadsTo = file[18],
								Equipment = file[20],
								DealerMessage = file[21],
								CertifiedYesNo = file[22],
								HighOctane = file[23],
								HighOctane360 = file[24],
								HighOctaneMultiPhotos = file[25],
								RetailPrice = float.Parse((file[26] == "" ? "0" : file[26])),
								DealerBlurb = file[27]
							};
							dealerfusion = rawDealerFusion;
							string images = string.Concat("http://www.advocar.com/MediaCom/InventoryImport/DealerFusion/Images/", dealerfusion.StockID, ".jpg,");
							if (dealerfusion.DealerID == "GWL")
							{
								images = "";
							}
							images = string.Concat(images, "http://www.advocar.com/MediaCom/InventoryImport/DealerFusion/Images/", dealerfusion.StockID, "__A.jpg,");
							images = string.Concat(images, "http://www.advocar.com/MediaCom/InventoryImport/DealerFusion/Images/", dealerfusion.StockID, "__B.jpg,");
							images = string.Concat(images, "http://www.advocar.com/MediaCom/InventoryImport/DealerFusion/Images/", dealerfusion.StockID, "__C.jpg,");
							images = string.Concat(images, "http://www.advocar.com/MediaCom/InventoryImport/DealerFusion/Images/", dealerfusion.StockID, "__D.jpg,");
							images = string.Concat(images, "http://www.advocar.com/MediaCom/InventoryImport/DealerFusion/Images/", dealerfusion.StockID, "__E.jpg,");
							images = string.Concat(images, "http://www.advocar.com/MediaCom/InventoryImport/DealerFusion/Images/", dealerfusion.StockID, "__F.jpg,");
							dealerfusion.PhotoUrl = images;
							query = string.Concat(query, dealerfusion.SaveQuery(), "\n");
							if (this.Raw.NoOfRecords % this.Interval == 0)
							{
								batch.ExecuteStatement(query);
								query = "";
								Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
							}
							Status status = Startup.Status;
							object[] upper = new object[] { "DEALER FUSION - ", dealerfusion.VIN.ToUpper(), " - ", dealerfusion.Year, " ", dealerfusion.Make, " ", dealerfusion.Model };
							status.UpdateStatus(string.Concat(upper));
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							GC.Collect();
						}
					}
					catch (System.Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (System.Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}