using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;

namespace Import.Execute.Raw
{
	public class BeavertonSaturn : JobRaw
	{
		public BeavertonSaturn()
		{
		}

		public override void Execute()
		{
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\BeavertonSaturn\\");
			this.SourceFile = "Saturn_Of_Beaverton.asc";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("BeavertonSaturn", LookupType.DataProvider, Startup.dbAime);
			if (base.Start())
			{
				this.importBeavertonSaturn();
				base.Complete();
			}
		}

		private void importBeavertonSaturn()
		{
			Exception err;
			string fileName = string.Concat(this.Directory, "\\Saturn_Of_Beaverton.asc");
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				DataFlatFile file = new DataFlatFile(fileName, "\t", true);
				RawBeavertonSaturn beaversat = new RawBeavertonSaturn();
				file.MoveNext();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecords = raw.NoOfRecords + 1;
						JobExecutionRaw noOfRecordsInFile = this.Raw;
						noOfRecordsInFile.NoOfRecordsInFile = noOfRecordsInFile.NoOfRecordsInFile + 1;
						this.Raw.NoOfDealers = 1;
						RawBeavertonSaturn rawBeavertonSaturn = new RawBeavertonSaturn(Startup.dbInventoryImport, "IMPORT.EXE", 0)
						{
							JobExecutionRawId = this.Raw.JobExecutionRawId,
							DealerID = "BEAVSAT",
							NewUsed = file[0],
							StockID = file[1],
							VehicleDescription = file[2],
							Year = int.Parse((file[3].Trim() == "" ? "0" : file[3])),
							Make = file[4],
							Model = file[5],
							Trim = file[6],
							VIN = file[7],
							Style = file[8],
							Engine = file[9],
							Transmission = file[10],
							Options = file[11],
							Mileage = int.Parse((file[12].Trim() == "" ? "0" : file[12])),
							SellingPrice = float.Parse((file[13].Trim() == "" ? "0" : file[13])),
							RetailPrice = float.Parse((file[14].Trim() == "" ? "0" : file[14])),
							ExtColor = file[15],
							ImageUrls = file[16]
						};
						beaversat = rawBeavertonSaturn;
						query = string.Concat(query, beaversat.SaveQuery(), "\n");
						if (this.Raw.NoOfRecords % this.Interval == 0)
						{
							batch.ExecuteStatement(query);
							query = "";
						}
						Status status = Startup.Status;
						object[] upper = new object[] { "BEAVERTON SATURN - ", beaversat.VIN.ToUpper(), " - ", beaversat.Year, " ", beaversat.Make, " ", beaversat.Model };
						status.UpdateStatus(string.Concat(upper));
						Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
						if (this.Raw.NoOfRecords % 100 == 0)
						{
							Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
						}
						GC.Collect();
					}
					catch (Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}