using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;

namespace Import.Execute.Raw
{
	public class DealerCom : JobRaw
	{
		public DealerCom()
		{
		}

		public override void Execute()
		{
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\DealerCom\\");
			this.SourceFile = "dealerdotcom.csv";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("DealerCom", LookupType.DataProvider, Startup.dbAime);
			if (base.Start())
			{
				this.importVehicles();
				base.Complete();
			}
		}

		private void importVehicles()
		{
			Exception err;
			string fileName = string.Concat(this.Directory, "\\dealerdotcom.csv");
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport)
				{
					Timeout = 600
				};
				DataFlatFile file = new DataFlatFile(fileName, ",", true);
				RawDealerCom dealercom = new RawDealerCom();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						if (!base.IsDealerPresent(file[0]))
						{
							file.MoveNext();
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							continue;
						}
						else
						{
							JobExecutionRaw noOfRecords = this.Raw;
							noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
							RawDealerCom rawDealerCom = new RawDealerCom(Startup.dbInventoryImport, "IMPORT.EXE", 0)
							{
								JobExecutionRawId = this.Raw.JobExecutionRawId,
								DealerId = file[0],
								Vin = file[1],
								StockNo = file[2],
								Year = file[3],
								Make = file[4],
								Model = file[5],
								ModelCode = file[6],
								Trim = file[7],
								BodyStyle = file[8],
								Transmission = file[9],
								Drivetrain = file[10],
								Doors = file[11],
								ExteriorColor = file[12],
								InteriorColor = file[13],
								TruckCab = file[14],
								TruckBed = file[15]
							};
							dealercom = rawDealerCom;
							try
							{
								dealercom.Msrp = float.Parse(file[16]);
							}
							catch
							{
								dealercom.Msrp = 0f;
							}
							try
							{
								dealercom.Invoice = float.Parse(file[17]);
							}
							catch
							{
								dealercom.Invoice = 0f;
							}
							try
							{
								dealercom.Price = float.Parse(file[18]);
							}
							catch
							{
								dealercom.Price = 0f;
							}
							try
							{
								dealercom.WholesalePrice = float.Parse(file[19]);
							}
							catch
							{
								dealercom.WholesalePrice = 0f;
							}
							try
							{
								dealercom.RetailPrice = float.Parse(file[20]);
							}
							catch
							{
								dealercom.RetailPrice = 0f;
							}
							try
							{
								dealercom.SalesPrice = float.Parse(file[21]);
							}
							catch
							{
								dealercom.SalesPrice = 0f;
							}
							try
							{
								dealercom.Mileage = int.Parse(file[22]);
							}
							catch
							{
								dealercom.Mileage = 0;
							}
							dealercom.Comments = file[23];
							dealercom.Options = file[24];
							dealercom.Images = file[25];
							dealercom.Type = file[26];
							dealercom.Certified = file[27];
							dealercom.Engine = file[28];
							dealercom.EngineType = file[29];
							dealercom.Fuel = file[30];
							dealercom.LiveDate = file[31];
							dealercom.Dealership = file[32];
							dealercom.DealershipAddress = file[33];
							dealercom.DealershipAddress2 = file[34];
							dealercom.DealershipCity = file[35];
							dealercom.DealershipState = file[36];
							dealercom.DealershipPostalCode = file[37];
							dealercom.DealershipCountry = file[38];
							dealercom.DealershipPhone = file[39];
							dealercom.DealershipUrl = file[40];
							query = string.Concat(query, dealercom.SaveQuery(), "\n");
							if (this.Raw.NoOfRecords % (this.Interval * 5) == 0)
							{
								batch.ExecuteStatement(query);
								query = "";
							}
							Status status = Startup.Status;
							string[] upper = new string[] { "DEALER COM - ", dealercom.Vin.ToUpper(), " - ", dealercom.Year, " ", dealercom.Make, " ", dealercom.Model, " ", dealercom.Trim };
							status.UpdateStatus(string.Concat(upper));
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							if (this.Raw.NoOfRecords % 100 == 0)
							{
								Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
							}
							GC.Collect();
						}
					}
					catch (Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}