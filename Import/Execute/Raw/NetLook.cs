using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;
using System.Diagnostics;
using System.IO;

namespace Import.Execute.Raw
{
	public class NetLook : JobRaw
	{
		public NetLook()
		{
		}

		public override void Execute()
		{
			string winzip = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "\\WinZip\\wzunzip.exe");
			string netLook = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\NetLook\\netlook.zip");
			bool winzipExists = true;
			if (!File.Exists(winzip))
			{
				winzip = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), " (x86)\\WinZip\\wzunzip.exe");
				if (!File.Exists(winzip))
				{
					winzipExists = false;
				}
			}
			if (!File.Exists(netLook))
			{
				winzipExists = false;
			}
			if (winzipExists)
			{
				Process process = new Process();
				process.StartInfo.FileName = winzip;
				process.StartInfo.Arguments = string.Concat(netLook, " \\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\NetLook\\ -o");
				process.Start();
				while (!process.HasExited)
				{
					winzipExists = true;
				}
			}
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\NetLook\\");
			this.SourceFile = "netlook.zip";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("NetLook", LookupType.DataProvider, Startup.dbAime);
			if (base.Start())
			{
				this.importVehicles();
				string source = netLook;
				source = string.Concat(this.Directory, "netlook.txt");
				if (File.Exists(source))
				{
					File.Delete(source);
				}
				base.Complete();
			}
		}

		private void importVehicles()
		{
			Exception err;
			string fileName = string.Concat(this.Directory, "netlook.txt");
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport)
				{
					Timeout = 600
				};
				DataFlatFile file = new DataFlatFile(fileName, ",", true);
				RawNetLook netlook = new RawNetLook();
				file.MoveNext();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						if (!base.IsDealerPresent(file[0]))
						{
							file.MoveNext();
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							continue;
						}
						else
						{
							JobExecutionRaw noOfRecords = this.Raw;
							noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
							RawNetLook rawNetLook = new RawNetLook(Startup.dbInventoryImport, "IMPORT.EXE", 0)
							{
								JobExecutionRawId = this.Raw.JobExecutionRawId,
								DealerId = file[0],
								StockNo = file[1],
								NewUsed = file[2],
								VehicleYear = file[3],
								VehicleMake = file[4],
								VehicleModel = file[5],
								Series = file[6],
								Vin = file[7]
							};
							netlook = rawNetLook;
							try
							{
								netlook.Mileage = int.Parse(file[8]);
							}
							catch
							{
								netlook.Mileage = 0;
							}
							try
							{
								netlook.Price = float.Parse(file[9]);
							}
							catch
							{
								netlook.Price = 0f;
							}
							netlook.ExteriorColor = file[10];
							netlook.InteriorColor = file[11];
							netlook.Engine = file[12];
							netlook.Transmission = file[13];
							netlook.Features = file[14];
							netlook.ImageLinks = file[15];
							netlook.SellersNotes = file[16];
							query = string.Concat(query, netlook.SaveQuery(), "\n");
							if (this.Raw.NoOfRecords % (this.Interval * 5) == 0)
							{
								batch.ExecuteStatement(query);
								query = "";
							}
							Status status = Startup.Status;
							string[] upper = new string[] { "NET LOOK - ", netlook.Vin.ToUpper(), " - ", netlook.VehicleYear, " ", netlook.VehicleMake, " ", netlook.VehicleModel };
							status.UpdateStatus(string.Concat(upper));
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							if (this.Raw.NoOfRecords % 100 == 0)
							{
								Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
							}
							GC.Collect();
						}
					}
					catch (Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}