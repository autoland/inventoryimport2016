using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;
using System.IO;

namespace Import.Execute.Raw
{
	public class JobRaw : Job
	{
		public JobExecutionRaw Raw = new JobExecutionRaw(Startup.dbInventoryImport, "IMPORT.EXE", 0);

		public string Directory = "";

		public string SourceFile = "";

		public int Interval = 20;

		public string[] DealerIds = new string[0];

		public JobRaw()
		{
		}

		public void Complete()
		{
			this.Raw.EndedOn = DateTime.Now.ToString();
			this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("Success", LookupType.JobExecutionStatus, Startup.dbAime);
			string source = string.Concat(this.Directory, this.SourceFile);
			string[] strArrays = new string[] { this.Directory, "Archive\\", null, null, null };
			string[] directory = strArrays;
			DateTime now = DateTime.Now;
			directory[2] = now.ToString("yyyyMMdd_mmhhss");
			directory[3] = "_";
			directory[4] = this.SourceFile;
			string destination = string.Concat(directory);
			if (File.Exists(source))
			{
				File.Copy(source, destination);
				File.Delete(source);
			}
			this.Raw.ArchiveFileName = destination;
			this.Raw.NoOfDealers = (this.Raw.NoOfDealers == 0 ? (int)this.DealerIds.Length : this.Raw.NoOfDealers);
			this.Raw.Save();
			Status status = Startup.Status;
			now = DateTime.Now;
			status.UpdateStatus(string.Concat("COMPLETED PROCESSING - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
			Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
			this.Raw = new JobExecutionRaw(Startup.dbInventoryImport, "IMPORT.EXE", 0);
		}

		public override void Execute()
		{
		}

		public bool IsDealerPresent(string DealerId)
		{
			bool flag;
			int index = 0;
			while (true)
			{
				if ((int)this.DealerIds.Length <= index)
				{
					flag = false;
					break;
				}
				else if (this.DealerIds[index] == DealerId)
				{
					flag = true;
					break;
				}
				else
				{
					index++;
				}
			}
			return flag;
		}

		public bool Start()
		{
			bool flag;
			if (!File.Exists(string.Concat(this.Directory, this.SourceFile)))
			{
				flag = false;
			}
			else
			{
				JobExecutionRaw raw = this.Raw;
				Status status = Startup.Status;
				string str = string.Concat(this.Directory, "Logs\\");
				DateTime now = DateTime.Now;
				raw.LogFileName = status.Open(str, string.Concat(now.ToString("yyyyMMddhhmmss"), ".log"));
				Status statu = Startup.Status;
				now = DateTime.Now;
				statu.UpdateStatus(string.Concat("BEGIN PROCESSING - ", now.ToString("MM/dd/yyyy hh:mm:ss")));
				DataAccess data = new DataAccess(Startup.dbAime);
				int lookupIdDataProvider = this.Raw.LookupId_DataProvider;
				data.AddParam("@LookupId_DataProvider", DataAccessParameterType.Numeric, lookupIdDataProvider.ToString());
				data.ExecuteProcedure("DEALER_GetListImport");
				this.DealerIds = new string[data.RecordCount];
				int index = -1;
				while (!data.EOF)
				{
					index++;
					this.DealerIds[index] = data["DataProviderCode"];
					data.MoveNext();
				}
				this.Raw.StartedOn = DateTime.Now.ToString();
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("New", LookupType.JobExecutionStatus, Startup.dbAime);
				this.Raw.Save();
				flag = true;
			}
			return flag;
		}
	}
}