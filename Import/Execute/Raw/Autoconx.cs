using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;

namespace Import.Execute.Raw
{
	public class Autoconx : JobRaw
	{
		public Autoconx()
		{
		}

		public override void Execute()
		{
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\Autoconx\\");
			this.SourceFile = "autoland.txt";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("Autoconx", LookupType.DataProvider, Startup.dbAime);
			if (base.Start())
			{
				this.importAutoconx();
				base.Complete();
			}
		}

		private void importAutoconx()
		{
			Exception err;
			string fileName = string.Concat(this.Directory, this.SourceFile);
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				DataFlatFile file = new DataFlatFile(fileName, "\t", true);
				RawAutoconx autoconx = new RawAutoconx();
				file.MoveNext();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						if (!base.IsDealerPresent(file[0]))
						{
							file.MoveNext();
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							continue;
						}
						else
						{
							JobExecutionRaw noOfRecords = this.Raw;
							noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
							RawAutoconx rawAutoconx = new RawAutoconx(Startup.dbInventoryImport, "IMPORT.EXE", 0)
							{
								JobExecutionRawId = this.Raw.JobExecutionRawId,
								DealerId = file[0],
								StockNumber = file[1],
								BodyStyle = file[2],
								Vin = file[3]
							};
							autoconx = rawAutoconx;
							try
							{
								autoconx.VehicleYear = int.Parse((file[4].Trim() == "" ? "0" : file[4]));
							}
							catch
							{
							}
							autoconx.Make = file[5];
							autoconx.Model = file[6];
							autoconx.Trim = file[7];
							try
							{
								autoconx.Miles = int.Parse((file[8].Trim() == "" ? "0" : file[8]));
							}
							catch
							{
							}
							autoconx.Transmission = file[9];
							autoconx.Engine = file[10];
							autoconx.Drivetrain = file[11];
							try
							{
								autoconx.Cylinders = int.Parse((file[12].Trim() == "" ? "0" : file[12]));
							}
							catch
							{
							}
							autoconx.FuelType = file[13];
							try
							{
								autoconx.Price = float.Parse((file[14].Trim() == "" ? "0" : file[14]));
							}
							catch
							{
							}
							try
							{
								autoconx.PriceInvoice = float.Parse((file[15].Trim() == "" ? "0" : file[15]));
							}
							catch
							{
							}
							try
							{
								autoconx.PriceMsrp = float.Parse((file[16].Trim() == "" ? "0" : file[16]));
							}
							catch
							{
							}
							try
							{
								autoconx.PriceRebate = float.Parse((file[17].Trim() == "" ? "0" : file[17]));
							}
							catch
							{
							}
							autoconx.ExteriorColor = file[18];
							autoconx.InteriorColor = file[19];
							autoconx.Certification = file[20];
							autoconx.Options = file[21];
							autoconx.Description = file[22];
							try
							{
								autoconx.IsNew = bool.Parse((file[23].ToLower() == "new" ? "1" : "0"));
							}
							catch
							{
							}
							autoconx.PhotoUrl = file[24];
							query = string.Concat(query, autoconx.SaveQuery(), "\n");
							if (this.Raw.NoOfRecords % this.Interval == 0)
							{
								batch.ExecuteStatement(query);
								query = "";
							}
							Status status = Startup.Status;
							object[] upper = new object[] { "AUTOCONX - ", autoconx.Vin.ToUpper(), " - ", autoconx.VehicleYear, " ", autoconx.Make, " ", autoconx.Model };
							status.UpdateStatus(string.Concat(upper));
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							if (this.Raw.NoOfRecords % 100 == 0)
							{
								Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
							}
							GC.Collect();
						}
					}
					catch (Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}