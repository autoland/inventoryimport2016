using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;

namespace Import.Execute.Raw
{
	public class LiquidMotors : JobRaw
	{
		public LiquidMotors()
		{
		}

		public override void Execute()
		{
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\LiquidMotors\\");
			this.SourceFile = "3077.txt";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("LiquidMotors", LookupType.DataProvider, Startup.dbAime);
			if (base.Start())
			{
				this.import();
				base.Complete();
			}
		}

		private void import()
		{
			Exception err;
			string fileName = string.Concat(this.Directory, this.SourceFile);
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				DataFlatFile file = new DataFlatFile(fileName, "|", true);
				RawLiquidMotors vehicle = new RawLiquidMotors();
				file.MoveNext();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						if (!base.IsDealerPresent(file[0]))
						{
							file.MoveNext();
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							continue;
						}
						else
						{
							JobExecutionRaw noOfRecords = this.Raw;
							noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
							RawLiquidMotors rawLiquidMotor = new RawLiquidMotors(Startup.dbInventoryImport, "IMPORT.EXE", 0)
							{
								JobExecutionRawId = this.Raw.JobExecutionRawId,
								DealerId = file[0],
								DealerStockNo = file[1]
							};
							vehicle = rawLiquidMotor;
							try
							{
								vehicle.Year = int.Parse(file[2]);
							}
							catch
							{
								vehicle.Year = 0;
							}
							vehicle.Make = file[3];
							vehicle.Model = file[4];
							vehicle.Trim = file[5];
							vehicle.Vin = file[6];
							try
							{
								vehicle.Mileage = int.Parse(file[7]);
							}
							catch
							{
								vehicle.Mileage = 0;
							}
							try
							{
								vehicle.Price = float.Parse(file[8]);
							}
							catch
							{
								vehicle.Price = 0f;
							}
							try
							{
								vehicle.Msrp = float.Parse(file[9]);
							}
							catch
							{
								vehicle.Msrp = 0f;
							}
							vehicle.ExteriorColor = file[10];
							vehicle.InteriorColor = file[11];
							vehicle.Transmission = file[12];
							vehicle.Image = file[13];
							vehicle.Description = file[14];
							vehicle.Notes = file[15];
							vehicle.BodyType = file[16];
							vehicle.EngineType = file[17];
							vehicle.DriveType = file[18];
							vehicle.FuelType = file[19];
							vehicle.NewUsedCertified = file[20];
							try
							{
								vehicle.InternetPrice = float.Parse(file[21]);
							}
							catch
							{
								vehicle.InternetPrice = 0f;
							}
							query = string.Concat(query, vehicle.SaveQuery(), "\n");
							if (this.Raw.NoOfRecords % this.Interval == 0)
							{
								batch.ExecuteStatement(query);
								query = "";
							}
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							if (this.Raw.NoOfRecords % 100 == 0)
							{
								Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
							}
							GC.Collect();
						}
					}
					catch (Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}