using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;
using System.IO;

namespace Import.Execute.Raw
{
	public class Karpower : JobRaw
	{
		public Karpower()
		{
		}

		public override void Execute()
		{
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\Karpower\\");
			DirectoryInfo[] directories = (new DirectoryInfo(this.Directory)).GetDirectories();
			for (int i = 0; i < (int)directories.Length; i++)
			{
				DirectoryInfo directory = directories[i];
				string[] media = new string[] { "\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\Karpower\\", directory.Name, "\\" };
				this.Directory = string.Concat(media);
				this.SourceFile = string.Concat(directory.Name, "_Karpower.asc");
				this.Raw.LookupId_DataProvider = Lookup.GetRecordId("Karpower", LookupType.DataProvider, Startup.dbAime);
				if (base.Start() && !((new DataFlatFile(string.Concat(this.Directory, this.SourceFile), "\t", false))[0] != "35"))
				{
					this.importKarpower(string.Concat(this.Directory, this.SourceFile), directory.Name);
					base.Complete();
				}
			}
		}

		private void importKarpower(string filename, string dirname)
		{
			int optionId;
			Exception err;
			string optionList;
			string query = "";
			try
			{
				DataFlatFile file = new DataFlatFile(filename, "\t", false);
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				RawKbbKarpower karpower = new RawKbbKarpower();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecords = raw.NoOfRecords + 1;
						JobExecutionRaw noOfRecordsInFile = this.Raw;
						noOfRecordsInFile.NoOfRecordsInFile = noOfRecordsInFile.NoOfRecordsInFile + 1;
						this.Raw.NoOfDealers = 1;
						karpower = new RawKbbKarpower(Startup.dbInventoryImport, "IMPORT.EXE", 0);
						string[] row = file.Row;
						karpower.JobExecutionRawId = this.Raw.JobExecutionRawId;
						karpower.DealerID = dirname;
						karpower.StockNo = file[1];
						karpower.DateStored = file[2];
						try
						{
							karpower.Year = int.Parse(file[3]);
						}
						catch
						{
							karpower.Year = 0;
						}
						karpower.Make = file[4];
						karpower.Model = file[5];
						karpower.Engine = file[6];
						karpower.Transmission = file[7];
						karpower.DriveTrain = file[8];
						karpower.Color = file[9];
						try
						{
							karpower.Mileage = int.Parse(file[10]);
						}
						catch
						{
							karpower.Mileage = 0;
						}
						karpower.VIN = file[11];
						karpower.License = file[12];
						karpower.PriceRecon = float.Parse((file[13] == "" ? "0" : file[13]));
						karpower.PriceSelling = float.Parse((file[14] == "" ? "0" : file[14]));
						karpower.PriceCost = float.Parse((file[15] == "" ? "0" : file[15]));
						karpower.DateBook = file[16];
						karpower.Classic = file[18];
						karpower.BaseWholesale = float.Parse((file[19] == "" ? "0" : file[19]));
						karpower.BaseRetail = float.Parse((file[20] == "" ? "0" : file[20]));
						karpower.EngineWholesale = float.Parse((file[21] == "" ? "0" : file[21]));
						karpower.EngineRetail = float.Parse((file[22] == "" ? "0" : file[22]));
						karpower.TransWholesale = float.Parse((file[23] == "" ? "0" : file[23]));
						karpower.TransRetail = float.Parse((file[24] == "" ? "0" : file[24]));
						karpower.DriveWholesale = float.Parse((file[25] == "" ? "0" : file[25]));
						karpower.DriveRetail = float.Parse((file[26] == "" ? "0" : file[26]));
						karpower.MileAdjust = float.Parse((file[27] == "" ? "0" : file[27]));
						karpower.NoMilesWholesale = float.Parse((file[28] == "" ? "0" : file[28]));
						karpower.NoMilesRetail = float.Parse((file[29] == "" ? "0" : file[29]));
						karpower.MaxDeduct = float.Parse((file[30] == "" ? "0" : file[30]));
						karpower.FullWholesale = float.Parse((file[33] == "" ? "0" : file[33]));
						karpower.FullRetail = float.Parse((file[34] == "" ? "0" : file[34]));
						karpower.DateUpdated = file[35];
						for (int fieldNo = 36; fieldNo < file.FieldCount; fieldNo++)
						{
							string lower = file[fieldNo].ToLower();
							if (lower != null)
							{
								if (lower == "*equip*")
								{
									fieldNo = fieldNo + 2;
									int noOfOptions = 0;
									try
									{
										noOfOptions = int.Parse(file[fieldNo]);
									}
									catch
									{
										noOfOptions = 0;
									}
									for (optionId = 1; noOfOptions >= optionId; optionId++)
									{
										fieldNo++;
										karpower.OptionList = string.Concat(karpower.OptionList, file[fieldNo], ",");
										fieldNo++;
										fieldNo++;
									}
								}
								else if (lower == "*msg std*")
								{
									fieldNo = fieldNo + 2;
									int noOfStandard = 0;
									try
									{
										noOfStandard = int.Parse(file[fieldNo]);
									}
									catch
									{
										noOfStandard = 0;
									}
									for (optionId = 1; noOfStandard >= optionId; optionId++)
									{
										fieldNo++;
										karpower.OptionList = string.Concat(karpower.OptionList, file[fieldNo], ",");
										fieldNo++;
										fieldNo++;
									}
								}
							}
						}
						string images = string.Concat("http://www.advocar.com/MediaCom/Inventory/", karpower.VIN, ".jpg");
						for (int imageNo = 2; imageNo <= 6; imageNo++)
						{
							string[] vIN = new string[] { images, ",http://www.advocar.com/MediaCom/Inventory/", karpower.VIN, "(", imageNo.ToString(), ").jpg" };
							images = string.Concat(vIN);
						}
						karpower.ImageUrlList = images;
						RawKbbKarpower rawKbbKarpower = karpower;
						if (karpower.OptionList.Length <= 0)
						{
							optionList = karpower.OptionList;
						}
						else
						{
							string str = karpower.OptionList.Substring(0, karpower.OptionList.Length - 1);
							string str1 = str;
							karpower.OptionList = str;
							optionList = str1;
						}
						rawKbbKarpower.OptionList = optionList;
						query = string.Concat(query, karpower.SaveQuery(), "\n");
						if (this.Raw.NoOfRecords % this.Interval == 0)
						{
							batch.ExecuteStatement(query);
							query = "";
						}
						Status status = Startup.Status;
						object[] objArray = new object[] { "KARPOWER", dirname, " - ", karpower.VIN.ToUpper(), " - ", karpower.Year, " ", karpower.Make, " ", karpower.Model };
						status.UpdateStatus(string.Concat(objArray));
						Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
						if (this.Raw.NoOfRecords % 100 == 0)
						{
							Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
						}
						GC.Collect();
					}
					catch (Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}