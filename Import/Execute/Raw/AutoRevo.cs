using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;

namespace Import.Execute.Raw
{
	public class AutoRevo : JobRaw
	{
		public AutoRevo()
		{
		}

		private void AutoRevoImport()
		{
			Exception err;
			string fileName = string.Concat(this.Directory, this.SourceFile);
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				DataFlatFile file = new DataFlatFile(fileName, "|", true);
				RawAutoRevo autoRevovehicle = new RawAutoRevo();
				file.MoveNext();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						if (!base.IsDealerPresent(file[0]))
						{
							file.MoveNext();
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							continue;
						}
						else
						{
							JobExecutionRaw noOfRecords = this.Raw;
							noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
							RawAutoRevo rawAutoRevo = new RawAutoRevo(Startup.dbInventoryImport, "IMPORT.EXE", 0)
							{
								JobExecutionRawId = this.Raw.JobExecutionRawId,
								DealerId = file[0],
								Vin = file[1],
								StockNumber = file[2],
								Year = int.Parse((file[3] == "" ? "0" : file[3])),
								Make = file[4],
								Model = file[5],
								Trim = file[6],
								Body = file[7],
								Mileage = int.Parse((file[8] == "" ? "0" : file[8])),
								WebPrice = float.Parse((file[9] == "" ? "0" : file[9])),
								RetailPrice = float.Parse((file[10] == "" ? "0" : file[10])),
								Certified = file[11],
								ExteriorColor = file[12],
								InteriorColor = file[13],
								Description = file[14],
								Optiones = file[15],
								ImageUrl = file[16],
								InteriorType = file[17],
								NumberOfCylinders = int.Parse(file[18]),
								DriveTrain = file[19],
								NumberOfDoors = int.Parse(file[20]),
								Transmission = file[21],
								NewUsed = file[22]
							};
							query = string.Concat(query, rawAutoRevo.SaveQuery(), "\n");
							if (this.Raw.NoOfRecords % this.Interval == 0)
							{
								batch.ExecuteStatement(query);
								query = "";
							}
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							if (this.Raw.NoOfRecords % 100 == 0)
							{
								Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
							}
							GC.Collect();
						}
					}
					catch (Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}

		public override void Execute()
		{
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\AutoRevo\\");
			this.SourceFile = "AR2636.txt";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("AutoRevo", LookupType.DataProvider, Startup.dbAime);
			if (base.Start())
			{
				this.AutoRevoImport();
				base.Complete();
			}
		}
	}
}