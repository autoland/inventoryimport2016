﻿using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;

namespace Import.Execute.Raw
{
    public class DealerSync : JobRaw
    {
        public DealerSync()
        {
        }

        public override void Execute()
        {
            this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\dealersync\\");
            this.SourceFile = "dealersync-autoland.txt";
            this.Raw.LookupId_DataProvider = Lookup.GetRecordId("DealerSync", LookupType.DataProvider, Startup.dbAime);
            if (base.Start())
            {
                this.ImportSync();
                base.Complete();
            }
        }

        private void ImportSync()
        {
            Exception err;
            string fileName = string.Concat(this.Directory, this.SourceFile);
            string query = "";
            try
            {
                DataAccess batch = new DataAccess(Startup.dbInventoryImport);
                DataFlatFile file = new DataFlatFile(fileName, ",", true);
                RawDealerSync vehicle = new RawDealerSync();
                file.MoveNext();
                while (!file.EOF)
                {
                    try
                    {
                        JobExecutionRaw raw = this.Raw;
                        raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
                        if (base.IsDealerPresent(file[0]))
                        {
                            JobExecutionRaw noOfRecords = this.Raw;
                            noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
                            vehicle = new RawDealerSync(Startup.dbInventoryImport, "IMPORT.EXE", 0)
                            {
                                JobExecutionRawId = this.Raw.JobExecutionRawId,
                                AutolandDealerID = file[0],
                                NewUsed = file[1],
                                VIN = file[2],
                                StockNumber = file[3],
                                Make = file[4],
                                Model = file[5],
                                ModelYear = int.Parse(file[6] == "" ? "0" : file[6]),
                                TrimPackage = file[7],
                                BodyStyle = file[8],
                                Miles = int.Parse(file[9] == "" ? "0" : file[9]),
                                Engine = file[10],
                                Cylinders = file[11],
                                FuelType = file[12],
                                Transmission = file[13],
                                Price = float.Parse((file[14] == "" ? "0" : file[14])),
                                ExteriorColor = file[15],
                                InteriorColor = file[16],
                                Options = file[17],
                                Description = file[18],
                                PhotoURL = file[19]

                            };
                            query = string.Concat(query, vehicle.SaveQuery(), "\n");
                            if (this.Raw.NoOfRecords % this.Interval == 0)
                            {
                                batch.ExecuteStatement(query);
                                query = "";
                            }
                            Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
                            if (this.Raw.NoOfRecords % 100 == 0)
                            {
                                Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
                            }
                            GC.Collect();
                        }
                        else
                        {
                            file.MoveNext();
                            Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
                            continue;
                        }
                    }
                    catch (Exception exception)
                    {
                        err = exception;
                        JobExecutionRaw noOfRowErrors = this.Raw;
                        noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
                        Startup.Status.WriteToFile(this.Raw.LogFileName, err);
                    }
                    file.MoveNext();
                }
                if (query.Length > 0)
                {
                    batch.ExecuteStatement(query);
                }
            }
            catch (Exception exception1)
            {
                err = exception1;
                this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
                Startup.Status.WriteToFile(this.Raw.LogFileName, err);
            }
        }
    }
}