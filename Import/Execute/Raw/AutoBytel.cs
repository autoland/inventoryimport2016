using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Advocar.Tools;
using Import;
using System;
using System.Diagnostics;
using System.IO;

namespace Import.Execute.Raw
{
	public class AutoBytel : JobRaw
	{
		public AutoBytel()
		{
		}

		public override void Execute()
		{
			DateTime dateTime = DateTime.Now.AddDays(-1);
			string source = string.Concat("ftp://DataFeed:UC13158D2@ucfeeds.services.autobytel.com/DataFeed/option04/Full/ABT_ALL4_", dateTime.ToString("yyyyMMdd"), ".zip");
			string destination = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\AutoBytel\\autobytel.zip");
			API.URLDownloadToFile(source, destination);
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\AutoBytel\\");
			this.SourceFile = "autobytel.csv";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("AutoBytel", LookupType.DataProvider, Startup.dbAime);
			string winzip = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "\\WinZip\\wzunzip.exe");
			bool winzipExists = true;
			if (!File.Exists(winzip))
			{
				winzip = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), " (x86)\\WinZip\\wzunzip.exe");
				if (!File.Exists(winzip))
				{
					winzipExists = false;
				}
			}
			if (File.Exists(string.Concat(this.Directory, "UC_Feed4.txt")))
			{
				File.Delete(string.Concat(this.Directory, "UC_Feed4.txt"));
			}
			if (File.Exists(string.Concat(this.Directory, "autobytel.csv")))
			{
				File.Delete(string.Concat(this.Directory, "autobytel.csv"));
			}
			if (winzipExists)
			{
				Process process = new Process();
				process.StartInfo.FileName = winzip;
				process.StartInfo.Arguments = string.Concat(destination, " \\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\AutoBytel\\ -o");
				process.Start();
				while (!process.HasExited)
				{
					winzipExists = true;
				}
			}
			if (File.Exists(string.Concat(this.Directory, "UC_Feed4.txt")))
			{
				File.Copy(string.Concat(this.Directory, "UC_Feed4.txt"), string.Concat(this.Directory, this.SourceFile));
				File.Delete(string.Concat(this.Directory, "UC_Feed4.txt"));
			}
			if (base.Start())
			{
				this.import();
				base.Complete();
				if (File.Exists(string.Concat(this.Directory, "UC_Feed4.txt")))
				{
					File.Delete(string.Concat(this.Directory, "UC_Feed4.txt"));
				}
				if (File.Exists(string.Concat(this.Directory, "autobytel.csv")))
				{
					File.Delete(string.Concat(this.Directory, "autobytel.csv"));
				}
				if (File.Exists(string.Concat(this.Directory, "autobytel.zip")))
				{
					File.Delete(string.Concat(this.Directory, "autobytel.zip"));
				}
			}
		}

		private void import()
		{
			System.Exception err;
			string fileName = string.Concat(this.Directory, this.SourceFile);
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				DataFlatFile file = new DataFlatFile(fileName, ",", true);
				RawAutobytel autobytel = new RawAutobytel();
				file.MoveNext();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						JobExecutionRaw noOfRecords = this.Raw;
						noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
						RawAutobytel rawAutobytel = new RawAutobytel(Startup.dbInventoryImport, "IMPORT.EXE", 0)
						{
							JobExecutionRawId = this.Raw.JobExecutionRawId,
							DealerId = file[14],
							VehicleId = int.Parse((file[1] == "" ? "0" : file[1])),
							VIN = file[2],
							VehicleYear = file[3],
							VehicleMake = file[4],
							VehicleModel = file[5],
							VehicleTrim = file[6]
						};
						autobytel = rawAutobytel;
						try
						{
							autobytel.PriceSelling = float.Parse(file[7]);
						}
						catch
						{
							autobytel.PriceSelling = 0f;
						}
						try
						{
							autobytel.Mileage = int.Parse(file[8]);
						}
						catch
						{
							autobytel.Mileage = 0;
						}
						autobytel.ExteriorColor = file[9];
						autobytel.Options = file[10];
						autobytel.Features = file[11];
						autobytel.City = file[12];
						autobytel.State = file[13];
						autobytel.ZipCode = file[14];
						autobytel.ImageUrl = file[15];
						try
						{
							autobytel.HasOtherImages = (int.Parse(file[16]) == 1 ? true : false);
						}
						catch
						{
							autobytel.HasOtherImages = false;
						}
						string imageOne = autobytel.ImageUrl;
						string images = imageOne;
						if (autobytel.HasOtherImages)
						{
							images = string.Concat(images, ",", imageOne.Replace("_1.jpg", "_2.jpg"));
							images = string.Concat(images, ",", imageOne.Replace("_1.jpg", "_3.jpg"));
							images = string.Concat(images, ",", imageOne.Replace("_1.jpg", "_4.jpg"));
							images = string.Concat(images, ",", imageOne.Replace("_1.jpg", "_5.jpg"));
							images = string.Concat(images, ",", imageOne.Replace("_1.jpg", "_6.jpg"));
							images = string.Concat(images, ",", imageOne.Replace("_1.jpg", "_7.jpg"));
						}
						autobytel.ImageUrl = images;
						query = string.Concat(query, autobytel.SaveQuery(), "\n");
						if (this.Raw.NoOfRecords % this.Interval == 0)
						{
							batch.ExecuteStatement(query);
							query = "";
						}
						Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
						if (this.Raw.NoOfRecords % 100 == 0)
						{
							Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
						}
						GC.Collect();
					}
					catch (System.Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (System.Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}