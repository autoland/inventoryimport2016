using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;

namespace Import.Execute.Raw
{
	public class EbizAutos : JobRaw
	{
		public EbizAutos()
		{
		}

		public override void Execute()
		{
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\EbizAutos\\");
			this.SourceFile = "ebizautos.txt";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("EbizAutos", LookupType.DataProvider, Startup.dbAime);
			if (base.Start())
			{
				this.importEbizAutos();
				base.Complete();
			}
		}

		private void importEbizAutos()
		{
			Exception err;
			string fileName = string.Concat(this.Directory, this.SourceFile);
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				DataFlatFile file = new DataFlatFile(fileName, "|", true);
				RawEbizautos ebiz = new RawEbizautos();
				file.MoveNext();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						if (!base.IsDealerPresent(file[0]))
						{
							file.MoveNext();
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							continue;
						}
						else
						{
							JobExecutionRaw noOfRecords = this.Raw;
							noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
							RawEbizautos rawEbizauto = new RawEbizautos(Startup.dbInventoryImport, "IMPORT.EXE", 0)
							{
								JobExecutionRawId = this.Raw.JobExecutionRawId,
								DealerId = int.Parse((file[0].Trim() == "" ? "0" : file[0])),
								CompanyName = file[1],
								CompanyAddress = file[2],
								CompanyCity = file[3],
								CompanyState = file[4],
								CompanyZip = file[5],
								CompanyPhone = file[6],
								ListingId = int.Parse((file[7].Trim() == "" ? "0" : file[7])),
								VinNo = file[8],
								NewUsed = file[9],
								StockNo = file[10],
								VehicleYear = file[11],
								Make = file[12],
								Model = file[13],
								BodyStyle = file[14],
								Doors = file[15],
								Trim = file[16],
								ExtColor = file[17],
								IntColor = file[18],
								IntSurface = file[19],
								Engine = file[20],
								Fuel = file[21],
								Drivetrain = file[22],
								Transmission = file[23],
								Mileage = int.Parse((file[24].Trim() == "" ? "0" : file[24])),
								InternetPrice = int.Parse((file[25].Trim() == "" ? "0" : file[25])),
								IsCertified = (file[26] == "1" ? true : false),
								Options = file[27],
								Description = file[28],
								PhotoUrl = file[29]
							};
							ebiz = rawEbizauto;
							query = string.Concat(query, ebiz.SaveQuery(), "\n");
							if (this.Raw.NoOfRecords % this.Interval == 0)
							{
								batch.ExecuteStatement(query);
								query = "";
							}
							Status status = Startup.Status;
							string[] upper = new string[] { "EBIZAUTOS - ", ebiz.VinNo.ToUpper(), " - ", ebiz.VehicleYear, " ", ebiz.Make, " ", ebiz.Model };
							status.UpdateStatus(string.Concat(upper));
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							if (this.Raw.NoOfRecords % 100 == 0)
							{
								Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
							}
							GC.Collect();
						}
					}
					catch (Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}