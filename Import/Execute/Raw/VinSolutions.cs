using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;

namespace Import.Execute.Raw
{
	public class VinSolutions : JobRaw
	{
		private string dealerStore = "";

		private int dealerId;

		public VinSolutions(string store)
		{
			this.dealerStore = store;
		}

		public override void Execute()
		{
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\VinSolutions\\");
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("VinSolutions", LookupType.DataProvider, Startup.dbAime);
			string str = this.dealerStore;
			if (str != null)
			{
				string str1 = str;
				string str2 = str1;
				if (str1 != null)
				{
					switch (str2)
					{
						case "mossbuickgmc":
						{
							this.SourceFile = "mossbrosbuickgmc.csv";
							this.dealerId = 4109;
							if (base.Start())
							{
								this.import();
								base.Complete();
							}
							return;
						}
						case "mosschevy":
						{
							this.SourceFile = "mossbroschevy.csv";
							this.dealerId = 4108;
							if (base.Start())
							{
								this.import();
								base.Complete();
							}
							return;
						}
						case "mosscjdmv":
						{
							this.SourceFile = "mossbroscjdmv.csv";
							this.dealerId = 4110;
							if (base.Start())
							{
								this.import();
								base.Complete();
							}
							return;
						}
						case "mosscjdriv":
						{
							this.SourceFile = "mossbroscjdriv.csv";
							this.dealerId = 4106;
							if (base.Start())
							{
								this.import();
								base.Complete();
							}
							return;
						}
						case "mosscjdsb":
						{
							this.SourceFile = "mossbroscjdsb.csv";
							this.dealerId = 4107;
							if (base.Start())
							{
								this.import();
								base.Complete();
							}
							return;
						}
						case "mosshonda":
						{
							this.SourceFile = "mossbroshonda.csv";
							this.dealerId = 4111;
							if (base.Start())
							{
								this.import();
								base.Complete();
							}
							return;
						}
						case "mosstoyota":
						{
							this.SourceFile = "mossbrostoyota.csv";
							this.dealerId = 4112;
							if (base.Start())
							{
								this.import();
								base.Complete();
							}
							return;
						}
						case "southbayford":
						{
							this.SourceFile = "southbayford.csv";
							this.dealerId = 4113;
							if (base.Start())
							{
								this.import();
								base.Complete();
							}
							return;
						}
						case "riversidemetro":
						{
							this.SourceFile = "riversidemetro.csv";
							this.dealerId = 4114;
							if (base.Start())
							{
								this.import();
								base.Complete();
							}
							return;
						}
						case "southbaybmwmini":
						{
							this.SourceFile = "southbaybmwmini.csv";
							this.dealerId = 4115;
							if (base.Start())
							{
								this.import();
								base.Complete();
							}
							return;
						}
						case "keyeslexus":
						{
							this.SourceFile = "keyeslexus.csv";
							this.dealerId = 4116;
							if (base.Start())
							{
								this.import();
								base.Complete();
							}
							return;
						}
						case "PacificHonda":
						{
							this.SourceFile = "pacifichonda.csv";
							this.dealerId = 3774;
							if (base.Start())
							{
								this.import();
								base.Complete();
							}
							return;
						}
					}
				}
				if (base.Start())
				{
					this.import();
					base.Complete();
				}
				return;
			}
			if (base.Start())
			{
				this.import();
				base.Complete();
			}
		}

		private void import()
		{
			Exception err;
			string fileName = string.Concat(this.Directory, this.SourceFile);
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				DataFlatFile file = new DataFlatFile(fileName, ",", false);
				RawVinSolutions vinSolutions = new RawVinSolutions();
				file.MoveNext();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						JobExecutionRaw noOfRecords = this.Raw;
						noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
						RawVinSolutions rawVinSolution = new RawVinSolutions(Startup.dbInventoryImport, "IMPORT.EXE", 0)
						{
							JobExecutionRawId = this.Raw.JobExecutionRawId,
							DealerId = this.dealerId.ToString(),
							NewUsed = file[0],
							Year = int.Parse((file[1] == "" ? "0" : file[1])),
							Make = file[2],
							Model = file[3],
							Trim = file[4],
							BodyStyle = file[5],
							StockNumber = file[6],
							Vin = file[7],
							Mileage = int.Parse((file[8] == "" ? "0" : file[8])),
							Price = float.Parse((file[9] == "" ? "0" : file[9])),
							Engine = file[10],
							Transmission = file[11],
							Color = file[12],
							Interior = file[13],
							Comment = file[14],
							Options = file[15],
							LotPrice = float.Parse((file[16] == "" ? "0" : file[16])),
							Msrp = float.Parse((file[17] == "" ? "0" : file[17])),
							Invoice = float.Parse((file[18] == "" ? "0" : file[18])),
							ImageURLs = file[19],
							Certified = file[20],
							Doors = int.Parse((file[36] == "" ? "0" : file[36])),
							ModelSeries = file[37],
							EngineCylinders = file[38]
						};
						vinSolutions = rawVinSolution;
						query = string.Concat(query, vinSolutions.SaveQuery(), "\n");
						if (this.Raw.NoOfRecords % this.Interval == 0)
						{
							batch.ExecuteStatement(query);
							query = "";
						}
						Status status = Startup.Status;
						object[] upper = new object[] { "VINSOLUTIONS - ", vinSolutions.Vin.ToUpper(), " - ", vinSolutions.Year, " ", vinSolutions.Make, " ", vinSolutions.Model };
						status.UpdateStatus(string.Concat(upper));
						Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
						if (this.Raw.NoOfRecords % 100 == 0)
						{
							Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
						}
						GC.Collect();
					}
					catch (Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}