using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;

namespace Import.Execute.Raw
{
	public class NetLab : JobRaw
	{
		public NetLab()
		{
		}

		public override void Execute()
		{
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\NetLab\\");
			this.SourceFile = "TheNetLab.csv";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("NetLab", LookupType.DataProvider, Startup.dbAime);
			if (base.Start())
			{
				this.importNetLab();
				base.Complete();
			}
		}

		private void importNetLab()
		{
			string fileName = string.Concat(this.Directory, this.SourceFile);
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				DataFlatFile file = new DataFlatFile(fileName, ",", true);
				RawNetLab netlab = new RawNetLab();
				file.MoveNext();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						if (!base.IsDealerPresent(file[0]))
						{
							file.MoveNext();
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							continue;
						}
						else
						{
							JobExecutionRaw noOfRecords = this.Raw;
							noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
							RawNetLab rawNetLab = new RawNetLab(Startup.dbInventoryImport, "IMPORT.EXE", 0)
							{
								JobExecutionRawId = this.Raw.JobExecutionRawId,
								DealerID = file[0],
								NewUsed = file[1],
								StockID = file[2],
								VehicleDescription = file[3],
								Year = int.Parse((file[4] == "" ? "0" : file[4])),
								Make = file[5],
								Model = file[6],
								Trim = file[7],
								VIN = file[8],
								Style = file[9],
								Engine = file[10],
								DriveTrain = file[11],
								Transmission = file[12],
								Options = file[13],
								Mileage = int.Parse((file[14].Trim() == "" ? "0" : file[14])),
								SellingPrice = float.Parse((file[15].Trim() == "" ? "0" : file[15])),
								RetailPrice = float.Parse((file[16].Trim() == "" ? "0" : file[16])),
								RetailPriceKbb = float.Parse((file[17].Trim() == "" ? "0" : file[17])),
								ImageUrls = file[18]
							};
							netlab = rawNetLab;
							query = string.Concat(query, netlab.SaveQuery(), "\n");
							if (this.Raw.NoOfRecords % this.Interval == 0)
							{
								batch.ExecuteStatement(query);
								query = "";
							}
							Status status = Startup.Status;
							object[] upper = new object[] { "NETLAB - ", netlab.VIN.ToUpper(), " - ", netlab.Year, " ", netlab.Make, " ", netlab.Model };
							status.UpdateStatus(string.Concat(upper));
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							if (this.Raw.NoOfRecords % 100 == 0)
							{
								Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
							}
							GC.Collect();
						}
					}
					catch (Exception exception)
					{
						Exception err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (Exception exception1)
			{
				Startup.Status.WriteToFile(this.Raw.LogFileName, exception1);
			}
		}
	}
}