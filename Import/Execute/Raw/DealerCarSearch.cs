using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Advocar.Tools;
using Import;
using System;

namespace Import.Execute.Raw
{
	public class DealerCarSearch : JobRaw
	{
		public DealerCarSearch()
		{
		}

		public override void Execute()
		{
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\DealerCarSearch\\");
			this.SourceFile = "DCS_Inventory.csv";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("DealerCarSearch", LookupType.DataProvider, Startup.dbAime);
			if (base.Start())
			{
				this.import();
				base.Complete();
			}
		}

		private void import()
		{
			System.Exception err;
			string fileName = string.Concat(this.Directory, this.SourceFile);
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				DataFlatFile file = new DataFlatFile(fileName, ",", false);
				RawDealerCarSearch dealerCarSearch = new RawDealerCarSearch();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						if (!base.IsDealerPresent(file[0]))
						{
							file.MoveNext();
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							continue;
						}
						else
						{
							JobExecutionRaw noOfRecords = this.Raw;
							noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
							RawDealerCarSearch rawDealerCarSearch = new RawDealerCarSearch(Startup.dbInventoryImport, "IMPORT.EXE", 0)
							{
								JobExecutionRawId = this.Raw.JobExecutionRawId,
								DealerId = file[0],
								Vin = file[1],
								Make = file[2],
								Model = file[3],
								Trim = file[4],
								DriveType = file[5],
								TransmissionType = file[6],
								Year = (Validation.IsNumeric(file[7]) ? Convert.ToInt32(file[7]) : 0),
								StockNumber = file[8],
								InteriorType = file[9],
								InteriorColor = file[10],
								ExteriorColor = file[11],
								Cylinders = (Validation.IsNumeric(file[12]) ? Convert.ToInt32(file[12]) : 0),
								Cost = (Validation.IsNumeric(file[13]) ? Convert.ToInt32(file[13]) : 0),
								Wholesale = (Validation.IsNumeric(file[14]) ? Convert.ToInt32(file[14]) : 0),
								Retail = (Validation.IsNumeric(file[15]) ? Convert.ToInt32(file[15]) : 0),
								Mileage = (Validation.IsNumeric(file[16]) ? Convert.ToInt32(file[16]) : 0),
								PurchaseDate = file[17],
								VideoUrl = file[18],
								Options = file[19],
								Images = file[20],
								LastModifiedDate = file[21],
								BodyType = file[22],
								Engine = file[23],
								MpgCity = (Validation.IsNumeric(file[24]) ? Convert.ToInt32(file[24]) : 0),
								MpgHighway = (Validation.IsNumeric(file[25]) ? Convert.ToInt32(file[25]) : 0),
								NewUsed = file[26],
								Msrp = (Validation.IsNumeric(file[27]) ? Convert.ToInt32(file[27]) : 0),
								ImageLastModifiedDate = file[28],
								Comments = file[29],
								CertifiedPreOwned = (Validation.IsNumeric(file[30]) ? Convert.ToInt32(file[30]) : 0) > 0
							};
							dealerCarSearch = rawDealerCarSearch;
							query = string.Concat(query, dealerCarSearch.SaveQuery(), "\n");
							if (this.Raw.NoOfRecords % this.Interval == 0)
							{
								batch.ExecuteStatement(query);
								query = "";
							}
							Status status = Startup.Status;
							object[] upper = new object[] { "DEALER CAR SEARCH - ", dealerCarSearch.Vin.ToUpper(), " - ", dealerCarSearch.Year, " ", dealerCarSearch.Make, " ", dealerCarSearch.Model };
							status.UpdateStatus(string.Concat(upper));
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							if (this.Raw.NoOfRecords % 100 == 0)
							{
								Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
							}
							GC.Collect();
						}
					}
					catch (System.Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (System.Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}