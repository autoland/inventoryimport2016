using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;
using System.Diagnostics;
using System.IO;

namespace Import.Execute.Raw
{
	public class HomeNet : JobRaw
	{
		public HomeNet()
		{
		}

		public override void Execute()
		{
			string winzip = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "\\WinZip\\wzunzip.exe");
			string HomeNet = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\HomeNet\\inventory.zip");
			bool winzipExists = true;
			if (!File.Exists(winzip))
			{
				winzip = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), " (x86)\\WinZip\\wzunzip.exe");
				if (!File.Exists(winzip))
				{
					winzipExists = false;
				}
			}
			if (!File.Exists(HomeNet))
			{
				winzipExists = false;
			}
			if (winzipExists)
			{
				Process process = new Process();
				process.StartInfo.FileName = winzip;
				process.StartInfo.Arguments = string.Concat(HomeNet, " \\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\HomeNet\\ -o");
				process.Start();
				while (!process.HasExited)
				{
					winzipExists = true;
				}
			}
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\HomeNet\\");
			this.SourceFile = "inventory.zip";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("HomeNet", LookupType.DataProvider, Startup.dbAime);
			if (base.Start())
			{
				this.importVehicles();
				string source = HomeNet;
				source = string.Concat(this.Directory, "inventory.txt");
				if (File.Exists(source))
				{
					File.Delete(source);
				}
				base.Complete();
			}
		}

		private void importVehicles()
		{
			Exception err;
			string fileName = string.Concat(this.Directory, "\\inventory.txt");
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport)
				{
					Timeout = 600
				};
				DataFlatFile file = new DataFlatFile(fileName, "|", true);
				RawHomeNet homenet = new RawHomeNet();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						if (!base.IsDealerPresent(file[0]))
						{
							file.MoveNext();
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							continue;
						}
						else
						{
							JobExecutionRaw noOfRecords = this.Raw;
							noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
							RawHomeNet rawHomeNet = new RawHomeNet(Startup.dbInventoryImport, "IMPORT.EXE", 0)
							{
								JobExecutionRawId = this.Raw.JobExecutionRawId,
								DealerId = file[0],
								NewUsed = file[1],
								Stock = file[2],
								Vin = file[3],
								VehicleYear = file[4],
								Make = file[5],
								Model = file[6],
								ModelNumber = file[7],
								Body = file[8],
								Trim = file[9],
								Doors = file[10],
								ExteriorColor = file[11],
								InteriorColor = file[12],
								EngineCylinders = file[13],
								EngineDisplacement = file[14],
								Transmission = file[15],
								Miles = file[16],
								SellingPrice = file[17],
								MSRP = file[18],
								BookValue = file[19],
								Invoice = file[20],
								Certified = file[21],
								DateInStock = file[22],
								Description = file[23],
								Options = file[24],
								StyleDescription = file[25],
								ExtColorGeneric = file[26],
								ExtColorCode = file[27],
								IntColorGeneric = file[28],
								IntColorCode = file[29],
								IntUpholstery = file[30],
								EngineBlockType = file[31],
								EngineAspirationType = file[32],
								EngineDescription = file[33],
								TransmissionSpeed = file[34],
								TransmissionDescription = file[35],
								Drivetrain = file[36],
								FuelType = file[37],
								CityMPG = file[38],
								HighwayMPG = file[39],
								EPAClassification = file[40],
								WheelbaseCode = file[41],
								InternetPrice = file[42],
								MiscPrice1 = file[43],
								MiscPrice2 = file[44],
								MiscPrice3 = file[45],
								FactoryCodes = file[46],
								MarketClass = file[47],
								PassengerCapacity = file[48],
								ExtColorHexCode = file[49],
								IntColorHexCode = file[50],
								EngineDisplacementCubicInches = file[52],
								PhotoURLs = file[63]
							};
							homenet = rawHomeNet;
							query = string.Concat(query, homenet.SaveQuery(), "\n");
							if (this.Raw.NoOfRecords % (this.Interval * 5) == 0)
							{
								batch.ExecuteStatement(query);
								query = "";
							}
							Status status = Startup.Status;
							string[] upper = new string[] { "HOME NET - ", homenet.Vin.ToUpper(), " - ", homenet.VehicleYear, " ", homenet.Make, " ", homenet.Model, " ", homenet.Trim };
							status.UpdateStatus(string.Concat(upper));
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							if (this.Raw.NoOfRecords % 100 == 0)
							{
								Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
							}
							GC.Collect();
						}
					}
					catch (Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}