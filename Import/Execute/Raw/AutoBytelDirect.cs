using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Advocar.Tools;
using Import;
using System;
using System.Diagnostics;
using System.IO;

namespace Import.Execute.Raw
{
	public class AutoBytelDirect : JobRaw
	{
		public AutoBytelDirect()
		{
		}

		public override void Execute()
		{
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\AutoBytelDirect\\");
			this.SourceFile = "ConciereBuyingService.zip";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("AutoBytelDirect", LookupType.DataProvider, Startup.dbAime);
			string destination = string.Concat(this.Directory, this.SourceFile);
			string winzip = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "\\WinZip\\wzunzip.exe");
			bool winzipExists = true;
			if (!File.Exists(winzip))
			{
				winzip = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), " (x86)\\WinZip\\wzunzip.exe");
				if (!File.Exists(winzip))
				{
					winzipExists = false;
				}
			}
			if (File.Exists(string.Concat(this.Directory, "ConciergeBuyingServiceStatus.txt")))
			{
				File.Delete(string.Concat(this.Directory, "ConciergeBuyingServiceStatus.txt"));
			}
			if (File.Exists(string.Concat(this.Directory, "NewMakeModelSeries.txt")))
			{
				File.Delete(string.Concat(this.Directory, "NewMakeModelSeries.txt"));
			}
			if (File.Exists(string.Concat(this.Directory, "UCDealers.txt")))
			{
				File.Delete(string.Concat(this.Directory, "UCDealers.txt"));
			}
			if (File.Exists(string.Concat(this.Directory, "UCInventory.txt")))
			{
				File.Delete(string.Concat(this.Directory, "UCInventory.txt"));
			}
			if (winzipExists)
			{
				Process process = new Process();
				process.StartInfo.FileName = winzip;
				process.StartInfo.Arguments = string.Concat(destination, " \\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\AutobytelDirect\\ -o");
				process.Start();
				while (!process.HasExited)
				{
					winzipExists = true;
				}
			}
			if (base.Start())
			{
				this.importStatus();
				this.importDealer();
				this.importNewInfo();
				this.import();
				base.Complete();
				if (File.Exists(string.Concat(this.Directory, "ConciergeBuyingServiceStatus.txt")))
				{
					File.Delete(string.Concat(this.Directory, "ConciergeBuyingServiceStatus.txt"));
				}
				if (File.Exists(string.Concat(this.Directory, "NewMakeModelSeries.txt")))
				{
					File.Delete(string.Concat(this.Directory, "NewMakeModelSeries.txt"));
				}
				if (File.Exists(string.Concat(this.Directory, "UCDealers.txt")))
				{
					File.Delete(string.Concat(this.Directory, "UCDealers.txt"));
				}
				if (File.Exists(string.Concat(this.Directory, "UCInventory.txt")))
				{
					File.Delete(string.Concat(this.Directory, "UCInventory.txt"));
				}
				if (File.Exists(string.Concat(this.Directory, "ConciereBuyingService.zip")))
				{
					File.Delete(string.Concat(this.Directory, "ConciereBuyingService.zip"));
				}
			}
		}

		private void import()
		{
			System.Exception err;
			string fileName = string.Concat(this.Directory, "UCInventory.txt");
			try
			{
				DataAccess dataAccess = new DataAccess(Startup.dbInventoryImport);
				DataFlatFile file = new DataFlatFile(fileName, ",", true);
				RawAutobytelDirect autobytel = new RawAutobytelDirect();
				file.MoveNext();
				autobytel = new RawAutobytelDirect(Startup.dbInventoryImport, "IMPORT.EXE", 0);
				autobytel.BulkInsertStart();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						JobExecutionRaw noOfRecords = this.Raw;
						noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
						autobytel.JobExecutionRawId = this.Raw.JobExecutionRawId;
						autobytel.VehicleID = file[0];
						autobytel.DealerID = file[1];
						autobytel.ProgramID = file[2];
						autobytel.VIN = file[3];
						autobytel.DealerStock = file[4];
						autobytel.Year = file[5];
						autobytel.Make = file[6];
						autobytel.Model = file[7];
						autobytel.Series = file[8];
						autobytel.Price = file[9];
						autobytel.Mileage = file[10];
						autobytel.Doors = file[11];
						autobytel.Cylinders = file[12];
						autobytel.Transmission = file[13];
						autobytel.InteriorColor = file[14];
						autobytel.ExteriorColor = file[15];
						autobytel.ImageURL = file[16];
						try
						{
							autobytel.Features = file[17].Substring(0, 3800);
						}
						catch
						{
							autobytel.Features = "";
						}
						if (Validation.IsNumeric(autobytel.VehicleID))
						{
							autobytel.BulkInsertSave();
						}
						if (this.Raw.NoOfRecords % 500 == 0)
						{
							autobytel.BuilInsertCommit();
							autobytel.BulkInsertStart();
						}
						Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
						if (this.Raw.NoOfRecords % 500 == 0)
						{
							Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
						}
						GC.Collect();
					}
					catch (System.Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				autobytel.BuilInsertCommit();
			}
			catch (System.Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}

		private void importDealer()
		{
			System.Exception err;
			string fileName = string.Concat(this.Directory, "UCDealers.txt");
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				DataFlatFile file = new DataFlatFile(fileName, ",", true);
				RawAutobytelDirectDealers dealer = new RawAutobytelDirectDealers();
				file.MoveNext();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						JobExecutionRaw noOfRecords = this.Raw;
						noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
						RawAutobytelDirectDealers rawAutobytelDirectDealer = new RawAutobytelDirectDealers(Startup.dbInventoryImport, "IMPORT.EXE", 0)
						{
							JobExecutionRawId = this.Raw.JobExecutionRawId,
							DealerID = file[0],
							Name = file[1],
							Address = file[2],
							City = file[3],
							State = file[4],
							ZipCode = file[5],
							Phone = file[6],
							Latitude = file[7],
							Longitude = file[8],
							ContactFirstName = file[9],
							ContactLastName = file[10],
							ContactPhone = file[11],
							FinancialRoutingPlatformName = file[12],
							FinancialRoutingAccountID = file[13],
							MonOpen = file[14],
							MonClose = file[15],
							TueOpen = file[16],
							TueClose = file[17],
							WedOpen = file[18],
							WedClose = file[19],
							ThrOpen = file[20],
							ThrClose = file[21],
							FriOpen = file[22],
							FriClose = file[23],
							SatOpen = file[24],
							SatClose = file[25],
							SunOpen = file[26],
							SunClose = file[27]
						};
						dealer = rawAutobytelDirectDealer;
						if (Validation.IsNumeric(dealer.DealerID))
						{
							query = string.Concat(query, dealer.SaveQuery(), "\n");
						}
						if (this.Raw.NoOfRecords % this.Interval == 0)
						{
							batch.ExecuteStatement(query);
							query = "";
						}
						Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
						if (this.Raw.NoOfRecords % 100 == 0)
						{
							Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
						}
						GC.Collect();
					}
					catch (System.Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (System.Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}

		private void importNewInfo()
		{
			System.Exception err;
			string fileName = string.Concat(this.Directory, "NewMakeModelSeries.txt");
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				DataFlatFile file = new DataFlatFile(fileName, ",", true);
				RawAutobytelDirectNewinfo newinfo = new RawAutobytelDirectNewinfo();
				file.MoveNext();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						JobExecutionRaw noOfRecords = this.Raw;
						noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
						RawAutobytelDirectNewinfo rawAutobytelDirectNewinfo = new RawAutobytelDirectNewinfo(Startup.dbInventoryImport, "IMPORT.EXE", 0)
						{
							JobExecutionRawId = this.Raw.JobExecutionRawId,
							Year = file[0],
							Make = file[1],
							Model = file[2],
							Series = file[3]
						};
						newinfo = rawAutobytelDirectNewinfo;
						if (Validation.IsNumeric(newinfo.Year))
						{
							query = string.Concat(query, newinfo.SaveQuery(), "\n");
						}
						if (this.Raw.NoOfRecords % this.Interval == 0)
						{
							batch.ExecuteStatement(query);
							query = "";
						}
						Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
						if (this.Raw.NoOfRecords % 100 == 0)
						{
							Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
						}
						GC.Collect();
					}
					catch (System.Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (System.Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}

		private void importStatus()
		{
			System.Exception err;
			string fileName = string.Concat(this.Directory, "ConciergeBuyingServiceStatus.txt");
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				DataFlatFile file = new DataFlatFile(fileName, ",", true);
				RawAutobytelDirectStatus status = new RawAutobytelDirectStatus();
				file.MoveNext();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						JobExecutionRaw noOfRecords = this.Raw;
						noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
						RawAutobytelDirectStatus rawAutobytelDirectStatu = new RawAutobytelDirectStatus(Startup.dbInventoryImport, "IMPORT.EXE", 0)
						{
							JobExecutionRawId = this.Raw.JobExecutionRawId,
							StatusID = file[0],
							StatusDesc = file[1]
						};
						status = rawAutobytelDirectStatu;
						if (Validation.IsNumeric(status.StatusID))
						{
							query = string.Concat(query, status.SaveQuery(), "\n");
						}
						if (this.Raw.NoOfRecords % this.Interval == 0)
						{
							batch.ExecuteStatement(query);
							query = "";
						}
						Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
						if (this.Raw.NoOfRecords % 100 == 0)
						{
							Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
						}
						GC.Collect();
					}
					catch (System.Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (System.Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}