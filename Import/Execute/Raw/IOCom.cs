using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;

namespace Import.Execute.Raw
{
	public class IOCom : JobRaw
	{
		public IOCom()
		{
		}

		public override void Execute()
		{
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\IOCom\\");
			this.SourceFile = "iocom.txt";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("IOCom", LookupType.DataProvider, Startup.dbAime);
			if (base.Start())
			{
				this.importIoCom();
				base.Complete();
			}
		}

		private void importIoCom()
		{
			Exception err;
			string fileName = string.Concat(this.Directory, this.SourceFile);
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				DataFlatFile file = new DataFlatFile(fileName, "|", true);
				RawIoCom iocom = new RawIoCom();
				file.MoveNext();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						if (!base.IsDealerPresent(file[0]))
						{
							file.MoveNext();
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							continue;
						}
						else
						{
							JobExecutionRaw noOfRecords = this.Raw;
							noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
							RawIoCom rawIoCom = new RawIoCom(Startup.dbInventoryImport, "IMPORT.EXE", 0)
							{
								JobExecutionRawId = this.Raw.JobExecutionRawId,
								DealerId = file[0],
								NewUsed = file[1],
								Vin = file[2],
								Miles = int.Parse((file[3] == "" ? "0" : file[3])),
								Doors = int.Parse((file[4] == "" ? "0" : file[4])),
								DealerStockNo = file[5],
								FeedVehicleDescription = file[6],
								FeedVehicleYear = int.Parse((file[7] == "" ? "0" : file[7])),
								FeedVehicleMake = file[8],
								FeedVehicleModel = file[9],
								FeedVehicleTrim = file[10],
								FeedVehicleStyle = file[11],
								FeedVehicleEngine = file[12],
								FeedVehicleDrivetrain = file[13],
								FeedVehicleTransmission = file[14],
								ExteriorColor = file[15],
								InteriorColor = file[16],
								OptionList = file[17],
								NoOfOptions = int.Parse((file[18] == "" ? "0" : file[18])),
								ImageList = file[19],
								NoOfImages = int.Parse((file[20] == "" ? "0" : file[20])),
								FeedPriceRetailMsrp = float.Parse((file[21] == "" ? "0" : file[21])),
								FeedPriceAcquisition = float.Parse((file[22] == "" ? "0" : file[22])),
								FeedPriceInvoiceWholesale = float.Parse((file[23] == "" ? "0" : file[23])),
								FeedPriceSelling = float.Parse((file[24] == "" ? "0" : file[24]))
							};
							iocom = rawIoCom;
							query = string.Concat(query, iocom.SaveQuery(), "\n");
							if (this.Raw.NoOfRecords % this.Interval == 0)
							{
								batch.ExecuteStatement(query);
								query = "";
							}
							Status status = Startup.Status;
							object[] upper = new object[] { "IOCOM - ", iocom.Vin.ToUpper(), " - ", iocom.FeedVehicleYear, " ", iocom.FeedVehicleMake, " ", iocom.FeedVehicleModel };
							status.UpdateStatus(string.Concat(upper));
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							if (this.Raw.NoOfRecords % 100 == 0)
							{
								Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
							}
							GC.Collect();
						}
					}
					catch (Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}