using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;
using System.Diagnostics;
using System.IO;

namespace Import.Execute.Raw
{
	public class DigitalMotorWorks : JobRaw
	{
		public DigitalMotorWorks()
		{
		}

		public override void Execute()
		{
			DateTime now;
			string winzip = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "\\WinZip\\wzunzip.exe");
			bool winzipExists = true;
			if (!File.Exists(winzip))
			{
				winzip = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), " (x86)\\WinZip\\wzunzip.exe");
				if (!File.Exists(winzip))
				{
					winzipExists = false;
				}
			}
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\DigitalMotorWorks\\");
			string[] files = System.IO.Directory.GetFiles(this.Directory);
			string fileToExtract = "";
			int index = 0;
			while ((int)files.Length > index)
			{
				string directory = this.Directory;
				now = DateTime.Now;
				fileToExtract = string.Concat(directory, "VINVENTORY_", now.ToString("yyyyMMdd"));
				if ((files[index].IndexOf(fileToExtract) < 0 ? false : files[index].IndexOf(".zip") > 0))
				{
					fileToExtract = files[index];
					break;
				}
				else
				{
					try
					{
						File.Delete(files[index]);
					}
					catch
					{
					}
					index++;
				}
			}
			if (!File.Exists(fileToExtract))
			{
				winzipExists = false;
			}
			if (winzipExists)
			{
				Process process = new Process();
				process.StartInfo.FileName = winzip;
				process.StartInfo.Arguments = string.Concat(fileToExtract, " \\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\DigitalMotorWorks\\ -o");
				process.Start();
				while (!process.HasExited)
				{
					winzipExists = true;
				}
			}
			string fileToProcess = fileToExtract.Replace(".zip", ".txt");
			if (File.Exists(string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\DigitalMotorWorks\\vehicles.txt")))
			{
				File.Delete(string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\DigitalMotorWorks\\vehicles.txt"));
			}
			File.Copy(fileToProcess, string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\DigitalMotorWorks\\vehicles.txt"));
			File.Delete(fileToProcess);
			fileToProcess = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\DigitalMotorWorks\\vehicles.txt");
			this.SourceFile = "vehicles.txt";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("DigitalMotorWorks", LookupType.DataProvider, Startup.dbAime);
			if (base.Start())
			{
				this.importVehicles();
				string source = string.Concat(this.Directory, "vehicles.txt");
				if (File.Exists(source))
				{
					File.Delete(source);
				}
				if (File.Exists(fileToExtract))
				{
					string str = this.Directory;
					now = DateTime.Now;
					File.Copy(fileToExtract, string.Concat(str, "\\Archive\\VINVENTORY_", now.ToString("yyyyMMdd"), ".zip"));
					File.Delete(fileToExtract);
				}
				base.Complete();
			}
		}

		private void importVehicles()
		{
			Exception err;
			string fileName = string.Concat(this.Directory, "\\vehicles.txt");
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport)
				{
					Timeout = 600
				};
				DataFlatFile file = new DataFlatFile(fileName, ",", false);
				RawDigitalMotorWorks vehicles = new RawDigitalMotorWorks();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						if (!base.IsDealerPresent(file[0]))
						{
							file.MoveNext();
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							continue;
						}
						else
						{
							JobExecutionRaw noOfRecords = this.Raw;
							noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
							RawDigitalMotorWorks rawDigitalMotorWork = new RawDigitalMotorWorks(Startup.dbInventoryImport, "IMPORT.EXE", 0)
							{
								JobExecutionRawId = this.Raw.JobExecutionRawId,
								DealerId = file[0],
								StockNumber = file[1]
							};
							vehicles = rawDigitalMotorWork;
							try
							{
								vehicles.InventoryDate = file[2];
							}
							catch
							{
								vehicles.InventoryDate = "";
							}
							vehicles.NewUsed = file[3];
							vehicles.Status = file[4];
							try
							{
								vehicles.InvoicePrice = float.Parse(file[5]);
							}
							catch
							{
								vehicles.InvoicePrice = 0f;
							}
							try
							{
								vehicles.PackAmount = float.Parse(file[6]);
							}
							catch
							{
								vehicles.PackAmount = 0f;
							}
							try
							{
								vehicles.Cost = float.Parse(file[7]);
							}
							catch
							{
								vehicles.Cost = 0f;
							}
							try
							{
								vehicles.ListPrice = float.Parse(file[8]);
							}
							catch
							{
								vehicles.ListPrice = 0f;
							}
							try
							{
								vehicles.Msrp = float.Parse(file[9]);
							}
							catch
							{
								vehicles.Msrp = 0f;
							}
							vehicles.LotLocation = file[10];
							vehicles.Condition = file[11];
							vehicles.Tagline = file[12];
							vehicles.IsCertified = file[13];
							vehicles.CertificationNumber = file[14];
							vehicles.VIN = file[15];
							vehicles.Make = file[16];
							vehicles.Model = file[17];
							try
							{
								vehicles.ModelYear = int.Parse(file[18]);
							}
							catch
							{
								vehicles.ModelYear = 0;
							}
							vehicles.ModelCode = file[19];
							vehicles.TrimLevel = file[20];
							vehicles.SubTrimLevel = file[21];
							vehicles.Classification = file[22];
							vehicles.VehicleTypeCode = file[23];
							try
							{
								vehicles.Odometer = int.Parse(file[24]);
							}
							catch
							{
								vehicles.Odometer = 0;
							}
							try
							{
								vehicles.PayloadCapcacity = int.Parse(file[25]);
							}
							catch
							{
								vehicles.PayloadCapcacity = 0;
							}
							try
							{
								vehicles.SeatingCapacity = int.Parse(file[26]);
							}
							catch
							{
								vehicles.SeatingCapacity = 0;
							}
							try
							{
								vehicles.WheelBase = float.Parse(file[27]);
							}
							catch
							{
								vehicles.WheelBase = 0f;
							}
							vehicles.BodyDescription = file[28];
							try
							{
								vehicles.BodyDoorCount = int.Parse(file[29]);
							}
							catch
							{
								vehicles.BodyDoorCount = 0;
							}
							vehicles.DrivetrainDescription = file[30];
							vehicles.EngineDescription = file[31];
							try
							{
								vehicles.EngineCylinderCount = int.Parse(file[32]);
							}
							catch
							{
								vehicles.EngineCylinderCount = 0;
							}
							vehicles.TransmissionDescription = file[33];
							vehicles.TransmissionType = file[34];
							vehicles.ExteriorColorDescription = file[35];
							vehicles.ExteriorColorBaseColor = file[36];
							vehicles.InteriorDescription = file[37];
							vehicles.InteriorColor = file[38];
							vehicles.StandardFeatures = file[39];
							vehicles.DealerAddedFeatures = file[40];
							vehicles.LastModifiedDate = file[42];
							vehicles.ModifiedFlag = file[43];
							vehicles.DealerName = file[44];
							vehicles.DealerAddress = file[45];
							vehicles.DealerCity = file[46];
							vehicles.DealerState = file[47];
							vehicles.DealerPostalCode = file[48];
							vehicles.DealerPhoneNumber = file[49];
							vehicles.MediaId = file[50];
							vehicles.ImageUrls = file[51];
							query = string.Concat(query, vehicles.SaveQuery(), "\n");
							if (this.Raw.NoOfRecords % (this.Interval * 5) == 0)
							{
								batch.ExecuteStatement(query);
								query = "";
								Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
							}
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							GC.Collect();
						}
					}
					catch (Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}