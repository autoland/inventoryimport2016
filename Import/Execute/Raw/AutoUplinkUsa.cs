using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;

namespace Import.Execute.Raw
{
	public class AutoUplinkUsa : JobRaw
	{
		public AutoUplinkUsa()
		{
		}

		public override void Execute()
		{
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\AutoUplinkUSA\\");
			this.SourceFile = "autouplinkusa.txt";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("AutoUplinkUSA", LookupType.DataProvider, Startup.dbAime);
			if (base.Start())
			{
				this.import();
				base.Complete();
			}
		}

		private void import()
		{
			Exception err;
			string fileName = string.Concat(this.Directory, this.SourceFile);
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				DataFlatFile file = new DataFlatFile(fileName, "|", true);
				RawAutouplinkusa vehicle = new RawAutouplinkusa();
				file.MoveNext();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						if (!base.IsDealerPresent(file[0]))
						{
							file.MoveNext();
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							continue;
						}
						else
						{
							JobExecutionRaw noOfRecords = this.Raw;
							noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
							RawAutouplinkusa rawAutouplinkusa = new RawAutouplinkusa(Startup.dbInventoryImport, "IMPORT.EXE", 0)
							{
								JobExecutionRawId = this.Raw.JobExecutionRawId,
								DealerId = int.Parse((file[0] == "" ? "0" : file[0])),
								StockNo = file[1],
								VIN = file[2],
								VehicleMake = file[3],
								VehicleModel = file[4],
								VehicleYear = file[5],
								Mileage = int.Parse((file[6] == "" ? "0" : file[6])),
								BodyStyle = file[7],
								ExtColor = file[8],
								IntColor = file[9],
								Transmission = file[10],
								EngineCid = file[11],
								EngineSize = file[12],
								EngineType = file[13],
								Price = float.Parse((file[14] == "" ? "0" : file[14])),
								SalesCost = float.Parse((file[15] == "" ? "0" : file[15])),
								Options = file[16],
								ImageUrl = file[17],
								VideoUrl = file[18]
							};
							vehicle = rawAutouplinkusa;
							query = string.Concat(query, vehicle.SaveQuery(), "\n");
							if (this.Raw.NoOfRecords % this.Interval == 0)
							{
								batch.ExecuteStatement(query);
								query = "";
							}
							Status status = Startup.Status;
							string[] upper = new string[] { "AUTOUPLINKUSA - ", vehicle.VIN.ToUpper(), " - ", vehicle.VehicleYear, " ", vehicle.VehicleMake, " ", vehicle.VehicleModel };
							status.UpdateStatus(string.Concat(upper));
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							if (this.Raw.NoOfRecords % 100 == 0)
							{
								Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
							}
							GC.Collect();
						}
					}
					catch (Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}