using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;

namespace Import.Execute.Raw
{
	public class Vauto : JobRaw
	{
		public Vauto()
		{
		}

		public override void Execute()
		{
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\Vauto\\");
			this.SourceFile = "vauto_inventory.txt";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("Vauto", LookupType.DataProvider, Startup.dbAime);
			if (base.Start())
			{
				this.import();
				base.Complete();
			}
		}

		private void import()
		{
			Exception err;
			string fileName = string.Concat(this.Directory, this.SourceFile);
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				DataFlatFile file = new DataFlatFile(fileName, "\t", false);
				RawVauto vauto = new RawVauto();
				while (!file.EOF)
				{
					query = string.Concat(query, vauto.SaveQuery(), "\n");
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						if (!base.IsDealerPresent(file[0]))
						{
							file.MoveNext();
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							continue;
						}
						else
						{
							JobExecutionRaw noOfRecords = this.Raw;
							noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
							RawVauto rawVauto = new RawVauto(Startup.dbInventoryImport, "IMPORT.EXE", 0)
							{
								JobExecutionRawId = this.Raw.JobExecutionRawId,
								DealerId = file[0],
								Vin = file[1],
								StockNumber = file[2],
								NewUsed = file[3],
								Year = int.Parse((file[4] == "" ? "0" : file[4])),
								Make = file[5],
								Model = file[6],
								ModelNumber = file[7],
								Body = file[8],
								Transmission = file[9],
								Series = file[10],
								BodyDoorCount = int.Parse((file[11] == "" ? "0" : file[11])),
								Odometer = int.Parse((file[12] == "" ? "0" : file[12])),
								EngineCylinderCount = file[13],
								EngineDisplacement = file[14],
								DriveTrain = file[15],
								Color = file[16],
								InteriorColor = file[17],
								Invoice = float.Parse((file[18] == "" ? "0" : file[18])),
								Msrp = float.Parse((file[19] == "" ? "0" : file[19])),
								BookValue = float.Parse((file[20] == "" ? "0" : file[20])),
								Price = float.Parse((file[21] == "" ? "0" : file[21])),
								InventoryDate = file[22],
								Certified = file[23],
								Description = file[24],
								Features = file[25],
								PhotoUrlList = file[26]
							};
							vauto = rawVauto;
							query = string.Concat(query, vauto.SaveQuery(), "\n");
							if (this.Raw.NoOfRecords % this.Interval == 0)
							{
								batch.ExecuteStatement(query);
								query = "";
							}
							Status status = Startup.Status;
							object[] upper = new object[] { "VAUTO - ", vauto.Vin.ToUpper(), " - ", vauto.Year, " ", vauto.Make, " ", vauto.Model };
							status.UpdateStatus(string.Concat(upper));
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							if (this.Raw.NoOfRecords % 100 == 0)
							{
								Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
							}
							GC.Collect();
						}
					}
					catch (Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}