using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;

namespace Import.Execute.Raw
{
	public class Cobalt : JobRaw
	{
		public Cobalt()
		{
		}

		public override void Execute()
		{
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\Cobalt\\");
			this.SourceFile = "cobalt.txt";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("Cobalt", LookupType.DataProvider, Startup.dbAime);
			if (base.Start())
			{
				this.importVehicles();
				base.Complete();
			}
		}

		private void importVehicles()
		{
			Exception err;
			string fileName = string.Concat(this.Directory, this.SourceFile);
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport)
				{
					Timeout = 600
				};
				DataFlatFile file = new DataFlatFile(fileName, "|", true);
				RawCobalt cobalt = new RawCobalt();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						if (!base.IsDealerPresent(file[0]))
						{
							file.MoveNext();
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							continue;
						}
						else
						{
							JobExecutionRaw noOfRecords = this.Raw;
							noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
							RawCobalt rawCobalt = new RawCobalt(Startup.dbInventoryImport, "IMPORT.EXE", 0)
							{
								JobExecutionRawId = this.Raw.JobExecutionRawId,
								DealerId = file[0],
								NewUsed = file[1],
								DealerStockNo = file[2],
								VehicleDescription = file[3],
								VehicleYear = file[4],
								VehicleMake = file[5],
								VehicleModel = file[6],
								VehicleTrim = file[7],
								Vin = file[8],
								VehicleStyle = file[9],
								Engine = file[10],
								Drivetrain = file[11],
								Transmission = file[12],
								ExteriorColor = file[13],
								InteriorColor = file[14],
								OptionList = file[15]
							};
							cobalt = rawCobalt;
							try
							{
								cobalt.Miles = int.Parse(file[16]);
							}
							catch
							{
								cobalt.Miles = 0;
							}
							try
							{
								cobalt.PriceAquisition = float.Parse(file[17]);
							}
							catch
							{
								cobalt.PriceAquisition = 0f;
							}
							try
							{
								cobalt.PriceInvoice = float.Parse(file[18]);
							}
							catch
							{
								cobalt.PriceInvoice = 0f;
							}
							try
							{
								cobalt.PriceSelling = float.Parse(file[19]);
							}
							catch
							{
								cobalt.PriceSelling = 0f;
							}
							try
							{
								cobalt.PriceRetail = float.Parse(file[20]);
							}
							catch
							{
								cobalt.PriceRetail = 0f;
							}
							try
							{
								cobalt.PriceMarketIndex = float.Parse(file[21]);
							}
							catch
							{
								cobalt.PriceMarketIndex = 0f;
							}
							try
							{
								cobalt.PriceMsrp = float.Parse(file[22]);
							}
							catch
							{
								cobalt.PriceMsrp = 0f;
							}
							cobalt.ImageLocationList = file[23];
							cobalt.Warranty = file[24];
							try
							{
								cobalt.Doors = int.Parse(file[25]);
							}
							catch
							{
								cobalt.Doors = 0;
							}
							cobalt.FuelType = file[26];
							query = string.Concat(query, cobalt.SaveQuery(), "\n");
							if (this.Raw.NoOfRecords % (this.Interval * 5) == 0)
							{
								batch.ExecuteStatement(query);
								query = "";
							}
							Status status = Startup.Status;
							string[] upper = new string[] { "COBALT - ", cobalt.Vin.ToUpper(), " - ", cobalt.VehicleYear, " ", cobalt.VehicleMake, " ", cobalt.VehicleModel, " ", cobalt.VehicleTrim };
							status.UpdateStatus(string.Concat(upper));
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							if (this.Raw.NoOfRecords % 100 == 0)
							{
								Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
							}
							GC.Collect();
						}
					}
					catch (Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}