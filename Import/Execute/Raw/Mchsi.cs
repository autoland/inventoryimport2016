using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;

namespace Import.Execute.Raw
{
	public class Mchsi : JobRaw
	{
		public Mchsi()
		{
		}

		public override void Execute()
		{
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\Mchsi\\");
			this.SourceFile = "vehicles.txt";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("Mchsi", LookupType.DataProvider, Startup.dbAime);
			if (base.Start())
			{
				this.importMchsi();
				base.Complete();
			}
		}

		private void importMchsi()
		{
			Exception err;
			string fileName = string.Concat(this.Directory, this.SourceFile);
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				DataFlatFile file = new DataFlatFile(fileName, "|", false);
				RawMchsi mchsi = new RawMchsi();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						if (!base.IsDealerPresent(file[0]))
						{
							file.MoveNext();
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							continue;
						}
						else
						{
							JobExecutionRaw noOfRecords = this.Raw;
							noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
							RawMchsi rawMchsi = new RawMchsi(Startup.dbInventoryImport, "IMPORT.EXE", 0)
							{
								JobExecutionRawId = this.Raw.JobExecutionRawId,
								DealerId = file[0],
								StockNo = file[1],
								Vin = file[2],
								VehicleYear = file[3],
								Manufacturer = file[4],
								Model = file[5],
								Trim = file[6],
								BodyType = file[7],
								Price = file[8],
								Mileage = file[9],
								Engine = file[10],
								Transmission = file[11],
								ExteriorColor = file[12],
								InteriorColor = file[13],
								Options = file[14],
								Images = file[15]
							};
							mchsi = rawMchsi;
							query = string.Concat(query, mchsi.SaveQuery(), "\n");
							if (this.Raw.NoOfRecords % this.Interval == 0)
							{
								batch.ExecuteStatement(query);
								query = "";
							}
							Status status = Startup.Status;
							string[] upper = new string[] { "MCHSI - ", mchsi.Vin.ToUpper(), " - ", mchsi.VehicleYear, " ", mchsi.Manufacturer, " ", mchsi.Model };
							status.UpdateStatus(string.Concat(upper));
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							if (this.Raw.NoOfRecords % 100 == 0)
							{
								Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
							}
							GC.Collect();
						}
					}
					catch (Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}