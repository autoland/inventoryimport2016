using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;
using System.Diagnostics;
using System.IO;

namespace Import.Execute.Raw
{
	public class DealerSpecialties : JobRaw
	{
		public DealerSpecialties()
		{
		}

		public override void Execute()
		{
			string winzip = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "\\WinZip\\wzunzip.exe");
			string[] strArrays = new string[] { "\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\DealerSpecialties\\Advocar_", null, null };
			string[] media = strArrays;
			DateTime now = DateTime.Now;
			media[3] = now.ToString("yyMMdd");
			media[4] = ".zip";
			string dealerSpecialties = string.Concat(media);
			bool winzipExists = true;
			if (!File.Exists(winzip))
			{
				winzip = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), " (x86)\\WinZip\\wzunzip.exe");
				if (!File.Exists(winzip))
				{
					winzipExists = false;
				}
			}
			if (!File.Exists(dealerSpecialties))
			{
				winzipExists = false;
			}
			if (winzipExists)
			{
				Process process = new Process();
				process.StartInfo.FileName = winzip;
				process.StartInfo.Arguments = string.Concat(dealerSpecialties, " \\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\DealerSpecialties\\ -o");
				process.Start();
				while (!process.HasExited)
				{
					winzipExists = true;
				}
			}
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\DealerSpecialties\\");
			now = DateTime.Now;
			this.SourceFile = string.Concat("Advocar_", now.ToString("yyMMdd"), ".zip");
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("DealerSpecialties", LookupType.DataProvider, Startup.dbAime);
			if (base.Start())
			{
				this.importVehicles();
				this.importLinks();
				string source = dealerSpecialties;
				source = string.Concat(this.Directory, "vehicles.txt");
				if (File.Exists(source))
				{
					File.Delete(source);
				}
				source = string.Concat(this.Directory, "links.txt");
				if (File.Exists(source))
				{
					File.Delete(source);
				}
				source = string.Concat(this.Directory, "lotdata.txt");
				if (File.Exists(source))
				{
					File.Delete(source);
				}
				base.Complete();
			}
		}

		private void importLinks()
		{
			Exception err;
			string fileName = string.Concat(this.Directory, "\\links.txt");
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport)
				{
					Timeout = 600
				};
				DataFlatFile file = new DataFlatFile(fileName, ",", true);
				RawDealerSpecialtiesLink links = new RawDealerSpecialtiesLink();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						if (!base.IsDealerPresent(file[0]))
						{
							file.MoveNext();
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							continue;
						}
						else
						{
							JobExecutionRaw noOfRecords = this.Raw;
							noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
							RawDealerSpecialtiesLink rawDealerSpecialtiesLink = new RawDealerSpecialtiesLink(Startup.dbInventoryImport, "IMPORT.EXE", 0)
							{
								JobExecutionRawId = this.Raw.JobExecutionRawId,
								DealerId = file[0],
								Vin = file[1],
								Vevo = file[2],
								AltVideo = file[3],
								PhotoUrls = file[4],
								WindowSticker = file[5],
								BuyersGuide = file[6],
								AuctionLink = file[7],
								VehicleHistoryReport = file[8],
								Reserved1 = file[9],
								Reserved2 = file[10],
								Reserved3 = file[11],
								Reserved4 = file[12],
								Reserved5 = file[13],
								Reserved6 = file[14]
							};
							links = rawDealerSpecialtiesLink;
							query = string.Concat(query, links.SaveQuery(), "\n");
							if (this.Raw.NoOfRecords % (this.Interval * 5) == 0)
							{
								batch.ExecuteStatement(query);
								query = "";
							}
							Startup.Status.UpdateStatus(string.Concat("DEALER SPECIALTIES LINKS - ", links.Vin.ToUpper()));
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							if (this.Raw.NoOfRecords % 100 == 0)
							{
								Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
							}
							GC.Collect();
						}
					}
					catch (Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}

		private void importVehicles()
		{
			Exception err;
			string fileName = string.Concat(this.Directory, "\\vehicles.txt");
			if (File.Exists(string.Concat(Environment.CurrentDirectory, "\\Download\\vehicles.txt")))
			{
				File.Delete(string.Concat(Environment.CurrentDirectory, "\\Download\\vehicles.txt"));
			}
			if (File.Exists(fileName))
			{
				File.Copy(fileName, string.Concat(Environment.CurrentDirectory, "\\Download\\vehicles.txt"));
				fileName = string.Concat(Environment.CurrentDirectory, "\\Download\\vehicles.txt");
			}
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport)
				{
					Timeout = 600
				};
				DataFlatFile file = new DataFlatFile(fileName, ",", true);
				RawDealerSpecialties dealerspecial = new RawDealerSpecialties();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						if (!base.IsDealerPresent(file[0]))
						{
							file.MoveNext();
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							continue;
						}
						else
						{
							JobExecutionRaw noOfRecords = this.Raw;
							noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
							RawDealerSpecialties rawDealerSpecialty = new RawDealerSpecialties(Startup.dbInventoryImport, "IMPORT.EXE", 0)
							{
								JobExecutionRawId = this.Raw.JobExecutionRawId,
								DealerId = file[0],
								Vin = file[1],
								StockNumber = file[2],
								Status = file[3]
							};
							dealerspecial = rawDealerSpecialty;
							try
							{
								dealerspecial.VehicleType = int.Parse((file[4] == "" ? "0" : file[4]));
							}
							catch
							{
								dealerspecial.VehicleType = 0;
							}
							try
							{
								dealerspecial.Year = int.Parse((file[5] == "" ? "0" : file[5]));
							}
							catch
							{
								dealerspecial.Year = 0;
							}
							dealerspecial.Make = file[6];
							dealerspecial.Model = file[7];
							dealerspecial.Trim = file[8];
							dealerspecial.Body = file[9];
							dealerspecial.VehicleCategory = file[11];
							try
							{
								dealerspecial.Mileage = int.Parse((file[12] == "" ? "0" : file[12]));
							}
							catch
							{
								dealerspecial.Mileage = 0;
							}
							dealerspecial.Transmission = file[13];
							dealerspecial.EngineDisplacement = file[14];
							dealerspecial.EngineSize = file[15];
							dealerspecial.Induction = file[16];
							dealerspecial.Drivetrain = file[17];
							dealerspecial.FuelType = file[18];
							try
							{
								dealerspecial.FuelEconomyCity = float.Parse((file[19] == "" ? "0" : file[19]));
							}
							catch
							{
								dealerspecial.FuelEconomyCity = 0f;
							}
							try
							{
								dealerspecial.FuelEconomyHighway = float.Parse((file[20] == "" ? "0" : file[20]));
							}
							catch
							{
								dealerspecial.FuelEconomyHighway = 0f;
							}
							try
							{
								dealerspecial.FuelEconomyCombined = float.Parse((file[21] == "" ? "0" : file[21]));
							}
							catch
							{
								dealerspecial.FuelEconomyCombined = 0f;
							}
							try
							{
								dealerspecial.Doors = int.Parse((file[22] == "" ? "0" : file[22]));
							}
							catch
							{
								dealerspecial.Doors = 0;
							}
							dealerspecial.OemColorCode = file[23];
							dealerspecial.ExternalColor = file[25];
							dealerspecial.InternalColor = file[26];
							dealerspecial.GenericColor = file[27];
							try
							{
								dealerspecial.InternetPrice = int.Parse((file[29] == "" ? "0" : file[29]));
							}
							catch
							{
								dealerspecial.InternetPrice = 0;
							}
							try
							{
								dealerspecial.ComparisonPrice = int.Parse((file[30] == "" ? "0" : file[30]));
							}
							catch
							{
								dealerspecial.ComparisonPrice = 0;
							}
							dealerspecial.OemModelCode = file[34];
							dealerspecial.HasWarranty = file[35];
							try
							{
								dealerspecial.CertificationWarranty = int.Parse((file[36] == "" ? "0" : file[36]));
							}
							catch
							{
								dealerspecial.CertificationWarranty = 0;
							}
							try
							{
								dealerspecial.WarrantyMonths = int.Parse((file[37] == "" ? "0" : file[37]));
							}
							catch
							{
								dealerspecial.WarrantyMonths = 0;
							}
							try
							{
								dealerspecial.WarrantyMiles = int.Parse((file[38] == "" ? "0" : file[38]));
							}
							catch
							{
								dealerspecial.WarrantyMiles = 0;
							}
							dealerspecial.CertificationNumber = file[39];
							dealerspecial.ServiceContract = file[40];
							dealerspecial.InServiceDate = file[41];
							dealerspecial.CertificationDate = file[42];
							dealerspecial.DateManufactured = file[43];
							dealerspecial.DateCreated = file[44];
							dealerspecial.DateUpdated = file[45];
							dealerspecial.DateRemoved = file[46];
							dealerspecial.Photos = file[48];
							try
							{
								dealerspecial.SuperSizePhotos = int.Parse((file[49] == "" ? "0" : file[49]));
							}
							catch
							{
								dealerspecial.SuperSizePhotos = 0;
							}
							dealerspecial.AddendumDetails = file[50];
							dealerspecial.DepartmentComments = file[51];
							dealerspecial.VehicleComments = file[52];
							dealerspecial.Options = file[53];
							try
							{
								dealerspecial.PurchasePayment = float.Parse((file[54] == "" ? "0" : file[54]));
							}
							catch
							{
								dealerspecial.PurchasePayment = 0f;
							}
							try
							{
								dealerspecial.PurchaseDownPayment = float.Parse((file[55] == "" ? "0" : file[55]));
							}
							catch
							{
								dealerspecial.PurchaseDownPayment = 0f;
							}
							try
							{
								dealerspecial.PurchaseTerm = int.Parse((file[56] == "" ? "0" : file[56]));
							}
							catch
							{
								dealerspecial.PurchaseTerm = 0;
							}
							dealerspecial.PurchaseDiclosure = file[57];
							try
							{
								dealerspecial.PurchaseRate = int.Parse((file[58] == "" ? "0" : file[58]));
							}
							catch
							{
								dealerspecial.PurchaseRate = 0;
							}
							try
							{
								dealerspecial.LeasePayment = float.Parse((file[59] == "" ? "0" : file[59]));
							}
							catch
							{
								dealerspecial.LeasePayment = 0f;
							}
							try
							{
								dealerspecial.LeaseDownPayment = float.Parse((file[60] == "" ? "0" : file[60]));
							}
							catch
							{
								dealerspecial.LeaseDownPayment = 0f;
							}
							try
							{
								dealerspecial.LeaseTerm = int.Parse((file[61] == "" ? "0" : file[61]));
							}
							catch
							{
								dealerspecial.LeaseTerm = 0;
							}
							dealerspecial.LeaseDisclosure = file[62];
							try
							{
								dealerspecial.LeaseRate = int.Parse((file[63] == "" ? "0" : file[63]));
							}
							catch
							{
								dealerspecial.LeaseRate = 0;
							}
							dealerspecial.Reserved1 = file[65];
							dealerspecial.Reserved2 = file[66];
							dealerspecial.Reserved3 = file[67];
							dealerspecial.Reserved4 = file[68];
							dealerspecial.Reserved5 = file[69];
							dealerspecial.Reserved6 = file[70];
							query = string.Concat(query, dealerspecial.SaveQuery(), "\n");
							if (this.Raw.NoOfRecords % (this.Interval * 5) == 0)
							{
								batch.ExecuteStatement(query);
								query = "";
								Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
							}
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							GC.Collect();
						}
					}
					catch (Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}