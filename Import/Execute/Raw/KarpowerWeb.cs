using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;
using System.IO;

namespace Import.Execute.Raw
{
	public class KarpowerWeb : JobRaw
	{
		public KarpowerWeb()
		{
		}

		public override void Execute()
		{
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\Karpower\\");
			DirectoryInfo[] directories = (new DirectoryInfo(this.Directory)).GetDirectories();
			for (int i = 0; i < (int)directories.Length; i++)
			{
				DirectoryInfo directory = directories[i];
				string[] media = new string[] { "\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\Karpower\\", directory.Name, "\\" };
				this.Directory = string.Concat(media);
				this.SourceFile = "vehicles.txt";
				this.Raw.LookupId_DataProvider = Lookup.GetRecordId("KarpowerWeb", LookupType.DataProvider, Startup.dbAime);
				if (base.Start())
				{
					DataFlatFile dataFlatFile = new DataFlatFile(string.Concat(this.Directory, this.SourceFile), "\t", false);
					this.importKarpowerWeb(string.Concat(this.Directory, this.SourceFile), directory.Name);
					base.Complete();
				}
			}
		}

		private void importKarpowerWeb(string filename, string dirname)
		{
			Exception err;
			string query = "";
			try
			{
				DataFlatFile file = new DataFlatFile(filename, "\t", false);
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				RawKbbKarpowerWeb karpower = new RawKbbKarpowerWeb();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecords = raw.NoOfRecords + 1;
						JobExecutionRaw noOfRecordsInFile = this.Raw;
						noOfRecordsInFile.NoOfRecordsInFile = noOfRecordsInFile.NoOfRecordsInFile + 1;
						this.Raw.NoOfDealers = 1;
						karpower = new RawKbbKarpowerWeb(Startup.dbInventoryImport, "IMPORT.EXE", 0);
						string[] row = file.Row;
						karpower.JobExecutionRawId = this.Raw.JobExecutionRawId;
						karpower.DealerId = file[0];
						karpower.DealerName = file[1];
						karpower.DealerAddress = file[2];
						karpower.DealerCity = file[3];
						karpower.DealerState = file[4];
						karpower.DealerZipCode = file[5];
						karpower.DealerCountry = file[6];
						karpower.DealerPhone = file[7];
						karpower.DealerFax = file[8];
						karpower.DealerWebsite = file[9];
						karpower.DealerEmailAddress = file[10];
						karpower.Vin = file[11];
						karpower.StockNumber = file[12];
						try
						{
							karpower.Year = int.Parse(file[13]);
						}
						catch
						{
							karpower.Year = 0;
						}
						karpower.Make = file[14];
						karpower.Model = file[15];
						karpower.Trim = file[16];
						karpower.ModelAndTrim = file[17];
						karpower.Engine = file[18];
						karpower.Drivetrain = file[19];
						karpower.Transmission = file[20];
						try
						{
							karpower.Mileage = int.Parse(file[21]);
						}
						catch
						{
							karpower.Mileage = 0;
						}
						try
						{
							karpower.MileageAdjustment = int.Parse(file[22]);
						}
						catch
						{
							karpower.MileageAdjustment = 0;
						}
						karpower.CurrentDate = file[23];
						karpower.KbbValuationDate = file[24];
						karpower.LastChangeDate = file[25];
						karpower.BookingDate = file[26];
						try
						{
							karpower.IsUsed = bool.Parse(file[27]);
						}
						catch
						{
							karpower.IsUsed = false;
						}
						try
						{
							karpower.IsCertified = bool.Parse(file[28]);
						}
						catch
						{
							karpower.IsCertified = false;
						}
						try
						{
							karpower.IsClassic = bool.Parse(file[29]);
						}
						catch
						{
							karpower.IsClassic = false;
						}
						karpower.ExteriorColor = (file[30] == "[None]" ? "" : file[30]);
						karpower.InteriorColor = (file[31] == "[None]" ? "" : file[31]);
						try
						{
							karpower.WebPrice = float.Parse(file[32]);
						}
						catch
						{
							karpower.WebPrice = 0f;
						}
						try
						{
							karpower.SellingPrice = float.Parse(file[33]);
						}
						catch
						{
							karpower.SellingPrice = 0f;
						}
						try
						{
							karpower.Cost = float.Parse(file[34]);
						}
						catch
						{
							karpower.Cost = 0f;
						}
						try
						{
							karpower.TradeInValue = float.Parse(file[35]);
						}
						catch
						{
							karpower.TradeInValue = 0f;
						}
						karpower.InventoryCategory = file[36];
						try
						{
							karpower.RetailPrice = float.Parse(file[37]);
						}
						catch
						{
							karpower.RetailPrice = 0f;
						}
						try
						{
							karpower.WholesalePrice = float.Parse(file[38]);
						}
						catch
						{
							karpower.WholesalePrice = 0f;
						}
						try
						{
							karpower.MaxDeductRetail = float.Parse(file[39]);
						}
						catch
						{
							karpower.MaxDeductRetail = 0f;
						}
						try
						{
							karpower.MaxDeductWholesale = float.Parse(file[40]);
						}
						catch
						{
							karpower.MaxDeductWholesale = 0f;
						}
						try
						{
							karpower.EstimatedReconditioningCost = float.Parse(file[41]);
						}
						catch
						{
							karpower.EstimatedReconditioningCost = 0f;
						}
						try
						{
							karpower.FullDealerCost = float.Parse(file[42]);
						}
						catch
						{
							karpower.FullDealerCost = 0f;
						}
						karpower.Equipment = file[43];
						karpower.DealerAdditions = file[44];
						karpower.DealerNoChargeAdditions = file[45];
						karpower.MissingEquipment = file[46];
						karpower.LicenseNumber = file[47];
						karpower.LicensePlateExpiration = file[48];
						karpower.LicensePlateState = file[49];
						try
						{
							karpower.SoldPrice = float.Parse(file[50]);
						}
						catch
						{
							karpower.SoldPrice = 0f;
						}
						karpower.SoldDate = file[51];
						karpower.Notes = file[52];
						karpower.MerchandisingText = file[53];
						karpower.VehicleType = file[54];
						karpower.TradeInDate = file[55];
						try
						{
							karpower.Invoice = float.Parse(file[56]);
						}
						catch
						{
							karpower.Invoice = 0f;
						}
						string images = string.Concat("http://www.advocar.com/MediaCom/Inventory/", karpower.Vin, ".jpg");
						for (int imageNo = 2; imageNo <= 6; imageNo++)
						{
							string[] vin = new string[] { images, ",http://www.advocar.com/MediaCom/Inventory/", karpower.Vin, "(", imageNo.ToString(), ").jpg" };
							images = string.Concat(vin);
						}
						karpower.ImageUrlList = images;
						if (karpower.Vin.Length == 17)
						{
							query = string.Concat(query, karpower.SaveQuery(), "\n");
							if (this.Raw.NoOfRecords % this.Interval == 0)
							{
								batch.ExecuteStatement(query);
								query = "";
							}
							Status status = Startup.Status;
							object[] objArray = new object[] { "KARPOWERWEB", dirname, " - ", karpower.Vin.ToUpper(), " - ", karpower.Year, " ", karpower.Make, " ", karpower.Model };
							status.UpdateStatus(string.Concat(objArray));
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							if (this.Raw.NoOfRecords % 100 == 0)
							{
								Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
							}
						}
						GC.Collect();
					}
					catch (Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}