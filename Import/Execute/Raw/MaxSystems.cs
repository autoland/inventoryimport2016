using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Advocar.Tools;
using Import;
using System;

namespace Import.Execute.Raw
{
	public class MaxSystems : JobRaw
	{
		public MaxSystems()
		{
		}

		public override void Execute()
		{
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\MaxSystems\\");
			this.SourceFile = "maxsystemsfl.txt";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("MaxSystems", LookupType.DataProvider, Startup.dbAime);
			if (base.Start())
			{
				this.importMaxSystems();
				base.Complete();
			}
		}

		private void importMaxSystems()
		{
			System.Exception err;
			string fileName = string.Concat(this.Directory, this.SourceFile);
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				DataFlatFile file = new DataFlatFile(fileName, "|", true);
				RawMaxSystems maxSystems = new RawMaxSystems();
				file.MoveNext();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						if (!base.IsDealerPresent(file[0]))
						{
							file.MoveNext();
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							continue;
						}
						else
						{
							JobExecutionRaw noOfRecords = this.Raw;
							noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
							RawMaxSystems rawMaxSystem = new RawMaxSystems(Startup.dbInventoryImport, "IMPORT.EXE", 0)
							{
								JobExecutionRawId = this.Raw.JobExecutionRawId,
								DealerId = file[0],
								StockNumber = file[1],
								Vin = file[2],
								Make = file[3],
								Model = file[4],
								Year = (Validation.IsNumeric(file[5]) ? Convert.ToInt32(file[5]) : 0),
								Mileage = (Validation.IsNumeric(file[6]) ? Convert.ToInt32(file[6]) : 0),
								BodyStyle = file[7],
								ExteriorColor = file[8],
								InteriorColor = file[9],
								Transmission = file[10],
								EngineCid = file[11],
								EngineSize = file[12],
								EngineType = file[13],
								Price = (Validation.IsNumeric(file[14]) ? Convert.ToInt32(file[14]) : 0),
								SalesCost = (Validation.IsNumeric(file[15]) ? Convert.ToInt32(file[15]) : 0),
								Options = file[16],
								ImageUrls = file[17],
								VideoUrl = file[18]
							};
							maxSystems = rawMaxSystem;
							query = string.Concat(query, maxSystems.SaveQuery(), "\n");
							if (this.Raw.NoOfRecords % this.Interval == 0)
							{
								batch.ExecuteStatement(query);
								query = "";
							}
							Status status = Startup.Status;
							object[] upper = new object[] { "MAX SYSTEMS - ", maxSystems.Vin.ToUpper(), " - ", maxSystems.Year, " ", maxSystems.Make, " ", maxSystems.Model };
							status.UpdateStatus(string.Concat(upper));
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							if (this.Raw.NoOfRecords % 100 == 0)
							{
								Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
							}
							GC.Collect();
						}
					}
					catch (System.Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (System.Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}