﻿using Advocar.Data;
using Advocar.Data.Aime;
using Advocar.Data.InventoryImport;
using Advocar.Interface;
using Import;
using System;

namespace Import.Execute.Raw
{
	public class Fex : JobRaw
	{
		public Fex()
		{
		}

		public override void Execute()
		{
			this.Directory = string.Concat("\\\\", Startup.Media, "\\MediaCOM\\InventoryImport\\fex\\");
			this.SourceFile = "fexinventory.csv ";
			this.Raw.LookupId_DataProvider = Lookup.GetRecordId("Fex", LookupType.DataProvider, Startup.dbAime);
			if (base.Start())
			{
				this.importFex();
				base.Complete();
			}
		}

		private void importFex()
		{
			Exception err;
			string fileName = string.Concat(this.Directory, this.SourceFile);
			string query = "";
			try
			{
				DataAccess batch = new DataAccess(Startup.dbInventoryImport);
				DataFlatFile file = new DataFlatFile(fileName, ",", true);
				RawFex fex = new RawFex();
				file.MoveNext();
				while (!file.EOF)
				{
					try
					{
						JobExecutionRaw raw = this.Raw;
						raw.NoOfRecordsInFile = raw.NoOfRecordsInFile + 1;
						if (!base.IsDealerPresent(file[0]))
						{
							file.MoveNext();
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							continue;
						}
						else
						{
							JobExecutionRaw noOfRecords = this.Raw;
							noOfRecords.NoOfRecords = noOfRecords.NoOfRecords + 1;
							RawFex rawFex = new RawFex(Startup.dbInventoryImport, "IMPORT.EXE", 0)
							{
								JobExecutionRawId = this.Raw.JobExecutionRawId,
								Provider_ID = int.Parse((file[0] == "" ? "0" : file[0])),
								Inst_Name = file[1],
								Lot_Add1 = file[2],
								Lot_City = file[3],
								Lot_State = file[4],
								Lot_Zip = int.Parse((file[5] == "" ? "0" : file[5])),
								Lot_Email = file[6],
								StockNumber = file[7],
								Year = int.Parse((file[8] == "" ? "0" : file[8])),
								Make = file[9],
								Model = file[10],
								Vin = file[11],
								Mileage = int.Parse((file[12] == "" ? "0" : file[12])),
								AcquiredDate = Convert.ToDateTime(file[13]),
								AskPrice_Low = float.Parse((file[14].Trim() == "" ? "0" : file[14])),
								BodyStyle = file[15],
								Comments = file[16],
								DriveType = file[17],
								Engine = file[18],
								ExtColor = file[19],
								IntColor = file[20],
								Transmission = file[21],
								Images = file[22]
							};
							fex = rawFex;
							query = string.Concat(query, fex.SaveQuery(), "\n");
							if (this.Raw.NoOfRecords % this.Interval == 0)
							{
								batch.ExecuteStatement(query);
								query = "";
							}
							Status status = Startup.Status;
							string[] upper = new string[] { "FEX - ", fex.Vin.ToUpper(), " - ", null, null, null, null, null };
							upper[3] = fex.Year.ToString();
							upper[4] = " ";
							upper[5] = fex.Make.ToString();
							upper[6] = " ";
							upper[7] = fex.Model;
							status.UpdateStatus(string.Concat(upper));
							Startup.Status.UpdateProgress(file.CurrentRecord, file.RecordCount);
							if (this.Raw.NoOfRecords % 100 == 0)
							{
								Startup.Status.WriteStatusToFile(this.Raw.LogFileName);
							}
							GC.Collect();
						}
					}
					catch (Exception exception)
					{
						err = exception;
						JobExecutionRaw noOfRowErrors = this.Raw;
						noOfRowErrors.NoOfRowErrors = noOfRowErrors.NoOfRowErrors + 1;
						Startup.Status.WriteToFile(this.Raw.LogFileName, err);
					}
					file.MoveNext();
				}
				if (query.Length > 0)
				{
					batch.ExecuteStatement(query);
				}
			}
			catch (Exception exception1)
			{
				err = exception1;
				this.Raw.LookupId_JobExecutionStatus = Lookup.GetRecordId("FatalError", LookupType.JobExecutionStatus, Startup.dbAime);
				Startup.Status.WriteToFile(this.Raw.LogFileName, err);
			}
		}
	}
}