using System;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;

namespace Advocar.WebService.DealerTrack
{
	internal class Process
	{
		public Process()
		{
		}

		public static XmlDocument PostXMLTransaction(string url, XmlDocument xmlDoc)
		{
			XmlTextReader xmlReader;
			XmlDocument xmlResponseDoc = null;
			HttpWebResponse response = null;
			Stream requestStream = null;
			Stream responseStream = null;
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
			try
			{
				try
				{
					byte[] bytes = Encoding.ASCII.GetBytes(xmlDoc.InnerXml);
					request.Method = "POST";
					request.ContentLength = (long)((int)bytes.Length);
					request.ContentType = "text/xml; encoding='utf-8'";
					requestStream = request.GetRequestStream();
					requestStream.Write(bytes, 0, (int)bytes.Length);
					requestStream.Close();
					response = (HttpWebResponse)request.GetResponse();
					if (response.StatusCode == HttpStatusCode.OK)
					{
						responseStream = response.GetResponseStream();
						xmlReader = new XmlTextReader(responseStream);
						XmlDocument xmldoc = new XmlDocument();
						xmldoc.Load(xmlReader);
						xmlResponseDoc = xmldoc;
						xmlReader.Close();
					}
					response.Close();
				}
				catch (Exception exception)
				{
					throw new Exception(exception.Message);
				}
			}
			finally
			{
				requestStream.Close();
				responseStream.Close();
				response.Close();
				xmlReader = null;
				requestStream = null;
				responseStream = null;
				response = null;
				request = null;
			}
			return xmlResponseDoc;
		}

		public string testing()
		{
			return "<dt_application active=\"yes\" status=\"new\" dtversion=\"2.0\">\t<key_data optout=\"no\">\t\t<dt_lender_id>TER</dt_lender_id>\t\t<dt_dealer_id>154769</dt_dealer_id>\t\t<dt_app_id>1243251949</dt_app_id>\t\t<lender_dealer_id>49</lender_dealer_id>\t\t<lender_app_id />\t\t<requestdate>2010-04-28T15:03:04</requestdate>\t\t<credit_type type=\"individual\" />\t\t<app_type type=\"personal\" />\t\t<product_type type=\"retail\" /> \t\t<vehicle_type type=\"new\" trade=\"no\" />\t\t<cust_credit_type type=\"none\" />\t\t<loan_type type=\"auto\" />\t\t<source />\t\t<user_name>PETE PUTTAGIO</user_name>\t</key_data>\t<application_data type=\"newapplication\" regb=\"no\" comu_state=\"no\" swap=\"no\" cosigner_intent=\"na\" program_routing_ind=\"pass\">\t\t<applicant_data type=\"primary\">\t\t\t<first_name>PETER</first_name>\t\t\t<mi /> \t\t\t<last_name>TESTR</last_name>\t\t\t<ssn>167181818</ssn>\t\t\t<dob>1988-10-17</dob>\t\t\t<address type=\"current\">\t\t\t\t<street_no>88</street_no>\t\t\t\t<street_name>WAY</street_name>\t\t\t\t<street_type>RD</street_type>\t\t\t\t<street_type_desc>ROAD</street_type_desc>\t\t\t\t<city>FARMINGDALE</city>\t\t\t\t<state>NY</state>\t\t\t\t<zip_code>11735</zip_code>\t\t\t</address>\t\t\t<home_phone_no>5167177171</home_phone_no>\t\t\t<years_at_address>10</years_at_address>\t\t\t<months_at_address>0</months_at_address>\t\t\t<years_at_prv_address />\t\t\t<months_at_prv_address />\t\t\t<email_address />\t\t\t<housing_status type=\"homeowner\" />\t\t\t<mortgage_rent>900</mortgage_rent>\t\t\t<employment_data type=\"current\">\t\t\t\t<emp_status type=\"unemployed\" />\t\t\t</employment_data>\t\t\t<comments>Test Payment Call</comments>\t\t\t<driver_license_no />\t\t\t<driver_license_state />\t\t\t<other_phone_no />\t\t</applicant_data>\t\t<vehicle_data>\t\t\t<certified_used />\t\t\t<chrome_style_id />\t\t\t<chrome_year />\t\t\t<chrome_make />\t\t\t<chrome_model />\t\t\t<chrome_trim />\t\t\t<other_year />\t\t\t<other_make />\t\t\t<other_model />\t\t\t<other_trim />\t\t\t<veh_desc_source>C</veh_desc_source>\t\t\t<bookout>N</bookout>\t\t\t<bookout_options />\t\t\t<uvc />\t\t\t<vin_uvc />\t\t</vehicle_data>\t\t<product_data>\t\t\t<term_months />\t\t\t<cash_selling_price />\t\t\t<ttl_estimate />\t\t\t<unpaid_balance />\t\t\t<creditlife />\t\t\t<acc_health_insurance />\t\t\t<est_amt_financed />\t\t\t<msrp />\t\t\t<lender_program>Regular Retail</lender_program>\t\t\t<sales_tax />\t\t\t<other_finance_fees />\t\t\t<gap />\t\t\t<other_fees />\t\t\t<wholesale_value condition=\"na\" type=\"na\">0</wholesale_value>\t\t\t<wholesale_source />\t\t\t<retail_value condition=\"na\">0</retail_value>\t\t\t<retail_source />\t\t</product_data>\t</application_data></dt_application>";
		}
	}
}