using System;

namespace Advocar.WebService.Autobytel
{
	public class Dealer
	{
		public bool IsCovered = false;

		public string Name = "";

		public string Address = "";

		public string City = "";

		public string State = "";

		public string ZipCode = "";

		public string Phone = "";

		public string Distance = "";

		public string Latitude = "";

		public string Longitude = "";

		public string ProgramId = "";

		public string DealerId = "";

		public string ContactFirstName = "";

		public string ContactLastName = "";

		public string ContactPhone = "";

		public string FinancialRoutingPlatformName = "";

		public string FinancialRoutingAccountId = "";

		public string MondayOpen = "";

		public string MondayClose = "";

		public string TuesdayOpen = "";

		public string TuesdayClose = "";

		public string WednesdayOpen = "";

		public string WednesdayClose = "";

		public string ThursdayOpen = "";

		public string ThursdayClose = "";

		public string FridayOpen = "";

		public string FridayClose = "";

		public string SaturdayOpen = "";

		public string SatrudayClose = "";

		public string SundayOpen = "";

		public string SundayClose = "";

		public Dealer()
		{
		}
	}
}