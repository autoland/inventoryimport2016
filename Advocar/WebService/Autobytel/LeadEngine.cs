using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web;

namespace Advocar.WebService.Autobytel
{
	public class LeadEngine
	{
		private string url = "http://leadengine.services.staging.myride.com/LeadEngine/ConciergeBuyingService.asmx";

		private string providerId = "31854";

		private string firstName = "";

		private string lastName = "";

		private string street = "";

		private string city = "";

		private string state = "";

		private string zip = "";

		private string primaryPhone = "";

		private string secondaryPhone = "";

		private string emailAddress = "";

		private string comments = "";

		private string year = "";

		private string make = "";

		private string model = "";

		private string trim = "";

		private string programId = "";

		private string dealerId = "";

		public string City
		{
			get
			{
				return this.city;
			}
			set
			{
				this.city = value;
			}
		}

		public string Comments
		{
			get
			{
				return this.comments;
			}
			set
			{
				this.comments = value;
			}
		}

		public string DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public string EmailAddress
		{
			get
			{
				return this.emailAddress;
			}
			set
			{
				this.emailAddress = value;
			}
		}

		public string FirstName
		{
			get
			{
				return this.firstName;
			}
			set
			{
				this.firstName = value;
			}
		}

		public string LastName
		{
			get
			{
				return this.lastName;
			}
			set
			{
				this.lastName = value;
			}
		}

		public string Make
		{
			get
			{
				return this.make;
			}
			set
			{
				this.make = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public string PrimaryPhone
		{
			get
			{
				return this.primaryPhone;
			}
			set
			{
				this.primaryPhone = value;
			}
		}

		public string ProgramId
		{
			get
			{
				return this.programId;
			}
			set
			{
				this.programId = value;
			}
		}

		public string SecondaryPhone
		{
			get
			{
				return this.secondaryPhone;
			}
			set
			{
				this.secondaryPhone = value;
			}
		}

		public string State
		{
			get
			{
				return this.state;
			}
			set
			{
				this.state = value;
			}
		}

		public string Street
		{
			get
			{
				return this.street;
			}
			set
			{
				this.street = value;
			}
		}

		public string Trim
		{
			get
			{
				return this.trim;
			}
			set
			{
				this.trim = value;
			}
		}

		public string Year
		{
			get
			{
				return this.year;
			}
			set
			{
				this.year = value;
			}
		}

		public string Zip
		{
			get
			{
				return this.zip;
			}
			set
			{
				this.zip = value;
			}
		}

		public LeadEngine()
		{
		}

		public string ExtractElementValue(string xml, string variable)
		{
			string str;
			if (xml.Length != 0)
			{
				xml = xml.ToLower();
				variable = variable.ToLower();
				string variableEnd = "";
				variableEnd = string.Concat("</", variable, ">");
				variable = string.Concat("<", variable, ">");
				int start = xml.IndexOf(variable);
				if (start >= 0)
				{
					start = start + variable.Length;
					int end = xml.IndexOf(variableEnd, start);
					str = (end >= start ? xml.Substring(start, end - start) : "");
				}
				else
				{
					str = "";
				}
			}
			else
			{
				str = "";
			}
			return str;
		}

		public Dealer GetNewCarDealer(DealerPing dealerPing)
		{
			Dealer dealer1;
			StringBuilder url = new StringBuilder();
			url.Append(this.url);
			url.Append("/Ping?providerID=");
			url.Append(HttpUtility.UrlEncode(this.providerId));
			url.Append("&year=");
			url.Append(HttpUtility.UrlEncode(dealerPing.Year));
			url.Append("&make=");
			url.Append(HttpUtility.UrlEncode(dealerPing.Make));
			url.Append("&model=");
			url.Append(HttpUtility.UrlEncode(dealerPing.Model));
			url.Append("&trim=");
			url.Append(HttpUtility.UrlEncode(dealerPing.Trim));
			url.Append("&zipCode=");
			url.Append(HttpUtility.UrlEncode(dealerPing.Zip));
			HttpWebRequest request = WebRequest.Create(url.ToString()) as HttpWebRequest;
			HttpWebResponse response = request.GetResponse() as HttpWebResponse;
			string soapReponse = (new StreamReader(response.GetResponseStream())).ReadToEnd();
			Dealer dealer = new Dealer();
			if (!(this.ExtractElementValue(soapReponse, "Coverage").ToLower() == "true"))
			{
				dealer.IsCovered = false;
				dealer1 = dealer;
			}
			else
			{
				dealer.IsCovered = true;
				string xml = this.ExtractElementValue(soapReponse, "CBSDealer");
				dealer.Name = this.ExtractElementValue(xml, "Name");
				dealer.Address = this.ExtractElementValue(xml, "Address");
				dealer.City = this.ExtractElementValue(xml, "City");
				dealer.State = this.ExtractElementValue(xml, "State");
				dealer.ZipCode = this.ExtractElementValue(xml, "ZipCode");
				dealer.Phone = this.ExtractElementValue(xml, "Phone");
				dealer.Distance = this.ExtractElementValue(xml, "Distance");
				dealer.Latitude = this.ExtractElementValue(xml, "Latitude");
				dealer.Longitude = this.ExtractElementValue(xml, "Longitude");
				dealer.ProgramId = this.ExtractElementValue(xml, "ProgramId");
				dealer.DealerId = this.ExtractElementValue(xml, "DealerId");
				dealer.ContactFirstName = this.ExtractElementValue(xml, "ContactFirstName");
				dealer.ContactLastName = this.ExtractElementValue(xml, "ContactLastName");
				dealer.ContactPhone = this.ExtractElementValue(xml, "ContactPhone");
				dealer.FinancialRoutingPlatformName = this.ExtractElementValue(xml, "FinancialRoutingPlatformName");
				dealer.MondayOpen = this.ExtractElementValue(xml, "MonOpen");
				dealer.MondayClose = this.ExtractElementValue(xml, "MonClose");
				dealer.TuesdayOpen = this.ExtractElementValue(xml, "TueOpen");
				dealer.TuesdayClose = this.ExtractElementValue(xml, "TueClose");
				dealer.WednesdayOpen = this.ExtractElementValue(xml, "WedOpen");
				dealer.WednesdayClose = this.ExtractElementValue(xml, "WedClose");
				dealer.ThursdayOpen = this.ExtractElementValue(xml, "ThrOpen");
				dealer.ThursdayClose = this.ExtractElementValue(xml, "ThrClose");
				dealer.FridayOpen = this.ExtractElementValue(xml, "FriOpen");
				dealer.FridayClose = this.ExtractElementValue(xml, "FriClose");
				dealer.SaturdayOpen = this.ExtractElementValue(xml, "SatOpen");
				dealer.SatrudayClose = this.ExtractElementValue(xml, "SatClose");
				dealer.SundayOpen = this.ExtractElementValue(xml, "SunOpen");
				dealer.SundayClose = this.ExtractElementValue(xml, "SunClose");
				dealer1 = dealer;
			}
			return dealer1;
		}

		public string PostNewCarLead()
		{
			StringBuilder soapRequest = new StringBuilder();
			soapRequest.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
			soapRequest.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
			soapRequest.Append("<soap:Body>");
			soapRequest.Append("<Post xmlns=\"http://www.autobytel.com/\">");
			soapRequest.Append("<lead>");
			soapRequest.Append("<Vehicle>");
			soapRequest.Append("<Status>New</Status>");
			soapRequest.Append(string.Concat("<Year>", HttpUtility.HtmlEncode(this.Year), "</Year>"));
			soapRequest.Append(string.Concat("<Make>", HttpUtility.HtmlEncode(this.Make), "</Make>"));
			soapRequest.Append(string.Concat("<Model>", HttpUtility.HtmlEncode(this.Model), "</Model>"));
			soapRequest.Append(string.Concat("<Trim>", HttpUtility.HtmlEncode(this.Trim), "</Trim>"));
			soapRequest.Append("</Vehicle>");
			soapRequest.Append("<Customer>");
			soapRequest.Append(string.Concat("<FirstName>", HttpUtility.HtmlEncode(this.FirstName), "</FirstName>"));
			soapRequest.Append(string.Concat("<LastName>", HttpUtility.HtmlEncode(this.LastName), "</LastName>"));
			soapRequest.Append(string.Concat("<Address1>", HttpUtility.HtmlEncode(this.Street), "</Address1>"));
			soapRequest.Append(string.Concat("<City>", HttpUtility.HtmlEncode(this.City), "</City>"));
			soapRequest.Append(string.Concat("<State>", HttpUtility.HtmlEncode(this.State), "</State>"));
			soapRequest.Append(string.Concat("<ZipCode>", HttpUtility.HtmlEncode(this.Zip), "</ZipCode>"));
			soapRequest.Append(string.Concat("<HomePhone>", HttpUtility.HtmlEncode(this.PrimaryPhone), "</HomePhone>"));
			soapRequest.Append(string.Concat("<WorkPhone>", HttpUtility.HtmlEncode(this.SecondaryPhone), "</WorkPhone>"));
			soapRequest.Append(string.Concat("<EmailAddress>", HttpUtility.HtmlEncode(this.EmailAddress), "</EmailAddress>"));
			soapRequest.Append(string.Concat("<Comments>", HttpUtility.HtmlEncode(this.Comments), "</Comments>"));
			soapRequest.Append("</Customer>");
			soapRequest.Append("<Dealers>");
			soapRequest.Append("<Dealer>");
			soapRequest.Append(string.Concat("<ProgramID>", HttpUtility.HtmlEncode(this.ProgramId), "</ProgramID>"));
			soapRequest.Append(string.Concat("<DealerID>", HttpUtility.HtmlEncode(this.DealerId), "</DealerID>"));
			soapRequest.Append("</Dealer>");
			soapRequest.Append("</Dealers>");
			soapRequest.Append("<Provider>");
			soapRequest.Append(string.Concat("<ProviderID>", HttpUtility.HtmlEncode(this.providerId), "</ProviderID>"));
			soapRequest.Append("</Provider>");
			soapRequest.Append("</lead>");
			soapRequest.Append("</Post>");
			soapRequest.Append("</soap:Body>");
			soapRequest.Append("</soap:Envelope>");
			HttpWebRequest request = WebRequest.Create(this.url) as HttpWebRequest;
			request.Method = "POST";
			request.ContentType = "text/xml; charset=utf-8";
			request.ContentLength = (long)soapRequest.Length;
			request.Headers.Add("SOAPAction", "http://www.autobytel.com/Post");
			StreamWriter writer = new StreamWriter(request.GetRequestStream());
			writer.Write(soapRequest);
			writer.Close();
			StreamReader reader = new StreamReader((request.GetResponse() as HttpWebResponse).GetResponseStream());
			string soapResponse = reader.ReadToEnd();
			reader.Close();
			return soapResponse;
		}

		public string PostUsedCarLead()
		{
			string firstName = "Mary";
			string lastName = "Jones";
			string street = "22 Elm";
			string city = "Orlando";
			string state = "FL";
			string zip = "32817";
			string primaryPhone = "9498623024";
			string secondaryPhone = "9492254586";
			string emailAddress = "MaryJones@yahoo.com";
			string comments = "To Be Determined";
			string vehicleID = "31785570";
			string programID = "135";
			string dealerID = "106307";
			StringBuilder soapRequest = new StringBuilder();
			soapRequest.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
			soapRequest.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
			soapRequest.Append("<soap:Body>");
			soapRequest.Append("<Post xmlns=\"http://www.autobytel.com/\">");
			soapRequest.Append("<lead>");
			soapRequest.Append("<Vehicle>");
			soapRequest.Append("<Status>Used</Status>");
			soapRequest.Append(string.Concat("<VehicleID>", HttpUtility.HtmlEncode(vehicleID), "</VehicleID>"));
			soapRequest.Append("</Vehicle>");
			soapRequest.Append("<Customer>");
			soapRequest.Append(string.Concat("<FirstName>", HttpUtility.HtmlEncode(firstName), "</FirstName>"));
			soapRequest.Append(string.Concat("<LastName>", HttpUtility.HtmlEncode(lastName), "</LastName>"));
			soapRequest.Append(string.Concat("<Address1>", HttpUtility.HtmlEncode(street), "</Address1>"));
			soapRequest.Append(string.Concat("<City>", HttpUtility.HtmlEncode(city), "</City>"));
			soapRequest.Append(string.Concat("<State>", HttpUtility.HtmlEncode(state), "</State>"));
			soapRequest.Append(string.Concat("<ZipCode>", HttpUtility.HtmlEncode(zip), "</ZipCode>"));
			soapRequest.Append(string.Concat("<HomePhone>", HttpUtility.HtmlEncode(primaryPhone), "</HomePhone>"));
			soapRequest.Append(string.Concat("<WorkPhone>", HttpUtility.HtmlEncode(secondaryPhone), "</WorkPhone>"));
			soapRequest.Append(string.Concat("<EmailAddress>", HttpUtility.HtmlEncode(emailAddress), "</EmailAddress>"));
			soapRequest.Append(string.Concat("<Comments>", HttpUtility.HtmlEncode(comments), "</Comments>"));
			soapRequest.Append("</Customer>");
			soapRequest.Append("<Dealers>");
			soapRequest.Append("<Dealer>");
			soapRequest.Append(string.Concat("<ProgramID>", HttpUtility.HtmlEncode(programID), "</ProgramID>"));
			soapRequest.Append(string.Concat("<DealerID>", HttpUtility.HtmlEncode(dealerID), "</DealerID>"));
			soapRequest.Append("</Dealer>");
			soapRequest.Append("</Dealers>");
			soapRequest.Append("<Provider>");
			soapRequest.Append(string.Concat("<ProviderID>", HttpUtility.HtmlEncode(this.providerId), "</ProviderID>"));
			soapRequest.Append("</Provider>");
			soapRequest.Append("</lead>");
			soapRequest.Append("</Post>");
			soapRequest.Append("</soap:Body>");
			soapRequest.Append("</soap:Envelope>");
			HttpWebRequest request = WebRequest.Create(this.url) as HttpWebRequest;
			request.Method = "POST";
			request.ContentType = "text/xml; charset=utf-8";
			request.ContentLength = (long)soapRequest.Length;
			request.Headers.Add("SOAPAction", "http://www.autobytel.com/Post");
			StreamWriter writer = new StreamWriter(request.GetRequestStream());
			writer.Write(soapRequest);
			writer.Close();
			StreamReader reader = new StreamReader((request.GetResponse() as HttpWebResponse).GetResponseStream());
			string soapResponse = reader.ReadToEnd();
			reader.Close();
			return soapResponse;
		}

		public void Update()
		{
			string leadID = "39683012";
			string statusID = "1";
			StringBuilder soapRequest = new StringBuilder();
			soapRequest.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
			soapRequest.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
			soapRequest.Append("<soap:Body>");
			soapRequest.Append("<Update xmlns=\"http://www.autobytel.com/\">");
			soapRequest.Append(string.Concat("<providerID>", HttpUtility.HtmlEncode(this.providerId), "</providerID>"));
			soapRequest.Append(string.Concat("<leadID>", HttpUtility.HtmlEncode(leadID), "</leadID>"));
			soapRequest.Append(string.Concat("<statusID>", HttpUtility.HtmlEncode(statusID), "</statusID>"));
			soapRequest.Append("</Update>");
			soapRequest.Append("</soap:Body>");
			soapRequest.Append("</soap:Envelope>");
			HttpWebRequest request = WebRequest.Create(this.url) as HttpWebRequest;
			request.Method = "POST";
			request.ContentType = "text/xml; charset=utf-8";
			request.ContentLength = (long)soapRequest.Length;
			request.Headers.Add("SOAPAction", "http://www.autobytel.com/Update");
			StreamWriter writer = new StreamWriter(request.GetRequestStream());
			writer.Write(soapRequest);
			writer.Close();
			StreamReader reader = new StreamReader((request.GetResponse() as HttpWebResponse).GetResponseStream());
			string soapResponse = reader.ReadToEnd();
			reader.Close();
			Console.WriteLine(soapResponse);
		}
	}
}