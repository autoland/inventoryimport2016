using System;

namespace Advocar.WebService.Autobytel
{
	public class DealerPing
	{
		public string Zip = "";

		public string Year = "";

		public string Make = "";

		public string Model = "";

		public string Trim = "";

		public DealerPing()
		{
		}
	}
}