using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.CarsFin
{
	public class AccountingJournal : Transaction
	{
		private int cacjID;

		private int cacsID;

		private string journal;

		private string description;

		private DateTime effectiveDate;

		public int CacjID
		{
			get
			{
				return this.cacjID;
			}
			set
			{
				this.cacjID = value;
			}
		}

		public int CacsID
		{
			get
			{
				return this.cacsID;
			}
			set
			{
				this.cacsID = value;
			}
		}

		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

		public DateTime EffectiveDate
		{
			get
			{
				return this.effectiveDate;
			}
			set
			{
				this.effectiveDate = value;
			}
		}

		public string Journal
		{
			get
			{
				return this.journal;
			}
			set
			{
				this.journal = value;
			}
		}

		public AccountingJournal(string connection, string modifiedUser, int cacjID) : base(connection, modifiedUser)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUser;
			this.cacjID = cacjID;
			if (cacjID > 0)
			{
				this.retrievedata();
			}
		}

		public AccountingJournal(string connection, string modifiedUser, int cacsID, string journal) : base(connection, modifiedUser)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUser;
			this.journal = journal;
			this.cacsID = cacsID;
			if (!string.IsNullOrEmpty(this.journal))
			{
				this.retrieveCacjIdFromJournal();
			}
		}

		public override void Delete()
		{
			DataAccess dataAccess = new DataAccess(this.connection);
			dataAccess.AddParam("@CacjID", DataAccessParameterType.Numeric, this.cacjID.ToString(), true);
			dataAccess.ExecuteProcedure("CACJ_Delete");
		}

		private void retrieveCacjIdFromJournal()
		{
			DataAccess dataAccess = new DataAccess(this.connection);
			dataAccess.AddParam("@CacsID", DataAccessParameterType.Numeric, this.cacsID.ToString());
			dataAccess.AddParam("@Journal", DataAccessParameterType.Text, this.journal);
			dataAccess.ExecuteProcedure("CACJ_GetRecord");
			if (!dataAccess.EOF)
			{
				this.cacjID = (Validation.IsNumeric(dataAccess["CacjID"]) ? Convert.ToInt32(dataAccess["CacjID"]) : 0);
				this.cacsID = (Validation.IsNumeric(dataAccess["CacsID"]) ? Convert.ToInt32(dataAccess["CacsID"]) : 0);
				this.journal = dataAccess["Journal"];
				this.description = dataAccess["Description"];
				this.effectiveDate = (Validation.IsDate(dataAccess["EffectiveDate"]) ? Convert.ToDateTime(dataAccess["EffectiveDate"]) : DateTime.MinValue);
			}
			else
			{
				this.wipeout();
			}
		}

		protected override void retrievedata()
		{
			DataAccess dataAccess = new DataAccess(this.connection);
			dataAccess.AddParam("@CacjID", DataAccessParameterType.Numeric, this.cacjID.ToString());
			dataAccess.ExecuteProcedure("CACJ_GetRecord");
			if (!dataAccess.EOF)
			{
				this.cacjID = (Validation.IsNumeric(dataAccess["CacjID"]) ? Convert.ToInt32(dataAccess["CacjID"]) : 0);
				this.cacsID = (Validation.IsNumeric(dataAccess["CacsID"]) ? Convert.ToInt32(dataAccess["CacsID"]) : 0);
				this.journal = dataAccess["Journal"];
				this.description = dataAccess["Description"];
				this.effectiveDate = (Validation.IsDate(dataAccess["EffectiveDate"]) ? Convert.ToDateTime(dataAccess["EffectiveDate"]) : DateTime.MinValue);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess dataAccess = new DataAccess(this.connection);
			dataAccess.AddParam("@CacjID", DataAccessParameterType.Numeric, this.cacjID.ToString(), true);
			dataAccess.AddParam("@CacsID", DataAccessParameterType.Numeric, this.cacsID.ToString(), true);
			dataAccess.AddParam("@Journal", DataAccessParameterType.Numeric, this.journal, true);
			dataAccess.AddParam("@Description", DataAccessParameterType.Numeric, this.description, true);
			dataAccess.AddParam("@EffectiveDate", DataAccessParameterType.DateTime, (this.effectiveDate.Equals(DateTime.MinValue) ? string.Empty : this.effectiveDate.ToString()), true);
			dataAccess.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID, true);
			dataAccess.ExecuteProcedure("CACJ_InsertUpdate");
			if (!dataAccess.EOF)
			{
				this.cacjID = (Validation.IsNumeric(dataAccess["CacjID"]) ? Convert.ToInt32(dataAccess["CacjID"]) : 0);
				this.retrievedata();
			}
		}
	}
}