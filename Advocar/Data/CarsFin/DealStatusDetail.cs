using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.CarsFin
{
	public class DealStatusDetail : Transaction
	{
		private int deasID = 0;

		private int dealID = 0;

		private int cddaIDNew = 0;

		private int cddaIDOld = 0;

		private string notes = string.Empty;

		public int CddaIDNew
		{
			get
			{
				return this.cddaIDNew;
			}
			set
			{
				this.cddaIDNew = value;
			}
		}

		public int CddaIDOld
		{
			get
			{
				return this.cddaIDOld;
			}
			set
			{
				this.cddaIDOld = value;
			}
		}

		public int DealID
		{
			get
			{
				return this.dealID;
			}
			set
			{
				this.dealID = value;
			}
		}

		public int DeasID
		{
			get
			{
				return this.deasID;
			}
			set
			{
				this.deasID = value;
			}
		}

		public string Notes
		{
			get
			{
				return this.notes;
			}
			set
			{
				this.notes = value;
			}
		}

		public DealStatusDetail(string connection, string modifiedUser, int deasID) : base(connection, modifiedUser)
		{
			this.connection = connection;
			this.modifiedUserID = this.modifiedUserID;
			this.deasID = deasID;
			if (this.deasID > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess dataAccess = new DataAccess(this.connection);
			dataAccess.AddParam("@DeasID", DataAccessParameterType.Numeric, this.deasID.ToString());
			dataAccess.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			dataAccess.ExecuteProcedure("DEAS_Delete");
		}

		protected override void retrievedata()
		{
			DataAccess dataAccess = new DataAccess(this.connection);
			dataAccess.AddParam("@DeasID", DataAccessParameterType.Numeric, this.deasID.ToString());
			dataAccess.ExecuteProcedure("DEAS_GetRecord");
			if (!dataAccess.EOF)
			{
				this.deasID = (Validation.IsNumeric(dataAccess["DeasID"]) ? Convert.ToInt32(dataAccess["DeasID"]) : 0);
				this.dealID = (Validation.IsNumeric(dataAccess["DealID"]) ? Convert.ToInt32(dataAccess["DealID"]) : 0);
				this.cddaIDOld = (Validation.IsNumeric(dataAccess["CddaID_Old"]) ? Convert.ToInt32(dataAccess["CddaID_Old"]) : 0);
				this.cddaIDNew = (Validation.IsNumeric(dataAccess["CddaID_New"]) ? Convert.ToInt32(dataAccess["CddaID_New"]) : 0);
				this.notes = dataAccess["Notes"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess dataAccess = new DataAccess(this.connection);
			dataAccess.AddParam("@DeasID", DataAccessParameterType.Numeric, this.deasID.ToString(), true);
			dataAccess.AddParam("@DealID", DataAccessParameterType.Numeric, this.dealID.ToString(), true);
			dataAccess.AddParam("@CddaID_Old", DataAccessParameterType.Numeric, this.cddaIDOld.ToString(), true);
			dataAccess.AddParam("@CddaID_New", DataAccessParameterType.Numeric, this.cddaIDNew.ToString(), true);
			dataAccess.AddParam("@Notes", DataAccessParameterType.Text, this.cddaIDNew.ToString(), true);
			dataAccess.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID, true);
			dataAccess.ExecuteProcedure("DEAS_InsertUpdate");
			if (!dataAccess.EOF)
			{
				this.deasID = (Validation.IsNumeric(dataAccess["DeasID"]) ? Convert.ToInt32(dataAccess["DeasID"]) : 0);
				this.retrievedata();
			}
		}
	}
}