using Advocar.Data;
using System;

namespace Advocar.Data.CarsFin
{
	public class AccountingControlSystem
	{
		private int cacsID = 0;

		private string description = string.Empty;

		public int CacsID
		{
			get
			{
				return this.cacsID;
			}
		}

		public string Description
		{
			get
			{
				return this.description;
			}
		}

		private AccountingControlSystem(int cacsID, string description)
		{
			this.cacsID = cacsID;
			this.description = description;
		}

		public static AccountingControlSystem GetControlSystem(string connection, string description)
		{
			AccountingControlSystem controlSystem = null;
			DataAccess dataAccess = new DataAccess(connection);
			dataAccess.AddParam("@Description", DataAccessParameterType.Text, description);
			dataAccess.ExecuteProcedure("CACS_GetRecord");
			if (!dataAccess.EOF)
			{
				int cacsID = Convert.ToInt32(dataAccess["CacsID"]);
				description = dataAccess["Description"];
				controlSystem = new AccountingControlSystem(cacsID, description);
			}
			return controlSystem;
		}
	}
}