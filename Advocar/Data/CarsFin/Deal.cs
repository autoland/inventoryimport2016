using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.CarsFin
{
	public class Deal : Transaction
	{
		private int dealID = 0;

		private string dealCode = "";

		private int deasID = 0;

		private int gpexID = 0;

		private int deaiID = 0;

		private int acceID = 0;

		private int accoID = 0;

		private int accfID = 0;

		private int accrID = 0;

		private int accaID_Source = 0;

		private int accbID_Lien = 0;

		private int accvID_Dealer = 0;

		private int accdID = 0;

		private int accvID = 0;

		private int acccID = 0;

		private int acccID_CoBuyer = 0;

		private string vin = "";

		private string accmID_StockNo = "";

		private string vehicleYear = "";

		private string vehicleMake = "";

		private string vehicleModel = "";

		private string newUsed = "";

		private string contractDate = "";

		private string lastName = "";

		private string deliveryDate = "";

		private string carSoldYN = "";

		private string vipYN = "";

		private string gpexYN = "";

		private string employeeDealYN = "";

		private string carShowYN = "";

		private string carShowReferralYN = "";

		private string carShowOnSiteYN = "";

		private string deductPacYN = "";

		private float pac = 0f;

		private string splitCommYN = "";

		private int acceID_SplitComm = 0;

		private float vehiclePrice = 0f;

		private float bottomLine = 0f;

		private float vehicleGrossProfit = 0f;

		private float transportationFees = 0f;

		private float docFees = 0f;

		private string deductDocFeesYN = "";

		private float licFees = 0f;

		private string deductLicFeesYN = "";

		private float refundAmount = 0f;

		private float specialCost = 0f;

		private float taxRate = 0f;

		private float taxAmount = 0f;

		private float totalDealAmount = 0f;

		private string countyCode = "";

		private int accmID_CountyQtr = 0;

		private int cddfID = 0;

		private float fundingProgramAmount = 0f;

		private string bonusRateMonth = "";

		private string bonusCalculatedYN = "";

		public string AccaID_Source
		{
			get
			{
				return this.accaID_Source.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.accaID_Source = 0;
				}
				else
				{
					this.accaID_Source = int.Parse(value);
				}
			}
		}

		public string AccbID_Lien
		{
			get
			{
				return this.accbID_Lien.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.accbID_Lien = 0;
				}
				else
				{
					this.accbID_Lien = int.Parse(value);
				}
			}
		}

		public string AcccID
		{
			get
			{
				return this.acccID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.acccID = 0;
				}
				else
				{
					this.acccID = int.Parse(value);
				}
			}
		}

		public string AcccID_CoBuyer
		{
			get
			{
				return this.acccID_CoBuyer.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.acccID_CoBuyer = 0;
				}
				else
				{
					this.acccID_CoBuyer = int.Parse(value);
				}
			}
		}

		public string AccdID
		{
			get
			{
				return this.accdID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.accdID = 0;
				}
				else
				{
					this.accdID = int.Parse(value);
				}
			}
		}

		public string AcceID
		{
			get
			{
				return this.acceID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.acceID = 0;
				}
				else
				{
					this.acceID = int.Parse(value);
				}
			}
		}

		public string AcceID_SplitComm
		{
			get
			{
				return this.acceID_SplitComm.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.acceID_SplitComm = 0;
				}
				else
				{
					this.acceID_SplitComm = int.Parse(value);
				}
			}
		}

		public string AccfID
		{
			get
			{
				return this.accfID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.accfID = 0;
				}
				else
				{
					this.accfID = int.Parse(value);
				}
			}
		}

		public string AccmID_CountyQtr
		{
			get
			{
				return this.accmID_CountyQtr.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.accmID_CountyQtr = 0;
				}
				else
				{
					this.accmID_CountyQtr = int.Parse(value);
				}
			}
		}

		public string AccmID_StockNo
		{
			get
			{
				return this.accmID_StockNo;
			}
			set
			{
				this.accmID_StockNo = value;
			}
		}

		public string AccoID
		{
			get
			{
				return this.accoID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.accoID = 0;
				}
				else
				{
					this.accoID = int.Parse(value);
				}
			}
		}

		public string AccrID
		{
			get
			{
				return this.accrID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.accrID = 0;
				}
				else
				{
					this.accrID = int.Parse(value);
				}
			}
		}

		public string AccvID
		{
			get
			{
				return this.accvID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.accvID = 0;
				}
				else
				{
					this.accvID = int.Parse(value);
				}
			}
		}

		public string AccvID_Dealer
		{
			get
			{
				return this.accvID_Dealer.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.accvID_Dealer = 0;
				}
				else
				{
					this.accvID_Dealer = int.Parse(value);
				}
			}
		}

		public string BonusCalculatedYN
		{
			get
			{
				return this.bonusCalculatedYN;
			}
			set
			{
				this.bonusCalculatedYN = value;
				this.bonusCalculatedYN = (this.bonusCalculatedYN.ToLower() == "y" ? "Y" : "N");
			}
		}

		public string BonusRateMonth
		{
			get
			{
				return this.bonusRateMonth;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.bonusRateMonth = "";
				}
				else
				{
					this.bonusRateMonth = value;
				}
			}
		}

		public float BottomLine
		{
			get
			{
				return this.bottomLine;
			}
			set
			{
				if (!Validation.IsNumeric(value.ToString()))
				{
					this.bottomLine = 0f;
				}
				else
				{
					this.bottomLine = float.Parse(value.ToString());
				}
			}
		}

		public string CarShowOnSiteYN
		{
			get
			{
				return this.carShowOnSiteYN;
			}
			set
			{
				this.carShowOnSiteYN = value;
				this.carShowOnSiteYN = (this.carShowOnSiteYN.ToLower() == "y" ? "Y" : "N");
			}
		}

		public string CarShowReferralYN
		{
			get
			{
				return this.carShowReferralYN;
			}
			set
			{
				this.carShowReferralYN = value;
				this.carShowReferralYN = (this.carShowReferralYN.ToLower() == "y" ? "Y" : "N");
			}
		}

		public string CarShowYN
		{
			get
			{
				return this.carShowYN;
			}
			set
			{
				this.carShowYN = value;
				this.carShowYN = (this.carShowYN.ToLower() == "y" ? "Y" : "N");
			}
		}

		public string CarSoldYN
		{
			get
			{
				return this.carSoldYN;
			}
			set
			{
				this.carSoldYN = value;
				this.carSoldYN = (this.carSoldYN.ToLower() == "y" ? "Y" : "N");
			}
		}

		public string CddfID
		{
			get
			{
				return this.cddfID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.cddfID = 0;
				}
				else
				{
					this.cddfID = int.Parse(value);
				}
			}
		}

		public string ContractDate
		{
			get
			{
				return this.contractDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.contractDate = "";
				}
				else
				{
					this.contractDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CountyCode
		{
			get
			{
				return this.countyCode;
			}
			set
			{
				this.countyCode = value;
			}
		}

		public string DeaiID
		{
			get
			{
				return this.deaiID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.deaiID = 0;
				}
				else
				{
					this.deaiID = int.Parse(value);
				}
			}
		}

		public string DealCode
		{
			get
			{
				return this.dealCode;
			}
			set
			{
				this.dealCode = value;
			}
		}

		public string DealID
		{
			get
			{
				return this.dealID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.dealID = 0;
				}
				else
				{
					this.dealID = int.Parse(value);
				}
			}
		}

		public string DeasID
		{
			get
			{
				return this.deasID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.deasID = 0;
				}
				else
				{
					this.deasID = int.Parse(value);
				}
			}
		}

		public string DeductDocFeesYN
		{
			get
			{
				return this.deductDocFeesYN;
			}
			set
			{
				this.deductDocFeesYN = value;
				this.deductDocFeesYN = (this.deductDocFeesYN.ToLower() == "y" ? "Y" : "N");
			}
		}

		public string DeductLicFeesYN
		{
			get
			{
				return this.deductLicFeesYN;
			}
			set
			{
				this.deductLicFeesYN = value;
				this.deductLicFeesYN = (this.deductLicFeesYN.ToLower() == "y" ? "Y" : "N");
			}
		}

		public string DeductPacYN
		{
			get
			{
				return this.deductPacYN;
			}
			set
			{
				this.deductPacYN = value;
				this.deductPacYN = (this.deductPacYN.ToLower() == "y" ? "Y" : "N");
			}
		}

		public string DeliveryDate
		{
			get
			{
				return this.deliveryDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.deliveryDate = "";
				}
				else
				{
					this.deliveryDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public float DocFees
		{
			get
			{
				return this.docFees;
			}
			set
			{
				if (!Validation.IsNumeric(value.ToString()))
				{
					this.docFees = 0f;
				}
				else
				{
					this.docFees = float.Parse(value.ToString());
				}
			}
		}

		public string EmployeeDealYN
		{
			get
			{
				return this.employeeDealYN;
			}
			set
			{
				this.employeeDealYN = value;
				this.employeeDealYN = (this.employeeDealYN.ToLower() == "y" ? "Y" : "N");
			}
		}

		public float FundingProgramAmount
		{
			get
			{
				return this.fundingProgramAmount;
			}
			set
			{
				if (!Validation.IsNumeric(value.ToString()))
				{
					this.fundingProgramAmount = 0f;
				}
				else
				{
					this.fundingProgramAmount = float.Parse(value.ToString());
				}
			}
		}

		public string GpexID
		{
			get
			{
				return this.gpexID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.gpexID = 0;
				}
				else
				{
					this.gpexID = int.Parse(value);
				}
			}
		}

		public string GpexYN
		{
			get
			{
				return this.gpexYN;
			}
			set
			{
				this.gpexYN = value;
				this.gpexYN = (this.gpexYN.ToLower() == "y" ? "Y" : "N");
			}
		}

		public string LastName
		{
			get
			{
				return this.lastName;
			}
			set
			{
				this.lastName = value;
			}
		}

		public float LicFees
		{
			get
			{
				return this.licFees;
			}
			set
			{
				if (!Validation.IsNumeric(value.ToString()))
				{
					this.licFees = 0f;
				}
				else
				{
					this.licFees = float.Parse(value.ToString());
				}
			}
		}

		public string NewUsed
		{
			get
			{
				return this.newUsed;
			}
			set
			{
				this.newUsed = value;
			}
		}

		public string Pac
		{
			get
			{
				return this.pac.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value.ToString()))
				{
					this.pac = 0f;
				}
				else
				{
					this.pac = float.Parse(value);
				}
			}
		}

		public float RefundAmount
		{
			get
			{
				return this.refundAmount;
			}
			set
			{
				if (!Validation.IsNumeric(value.ToString()))
				{
					this.refundAmount = 0f;
				}
				else
				{
					this.refundAmount = float.Parse(value.ToString());
				}
			}
		}

		public float SpecialCost
		{
			get
			{
				return this.specialCost;
			}
			set
			{
				if (!Validation.IsNumeric(value.ToString()))
				{
					this.specialCost = 0f;
				}
				else
				{
					this.specialCost = float.Parse(value.ToString());
				}
			}
		}

		public string SplitCommYN
		{
			get
			{
				return this.splitCommYN;
			}
			set
			{
				this.splitCommYN = value;
				this.splitCommYN = (this.splitCommYN.ToLower() == "y" ? "Y" : "N");
			}
		}

		public float TaxAmount
		{
			get
			{
				return this.taxAmount;
			}
			set
			{
				if (!Validation.IsNumeric(value.ToString()))
				{
					this.taxAmount = 0f;
				}
				else
				{
					this.taxAmount = float.Parse(value.ToString());
				}
			}
		}

		public float TaxRate
		{
			get
			{
				return this.taxRate;
			}
			set
			{
				if (!Validation.IsNumeric(value.ToString()))
				{
					this.taxRate = 0f;
				}
				else
				{
					this.taxRate = float.Parse(value.ToString());
				}
			}
		}

		public float TotalDealAmount
		{
			get
			{
				return this.totalDealAmount;
			}
			set
			{
				if (!Validation.IsNumeric(value.ToString()))
				{
					this.totalDealAmount = 0f;
				}
				else
				{
					this.totalDealAmount = float.Parse(value.ToString());
				}
			}
		}

		public float TransportationFees
		{
			get
			{
				return this.transportationFees;
			}
			set
			{
				if (!Validation.IsNumeric(value.ToString()))
				{
					this.transportationFees = 0f;
				}
				else
				{
					this.transportationFees = float.Parse(value.ToString());
				}
			}
		}

		public float VehicleGrossProfit
		{
			get
			{
				return this.vehicleGrossProfit;
			}
			set
			{
				if (!Validation.IsNumeric(value.ToString()))
				{
					this.vehicleGrossProfit = 0f;
				}
				else
				{
					this.vehicleGrossProfit = float.Parse(value.ToString());
				}
			}
		}

		public string VehicleMake
		{
			get
			{
				return this.vehicleMake;
			}
			set
			{
				this.vehicleMake = value;
			}
		}

		public string VehicleModel
		{
			get
			{
				return this.vehicleModel;
			}
			set
			{
				this.vehicleModel = value;
			}
		}

		public float VehiclePrice
		{
			get
			{
				return this.vehiclePrice;
			}
			set
			{
				if (!Validation.IsNumeric(value.ToString()))
				{
					this.vehiclePrice = 0f;
				}
				else
				{
					this.vehiclePrice = float.Parse(value.ToString());
				}
			}
		}

		public string VehicleYear
		{
			get
			{
				return this.vehicleYear;
			}
			set
			{
				this.vehicleYear = value;
			}
		}

		public string VIN
		{
			get
			{
				return this.vin;
			}
			set
			{
				this.vin = value;
			}
		}

		public string VipYN
		{
			get
			{
				return this.vipYN;
			}
			set
			{
				this.vipYN = value;
				this.vipYN = (this.vipYN.ToLower() == "y" ? "Y" : "N");
			}
		}

		public Deal()
		{
		}

		public Deal(string connection, string modifiedUserID, int dealID) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.dealID = dealID;
			if (this.dealID > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@DealID", DataAccessParameterType.Numeric, this.dealID.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("DEAL_Delete");
			this.wipeout();
		}

		public static int GetRecordIdFromDealCode(string connectionString, string dealCode)
		{
			int dealId = 0;
			DataAccess data = new DataAccess(connectionString);
			data.AddParam("@DealCode", DataAccessParameterType.Text, dealCode);
			data.ExecuteProcedure("DEAL_GetRecord");
			if (!data.EOF)
			{
				dealId = Convert.ToInt32(data["DealID"]);
			}
			return dealId;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@DealID", DataAccessParameterType.Numeric, this.dealID.ToString());
			data.ExecuteProcedure("DEAL_GetRecord");
			if (!data.EOF)
			{
				this.DealID = data["DealID"];
				this.DealCode = data["DealCode"];
				this.DeasID = data["DeasID"];
				this.DeaiID = data["DeaiID"];
				this.AcceID = data["AcceID"];
				this.GpexID = data["GpexID"];
				this.AccoID = data["AccoID"];
				this.AccfID = data["AccfID"];
				this.AccrID = data["AccrID"];
				this.AccaID_Source = data["AccaID_Source"];
				this.AccbID_Lien = data["AccbID_Lien"];
				this.AccdID = data["AccdID"];
				this.AccvID_Dealer = data["AccvID_Dealer"];
				this.AcccID = data["AcccID"];
				this.AcccID_CoBuyer = data["AcccID_CoBuyer"];
				this.VIN = data["VIN"];
				this.AccmID_StockNo = data["AccmID_StockNo"];
				this.VehicleYear = data["VehicleYear"];
				this.VehicleMake = data["VehicleMake"];
				this.VehicleModel = data["VehicleModel"];
				this.NewUsed = data["NewUsed"];
				this.ContractDate = data["ContractDate"];
				this.LastName = data["LastName"];
				this.DeliveryDate = data["DeliveryDate"];
				this.CarSoldYN = data["CarSoldYN"];
				this.VipYN = data["VIPYN"];
				this.GpexYN = data["GpexYN"];
				this.EmployeeDealYN = data["EmployeeDealYN"];
				this.CarShowYN = data["CarShowYN"];
				this.CarShowReferralYN = data["CarShowReferralYN"];
				this.CarShowOnSiteYN = data["CarShowOnSiteYN"];
				this.DeductPacYN = data["DeductPacYN"];
				this.Pac = data["Pac"];
				this.SplitCommYN = data["SplitCommYN"];
				this.AcceID_SplitComm = data["AcceID_SplitComm"];
				this.VehiclePrice = (Validation.IsNumeric(data["VehiclePrice"]) ? float.Parse(data["VehiclePrice"]) : 0f);
				this.BottomLine = (Validation.IsNumeric(data["BottomLine"]) ? float.Parse(data["BottomLine"]) : 0f);
				this.VehicleGrossProfit = (Validation.IsNumeric(data["VehicleGrossProfit"]) ? float.Parse(data["VehicleGrossProfit"]) : 0f);
				this.TransportationFees = (Validation.IsNumeric(data["TransportationFees"]) ? float.Parse(data["TransportationFees"]) : 0f);
				this.DocFees = (Validation.IsNumeric(data["DocFees"]) ? float.Parse(data["DocFees"]) : 0f);
				this.DeductDocFeesYN = data["DeductDocFeesYN"];
				this.LicFees = (Validation.IsNumeric(data["LicFees"]) ? float.Parse(data["LicFees"]) : 0f);
				this.DeductLicFeesYN = data["DeductLicFeesYN"];
				this.RefundAmount = (Validation.IsNumeric(data["RefundAmount"]) ? float.Parse(data["RefundAmount"]) : 0f);
				this.SpecialCost = (Validation.IsNumeric(data["SpecialCost"]) ? float.Parse(data["SpecialCost"]) : 0f);
				this.TaxRate = (Validation.IsNumeric(data["TaxRate"]) ? float.Parse(data["TaxRate"]) : 0f);
				this.TaxAmount = (Validation.IsNumeric(data["TaxAmount"]) ? float.Parse(data["TaxAmount"]) : 0f);
				this.TotalDealAmount = (Validation.IsNumeric(data["TotalDealAmount"]) ? float.Parse(data["TotalDealAmount"]) : 0f);
				this.CountyCode = data["CountyCode"];
				this.AccmID_CountyQtr = data["AccmID_CountyQtr"];
				this.CddfID = data["CddfID"];
				this.FundingProgramAmount = (Validation.IsNumeric(data["FundingProgramAmount"]) ? float.Parse(data["FundingProgramAmount"]) : 0f);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@DealID", DataAccessParameterType.Numeric, this.DealID);
			data.AddParam("@DealCode", DataAccessParameterType.Text, this.DealCode);
			data.AddParam("@DeasID", DataAccessParameterType.Numeric, this.DeasID, false);
			data.AddParam("@DeaiID", DataAccessParameterType.Numeric, this.DeaiID, false);
			data.AddParam("@AcceID", DataAccessParameterType.Numeric, this.AcceID, false);
			data.AddParam("@AccoID", DataAccessParameterType.Numeric, this.AccoID, false);
			data.AddParam("@AccfID", DataAccessParameterType.Numeric, this.AccfID, false);
			data.AddParam("@AccrID", DataAccessParameterType.Numeric, this.AccrID, false);
			data.AddParam("@AccaID_Source", DataAccessParameterType.Numeric, this.AccaID_Source, false);
			data.AddParam("@AccbID_Lien", DataAccessParameterType.Numeric, this.AccbID_Lien, false);
			data.AddParam("@AccdID", DataAccessParameterType.Numeric, this.AccdID, false);
			data.AddParam("@AccvID_Dealer", DataAccessParameterType.Numeric, this.AccvID_Dealer, false);
			data.AddParam("@AcccID", DataAccessParameterType.Numeric, this.AcccID, false);
			data.AddParam("@GpexID", DataAccessParameterType.Numeric, this.GpexID, false);
			data.AddParam("@AcccID_CoBuyer", DataAccessParameterType.Numeric, this.AcccID_CoBuyer, false);
			data.AddParam("@VIN", DataAccessParameterType.Text, this.VIN);
			data.AddParam("@AccmID_StockNo", DataAccessParameterType.Numeric, this.AccmID_StockNo, false);
			data.AddParam("@VehicleYear", DataAccessParameterType.Text, this.VehicleYear);
			data.AddParam("@VehicleMake", DataAccessParameterType.Text, this.VehicleMake);
			data.AddParam("@VehicleModel", DataAccessParameterType.Text, this.VehicleModel);
			data.AddParam("@NewUsed", DataAccessParameterType.Text, this.NewUsed);
			data.AddParam("@ContractDate", DataAccessParameterType.DateTime, this.ContractDate);
			data.AddParam("@LastName", DataAccessParameterType.Text, this.LastName);
			data.AddParam("@DeliveryDate", DataAccessParameterType.DateTime, this.DeliveryDate);
			data.AddParam("@CarsoldYN", DataAccessParameterType.Text, this.CarSoldYN);
			data.AddParam("@VIPYN", DataAccessParameterType.Text, this.VipYN);
			data.AddParam("@GpexYN", DataAccessParameterType.Text, this.GpexYN);
			data.AddParam("@EmployeeDealYN", DataAccessParameterType.Text, this.EmployeeDealYN);
			data.AddParam("@CarShowYN", DataAccessParameterType.Text, this.CarShowYN);
			data.AddParam("@CarShowReferralYN", DataAccessParameterType.Text, this.CarShowReferralYN);
			data.AddParam("@CarShowOnSiteYN", DataAccessParameterType.Text, this.CarShowOnSiteYN);
			data.AddParam("@DeductPacYN", DataAccessParameterType.Text, this.DeductPacYN);
			data.AddParam("@Pac", DataAccessParameterType.Numeric, this.Pac, false);
			data.AddParam("@SplitCommYN", DataAccessParameterType.Text, this.SplitCommYN);
			data.AddParam("@AcceID_SplitComm", DataAccessParameterType.Numeric, this.AcceID_SplitComm, false);
			float vehiclePrice = this.VehiclePrice;
			data.AddParam("@VehiclePrice", DataAccessParameterType.Numeric, vehiclePrice.ToString(), false);
			vehiclePrice = this.BottomLine;
			data.AddParam("@BottomLine", DataAccessParameterType.Numeric, vehiclePrice.ToString(), false);
			vehiclePrice = this.VehicleGrossProfit;
			data.AddParam("@VehicleGrossProfit", DataAccessParameterType.Numeric, vehiclePrice.ToString(), false);
			vehiclePrice = this.TransportationFees;
			data.AddParam("@TransportationFees", DataAccessParameterType.Numeric, vehiclePrice.ToString(), false);
			vehiclePrice = this.DocFees;
			data.AddParam("@DocFees", DataAccessParameterType.Numeric, vehiclePrice.ToString(), false);
			data.AddParam("@DeductDocFeesYN", DataAccessParameterType.Text, this.DeductDocFeesYN);
			vehiclePrice = this.LicFees;
			data.AddParam("@LicFees", DataAccessParameterType.Numeric, vehiclePrice.ToString(), false);
			data.AddParam("@DeductLicFeesYN", DataAccessParameterType.Text, this.DeductLicFeesYN);
			vehiclePrice = this.RefundAmount;
			data.AddParam("@RefundAmount", DataAccessParameterType.Numeric, vehiclePrice.ToString(), false);
			vehiclePrice = this.SpecialCost;
			data.AddParam("@SpecialCost", DataAccessParameterType.Numeric, vehiclePrice.ToString(), false);
			vehiclePrice = this.TaxRate;
			data.AddParam("@TaxRate", DataAccessParameterType.Numeric, vehiclePrice.ToString(), false);
			vehiclePrice = this.TaxAmount;
			data.AddParam("@TaxAmount", DataAccessParameterType.Numeric, vehiclePrice.ToString(), false);
			vehiclePrice = this.TotalDealAmount;
			data.AddParam("@TotalDealAmount", DataAccessParameterType.Numeric, vehiclePrice.ToString(), false);
			data.AddParam("@CountyCode", DataAccessParameterType.Text, this.CountyCode);
			data.AddParam("@AccmID_CountyQtr", DataAccessParameterType.Numeric, this.AccmID_CountyQtr);
			data.AddParam("@CddfID", DataAccessParameterType.Numeric, this.CddfID);
			vehiclePrice = this.FundingProgramAmount;
			data.AddParam("@FundingProgramAmount", DataAccessParameterType.Numeric, vehiclePrice.ToString(), false);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("DEAL_InsertUpdate");
			if (!data.EOF)
			{
				this.DealID = data["DealID"];
			}
			this.retrievedata();
		}
	}
}