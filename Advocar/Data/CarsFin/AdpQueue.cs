using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;
using System.Collections.Generic;

namespace Advocar.Data.CarsFin
{
	public class AdpQueue : Transaction
	{
		private int adpqID = 0;

		private string fxn = "";

		private string param1 = "";

		private string param2 = "";

		private string param3 = "";

		private string param4 = "";

		private string param5 = "";

		private string status = "";

		public string AdpqID
		{
			get
			{
				return this.adpqID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.adpqID = 0;
				}
				else
				{
					this.adpqID = int.Parse(value);
				}
			}
		}

		public string Fxn
		{
			get
			{
				return this.fxn;
			}
			set
			{
				this.fxn = value;
			}
		}

		public string Param1
		{
			get
			{
				return this.param1;
			}
			set
			{
				this.param1 = value;
			}
		}

		public string Param2
		{
			get
			{
				return this.param2;
			}
			set
			{
				this.param2 = value;
			}
		}

		public string Param3
		{
			get
			{
				return this.param3;
			}
			set
			{
				this.param3 = value;
			}
		}

		public string Param4
		{
			get
			{
				return this.param4;
			}
			set
			{
				this.param4 = value;
			}
		}

		public string Param5
		{
			get
			{
				return this.param5;
			}
			set
			{
				this.param5 = value;
			}
		}

		public string Status
		{
			get
			{
				return this.status;
			}
			set
			{
				this.status = value;
			}
		}

		public AdpQueue()
		{
		}

		public AdpQueue(string connection, string modifiedUserID, int adpqID) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.adpqID = adpqID;
			if (this.adpqID > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@AdpqId", DataAccessParameterType.Numeric, this.AdpqID);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("ADPQ_Delete");
			this.wipeout();
		}

		public void GetNext()
		{
			DataAccess data = new DataAccess(this.connection);
			data.ExecuteProcedure("ADPQ_GetRecordNext");
			if (!data.EOF)
			{
				this.adpqID = int.Parse(data["AdpqID"]);
				this.fxn = data["Fxn"];
				this.param1 = data["Param1"];
				this.param2 = data["Param2"];
				this.param3 = data["Param3"];
				this.param4 = data["Param4"];
				this.param5 = data["Param5"];
				this.status = data["Status"];
			}
		}

		public static AdpQueue[] GetNextList(string connection, string modifiedUser)
		{
			List<AdpQueue> queueList = new List<AdpQueue>();
			DataAccess data = new DataAccess(connection);
			data.ExecuteProcedure("ADPQ_GetListNext");
			while (!data.EOF)
			{
				AdpQueue queueItem = new AdpQueue(connection, modifiedUser, 0)
				{
					adpqID = int.Parse(data["AdpqID"]),
					fxn = data["Fxn"],
					param1 = data["Param1"],
					param2 = data["Param2"],
					param3 = data["Param3"],
					param4 = data["Param4"],
					param5 = data["Param5"],
					status = data["Status"]
				};
				data.MoveNext();
				queueList.Add(queueItem);
			}
			return queueList.ToArray();
		}

		public static int GetRecordId(int adpQueueID, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@AdpqID", DataAccessParameterType.Numeric, adpQueueID.ToString());
			data.ExecuteProcedure("ADPQ_GetRecord");
			num = (!data.EOF ? int.Parse(data["AdpqID"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@AdpqID", DataAccessParameterType.Numeric, this.adpqID.ToString());
			data.ExecuteProcedure("ADPQ_GetRecord");
			if (!data.EOF)
			{
				this.AdpqID = data["AdpqID"];
				this.Fxn = data["Fxn"];
				this.Param1 = data["Param1"];
				this.Param2 = data["Param2"];
				this.Param3 = data["Param3"];
				this.Param4 = data["Param4"];
				this.Param5 = data["Param5"];
				this.Status = data["Status"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@AdpqID", DataAccessParameterType.Numeric, this.AdpqID.ToString());
			data.AddParam("@Fxn", DataAccessParameterType.Text, this.Fxn.ToString());
			data.AddParam("@Param1", DataAccessParameterType.Text, this.Param1.ToString());
			data.AddParam("@Param2", DataAccessParameterType.Text, this.Param2.ToString());
			data.AddParam("@Param3", DataAccessParameterType.Text, this.Param3.ToString());
			data.AddParam("@Param4", DataAccessParameterType.Text, this.Param4.ToString());
			data.AddParam("@Param5", DataAccessParameterType.Text, this.Param5.ToString());
			data.AddParam("@Status", DataAccessParameterType.Text, this.Status.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID.ToString());
			data.ExecuteProcedure("ADPQ_InsertUpdate");
			if (!data.EOF)
			{
				this.AdpqID = data["AdpqID"];
			}
			this.retrievedata();
		}

		public void SetCompleted()
		{
			if (this.adpqID != 0)
			{
				this.UpdateStatus("Completed");
			}
		}

		public void SetPending()
		{
			if (this.adpqID != 0)
			{
				this.UpdateStatus("Pending");
			}
		}

		public void SetProcessing()
		{
			if (this.adpqID != 0)
			{
				this.UpdateStatus("Processing");
			}
		}

		public void UpdateStatus(string status)
		{
			if (this.adpqID != 0)
			{
				string lower = status.ToLower();
				if (lower != null)
				{
					switch (lower)
					{
						case "pending":
						{
							this.Status = status;
							this.Save();
							break;
						}
						case "processing":
						{
							this.Status = status;
							this.Save();
							break;
						}
						case "completed":
						{
							this.Status = status;
							this.Save();
							break;
						}
						case "error1":
						{
							this.Status = status;
							this.Save();
							break;
						}
						case "error2":
						{
							this.Status = status;
							this.Save();
							break;
						}
						case "error3":
						{
							this.Status = status;
							this.Save();
							break;
						}
						default:
						{
							return;
						}
					}
				}
				else
				{
					return;
				}
			}
		}
	}
}