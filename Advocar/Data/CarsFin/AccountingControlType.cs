using Advocar.Data;
using System;

namespace Advocar.Data.CarsFin
{
	public class AccountingControlType
	{
		private int cactID = 0;

		private string description = string.Empty;

		private string tableCode = string.Empty;

		public int CactID
		{
			get
			{
				return this.cactID;
			}
		}

		public string Description
		{
			get
			{
				return this.description;
			}
		}

		public string TableCode
		{
			get
			{
				return this.tableCode;
			}
		}

		private AccountingControlType(int cactID, string description, string tableCode)
		{
			this.cactID = cactID;
			this.description = description;
			this.tableCode = tableCode;
		}

		public static AccountingControlType GetControlType(string connection, string description)
		{
			AccountingControlType controlType = null;
			DataAccess dataAccess = new DataAccess(connection);
			dataAccess.AddParam("@Description", DataAccessParameterType.Text, description);
			dataAccess.ExecuteProcedure("CACT_GetRecord");
			if (!dataAccess.EOF)
			{
				int cactID = Convert.ToInt32(dataAccess["CactID"]);
				description = dataAccess["Description"];
				controlType = new AccountingControlType(cactID, description, dataAccess["TableCode"]);
			}
			return controlType;
		}
	}
}