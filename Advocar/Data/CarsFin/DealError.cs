using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.CarsFin
{
	public class DealError : Transaction
	{
		private int deaeID = 0;

		private int dealID = 0;

		private int cddeID = 0;

		private int deadID = 0;

		private string notes = string.Empty;

		public int CddeID
		{
			get
			{
				return this.cddeID;
			}
			set
			{
				this.cddeID = value;
			}
		}

		public int DeadID
		{
			get
			{
				return this.deadID;
			}
			set
			{
				this.deadID = value;
			}
		}

		public int DeaeID
		{
			get
			{
				return this.deaeID;
			}
			set
			{
				this.deaeID = value;
			}
		}

		public int DealID
		{
			get
			{
				return this.dealID;
			}
			set
			{
				this.dealID = value;
			}
		}

		public string Notes
		{
			get
			{
				return this.notes;
			}
			set
			{
				this.notes = value;
			}
		}

		public DealError(string connection, string modifiedUser, int deaeID)
		{
			this.connection = connection;
			this.modifiedUserID = this.modifiedUserID;
			this.deaeID = deaeID;
			if (this.deaeID > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess dataAccess = new DataAccess(this.connection);
			dataAccess.AddParam("@DeaeID", DataAccessParameterType.Numeric, this.deaeID.ToString());
			dataAccess.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			dataAccess.ExecuteProcedure("DEAE_Delete");
		}

		protected override void retrievedata()
		{
			DataAccess dataAccess = new DataAccess(this.connection);
			dataAccess.AddParam("@DeaeID", DataAccessParameterType.Numeric, this.deaeID.ToString());
			dataAccess.ExecuteProcedure("DEAE_GetRecord");
			if (!dataAccess.EOF)
			{
				this.deaeID = (Validation.IsNumeric(dataAccess["DeaeID"]) ? Convert.ToInt32(dataAccess["DeaeID"]) : 0);
				this.dealID = (Validation.IsNumeric(dataAccess["DealID"]) ? Convert.ToInt32(dataAccess["DealID"]) : 0);
				this.cddeID = (Validation.IsNumeric(dataAccess["CddeID"]) ? Convert.ToInt32(dataAccess["CddeID"]) : 0);
				this.deadID = (Validation.IsNumeric(dataAccess["DeadID"]) ? Convert.ToInt32(dataAccess["DeadID"]) : 0);
				this.notes = dataAccess["Notes"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess dataAccess = new DataAccess(this.connection);
			dataAccess.AddParam("@DeaeID", DataAccessParameterType.Numeric, this.deaeID.ToString());
			dataAccess.AddParam("@DealID", DataAccessParameterType.Numeric, this.dealID.ToString());
			dataAccess.AddParam("@CddeID", DataAccessParameterType.Numeric, this.cddeID.ToString());
			dataAccess.AddParam("@DeadID", DataAccessParameterType.Numeric, this.deadID.ToString());
			dataAccess.AddParam("@Notes", DataAccessParameterType.Text, this.notes);
			dataAccess.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			dataAccess.ExecuteProcedure("DEAE_InsertUpdate");
			if (!dataAccess.EOF)
			{
				this.deaeID = (Validation.IsNumeric(dataAccess["DeaeID"]) ? Convert.ToInt32(dataAccess["DeaeID"]) : 0);
			}
			this.retrievedata();
		}
	}
}