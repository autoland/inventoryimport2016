using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.CarsFin
{
	public class DealErrorStatusDetail : Transaction
	{
		private int deadID = 0;

		private int deaeID = 0;

		private int cddsIdOld = 0;

		private int cddsIdNew = 0;

		private string notes = string.Empty;

		public int CddsIdNew
		{
			get
			{
				return this.cddsIdNew;
			}
			set
			{
				this.cddsIdNew = value;
			}
		}

		public int CddsIdOld
		{
			get
			{
				return this.cddsIdOld;
			}
			set
			{
				this.cddsIdOld = value;
			}
		}

		public int DeadID
		{
			get
			{
				return this.deadID;
			}
			set
			{
				this.deadID = value;
			}
		}

		public int DeaeID
		{
			get
			{
				return this.deaeID;
			}
			set
			{
				this.deaeID = value;
			}
		}

		public string Notes
		{
			get
			{
				return this.notes;
			}
			set
			{
				this.notes = value;
			}
		}

		public DealErrorStatusDetail(string connection, string modifiedUser, int deadID) : base(connection, modifiedUser)
		{
			this.connection = connection;
			this.modifiedUserID = this.modifiedUserID;
			this.deadID = deadID;
			if (this.deadID > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess dataAccess = new DataAccess(this.connection);
			dataAccess.AddParam("@DeadID", DataAccessParameterType.Numeric, this.deadID.ToString());
			dataAccess.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			dataAccess.ExecuteProcedure("DEAD_Delete");
		}

		protected override void retrievedata()
		{
			DataAccess dataAccess = new DataAccess(this.connection);
			dataAccess.AddParam("@DeadID", DataAccessParameterType.Numeric, this.deadID.ToString());
			dataAccess.ExecuteProcedure("DEAD_GetRecord");
			if (!dataAccess.EOF)
			{
				this.deadID = (Validation.IsNumeric(dataAccess["DeadID"]) ? Convert.ToInt32(dataAccess["DeadID"]) : 0);
				this.deaeID = (Validation.IsNumeric(dataAccess["DeaeID"]) ? Convert.ToInt32(dataAccess["DeaeID"]) : 0);
				this.cddsIdNew = (Validation.IsNumeric(dataAccess["CddsID_New"]) ? Convert.ToInt32(dataAccess["CddsID_New"]) : 0);
				this.cddsIdOld = (Validation.IsNumeric(dataAccess["CddsID_Old"]) ? Convert.ToInt32(dataAccess["CddsID_Old"]) : 0);
				this.notes = dataAccess["Notes"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess dataAccess = new DataAccess(this.connection);
			dataAccess.AddParam("@DeadID", DataAccessParameterType.Numeric, this.deadID.ToString());
			dataAccess.AddParam("@DeaeID", DataAccessParameterType.Numeric, this.deaeID.ToString());
			dataAccess.AddParam("@CddsID_Old", DataAccessParameterType.Numeric, this.cddsIdOld.ToString());
			dataAccess.AddParam("@CddsID_New", DataAccessParameterType.Numeric, this.cddsIdNew.ToString());
			dataAccess.AddParam("@Notes", DataAccessParameterType.Text, this.notes);
			dataAccess.ExecuteProcedure("DEAD_InsertUpdate");
			if (!dataAccess.EOF)
			{
				this.deadID = (Validation.IsNumeric(dataAccess["DeadID"]) ? Convert.ToInt32(dataAccess["DeadID"]) : 0);
			}
			this.retrievedata();
		}
	}
}