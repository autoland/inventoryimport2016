using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.CarsFin
{
	public class DealProduct : Transaction
	{
		private int deaqID;

		private int dealID;

		private int cddqID;

		private float salePrice;

		private string topLineYN = string.Empty;

		private string description = string.Empty;

		public int CddqID
		{
			get
			{
				return this.cddqID;
			}
			set
			{
				this.cddqID = value;
			}
		}

		public int DealID
		{
			get
			{
				return this.dealID;
			}
			set
			{
				this.dealID = value;
			}
		}

		public int DeaqID
		{
			get
			{
				return this.deaqID;
			}
			set
			{
				this.deaqID = value;
			}
		}

		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

		public float SalePrice
		{
			get
			{
				return this.salePrice;
			}
			set
			{
				this.salePrice = value;
			}
		}

		public string TopLineYN
		{
			get
			{
				return this.topLineYN;
			}
			set
			{
				this.topLineYN = value;
			}
		}

		public DealProduct(string connection, string modifieduser, int deaqID) : base(connection, modifieduser)
		{
			this.connection = connection;
			this.modifiedUserID = modifieduser;
			this.deaqID = deaqID;
			if (this.deaqID > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess dataAccess = new DataAccess(this.connection);
			dataAccess.AddParam("@DeaqID", DataAccessParameterType.Numeric, this.deaqID.ToString());
			dataAccess.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			dataAccess.ExecuteProcedure("DEAQ_Delete");
		}

		protected override void retrievedata()
		{
			DataAccess dataAccess = new DataAccess(this.connection);
			dataAccess.AddParam("@DeaqID", DataAccessParameterType.Numeric, this.deaqID.ToString());
			dataAccess.ExecuteProcedure("DEAQ_GetRecord");
			if (!dataAccess.EOF)
			{
				this.deaqID = (Validation.IsNumeric(dataAccess["DeaqID"]) ? Convert.ToInt32(dataAccess["DeaqID"]) : 0);
				this.dealID = (Validation.IsNumeric(dataAccess["DealID"]) ? Convert.ToInt32(dataAccess["DealID"]) : 0);
				this.cddqID = (Validation.IsNumeric(dataAccess["CddqID"]) ? Convert.ToInt32(dataAccess["CddqID"]) : 0);
				this.description = dataAccess["Description"];
				this.salePrice = (Validation.IsNumeric(dataAccess["CddqID"]) ? float.Parse(dataAccess["CddqID"]) : 0f);
				this.topLineYN = dataAccess["TopLineYN"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess dataAccess = new DataAccess(this.connection);
			dataAccess.AddParam("@DeaqID", DataAccessParameterType.Numeric, this.deaqID.ToString());
			dataAccess.AddParam("@DealID", DataAccessParameterType.Numeric, this.dealID.ToString());
			dataAccess.AddParam("@CddqID", DataAccessParameterType.Numeric, this.cddqID.ToString());
			dataAccess.AddParam("@SalePrice", DataAccessParameterType.Numeric, this.salePrice.ToString());
			dataAccess.AddParam("@TopLineYN", DataAccessParameterType.Text, this.topLineYN, true);
			dataAccess.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID, true);
			dataAccess.ExecuteProcedure("DEAQ_InsertUpdate");
			if (!dataAccess.EOF)
			{
				this.deaqID = (Validation.IsNumeric(dataAccess["DeaqID"]) ? Convert.ToInt32(dataAccess["DeaqID"]) : 0);
				this.retrievedata();
			}
		}
	}
}