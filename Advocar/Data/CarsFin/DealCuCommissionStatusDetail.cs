using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.CarsFin
{
	public class DealCuCommissionStatusDetail : Transaction
	{
		private int deaiID = 0;

		private int dealID = 0;

		private int cddiIDOld = 0;

		private int cddiIDNew = 0;

		private string notes = string.Empty;

		public int CddiIDNew
		{
			get
			{
				return this.cddiIDNew;
			}
			set
			{
				this.cddiIDNew = value;
			}
		}

		public int CddiIDOld
		{
			get
			{
				return this.cddiIDOld;
			}
			set
			{
				this.cddiIDOld = value;
			}
		}

		public int DeaiID
		{
			get
			{
				return this.deaiID;
			}
			set
			{
				this.deaiID = value;
			}
		}

		public int DealID
		{
			get
			{
				return this.dealID;
			}
			set
			{
				this.dealID = value;
			}
		}

		public string Notes
		{
			get
			{
				return this.notes;
			}
			set
			{
				this.notes = value;
			}
		}

		public DealCuCommissionStatusDetail(string connection, string modifiedUser, int deaiID) : base(connection, modifiedUser)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUser;
			this.deaiID = deaiID;
			if (this.deaiID > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess dataAccess = new DataAccess(this.connection);
			dataAccess.AddParam("@DeaiID", DataAccessParameterType.Numeric, this.deaiID.ToString(), true);
			dataAccess.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID, true);
			dataAccess.ExecuteProcedure("DEAI_Delete    ");
		}

		protected override void retrievedata()
		{
			DataAccess dataAccess = new DataAccess(this.connection);
			dataAccess.AddParam("@DeaiID", DataAccessParameterType.Numeric, this.deaiID.ToString());
			dataAccess.ExecuteProcedure("DEAI_GetRecord");
			if (!dataAccess.EOF)
			{
				this.deaiID = (Validation.IsNumeric(dataAccess["DeaiID"]) ? Convert.ToInt32(dataAccess["DeaiID"]) : 0);
				this.dealID = (Validation.IsNumeric(dataAccess["DealID"]) ? Convert.ToInt32(dataAccess["DealID"]) : 0);
				this.cddiIDOld = (Validation.IsNumeric(dataAccess["CddiID_Old"]) ? Convert.ToInt32(dataAccess["CddiID_Old"]) : 0);
				this.cddiIDNew = (Validation.IsNumeric(dataAccess["CddiID_New"]) ? Convert.ToInt32(dataAccess["CddiID_New"]) : 0);
				this.notes = dataAccess["Notes"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess dataAccess = new DataAccess(this.connection);
			dataAccess.AddParam("@DeaiID", DataAccessParameterType.Numeric, this.deaiID.ToString(), true);
			dataAccess.AddParam("@DealID", DataAccessParameterType.Numeric, this.dealID.ToString(), true);
			dataAccess.AddParam("@CddiID_Old", DataAccessParameterType.Numeric, this.cddiIDOld.ToString(), true);
			dataAccess.AddParam("@CddiID_New", DataAccessParameterType.Numeric, this.cddiIDNew.ToString(), true);
			dataAccess.AddParam("@Notes", DataAccessParameterType.Text, this.notes, true);
			dataAccess.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID, true);
			dataAccess.ExecuteProcedure("DEAI_InsertUpdate");
			if (!dataAccess.EOF)
			{
				this.deaiID = (Validation.IsNumeric(dataAccess["DeaiID"]) ? Convert.ToInt32(dataAccess["DeaiID"]) : 0);
			}
			this.retrievedata();
		}
	}
}