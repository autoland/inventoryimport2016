using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.CarsFin
{
	public class ControlCodesMaster : Transaction
	{
		private int accmID = 0;

		private int cactID = 0;

		private int cacsID = 0;

		private string controlCode = string.Empty;

		public int AccmID
		{
			get
			{
				return this.accmID;
			}
			set
			{
				this.accmID = value;
			}
		}

		public int CacsID
		{
			get
			{
				return this.cacsID;
			}
			set
			{
				this.cacsID = value;
			}
		}

		public int CactID
		{
			get
			{
				return this.cactID;
			}
			set
			{
				this.cactID = value;
			}
		}

		public string ControlCode
		{
			get
			{
				return this.controlCode;
			}
			set
			{
				this.controlCode = value;
			}
		}

		public ControlCodesMaster()
		{
		}

		public ControlCodesMaster(string connection, string modifiedUserID, int accmID) : base(connection, modifiedUserID)
		{
			this.accmID = accmID;
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			if (this.accmID > 0)
			{
				this.retrievedata();
			}
		}

		public ControlCodesMaster(string connection, string modifiedUserID, string controlCode, int cactId, int cacsId) : base(connection, modifiedUserID)
		{
			this.controlCode = controlCode;
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.cactID = cactId;
			this.cacsID = cacsId;
			this.initializeFromControlCode();
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@AccmID", DataAccessParameterType.Numeric, this.AccmID.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("ACCM_Delete");
			this.wipeout();
		}

		private void initializeFromControlCode()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@CacsID", DataAccessParameterType.Numeric, this.cacsID.ToString());
			data.AddParam("@CactID", DataAccessParameterType.Numeric, this.cactID.ToString());
			data.AddParam("@ControlCode", DataAccessParameterType.Text, this.controlCode);
			data.ExecuteProcedure("ACCM_GetRecord");
			if (!data.EOF)
			{
				this.accmID = Convert.ToInt32(data["AccmID"]);
			}
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@AccmID", DataAccessParameterType.Numeric, this.AccmID.ToString());
			data.ExecuteProcedure("ACCM_GetRecord");
			if (!data.EOF)
			{
				this.CacsID = Convert.ToInt32(data["CacsID"]);
				this.CactID = Convert.ToInt32(data["CactID"]);
				this.ControlCode = data["ControlCode"].Trim();
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			int accmID;
			DataAccess data = new DataAccess(this.connection);
			if (this.AccmID > 0)
			{
				accmID = this.AccmID;
				data.AddParam("@AccmID", DataAccessParameterType.Numeric, accmID.ToString());
			}
			if (this.CacsID > 0)
			{
				accmID = this.CacsID;
				data.AddParam("@CacsID", DataAccessParameterType.Numeric, accmID.ToString());
			}
			if (this.CactID > 0)
			{
				accmID = this.CactID;
				data.AddParam("@CactID", DataAccessParameterType.Numeric, accmID.ToString());
			}
			if (!string.IsNullOrEmpty(this.ControlCode))
			{
				data.AddParam("@ControlCode", DataAccessParameterType.Text, this.ControlCode);
			}
			if ((int)data.Parameters.Length > 0)
			{
				data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
				data.ExecuteProcedure("ACCM_InsertUpdate");
				if (!data.EOF)
				{
					this.AccmID = Convert.ToInt32(data["AccmID"]);
				}
			}
			this.retrievedata();
		}
	}
}