using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.CarsFin
{
	public class DealTradeIn : Transaction
	{
		private int deatID = 0;

		private int dealID = 0;

		private int accmID_StockNo = 0;

		private string vehicleMake = string.Empty;

		private string vehicleModel = string.Empty;

		private string vehicleYear = string.Empty;

		private string vin = string.Empty;

		private float allowance = 0f;

		private float cashValue = 0f;

		private float payoffAmount = 0f;

		private string topLineYN = string.Empty;

		private float topLineAmount = 0f;

		public string AccmID_StockNo
		{
			get
			{
				return this.accmID_StockNo.ToString();
			}
			set
			{
				if (Validation.IsNumeric(value))
				{
					this.accmID_StockNo = Convert.ToInt32(value);
				}
			}
		}

		public float Allowance
		{
			get
			{
				return this.allowance;
			}
			set
			{
				this.allowance = value;
			}
		}

		public float CashValue
		{
			get
			{
				return this.cashValue;
			}
			set
			{
				this.cashValue = value;
			}
		}

		public string DealID
		{
			get
			{
				return this.dealID.ToString();
			}
			set
			{
				if (Validation.IsNumeric(value))
				{
					this.dealID = Convert.ToInt32(value);
				}
			}
		}

		public string DeatID
		{
			get
			{
				return this.deatID.ToString();
			}
			set
			{
				if (Validation.IsNumeric(value))
				{
					this.deatID = Convert.ToInt32(value);
				}
			}
		}

		public float PayoffAmount
		{
			get
			{
				return this.payoffAmount;
			}
			set
			{
				this.payoffAmount = value;
			}
		}

		public float TopLineAmount
		{
			get
			{
				return this.topLineAmount;
			}
			set
			{
				this.topLineAmount = value;
			}
		}

		public string TopLineYN
		{
			get
			{
				return this.topLineYN;
			}
			set
			{
				this.topLineYN = value;
			}
		}

		public string VehicleMake
		{
			get
			{
				return this.vehicleMake;
			}
			set
			{
				this.vehicleMake = value;
			}
		}

		public string VehicleModel
		{
			get
			{
				return this.vehicleModel;
			}
			set
			{
				this.vehicleModel = value;
			}
		}

		public string VehicleYear
		{
			get
			{
				return this.vehicleYear;
			}
			set
			{
				this.vehicleYear = value;
			}
		}

		public string Vin
		{
			get
			{
				return this.vin;
			}
			set
			{
				this.vin = value;
			}
		}

		public DealTradeIn(string connection, string modifiedUser, int deatID) : base(connection, modifiedUser)
		{
			this.connection = connection;
			this.modifiedUserID = this.modifiedUserID;
			this.deatID = deatID;
			if (this.deatID > 0)
			{
				this.retrievedata();
			}
		}

		public DealTradeIn(string connection, string modifiedUser, string stockNumber) : base(connection, modifiedUser)
		{
			this.connection = connection;
			this.modifiedUserID = this.modifiedUserID;
			if (!string.IsNullOrEmpty(stockNumber))
			{
				this.retrievedataFromStockNumber(stockNumber);
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@DeatID", DataAccessParameterType.Numeric, this.deatID.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("DEAT_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@DeatID", DataAccessParameterType.Numeric, this.deatID.ToString());
			data.ExecuteProcedure("DEAT_GetRecord");
			if (!data.EOF)
			{
				this.DeatID = data["DeatID"];
				this.DealID = data["DealID"];
				this.AccmID_StockNo = data["AccmID_StockNoTrade"];
				this.VehicleMake = data["VehicleMake"];
				this.VehicleModel = data["VehicleModel"];
				this.VehicleYear = data["VehicleYear"];
				this.Vin = data["VIN"];
				this.Allowance = (Validation.IsNumeric(data["Allowance"]) ? float.Parse(data["Allowance"]) : 0f);
				this.CashValue = (Validation.IsNumeric(data["CashValue"]) ? float.Parse(data["CashValue"]) : 0f);
				this.PayoffAmount = (Validation.IsNumeric(data["PayoffAmount"]) ? float.Parse(data["PayoffAmount"]) : 0f);
				this.TopLineYN = data["TopLineYN"];
				this.TopLineAmount = (Validation.IsNumeric(data["TopLineAmount"]) ? float.Parse(data["TopLineAmount"]) : 0f);
			}
			else
			{
				this.wipeout();
			}
		}

		private void retrievedataFromStockNumber(string stockNumber)
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@StockNoTradeCode", DataAccessParameterType.Numeric, stockNumber);
			data.ExecuteProcedure("DEAT_GetRecord");
			if (!data.EOF)
			{
				this.DeatID = data["DeatID"];
				this.DealID = data["DealID"];
				this.AccmID_StockNo = data["AccmID_StockNoTrade"];
				this.VehicleMake = data["VehicleMake"];
				this.VehicleModel = data["VehicleModel"];
				this.VehicleYear = data["VehicleYear"];
				this.Vin = data["VIN"];
				this.Allowance = (Validation.IsNumeric(data["Allowance"]) ? float.Parse(data["Allowance"]) : 0f);
				this.CashValue = (Validation.IsNumeric(data["CashValue"]) ? float.Parse(data["CashValue"]) : 0f);
				this.PayoffAmount = (Validation.IsNumeric(data["PayoffAmount"]) ? float.Parse(data["PayoffAmount"]) : 0f);
				this.TopLineYN = data["TopLineYN"];
				this.TopLineAmount = (Validation.IsNumeric(data["TopLineAmount"]) ? float.Parse(data["TopLineAmount"]) : 0f);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@DeatID", DataAccessParameterType.Numeric, this.deatID.ToString(), true);
			data.AddParam("@DealID", DataAccessParameterType.Numeric, this.dealID.ToString());
			data.AddParam("@AccmID_StockNoTrade", DataAccessParameterType.Numeric, this.accmID_StockNo.ToString());
			data.AddParam("@VehicleMake", DataAccessParameterType.Text, this.vehicleMake.ToString());
			data.AddParam("@VehicleModel", DataAccessParameterType.Text, this.vehicleModel.ToString());
			data.AddParam("@VehicleYear", DataAccessParameterType.Text, this.vehicleYear.ToString());
			data.AddParam("@VIN", DataAccessParameterType.Text, this.vin.ToString());
			data.AddParam("@Allowance", DataAccessParameterType.Numeric, this.allowance.ToString());
			data.AddParam("@CashValue", DataAccessParameterType.Numeric, this.cashValue.ToString());
			data.AddParam("@PayoffAmount", DataAccessParameterType.Numeric, this.payoffAmount.ToString());
			data.AddParam("@TopLineYN", DataAccessParameterType.Text, this.topLineYN.ToString());
			data.AddParam("@TopLineAmount", DataAccessParameterType.Numeric, this.topLineAmount.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID.ToString());
			data.ExecuteProcedure("DEAT_InsertUpdate");
			if (!data.EOF)
			{
				this.DeatID = data["DeatID"];
			}
			this.retrievedata();
		}
	}
}