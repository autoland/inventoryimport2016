using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.CarsFin
{
	public class DealGLEntry : Transaction
	{
		private int deaaID = 0;

		private int dealID = 0;

		private int cacaID = 0;

		private int cacjID = 0;

		private int accmIdControl1 = 0;

		private string control2 = string.Empty;

		private string postDescription = string.Empty;

		private string reconciliationID = string.Empty;

		private string reference = string.Empty;

		private double postAmount = 0;

		private DateTime postDate = DateTime.MinValue;

		private DateTime accountingDate = DateTime.MinValue;

		private DateTime reconciliationDate = DateTime.MinValue;

		public int AccmIdControl1
		{
			get
			{
				return this.accmIdControl1;
			}
			set
			{
				this.accmIdControl1 = value;
			}
		}

		public DateTime AccountingDate
		{
			get
			{
				return this.accountingDate;
			}
			set
			{
				this.accountingDate = value;
			}
		}

		public int CacaID
		{
			get
			{
				return this.cacaID;
			}
			set
			{
				this.cacaID = value;
			}
		}

		public int CacjID
		{
			get
			{
				return this.cacjID;
			}
			set
			{
				this.cacjID = value;
			}
		}

		public string Control2
		{
			get
			{
				return this.control2;
			}
			set
			{
				this.control2 = value;
			}
		}

		public int DeaaID
		{
			get
			{
				return this.deaaID;
			}
			set
			{
				this.deaaID = value;
			}
		}

		public int DealID
		{
			get
			{
				return this.dealID;
			}
			set
			{
				this.dealID = value;
			}
		}

		public double PostAmount
		{
			get
			{
				return this.postAmount;
			}
			set
			{
				this.postAmount = value;
			}
		}

		public DateTime PostDate
		{
			get
			{
				return this.postDate;
			}
			set
			{
				this.postDate = value;
			}
		}

		public string PostDescription
		{
			get
			{
				return this.postDescription;
			}
			set
			{
				this.postDescription = value;
			}
		}

		public DateTime ReconciliationDate
		{
			get
			{
				return this.reconciliationDate;
			}
			set
			{
				this.reconciliationDate = value;
			}
		}

		public string ReconciliationID
		{
			get
			{
				return this.reconciliationID;
			}
			set
			{
				this.reconciliationID = value;
			}
		}

		public string Reference
		{
			get
			{
				return this.reference;
			}
			set
			{
				this.reference = value;
			}
		}

		public DealGLEntry()
		{
		}

		public DealGLEntry(string connection, string modifiedUserID, int deaaID) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.deaaID = deaaID;
			if (this.deaaID > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@DeaaID", DataAccessParameterType.Numeric, this.DeaaID.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("DEAA_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@DeaaID", DataAccessParameterType.Numeric, this.deaaID.ToString());
			data.ExecuteProcedure("DEAA_GetRecord");
			if (!data.EOF)
			{
				this.DealID = Convert.ToInt32(data["DealID"]);
				this.CacaID = Convert.ToInt32(data["CacaID"]);
				this.CacjID = Convert.ToInt32(data["CacjID"]);
				this.AccmIdControl1 = Convert.ToInt32(data["AccmID_Control1"]);
				this.Reference = data["Reference"];
				this.ReconciliationID = data["ReconciliationID"];
				this.Control2 = data["Control2"];
				this.PostDescription = data["PostDescription"];
				this.PostDate = Convert.ToDateTime(data["PostDate"]);
				this.AccountingDate = Convert.ToDateTime(data["AccountingDate"]);
				this.ReconciliationDate = Convert.ToDateTime(data["ReconciliationDate"]);
				this.postAmount = Convert.ToDouble(data["PostAmount"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			int deaaID;
			DateTime postDate;
			DataAccess data = new DataAccess(this.connection);
			if (this.deaaID > 0)
			{
				deaaID = this.DeaaID;
				data.AddParam("@DeaaID", DataAccessParameterType.Numeric, deaaID.ToString());
			}
			if (this.DealID > 0)
			{
				deaaID = this.DealID;
				data.AddParam("@DealID", DataAccessParameterType.Numeric, deaaID.ToString());
			}
			if (this.CacaID > 0)
			{
				deaaID = this.CacaID;
				data.AddParam("@CacaID", DataAccessParameterType.Numeric, deaaID.ToString());
			}
			if (this.CacjID > 0)
			{
				deaaID = this.CacjID;
				data.AddParam("@CacjID", DataAccessParameterType.Numeric, deaaID.ToString());
			}
			if (this.AccmIdControl1 > 0)
			{
				deaaID = this.AccmIdControl1;
				data.AddParam("@AccmID_Control1", DataAccessParameterType.Numeric, deaaID.ToString());
			}
			if (Math.Abs(this.PostAmount) > 0)
			{
				data.AddParam("@PostAmount", DataAccessParameterType.Numeric, this.postAmount.ToString());
			}
			if (!string.IsNullOrEmpty(this.Reference))
			{
				data.AddParam("@Reference", DataAccessParameterType.Text, this.Reference.ToString());
			}
			if (!string.IsNullOrEmpty(this.Control2))
			{
				data.AddParam("@Control2", DataAccessParameterType.Text, this.Control2.ToString());
			}
			if (!string.IsNullOrEmpty(this.PostDescription))
			{
				data.AddParam("@PostDescription", DataAccessParameterType.Text, this.PostDescription.ToString());
			}
			if (!string.IsNullOrEmpty(this.ReconciliationID))
			{
				data.AddParam("@ReconciliationID", DataAccessParameterType.Text, this.ReconciliationID.ToString());
			}
			if (!this.PostDate.Equals(DateTime.MinValue))
			{
				postDate = this.PostDate;
				data.AddParam("@PostDate", DataAccessParameterType.DateTime, postDate.ToString());
			}
			if (!this.AccountingDate.Equals(DateTime.MinValue))
			{
				postDate = this.AccountingDate;
				data.AddParam("@AccountingDate", DataAccessParameterType.DateTime, postDate.ToString());
			}
			if (!this.ReconciliationDate.Equals(DateTime.MinValue))
			{
				postDate = this.ReconciliationDate;
				data.AddParam("@ReconciliationDate", DataAccessParameterType.DateTime, postDate.ToString());
			}
			if ((int)data.Parameters.Length > 0)
			{
				data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
				data.ExecuteProcedure("DEAA_InsertUpdate");
				if (!data.EOF)
				{
					this.DeaaID = Convert.ToInt32(data["DeaaID"]);
				}
			}
			this.retrievedata();
		}
	}
}