using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.CarsFin
{
	public class GlAccount : Transaction
	{
		private int cacaID = 0;

		private int cacsID = 0;

		private int cacdID = 0;

		private int cactIdControl1 = 0;

		private int cactIdControl2 = 0;

		private string accountCode = string.Empty;

		private string description = string.Empty;

		private DateTime effectiveDate = DateTime.MinValue;

		public string AccountCode
		{
			get
			{
				return this.accountCode;
			}
			set
			{
				this.accountCode = value;
			}
		}

		public int CacaID
		{
			get
			{
				return this.cacaID;
			}
			set
			{
				this.cacaID = value;
			}
		}

		public int CacdID
		{
			get
			{
				return this.cacdID;
			}
			set
			{
				this.cacdID = value;
			}
		}

		public int CacsID
		{
			get
			{
				return this.cacsID;
			}
			set
			{
				this.cacsID = value;
			}
		}

		public int CactIdControl1
		{
			get
			{
				return this.cactIdControl1;
			}
			set
			{
				this.cactIdControl1 = value;
			}
		}

		public int CactIdControl2
		{
			get
			{
				return this.cactIdControl2;
			}
			set
			{
				this.cactIdControl2 = value;
			}
		}

		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

		public DateTime EffectiveDate
		{
			get
			{
				return this.effectiveDate;
			}
			set
			{
				this.effectiveDate = value;
			}
		}

		public GlAccount()
		{
		}

		public GlAccount(string connection, string modifiedUserID, int cacaId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.cacaID = cacaId;
			if (this.cacaID > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@CacaID", DataAccessParameterType.Numeric, this.CacaID.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Numeric, this.modifiedUserID);
			data.ExecuteProcedure("CACA_Delete");
			this.wipeout();
		}

		public static int GetCacaId(string connection, string account, int cacsID)
		{
			int cacaID = 0;
			DataAccess dataAccess = new DataAccess(connection);
			dataAccess.AddParam("@Account", DataAccessParameterType.Text, account);
			dataAccess.AddParam("@CacsID", DataAccessParameterType.Numeric, cacsID.ToString());
			dataAccess.ExecuteProcedure("CACA_GetRecord");
			if (!dataAccess.EOF)
			{
				cacaID = (Validation.IsNumeric(dataAccess["CacaID"]) ? Convert.ToInt32(dataAccess["CacaID"]) : 0);
			}
			return cacaID;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@CacaID", DataAccessParameterType.Numeric, this.CacaID.ToString());
			data.ExecuteProcedure("CACA_GetRecord");
			if (!data.EOF)
			{
				this.CacsID = Convert.ToInt32(data["CacsID"]);
				this.CacdID = Convert.ToInt32(data["CacdID"]);
				this.CactIdControl1 = Convert.ToInt32(data["CactID_Control1"]);
				this.CactIdControl2 = Convert.ToInt32(data["CactID_Control2"]);
				this.accountCode = data["Account"].Trim();
				this.Description = data["CacaDescription"].Trim();
				this.EffectiveDate = Convert.ToDateTime(data["EffectiveDate"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			int cacaID;
			DataAccess data = new DataAccess(this.connection);
			if (this.CacaID > 0)
			{
				cacaID = this.CacaID;
				data.AddParam("@CacaID", DataAccessParameterType.Numeric, cacaID.ToString());
			}
			if (this.CacsID > 0)
			{
				cacaID = this.CacsID;
				data.AddParam("@CacsID", DataAccessParameterType.Numeric, cacaID.ToString());
			}
			if (this.CacdID > 0)
			{
				cacaID = this.CacdID;
				data.AddParam("@CacdID", DataAccessParameterType.Numeric, cacaID.ToString());
			}
			if (this.CactIdControl1 > 0)
			{
				cacaID = this.CactIdControl1;
				data.AddParam("@CactID_Control1", DataAccessParameterType.Numeric, cacaID.ToString());
			}
			if (this.CactIdControl2 > 0)
			{
				cacaID = this.CactIdControl2;
				data.AddParam("@CactID_Control2", DataAccessParameterType.Numeric, cacaID.ToString());
			}
			if (!string.IsNullOrEmpty(this.accountCode))
			{
				data.AddParam("@Account", DataAccessParameterType.Text, this.accountCode.ToString());
			}
			if (!string.IsNullOrEmpty(this.Description))
			{
				data.AddParam("@Account", DataAccessParameterType.Text, this.Description.ToString());
			}
			if (!this.EffectiveDate.Equals(DateTime.MinValue))
			{
				data.AddParam("@EffectiveDate", DataAccessParameterType.DateTime, this.EffectiveDate.ToString());
			}
			if ((int)data.Parameters.Length > 0)
			{
				data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID.ToString());
				data.ExecuteProcedure("CACA_InsertUpdate");
				if (!data.EOF)
				{
					this.CacaID = Convert.ToInt32(data["CacaID"]);
				}
			}
			this.retrievedata();
		}
	}
}