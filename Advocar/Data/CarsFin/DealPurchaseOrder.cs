using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.CarsFin
{
	public class DealPurchaseOrder : Transaction
	{
		private int deapID = 0;

		private int dealID = 0;

		private int accvID = 0;

		private int cddpID = 0;

		private double amount = 0;

		private string deductGPYN = string.Empty;

		private string deductGPCommissionYN = string.Empty;

		private string poNumber = string.Empty;

		private string description = string.Empty;

		public int AccvID
		{
			get
			{
				return this.accvID;
			}
			set
			{
				this.accvID = value;
			}
		}

		public double Amount
		{
			get
			{
				return this.amount;
			}
			set
			{
				this.amount = value;
			}
		}

		public int CddpID
		{
			get
			{
				return this.cddpID;
			}
			set
			{
				this.cddpID = value;
			}
		}

		public int DealID
		{
			get
			{
				return this.dealID;
			}
			set
			{
				this.dealID = value;
			}
		}

		public int DeapID
		{
			get
			{
				return this.deapID;
			}
			set
			{
				this.deapID = value;
			}
		}

		public string DeductGPCommissionYN
		{
			get
			{
				return this.deductGPCommissionYN;
			}
			set
			{
				this.deductGPCommissionYN = value;
			}
		}

		public string DeductGPYN
		{
			get
			{
				return this.deductGPYN;
			}
			set
			{
				this.deductGPYN = value;
			}
		}

		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

		public string PoNumber
		{
			get
			{
				return this.poNumber;
			}
			set
			{
				this.poNumber = value;
			}
		}

		public DealPurchaseOrder()
		{
		}

		public DealPurchaseOrder(string connection, string modifiedUserID, int deapID) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.deapID = deapID;
			if (this.deapID > 0)
			{
				this.retrievedata();
			}
		}

		public DealPurchaseOrder(string connection, string modifiedUserID, string poNumber) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.poNumber = poNumber;
			if (!string.IsNullOrEmpty(this.poNumber))
			{
				this.retrieveDataByPoNumber();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@DeapID", DataAccessParameterType.Numeric, this.DeapID.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("DEAP_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@DeapID", DataAccessParameterType.Numeric, this.DeapID.ToString());
			data.ExecuteProcedure("DEAP_GetRecord");
			if (!data.EOF)
			{
				this.deapID = (Validation.IsNumeric(data["DeapID"]) ? Convert.ToInt32(data["DeapID"]) : 0);
				this.DealID = (Validation.IsNumeric(data["DealID"]) ? Convert.ToInt32(data["DealID"]) : 0);
				this.AccvID = (Validation.IsNumeric(data["AccvID"]) ? Convert.ToInt32(data["AccvID"]) : 0);
				this.CddpID = (Validation.IsNumeric(data["CddpID"]) ? Convert.ToInt32(data["CddpID"]) : 0);
				this.Amount = (Validation.IsNumeric(data["Amount"]) ? Convert.ToDouble(data["Amount"]) : 0);
				this.PoNumber = data["PONumber"];
				this.Description = data["Description"];
				this.DeductGPYN = data["DeductGPYN"];
				this.DeductGPCommissionYN = data["DeductGPCommissionYN"];
			}
			else
			{
				this.wipeout();
			}
		}

		private void retrieveDataByPoNumber()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@PONumber", DataAccessParameterType.Text, this.poNumber.ToString());
			data.ExecuteProcedure("DEAP_GetRecord");
			if (!data.EOF)
			{
				this.deapID = (Validation.IsNumeric(data["DeapID"]) ? Convert.ToInt32(data["DeapID"]) : 0);
				this.DealID = (Validation.IsNumeric(data["DealID"]) ? Convert.ToInt32(data["DealID"]) : 0);
				this.AccvID = (Validation.IsNumeric(data["AccvID"]) ? Convert.ToInt32(data["AccvID"]) : 0);
				this.CddpID = (Validation.IsNumeric(data["CddpID"]) ? Convert.ToInt32(data["CddpID"]) : 0);
				this.Amount = (Validation.IsNumeric(data["Amount"]) ? Convert.ToDouble(data["Amount"]) : 0);
				this.PoNumber = data["PONumber"];
				this.Description = data["Description"];
				this.DeductGPYN = data["DeductGPYN"];
				this.DeductGPCommissionYN = data["DeductGPCommissionYN"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			int deapID;
			DataAccess data = new DataAccess(this.connection);
			if (this.DeapID > 0)
			{
				deapID = this.DeapID;
				data.AddParam("@DeapID", DataAccessParameterType.Numeric, deapID.ToString());
			}
			if (this.DealID > 0)
			{
				deapID = this.DealID;
				data.AddParam("@DealID", DataAccessParameterType.Numeric, deapID.ToString());
			}
			if (this.AccvID > 0)
			{
				deapID = this.AccvID;
				data.AddParam("@AccvID", DataAccessParameterType.Numeric, deapID.ToString());
			}
			if (this.CddpID > 0)
			{
				deapID = this.CddpID;
				data.AddParam("@CddpID", DataAccessParameterType.Numeric, deapID.ToString());
			}
			if (this.Amount > 0)
			{
				data.AddParam("@Amount", DataAccessParameterType.Numeric, this.Amount.ToString());
			}
			if (!string.IsNullOrEmpty(this.DeductGPYN))
			{
				data.AddParam("@DeductGPYN", DataAccessParameterType.Text, this.DeductGPYN);
			}
			if (!string.IsNullOrEmpty(this.DeductGPCommissionYN))
			{
				data.AddParam("@DeductGPCommissionYN", DataAccessParameterType.Text, this.DeductGPCommissionYN);
			}
			if (!string.IsNullOrEmpty(this.PoNumber))
			{
				data.AddParam("@PONumber", DataAccessParameterType.Text, this.PoNumber);
			}
			if (!string.IsNullOrEmpty(this.Description))
			{
				data.AddParam("@Description", DataAccessParameterType.Text, this.Description);
			}
			if ((int)data.Parameters.Length > 0)
			{
				data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
				data.ExecuteProcedure("DEAP_InsertUpdate");
				if (!data.EOF)
				{
					this.DeapID = Convert.ToInt32(data["DeapID"]);
				}
			}
			this.retrievedata();
		}
	}
}