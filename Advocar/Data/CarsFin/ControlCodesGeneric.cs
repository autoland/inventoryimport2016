using Advocar.Data;
using Advocar.Interface;
using System;
using System.Collections.Generic;

namespace Advocar.Data.CarsFin
{
	public class ControlCodesGeneric : Transaction
	{
		private readonly static Dictionary<string, string> TABLE_ACRONYM_MAP;

		private string carsfinTableAcronym = string.Empty;

		private string carsTableAcronym = string.Empty;

		private int accxId;

		private int accmId;

		private int carsId;

		private string description = string.Empty;

		private string controlCode = string.Empty;

		public int AccmId
		{
			get
			{
				return this.accmId;
			}
			set
			{
				this.accmId = value;
			}
		}

		public int AccxId
		{
			get
			{
				return this.accxId;
			}
			set
			{
				this.accxId = value;
			}
		}

		public string CarsfinTableAcronym
		{
			get
			{
				return this.carsfinTableAcronym;
			}
			set
			{
				this.carsfinTableAcronym = value;
			}
		}

		public int CarsId
		{
			get
			{
				return this.carsId;
			}
			set
			{
				this.carsId = value;
			}
		}

		public string CarsTableAcronym
		{
			get
			{
				return this.carsTableAcronym;
			}
			set
			{
				this.carsTableAcronym = value;
			}
		}

		public string ControlCode
		{
			get
			{
				return this.controlCode;
			}
			set
			{
				this.controlCode = value;
			}
		}

		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

		static ControlCodesGeneric()
		{
			ControlCodesGeneric.TABLE_ACRONYM_MAP = ControlCodesGeneric.BuildDictionary();
		}

		public ControlCodesGeneric(string connectionString, string modifiedUser, string carsfinTableAcronym, int accxId) : base(connectionString, modifiedUser)
		{
			this.carsfinTableAcronym = carsfinTableAcronym;
			this.carsTableAcronym = ControlCodesGeneric.TABLE_ACRONYM_MAP[this.carsfinTableAcronym];
			this.accxId = accxId;
			if (this.accxId > 0)
			{
				this.retrievedata();
			}
		}

		public ControlCodesGeneric(string connectionString, string modifiedUser, int accmId, string carsfinTableAcronym) : base(connectionString, modifiedUser)
		{
			this.carsfinTableAcronym = carsfinTableAcronym;
			this.carsTableAcronym = ControlCodesGeneric.TABLE_ACRONYM_MAP[this.carsfinTableAcronym];
			this.accmId = accmId;
			if (this.accmId > 0)
			{
				this.initializeFromAccmId();
			}
		}

		private static Dictionary<string, string> BuildDictionary()
		{
			Dictionary<string, string> dict = new Dictionary<string, string>()
			{
				{ "ACCA", "Agra" },
				{ "ACCB", "Agrb" },
				{ "ACCC", "Memb" },
				{ "ACCD", "Dlra" },
				{ "ACCE", "Empl" },
				{ "ACCF", "Cmed" },
				{ "ACCR", "Cmer" },
				{ "ACCV", "Cdcv" },
				{ "ACCO", "Cmeo" }
			};
			return dict;
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam(string.Concat("@", this.carsfinTableAcronym, "ID"), DataAccessParameterType.Numeric, this.accxId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure(string.Concat(this.carsfinTableAcronym.ToUpper(), "_Delete"));
		}

		private void initializeFromAccmId()
		{
			if (this.accmId != 0)
			{
				DataAccess data = new DataAccess(this.connection);
				data.AddParam("@AccmID", DataAccessParameterType.Numeric, this.accmId.ToString());
				data.ExecuteProcedure(string.Concat(this.carsfinTableAcronym, "_GetRecord"));
				if (!data.EOF)
				{
					this.accxId = Convert.ToInt32(data[0]);
					this.carsId = Convert.ToInt32(data[string.Concat(this.carsTableAcronym, "ID")]);
					this.controlCode = data["ControlCode"];
				}
			}
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam(string.Concat("@", this.carsfinTableAcronym, "ID"), DataAccessParameterType.Numeric, this.accxId.ToString());
			data.ExecuteProcedure(string.Concat(this.carsfinTableAcronym.ToUpper(), "_GetRecord"));
			if (!data.EOF)
			{
				this.accmId = Convert.ToInt32(data["AccmID"]);
				this.carsId = Convert.ToInt32(data[string.Concat(this.carsTableAcronym, "ID")]);
				this.controlCode = data["ControlCode"];
				this.description = data["Description"];
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam(string.Concat("@", this.carsfinTableAcronym, "ID"), DataAccessParameterType.Numeric, this.accxId.ToString());
			data.AddParam("@AccmID", DataAccessParameterType.Numeric, this.accmId.ToString());
			data.AddParam(string.Concat("@", this.carsTableAcronym, "ID"), DataAccessParameterType.Numeric, this.carsId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure(string.Concat(this.carsfinTableAcronym.ToUpper(), "_InsertUpdate"));
			this.accxId = Convert.ToInt32(data[string.Concat(this.carsfinTableAcronym, "ID")]);
		}
	}
}