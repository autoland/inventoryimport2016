using Advocar.Tools;
using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Advocar.Data
{
	public class DataFlatFile
	{
		public string FileName = "";

		public string Delimiter = "";

		public bool IsFirstRowHeading = false;

		private string[,] data = new string[0, 0];

		public string[] Row = new string[0];

		private int currentRecord = 0;

		public int FieldCount = 0;

		private bool eof = false;

		public int CurrentRecord
		{
			get
			{
				return this.currentRecord;
			}
		}

		public bool EOF
		{
			get
			{
				return this.eof;
			}
		}

		public string this[int index]
		{
			get
			{
				string str;
				if (!this.eof)
				{
					str = this.data[this.currentRecord, index];
				}
				else
				{
					str = null;
				}
				return str;
			}
		}

		public string this[string name]
		{
			get
			{
				int fieldNo = -1;
				for (int index = 0; this.FieldCount > index; index++)
				{
					if (this.data[0, index].ToLower() == name.ToLower())
					{
						fieldNo = index;
					}
				}
				return this[fieldNo];
			}
		}

		public int RecordCount
		{
			get
			{
				return (int)this.Row.Length;
			}
		}

		public DataFlatFile(string fileName, string delimiter, bool isFirstRowHeading)
		{
			this.FileName = fileName;
			this.Delimiter = delimiter;
			this.IsFirstRowHeading = isFirstRowHeading;
			this.Load();
			this.currentRecord = 0;
		}

		public void Load()
		{
			int rowIndex;
			int fieldIndex;
			StreamReader file = new StreamReader(this.FileName);
			this.Row = Regex.Split(file.ReadToEnd().Replace("\r\n", "\n"), "[\\u000a]");
			string regex = ",(?=([^\"]*\"[^\"]*\")*(?![^\"]*\"))";
			if (this.Delimiter != ",")
			{
				regex = string.Concat("[", this.Delimiter, "]");
			}
			if (this.Delimiter == "\t")
			{
				regex = this.Delimiter;
			}
			Regex field = new Regex(regex);
			this.FieldCount = 0;
			for (int index = 0; (int)this.Row.Length > index; index++)
			{
				int fieldMatches = field.Matches(this.Row[index]).Count + 1;
				this.FieldCount = (this.FieldCount < fieldMatches ? fieldMatches : this.FieldCount);
			}
			int rowCount = (int)this.Row.Length;
			this.data = new string[rowCount, this.FieldCount];
			for (rowIndex = 0; rowCount > rowIndex; rowIndex++)
			{
				fieldIndex = 0;
				int start = 0;
				string line = this.Row[rowIndex];
				foreach (Match match in field.Matches(line))
				{
					this.data[rowIndex, fieldIndex] = line.Substring(start, match.Index - start).Trim("\"".ToCharArray());
					start = match.Index + 1;
					fieldIndex++;
				}
				this.data[rowIndex, fieldIndex] = line.Substring(start, line.Length - start).Trim("\"".ToCharArray());
			}
			for (rowIndex = 0; (int)this.Row.Length > rowIndex; rowIndex++)
			{
				for (fieldIndex = 0; this.FieldCount > fieldIndex; fieldIndex++)
				{
					if (Validation.IsNull(this.data[rowIndex, fieldIndex]))
					{
						this.data[rowIndex, fieldIndex] = "";
					}
				}
			}
		}

		public virtual void MoveNext()
		{
			this.MoveNext(1);
		}

		public virtual void MoveNext(int count)
		{
			this.currentRecord = this.currentRecord + count;
			if (this.currentRecord >= (int)this.Row.Length)
			{
				this.eof = true;
			}
		}
	}
}