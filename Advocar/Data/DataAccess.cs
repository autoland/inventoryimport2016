using Advocar.Tools;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;

namespace Advocar.Data
{
	public class DataAccess
	{
		public string Server = "";

		public string Database = "";

		public string DbUser = "";

		public string DbPassword = "";

		private SqlConnection connection = new SqlConnection();

		private SqlDataReader recordset;

		public DataAccessParameter[] Parameters = new DataAccessParameter[0];

		private string query = "";

		private string procedure;

		private int timeout = 90;

		private int recordCount = 0;

		private bool eof = false;

		public string Connection
		{
			get
			{
				return this.connection.ConnectionString;
			}
			set
			{
				this.connection.ConnectionString = value;
			}
		}

		public string ConnectionString
		{
			get
			{
				string[] dbUser = new string[] { "User ID=", this.DbUser, ";password=", this.DbPassword, ";Initial Catalog=", this.Database, ";Data Source=", this.Server, ";" };
				return string.Concat(dbUser);
			}
		}

		public bool EOF
		{
			get
			{
				return this.eof;
			}
		}

		public string this[int index]
		{
			get
			{
				string str;
				if (!this.eof)
				{
					str = this.recordset[index].ToString();
				}
				else
				{
					str = null;
				}
				return str;
			}
		}

		public string this[string name]
		{
			get
			{
				string str;
				if (!this.eof)
				{
					str = this.recordset[name].ToString();
				}
				else
				{
					str = null;
				}
				return str;
			}
		}

		public string Procedure
		{
			get
			{
				return this.procedure;
			}
			set
			{
				this.procedure = value;
			}
		}

		public string Query
		{
			get
			{
				if (this.query.Length == 0)
				{
					this.CreateQueryString();
				}
				return this.query;
			}
			set
			{
				this.query = value;
			}
		}

		public int RecordCount
		{
			get
			{
				return this.recordCount;
			}
		}

		public int Timeout
		{
			get
			{
				return this.timeout;
			}
			set
			{
				this.timeout = value;
			}
		}

		public DataAccess()
		{
		}

		public DataAccess(string connection)
		{
			this.Connection = connection;
			this.Server = DataAccess.GetServer(connection);
			this.Database = DataAccess.GetDatabase(connection);
			this.DbUser = DataAccess.GetUser(connection);
			this.DbPassword = DataAccess.GetPassword(connection);
		}

		public DataAccess(string connection, string procedure)
		{
			this.Connection = connection;
			this.Procedure = procedure;
			this.Server = DataAccess.GetServer(connection);
			this.Database = DataAccess.GetDatabase(connection);
			this.DbUser = DataAccess.GetUser(connection);
			this.DbPassword = DataAccess.GetPassword(connection);
		}

		public DataAccess(string server, string database, string user, string password)
		{
			this.Server = server;
			this.Database = database;
			this.DbUser = user;
			this.DbPassword = password;
			string[] strArrays = new string[] { "User ID=", user, ";password=", password, ";Initial Catalog=", database, ";Data Source=", server, ";" };
			this.Connection = string.Concat(strArrays);
		}

		public DataAccess(string server, string database, string user, string password, string procedure)
		{
			this.Server = server;
			this.Database = database;
			this.DbUser = user;
			this.DbPassword = password;
			string[] strArrays = new string[] { "User ID=", user, ";password=", password, ";Initial Catalog=", database, ";Data Source=", server, ";" };
			this.Connection = string.Concat(strArrays);
			this.Procedure = procedure;
		}

		public void AddParam(string name, float value)
		{
			this.AddParam(name, DataAccessParameterType.Numeric, value.ToString(), false);
		}

		public void AddParam(string name, string value)
		{
			this.AddParam(name, DataAccessParameterType.Text, value, false);
		}

		public void AddParam(string name, DateTime value)
		{
			this.AddParam(name, DataAccessParameterType.DateTime, value.ToString(), false);
		}

		public void AddParam(string name, DataAccessParameterType type, string value)
		{
			this.AddParam(name, type, value, false);
		}

		public void AddParam(string name, DataAccessParameterType type, string value, bool nullifblank)
		{
			int paramID = -1;
			if (Validation.IsNull(value))
			{
				value = "";
			}
			if ((type != DataAccessParameterType.DateTime || Validation.IsDate(value) ? false : value != "NULL"))
			{
				value = "";
			}
			if ((type != DataAccessParameterType.Numeric ? false : !Validation.IsNumeric(value)))
			{
				value = "0";
			}
			if (type == DataAccessParameterType.Text)
			{
				value = value.Replace("'", "''");
			}
			if (nullifblank)
			{
				if ((Validation.IsNull(value) || value.Length == 0 ? true : value == "0"))
				{
					value = "NULL";
					type = DataAccessParameterType.Numeric;
				}
			}
			int count = (int)this.Parameters.Length;
			int row = 0;
			while (row < count)
			{
				if (!(this.Parameters[row].Name.ToLower() == name.ToLower()))
				{
					row++;
				}
				else
				{
					paramID = row;
					break;
				}
			}
			if (paramID != -1)
			{
				this.Parameters[paramID].Value = value;
			}
			else
			{
				count++;
				paramID = count - 1;
				DataAccessParameter param = new DataAccessParameter(name, value, type);
				DataAccessParameter[] arrTemp = new DataAccessParameter[(int)this.Parameters.Length + 1];
				this.Parameters.CopyTo(arrTemp, 0);
				this.Parameters = new DataAccessParameter[count];
				arrTemp.CopyTo(this.Parameters, 0);
				this.Parameters.SetValue(param, count - 1);
			}
		}

		public virtual string AssignValue(string fieldString)
		{
			string item;
			if (fieldString.Length != 0)
			{
				if (this.FieldID(fieldString.Replace("{", "").Replace("}", "")) >= 0)
				{
					if (this[fieldString.Replace("{", "").Replace("}", "")] != null)
					{
						item = this[fieldString.Replace("{", "").Replace("}", "")];
						return item;
					}
				}
				int start = 0;
				int end = 0;
				string field = "";
				string fieldName = "";
				string fieldFormat = "";
				string fieldValue = "";
				string temp = fieldString;
				bool loop = true;
				while (loop)
				{
					field = "";
					fieldFormat = "";
					start = temp.IndexOf("{");
					end = temp.IndexOf("}");
					if (end > start)
					{
						fieldName = temp.Substring(start, end - start + 1);
						field = fieldName.Replace("{", "").Replace("}", "");
						start = field.IndexOf("(");
						end = field.IndexOf(")");
						if ((end <= start ? false : start >= 0))
						{
							fieldFormat = field.Substring(start, end - start + 1).Replace("(", "").Replace(")", "");
							field = field.Remove(start, end - start + 1);
						}
						fieldValue = (this.FieldID(field) >= 0 ? this[field] : field);
						if (fieldFormat.Length > 0)
						{
							if (fieldFormat == "boolean")
							{
								fieldValue = (fieldValue == "True" ? "*" : fieldValue);
								fieldValue = (fieldValue == "1" ? "*" : fieldValue);
								fieldValue = (fieldValue == "Y" ? "*" : fieldValue);
								fieldValue = (fieldValue == "Yes" ? "*" : fieldValue);
								if (fieldValue != "*")
								{
									fieldValue = "";
								}
							}
							else if (Validation.IsNumeric(fieldValue))
							{
								fieldValue = float.Parse(fieldValue).ToString(fieldFormat);
							}
							else if (Validation.IsDate(fieldValue))
							{
								fieldValue = DateTime.Parse(fieldValue).ToString(fieldFormat);
							}
							else if (fieldFormat == "formatPhone")
							{
								fieldValue = Validation.PhoneFormat(fieldValue);
							}
						}
						temp = temp.Replace(fieldName, fieldValue);
					}
					else
					{
						break;
					}
				}
				item = temp;
			}
			else
			{
				item = "";
			}
			return item;
		}

		public virtual void ClearParams()
		{
			this.Parameters = null;
			this.Parameters = new DataAccessParameter[0];
		}

		public string CreateQueryString()
		{
			return this.CreateQueryString("");
		}

		public string CreateQueryString(string procedure)
		{
			if (procedure.Length > 0)
			{
				this.Procedure = procedure;
			}
			procedure = this.Procedure;
			string strname = "";
			string query = string.Concat("EXEC ", procedure);
			for (int row = 0; row < (int)this.Parameters.Length; row++)
			{
				if (row > 0)
				{
					query = string.Concat(query, ",");
				}
				strname = (this.Parameters[row].Name.Length == 0 ? "" : string.Concat(this.Parameters[row].Name, " = "));
				switch (this.Parameters[row].Type)
				{
					case DataAccessParameterType.Text:
					{
						string[] value = new string[] { query, " ", strname, "'", this.Parameters[row].Value, "'" };
						query = string.Concat(value);
						break;
					}
					case DataAccessParameterType.Numeric:
					{
						query = string.Concat(query, " ", strname, this.Parameters[row].Value);
						break;
					}
					case DataAccessParameterType.DateTime:
					{
						goto case DataAccessParameterType.Text;
					}
					case DataAccessParameterType.Bool:
					{
						query = string.Concat(query, " ", strname, (this.Parameters[row].Value.ToLower() == "true" || this.Parameters[row].Value == "1" ? "1" : "0"));
						break;
					}
				}
			}
			return query;
		}

		public virtual void ExecuteProcedure(string procedure)
		{
			this.ExecuteProcedure(procedure, true);
		}

		public virtual void ExecuteProcedure(string procedure, bool clearParams)
		{
			SqlCommand command = new SqlCommand();
			try
			{
				this.query = this.CreateQueryString(procedure);
				if (clearParams)
				{
					this.ClearParams();
				}
				command.Connection = this.connection;
				command.CommandType = CommandType.Text;
				command.CommandText = this.query;
				command.CommandTimeout = this.timeout;
				try
				{
					command.Connection.Open();
				}
				catch
				{
					command.Connection.Close();
					command.Connection.Open();
				}
				this.recordset = command.ExecuteReader();
				this.MoveNext();
				this.recordCount = this.setRecordCount();
				GC.Collect();
			}
			catch (SqlException sqlException)
			{
				SqlException err = sqlException;
				throw new Advocar.Tools.Exception(err, "Advocar.Data.DataAccess.ExecuteProcedure", string.Concat(err.Message, " ", this.query));
			}
			catch (System.Exception exception)
			{
				System.Exception err = exception;
				throw new Advocar.Tools.Exception(err, "Advocar.Data.DataAccess.ExecuteProcedure", string.Concat(err.Message, " ", this.query));
			}
		}

		public virtual void ExecuteStatement(string query)
		{
			SqlCommand command = new SqlCommand();
			this.ClearParams();
			this.query = query;
			try
			{
				if (this.connection.State != ConnectionState.Closed)
				{
					this.connection.Close();
				}
				this.connection.Open();
				command.Connection = this.connection;
				command.CommandType = CommandType.Text;
				command.CommandText = query;
				command.CommandTimeout = this.timeout;
				this.recordset = command.ExecuteReader();
				this.MoveNext();
				this.recordCount = this.setRecordCount();
				GC.Collect();
			}
			catch (SqlException sqlException)
			{
				SqlException err = sqlException;
				throw new Advocar.Tools.Exception(err, "Advocar.Data.DataAccess.ExecuteProcedure", string.Concat(err.Message, " ", query));
			}
			catch (System.Exception exception)
			{
				System.Exception err = exception;
				throw new Advocar.Tools.Exception(err, "Advocar.Data.DataAccess.ExecuteProcedure", string.Concat(err.Message, " ", query));
			}
		}

		public int FieldID(string name)
		{
			int id;
			try
			{
				id = this.recordset.GetOrdinal(name);
			}
			catch
			{
				id = -1;
			}
			return id;
		}

		public string FieldName(int id)
		{
			string name;
			try
			{
				name = this.recordset.GetName(id);
			}
			catch
			{
				name = "";
			}
			return name;
		}

		~DataAccess()
		{
			this.connection = null;
			this.recordset = null;
			this.Parameters = null;
		}

		public static string GetConnectionString(string server, string database, string user, string password)
		{
			string[] strArrays = new string[] { "User ID=", user, ";password=", password, ";Initial Catalog=", database, ";Data Source=", server, ";" };
			return string.Concat(strArrays);
		}

		public static string GetDatabase(string connection)
		{
			string str;
			int start = connection.LastIndexOf("Initial Catalog=") + 16;
			if (start >= 16)
			{
				int end = connection.IndexOf(";", start);
				try
				{
					str = connection.Substring(start, end - start);
				}
				catch
				{
					str = "";
				}
			}
			else
			{
				str = "";
			}
			return str;
		}

		public static string GetPassword(string connection)
		{
			string str;
			int start = connection.LastIndexOf("password=") + 9;
			if (start >= 9)
			{
				int end = connection.IndexOf(";", start);
				try
				{
					str = connection.Substring(start, end - start);
				}
				catch
				{
					str = "";
				}
			}
			else
			{
				str = "";
			}
			return str;
		}

		public static string GetServer(string connection)
		{
			string str;
			int start = connection.IndexOf("Data Source=") + 12;
			if (start >= 12)
			{
				int end = connection.IndexOf(";", start);
				try
				{
					str = connection.Substring(start, end - start);
				}
				catch
				{
					str = "";
				}
			}
			else
			{
				str = "";
			}
			return str;
		}

		public static string GetUser(string connection)
		{
			string str;
			int start = connection.LastIndexOf("User ID=") + 8;
			if (start >= 8)
			{
				int end = connection.IndexOf(";", start);
				try
				{
					str = connection.Substring(start, end - start);
				}
				catch
				{
					str = "";
				}
			}
			else
			{
				str = "";
			}
			return str;
		}

		public virtual void MoveNext(int count)
		{
			for (int row = 1; count >= row; row++)
			{
				this.recordset.Read();
			}
		}

		public virtual void MoveNext()
		{
			this.eof = !this.recordset.Read();
		}

		private int setRecordCount()
		{
			int recordsAffected;
			if (this.recordset.RecordsAffected <= 0)
			{
				if (this.recordset.FieldCount > 0)
				{
					int fieldCount = this.recordset.FieldCount;
					if (this.recordset.GetName(fieldCount - 1) == "RecordCount")
					{
						recordsAffected = (this.EOF ? 0 : int.Parse(this.recordset["RecordCount"].ToString()));
						return recordsAffected;
					}
				}
				recordsAffected = 0;
			}
			else
			{
				recordsAffected = this.recordset.RecordsAffected;
			}
			return recordsAffected;
		}

		public override string ToString()
		{
			return this.query;
		}
	}
}