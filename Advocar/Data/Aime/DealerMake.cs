using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class DealerMake : Transaction
	{
		private int dealerMakeId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int dealerId = 0;

		private int configuratorAd_VehicleMakeId = 0;

		private int territoryId = 0;

		public int ConfiguratorAd_VehicleMakeId
		{
			get
			{
				return this.configuratorAd_VehicleMakeId;
			}
			set
			{
				this.configuratorAd_VehicleMakeId = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public int DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public int DealerMakeId
		{
			get
			{
				return this.dealerMakeId;
			}
			set
			{
				this.dealerMakeId = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public int TerritoryId
		{
			get
			{
				return this.territoryId;
			}
			set
			{
				this.territoryId = value;
			}
		}

		public DealerMake()
		{
		}

		public DealerMake(string connection, string modifiedUserID, int dealerMakeId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "DEALER_MAKE";
			this.DealerMakeId = dealerMakeId;
			if (this.DealerMakeId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@DealerMakeId", DataAccessParameterType.Numeric, this.DealerMakeId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("DEALER_MAKE_Delete");
			this.wipeout();
		}

		public static int GetRecordId(int vehicleMakeId, int territoryId, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@ConfiguratorAd_VehicleMakeId", DataAccessParameterType.Numeric, vehicleMakeId.ToString());
			data.AddParam("@TerritoryId", DataAccessParameterType.Numeric, territoryId.ToString());
			data.ExecuteProcedure("DEALER_MAKE_GetRecord");
			num = (!data.EOF ? int.Parse(data["DealerMakeId"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@DealerMakeId", DataAccessParameterType.Numeric, this.DealerMakeId.ToString());
			data.ExecuteProcedure("DEALER_MAKE_GetRecord");
			if (!data.EOF)
			{
				this.DealerMakeId = int.Parse(data["DealerMakeId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.DealerId = int.Parse(data["DealerId"]);
				this.ConfiguratorAd_VehicleMakeId = int.Parse(data["ConfiguratorAd_VehicleMakeId"]);
				this.TerritoryId = int.Parse(data["TerritoryId"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int dealerMakeId = this.DealerMakeId;
			data.AddParam("@DealerMakeId", DataAccessParameterType.Numeric, dealerMakeId.ToString());
			dealerMakeId = this.DealerId;
			data.AddParam("@DealerId", DataAccessParameterType.Numeric, dealerMakeId.ToString());
			dealerMakeId = this.ConfiguratorAd_VehicleMakeId;
			data.AddParam("@ConfiguratorAd_VehicleMakeId", DataAccessParameterType.Numeric, dealerMakeId.ToString());
			dealerMakeId = this.TerritoryId;
			data.AddParam("@TerritoryId", DataAccessParameterType.Numeric, dealerMakeId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("DEALER_MAKE_InsertUpdate");
			if (!data.EOF)
			{
				this.DealerMakeId = int.Parse(data["DealerMakeId"]);
			}
			this.retrievedata();
		}
	}
}