using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class Lead : Transaction
	{
		private int leadId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int com_CntlId = 0;

		private int com_CnttId = 0;

		private int com_CntcId = 0;

		private int affinityGroupId = 0;

		private bool isDistributedToDealer = false;

		private int dealerId = 0;

		private bool isDistributedToConsultant = false;

		private int cars_EmplId = 0;

		private int cars_WorkId = 0;

		private int cars_CowsId = 0;

		private int cars_MembId = 0;

		private string callToActionOn = "";

		private string firstName = "";

		private string lastName = "";

		private string address = "";

		private string address2 = "";

		private string city = "";

		private string state = "";

		private string zipCode = "";

		private string phone = "";

		private string email = "";

		private string notes = "";

		private bool isSendEmailDay01 = false;

		private bool isEmailDay01Sent = false;

		private bool isSendEmailDay03 = false;

		private bool isEmailDay03Sent = false;

		private bool isSendEmailDay05 = false;

		private bool isEmailDay05Sent = false;

		private bool isSendEmailDay10 = false;

		private bool isEmailDay10Sent = false;

		private bool isSendEmailDay21 = false;

		private bool isEmailDay21Sent = false;

		public string Address
		{
			get
			{
				return this.address;
			}
			set
			{
				this.address = value;
			}
		}

		public string Address2
		{
			get
			{
				return this.address2;
			}
			set
			{
				this.address2 = value;
			}
		}

		public int AffinityGroupId
		{
			get
			{
				return this.affinityGroupId;
			}
			set
			{
				this.affinityGroupId = value;
			}
		}

		public string CallToActionOn
		{
			get
			{
				return this.callToActionOn;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.callToActionOn = "";
				}
				else
				{
					this.callToActionOn = DateTime.Parse(value).ToString();
				}
			}
		}

		public int Cars_CowsId
		{
			get
			{
				return this.cars_CowsId;
			}
			set
			{
				this.cars_CowsId = value;
			}
		}

		public int Cars_EmplId
		{
			get
			{
				return this.cars_EmplId;
			}
			set
			{
				this.cars_EmplId = value;
			}
		}

		public int Cars_MembId
		{
			get
			{
				return this.cars_MembId;
			}
			set
			{
				this.cars_MembId = value;
			}
		}

		public int Cars_WorkId
		{
			get
			{
				return this.cars_WorkId;
			}
			set
			{
				this.cars_WorkId = value;
			}
		}

		public string City
		{
			get
			{
				return this.city;
			}
			set
			{
				this.city = value;
			}
		}

		public int Com_CntcId
		{
			get
			{
				return this.com_CntcId;
			}
			set
			{
				this.com_CntcId = value;
			}
		}

		public int Com_CntlId
		{
			get
			{
				return this.com_CntlId;
			}
			set
			{
				this.com_CntlId = value;
			}
		}

		public int Com_CnttId
		{
			get
			{
				return this.com_CnttId;
			}
			set
			{
				this.com_CnttId = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public int DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public string Email
		{
			get
			{
				return this.email;
			}
			set
			{
				this.email = value;
			}
		}

		public string FirstName
		{
			get
			{
				return this.firstName;
			}
			set
			{
				this.firstName = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public bool IsDistributedToConsultant
		{
			get
			{
				return this.isDistributedToConsultant;
			}
			set
			{
				this.isDistributedToConsultant = value;
			}
		}

		public bool IsDistributedToDealer
		{
			get
			{
				return this.isDistributedToDealer;
			}
			set
			{
				this.isDistributedToDealer = value;
			}
		}

		public bool IsEmailDay01Sent
		{
			get
			{
				return this.isEmailDay01Sent;
			}
			set
			{
				this.isEmailDay01Sent = value;
			}
		}

		public bool IsEmailDay03Sent
		{
			get
			{
				return this.isEmailDay03Sent;
			}
			set
			{
				this.isEmailDay03Sent = value;
			}
		}

		public bool IsEmailDay05Sent
		{
			get
			{
				return this.isEmailDay05Sent;
			}
			set
			{
				this.isEmailDay05Sent = value;
			}
		}

		public bool IsEmailDay10Sent
		{
			get
			{
				return this.isEmailDay10Sent;
			}
			set
			{
				this.isEmailDay10Sent = value;
			}
		}

		public bool IsEmailDay21Sent
		{
			get
			{
				return this.isEmailDay21Sent;
			}
			set
			{
				this.isEmailDay21Sent = value;
			}
		}

		public bool IsSendEmailDay01
		{
			get
			{
				return this.isSendEmailDay01;
			}
			set
			{
				this.isSendEmailDay01 = value;
			}
		}

		public bool IsSendEmailDay03
		{
			get
			{
				return this.isSendEmailDay03;
			}
			set
			{
				this.isSendEmailDay03 = value;
			}
		}

		public bool IsSendEmailDay05
		{
			get
			{
				return this.isSendEmailDay05;
			}
			set
			{
				this.isSendEmailDay05 = value;
			}
		}

		public bool IsSendEmailDay10
		{
			get
			{
				return this.isSendEmailDay10;
			}
			set
			{
				this.isSendEmailDay10 = value;
			}
		}

		public bool IsSendEmailDay21
		{
			get
			{
				return this.isSendEmailDay21;
			}
			set
			{
				this.isSendEmailDay21 = value;
			}
		}

		public string LastName
		{
			get
			{
				return this.lastName;
			}
			set
			{
				this.lastName = value;
			}
		}

		public int LeadId
		{
			get
			{
				return this.leadId;
			}
			set
			{
				this.leadId = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public string Notes
		{
			get
			{
				return this.notes;
			}
			set
			{
				this.notes = value;
			}
		}

		public string Phone
		{
			get
			{
				return this.phone;
			}
			set
			{
				this.phone = value;
			}
		}

		public string State
		{
			get
			{
				return this.state;
			}
			set
			{
				this.state = value;
			}
		}

		public string ZipCode
		{
			get
			{
				return this.zipCode;
			}
			set
			{
				this.zipCode = value;
			}
		}

		public Lead()
		{
		}

		public Lead(string connection, string modifiedUserID, int leadId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "LEAD";
			this.LeadId = leadId;
			if (this.LeadId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@LeadId", DataAccessParameterType.Numeric, this.LeadId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("LEAD_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@LeadId", DataAccessParameterType.Numeric, this.LeadId.ToString());
			data.ExecuteProcedure("LEAD_GetRecord");
			if (!data.EOF)
			{
				this.LeadId = int.Parse(data["LeadId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.Com_CntlId = int.Parse(data["Com_CntlId"]);
				this.Com_CnttId = int.Parse(data["Com_CnttId"]);
				this.Com_CntcId = int.Parse(data["Com_CntcId"]);
				this.AffinityGroupId = int.Parse(data["AffinityGroupId"]);
				this.IsDistributedToDealer = bool.Parse(data["IsDistributedToDealer"]);
				this.DealerId = int.Parse(data["DealerId"]);
				this.IsDistributedToConsultant = bool.Parse(data["IsDistributedToConsultant"]);
				this.Cars_EmplId = int.Parse(data["Cars_EmplId"]);
				this.Cars_WorkId = int.Parse(data["Cars_WorkId"]);
				this.Cars_CowsId = int.Parse(data["Cars_CowsId"]);
				this.Cars_MembId = int.Parse(data["Cars_MembId"]);
				this.CallToActionOn = data["CallToActionOn"];
				this.FirstName = data["FirstName"];
				this.LastName = data["LastName"];
				this.Address = data["Address"];
				this.Address2 = data["Address2"];
				this.City = data["City"];
				this.State = data["State"];
				this.ZipCode = data["ZipCode"];
				this.Phone = data["Phone"];
				this.Email = data["Email"];
				this.Notes = data["Notes"];
				this.IsSendEmailDay01 = bool.Parse(data["IsSendEmailDay01"]);
				this.IsEmailDay01Sent = bool.Parse(data["IsEmailDay01Sent"]);
				this.IsSendEmailDay03 = bool.Parse(data["IsSendEmailDay03"]);
				this.IsSendEmailDay05 = bool.Parse(data["IsSendEmailDay05"]);
				this.IsEmailDay03Sent = bool.Parse(data["IsEmailDay03Sent"]);
				this.IsSendEmailDay10 = bool.Parse(data["IsSendEmailDay10"]);
				this.IsEmailDay10Sent = bool.Parse(data["IsEmailDay10Sent"]);
				this.IsSendEmailDay21 = bool.Parse(data["IsSendEmailDay21"]);
				this.IsEmailDay21Sent = bool.Parse(data["IsEmailDay21Sent"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int leadId = this.LeadId;
			data.AddParam("@LeadId", DataAccessParameterType.Numeric, leadId.ToString());
			leadId = this.Com_CntlId;
			data.AddParam("@Com_CntlId", DataAccessParameterType.Numeric, leadId.ToString());
			leadId = this.Com_CnttId;
			data.AddParam("@Com_CnttId", DataAccessParameterType.Numeric, leadId.ToString());
			leadId = this.Com_CntcId;
			data.AddParam("@Com_CntcId", DataAccessParameterType.Numeric, leadId.ToString());
			leadId = this.AffinityGroupId;
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, leadId.ToString());
			bool isDistributedToDealer = this.IsDistributedToDealer;
			data.AddParam("@IsDistributedToDealer", DataAccessParameterType.Bool, isDistributedToDealer.ToString());
			leadId = this.DealerId;
			data.AddParam("@DealerId", DataAccessParameterType.Numeric, leadId.ToString());
			isDistributedToDealer = this.IsDistributedToConsultant;
			data.AddParam("@IsDistributedToConsultant", DataAccessParameterType.Bool, isDistributedToDealer.ToString());
			leadId = this.Cars_EmplId;
			data.AddParam("@Cars_EmplId", DataAccessParameterType.Numeric, leadId.ToString());
			leadId = this.Cars_WorkId;
			data.AddParam("@Cars_WorkId", DataAccessParameterType.Numeric, leadId.ToString());
			leadId = this.Cars_CowsId;
			data.AddParam("@Cars_CowsId", DataAccessParameterType.Numeric, leadId.ToString());
			leadId = this.Cars_MembId;
			data.AddParam("@Cars_MembId", DataAccessParameterType.Numeric, leadId.ToString());
			data.AddParam("@CallToActionOn", DataAccessParameterType.DateTime, this.CallToActionOn);
			data.AddParam("@FirstName", DataAccessParameterType.Text, this.FirstName);
			data.AddParam("@LastName", DataAccessParameterType.Text, this.LastName);
			data.AddParam("@Address", DataAccessParameterType.Text, this.Address);
			data.AddParam("@Address2", DataAccessParameterType.Text, this.Address2);
			data.AddParam("@City", DataAccessParameterType.Text, this.City);
			data.AddParam("@State", DataAccessParameterType.Text, this.State);
			data.AddParam("@ZipCode", DataAccessParameterType.Text, this.ZipCode);
			data.AddParam("@Phone", DataAccessParameterType.Text, this.Phone);
			data.AddParam("@Email", DataAccessParameterType.Text, this.Email);
			data.AddParam("@Notes", DataAccessParameterType.Text, this.Notes);
			isDistributedToDealer = this.IsSendEmailDay01;
			data.AddParam("@IsSendEmailDay01", DataAccessParameterType.Bool, isDistributedToDealer.ToString());
			isDistributedToDealer = this.IsEmailDay01Sent;
			data.AddParam("@IsEmailDay01Sent", DataAccessParameterType.Bool, isDistributedToDealer.ToString());
			isDistributedToDealer = this.IsSendEmailDay03;
			data.AddParam("@IsSendEmailDay03", DataAccessParameterType.Bool, isDistributedToDealer.ToString());
			isDistributedToDealer = this.IsEmailDay03Sent;
			data.AddParam("@IsEmailDay03Sent", DataAccessParameterType.Bool, isDistributedToDealer.ToString());
			isDistributedToDealer = this.IsSendEmailDay05;
			data.AddParam("@IsSendEmailDay05", DataAccessParameterType.Bool, isDistributedToDealer.ToString());
			isDistributedToDealer = this.IsEmailDay05Sent;
			data.AddParam("@IsEmailDay05Sent", DataAccessParameterType.Bool, isDistributedToDealer.ToString());
			isDistributedToDealer = this.IsSendEmailDay10;
			data.AddParam("@IsSendEmailDay10", DataAccessParameterType.Bool, isDistributedToDealer.ToString());
			isDistributedToDealer = this.IsEmailDay10Sent;
			data.AddParam("@IsEmailDay10Sent", DataAccessParameterType.Bool, isDistributedToDealer.ToString());
			isDistributedToDealer = this.IsSendEmailDay21;
			data.AddParam("@IsSendEmailDay21", DataAccessParameterType.Bool, isDistributedToDealer.ToString());
			isDistributedToDealer = this.IsEmailDay21Sent;
			data.AddParam("@IsEmailDay21Sent", DataAccessParameterType.Bool, isDistributedToDealer.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("LEAD_InsertUpdate");
			if (!data.EOF)
			{
				this.LeadId = int.Parse(data["LeadId"]);
			}
			this.retrievedata();
		}
	}
}