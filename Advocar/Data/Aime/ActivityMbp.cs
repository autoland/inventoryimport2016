using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class ActivityMbp : Transaction
	{
		private int activityMbpId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int activityId = 0;

		private int affinityGroupId = 0;

		private int memberId = 0;

		private int lookupId_MbpStatus = 0;

		private int configurator_YearId = 0;

		private int productMbpMakeId = 0;

		private int productMbpModelId = 0;

		private string mbpVsc = "";

		private int lookupId_MbpPlan = 0;

		private int lookupId_MbpMileageBand = 0;

		private string mbpClass = "";

		private int currentMiles = 0;

		private int termYears = 0;

		private int termMiles = 0;

		private float deductible = 0f;

		private bool isNoncoveredPartCauseOfLoss = false;

		private bool isCoveredPartCauseOfLoss = false;

		private bool isEmissionCoverage = false;

		private bool isLightBusinessUse = false;

		private int productPricingMbpId = 0;

		private float premium = 0f;

		private float premiumCost = 0f;

		private string vin = "";

		private string vehiclePurchaseDate = "";

		private float vehiclePurchasePrice = 0f;

		private string coverageStartDate = "";

		private string coverageEndDate = "";

		private int coverageEndMiles = 0;

		private int affinityGroupBranchId = 0;

		private int affinityGroupBranchId_Lienholder = 0;

		private int affinityGroupBranchId_Groupholder = 0;

		private string userId_Extranet = "";

		private string issuedDate = "";

		private int productId = 0;

		private string spouseName = "";

		private string issuedState = string.Empty;

		public int ActivityId
		{
			get
			{
				return this.activityId;
			}
			set
			{
				this.activityId = value;
			}
		}

		public int ActivityMbpId
		{
			get
			{
				return this.activityMbpId;
			}
			set
			{
				this.activityMbpId = value;
			}
		}

		public int AffinityGroupBranchId
		{
			get
			{
				return this.affinityGroupBranchId;
			}
			set
			{
				this.affinityGroupBranchId = value;
			}
		}

		public int AffinityGroupBranchId_Groupholder
		{
			get
			{
				return this.affinityGroupBranchId_Groupholder;
			}
			set
			{
				this.affinityGroupBranchId_Groupholder = value;
			}
		}

		public int AffinityGroupBranchId_Lienholder
		{
			get
			{
				return this.affinityGroupBranchId_Lienholder;
			}
			set
			{
				this.affinityGroupBranchId_Lienholder = value;
			}
		}

		public int AffinityGroupId
		{
			get
			{
				return this.affinityGroupId;
			}
			set
			{
				this.affinityGroupId = value;
			}
		}

		public int Configurator_YearId
		{
			get
			{
				return this.configurator_YearId;
			}
			set
			{
				this.configurator_YearId = value;
			}
		}

		public string CoverageEndDate
		{
			get
			{
				return this.coverageEndDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.coverageEndDate = "";
				}
				else
				{
					this.coverageEndDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public int CoverageEndMiles
		{
			get
			{
				return this.coverageEndMiles;
			}
			set
			{
				this.coverageEndMiles = value;
			}
		}

		public string CoverageStartDate
		{
			get
			{
				return this.coverageStartDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.coverageStartDate = "";
				}
				else
				{
					this.coverageStartDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public int CurrentMiles
		{
			get
			{
				return this.currentMiles;
			}
			set
			{
				this.currentMiles = value;
			}
		}

		public float Deductible
		{
			get
			{
				return this.deductible;
			}
			set
			{
				this.deductible = value;
			}
		}

		public bool IsCoveredPartCauseOfLoss
		{
			get
			{
				return this.isCoveredPartCauseOfLoss;
			}
			set
			{
				this.isCoveredPartCauseOfLoss = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public bool IsEmissionCoverage
		{
			get
			{
				return this.isEmissionCoverage;
			}
			set
			{
				this.isEmissionCoverage = value;
			}
		}

		public bool IsLightBusinessUse
		{
			get
			{
				return this.isLightBusinessUse;
			}
			set
			{
				this.isLightBusinessUse = value;
			}
		}

		public bool IsNoncoveredPartCauseOfLoss
		{
			get
			{
				return this.isNoncoveredPartCauseOfLoss;
			}
			set
			{
				this.isNoncoveredPartCauseOfLoss = value;
			}
		}

		public string IssuedDate
		{
			get
			{
				return this.issuedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.issuedDate = "";
				}
				else
				{
					this.issuedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string IssuedState
		{
			get
			{
				return this.issuedState;
			}
			set
			{
				this.issuedState = value;
			}
		}

		public int LookupId_MbpMileageBand
		{
			get
			{
				return this.lookupId_MbpMileageBand;
			}
			set
			{
				this.lookupId_MbpMileageBand = value;
			}
		}

		public int LookupId_MbpPlan
		{
			get
			{
				return this.lookupId_MbpPlan;
			}
			set
			{
				this.lookupId_MbpPlan = value;
			}
		}

		public int LookupId_MbpStatus
		{
			get
			{
				return this.lookupId_MbpStatus;
			}
			set
			{
				this.lookupId_MbpStatus = value;
			}
		}

		public string MbpClass
		{
			get
			{
				return this.mbpClass;
			}
			set
			{
				this.mbpClass = value;
			}
		}

		public string MbpVsc
		{
			get
			{
				return this.mbpVsc;
			}
			set
			{
				this.mbpVsc = value;
			}
		}

		public int MemberId
		{
			get
			{
				return this.memberId;
			}
			set
			{
				this.memberId = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public float Premium
		{
			get
			{
				return this.premium;
			}
			set
			{
				this.premium = value;
			}
		}

		public float PremiumCost
		{
			get
			{
				return this.premiumCost;
			}
			set
			{
				this.premiumCost = value;
			}
		}

		public int ProductId
		{
			get
			{
				return this.productId;
			}
			set
			{
				this.productId = value;
			}
		}

		public int ProductMbpMakeId
		{
			get
			{
				return this.productMbpMakeId;
			}
			set
			{
				this.productMbpMakeId = value;
			}
		}

		public int ProductMbpModelId
		{
			get
			{
				return this.productMbpModelId;
			}
			set
			{
				this.productMbpModelId = value;
			}
		}

		public int ProductPricingMbpId
		{
			get
			{
				return this.productPricingMbpId;
			}
			set
			{
				this.productPricingMbpId = value;
			}
		}

		public string SpouseName
		{
			get
			{
				return this.spouseName;
			}
			set
			{
				this.spouseName = value;
			}
		}

		public int TermMiles
		{
			get
			{
				return this.termMiles;
			}
			set
			{
				this.termMiles = value;
			}
		}

		public int TermYears
		{
			get
			{
				return this.termYears;
			}
			set
			{
				this.termYears = value;
			}
		}

		public string UserId_Extranet
		{
			get
			{
				return this.userId_Extranet;
			}
			set
			{
				this.userId_Extranet = value;
			}
		}

		public string VehiclePurchaseDate
		{
			get
			{
				return this.vehiclePurchaseDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.vehiclePurchaseDate = "";
				}
				else
				{
					this.vehiclePurchaseDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public float VehiclePurchasePrice
		{
			get
			{
				return this.vehiclePurchasePrice;
			}
			set
			{
				this.vehiclePurchasePrice = value;
			}
		}

		public string Vin
		{
			get
			{
				return this.vin;
			}
			set
			{
				this.vin = value;
			}
		}

		public ActivityMbp()
		{
		}

		public ActivityMbp(string connection, string modifiedUserID, int activityMbpId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.ActivityMbpId = activityMbpId;
			if (this.ActivityMbpId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ActivityMbpId", DataAccessParameterType.Numeric, this.ActivityMbpId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("ACTIVITY_MBP_Delete");
			this.wipeout();
		}

		public static int GetRecordId(int activityId, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@ActivityId", DataAccessParameterType.Numeric, activityId.ToString());
			data.ExecuteProcedure("ACTIVITY_MBP_GetRecord");
			num = (!data.EOF ? int.Parse(data["ActivityMbpId"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ActivityMbpId", DataAccessParameterType.Numeric, this.ActivityMbpId.ToString());
			data.ExecuteProcedure("ACTIVITY_MBP_GetRecord");
			if (!data.EOF)
			{
				this.ActivityMbpId = int.Parse(data["ActivityMbpId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.ActivityId = int.Parse(data["ActivityId"]);
				this.AffinityGroupId = int.Parse(data["AffinityGroupId"]);
				this.MemberId = int.Parse(data["MemberId"]);
				this.LookupId_MbpStatus = int.Parse(data["LookupId_MbpStatus"]);
				this.Configurator_YearId = int.Parse(data["Configurator_YearId"]);
				this.ProductMbpMakeId = int.Parse(data["ProductMbpMakeId"]);
				this.ProductMbpModelId = int.Parse(data["ProductMbpModelId"]);
				this.MbpVsc = data["MbpVsc"];
				this.LookupId_MbpPlan = int.Parse(data["LookupId_MbpPlan"]);
				this.LookupId_MbpMileageBand = int.Parse(data["LookupId_MbpMileageBand"]);
				this.MbpClass = data["MbpClass"];
				this.CurrentMiles = int.Parse(data["CurrentMiles"]);
				this.TermYears = int.Parse(data["TermYears"]);
				this.TermMiles = int.Parse(data["TermMiles"]);
				this.Deductible = float.Parse(data["Deductible"]);
				this.IsNoncoveredPartCauseOfLoss = bool.Parse(data["IsNoncoveredPartCauseOfLoss"]);
				this.IsCoveredPartCauseOfLoss = bool.Parse(data["IsCoveredPartCauseOfLoss"]);
				this.IsEmissionCoverage = bool.Parse(data["IsEmissionCoverage"]);
				this.IsLightBusinessUse = bool.Parse(data["IsLightBusinessUse"]);
				this.ProductPricingMbpId = int.Parse(data["ProductPricingMbpId"]);
				this.Premium = float.Parse(data["Premium"]);
				this.PremiumCost = float.Parse(data["PremiumCost"]);
				this.Vin = data["Vin"];
				this.VehiclePurchaseDate = data["VehiclePurchaseDate"];
				this.VehiclePurchasePrice = float.Parse(data["VehiclePurchasePrice"]);
				this.CoverageStartDate = data["CoverageStartDate"];
				this.CoverageEndDate = data["CoverageEndDate"];
				this.CoverageEndMiles = int.Parse(data["CoverageEndMiles"]);
				this.AffinityGroupBranchId = int.Parse(data["AffinityGroupBranchId"]);
				this.AffinityGroupBranchId_Lienholder = int.Parse(data["AffinityGroupBranchId_Lienholder"]);
				this.AffinityGroupBranchId_Groupholder = int.Parse(data["AffinityGroupBranchId_Groupholder"]);
				this.UserId_Extranet = data["UserId_Extranet"];
				this.IssuedDate = data["IssuedDate"];
				this.ProductId = int.Parse(data["ProductId"]);
				this.SpouseName = data["SpouseName"];
				this.IssuedState = data["IssuedState"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int activityMbpId = this.ActivityMbpId;
			data.AddParam("@ActivityMbpId", DataAccessParameterType.Numeric, activityMbpId.ToString());
			activityMbpId = this.ActivityId;
			data.AddParam("@ActivityId", DataAccessParameterType.Numeric, activityMbpId.ToString());
			activityMbpId = this.AffinityGroupId;
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, activityMbpId.ToString());
			activityMbpId = this.MemberId;
			data.AddParam("@MemberId", DataAccessParameterType.Numeric, activityMbpId.ToString());
			activityMbpId = this.LookupId_MbpStatus;
			data.AddParam("@LookupId_MbpStatus", DataAccessParameterType.Numeric, activityMbpId.ToString());
			activityMbpId = this.Configurator_YearId;
			data.AddParam("@Configurator_YearId", DataAccessParameterType.Numeric, activityMbpId.ToString());
			activityMbpId = this.ProductMbpMakeId;
			data.AddParam("@ProductMbpMakeId", DataAccessParameterType.Numeric, activityMbpId.ToString());
			activityMbpId = this.ProductMbpModelId;
			data.AddParam("@ProductMbpModelId", DataAccessParameterType.Numeric, activityMbpId.ToString());
			data.AddParam("@MbpVsc", DataAccessParameterType.Text, this.MbpVsc);
			activityMbpId = this.LookupId_MbpPlan;
			data.AddParam("@LookupId_MbpPlan", DataAccessParameterType.Numeric, activityMbpId.ToString());
			activityMbpId = this.LookupId_MbpMileageBand;
			data.AddParam("@LookupId_MbpMileageBand", DataAccessParameterType.Numeric, activityMbpId.ToString());
			data.AddParam("@MbpClass", DataAccessParameterType.Text, this.MbpClass);
			activityMbpId = this.CurrentMiles;
			data.AddParam("@CurrentMiles", DataAccessParameterType.Numeric, activityMbpId.ToString());
			activityMbpId = this.TermYears;
			data.AddParam("@TermYears", DataAccessParameterType.Numeric, activityMbpId.ToString());
			activityMbpId = this.TermMiles;
			data.AddParam("@TermMiles", DataAccessParameterType.Numeric, activityMbpId.ToString());
			float deductible = this.Deductible;
			data.AddParam("@Deductible", DataAccessParameterType.Numeric, deductible.ToString());
			bool isNoncoveredPartCauseOfLoss = this.IsNoncoveredPartCauseOfLoss;
			data.AddParam("@IsNoncoveredPartCauseOfLoss", DataAccessParameterType.Bool, isNoncoveredPartCauseOfLoss.ToString());
			isNoncoveredPartCauseOfLoss = this.IsCoveredPartCauseOfLoss;
			data.AddParam("@IsCoveredPartCauseOfLoss", DataAccessParameterType.Bool, isNoncoveredPartCauseOfLoss.ToString());
			isNoncoveredPartCauseOfLoss = this.IsEmissionCoverage;
			data.AddParam("@IsEmissionCoverage", DataAccessParameterType.Bool, isNoncoveredPartCauseOfLoss.ToString());
			isNoncoveredPartCauseOfLoss = this.IsLightBusinessUse;
			data.AddParam("@IsLightBusinessUse", DataAccessParameterType.Bool, isNoncoveredPartCauseOfLoss.ToString());
			activityMbpId = this.ProductPricingMbpId;
			data.AddParam("@ProductPricingMbpId", DataAccessParameterType.Numeric, activityMbpId.ToString());
			deductible = this.Premium;
			data.AddParam("@Premium", DataAccessParameterType.Numeric, deductible.ToString());
			deductible = this.PremiumCost;
			data.AddParam("@PremiumCost", DataAccessParameterType.Numeric, deductible.ToString());
			data.AddParam("@Vin", DataAccessParameterType.Text, this.Vin);
			data.AddParam("@VehiclePurchaseDate", DataAccessParameterType.DateTime, this.VehiclePurchaseDate);
			deductible = this.VehiclePurchasePrice;
			data.AddParam("@VehiclePurchasePrice", DataAccessParameterType.Numeric, deductible.ToString());
			data.AddParam("@CoverageStartDate", DataAccessParameterType.DateTime, this.CoverageStartDate);
			data.AddParam("@CoverageEndDate", DataAccessParameterType.DateTime, this.CoverageEndDate);
			activityMbpId = this.CoverageEndMiles;
			data.AddParam("@CoverageEndMiles", DataAccessParameterType.Numeric, activityMbpId.ToString());
			activityMbpId = this.AffinityGroupBranchId;
			data.AddParam("@AffinityGroupBranchId", DataAccessParameterType.Numeric, activityMbpId.ToString());
			activityMbpId = this.AffinityGroupBranchId_Lienholder;
			data.AddParam("@AffinityGroupBranchId_Lienholder", DataAccessParameterType.Numeric, activityMbpId.ToString());
			activityMbpId = this.AffinityGroupBranchId_Groupholder;
			data.AddParam("@AffinityGroupBranchId_Groupholder", DataAccessParameterType.Numeric, activityMbpId.ToString());
			data.AddParam("@UserId_Extranet", DataAccessParameterType.Text, this.UserId_Extranet);
			data.AddParam("@IssuedDate", DataAccessParameterType.DateTime, this.IssuedDate);
			activityMbpId = this.ProductId;
			data.AddParam("@ProductId", DataAccessParameterType.Numeric, activityMbpId.ToString());
			data.AddParam("@SpouseName", DataAccessParameterType.Text, this.SpouseName);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.AddParam("@IssuedState", DataAccessParameterType.Text, this.issuedState);
			data.ExecuteProcedure("ACTIVITY_MBP_InsertUpdate");
			if (!data.EOF)
			{
				this.ActivityMbpId = int.Parse(data["ActivityMbpId"]);
			}
			this.retrievedata();
		}
	}
}