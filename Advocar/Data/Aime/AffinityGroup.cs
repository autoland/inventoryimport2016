using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class AffinityGroup : Transaction
	{
		private int affinityGroupId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int affinityGroupBranchId = 0;

		private string code = "";

		private int oldId = 0;

		private string groupName = "";

		private string tollFreePhone = "";

		private bool isAnonymousUser = false;

		private int anonymousUserCount = 0;

		private float interestRate = 0f;

		private bool isActive = false;

		private bool isAutolandRewards = false;

		private int vendorId_Mailhouse = 0;

		private int noteId = 0;

		private int affinityGroupWebProfileId = 0;

		private int accountingControlId = 0;

		private bool isWww2 = false;

		private bool isInventoryOnly = false;

		private bool isDealerDirect = false;

		private bool isNewConsumerSite = false;

		private bool isLicensedGroup = false;

		private float consultantCommission = 0f;

		private string licensedGroupEmailLeads = "";

		private bool isLendingManual = false;

		private bool isLendingCudl = false;

		private int lendingCudlId = 0;

		private bool isLendingRouteOne = false;

		private int lendingRouteOneId = 0;

		private bool isLendingDealerTrack = false;

		private int lendingDealerTrackId = 0;

		private int lendingMinimumScore = 0;

		private int lendingMinimumYear = 0;

		private bool isBank = false;

		public int AccountingControlId
		{
			get
			{
				return this.accountingControlId;
			}
			set
			{
				this.accountingControlId = value;
			}
		}

		public int AffinityGroupBranchId
		{
			get
			{
				return this.affinityGroupBranchId;
			}
			set
			{
				this.affinityGroupBranchId = value;
			}
		}

		public int AffinityGroupId
		{
			get
			{
				return this.affinityGroupId;
			}
			set
			{
				this.affinityGroupId = value;
			}
		}

		public int AffinityGroupWebProfileId
		{
			get
			{
				return this.affinityGroupWebProfileId;
			}
			set
			{
				this.affinityGroupWebProfileId = value;
			}
		}

		public int AnonymousUserCount
		{
			get
			{
				return this.anonymousUserCount;
			}
			set
			{
				this.anonymousUserCount = value;
			}
		}

		public string Code
		{
			get
			{
				return this.code;
			}
			set
			{
				this.code = value;
			}
		}

		public float ConsultantCommission
		{
			get
			{
				return this.consultantCommission;
			}
			set
			{
				this.consultantCommission = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string GroupName
		{
			get
			{
				return this.groupName;
			}
			set
			{
				this.groupName = value;
			}
		}

		public float InterestRate
		{
			get
			{
				return this.interestRate;
			}
			set
			{
				this.interestRate = value;
			}
		}

		public bool IsActive
		{
			get
			{
				return this.isActive;
			}
			set
			{
				this.isActive = value;
			}
		}

		public bool IsAnonymousUser
		{
			get
			{
				return this.isAnonymousUser;
			}
			set
			{
				this.isAnonymousUser = value;
			}
		}

		public bool IsAutolandRewards
		{
			get
			{
				return this.isAutolandRewards;
			}
			set
			{
				this.isAutolandRewards = value;
			}
		}

		public bool IsBank
		{
			get
			{
				return this.isBank;
			}
			set
			{
				this.isBank = value;
			}
		}

		public bool IsCarFax
		{
			get
			{
				int affinityGroupSiteMenuId = AffinityGroupSiteMenu.GetRecordId("Resources_Carfax", this.AffinityGroupId, this.connection);
				return (new AffinityGroupSiteMenu(this.connection, this.ModifiedUser, affinityGroupSiteMenuId)).IsActive;
			}
		}

		public bool IsClassifieds
		{
			get
			{
				int affinityGroupSiteMenuId = AffinityGroupSiteMenu.GetRecordId("PreOwned_BrowseClassified", this.AffinityGroupId, this.connection);
				return (new AffinityGroupSiteMenu(this.connection, this.ModifiedUser, affinityGroupSiteMenuId)).IsActive;
			}
		}

		public bool IsDealerDirect
		{
			get
			{
				return this.isDealerDirect;
			}
			set
			{
				this.isDealerDirect = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public bool IsInventoryOnly
		{
			get
			{
				return this.isInventoryOnly;
			}
			set
			{
				this.isInventoryOnly = value;
			}
		}

		public bool IsKbb
		{
			get
			{
				int affinityGroupSiteMenuId = AffinityGroupSiteMenu.GetRecordId("Resources_Kbb", this.AffinityGroupId, this.connection);
				return (new AffinityGroupSiteMenu(this.connection, this.ModifiedUser, affinityGroupSiteMenuId)).IsActive;
			}
		}

		public bool IsLendingCudl
		{
			get
			{
				return this.isLendingCudl;
			}
			set
			{
				this.isLendingCudl = value;
			}
		}

		public bool IsLendingDealerTrack
		{
			get
			{
				return this.isLendingDealerTrack;
			}
			set
			{
				this.isLendingDealerTrack = value;
			}
		}

		public bool IsLendingManual
		{
			get
			{
				return this.isLendingManual;
			}
			set
			{
				this.isLendingManual = value;
			}
		}

		public bool IsLendingRouteOne
		{
			get
			{
				return this.isLendingRouteOne;
			}
			set
			{
				this.isLendingRouteOne = value;
			}
		}

		public bool IsLicensedGroup
		{
			get
			{
				return this.isLicensedGroup;
			}
			set
			{
				this.isLicensedGroup = value;
			}
		}

		public bool IsLoanApplication
		{
			get
			{
				int affinityGroupSiteMenuId = AffinityGroupSiteMenu.GetRecordId("Finance_ApplyForLoan", this.AffinityGroupId, this.connection);
				return (new AffinityGroupSiteMenu(this.connection, this.ModifiedUser, affinityGroupSiteMenuId)).IsActive;
			}
		}

		public bool IsMbp
		{
			get
			{
				int affinityGroupSiteMenuId = AffinityGroupSiteMenu.GetRecordId("Mbp_Quote", this.AffinityGroupId, this.connection);
				return (new AffinityGroupSiteMenu(this.connection, this.ModifiedUser, affinityGroupSiteMenuId)).IsActive;
			}
		}

		public bool IsNewConsumerSite
		{
			get
			{
				return this.isNewConsumerSite;
			}
			set
			{
				this.isNewConsumerSite = value;
			}
		}

		public bool IsVsc
		{
			get
			{
				bool isActive;
				int affinityGroupSiteMenuId = AffinityGroupSiteMenu.GetRecordId("Vsc_Quote", this.AffinityGroupId, this.connection);
				if (affinityGroupSiteMenuId <= 0)
				{
					isActive = false;
				}
				else
				{
					AffinityGroupSiteMenu menu = new AffinityGroupSiteMenu(this.connection, this.ModifiedUser, affinityGroupSiteMenuId);
					isActive = menu.IsActive;
				}
				return isActive;
			}
		}

		public bool IsWww2
		{
			get
			{
				return this.isWww2;
			}
		}

		public int LendingCudlId
		{
			get
			{
				return this.lendingCudlId;
			}
			set
			{
				this.lendingCudlId = value;
			}
		}

		public int LendingDealerTrackId
		{
			get
			{
				return this.lendingDealerTrackId;
			}
			set
			{
				this.lendingDealerTrackId = value;
			}
		}

		public int LendingMinimumScore
		{
			get
			{
				return this.lendingMinimumScore;
			}
			set
			{
				this.lendingMinimumScore = value;
			}
		}

		public int LendingMinimumYear
		{
			get
			{
				return this.lendingMinimumYear;
			}
			set
			{
				this.lendingMinimumYear = value;
			}
		}

		public int LendingRouteOneId
		{
			get
			{
				return this.lendingRouteOneId;
			}
			set
			{
				this.lendingRouteOneId = value;
			}
		}

		public string LicensedGroupEmailLeads
		{
			get
			{
				return this.licensedGroupEmailLeads;
			}
			set
			{
				this.licensedGroupEmailLeads = value;
			}
		}

		public string LoanApplicationUrl
		{
			get
			{
				int affinityGroupSiteMenuId = AffinityGroupSiteMenu.GetRecordId("Finance_ApplyForLoan", this.AffinityGroupId, this.connection);
				return (new AffinityGroupSiteMenu(this.connection, this.ModifiedUser, affinityGroupSiteMenuId)).AlternateUrl;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public int NoteId
		{
			get
			{
				return this.noteId;
			}
			set
			{
				this.noteId = value;
			}
		}

		public int OldId
		{
			get
			{
				return this.oldId;
			}
			set
			{
				this.oldId = value;
			}
		}

		public string TollFreePhone
		{
			get
			{
				return this.tollFreePhone;
			}
			set
			{
				this.tollFreePhone = value;
			}
		}

		public int VendorId_Mailhouse
		{
			get
			{
				return this.vendorId_Mailhouse;
			}
			set
			{
				this.vendorId_Mailhouse = value;
			}
		}

		public AffinityGroup()
		{
		}

		public AffinityGroup(string connection, string modifiedUserID, int affinityGroupId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.AffinityGroupId = affinityGroupId;
			if (this.AffinityGroupId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, this.AffinityGroupId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("AFFINITY_GROUP_Delete");
			this.wipeout();
		}

		public string GetAffinityGroupLeadEmail(string emailType, string connection)
		{
			DataAccess data = new DataAccess(connection);
			data.AddParam("@AgrpId", DataAccessParameterType.Numeric, this.OldId.ToString());
			data.ExecuteProcedure("AGRP_GetRecordEmails");
			string emails = "";
			string str = emailType;
			if (str != null)
			{
				switch (str)
				{
					case "Classified":
					{
						emails = data["EmailClassified"];
						break;
					}
					case "ContactUs":
					{
						emails = data["EmailContactUs"];
						break;
					}
					case "Mbp":
					{
						emails = data["EmailMbp"];
						break;
					}
					case "GetQuote":
					{
						emails = data["EmailGetQuote"];
						break;
					}
					case "Lease":
					{
						emails = data["EmailLease"];
						break;
					}
					case "LoanApp":
					{
						emails = data["EmailLoanApp"];
						break;
					}
					case "Locates":
					{
						emails = data["EmailLocates"];
						break;
					}
					case "Bids":
					{
						emails = data["EmailBids"];
						break;
					}
				}
			}
			return emails;
		}

		public static int GetRecordId(int code, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@OldId", DataAccessParameterType.Numeric, code.ToString());
			data.ExecuteProcedure("AFFINITY_GROUP_GetRecord");
			num = (!data.EOF ? int.Parse(data["AffinityGroupId"]) : 0);
			return num;
		}

		public static int GetRecordId(string code, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@Code", DataAccessParameterType.Text, code);
			data.ExecuteProcedure("AFFINITY_GROUP_GetRecord");
			num = (!data.EOF ? int.Parse(data["AffinityGroupId"]) : 0);
			return num;
		}

		public static int GetRecordLienholder(int AffinityGroupId, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, AffinityGroupId.ToString());
			data.ExecuteProcedure("AFFINITY_GROUP_GetRecord_Lienholder");
			num = (!data.EOF ? int.Parse(data["AffinityGroupBranchId"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, this.AffinityGroupId.ToString());
			data.ExecuteProcedure("AFFINITY_GROUP_GetRecord");
			if (!data.EOF)
			{
				this.AffinityGroupId = int.Parse(data["AffinityGroupId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.AffinityGroupBranchId = int.Parse(data["AffinityGroupBranchId"]);
				this.Code = data["Code"];
				this.OldId = int.Parse(data["OldId"]);
				this.GroupName = data["GroupName"];
				this.TollFreePhone = data["TollFreePhone"];
				this.IsAnonymousUser = bool.Parse(data["IsAnonymousUser"]);
				this.AnonymousUserCount = int.Parse(data["AnonymousUserCount"]);
				this.InterestRate = float.Parse(data["InterestRate"]);
				this.IsActive = bool.Parse(data["IsActive"]);
				this.IsAutolandRewards = bool.Parse(data["IsAutolandRewards"]);
				this.VendorId_Mailhouse = int.Parse(data["VendorId_Mailhouse"]);
				this.NoteId = int.Parse(data["NoteId"]);
				this.AffinityGroupWebProfileId = int.Parse(data["AffinityGroupWebProfileId"]);
				this.AccountingControlId = int.Parse(data["AccountingControlId"]);
				this.isWww2 = (int.Parse(data["IsWww2"]) == 1 ? true : false);
				this.isInventoryOnly = bool.Parse(data["IsInventoryOnly"]);
				this.IsDealerDirect = bool.Parse(data["IsDealerDirect"]);
				this.IsLicensedGroup = bool.Parse(data["IsLicensedGroup"]);
				this.IsNewConsumerSite = bool.Parse(data["IsNewConsumerSite"]);
				this.LicensedGroupEmailLeads = data["LicensedGroupEmailLeads"];
				this.IsLendingManual = bool.Parse(data["IsLendingManual"]);
				this.IsLendingCudl = bool.Parse(data["IsLendingCudl"]);
				this.LendingCudlId = int.Parse(data["LendingCudlId"]);
				this.IsLendingRouteOne = bool.Parse(data["IsLendingRouteOne"]);
				this.LendingRouteOneId = int.Parse(data["LendingRouteOneId"]);
				this.IsLendingDealerTrack = bool.Parse(data["IsLendingDealerTrack"]);
				this.LendingDealerTrackId = int.Parse(data["LendingDealerTrackId"]);
				this.LendingMinimumScore = int.Parse(data["LendingMinimumScore"]);
				this.LendingMinimumYear = int.Parse(data["LendingMinimumYear"]);
				this.IsBank = bool.Parse(data["IsBank"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int affinityGroupId = this.AffinityGroupId;
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, affinityGroupId.ToString());
			affinityGroupId = this.AffinityGroupBranchId;
			data.AddParam("@AffinityGroupBranchId", DataAccessParameterType.Numeric, affinityGroupId.ToString());
			data.AddParam("@Code", DataAccessParameterType.Text, this.Code);
			affinityGroupId = this.OldId;
			data.AddParam("@OldId", DataAccessParameterType.Numeric, affinityGroupId.ToString());
			data.AddParam("@GroupName", DataAccessParameterType.Text, this.GroupName);
			data.AddParam("@TollFreePhone", DataAccessParameterType.Text, this.TollFreePhone);
			bool isAnonymousUser = this.IsAnonymousUser;
			data.AddParam("@IsAnonymousUser", DataAccessParameterType.Bool, isAnonymousUser.ToString());
			affinityGroupId = this.AnonymousUserCount;
			data.AddParam("@AnonymousUserCount", DataAccessParameterType.Numeric, affinityGroupId.ToString());
			data.AddParam("@InterestRate", DataAccessParameterType.Numeric, this.InterestRate.ToString());
			isAnonymousUser = this.IsActive;
			data.AddParam("@IsActive", DataAccessParameterType.Bool, isAnonymousUser.ToString());
			isAnonymousUser = this.IsAutolandRewards;
			data.AddParam("@IsAutolandRewards", DataAccessParameterType.Bool, isAnonymousUser.ToString());
			affinityGroupId = this.VendorId_Mailhouse;
			data.AddParam("@VendorId_Mailhouse", DataAccessParameterType.Numeric, affinityGroupId.ToString());
			affinityGroupId = this.NoteId;
			data.AddParam("@NoteId", DataAccessParameterType.Numeric, affinityGroupId.ToString());
			affinityGroupId = this.AffinityGroupWebProfileId;
			data.AddParam("@AffinityGroupWebProfileId", DataAccessParameterType.Numeric, affinityGroupId.ToString());
			affinityGroupId = this.AccountingControlId;
			data.AddParam("@AccountingControlId", DataAccessParameterType.Numeric, affinityGroupId.ToString());
			isAnonymousUser = this.IsInventoryOnly;
			data.AddParam("@IsInventoryOnly", DataAccessParameterType.Bool, isAnonymousUser.ToString());
			isAnonymousUser = this.IsDealerDirect;
			data.AddParam("@IsDealerDirect", DataAccessParameterType.Bool, isAnonymousUser.ToString());
			isAnonymousUser = this.IsNewConsumerSite;
			data.AddParam("@IsNewConsumerSite", DataAccessParameterType.Bool, isAnonymousUser.ToString());
			isAnonymousUser = this.IsLicensedGroup;
			data.AddParam("@IsLicensedGroup", DataAccessParameterType.Bool, isAnonymousUser.ToString());
			data.AddParam("@LicensedGroupEmailLeads", DataAccessParameterType.Text, this.LicensedGroupEmailLeads);
			isAnonymousUser = this.IsLendingManual;
			data.AddParam("@IsLendingManual", DataAccessParameterType.Bool, isAnonymousUser.ToString());
			isAnonymousUser = this.IsLendingCudl;
			data.AddParam("@IsLendingCudl", DataAccessParameterType.Bool, isAnonymousUser.ToString());
			affinityGroupId = this.LendingCudlId;
			data.AddParam("@LendingCudlId", DataAccessParameterType.Numeric, affinityGroupId.ToString());
			isAnonymousUser = this.IsLendingRouteOne;
			data.AddParam("@IsLendingRouteOne", DataAccessParameterType.Bool, isAnonymousUser.ToString());
			affinityGroupId = this.LendingRouteOneId;
			data.AddParam("@LendingRouteOneId", DataAccessParameterType.Numeric, affinityGroupId.ToString());
			isAnonymousUser = this.IsLendingDealerTrack;
			data.AddParam("@IsLendingDealerTrack", DataAccessParameterType.Bool, isAnonymousUser.ToString());
			affinityGroupId = this.LendingDealerTrackId;
			data.AddParam("@LendingDealerTrackId", DataAccessParameterType.Numeric, affinityGroupId.ToString());
			affinityGroupId = this.LendingMinimumScore;
			data.AddParam("@LendingMinimumScore", DataAccessParameterType.Numeric, affinityGroupId.ToString());
			affinityGroupId = this.LendingMinimumYear;
			data.AddParam("@LendingMinimumYear", DataAccessParameterType.Numeric, affinityGroupId.ToString());
			isAnonymousUser = this.IsBank;
			data.AddParam("@IsBank", DataAccessParameterType.Bool, isAnonymousUser.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("AFFINITY_GROUP_InsertUpdate");
			if (!data.EOF)
			{
				this.AffinityGroupId = int.Parse(data["AffinityGroupId"]);
			}
			this.retrievedata();
		}
	}
}