using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class ProductReceipt : Transaction
	{
		private int productReceiptId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int productFamilyId = 0;

		private string productContractList = "";

		private int productContractCount = 0;

		private int lookupId_ProductSeller = 0;

		private int affinityGroupId = 0;

		private string reference = "";

		private string receivedDate = "";

		private float premiumTotal = 0f;

		private float soldAtTotal = 0f;

		private float withholdingPercent = 0f;

		private float withholdingAmount = 0f;

		private float withholdingOverage = 0f;

		private float receivedAmount = 0f;

		private float amountDue = 0f;

		private string notes = "";

		private bool isCompleted = false;

		private bool isReversible = false;

		private string reportExtranetRemittance = "";

		private string reportFinalRemittance = "";

		public int AffinityGroupId
		{
			get
			{
				return this.affinityGroupId;
			}
			set
			{
				this.affinityGroupId = value;
			}
		}

		public float AmountDue
		{
			get
			{
				return this.amountDue;
			}
			set
			{
				this.amountDue = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public bool IsCompleted
		{
			get
			{
				return this.isCompleted;
			}
			set
			{
				this.isCompleted = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public bool IsReversible
		{
			get
			{
				return this.isReversible;
			}
			set
			{
				this.isReversible = value;
			}
		}

		public int LookupId_ProductSeller
		{
			get
			{
				return this.lookupId_ProductSeller;
			}
			set
			{
				this.lookupId_ProductSeller = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public string Notes
		{
			get
			{
				return this.notes;
			}
			set
			{
				this.notes = value;
			}
		}

		public float PremiumTotal
		{
			get
			{
				return this.premiumTotal;
			}
			set
			{
				this.premiumTotal = value;
			}
		}

		public int ProductContractCount
		{
			get
			{
				return this.productContractCount;
			}
			set
			{
				this.productContractCount = value;
			}
		}

		public string ProductContractList
		{
			get
			{
				return this.productContractList;
			}
			set
			{
				this.productContractList = value;
			}
		}

		public int ProductFamilyId
		{
			get
			{
				return this.productFamilyId;
			}
			set
			{
				this.productFamilyId = value;
			}
		}

		public int ProductReceiptId
		{
			get
			{
				return this.productReceiptId;
			}
			set
			{
				this.productReceiptId = value;
			}
		}

		public float ReceivedAmount
		{
			get
			{
				return this.receivedAmount;
			}
			set
			{
				this.receivedAmount = value;
			}
		}

		public string ReceivedDate
		{
			get
			{
				return this.receivedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.receivedDate = "";
				}
				else
				{
					this.receivedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string Reference
		{
			get
			{
				return this.reference;
			}
			set
			{
				this.reference = value;
			}
		}

		public string ReportExtranetRemittance
		{
			get
			{
				return this.reportExtranetRemittance;
			}
			set
			{
				this.reportExtranetRemittance = value;
			}
		}

		public string ReportFinalRemittance
		{
			get
			{
				return this.reportFinalRemittance;
			}
			set
			{
				this.reportFinalRemittance = value;
			}
		}

		public float SoldAtTotal
		{
			get
			{
				return this.soldAtTotal;
			}
			set
			{
				this.soldAtTotal = value;
			}
		}

		public float WithholdingAmount
		{
			get
			{
				return this.withholdingAmount;
			}
			set
			{
				this.withholdingAmount = value;
			}
		}

		public float WithholdingOverage
		{
			get
			{
				return this.withholdingOverage;
			}
			set
			{
				this.withholdingOverage = value;
			}
		}

		public float WithholdingPercent
		{
			get
			{
				return this.withholdingPercent;
			}
			set
			{
				this.withholdingPercent = value;
			}
		}

		public ProductReceipt()
		{
		}

		public ProductReceipt(string connection, string modifiedUserID, int productReceiptId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.ProductReceiptId = productReceiptId;
			if (this.ProductReceiptId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ProductReceiptId", DataAccessParameterType.Numeric, this.ProductReceiptId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("PRODUCT_RECEIPT_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ProductReceiptId", DataAccessParameterType.Numeric, this.ProductReceiptId.ToString());
			data.ExecuteProcedure("PRODUCT_RECEIPT_GetRecord");
			if (!data.EOF)
			{
				this.ProductReceiptId = int.Parse(data["ProductReceiptId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.ProductFamilyId = int.Parse(data["ProductFamilyId"]);
				this.LookupId_ProductSeller = int.Parse(data["LookupId_ProductSeller"]);
				this.AffinityGroupId = int.Parse(data["AffinityGroupId"]);
				this.Reference = data["Reference"];
				this.ReceivedDate = data["ReceivedDate"];
				this.PremiumTotal = float.Parse(data["PremiumTotal"]);
				this.SoldAtTotal = float.Parse(data["SoldAtTotal"]);
				this.WithholdingPercent = float.Parse(data["WithholdingPercent"]);
				this.WithholdingAmount = float.Parse(data["WithholdingAmount"]);
				this.WithholdingOverage = float.Parse(data["WithholdingOverage"]);
				this.ReceivedAmount = float.Parse(data["ReceivedAmount"]);
				this.AmountDue = float.Parse(data["AmountDue"]);
				this.Notes = data["Notes"];
				this.IsCompleted = bool.Parse(data["IsCompleted"]);
				this.ProductContractList = data["ProductContractList"];
				this.ProductContractCount = int.Parse(data["ProductContractCount"]);
				this.isReversible = bool.Parse(data["IsReversible"]);
				this.ReportExtranetRemittance = data["ReportExtranetRemittance"];
				this.ReportFinalRemittance = data["ReportFinalRemittance"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int productReceiptId = this.ProductReceiptId;
			data.AddParam("@ProductReceiptId", DataAccessParameterType.Numeric, productReceiptId.ToString());
			productReceiptId = this.ProductFamilyId;
			data.AddParam("@ProductFamilyId", DataAccessParameterType.Numeric, productReceiptId.ToString());
			productReceiptId = this.LookupId_ProductSeller;
			data.AddParam("@LookupId_ProductSeller", DataAccessParameterType.Numeric, productReceiptId.ToString());
			productReceiptId = this.AffinityGroupId;
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, productReceiptId.ToString());
			data.AddParam("@Reference", DataAccessParameterType.Text, this.Reference);
			data.AddParam("@ReceivedDate", DataAccessParameterType.DateTime, this.ReceivedDate);
			float premiumTotal = this.PremiumTotal;
			data.AddParam("@PremiumTotal", DataAccessParameterType.Numeric, premiumTotal.ToString());
			premiumTotal = this.SoldAtTotal;
			data.AddParam("@SoldAtTotal", DataAccessParameterType.Numeric, premiumTotal.ToString());
			premiumTotal = this.WithholdingPercent;
			data.AddParam("@WithholdingPercent", DataAccessParameterType.Numeric, premiumTotal.ToString());
			premiumTotal = this.WithholdingAmount;
			data.AddParam("@WithholdingAmount", DataAccessParameterType.Numeric, premiumTotal.ToString());
			premiumTotal = this.WithholdingOverage;
			data.AddParam("@WithholdingOverage", DataAccessParameterType.Numeric, premiumTotal.ToString());
			premiumTotal = this.ReceivedAmount;
			data.AddParam("@ReceivedAmount", DataAccessParameterType.Numeric, premiumTotal.ToString());
			premiumTotal = this.AmountDue;
			data.AddParam("@AmountDue", DataAccessParameterType.Numeric, premiumTotal.ToString());
			data.AddParam("@Notes", DataAccessParameterType.Text, this.Notes);
			data.AddParam("@IsCompleted", DataAccessParameterType.Bool, this.IsCompleted.ToString());
			data.AddParam("@ProductContractList", DataAccessParameterType.Text, this.ProductContractList);
			data.AddParam("@ReportExtranetRemittance", DataAccessParameterType.Text, this.ReportExtranetRemittance);
			data.AddParam("@ReportFinalRemittance", DataAccessParameterType.Text, this.ReportFinalRemittance);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("PRODUCT_RECEIPT_InsertUpdate");
			if (!data.EOF)
			{
				this.ProductReceiptId = int.Parse(data["ProductReceiptId"]);
			}
			this.retrievedata();
		}
	}
}