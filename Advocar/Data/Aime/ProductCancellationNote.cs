using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class ProductCancellationNote : Transaction
	{
		private int productCancellationNoteId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int productCancellationId = 0;

		private string notes = "";

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public string Notes
		{
			get
			{
				return this.notes;
			}
			set
			{
				this.notes = value;
			}
		}

		public int ProductCancellationId
		{
			get
			{
				return this.productCancellationId;
			}
			set
			{
				this.productCancellationId = value;
			}
		}

		public int ProductCancellationNoteId
		{
			get
			{
				return this.productCancellationNoteId;
			}
			set
			{
				this.productCancellationNoteId = value;
			}
		}

		public ProductCancellationNote()
		{
		}

		public ProductCancellationNote(string connection, string modifiedUserID, int productCancellationNoteId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.ProductCancellationNoteId = productCancellationNoteId;
			if (this.ProductCancellationNoteId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ProductCancellationNoteId", DataAccessParameterType.Numeric, this.ProductCancellationNoteId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("PRODUCT_CANCELLATION_NOTE_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ProductCancellationNoteId", DataAccessParameterType.Numeric, this.ProductCancellationNoteId.ToString());
			data.ExecuteProcedure("PRODUCT_CANCELLATION_NOTE_GetRecord");
			if (!data.EOF)
			{
				this.ProductCancellationNoteId = int.Parse(data["ProductCancellationNoteId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.ProductCancellationId = int.Parse(data["ProductCancellationId"]);
				this.Notes = data["Notes"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int productCancellationNoteId = this.ProductCancellationNoteId;
			data.AddParam("@ProductCancellationNoteId", DataAccessParameterType.Numeric, productCancellationNoteId.ToString());
			productCancellationNoteId = this.ProductCancellationId;
			data.AddParam("@ProductCancellationId", DataAccessParameterType.Numeric, productCancellationNoteId.ToString());
			data.AddParam("@Notes", DataAccessParameterType.Text, this.Notes);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("PRODUCT_CANCELLATION_NOTE_InsertUpdate");
			if (!data.EOF)
			{
				this.ProductCancellationNoteId = int.Parse(data["ProductCancellationNoteId"]);
			}
			this.retrievedata();
		}
	}
}