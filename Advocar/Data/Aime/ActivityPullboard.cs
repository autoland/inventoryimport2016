using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class ActivityPullboard : Transaction
	{
		private int activityPullboardId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int activityId = 0;

		private int affinityGroupId = 0;

		private int memberId = 0;

		private int affinityGroupBranchId = 0;

		private int affinityGroupContactId = 0;

		private int affinityGroupPullboardId = 0;

		private int noOfReferrals = 0;

		private bool isWinner = false;

		private string passcode = "";

		public int ActivityId
		{
			get
			{
				return this.activityId;
			}
			set
			{
				this.activityId = value;
			}
		}

		public int ActivityPullboardId
		{
			get
			{
				return this.activityPullboardId;
			}
			set
			{
				this.activityPullboardId = value;
			}
		}

		public int AffinityGroupBranchId
		{
			get
			{
				return this.affinityGroupBranchId;
			}
			set
			{
				this.affinityGroupBranchId = value;
			}
		}

		public int AffinityGroupContactId
		{
			get
			{
				return this.affinityGroupContactId;
			}
			set
			{
				this.affinityGroupContactId = value;
			}
		}

		public int AffinityGroupId
		{
			get
			{
				return this.affinityGroupId;
			}
			set
			{
				this.affinityGroupId = value;
			}
		}

		public int AffinityGroupPullboardId
		{
			get
			{
				return this.affinityGroupPullboardId;
			}
			set
			{
				this.affinityGroupPullboardId = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public bool IsWinner
		{
			get
			{
				return this.isWinner;
			}
			set
			{
				this.isWinner = value;
			}
		}

		public int MemberId
		{
			get
			{
				return this.memberId;
			}
			set
			{
				this.memberId = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public int NoOfReferrals
		{
			get
			{
				return this.noOfReferrals;
			}
			set
			{
				this.noOfReferrals = value;
			}
		}

		public string Passcode
		{
			get
			{
				return this.passcode;
			}
			set
			{
				this.passcode = value;
			}
		}

		public ActivityPullboard()
		{
		}

		public ActivityPullboard(string connection, string modifiedUserID, int activityPullboardId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.ActivityPullboardId = activityPullboardId;
			if (this.ActivityPullboardId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ActivityPullboardId", DataAccessParameterType.Numeric, this.ActivityPullboardId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("ACTIVITY_PULLBOARD_Delete");
			this.wipeout();
		}

		public static int GetRecordId(int activityId, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@ActivityId", DataAccessParameterType.Numeric, activityId.ToString());
			data.ExecuteProcedure("ACTIVITY_PULLBOARD_GetRecord");
			num = (!data.EOF ? int.Parse(data["ActivityPullboardId"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ActivityPullboardId", DataAccessParameterType.Numeric, this.ActivityPullboardId.ToString());
			data.ExecuteProcedure("ACTIVITY_PULLBOARD_GetRecord");
			if (!data.EOF)
			{
				this.ActivityPullboardId = int.Parse(data["ActivityPullboardId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.ActivityId = int.Parse(data["ActivityId"]);
				this.AffinityGroupId = int.Parse(data["AffinityGroupId"]);
				this.MemberId = int.Parse(data["MemberId"]);
				this.AffinityGroupBranchId = int.Parse(data["AffinityGroupBranchId"]);
				this.AffinityGroupContactId = int.Parse(data["AffinityGroupContactId"]);
				this.AffinityGroupPullboardId = int.Parse(data["AffinityGroupPullboardId"]);
				this.NoOfReferrals = int.Parse(data["NoOfReferrals"]);
				this.IsWinner = bool.Parse(data["IsWinner"]);
				this.Passcode = data["Passcode"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int activityPullboardId = this.ActivityPullboardId;
			data.AddParam("@ActivityPullboardId", DataAccessParameterType.Numeric, activityPullboardId.ToString());
			activityPullboardId = this.ActivityId;
			data.AddParam("@ActivityId", DataAccessParameterType.Numeric, activityPullboardId.ToString());
			activityPullboardId = this.AffinityGroupId;
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, activityPullboardId.ToString());
			activityPullboardId = this.MemberId;
			data.AddParam("@MemberId", DataAccessParameterType.Numeric, activityPullboardId.ToString());
			activityPullboardId = this.AffinityGroupBranchId;
			data.AddParam("@AffinityGroupBranchId", DataAccessParameterType.Numeric, activityPullboardId.ToString());
			activityPullboardId = this.AffinityGroupContactId;
			data.AddParam("@AffinityGroupContactId", DataAccessParameterType.Numeric, activityPullboardId.ToString());
			activityPullboardId = this.AffinityGroupPullboardId;
			data.AddParam("@AffinityGroupPullboardId", DataAccessParameterType.Numeric, activityPullboardId.ToString());
			activityPullboardId = this.NoOfReferrals;
			data.AddParam("@NoOfReferrals", DataAccessParameterType.Numeric, activityPullboardId.ToString());
			data.AddParam("@IsWinner", DataAccessParameterType.Bool, this.IsWinner.ToString());
			data.AddParam("@Passcode", DataAccessParameterType.Text, this.Passcode);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("ACTIVITY_PULLBOARD_InsertUpdate");
			if (!data.EOF)
			{
				this.ActivityPullboardId = int.Parse(data["ActivityPullboardId"]);
			}
			this.retrievedata();
		}
	}
}