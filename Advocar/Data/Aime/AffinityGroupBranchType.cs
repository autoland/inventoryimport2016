using System;

namespace Advocar.Data.Aime
{
	public enum AffinityGroupBranchType
	{
		Main,
		Lienholder,
		Groupholder
	}
}