using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class AffinityGroupSiteMenu : Transaction
	{
		private int affinityGroupSiteMenuId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int siteMenuId = 0;

		private string siteMenuCode = "";

		private int affinityGroupId = 0;

		private string alternateUrl = "";

		private bool isActive = false;

		public int AffinityGroupId
		{
			get
			{
				return this.affinityGroupId;
			}
			set
			{
				this.affinityGroupId = value;
			}
		}

		public int AffinityGroupSiteMenuId
		{
			get
			{
				return this.affinityGroupSiteMenuId;
			}
			set
			{
				this.affinityGroupSiteMenuId = value;
			}
		}

		public string AlternateUrl
		{
			get
			{
				return this.alternateUrl;
			}
			set
			{
				this.alternateUrl = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public bool IsActive
		{
			get
			{
				return this.isActive;
			}
			set
			{
				this.isActive = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public string SiteMenuCode
		{
			get
			{
				return this.siteMenuCode;
			}
		}

		public int SiteMenuId
		{
			get
			{
				return this.siteMenuId;
			}
			set
			{
				this.siteMenuId = value;
			}
		}

		public AffinityGroupSiteMenu()
		{
		}

		public AffinityGroupSiteMenu(string connection, string modifiedUserID, int affinityGroupSiteMenuId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.AffinityGroupSiteMenuId = affinityGroupSiteMenuId;
			if (this.AffinityGroupSiteMenuId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@AffinityGroupSiteMenuId", DataAccessParameterType.Numeric, this.AffinityGroupSiteMenuId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("AFFINITY_GROUP_SITE_MENU_Delete");
			this.wipeout();
		}

		public static int GetRecordId(string code, int affinityGroupId, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@Code", DataAccessParameterType.Text, code);
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, affinityGroupId.ToString());
			data.ExecuteProcedure("AFFINITY_GROUP_SITE_MENU_GetRecord");
			num = (!data.EOF ? int.Parse(data["AffinityGroupSiteMenuId"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@AffinityGroupSiteMenuId", DataAccessParameterType.Numeric, this.AffinityGroupSiteMenuId.ToString());
			data.ExecuteProcedure("AFFINITY_GROUP_SITE_MENU_GetRecord");
			if (!data.EOF)
			{
				this.AffinityGroupSiteMenuId = int.Parse(data["AffinityGroupSiteMenuId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.SiteMenuId = int.Parse(data["SiteMenuId"]);
				this.AffinityGroupId = int.Parse(data["AffinityGroupId"]);
				this.AlternateUrl = data["AlternateUrl"];
				this.IsActive = bool.Parse(data["IsActive"]);
				this.siteMenuCode = data["SiteMenuCode"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int affinityGroupSiteMenuId = this.AffinityGroupSiteMenuId;
			data.AddParam("@AffinityGroupSiteMenuId", DataAccessParameterType.Numeric, affinityGroupSiteMenuId.ToString());
			affinityGroupSiteMenuId = this.SiteMenuId;
			data.AddParam("@SiteMenuId", DataAccessParameterType.Numeric, affinityGroupSiteMenuId.ToString());
			affinityGroupSiteMenuId = this.AffinityGroupId;
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, affinityGroupSiteMenuId.ToString());
			data.AddParam("@AlternateUrl", DataAccessParameterType.Text, this.AlternateUrl);
			data.AddParam("@IsActive", DataAccessParameterType.Bool, this.IsActive.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("AFFINITY_GROUP_SITE_MENU_InsertUpdate");
			if (!data.EOF)
			{
				this.AffinityGroupSiteMenuId = int.Parse(data["AffinityGroupSiteMenuId"]);
			}
			this.retrievedata();
		}
	}
}