using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class PhoneLog : Transaction
	{
		private int phoneLogId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private string customerName = "";

		private string phone = "";

		private int affinityGroupId = 0;

		private int lookupId_CallLogType = 0;

		private int lookupId_CallLogCategory = 0;

		private float callLengthMinutes = 0f;

		private string notes = "";

		public int AffinityGroupId
		{
			get
			{
				return this.affinityGroupId;
			}
			set
			{
				this.affinityGroupId = value;
			}
		}

		public float CallLengthMinutes
		{
			get
			{
				return this.callLengthMinutes;
			}
			set
			{
				this.callLengthMinutes = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string CustomerName
		{
			get
			{
				return this.customerName;
			}
			set
			{
				this.customerName = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public int LookupId_CallLogCategory
		{
			get
			{
				return this.lookupId_CallLogCategory;
			}
			set
			{
				this.lookupId_CallLogCategory = value;
			}
		}

		public int LookupId_CallLogType
		{
			get
			{
				return this.lookupId_CallLogType;
			}
			set
			{
				this.lookupId_CallLogType = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public string Notes
		{
			get
			{
				return this.notes;
			}
			set
			{
				this.notes = value;
			}
		}

		public string Phone
		{
			get
			{
				return this.phone;
			}
			set
			{
				this.phone = value;
			}
		}

		public int PhoneLogId
		{
			get
			{
				return this.phoneLogId;
			}
			set
			{
				this.phoneLogId = value;
			}
		}

		public PhoneLog()
		{
		}

		public PhoneLog(string connection, string modifiedUserID, int phoneLogId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "PHONE_LOG";
			this.PhoneLogId = phoneLogId;
			if (this.PhoneLogId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@PhoneLogId", DataAccessParameterType.Numeric, this.PhoneLogId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("PHONE_LOG_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@PhoneLogId", DataAccessParameterType.Numeric, this.PhoneLogId.ToString());
			data.ExecuteProcedure("PHONE_LOG_GetRecord");
			if (!data.EOF)
			{
				this.PhoneLogId = int.Parse(data["PhoneLogId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.CustomerName = data["CustomerName"];
				this.Phone = data["Phone"];
				this.AffinityGroupId = int.Parse(data["AffinityGroupId"]);
				this.LookupId_CallLogType = int.Parse(data["LookupId_CallLogType"]);
				this.LookupId_CallLogCategory = int.Parse(data["LookupId_CallLogCategory"]);
				this.CallLengthMinutes = float.Parse(data["CallLengthMinutes"]);
				this.Notes = data["Notes"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int phoneLogId = this.PhoneLogId;
			data.AddParam("@PhoneLogId", DataAccessParameterType.Numeric, phoneLogId.ToString());
			data.AddParam("@CustomerName", DataAccessParameterType.Text, this.CustomerName);
			data.AddParam("@Phone", DataAccessParameterType.Text, this.Phone);
			phoneLogId = this.AffinityGroupId;
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, phoneLogId.ToString());
			phoneLogId = this.LookupId_CallLogType;
			data.AddParam("@LookupId_CallLogType", DataAccessParameterType.Numeric, phoneLogId.ToString());
			phoneLogId = this.LookupId_CallLogCategory;
			data.AddParam("@LookupId_CallLogCategory", DataAccessParameterType.Numeric, phoneLogId.ToString());
			data.AddParam("@CallLengthMinutes", DataAccessParameterType.Numeric, this.CallLengthMinutes.ToString());
			data.AddParam("@Notes", DataAccessParameterType.Text, this.Notes);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("PHONE_LOG_InsertUpdate");
			if (!data.EOF)
			{
				this.PhoneLogId = int.Parse(data["PhoneLogId"]);
			}
			this.retrievedata();
		}
	}
}