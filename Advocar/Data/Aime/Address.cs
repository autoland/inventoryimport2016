using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class Address : Transaction
	{
		private int addressId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private string address = "";

		private string address2 = "";

		private string city = "";

		private string stateCode = "";

		private int lookupId_State = 0;

		private string zipCode = "";

		private int zipCodeId = 0;

		private int zipCodeCityId = 0;

		private string phone = "";

		private string phoneOther = "";

		private string phoneFax = "";

		private float longitude = 0f;

		private float latitude = 0f;

		public string Address1
		{
			get
			{
				return this.address;
			}
			set
			{
				this.address = value;
			}
		}

		public string Address2
		{
			get
			{
				return this.address2;
			}
			set
			{
				this.address2 = value;
			}
		}

		public int AddressId
		{
			get
			{
				return this.addressId;
			}
			set
			{
				this.addressId = value;
			}
		}

		public string City
		{
			get
			{
				return this.city;
			}
			set
			{
				this.city = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public float Latitude
		{
			get
			{
				return this.latitude;
			}
			set
			{
				this.latitude = value;
			}
		}

		public float Longitude
		{
			get
			{
				return this.longitude;
			}
			set
			{
				this.longitude = value;
			}
		}

		public int LookupId_State
		{
			get
			{
				return this.lookupId_State;
			}
			set
			{
				this.lookupId_State = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public string Phone
		{
			get
			{
				return this.phone;
			}
			set
			{
				this.phone = value;
			}
		}

		public string PhoneFax
		{
			get
			{
				return this.phoneFax;
			}
			set
			{
				this.phoneFax = value;
			}
		}

		public string PhoneOther
		{
			get
			{
				return this.phoneOther;
			}
			set
			{
				this.phoneOther = value;
			}
		}

		public string StateCode
		{
			get
			{
				return this.stateCode;
			}
			set
			{
				this.stateCode = value;
			}
		}

		public string ZipCode
		{
			get
			{
				return this.zipCode;
			}
			set
			{
				this.zipCode = value;
			}
		}

		public int ZipCodeCityId
		{
			get
			{
				return this.zipCodeCityId;
			}
			set
			{
				this.zipCodeCityId = value;
			}
		}

		public int ZipCodeId
		{
			get
			{
				return this.zipCodeId;
			}
			set
			{
				this.zipCodeId = value;
			}
		}

		public Address()
		{
		}

		public Address(string connection, string modifiedUserID, int addressId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.AddressId = addressId;
			if (this.AddressId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@AddressId", DataAccessParameterType.Numeric, this.AddressId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("ADDRESS_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@AddressId", DataAccessParameterType.Numeric, this.AddressId.ToString());
			data.ExecuteProcedure("ADDRESS_GetRecord");
			if (!data.EOF)
			{
				this.AddressId = int.Parse(data["AddressId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.Address1 = data["Address"];
				this.Address2 = data["Address2"];
				this.City = data["City"];
				this.StateCode = data["StateCode"];
				this.LookupId_State = int.Parse(data["LookupId_State"]);
				this.ZipCode = data["ZipCode"];
				this.ZipCodeId = int.Parse(data["ZipCodeId"]);
				this.ZipCodeCityId = int.Parse(data["ZipCodeCityId"]);
				this.Phone = data["Phone"];
				this.PhoneOther = data["PhoneOther"];
				this.PhoneFax = data["PhoneFax"];
				this.Longitude = float.Parse(data["Longitude"]);
				this.Latitude = float.Parse(data["Latitude"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int addressId = this.AddressId;
			data.AddParam("@AddressId", DataAccessParameterType.Numeric, addressId.ToString());
			data.AddParam("@Address", DataAccessParameterType.Text, this.Address1);
			data.AddParam("@Address2", DataAccessParameterType.Text, this.Address2);
			data.AddParam("@City", DataAccessParameterType.Text, this.City);
			data.AddParam("@StateCode", DataAccessParameterType.Text, this.StateCode);
			addressId = this.LookupId_State;
			data.AddParam("@LookupId_State", DataAccessParameterType.Numeric, addressId.ToString());
			data.AddParam("@ZipCode", DataAccessParameterType.Text, this.ZipCode);
			addressId = this.ZipCodeId;
			data.AddParam("@ZipCodeId", DataAccessParameterType.Numeric, addressId.ToString());
			addressId = this.ZipCodeCityId;
			data.AddParam("@ZipCodeCityId", DataAccessParameterType.Numeric, addressId.ToString());
			data.AddParam("@Phone", DataAccessParameterType.Text, this.Phone);
			data.AddParam("@PhoneOther", DataAccessParameterType.Text, this.PhoneOther);
			data.AddParam("@PhoneFax", DataAccessParameterType.Text, this.PhoneFax);
			float longitude = this.Longitude;
			data.AddParam("@Longitude", DataAccessParameterType.Numeric, longitude.ToString());
			longitude = this.Latitude;
			data.AddParam("@Latitude", DataAccessParameterType.Numeric, longitude.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("ADDRESS_InsertUpdate");
			if (!data.EOF)
			{
				this.AddressId = int.Parse(data["AddressId"]);
			}
			this.retrievedata();
		}
	}
}