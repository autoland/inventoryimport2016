using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class AffinityGroupSurveyQuestion : Transaction
	{
		private int affinityGroupSurveyQuestionId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int affinityGroupId = 0;

		private int surveyQuestionId = 0;

		private string msreplTranVersion = "";

		public int AffinityGroupId
		{
			get
			{
				return this.affinityGroupId;
			}
			set
			{
				this.affinityGroupId = value;
			}
		}

		public int AffinityGroupSurveyQuestionId
		{
			get
			{
				return this.affinityGroupSurveyQuestionId;
			}
			set
			{
				this.affinityGroupSurveyQuestionId = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public string MsreplTranVersion
		{
			get
			{
				return this.msreplTranVersion;
			}
			set
			{
				this.msreplTranVersion = value;
			}
		}

		public int SurveyQuestionId
		{
			get
			{
				return this.surveyQuestionId;
			}
			set
			{
				this.surveyQuestionId = value;
			}
		}

		public AffinityGroupSurveyQuestion()
		{
		}

		public AffinityGroupSurveyQuestion(string connection, string modifiedUserID, int affinityGroupSurveyQuestionId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "AFFINITY_GROUP_SURVEY_QUESTION";
			this.AffinityGroupSurveyQuestionId = affinityGroupSurveyQuestionId;
			if (this.AffinityGroupSurveyQuestionId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@AffinityGroupSurveyQuestionId", DataAccessParameterType.Numeric, this.AffinityGroupSurveyQuestionId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("AFFINITY_GROUP_SURVEY_QUESTION_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@AffinityGroupSurveyQuestionId", DataAccessParameterType.Numeric, this.AffinityGroupSurveyQuestionId.ToString());
			data.ExecuteProcedure("AFFINITY_GROUP_SURVEY_QUESTION_GetRecord");
			if (!data.EOF)
			{
				this.AffinityGroupSurveyQuestionId = int.Parse(data["AffinityGroupSurveyQuestionId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.AffinityGroupId = int.Parse(data["AffinityGroupId"]);
				this.SurveyQuestionId = int.Parse(data["SurveyQuestionId"]);
				this.MsreplTranVersion = data["MsreplTranVersion"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int affinityGroupSurveyQuestionId = this.AffinityGroupSurveyQuestionId;
			data.AddParam("@AffinityGroupSurveyQuestionId", DataAccessParameterType.Numeric, affinityGroupSurveyQuestionId.ToString());
			affinityGroupSurveyQuestionId = this.AffinityGroupId;
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, affinityGroupSurveyQuestionId.ToString());
			affinityGroupSurveyQuestionId = this.SurveyQuestionId;
			data.AddParam("@SurveyQuestionId", DataAccessParameterType.Numeric, affinityGroupSurveyQuestionId.ToString());
			data.AddParam("@MsreplTranVersion", DataAccessParameterType.Text, this.MsreplTranVersion);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("AFFINITY_GROUP_SURVEY_QUESTION_InsertUpdate");
			if (!data.EOF)
			{
				this.AffinityGroupSurveyQuestionId = int.Parse(data["AffinityGroupSurveyQuestionId"]);
			}
			this.retrievedata();
		}
	}
}