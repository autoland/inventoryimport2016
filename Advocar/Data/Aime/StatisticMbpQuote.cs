using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class StatisticMbpQuote : Transaction
	{
		private int statisticMbpQuoteId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int activityId = 0;

		private int affinityGroupId = 0;

		private int affinityGroupBranchId = 0;

		private int worksheetId = 0;

		private int productId = 0;

		private int configurator_YearId = 0;

		private int productMbpMakeId = 0;

		private int productMbpModelId = 0;

		private int lookupId_MbpMileageBand = 0;

		private int termMiles = 0;

		private int termMonths = 0;

		public int ActivityId
		{
			get
			{
				return this.activityId;
			}
			set
			{
				this.activityId = value;
			}
		}

		public int AffinityGroupBranchId
		{
			get
			{
				return this.affinityGroupBranchId;
			}
			set
			{
				this.affinityGroupBranchId = value;
			}
		}

		public int AffinityGroupId
		{
			get
			{
				return this.affinityGroupId;
			}
			set
			{
				this.affinityGroupId = value;
			}
		}

		public int Configurator_YearId
		{
			get
			{
				return this.configurator_YearId;
			}
			set
			{
				this.configurator_YearId = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public int LookupId_MbpMileageBand
		{
			get
			{
				return this.lookupId_MbpMileageBand;
			}
			set
			{
				this.lookupId_MbpMileageBand = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public int ProductId
		{
			get
			{
				return this.productId;
			}
			set
			{
				this.productId = value;
			}
		}

		public int ProductMbpMakeId
		{
			get
			{
				return this.productMbpMakeId;
			}
			set
			{
				this.productMbpMakeId = value;
			}
		}

		public int ProductMbpModelId
		{
			get
			{
				return this.productMbpModelId;
			}
			set
			{
				this.productMbpModelId = value;
			}
		}

		public int StatisticMbpQuoteId
		{
			get
			{
				return this.statisticMbpQuoteId;
			}
			set
			{
				this.statisticMbpQuoteId = value;
			}
		}

		public int TermMiles
		{
			get
			{
				return this.termMiles;
			}
			set
			{
				this.termMiles = value;
			}
		}

		public int TermMonths
		{
			get
			{
				return this.termMonths;
			}
			set
			{
				this.termMonths = value;
			}
		}

		public int WorksheetId
		{
			get
			{
				return this.worksheetId;
			}
			set
			{
				this.worksheetId = value;
			}
		}

		public StatisticMbpQuote()
		{
		}

		public StatisticMbpQuote(string connection, string modifiedUserID, int statisticMbpQuoteId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.StatisticMbpQuoteId = statisticMbpQuoteId;
			if (this.StatisticMbpQuoteId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@StatisticMbpQuoteId", DataAccessParameterType.Numeric, this.StatisticMbpQuoteId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("STATISTIC_MBP_QUOTE_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@StatisticMbpQuoteId", DataAccessParameterType.Numeric, this.StatisticMbpQuoteId.ToString());
			data.ExecuteProcedure("STATISTIC_MBP_QUOTE_GetRecord");
			if (!data.EOF)
			{
				this.StatisticMbpQuoteId = int.Parse(data["StatisticMbpQuoteId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.ActivityId = int.Parse(data["ActivityId"]);
				this.AffinityGroupId = int.Parse(data["AffinityGroupId"]);
				this.AffinityGroupBranchId = int.Parse(data["AffinityGroupBranchId"]);
				this.WorksheetId = int.Parse(data["WorksheetId"]);
				this.ProductId = int.Parse(data["ProductId"]);
				this.Configurator_YearId = int.Parse(data["Configurator_YearId"]);
				this.ProductMbpMakeId = int.Parse(data["ProductMbpMakeId"]);
				this.ProductMbpModelId = int.Parse(data["ProductMbpModelId"]);
				this.LookupId_MbpMileageBand = int.Parse(data["LookupId_MbpMileageBand"]);
				this.TermMiles = int.Parse(data["TermMiles"]);
				this.TermMonths = int.Parse(data["TermMonths"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int statisticMbpQuoteId = this.StatisticMbpQuoteId;
			data.AddParam("@StatisticMbpQuoteId", DataAccessParameterType.Numeric, statisticMbpQuoteId.ToString());
			statisticMbpQuoteId = this.ActivityId;
			data.AddParam("@ActivityId", DataAccessParameterType.Numeric, statisticMbpQuoteId.ToString());
			statisticMbpQuoteId = this.AffinityGroupId;
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, statisticMbpQuoteId.ToString());
			statisticMbpQuoteId = this.AffinityGroupBranchId;
			data.AddParam("@AffinityGroupBranchId", DataAccessParameterType.Numeric, statisticMbpQuoteId.ToString());
			statisticMbpQuoteId = this.WorksheetId;
			data.AddParam("@WorksheetId", DataAccessParameterType.Numeric, statisticMbpQuoteId.ToString());
			statisticMbpQuoteId = this.ProductId;
			data.AddParam("@ProductId", DataAccessParameterType.Numeric, statisticMbpQuoteId.ToString());
			statisticMbpQuoteId = this.Configurator_YearId;
			data.AddParam("@Configurator_YearId", DataAccessParameterType.Numeric, statisticMbpQuoteId.ToString());
			statisticMbpQuoteId = this.ProductMbpMakeId;
			data.AddParam("@ProductMbpMakeId", DataAccessParameterType.Numeric, statisticMbpQuoteId.ToString());
			statisticMbpQuoteId = this.ProductMbpModelId;
			data.AddParam("@ProductMbpModelId", DataAccessParameterType.Numeric, statisticMbpQuoteId.ToString());
			statisticMbpQuoteId = this.LookupId_MbpMileageBand;
			data.AddParam("@LookupId_MbpMileageBand", DataAccessParameterType.Numeric, statisticMbpQuoteId.ToString());
			statisticMbpQuoteId = this.TermMiles;
			data.AddParam("@TermMiles", DataAccessParameterType.Numeric, statisticMbpQuoteId.ToString());
			statisticMbpQuoteId = this.TermMonths;
			data.AddParam("@TermMonths", DataAccessParameterType.Numeric, statisticMbpQuoteId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("STATISTIC_MBP_QUOTE_InsertUpdate");
			if (!data.EOF)
			{
				this.StatisticMbpQuoteId = int.Parse(data["StatisticMbpQuoteId"]);
			}
			this.retrievedata();
		}
	}
}