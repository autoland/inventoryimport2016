using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class ProductCancellation : Transaction
	{
		private int productCancellationId = 0;

		private bool isDeleted = false;

		private string modifiedDate = "";

		private string modifiedUser = "";

		private string createdDate = "";

		private string createdUser = "";

		private int productContractId = 0;

		private bool isRequested = false;

		private string requestedDate = "";

		private bool isFormMailed = false;

		private string formMailedDate = "";

		private bool isFormReceived = false;

		private string formReceivedDate = "";

		private bool isConfirmMailed = false;

		private string confirmMailedDate = "";

		private bool isFundsReceived = false;

		private string fundsReceivedDate = "";

		private float fundsReceivedAmount = 0f;

		private float premiumAmount = 0f;

		private int mileageCurrent = 0;

		private string cancellationDate = "";

		private float refundPercent = 0f;

		private float refundCancellationFee = 0f;

		private float refundAmount = 0f;

		private float paymentAmount = 0f;

		private float refundAdministratorAmount = 0f;

		private float refundAffinityGroupAmount = 0f;

		private bool isRefundAffinityGroup = false;

		private int productPaymentId_AffinityGroup = 0;

		private float refundAgentAmount = 0f;

		private bool isRefundAgent = false;

		private int productPaymentId_Agent = 0;

		private int lookupId_ProductCancelReason = 0;

		private string reasonOther = "";

		private float refundAutolandAmount = 0f;

		private bool isRefundMailed = false;

		private string refundMailedDate = "";

		private string refundMailedName1 = "";

		private string refundMailedName2 = "";

		private string refundMailedAddress1 = "";

		private string refundMailedAddress2 = "";

		private string refundMailedCity = "";

		private string refundMailedState = "";

		private string refundMailedZipCode = "";

		private string notes = "";

		private bool isCompleted = false;

		private string reportCertificate = "";

		private string reportConfirmationLetter = "";

		private string reportRefundLetter = "";

		public string CancellationDate
		{
			get
			{
				return this.cancellationDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.cancellationDate = "";
				}
				else
				{
					this.cancellationDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ConfirmMailedDate
		{
			get
			{
				return this.confirmMailedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.confirmMailedDate = "";
				}
				else
				{
					this.confirmMailedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string FormMailedDate
		{
			get
			{
				return this.formMailedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.formMailedDate = "";
				}
				else
				{
					this.formMailedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string FormReceivedDate
		{
			get
			{
				return this.formReceivedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.formReceivedDate = "";
				}
				else
				{
					this.formReceivedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public float FundsReceivedAmount
		{
			get
			{
				return this.fundsReceivedAmount;
			}
			set
			{
				this.fundsReceivedAmount = value;
			}
		}

		public string FundsReceivedDate
		{
			get
			{
				return this.fundsReceivedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.fundsReceivedDate = "";
				}
				else
				{
					this.fundsReceivedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public bool IsCompleted
		{
			get
			{
				return this.isCompleted;
			}
			set
			{
				this.isCompleted = value;
			}
		}

		public bool IsConfirmMailed
		{
			get
			{
				return this.isConfirmMailed;
			}
			set
			{
				this.isConfirmMailed = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public bool IsFormMailed
		{
			get
			{
				return this.isFormMailed;
			}
			set
			{
				this.isFormMailed = value;
			}
		}

		public bool IsFormReceived
		{
			get
			{
				return this.isFormReceived;
			}
			set
			{
				this.isFormReceived = value;
			}
		}

		public bool IsFundsReceived
		{
			get
			{
				return this.isFundsReceived;
			}
			set
			{
				this.isFundsReceived = value;
			}
		}

		public bool IsRefundAffinityGroup
		{
			get
			{
				return this.isRefundAffinityGroup;
			}
			set
			{
				this.isRefundAffinityGroup = value;
			}
		}

		public bool IsRefundAgent
		{
			get
			{
				return this.isRefundAgent;
			}
			set
			{
				this.isRefundAgent = value;
			}
		}

		public bool IsRefundMailed
		{
			get
			{
				return this.isRefundMailed;
			}
			set
			{
				this.isRefundMailed = value;
			}
		}

		public bool IsRequested
		{
			get
			{
				return this.isRequested;
			}
			set
			{
				this.isRequested = value;
			}
		}

		public int LookupId_ProductCancelReason
		{
			get
			{
				return this.lookupId_ProductCancelReason;
			}
			set
			{
				this.lookupId_ProductCancelReason = value;
			}
		}

		public int MileageCurrent
		{
			get
			{
				return this.mileageCurrent;
			}
			set
			{
				this.mileageCurrent = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public string Notes
		{
			get
			{
				return this.notes;
			}
			set
			{
				this.notes = value;
			}
		}

		public float PaymentAmount
		{
			get
			{
				return this.paymentAmount;
			}
			set
			{
				this.paymentAmount = value;
			}
		}

		public float PremiumAmount
		{
			get
			{
				return this.premiumAmount;
			}
			set
			{
				this.premiumAmount = value;
			}
		}

		public int ProductCancellationId
		{
			get
			{
				return this.productCancellationId;
			}
			set
			{
				this.productCancellationId = value;
			}
		}

		public int ProductContractId
		{
			get
			{
				return this.productContractId;
			}
			set
			{
				this.productContractId = value;
			}
		}

		public int ProductPaymentId_AffinityGroup
		{
			get
			{
				return this.productPaymentId_AffinityGroup;
			}
			set
			{
				this.productPaymentId_AffinityGroup = value;
			}
		}

		public int ProductPaymentId_Agent
		{
			get
			{
				return this.productPaymentId_Agent;
			}
			set
			{
				this.productPaymentId_Agent = value;
			}
		}

		public string ReasonOther
		{
			get
			{
				return this.reasonOther;
			}
			set
			{
				this.reasonOther = value;
			}
		}

		public float RefundAdministratorAmount
		{
			get
			{
				return this.refundAdministratorAmount;
			}
			set
			{
				this.refundAdministratorAmount = value;
			}
		}

		public float RefundAffinityGroupAmount
		{
			get
			{
				return this.refundAffinityGroupAmount;
			}
			set
			{
				this.refundAffinityGroupAmount = value;
			}
		}

		public float RefundAgentAmount
		{
			get
			{
				return this.refundAgentAmount;
			}
			set
			{
				this.refundAgentAmount = value;
			}
		}

		public float RefundAmount
		{
			get
			{
				return this.refundAmount;
			}
			set
			{
				this.refundAmount = value;
			}
		}

		public float RefundAutolandAmount
		{
			get
			{
				return this.refundAutolandAmount;
			}
			set
			{
				this.refundAutolandAmount = value;
			}
		}

		public float RefundCancellationFee
		{
			get
			{
				return this.refundCancellationFee;
			}
			set
			{
				this.refundCancellationFee = value;
			}
		}

		public string RefundMailedAddress1
		{
			get
			{
				return this.refundMailedAddress1;
			}
			set
			{
				this.refundMailedAddress1 = value;
			}
		}

		public string RefundMailedAddress2
		{
			get
			{
				return this.refundMailedAddress2;
			}
			set
			{
				this.refundMailedAddress2 = value;
			}
		}

		public string RefundMailedCity
		{
			get
			{
				return this.refundMailedCity;
			}
			set
			{
				this.refundMailedCity = value;
			}
		}

		public string RefundMailedDate
		{
			get
			{
				return this.refundMailedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.refundMailedDate = "";
				}
				else
				{
					this.refundMailedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string RefundMailedName1
		{
			get
			{
				return this.refundMailedName1;
			}
			set
			{
				this.refundMailedName1 = value;
			}
		}

		public string RefundMailedName2
		{
			get
			{
				return this.refundMailedName2;
			}
			set
			{
				this.refundMailedName2 = value;
			}
		}

		public string RefundMailedState
		{
			get
			{
				return this.refundMailedState;
			}
			set
			{
				this.refundMailedState = value;
			}
		}

		public string RefundMailedZipCode
		{
			get
			{
				return this.refundMailedZipCode;
			}
			set
			{
				this.refundMailedZipCode = value;
			}
		}

		public float RefundPercent
		{
			get
			{
				return this.refundPercent;
			}
			set
			{
				this.refundPercent = value;
			}
		}

		public string ReportCertificate
		{
			get
			{
				return this.reportCertificate;
			}
			set
			{
				this.reportCertificate = value;
			}
		}

		public string ReportConfirmationLetter
		{
			get
			{
				return this.reportConfirmationLetter;
			}
			set
			{
				this.reportConfirmationLetter = value;
			}
		}

		public string ReportRefundLetter
		{
			get
			{
				return this.reportRefundLetter;
			}
			set
			{
				this.reportRefundLetter = value;
			}
		}

		public string RequestedDate
		{
			get
			{
				return this.requestedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.requestedDate = "";
				}
				else
				{
					this.requestedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public ProductCancellation()
		{
		}

		public ProductCancellation(string connection, string modifiedUserID, int productCancellationId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.ProductCancellationId = productCancellationId;
			if (this.ProductCancellationId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ProductCancellationId", DataAccessParameterType.Numeric, this.ProductCancellationId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("PRODUCT_CANCELLATION_Delete");
			this.wipeout();
		}

		public static int GetRecordId(int productContractId, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@ProductContractId", DataAccessParameterType.Numeric, productContractId.ToString());
			data.ExecuteProcedure("PRODUCT_CANCELLATION_GetRecord");
			num = (!data.EOF ? int.Parse(data["ProductCancellationId"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ProductCancellationId", DataAccessParameterType.Numeric, this.ProductCancellationId.ToString());
			data.ExecuteProcedure("PRODUCT_CANCELLATION_GetRecord");
			if (!data.EOF)
			{
				this.ProductCancellationId = int.Parse(data["ProductCancellationId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ProductContractId = int.Parse(data["ProductContractId"]);
				this.IsRequested = bool.Parse(data["IsRequested"]);
				this.RequestedDate = data["RequestedDate"];
				this.IsFormMailed = bool.Parse(data["IsFormMailed"]);
				this.FormMailedDate = data["FormMailedDate"];
				this.IsFormReceived = bool.Parse(data["IsFormReceived"]);
				this.FormReceivedDate = data["FormReceivedDate"];
				this.IsConfirmMailed = bool.Parse(data["IsConfirmMailed"]);
				this.ConfirmMailedDate = data["ConfirmMailedDate"];
				this.IsFundsReceived = bool.Parse(data["IsFundsReceived"]);
				this.FundsReceivedDate = data["FundsReceivedDate"];
				this.FundsReceivedAmount = float.Parse(data["FundsReceivedAmount"]);
				this.PremiumAmount = float.Parse(data["PremiumAmount"]);
				this.MileageCurrent = int.Parse(data["MileageCurrent"]);
				this.CancellationDate = data["CancellationDate"];
				this.RefundPercent = float.Parse(data["RefundPercent"]);
				this.RefundCancellationFee = float.Parse(data["RefundCancellationFee"]);
				this.RefundAmount = float.Parse(data["RefundAmount"]);
				this.PaymentAmount = float.Parse(data["PaymentAmount"]);
				this.RefundAdministratorAmount = float.Parse(data["RefundAdministratorAmount"]);
				this.RefundAffinityGroupAmount = float.Parse(data["RefundAffinityGroupAmount"]);
				this.IsRefundAffinityGroup = bool.Parse(data["IsRefundAffinityGroup"]);
				this.ProductPaymentId_AffinityGroup = int.Parse(data["ProductPaymentId_AffinityGroup"]);
				this.RefundAgentAmount = float.Parse(data["RefundAgentAmount"]);
				this.IsRefundAgent = bool.Parse(data["IsRefundAgent"]);
				this.ProductPaymentId_Agent = int.Parse(data["ProductPaymentId_Agent"]);
				this.LookupId_ProductCancelReason = int.Parse(data["LookupId_ProductCancelReason"]);
				this.ReasonOther = data["ReasonOther"];
				this.RefundAutolandAmount = float.Parse(data["RefundAutolandAmount"]);
				this.IsRefundMailed = bool.Parse(data["IsRefundMailed"]);
				this.RefundMailedDate = data["RefundMailedDate"];
				this.RefundMailedName1 = data["RefundMailedName1"];
				this.RefundMailedName2 = data["RefundMailedName2"];
				this.RefundMailedAddress1 = data["RefundMailedAddress1"];
				this.RefundMailedAddress2 = data["RefundMailedAddress2"];
				this.RefundMailedCity = data["RefundMailedCity"];
				this.RefundMailedState = data["RefundMailedState"];
				this.RefundMailedZipCode = data["RefundMailedZipCode"];
				this.Notes = data["Notes"];
				this.IsCompleted = bool.Parse(data["IsCompleted"]);
				this.ReportCertificate = data["ReportCertificate"];
				this.ReportConfirmationLetter = data["ReportConfirmationLetter"];
				this.ReportRefundLetter = data["ReportRefundLetter"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int productCancellationId = this.ProductCancellationId;
			data.AddParam("@ProductCancellationId", DataAccessParameterType.Numeric, productCancellationId.ToString());
			productCancellationId = this.ProductContractId;
			data.AddParam("@ProductContractId", DataAccessParameterType.Numeric, productCancellationId.ToString());
			bool isRequested = this.IsRequested;
			data.AddParam("@IsRequested", DataAccessParameterType.Bool, isRequested.ToString());
			data.AddParam("@RequestedDate", DataAccessParameterType.DateTime, this.RequestedDate);
			isRequested = this.IsFormMailed;
			data.AddParam("@IsFormMailed", DataAccessParameterType.Bool, isRequested.ToString());
			data.AddParam("@FormMailedDate", DataAccessParameterType.DateTime, this.FormMailedDate);
			isRequested = this.IsFormReceived;
			data.AddParam("@IsFormReceived", DataAccessParameterType.Bool, isRequested.ToString());
			data.AddParam("@FormReceivedDate", DataAccessParameterType.DateTime, this.FormReceivedDate);
			isRequested = this.IsConfirmMailed;
			data.AddParam("@IsConfirmMailed", DataAccessParameterType.Bool, isRequested.ToString());
			data.AddParam("@ConfirmMailedDate", DataAccessParameterType.DateTime, this.ConfirmMailedDate);
			isRequested = this.IsFundsReceived;
			data.AddParam("@IsFundsReceived", DataAccessParameterType.Bool, isRequested.ToString());
			data.AddParam("@FundsReceivedDate", DataAccessParameterType.DateTime, this.FundsReceivedDate);
			float fundsReceivedAmount = this.FundsReceivedAmount;
			data.AddParam("@FundsReceivedAmount", DataAccessParameterType.Numeric, fundsReceivedAmount.ToString());
			fundsReceivedAmount = this.PremiumAmount;
			data.AddParam("@PremiumAmount", DataAccessParameterType.Numeric, fundsReceivedAmount.ToString());
			productCancellationId = this.MileageCurrent;
			data.AddParam("@MileageCurrent", DataAccessParameterType.Numeric, productCancellationId.ToString());
			data.AddParam("@CancellationDate", DataAccessParameterType.DateTime, this.CancellationDate);
			fundsReceivedAmount = this.RefundPercent;
			data.AddParam("@RefundPercent", DataAccessParameterType.Numeric, fundsReceivedAmount.ToString());
			fundsReceivedAmount = this.RefundCancellationFee;
			data.AddParam("@RefundCancellationFee", DataAccessParameterType.Numeric, fundsReceivedAmount.ToString());
			fundsReceivedAmount = this.RefundAmount;
			data.AddParam("@RefundAmount", DataAccessParameterType.Numeric, fundsReceivedAmount.ToString());
			fundsReceivedAmount = this.PaymentAmount;
			data.AddParam("@PaymentAmount", DataAccessParameterType.Numeric, fundsReceivedAmount.ToString());
			fundsReceivedAmount = this.RefundAdministratorAmount;
			data.AddParam("@RefundAdministratorAmount", DataAccessParameterType.Numeric, fundsReceivedAmount.ToString());
			fundsReceivedAmount = this.RefundAffinityGroupAmount;
			data.AddParam("@RefundAffinityGroupAmount", DataAccessParameterType.Numeric, fundsReceivedAmount.ToString());
			isRequested = this.IsRefundAffinityGroup;
			data.AddParam("@IsRefundAffinityGroup", DataAccessParameterType.Bool, isRequested.ToString());
			productCancellationId = this.ProductPaymentId_AffinityGroup;
			data.AddParam("@ProductPaymentId_AffinityGroup", DataAccessParameterType.Numeric, productCancellationId.ToString());
			fundsReceivedAmount = this.RefundAgentAmount;
			data.AddParam("@RefundAgentAmount", DataAccessParameterType.Numeric, fundsReceivedAmount.ToString());
			isRequested = this.IsRefundAgent;
			data.AddParam("@IsRefundAgent", DataAccessParameterType.Bool, isRequested.ToString());
			productCancellationId = this.ProductPaymentId_Agent;
			data.AddParam("@ProductPaymentId_Agent", DataAccessParameterType.Numeric, productCancellationId.ToString());
			productCancellationId = this.LookupId_ProductCancelReason;
			data.AddParam("@LookupId_ProductCancelReason", DataAccessParameterType.Numeric, productCancellationId.ToString());
			data.AddParam("@ReasonOther", DataAccessParameterType.Text, this.ReasonOther);
			fundsReceivedAmount = this.RefundAutolandAmount;
			data.AddParam("@RefundAutolandAmount", DataAccessParameterType.Numeric, fundsReceivedAmount.ToString());
			isRequested = this.IsRefundMailed;
			data.AddParam("@IsRefundMailed", DataAccessParameterType.Bool, isRequested.ToString());
			data.AddParam("@RefundMailedDate", DataAccessParameterType.DateTime, this.RefundMailedDate);
			data.AddParam("@RefundMailedName1", DataAccessParameterType.Text, this.RefundMailedName1);
			data.AddParam("@RefundMailedName2", DataAccessParameterType.Text, this.RefundMailedName2);
			data.AddParam("@RefundMailedAddress1", DataAccessParameterType.Text, this.RefundMailedAddress1);
			data.AddParam("@RefundMailedAddress2", DataAccessParameterType.Text, this.RefundMailedAddress2);
			data.AddParam("@RefundMailedCity", DataAccessParameterType.Text, this.RefundMailedCity);
			data.AddParam("@RefundMailedState", DataAccessParameterType.Text, this.RefundMailedState);
			data.AddParam("@RefundMailedZipCode", DataAccessParameterType.Text, this.RefundMailedZipCode);
			data.AddParam("@Notes", DataAccessParameterType.Text, this.Notes);
			isRequested = this.IsCompleted;
			data.AddParam("@IsCompleted", DataAccessParameterType.Bool, isRequested.ToString());
			data.AddParam("@ReportCertificate", DataAccessParameterType.Text, this.ReportCertificate);
			data.AddParam("@ReportConfirmationLetter", DataAccessParameterType.Text, this.ReportConfirmationLetter);
			data.AddParam("@ReportRefundLetter", DataAccessParameterType.Text, this.ReportRefundLetter);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("PRODUCT_CANCELLATION_InsertUpdate");
			if (!data.EOF)
			{
				this.ProductCancellationId = int.Parse(data["ProductCancellationId"]);
			}
			this.retrievedata();
		}
	}
}