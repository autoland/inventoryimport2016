using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class ProductFamily : Transaction
	{
		private int productFamilyId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private string code = "";

		private string description = "";

		public string Code
		{
			get
			{
				return this.code;
			}
			set
			{
				this.code = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public int ProductFamilyId
		{
			get
			{
				return this.productFamilyId;
			}
			set
			{
				this.productFamilyId = value;
			}
		}

		public ProductFamily()
		{
		}

		public ProductFamily(string connection, string modifiedUserID, int productFamilyId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.ProductFamilyId = productFamilyId;
			if (this.ProductFamilyId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ProductFamilyId", DataAccessParameterType.Numeric, this.ProductFamilyId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("PRODUCT_FAMILY_Delete");
			this.wipeout();
		}

		public static int GetRecordId(string code, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@Code", DataAccessParameterType.Text, code);
			data.ExecuteProcedure("PRODUCT_FAMILY_GetRecord");
			num = (!data.EOF ? int.Parse(data["ProductFamilyId"]) : 0);
			return num;
		}

		public static int GetRecordId(int productId, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@ProductId", DataAccessParameterType.Numeric, productId.ToString());
			data.ExecuteProcedure("PRODUCT_FAMILY_GetRecord");
			num = (!data.EOF ? int.Parse(data["ProductFamilyId"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ProductFamilyId", DataAccessParameterType.Numeric, this.ProductFamilyId.ToString());
			data.ExecuteProcedure("PRODUCT_FAMILY_GetRecord");
			if (!data.EOF)
			{
				this.ProductFamilyId = int.Parse(data["ProductFamilyId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.Code = data["Code"];
				this.Description = data["Description"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ProductFamilyId", DataAccessParameterType.Numeric, this.ProductFamilyId.ToString());
			data.AddParam("@Code", DataAccessParameterType.Text, this.Code);
			data.AddParam("@Description", DataAccessParameterType.Text, this.Description);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("PRODUCT_FAMILY_InsertUpdate");
			if (!data.EOF)
			{
				this.ProductFamilyId = int.Parse(data["ProductFamilyId"]);
			}
			this.retrievedata();
		}
	}
}