using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class ActivityGap : Transaction
	{
		private int activityGapId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int activityId = 0;

		private int affinityGroupId = 0;

		private int affinityGroupBranchId = 0;

		private int memberId = 0;

		private int lookupId_GapStatus = 0;

		private int lookupId_GapPlan = 0;

		private int configurator_YearId = 0;

		private int configurator_MakeId = 0;

		private int configurator_ModelId = 0;

		private string vin = "";

		private string makeName = "";

		private string modelName = "";

		private int mileage = 0;

		private string dateOfLoan = "";

		private string loanNumber = "";

		private string lenderName = "";

		private string lenderAddress = "";

		private string lenderCity = "";

		private string lenderState = "";

		private int lenderZipCode = 0;

		private float amountFinanced = 0f;

		private int financeTermsInMonths = 0;

		private float interestRate = 0f;

		private string policyNumber = "";

		private string policyType = "";

		private float premium = 0f;

		private float premiumCost = 0f;

		private string issuedDate = "";

		private string userId_Extranet = "";

		private int productId = 0;

		private int affinityGroupBranchId_Lienholder = 0;

		private int affinityGroupBranchId_Groupholder = 0;

		public int ActivityGapId
		{
			get
			{
				return this.activityGapId;
			}
			set
			{
				this.activityGapId = value;
			}
		}

		public int ActivityId
		{
			get
			{
				return this.activityId;
			}
			set
			{
				this.activityId = value;
			}
		}

		public int AffinityGroupBranchId
		{
			get
			{
				return this.affinityGroupBranchId;
			}
			set
			{
				this.affinityGroupBranchId = value;
			}
		}

		public int AffinityGroupBranchId_Groupholder
		{
			get
			{
				return this.affinityGroupBranchId_Groupholder;
			}
			set
			{
				this.affinityGroupBranchId_Groupholder = value;
			}
		}

		public int AffinityGroupBranchId_Lienholder
		{
			get
			{
				return this.affinityGroupBranchId_Lienholder;
			}
			set
			{
				this.affinityGroupBranchId_Lienholder = value;
			}
		}

		public int AffinityGroupId
		{
			get
			{
				return this.affinityGroupId;
			}
			set
			{
				this.affinityGroupId = value;
			}
		}

		public float AmountFinanced
		{
			get
			{
				return this.amountFinanced;
			}
			set
			{
				this.amountFinanced = value;
			}
		}

		public int Configurator_MakeId
		{
			get
			{
				return this.configurator_MakeId;
			}
			set
			{
				this.configurator_MakeId = value;
			}
		}

		public int Configurator_ModelId
		{
			get
			{
				return this.configurator_ModelId;
			}
			set
			{
				this.configurator_ModelId = value;
			}
		}

		public int Configurator_YearId
		{
			get
			{
				return this.configurator_YearId;
			}
			set
			{
				this.configurator_YearId = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string DateOfLoan
		{
			get
			{
				return this.dateOfLoan;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.dateOfLoan = "";
				}
				else
				{
					this.dateOfLoan = DateTime.Parse(value).ToString();
				}
			}
		}

		public int FinanceTermsInMonths
		{
			get
			{
				return this.financeTermsInMonths;
			}
			set
			{
				this.financeTermsInMonths = value;
			}
		}

		public float InterestRate
		{
			get
			{
				return this.interestRate;
			}
			set
			{
				this.interestRate = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public string IssuedDate
		{
			get
			{
				return this.issuedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.issuedDate = "";
				}
				else
				{
					this.issuedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string LenderAddress
		{
			get
			{
				return this.lenderAddress;
			}
			set
			{
				this.lenderAddress = value;
			}
		}

		public string LenderCity
		{
			get
			{
				return this.lenderCity;
			}
			set
			{
				this.lenderCity = value;
			}
		}

		public string LenderName
		{
			get
			{
				return this.lenderName;
			}
			set
			{
				this.lenderName = value;
			}
		}

		public string LenderState
		{
			get
			{
				return this.lenderState;
			}
			set
			{
				this.lenderState = value;
			}
		}

		public int LenderZipCode
		{
			get
			{
				return this.lenderZipCode;
			}
			set
			{
				this.lenderZipCode = value;
			}
		}

		public string LoanNumber
		{
			get
			{
				return this.loanNumber;
			}
			set
			{
				this.loanNumber = value;
			}
		}

		public int LookupId_GapPlan
		{
			get
			{
				return this.lookupId_GapPlan;
			}
			set
			{
				this.lookupId_GapPlan = value;
			}
		}

		public int LookupId_GapStatus
		{
			get
			{
				return this.lookupId_GapStatus;
			}
			set
			{
				this.lookupId_GapStatus = value;
			}
		}

		public string MakeName
		{
			get
			{
				return this.makeName;
			}
		}

		public int MemberId
		{
			get
			{
				return this.memberId;
			}
			set
			{
				this.memberId = value;
			}
		}

		public int Mileage
		{
			get
			{
				return this.mileage;
			}
			set
			{
				this.mileage = value;
			}
		}

		public string ModelName
		{
			get
			{
				return this.modelName;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public string PolicyNumber
		{
			get
			{
				return this.policyNumber;
			}
			set
			{
				this.policyNumber = value;
			}
		}

		public string PolicyType
		{
			get
			{
				return this.policyType;
			}
			set
			{
				this.policyType = value;
			}
		}

		public float Premium
		{
			get
			{
				return this.premium;
			}
			set
			{
				this.premium = value;
			}
		}

		public float PremiumCost
		{
			get
			{
				return this.premiumCost;
			}
			set
			{
				this.premiumCost = value;
			}
		}

		public int ProductId
		{
			get
			{
				return this.productId;
			}
			set
			{
				this.productId = value;
			}
		}

		public string UserId_Extranet
		{
			get
			{
				return this.userId_Extranet;
			}
			set
			{
				this.userId_Extranet = value;
			}
		}

		public string Vin
		{
			get
			{
				return this.vin;
			}
			set
			{
				this.vin = value;
			}
		}

		public ActivityGap()
		{
		}

		public ActivityGap(string connection, string modifiedUserID, int activityGapId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.ActivityGapId = activityGapId;
			if (this.ActivityGapId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ActivityGapId", DataAccessParameterType.Numeric, this.ActivityGapId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("ACTIVITY_GAP_Delete");
			this.wipeout();
		}

		public static int GetRecordId(int activityId, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@ActivityId", DataAccessParameterType.Numeric, activityId.ToString());
			data.ExecuteProcedure("ACTIVITY_GAP_GetRecord");
			num = (!data.EOF ? int.Parse(data["ActivityGapId"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ActivityGapId", DataAccessParameterType.Numeric, this.ActivityGapId.ToString());
			data.ExecuteProcedure("ACTIVITY_GAP_GetRecord");
			if (!data.EOF)
			{
				this.ActivityGapId = int.Parse(data["ActivityGapId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.ActivityId = int.Parse(data["ActivityId"]);
				this.AffinityGroupId = int.Parse(data["AffinityGroupId"]);
				this.AffinityGroupBranchId = int.Parse(data["AffinityGroupBranchId"]);
				this.MemberId = int.Parse(data["MemberId"]);
				this.LookupId_GapStatus = int.Parse(data["LookupId_GapStatus"]);
				this.LookupId_GapPlan = int.Parse(data["LookupId_GapPlan"]);
				this.Configurator_YearId = int.Parse(data["Configurator_YearId"]);
				this.Configurator_MakeId = int.Parse(data["Configurator_MakeId"]);
				this.Configurator_ModelId = int.Parse(data["Configurator_ModelId"]);
				this.makeName = data["MakeName"];
				this.modelName = data["ModelName"];
				this.Vin = data["Vin"];
				this.Mileage = int.Parse(data["Mileage"]);
				this.DateOfLoan = data["DateOfLoan"];
				this.LoanNumber = data["LoanNumber"];
				this.LenderName = data["LenderName"];
				this.LenderAddress = data["LenderAddress"];
				this.LenderCity = data["LenderCity"];
				this.LenderState = data["LenderState"];
				this.LenderZipCode = int.Parse(data["LenderZipCode"]);
				this.AmountFinanced = float.Parse(data["AmountFinanced"]);
				this.FinanceTermsInMonths = int.Parse(data["FinanceTermsInMonths"]);
				this.InterestRate = float.Parse(data["InterestRate"]);
				this.PolicyNumber = data["PolicyNumber"];
				this.PolicyType = data["PolicyType"];
				this.Premium = float.Parse(data["Premium"]);
				this.PremiumCost = float.Parse(data["PremiumCost"]);
				this.IssuedDate = data["IssuedDate"];
				this.UserId_Extranet = data["UserId_Extranet"];
				this.ProductId = int.Parse(data["ProductId"]);
				this.AffinityGroupBranchId_Lienholder = int.Parse(data["AffinityGroupBranchId_Lienholder"]);
				this.AffinityGroupBranchId_Groupholder = int.Parse(data["AffinityGroupBranchId_Groupholder"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int activityGapId = this.ActivityGapId;
			data.AddParam("@ActivityGapId", DataAccessParameterType.Numeric, activityGapId.ToString());
			activityGapId = this.ActivityId;
			data.AddParam("@ActivityId", DataAccessParameterType.Numeric, activityGapId.ToString());
			activityGapId = this.AffinityGroupId;
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, activityGapId.ToString());
			activityGapId = this.AffinityGroupBranchId;
			data.AddParam("@AffinityGroupBranchId", DataAccessParameterType.Numeric, activityGapId.ToString());
			activityGapId = this.MemberId;
			data.AddParam("@MemberId", DataAccessParameterType.Numeric, activityGapId.ToString());
			activityGapId = this.LookupId_GapStatus;
			data.AddParam("@LookupId_GapStatus", DataAccessParameterType.Numeric, activityGapId.ToString());
			activityGapId = this.LookupId_GapPlan;
			data.AddParam("@LookupId_GapPlan", DataAccessParameterType.Numeric, activityGapId.ToString());
			activityGapId = this.Configurator_YearId;
			data.AddParam("@Configurator_YearId", DataAccessParameterType.Numeric, activityGapId.ToString());
			activityGapId = this.Configurator_MakeId;
			data.AddParam("@Configurator_MakeId", DataAccessParameterType.Numeric, activityGapId.ToString());
			activityGapId = this.Configurator_ModelId;
			data.AddParam("@Configurator_ModelId", DataAccessParameterType.Numeric, activityGapId.ToString());
			data.AddParam("@Vin", DataAccessParameterType.Text, this.Vin);
			activityGapId = this.Mileage;
			data.AddParam("@Mileage", DataAccessParameterType.Numeric, activityGapId.ToString());
			data.AddParam("@DateOfLoan", DataAccessParameterType.DateTime, this.DateOfLoan);
			data.AddParam("@LoanNumber", DataAccessParameterType.Text, this.LoanNumber);
			data.AddParam("@LenderName", DataAccessParameterType.Text, this.LenderName);
			data.AddParam("@LenderAddress", DataAccessParameterType.Text, this.LenderAddress);
			data.AddParam("@LenderCity", DataAccessParameterType.Text, this.LenderCity);
			data.AddParam("@LenderState", DataAccessParameterType.Text, this.LenderState);
			activityGapId = this.LenderZipCode;
			data.AddParam("@LenderZipCode", DataAccessParameterType.Numeric, activityGapId.ToString());
			float amountFinanced = this.AmountFinanced;
			data.AddParam("@AmountFinanced", DataAccessParameterType.Numeric, amountFinanced.ToString());
			activityGapId = this.FinanceTermsInMonths;
			data.AddParam("@FinanceTermsInMonths", DataAccessParameterType.Numeric, activityGapId.ToString());
			amountFinanced = this.InterestRate;
			data.AddParam("@InterestRate", DataAccessParameterType.Numeric, amountFinanced.ToString());
			data.AddParam("@PolicyNumber", DataAccessParameterType.Text, this.PolicyNumber);
			data.AddParam("@PolicyType", DataAccessParameterType.Text, this.PolicyType);
			amountFinanced = this.Premium;
			data.AddParam("@Premium", DataAccessParameterType.Numeric, amountFinanced.ToString());
			amountFinanced = this.PremiumCost;
			data.AddParam("@PremiumCost", DataAccessParameterType.Numeric, amountFinanced.ToString());
			data.AddParam("@IssuedDate", DataAccessParameterType.DateTime, this.IssuedDate);
			data.AddParam("@UserId_Extranet", DataAccessParameterType.Text, this.UserId_Extranet);
			activityGapId = this.ProductId;
			data.AddParam("@ProductId", DataAccessParameterType.Numeric, activityGapId.ToString());
			activityGapId = this.AffinityGroupBranchId_Lienholder;
			data.AddParam("@AffinityGroupBranchId_Lienholder", DataAccessParameterType.Numeric, activityGapId.ToString());
			activityGapId = this.AffinityGroupBranchId_Groupholder;
			data.AddParam("@AffinityGroupBranchId_Groupholder", DataAccessParameterType.Numeric, activityGapId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("ACTIVITY_GAP_InsertUpdate");
			if (!data.EOF)
			{
				this.ActivityGapId = int.Parse(data["ActivityGapId"]);
			}
			this.retrievedata();
		}
	}
}