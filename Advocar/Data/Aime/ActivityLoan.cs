using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class ActivityLoan : Transaction
	{
		private int activityLoanId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int affinityGroupId = 0;

		private int activityId = 0;

		private int memberId = 0;

		private string applicationNumber = "";

		private int lookupId_LoanType = 0;

		private string loanType = "";

		private string name = "";

		private string submittedDateTime = "";

		private int lookupId_LoanStatus = 0;

		private string loanStatus = "";

		private int lookupId_LoanStatusCancelReason = 0;

		private string applicationStage = "";

		private bool isStipulationsMet = false;

		private float amountRequested = 0f;

		private float amountApproved = 0f;

		private string dateFunded = "";

		private string consultant = "";

		private string dealNumber = "";

		private int affinityGroupId_Source = 0;

		private string source = "";

		private int affinityGroupBranchId_Lienholder = 0;

		private string lienholder = "";

		private string notes = "";

		public int ActivityId
		{
			get
			{
				return this.activityId;
			}
			set
			{
				this.activityId = value;
			}
		}

		public int ActivityLoanId
		{
			get
			{
				return this.activityLoanId;
			}
			set
			{
				this.activityLoanId = value;
			}
		}

		public int AffinityGroupBranchId_Lienholder
		{
			get
			{
				return this.affinityGroupBranchId_Lienholder;
			}
			set
			{
				this.affinityGroupBranchId_Lienholder = value;
			}
		}

		public int AffinityGroupId
		{
			get
			{
				return this.affinityGroupId;
			}
			set
			{
				this.affinityGroupId = value;
			}
		}

		public int AffinityGroupId_Source
		{
			get
			{
				return this.affinityGroupId_Source;
			}
			set
			{
				this.affinityGroupId_Source = value;
			}
		}

		public float AmountApproved
		{
			get
			{
				return this.amountApproved;
			}
			set
			{
				this.amountApproved = value;
			}
		}

		public float AmountRequested
		{
			get
			{
				return this.amountRequested;
			}
			set
			{
				this.amountRequested = value;
			}
		}

		public string ApplicationNumber
		{
			get
			{
				return this.applicationNumber;
			}
			set
			{
				this.applicationNumber = value;
			}
		}

		public string ApplicationStage
		{
			get
			{
				return this.applicationStage;
			}
			set
			{
				this.applicationStage = value;
			}
		}

		public string Consultant
		{
			get
			{
				return this.consultant;
			}
			set
			{
				this.consultant = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string DateFunded
		{
			get
			{
				return this.dateFunded;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.dateFunded = "";
				}
				else
				{
					this.dateFunded = DateTime.Parse(value).ToString();
				}
			}
		}

		public string DealNumber
		{
			get
			{
				return this.dealNumber;
			}
			set
			{
				this.dealNumber = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public bool IsStipulationsMet
		{
			get
			{
				return this.isStipulationsMet;
			}
			set
			{
				this.isStipulationsMet = value;
			}
		}

		public string Lienholder
		{
			get
			{
				return this.lienholder;
			}
			set
			{
				this.lienholder = value;
			}
		}

		public string LoanStatus
		{
			get
			{
				return this.loanStatus;
			}
			set
			{
				this.loanStatus = value;
			}
		}

		public string LoanType
		{
			get
			{
				return this.loanType;
			}
			set
			{
				this.loanType = value;
			}
		}

		public int LookupId_LoanStatus
		{
			get
			{
				return this.lookupId_LoanStatus;
			}
			set
			{
				this.lookupId_LoanStatus = value;
			}
		}

		public int LookupId_LoanStatusCancelReason
		{
			get
			{
				return this.lookupId_LoanStatusCancelReason;
			}
			set
			{
				this.lookupId_LoanStatusCancelReason = value;
			}
		}

		public int LookupId_LoanType
		{
			get
			{
				return this.lookupId_LoanType;
			}
			set
			{
				this.lookupId_LoanType = value;
			}
		}

		public int MemberId
		{
			get
			{
				return this.memberId;
			}
			set
			{
				this.memberId = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		public string Notes
		{
			get
			{
				return this.notes;
			}
			set
			{
				this.notes = value;
			}
		}

		public string Source
		{
			get
			{
				return this.source;
			}
			set
			{
				this.source = value;
			}
		}

		public string SubmittedDateTime
		{
			get
			{
				return this.submittedDateTime;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.submittedDateTime = "";
				}
				else
				{
					this.submittedDateTime = DateTime.Parse(value).ToString();
				}
			}
		}

		public ActivityLoan()
		{
		}

		public ActivityLoan(string connection, string modifiedUserID, int activityLoanId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.ActivityLoanId = activityLoanId;
			if (this.ActivityLoanId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ActivityLoanId", DataAccessParameterType.Numeric, this.ActivityLoanId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("ACTIVITY_LOAN_Delete");
			this.wipeout();
		}

		public static int GetRecordId(int activityId, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@ActivityId", DataAccessParameterType.Numeric, activityId.ToString());
			data.ExecuteProcedure("ACTIVITY_LOAN_GetRecord");
			num = (!data.EOF ? int.Parse(data["ActivityLoanId"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ActivityLoanId", DataAccessParameterType.Numeric, this.ActivityLoanId.ToString());
			data.ExecuteProcedure("ACTIVITY_LOAN_GetRecord");
			if (!data.EOF)
			{
				this.ActivityLoanId = int.Parse(data["ActivityLoanId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.AffinityGroupId = int.Parse(data["AffinityGroupId"]);
				this.ActivityId = int.Parse(data["ActivityId"]);
				this.MemberId = int.Parse(data["MemberId"]);
				this.ApplicationNumber = data["ApplicationNumber"];
				this.LookupId_LoanType = int.Parse(data["LookupId_LoanType"]);
				this.LoanType = data["LoanType"];
				this.Name = data["Name"];
				this.SubmittedDateTime = data["SubmittedDateTime"];
				this.LookupId_LoanStatus = int.Parse(data["LookupId_LoanStatus"]);
				this.LoanStatus = data["LoanStatus"];
				this.LookupId_LoanStatusCancelReason = int.Parse(data["lookupId_LoanStatusCancelReason"]);
				this.ApplicationStage = data["ApplicationStage"];
				this.IsStipulationsMet = bool.Parse(data["IsStipulationsMet"]);
				this.AmountRequested = float.Parse(data["AmountRequested"]);
				this.AmountApproved = float.Parse(data["AmountApproved"]);
				this.DateFunded = data["DateFunded"];
				this.Consultant = data["Consultant"];
				this.DealNumber = data["DealNumber"];
				this.AffinityGroupId_Source = int.Parse(data["AffinityGroupId_Source"]);
				this.Source = data["Source"];
				this.AffinityGroupBranchId_Lienholder = int.Parse(data["AffinityGroupBranchId_Lienholder"]);
				this.Lienholder = data["Lienholder"];
				this.Notes = data["Notes"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int activityLoanId = this.ActivityLoanId;
			data.AddParam("@ActivityLoanId", DataAccessParameterType.Numeric, activityLoanId.ToString());
			activityLoanId = this.AffinityGroupId;
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, activityLoanId.ToString());
			activityLoanId = this.ActivityId;
			data.AddParam("@ActivityId", DataAccessParameterType.Numeric, activityLoanId.ToString());
			activityLoanId = this.MemberId;
			data.AddParam("@MemberId", DataAccessParameterType.Numeric, activityLoanId.ToString());
			data.AddParam("@ApplicationNumber", DataAccessParameterType.Text, this.ApplicationNumber);
			activityLoanId = this.LookupId_LoanType;
			data.AddParam("@LookupId_LoanType", DataAccessParameterType.Numeric, activityLoanId.ToString());
			data.AddParam("@LoanType", DataAccessParameterType.Text, this.LoanType.ToString());
			data.AddParam("@Name", DataAccessParameterType.Text, this.Name);
			data.AddParam("@SubmittedDateTime", DataAccessParameterType.DateTime, this.SubmittedDateTime);
			activityLoanId = this.LookupId_LoanStatus;
			data.AddParam("@LookupId_LoanStatus", DataAccessParameterType.Numeric, activityLoanId.ToString());
			data.AddParam("@LoanStatus", DataAccessParameterType.Text, this.LoanStatus);
			activityLoanId = this.LookupId_LoanStatusCancelReason;
			data.AddParam("@LookupId_LoanStatusCancelReason", DataAccessParameterType.Numeric, activityLoanId.ToString());
			data.AddParam("@ApplicationStage", DataAccessParameterType.Text, this.ApplicationStage);
			data.AddParam("@IsStipulationsMet", DataAccessParameterType.Bool, this.IsStipulationsMet.ToString());
			float amountRequested = this.AmountRequested;
			data.AddParam("@AmountRequested", DataAccessParameterType.Numeric, amountRequested.ToString());
			amountRequested = this.AmountApproved;
			data.AddParam("@AmountApproved", DataAccessParameterType.Numeric, amountRequested.ToString());
			data.AddParam("@DateFunded", DataAccessParameterType.DateTime, this.DateFunded);
			data.AddParam("@Consultant", DataAccessParameterType.Text, this.Consultant);
			data.AddParam("@DealNumber", DataAccessParameterType.Text, this.DealNumber);
			activityLoanId = this.AffinityGroupId_Source;
			data.AddParam("@AffinityGroupId_Source", DataAccessParameterType.Numeric, activityLoanId.ToString());
			data.AddParam("@Source", DataAccessParameterType.Text, this.Source);
			activityLoanId = this.AffinityGroupBranchId_Lienholder;
			data.AddParam("@AffinityGroupBranchId_Lienholder", DataAccessParameterType.Numeric, activityLoanId.ToString());
			data.AddParam("@Lienholder", DataAccessParameterType.Text, this.Lienholder);
			data.AddParam("@Notes", DataAccessParameterType.Text, this.Notes);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("ACTIVITY_LOAN_InsertUpdate");
			if (!data.EOF)
			{
				this.ActivityLoanId = int.Parse(data["ActivityLoanId"]);
			}
			this.retrievedata();
		}
	}
}