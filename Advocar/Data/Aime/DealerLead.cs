using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class DealerLead : Transaction
	{
		private int dealerLeadId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int dealerId = 0;

		private int com_CntlId = 0;

		private bool isSentToDealer = false;

		private string sentToDealerDate = "";

		private string sentResponse = "";

		public int Com_CntlId
		{
			get
			{
				return this.com_CntlId;
			}
			set
			{
				this.com_CntlId = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public int DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public int DealerLeadId
		{
			get
			{
				return this.dealerLeadId;
			}
			set
			{
				this.dealerLeadId = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public bool IsSentToDealer
		{
			get
			{
				return this.isSentToDealer;
			}
			set
			{
				this.isSentToDealer = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public string SentResponse
		{
			get
			{
				return this.sentResponse;
			}
			set
			{
				this.sentResponse = value;
			}
		}

		public string SentToDealerDate
		{
			get
			{
				return this.sentToDealerDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.sentToDealerDate = "";
				}
				else
				{
					this.sentToDealerDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public DealerLead()
		{
		}

		public DealerLead(string connection, string modifiedUserID, int dealerLeadId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "DEALER_LEAD";
			this.DealerLeadId = dealerLeadId;
			if (this.DealerLeadId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@DealerLeadId", DataAccessParameterType.Numeric, this.DealerLeadId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("DEALER_LEAD_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@DealerLeadId", DataAccessParameterType.Numeric, this.DealerLeadId.ToString());
			data.ExecuteProcedure("DEALER_LEAD_GetRecord");
			if (!data.EOF)
			{
				this.DealerLeadId = int.Parse(data["DealerLeadId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.DealerId = int.Parse(data["DealerId"]);
				this.Com_CntlId = int.Parse(data["Com_CntlId"]);
				this.IsSentToDealer = bool.Parse(data["IsSentToDealer"]);
				this.SentToDealerDate = data["SentToDealerDate"];
				this.SentResponse = data["SentResponse"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int dealerLeadId = this.DealerLeadId;
			data.AddParam("@DealerLeadId", DataAccessParameterType.Numeric, dealerLeadId.ToString());
			dealerLeadId = this.DealerId;
			data.AddParam("@DealerId", DataAccessParameterType.Numeric, dealerLeadId.ToString());
			dealerLeadId = this.Com_CntlId;
			data.AddParam("@Com_CntlId", DataAccessParameterType.Numeric, dealerLeadId.ToString());
			data.AddParam("@IsSentToDealer", DataAccessParameterType.Bool, this.IsSentToDealer.ToString());
			data.AddParam("@SentToDealerDate", DataAccessParameterType.DateTime, this.SentToDealerDate);
			data.AddParam("@SentResponse", DataAccessParameterType.Text, this.SentResponse);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("DEALER_LEAD_InsertUpdate");
			if (!data.EOF)
			{
				this.DealerLeadId = int.Parse(data["DealerLeadId"]);
			}
			this.retrievedata();
		}
	}
}