using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class LeadPromotion : Transaction
	{
		private int leadPromotionId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int com_CntlId = 0;

		private int affinityGroupId = 0;

		private string firstName = "";

		private string lastName = "";

		private string address1 = "";

		private string address2 = "";

		private string city = "";

		private string stateCode = "";

		private string zipCode = "";

		private string phone = "";

		private string email = "";

		private bool isCreditUnionLoan = false;

		private bool isHelpAutolandWanted = false;

		public string Address1
		{
			get
			{
				return this.address1;
			}
			set
			{
				this.address1 = value;
			}
		}

		public string Address2
		{
			get
			{
				return this.address2;
			}
			set
			{
				this.address2 = value;
			}
		}

		public int AffinityGroupId
		{
			get
			{
				return this.affinityGroupId;
			}
			set
			{
				this.affinityGroupId = value;
			}
		}

		public string City
		{
			get
			{
				return this.city;
			}
			set
			{
				this.city = value;
			}
		}

		public int Com_CntlId
		{
			get
			{
				return this.com_CntlId;
			}
			set
			{
				this.com_CntlId = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string Email
		{
			get
			{
				return this.email;
			}
			set
			{
				this.email = value;
			}
		}

		public string FirstName
		{
			get
			{
				return this.firstName;
			}
			set
			{
				this.firstName = value;
			}
		}

		public bool IsCreditUnionLoan
		{
			get
			{
				return this.isCreditUnionLoan;
			}
			set
			{
				this.isCreditUnionLoan = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public bool IsHelpAutolandWanted
		{
			get
			{
				return this.isHelpAutolandWanted;
			}
			set
			{
				this.isHelpAutolandWanted = value;
			}
		}

		public string LastName
		{
			get
			{
				return this.lastName;
			}
			set
			{
				this.lastName = value;
			}
		}

		public int LeadPromotionId
		{
			get
			{
				return this.leadPromotionId;
			}
			set
			{
				this.leadPromotionId = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public string Phone
		{
			get
			{
				return this.phone;
			}
			set
			{
				this.phone = value;
			}
		}

		public string StateCode
		{
			get
			{
				return this.stateCode;
			}
			set
			{
				this.stateCode = value;
			}
		}

		public string ZipCode
		{
			get
			{
				return this.zipCode;
			}
			set
			{
				this.zipCode = value;
			}
		}

		public LeadPromotion()
		{
		}

		public LeadPromotion(string connection, string modifiedUserID, int leadPromotionId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "LEAD_PROMOTION";
			this.LeadPromotionId = leadPromotionId;
			if (this.LeadPromotionId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@LeadPromotionId", DataAccessParameterType.Numeric, this.LeadPromotionId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("LEAD_PROMOTION_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@LeadPromotionId", DataAccessParameterType.Numeric, this.LeadPromotionId.ToString());
			data.ExecuteProcedure("LEAD_PROMOTION_GetRecord");
			if (!data.EOF)
			{
				this.LeadPromotionId = int.Parse(data["LeadPromotionId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.Com_CntlId = int.Parse(data["Com_CntlId"]);
				this.AffinityGroupId = int.Parse(data["AffinityGroupId"]);
				this.FirstName = data["FirstName"];
				this.LastName = data["LastName"];
				this.Address1 = data["Address1"];
				this.Address2 = data["Address2"];
				this.City = data["City"];
				this.StateCode = data["StateCode"];
				this.ZipCode = data["ZipCode"];
				this.Phone = data["Phone"];
				this.Email = data["Email"];
				this.IsCreditUnionLoan = bool.Parse(data["IsCreditUnionLoan"]);
				this.IsHelpAutolandWanted = bool.Parse(data["IsHelpAutolandWanted"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int leadPromotionId = this.LeadPromotionId;
			data.AddParam("@LeadPromotionId", DataAccessParameterType.Numeric, leadPromotionId.ToString());
			leadPromotionId = this.Com_CntlId;
			data.AddParam("@Com_CntlId", DataAccessParameterType.Numeric, leadPromotionId.ToString());
			leadPromotionId = this.AffinityGroupId;
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, leadPromotionId.ToString());
			data.AddParam("@FirstName", DataAccessParameterType.Text, this.FirstName);
			data.AddParam("@LastName", DataAccessParameterType.Text, this.LastName);
			data.AddParam("@Address1", DataAccessParameterType.Text, this.Address1);
			data.AddParam("@Address2", DataAccessParameterType.Text, this.Address2);
			data.AddParam("@City", DataAccessParameterType.Text, this.City);
			data.AddParam("@StateCode", DataAccessParameterType.Text, this.StateCode);
			data.AddParam("@ZipCode", DataAccessParameterType.Text, this.ZipCode);
			data.AddParam("@Phone", DataAccessParameterType.Text, this.Phone);
			data.AddParam("@Email", DataAccessParameterType.Text, this.Email);
			bool isCreditUnionLoan = this.IsCreditUnionLoan;
			data.AddParam("@IsCreditUnionLoan", DataAccessParameterType.Bool, isCreditUnionLoan.ToString());
			isCreditUnionLoan = this.IsHelpAutolandWanted;
			data.AddParam("@IsHelpAutolandWanted", DataAccessParameterType.Bool, isCreditUnionLoan.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("LEAD_PROMOTION_InsertUpdate");
			if (!data.EOF)
			{
				this.LeadPromotionId = int.Parse(data["LeadPromotionId"]);
			}
			this.retrievedata();
		}
	}
}