using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class Lookup : Transaction
	{
		private int lookupId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int lookupTypeId = 0;

		private string code = "";

		private string description = "";

		private string otherValue1 = "";

		private string otherValue2 = "";

		private string otherValue3 = "";

		private string otherValue4 = "";

		private string otherValue5 = "";

		public string Code
		{
			get
			{
				return this.code;
			}
			set
			{
				this.code = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public int LookupId
		{
			get
			{
				return this.lookupId;
			}
			set
			{
				this.lookupId = value;
			}
		}

		public int LookupTypeId
		{
			get
			{
				return this.lookupTypeId;
			}
			set
			{
				this.lookupTypeId = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public string OtherValue1
		{
			get
			{
				return this.otherValue1;
			}
			set
			{
				this.otherValue1 = value;
			}
		}

		public string OtherValue2
		{
			get
			{
				return this.otherValue2;
			}
			set
			{
				this.otherValue2 = value;
			}
		}

		public string OtherValue3
		{
			get
			{
				return this.otherValue3;
			}
			set
			{
				this.otherValue3 = value;
			}
		}

		public string OtherValue4
		{
			get
			{
				return this.otherValue4;
			}
			set
			{
				this.otherValue4 = value;
			}
		}

		public string OtherValue5
		{
			get
			{
				return this.otherValue5;
			}
			set
			{
				this.otherValue5 = value;
			}
		}

		public Lookup()
		{
		}

		public Lookup(string connection, string modifiedUserID, int lookupId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.LookupId = lookupId;
			if (this.LookupId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@LookupId", DataAccessParameterType.Numeric, this.LookupId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("LOOKUP_Delete");
			this.wipeout();
		}

		public static int GetRecordId(string code, LookupType lookupType, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@LookupType", DataAccessParameterType.Text, lookupType.ToString());
			data.AddParam("@Code", DataAccessParameterType.Text, code);
			data.ExecuteProcedure("LOOKUP_GetRecord");
			num = (!data.EOF ? int.Parse(data["LookupId"]) : 0);
			return num;
		}

		public static string GetRecordIdByDescription(string description, LookupType lookupType, string connection)
		{
			DataAccess data = new DataAccess(connection);
			data.AddParam("@Description", DataAccessParameterType.Text, description);
			data.AddParam("@LookupType", DataAccessParameterType.Text, lookupType.ToString());
			data.ExecuteProcedure("LOOKUP_GetRecord");
			return (!data.EOF ? data["Code"] : "");
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@LookupId", DataAccessParameterType.Numeric, this.LookupId.ToString());
			data.ExecuteProcedure("LOOKUP_GetRecord");
			if (!data.EOF)
			{
				this.LookupId = int.Parse(data["LookupId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.LookupTypeId = int.Parse(data["LookupTypeId"]);
				this.Code = data["Code"];
				this.Description = data["Description"];
				this.OtherValue1 = data["OtherValue1"];
				this.OtherValue2 = data["OtherValue2"];
				this.OtherValue3 = data["OtherValue3"];
				this.OtherValue4 = data["OtherValue4"];
				this.OtherValue5 = data["OtherValue5"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int lookupId = this.LookupId;
			data.AddParam("@LookupId", DataAccessParameterType.Numeric, lookupId.ToString());
			lookupId = this.LookupTypeId;
			data.AddParam("@LookupTypeId", DataAccessParameterType.Numeric, lookupId.ToString());
			data.AddParam("@Code", DataAccessParameterType.Text, this.Code);
			data.AddParam("@Description", DataAccessParameterType.Text, this.Description);
			data.AddParam("@OtherValue1", DataAccessParameterType.Text, this.OtherValue1);
			data.AddParam("@OtherValue2", DataAccessParameterType.Text, this.OtherValue2);
			data.AddParam("@OtherValue3", DataAccessParameterType.Text, this.OtherValue3);
			data.AddParam("@OtherValue4", DataAccessParameterType.Text, this.OtherValue4);
			data.AddParam("@OtherValue5", DataAccessParameterType.Text, this.OtherValue5);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("LOOKUP_InsertUpdate");
			if (!data.EOF)
			{
				this.LookupId = int.Parse(data["LookupId"]);
			}
			this.retrievedata();
		}
	}
}