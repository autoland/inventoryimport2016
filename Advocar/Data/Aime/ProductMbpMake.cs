using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class ProductMbpMake : Transaction
	{
		private int productMbpMakeId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private string makeName = "";

		private bool isPremierWrap = false;

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public bool IsPremierWrap
		{
			get
			{
				return this.isPremierWrap;
			}
			set
			{
				this.isPremierWrap = value;
			}
		}

		public string MakeName
		{
			get
			{
				return this.makeName;
			}
			set
			{
				this.makeName = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public int ProductMbpMakeId
		{
			get
			{
				return this.productMbpMakeId;
			}
			set
			{
				this.productMbpMakeId = value;
			}
		}

		public ProductMbpMake()
		{
		}

		public ProductMbpMake(string connection, string modifiedUserID, int productMbpMakeId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.ProductMbpMakeId = productMbpMakeId;
			if (this.ProductMbpMakeId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ProductMbpMakeId", DataAccessParameterType.Numeric, this.ProductMbpMakeId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("PRODUCT_MBP_MAKE_Delete");
			this.wipeout();
		}

		public static int GetRecordId(string connection, string makeName)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@MakeName", DataAccessParameterType.Text, makeName);
			data.ExecuteProcedure("PRODUCT_MBP_MAKE_GetRecord");
			num = (!data.EOF ? int.Parse(data["ProductMbpMakeId"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ProductMbpMakeId", DataAccessParameterType.Numeric, this.ProductMbpMakeId.ToString());
			data.ExecuteProcedure("PRODUCT_MBP_MAKE_GetRecord");
			if (!data.EOF)
			{
				this.ProductMbpMakeId = int.Parse(data["ProductMbpMakeId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.MakeName = data["MakeName"];
				this.IsPremierWrap = bool.Parse(data["IsPremierWrap"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ProductMbpMakeId", DataAccessParameterType.Numeric, this.ProductMbpMakeId.ToString());
			data.AddParam("@MakeName", DataAccessParameterType.Text, this.MakeName);
			data.AddParam("@IsPremierWrap", DataAccessParameterType.Bool, this.IsPremierWrap.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("PRODUCT_MBP_MAKE_InsertUpdate");
			if (!data.EOF)
			{
				this.ProductMbpMakeId = int.Parse(data["ProductMbpMakeId"]);
			}
			this.retrievedata();
		}
	}
}