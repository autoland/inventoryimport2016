using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class AffinityGroupWebProfile : Transaction
	{
		private int affinityGroupWebProfileId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int lookupId_WebProfile = 0;

		private bool isLogo = false;

		private bool isPromo = false;

		private bool isDisplayTop = false;

		private bool isKbb = false;

		private string homeUrl = "";

		private string linkText1 = "";

		private string linkUrl1 = "";

		private string linkText2 = "";

		private string linkUrl2 = "";

		private string linkText3 = "";

		private string linkUrl3 = "";

		public int AffinityGroupWebProfileId
		{
			get
			{
				return this.affinityGroupWebProfileId;
			}
			set
			{
				this.affinityGroupWebProfileId = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string HomeUrl
		{
			get
			{
				return this.homeUrl;
			}
			set
			{
				this.homeUrl = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public bool IsDisplayTop
		{
			get
			{
				return this.isDisplayTop;
			}
			set
			{
				this.isDisplayTop = value;
			}
		}

		public bool IsKbb
		{
			get
			{
				return this.isKbb;
			}
			set
			{
				this.isKbb = value;
			}
		}

		public bool IsLogo
		{
			get
			{
				return this.isLogo;
			}
			set
			{
				this.isLogo = value;
			}
		}

		public bool IsPromo
		{
			get
			{
				return this.isPromo;
			}
			set
			{
				this.isPromo = value;
			}
		}

		public string LinkText1
		{
			get
			{
				return this.linkText1;
			}
			set
			{
				this.linkText1 = value;
			}
		}

		public string LinkText2
		{
			get
			{
				return this.linkText2;
			}
			set
			{
				this.linkText2 = value;
			}
		}

		public string LinkText3
		{
			get
			{
				return this.linkText3;
			}
			set
			{
				this.linkText3 = value;
			}
		}

		public string LinkUrl1
		{
			get
			{
				return this.linkUrl1;
			}
			set
			{
				this.linkUrl1 = value;
			}
		}

		public string LinkUrl2
		{
			get
			{
				return this.linkUrl2;
			}
			set
			{
				this.linkUrl2 = value;
			}
		}

		public string LinkUrl3
		{
			get
			{
				return this.linkUrl3;
			}
			set
			{
				this.linkUrl3 = value;
			}
		}

		public int LookupId_WebProfile
		{
			get
			{
				return this.lookupId_WebProfile;
			}
			set
			{
				this.lookupId_WebProfile = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public AffinityGroupWebProfile()
		{
		}

		public AffinityGroupWebProfile(string connection, string modifiedUserID, int affinityGroupWebProfileId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.AffinityGroupWebProfileId = affinityGroupWebProfileId;
			if (this.AffinityGroupWebProfileId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@AffinityGroupWebProfileId", DataAccessParameterType.Numeric, this.AffinityGroupWebProfileId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("AFFINITY_GROUP_WEB_PROFILE_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@AffinityGroupWebProfileId", DataAccessParameterType.Numeric, this.AffinityGroupWebProfileId.ToString());
			data.ExecuteProcedure("AFFINITY_GROUP_WEB_PROFILE_GetRecord");
			if (!data.EOF)
			{
				this.AffinityGroupWebProfileId = int.Parse(data["AffinityGroupWebProfileId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.LookupId_WebProfile = int.Parse(data["LookupId_WebProfile"]);
				this.IsLogo = bool.Parse(data["IsLogo"]);
				this.IsPromo = bool.Parse(data["IsPromo"]);
				this.IsDisplayTop = bool.Parse(data["IsDisplayTop"]);
				this.IsKbb = bool.Parse(data["IsKbb"]);
				this.HomeUrl = data["HomeUrl"];
				this.LinkText1 = data["LinkText1"];
				this.LinkUrl1 = data["LinkUrl1"];
				this.LinkText2 = data["LinkText2"];
				this.LinkUrl2 = data["LinkUrl2"];
				this.LinkText3 = data["LinkText3"];
				this.LinkUrl3 = data["LinkUrl3"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int affinityGroupWebProfileId = this.AffinityGroupWebProfileId;
			data.AddParam("@AffinityGroupWebProfileId", DataAccessParameterType.Numeric, affinityGroupWebProfileId.ToString());
			affinityGroupWebProfileId = this.LookupId_WebProfile;
			data.AddParam("@LookupId_WebProfile", DataAccessParameterType.Numeric, affinityGroupWebProfileId.ToString());
			bool isLogo = this.IsLogo;
			data.AddParam("@IsLogo", DataAccessParameterType.Bool, isLogo.ToString());
			isLogo = this.IsPromo;
			data.AddParam("@IsPromo", DataAccessParameterType.Bool, isLogo.ToString());
			isLogo = this.IsDisplayTop;
			data.AddParam("@IsDisplayTop", DataAccessParameterType.Bool, isLogo.ToString());
			isLogo = this.IsKbb;
			data.AddParam("@IsKbb", DataAccessParameterType.Bool, isLogo.ToString());
			data.AddParam("@HomeUrl", DataAccessParameterType.Text, this.HomeUrl);
			data.AddParam("@LinkText1", DataAccessParameterType.Text, this.LinkText1);
			data.AddParam("@LinkUrl1", DataAccessParameterType.Text, this.LinkUrl1);
			data.AddParam("@LinkText2", DataAccessParameterType.Text, this.LinkText2);
			data.AddParam("@LinkUrl2", DataAccessParameterType.Text, this.LinkUrl2);
			data.AddParam("@LinkText3", DataAccessParameterType.Text, this.LinkText3);
			data.AddParam("@LinkUrl3", DataAccessParameterType.Text, this.LinkUrl3);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("AFFINITY_GROUP_WEB_PROFILE_InsertUpdate");
			if (!data.EOF)
			{
				this.AffinityGroupWebProfileId = int.Parse(data["AffinityGroupWebProfileId"]);
			}
			this.retrievedata();
		}
	}
}