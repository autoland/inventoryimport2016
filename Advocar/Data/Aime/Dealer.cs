using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class Dealer : Transaction
	{
		private int dealerId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int lookupId_DealerType = 0;

		private string dealerName = "";

		private string address = "";

		private string address2 = "";

		private string city = "";

		private string stateCode = "";

		private string zipCode = "";

		private string phone = "";

		private string fax = "";

		private string email = "";

		private string contact = "";

		private float latitude = 0f;

		private float longitude = 0f;

		private int leadCount = 0;

		private int leadSequence = 0;

		private int lookupId_DataProvider = 0;

		private string dataProviderCode = "";

		private bool isInventoryActive = false;

		private bool isInventoryImport = false;

		private int lookupId_DealerCrmSystem = 0;

		private string dealerCrmSystemCode = "";

		public string Address
		{
			get
			{
				return this.address;
			}
			set
			{
				this.address = value;
			}
		}

		public string Address2
		{
			get
			{
				return this.address2;
			}
			set
			{
				this.address2 = value;
			}
		}

		public string City
		{
			get
			{
				return this.city;
			}
			set
			{
				this.city = value;
			}
		}

		public string Contact
		{
			get
			{
				return this.contact;
			}
			set
			{
				this.contact = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string DataProviderCode
		{
			get
			{
				return this.dataProviderCode;
			}
			set
			{
				this.dataProviderCode = value;
			}
		}

		public string DealerCrmSystemCode
		{
			get
			{
				return this.dealerCrmSystemCode;
			}
			set
			{
				this.dealerCrmSystemCode = value;
			}
		}

		public int DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public string DealerName
		{
			get
			{
				return this.dealerName;
			}
			set
			{
				this.dealerName = value;
			}
		}

		public string Email
		{
			get
			{
				return this.email;
			}
			set
			{
				this.email = value;
			}
		}

		public string Fax
		{
			get
			{
				return this.fax;
			}
			set
			{
				this.fax = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public bool IsInventoryActive
		{
			get
			{
				return this.isInventoryActive;
			}
			set
			{
				this.isInventoryActive = value;
			}
		}

		public bool IsInventoryImport
		{
			get
			{
				return this.isInventoryImport;
			}
			set
			{
				this.isInventoryImport = value;
			}
		}

		public float Latitude
		{
			get
			{
				return this.latitude;
			}
			set
			{
				this.latitude = value;
			}
		}

		public int LeadCount
		{
			get
			{
				return this.leadCount;
			}
			set
			{
				this.leadCount = value;
			}
		}

		public int LeadSequence
		{
			get
			{
				return this.leadSequence;
			}
			set
			{
				this.leadSequence = value;
			}
		}

		public float Longitude
		{
			get
			{
				return this.longitude;
			}
			set
			{
				this.longitude = value;
			}
		}

		public int LookupId_DataProvider
		{
			get
			{
				return this.lookupId_DataProvider;
			}
			set
			{
				this.lookupId_DataProvider = value;
			}
		}

		public int LookupId_DealerCrmSystem
		{
			get
			{
				return this.lookupId_DealerCrmSystem;
			}
			set
			{
				this.lookupId_DealerCrmSystem = value;
			}
		}

		public int LookupId_DealerType
		{
			get
			{
				return this.lookupId_DealerType;
			}
			set
			{
				this.lookupId_DealerType = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public string Phone
		{
			get
			{
				return this.phone;
			}
			set
			{
				this.phone = value;
			}
		}

		public string StateCode
		{
			get
			{
				return this.stateCode;
			}
			set
			{
				this.stateCode = value;
			}
		}

		public string ZipCode
		{
			get
			{
				return this.zipCode;
			}
			set
			{
				this.zipCode = value;
			}
		}

		public Dealer()
		{
		}

		public Dealer(string connection, string modifiedUserID, int dealerId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "DEALER";
			this.DealerId = dealerId;
			if (this.DealerId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@DealerId", DataAccessParameterType.Numeric, this.DealerId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("DEALER_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@DealerId", DataAccessParameterType.Numeric, this.DealerId.ToString());
			data.ExecuteProcedure("DEALER_GetRecord");
			if (!data.EOF)
			{
				this.DealerId = int.Parse(data["DealerId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.LookupId_DealerType = int.Parse(data["LookupId_DealerType"]);
				this.DealerName = data["DealerName"];
				this.Address = data["Address"];
				this.Address2 = data["Address2"];
				this.City = data["City"];
				this.StateCode = data["StateCode"];
				this.ZipCode = data["ZipCode"];
				this.Phone = data["Phone"];
				this.Fax = data["Fax"];
				this.Email = data["Email"];
				this.Contact = data["Contact"];
				this.Latitude = float.Parse(data["Latitude"]);
				this.Longitude = float.Parse(data["Longitude"]);
				this.LeadCount = int.Parse(data["LeadCount"]);
				this.LeadSequence = int.Parse(data["LeadSequence"]);
				this.LookupId_DataProvider = int.Parse(data["LookupId_DataProvider"]);
				this.DataProviderCode = data["DataProviderCode"];
				this.IsInventoryActive = bool.Parse(data["IsInventoryActive"]);
				this.IsInventoryImport = bool.Parse(data["IsInventoryImport"]);
				this.LookupId_DealerCrmSystem = int.Parse(data["LookupId_DealerCrmSystem"]);
				this.DealerCrmSystemCode = data["DealerCrmSystemCode"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int dealerId = this.DealerId;
			data.AddParam("@DealerId", DataAccessParameterType.Numeric, dealerId.ToString());
			dealerId = this.LookupId_DealerType;
			data.AddParam("@LookupId_DealerType", DataAccessParameterType.Numeric, dealerId.ToString());
			data.AddParam("@DealerName", DataAccessParameterType.Text, this.DealerName);
			data.AddParam("@Address", DataAccessParameterType.Text, this.Address);
			data.AddParam("@Address2", DataAccessParameterType.Text, this.Address2);
			data.AddParam("@City", DataAccessParameterType.Text, this.City);
			data.AddParam("@StateCode", DataAccessParameterType.Text, this.StateCode);
			data.AddParam("@ZipCode", DataAccessParameterType.Text, this.ZipCode);
			data.AddParam("@Phone", DataAccessParameterType.Text, this.Phone);
			data.AddParam("@Fax", DataAccessParameterType.Text, this.Fax);
			data.AddParam("@Email", DataAccessParameterType.Text, this.Email);
			data.AddParam("@Contact", DataAccessParameterType.Text, this.Contact);
			float latitude = this.Latitude;
			data.AddParam("@Latitude", DataAccessParameterType.Numeric, latitude.ToString());
			latitude = this.Longitude;
			data.AddParam("@Longitude", DataAccessParameterType.Numeric, latitude.ToString());
			dealerId = this.LeadCount;
			data.AddParam("@LeadCount", DataAccessParameterType.Numeric, dealerId.ToString());
			dealerId = this.LeadSequence;
			data.AddParam("@LeadSequence", DataAccessParameterType.Numeric, dealerId.ToString());
			dealerId = this.LookupId_DataProvider;
			data.AddParam("@LookupId_DataProvider", DataAccessParameterType.Numeric, dealerId.ToString());
			data.AddParam("@DataProviderCode", DataAccessParameterType.Text, this.DataProviderCode);
			bool isInventoryActive = this.IsInventoryActive;
			data.AddParam("@IsInventoryActive", DataAccessParameterType.Bool, isInventoryActive.ToString());
			isInventoryActive = this.IsInventoryImport;
			data.AddParam("@IsInventoryImport", DataAccessParameterType.Bool, isInventoryActive.ToString());
			dealerId = this.LookupId_DealerCrmSystem;
			data.AddParam("@LookupId_DealerCrmSystem", DataAccessParameterType.Numeric, dealerId.ToString());
			data.AddParam("@DealerCrmSystemCode", DataAccessParameterType.Text, this.DealerCrmSystemCode);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("DEALER_InsertUpdate");
			if (!data.EOF)
			{
				this.DealerId = int.Parse(data["DealerId"]);
			}
			this.retrievedata();
		}
	}
}