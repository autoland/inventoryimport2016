using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class Vehicle : Transaction
	{
		private int vehicleId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int inventoryImport_MappingVehicleId = 0;

		private int inventoryImport_JobExecutionFinalId = 0;

		private int lookupId_InventoryStatus = 0;

		private int lookupId_DataProvider = 0;

		private int aime_DealerId = 0;

		private float longitude = 0f;

		private float latitude = 0f;

		private string zipCode = "";

		private int configurator_VehicleYearId = 0;

		private int configurator_VehicleMakeId = 0;

		private string configuratorMakeName = "";

		private int configurator_VehicleModelId = 0;

		private string configuratorModelName = "";

		private int configurator_VehicleTrimId = 0;

		private string configuratorTrimName = "";

		private int lookupId_VehicleStyle = 0;

		private string configuratorVehicleStyle = "";

		private int lookupId_VehicleEngine = 0;

		private string configuratorVehicleEngine = "";

		private int lookupId_VehicleDrivetrain = 0;

		private string configuratorVehicleDrivetrain = "";

		private int lookupId_VehicleTransmission = 0;

		private string configuratorVehicleTransmission = "";

		private string newUsed = "";

		private string vin = "";

		private string vinPattern = "";

		private int miles = 0;

		private int doors = 0;

		private string dealerStockNo = "";

		private string feedVehicleDescription = "";

		private string feedVehicleMake = "";

		private string feedVehicleModel = "";

		private string feedVehicleTrim = "";

		private string feedVehicleStyle = "";

		private string feedVehicleEngine = "";

		private string feedVehicleDrivetrain = "";

		private string feedVehicleTransmission = "";

		private string exteriorColor = "";

		private string interiorColor = "";

		private string optionList = "";

		private int noOfOptions = 0;

		private float feedPriceRetailMsrp = 0f;

		private float feedPriceInvoiceWholesale = 0f;

		private float feedPriceSelling = 0f;

		private float feedPriceAcquisition = 0f;

		private float priceSelling = 0f;

		private float priceAcquisition = 0f;

		private int daysInInventory = 0;

		private bool isManuallyModified = false;

		private bool isAutolandWarranty = false;

		public int Aime_DealerId
		{
			get
			{
				return this.aime_DealerId;
			}
			set
			{
				this.aime_DealerId = value;
			}
		}

		public int Configurator_VehicleMakeId
		{
			get
			{
				return this.configurator_VehicleMakeId;
			}
			set
			{
				this.configurator_VehicleMakeId = value;
			}
		}

		public int Configurator_VehicleModelId
		{
			get
			{
				return this.configurator_VehicleModelId;
			}
			set
			{
				this.configurator_VehicleModelId = value;
			}
		}

		public int Configurator_VehicleTrimId
		{
			get
			{
				return this.configurator_VehicleTrimId;
			}
			set
			{
				this.configurator_VehicleTrimId = value;
			}
		}

		public int Configurator_VehicleYearId
		{
			get
			{
				return this.configurator_VehicleYearId;
			}
			set
			{
				this.configurator_VehicleYearId = value;
			}
		}

		public string ConfiguratorMakeName
		{
			get
			{
				return this.configuratorMakeName;
			}
			set
			{
				this.configuratorMakeName = value;
			}
		}

		public string ConfiguratorModelName
		{
			get
			{
				return this.configuratorModelName;
			}
			set
			{
				this.configuratorModelName = value;
			}
		}

		public string ConfiguratorTrimName
		{
			get
			{
				return this.configuratorTrimName;
			}
			set
			{
				this.configuratorTrimName = value;
			}
		}

		public string ConfiguratorVehicleDrivetrain
		{
			get
			{
				return this.configuratorVehicleDrivetrain;
			}
			set
			{
				this.configuratorVehicleDrivetrain = value;
			}
		}

		public string ConfiguratorVehicleEngine
		{
			get
			{
				return this.configuratorVehicleEngine;
			}
			set
			{
				this.configuratorVehicleEngine = value;
			}
		}

		public string ConfiguratorVehicleStyle
		{
			get
			{
				return this.configuratorVehicleStyle;
			}
			set
			{
				this.configuratorVehicleStyle = value;
			}
		}

		public string ConfiguratorVehicleTransmission
		{
			get
			{
				return this.configuratorVehicleTransmission;
			}
			set
			{
				this.configuratorVehicleTransmission = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public int DaysInInventory
		{
			get
			{
				return this.daysInInventory;
			}
			set
			{
				this.daysInInventory = value;
			}
		}

		public string DealerStockNo
		{
			get
			{
				return this.dealerStockNo;
			}
			set
			{
				this.dealerStockNo = value;
			}
		}

		public int Doors
		{
			get
			{
				return this.doors;
			}
			set
			{
				this.doors = value;
			}
		}

		public string ExteriorColor
		{
			get
			{
				return this.exteriorColor;
			}
			set
			{
				this.exteriorColor = value;
			}
		}

		public float FeedPriceAcquisition
		{
			get
			{
				return this.feedPriceAcquisition;
			}
			set
			{
				this.feedPriceAcquisition = value;
			}
		}

		public float FeedPriceInvoiceWholesale
		{
			get
			{
				return this.feedPriceInvoiceWholesale;
			}
			set
			{
				this.feedPriceInvoiceWholesale = value;
			}
		}

		public float FeedPriceRetailMsrp
		{
			get
			{
				return this.feedPriceRetailMsrp;
			}
			set
			{
				this.feedPriceRetailMsrp = value;
			}
		}

		public float FeedPriceSelling
		{
			get
			{
				return this.feedPriceSelling;
			}
			set
			{
				this.feedPriceSelling = value;
			}
		}

		public string FeedVehicleDescription
		{
			get
			{
				return this.feedVehicleDescription;
			}
			set
			{
				this.feedVehicleDescription = value;
			}
		}

		public string FeedVehicleDrivetrain
		{
			get
			{
				return this.feedVehicleDrivetrain;
			}
			set
			{
				this.feedVehicleDrivetrain = value;
			}
		}

		public string FeedVehicleEngine
		{
			get
			{
				return this.feedVehicleEngine;
			}
			set
			{
				this.feedVehicleEngine = value;
			}
		}

		public string FeedVehicleMake
		{
			get
			{
				return this.feedVehicleMake;
			}
			set
			{
				this.feedVehicleMake = value;
			}
		}

		public string FeedVehicleModel
		{
			get
			{
				return this.feedVehicleModel;
			}
			set
			{
				this.feedVehicleModel = value;
			}
		}

		public string FeedVehicleStyle
		{
			get
			{
				return this.feedVehicleStyle;
			}
			set
			{
				this.feedVehicleStyle = value;
			}
		}

		public string FeedVehicleTransmission
		{
			get
			{
				return this.feedVehicleTransmission;
			}
			set
			{
				this.feedVehicleTransmission = value;
			}
		}

		public string FeedVehicleTrim
		{
			get
			{
				return this.feedVehicleTrim;
			}
			set
			{
				this.feedVehicleTrim = value;
			}
		}

		public string InteriorColor
		{
			get
			{
				return this.interiorColor;
			}
			set
			{
				this.interiorColor = value;
			}
		}

		public int InventoryImport_JobExecutionFinalId
		{
			get
			{
				return this.inventoryImport_JobExecutionFinalId;
			}
			set
			{
				this.inventoryImport_JobExecutionFinalId = value;
			}
		}

		public int InventoryImport_MappingVehicleId
		{
			get
			{
				return this.inventoryImport_MappingVehicleId;
			}
			set
			{
				this.inventoryImport_MappingVehicleId = value;
			}
		}

		public bool IsAutolandWarranty
		{
			get
			{
				return this.isAutolandWarranty;
			}
			set
			{
				this.isAutolandWarranty = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public bool IsManuallyModified
		{
			get
			{
				return this.isManuallyModified;
			}
			set
			{
				this.isManuallyModified = value;
			}
		}

		public float Latitude
		{
			get
			{
				return this.latitude;
			}
			set
			{
				this.latitude = value;
			}
		}

		public float Longitude
		{
			get
			{
				return this.longitude;
			}
			set
			{
				this.longitude = value;
			}
		}

		public int LookupId_DataProvider
		{
			get
			{
				return this.lookupId_DataProvider;
			}
			set
			{
				this.lookupId_DataProvider = value;
			}
		}

		public int LookupId_InventoryStatus
		{
			get
			{
				return this.lookupId_InventoryStatus;
			}
			set
			{
				this.lookupId_InventoryStatus = value;
			}
		}

		public int LookupId_VehicleDrivetrain
		{
			get
			{
				return this.lookupId_VehicleDrivetrain;
			}
			set
			{
				this.lookupId_VehicleDrivetrain = value;
			}
		}

		public int LookupId_VehicleEngine
		{
			get
			{
				return this.lookupId_VehicleEngine;
			}
			set
			{
				this.lookupId_VehicleEngine = value;
			}
		}

		public int LookupId_VehicleStyle
		{
			get
			{
				return this.lookupId_VehicleStyle;
			}
			set
			{
				this.lookupId_VehicleStyle = value;
			}
		}

		public int LookupId_VehicleTransmission
		{
			get
			{
				return this.lookupId_VehicleTransmission;
			}
			set
			{
				this.lookupId_VehicleTransmission = value;
			}
		}

		public int Miles
		{
			get
			{
				return this.miles;
			}
			set
			{
				this.miles = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public string NewUsed
		{
			get
			{
				return this.newUsed;
			}
			set
			{
				this.newUsed = value;
			}
		}

		public int NoOfOptions
		{
			get
			{
				return this.noOfOptions;
			}
			set
			{
				this.noOfOptions = value;
			}
		}

		public string OptionList
		{
			get
			{
				return this.optionList;
			}
			set
			{
				this.optionList = value;
			}
		}

		public float PriceAcquisition
		{
			get
			{
				return this.priceAcquisition;
			}
			set
			{
				this.priceAcquisition = value;
			}
		}

		public float PriceSelling
		{
			get
			{
				return this.priceSelling;
			}
			set
			{
				this.priceSelling = value;
			}
		}

		public int VehicleId
		{
			get
			{
				return this.vehicleId;
			}
			set
			{
				this.vehicleId = value;
			}
		}

		public string Vin
		{
			get
			{
				return this.vin;
			}
			set
			{
				this.vin = value;
			}
		}

		public string VinPattern
		{
			get
			{
				return this.vinPattern;
			}
			set
			{
				this.vinPattern = value;
			}
		}

		public string ZipCode
		{
			get
			{
				return this.zipCode;
			}
			set
			{
				this.zipCode = value;
			}
		}

		public Vehicle()
		{
		}

		public Vehicle(string connection, string modifiedUserID, int vehicleId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.VehicleId = vehicleId;
			if (this.VehicleId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleId", DataAccessParameterType.Numeric, this.VehicleId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleId", DataAccessParameterType.Numeric, this.VehicleId.ToString());
			data.ExecuteProcedure("VEHICLE_GetRecord");
			if (!data.EOF)
			{
				this.VehicleId = int.Parse(data["VehicleId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.InventoryImport_MappingVehicleId = int.Parse(data["InventoryImport_MappingVehicleId"]);
				this.InventoryImport_JobExecutionFinalId = int.Parse(data["InventoryImport_JobExecutionFinalId"]);
				this.LookupId_InventoryStatus = int.Parse(data["LookupId_InventoryStatus"]);
				this.LookupId_DataProvider = int.Parse(data["LookupId_DataProvider"]);
				this.Aime_DealerId = int.Parse(data["Aime_DealerId"]);
				this.Longitude = float.Parse(data["Longitude"]);
				this.Latitude = float.Parse(data["Latitude"]);
				this.ZipCode = data["ZipCode"];
				this.Configurator_VehicleYearId = int.Parse(data["Configurator_VehicleYearId"]);
				this.Configurator_VehicleMakeId = int.Parse(data["Configurator_VehicleMakeId"]);
				this.ConfiguratorMakeName = data["ConfiguratorMakeName"];
				this.Configurator_VehicleModelId = int.Parse(data["Configurator_VehicleModelId"]);
				this.ConfiguratorModelName = data["ConfiguratorModelName"];
				this.Configurator_VehicleTrimId = int.Parse(data["Configurator_VehicleTrimId"]);
				this.ConfiguratorTrimName = data["ConfiguratorTrimName"];
				this.LookupId_VehicleStyle = int.Parse(data["LookupId_VehicleStyle"]);
				this.ConfiguratorVehicleStyle = data["ConfiguratorVehicleStyle"];
				this.LookupId_VehicleEngine = int.Parse(data["LookupId_VehicleEngine"]);
				this.ConfiguratorVehicleEngine = data["ConfiguratorVehicleEngine"];
				this.LookupId_VehicleDrivetrain = int.Parse(data["LookupId_VehicleDrivetrain"]);
				this.ConfiguratorVehicleDrivetrain = data["ConfiguratorVehicleDrivetrain"];
				this.LookupId_VehicleTransmission = int.Parse(data["LookupId_VehicleTransmission"]);
				this.ConfiguratorVehicleTransmission = data["ConfiguratorVehicleTransmission"];
				this.NewUsed = data["NewUsed"];
				this.Vin = data["Vin"];
				this.VinPattern = data["VinPattern"];
				this.Miles = int.Parse(data["Miles"]);
				this.Doors = int.Parse(data["Doors"]);
				this.DealerStockNo = data["DealerStockNo"];
				this.FeedVehicleDescription = data["FeedVehicleDescription"];
				this.FeedVehicleMake = data["FeedVehicleMake"];
				this.FeedVehicleModel = data["FeedVehicleModel"];
				this.FeedVehicleTrim = data["FeedVehicleTrim"];
				this.FeedVehicleStyle = data["FeedVehicleStyle"];
				this.FeedVehicleEngine = data["FeedVehicleEngine"];
				this.FeedVehicleDrivetrain = data["FeedVehicleDrivetrain"];
				this.FeedVehicleTransmission = data["FeedVehicleTransmission"];
				this.ExteriorColor = data["ExteriorColor"];
				this.InteriorColor = data["InteriorColor"];
				this.OptionList = data["OptionList"];
				this.NoOfOptions = int.Parse(data["NoOfOptions"]);
				this.FeedPriceRetailMsrp = float.Parse(data["FeedPriceRetailMsrp"]);
				this.FeedPriceInvoiceWholesale = float.Parse(data["FeedPriceInvoiceWholesale"]);
				this.FeedPriceSelling = float.Parse(data["FeedPriceSelling"]);
				this.FeedPriceAcquisition = float.Parse(data["FeedPriceAcquisition"]);
				this.PriceSelling = float.Parse(data["PriceSelling"]);
				this.PriceAcquisition = float.Parse(data["PriceAcquisition"]);
				this.DaysInInventory = int.Parse(data["DaysInInventory"]);
				this.IsManuallyModified = bool.Parse(data["IsManuallyModified"]);
				this.IsAutolandWarranty = bool.Parse(data["IsAutolandWarranty"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int vehicleId = this.VehicleId;
			data.AddParam("@VehicleId", DataAccessParameterType.Numeric, vehicleId.ToString());
			vehicleId = this.InventoryImport_MappingVehicleId;
			data.AddParam("@InventoryImport_MappingVehicleId", DataAccessParameterType.Numeric, vehicleId.ToString());
			vehicleId = this.InventoryImport_JobExecutionFinalId;
			data.AddParam("@InventoryImport_JobExecutionFinalId", DataAccessParameterType.Numeric, vehicleId.ToString());
			vehicleId = this.LookupId_InventoryStatus;
			data.AddParam("@LookupId_InventoryStatus", DataAccessParameterType.Numeric, vehicleId.ToString());
			vehicleId = this.LookupId_DataProvider;
			data.AddParam("@LookupId_DataProvider", DataAccessParameterType.Numeric, vehicleId.ToString());
			vehicleId = this.Aime_DealerId;
			data.AddParam("@Aime_DealerId", DataAccessParameterType.Numeric, vehicleId.ToString());
			float longitude = this.Longitude;
			data.AddParam("@Longitude", DataAccessParameterType.Numeric, longitude.ToString());
			longitude = this.Latitude;
			data.AddParam("@Latitude", DataAccessParameterType.Numeric, longitude.ToString());
			data.AddParam("@ZipCode", DataAccessParameterType.Text, this.ZipCode);
			vehicleId = this.Configurator_VehicleYearId;
			data.AddParam("@Configurator_VehicleYearId", DataAccessParameterType.Numeric, vehicleId.ToString());
			vehicleId = this.Configurator_VehicleMakeId;
			data.AddParam("@Configurator_VehicleMakeId", DataAccessParameterType.Numeric, vehicleId.ToString());
			data.AddParam("@ConfiguratorMakeName", DataAccessParameterType.Text, this.ConfiguratorMakeName);
			vehicleId = this.Configurator_VehicleModelId;
			data.AddParam("@Configurator_VehicleModelId", DataAccessParameterType.Numeric, vehicleId.ToString());
			data.AddParam("@ConfiguratorModelName", DataAccessParameterType.Text, this.ConfiguratorModelName);
			vehicleId = this.Configurator_VehicleTrimId;
			data.AddParam("@Configurator_VehicleTrimId", DataAccessParameterType.Numeric, vehicleId.ToString());
			data.AddParam("@ConfiguratorTrimName", DataAccessParameterType.Text, this.ConfiguratorTrimName);
			vehicleId = this.LookupId_VehicleStyle;
			data.AddParam("@LookupId_VehicleStyle", DataAccessParameterType.Numeric, vehicleId.ToString());
			data.AddParam("@ConfiguratorVehicleStyle", DataAccessParameterType.Text, this.ConfiguratorVehicleStyle);
			vehicleId = this.LookupId_VehicleEngine;
			data.AddParam("@LookupId_VehicleEngine", DataAccessParameterType.Numeric, vehicleId.ToString());
			data.AddParam("@ConfiguratorVehicleEngine", DataAccessParameterType.Text, this.ConfiguratorVehicleEngine);
			vehicleId = this.LookupId_VehicleDrivetrain;
			data.AddParam("@LookupId_VehicleDrivetrain", DataAccessParameterType.Numeric, vehicleId.ToString());
			data.AddParam("@ConfiguratorVehicleDrivetrain", DataAccessParameterType.Text, this.ConfiguratorVehicleDrivetrain);
			vehicleId = this.LookupId_VehicleTransmission;
			data.AddParam("@LookupId_VehicleTransmission", DataAccessParameterType.Numeric, vehicleId.ToString());
			data.AddParam("@ConfiguratorVehicleTransmission", DataAccessParameterType.Text, this.ConfiguratorVehicleTransmission);
			data.AddParam("@NewUsed", DataAccessParameterType.Text, this.NewUsed);
			data.AddParam("@Vin", DataAccessParameterType.Text, this.Vin);
			data.AddParam("@VinPattern", DataAccessParameterType.Text, this.VinPattern);
			vehicleId = this.Miles;
			data.AddParam("@Miles", DataAccessParameterType.Numeric, vehicleId.ToString());
			vehicleId = this.Doors;
			data.AddParam("@Doors", DataAccessParameterType.Numeric, vehicleId.ToString());
			data.AddParam("@DealerStockNo", DataAccessParameterType.Text, this.DealerStockNo);
			data.AddParam("@FeedVehicleDescription", DataAccessParameterType.Text, this.FeedVehicleDescription);
			data.AddParam("@FeedVehicleMake", DataAccessParameterType.Text, this.FeedVehicleMake);
			data.AddParam("@FeedVehicleModel", DataAccessParameterType.Text, this.FeedVehicleModel);
			data.AddParam("@FeedVehicleTrim", DataAccessParameterType.Text, this.FeedVehicleTrim);
			data.AddParam("@FeedVehicleStyle", DataAccessParameterType.Text, this.FeedVehicleStyle);
			data.AddParam("@FeedVehicleEngine", DataAccessParameterType.Text, this.FeedVehicleEngine);
			data.AddParam("@FeedVehicleDrivetrain", DataAccessParameterType.Text, this.FeedVehicleDrivetrain);
			data.AddParam("@FeedVehicleTransmission", DataAccessParameterType.Text, this.FeedVehicleTransmission);
			data.AddParam("@ExteriorColor", DataAccessParameterType.Text, this.ExteriorColor);
			data.AddParam("@InteriorColor", DataAccessParameterType.Text, this.InteriorColor);
			data.AddParam("@OptionList", DataAccessParameterType.Text, this.OptionList);
			vehicleId = this.NoOfOptions;
			data.AddParam("@NoOfOptions", DataAccessParameterType.Numeric, vehicleId.ToString());
			longitude = this.FeedPriceRetailMsrp;
			data.AddParam("@FeedPriceRetailMsrp", DataAccessParameterType.Numeric, longitude.ToString());
			longitude = this.FeedPriceInvoiceWholesale;
			data.AddParam("@FeedPriceInvoiceWholesale", DataAccessParameterType.Numeric, longitude.ToString());
			longitude = this.FeedPriceSelling;
			data.AddParam("@FeedPriceSelling", DataAccessParameterType.Numeric, longitude.ToString());
			longitude = this.FeedPriceAcquisition;
			data.AddParam("@FeedPriceAcquisition", DataAccessParameterType.Numeric, longitude.ToString());
			longitude = this.PriceSelling;
			data.AddParam("@PriceSelling", DataAccessParameterType.Numeric, longitude.ToString());
			longitude = this.PriceAcquisition;
			data.AddParam("@PriceAcquisition", DataAccessParameterType.Numeric, longitude.ToString());
			vehicleId = this.DaysInInventory;
			data.AddParam("@DaysInInventory", DataAccessParameterType.Numeric, vehicleId.ToString());
			bool isManuallyModified = this.IsManuallyModified;
			data.AddParam("@IsManuallyModified", DataAccessParameterType.Bool, isManuallyModified.ToString());
			isManuallyModified = this.IsAutolandWarranty;
			data.AddParam("@IsAutolandWarranty", DataAccessParameterType.Bool, isManuallyModified.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_InsertUpdate");
			if (!data.EOF)
			{
				this.VehicleId = int.Parse(data["VehicleId"]);
			}
			this.retrievedata();
		}
	}
}