using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class Vendor : Transaction
	{
		private int vendorId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int lookupId_VendorType = 0;

		private int addressId = 0;

		private string vendorName = "";

		private string vendorContact = "";

		private int noteId = 0;

		private string msreplTranVersion = "";

		public int AddressId
		{
			get
			{
				return this.addressId;
			}
			set
			{
				this.addressId = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public int LookupId_VendorType
		{
			get
			{
				return this.lookupId_VendorType;
			}
			set
			{
				this.lookupId_VendorType = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public string MsreplTranVersion
		{
			get
			{
				return this.msreplTranVersion;
			}
			set
			{
				this.msreplTranVersion = value;
			}
		}

		public int NoteId
		{
			get
			{
				return this.noteId;
			}
			set
			{
				this.noteId = value;
			}
		}

		public string VendorContact
		{
			get
			{
				return this.vendorContact;
			}
			set
			{
				this.vendorContact = value;
			}
		}

		public int VendorId
		{
			get
			{
				return this.vendorId;
			}
			set
			{
				this.vendorId = value;
			}
		}

		public string VendorName
		{
			get
			{
				return this.vendorName;
			}
			set
			{
				this.vendorName = value;
			}
		}

		public Vendor()
		{
		}

		public Vendor(string connection, string modifiedUserID, int vendorId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "VENDOR";
			this.VendorId = vendorId;
			if (this.VendorId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VendorId", DataAccessParameterType.Numeric, this.VendorId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VENDOR_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VendorId", DataAccessParameterType.Numeric, this.VendorId.ToString());
			data.ExecuteProcedure("VENDOR_GetRecord");
			if (!data.EOF)
			{
				this.VendorId = int.Parse(data["VendorId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.LookupId_VendorType = int.Parse(data["LookupId_VendorType"]);
				this.AddressId = int.Parse(data["AddressId"]);
				this.VendorName = data["VendorName"];
				this.VendorContact = data["VendorContact"];
				this.NoteId = int.Parse(data["NoteId"]);
				this.MsreplTranVersion = data["MsreplTranVersion"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int vendorId = this.VendorId;
			data.AddParam("@VendorId", DataAccessParameterType.Numeric, vendorId.ToString());
			vendorId = this.LookupId_VendorType;
			data.AddParam("@LookupId_VendorType", DataAccessParameterType.Numeric, vendorId.ToString());
			vendorId = this.AddressId;
			data.AddParam("@AddressId", DataAccessParameterType.Numeric, vendorId.ToString());
			data.AddParam("@VendorName", DataAccessParameterType.Text, this.VendorName);
			data.AddParam("@VendorContact", DataAccessParameterType.Text, this.VendorContact);
			vendorId = this.NoteId;
			data.AddParam("@NoteId", DataAccessParameterType.Numeric, vendorId.ToString());
			data.AddParam("@MsreplTranVersion", DataAccessParameterType.Text, this.MsreplTranVersion);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VENDOR_InsertUpdate");
			if (!data.EOF)
			{
				this.VendorId = int.Parse(data["VendorId"]);
			}
			this.retrievedata();
		}
	}
}