using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class ProductPricingCertifiedPlus : Transaction
	{
		private int productPricingCertifiedPlusId = 0;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private bool isDeleted = false;

		private float premiumCost = 0f;

		private int termYears = 0;

		private int isCertPlusOrPDROnly = 0;

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public int IsCertPlusOrPDROnly
		{
			get
			{
				return this.isCertPlusOrPDROnly;
			}
			set
			{
				this.isCertPlusOrPDROnly = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public float PremiumCost
		{
			get
			{
				return this.premiumCost;
			}
			set
			{
				this.premiumCost = value;
			}
		}

		public int ProductPricingCertifiedPlusId
		{
			get
			{
				return this.productPricingCertifiedPlusId;
			}
			set
			{
				this.productPricingCertifiedPlusId = value;
			}
		}

		public int TermYears
		{
			get
			{
				return this.termYears;
			}
			set
			{
				this.termYears = value;
			}
		}

		public ProductPricingCertifiedPlus()
		{
		}

		public ProductPricingCertifiedPlus(string connection, string modifiedUserID, int productPricingCertifiedPlusId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.ProductPricingCertifiedPlusId = productPricingCertifiedPlusId;
			if (this.ProductPricingCertifiedPlusId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ProductPricingCertifiedPlusId", DataAccessParameterType.Numeric, this.ProductPricingCertifiedPlusId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("PRODUCT_PRICING_CERTIFIED_PLUS_Delete");
			this.wipeout();
		}

		public static int GetRecordId(int isCertPlusOrPDROnly, int termYears, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@IsCertPlusOrPDROnly", DataAccessParameterType.Numeric, isCertPlusOrPDROnly.ToString());
			data.AddParam("@TermYears", DataAccessParameterType.Numeric, termYears.ToString());
			data.ExecuteProcedure("PRODUCT_PRICING_CERTIFIED_PLUS_GetRecord");
			num = (!data.EOF ? int.Parse(data["ProductPricingCertifiedPlusId"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ProductPricingCertifiedPlusId", DataAccessParameterType.Numeric, this.ProductPricingCertifiedPlusId.ToString());
			data.ExecuteProcedure("PRODUCT_PRICING_CERTIFIED_PLUS_GetRecord");
			if (!data.EOF)
			{
				this.ProductPricingCertifiedPlusId = int.Parse(data["ProductPricingCertifiedPlusId"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.PremiumCost = float.Parse(data["PremiumCost"]);
				this.TermYears = int.Parse(data["TermYears"]);
				this.IsCertPlusOrPDROnly = int.Parse(data["IsCertPlusOrPDROnly"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int productPricingCertifiedPlusId = this.ProductPricingCertifiedPlusId;
			data.AddParam("@ProductPricingCertifiedPlusId", DataAccessParameterType.Numeric, productPricingCertifiedPlusId.ToString());
			data.AddParam("@PremiumCost", DataAccessParameterType.Numeric, this.PremiumCost.ToString());
			productPricingCertifiedPlusId = this.TermYears;
			data.AddParam("@TermYears", DataAccessParameterType.Numeric, productPricingCertifiedPlusId.ToString());
			productPricingCertifiedPlusId = this.IsCertPlusOrPDROnly;
			data.AddParam("@IsCertPlusOrPDROnly", DataAccessParameterType.Numeric, productPricingCertifiedPlusId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("PRODUCT_PRICING_CERTIFIED_PLUS_InsertUpdate");
			if (!data.EOF)
			{
				this.ProductPricingCertifiedPlusId = int.Parse(data["ProductPricingCertifiedPlusId"]);
			}
			this.retrievedata();
		}
	}
}