using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class ProductCommissionRate : Transaction
	{
		private int productCommissionRateId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int oldId = 0;

		private int affinityGroupId = 0;

		private int affinityGroupBranchId = 0;

		private int productId = 0;

		private string effectiveDate = "";

		private int quantityStart = 0;

		private int quantityEnd = 0;

		private float affinityGroupPercent = 0f;

		private float affinityGroupAmount = 0f;

		private float agentPercent = 0f;

		private float agentAmount = 0f;

		private float autolandPercent = 0f;

		private float autolandAmount = 0f;

		private bool isEnabled = false;

		public float AffinityGroupAmount
		{
			get
			{
				return this.affinityGroupAmount;
			}
			set
			{
				this.affinityGroupAmount = value;
			}
		}

		public int AffinityGroupBranchId
		{
			get
			{
				return this.affinityGroupBranchId;
			}
			set
			{
				this.affinityGroupBranchId = value;
			}
		}

		public int AffinityGroupId
		{
			get
			{
				return this.affinityGroupId;
			}
			set
			{
				this.affinityGroupId = value;
			}
		}

		public float AffinityGroupPercent
		{
			get
			{
				return this.affinityGroupPercent;
			}
			set
			{
				this.affinityGroupPercent = value;
			}
		}

		public float AgentAmount
		{
			get
			{
				return this.agentAmount;
			}
			set
			{
				this.agentAmount = value;
			}
		}

		public float AgentPercent
		{
			get
			{
				return this.agentPercent;
			}
			set
			{
				this.agentPercent = value;
			}
		}

		public float AutolandAmount
		{
			get
			{
				return this.autolandAmount;
			}
			set
			{
				this.autolandAmount = value;
			}
		}

		public float AutolandPercent
		{
			get
			{
				return this.autolandPercent;
			}
			set
			{
				this.autolandPercent = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string EffectiveDate
		{
			get
			{
				return this.effectiveDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.effectiveDate = "";
				}
				else
				{
					this.effectiveDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public bool IsEnabled
		{
			get
			{
				return this.isEnabled;
			}
			set
			{
				this.isEnabled = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public int OldId
		{
			get
			{
				return this.oldId;
			}
			set
			{
				this.oldId = value;
			}
		}

		public int ProductCommissionRateId
		{
			get
			{
				return this.productCommissionRateId;
			}
			set
			{
				this.productCommissionRateId = value;
			}
		}

		public int ProductId
		{
			get
			{
				return this.productId;
			}
			set
			{
				this.productId = value;
			}
		}

		public int QuantityEnd
		{
			get
			{
				return this.quantityEnd;
			}
			set
			{
				this.quantityEnd = value;
			}
		}

		public int QuantityStart
		{
			get
			{
				return this.quantityStart;
			}
			set
			{
				this.quantityStart = value;
			}
		}

		public ProductCommissionRate()
		{
		}

		public ProductCommissionRate(string connection, string modifiedUserID, int productCommissionRateId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.ProductCommissionRateId = productCommissionRateId;
			if (this.ProductCommissionRateId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ProductCommissionRateId", DataAccessParameterType.Numeric, this.ProductCommissionRateId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("PRODUCT_COMMISSION_RATE_Delete");
			this.wipeout();
		}

		public void DeleteAll()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, this.affinityGroupId.ToString());
			data.AddParam("@ProductId", DataAccessParameterType.Numeric, this.ProductId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.AddParam("@QuantityStart", DataAccessParameterType.Numeric, this.quantityStart.ToString());
			data.AddParam("@QuantityEnd", DataAccessParameterType.Numeric, this.quantityEnd.ToString());
			data.ExecuteProcedure("PRODUCT_COMMISSION_RATE_DeleteBatch");
			this.wipeout();
		}

		public static int GetRecordId(int productId, int affinityGroupId, int affinityGroupBranchId, int quantityStart, int quantityEnd, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, affinityGroupId.ToString());
			data.AddParam("@AffinityGroupBranchId", DataAccessParameterType.Numeric, affinityGroupBranchId.ToString());
			data.AddParam("@ProductId", DataAccessParameterType.Numeric, productId.ToString());
			data.AddParam("@QuantityStart", DataAccessParameterType.Numeric, quantityStart.ToString());
			data.AddParam("@QuantityEnd", DataAccessParameterType.Numeric, quantityEnd.ToString());
			data.ExecuteProcedure("PRODUCT_COMMISSION_RATE_GetRecordConflict");
			num = (!data.EOF ? int.Parse(data["ProductCommissionRateId"]) : 0);
			return num;
		}

		public static int GetRecordId(int productId, int affinityGroupId, int affinityGroupBranchId, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, affinityGroupId.ToString());
			data.AddParam("@AffinityGroupBranchId", DataAccessParameterType.Numeric, affinityGroupBranchId.ToString());
			data.AddParam("@ProductId", DataAccessParameterType.Numeric, productId.ToString());
			data.ExecuteProcedure("PRODUCT_COMMISSION_RATE_GetRecord");
			num = (!data.EOF ? int.Parse(data["ProductCommissionRateId"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ProductCommissionRateId", DataAccessParameterType.Numeric, this.ProductCommissionRateId.ToString());
			data.ExecuteProcedure("PRODUCT_COMMISSION_RATE_GetRecord");
			if (!data.EOF)
			{
				this.ProductCommissionRateId = int.Parse(data["ProductCommissionRateId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.OldId = int.Parse(data["OldId"]);
				this.AffinityGroupId = int.Parse(data["AffinityGroupId"]);
				this.AffinityGroupBranchId = int.Parse(data["AffinityGroupBranchId"]);
				this.ProductId = int.Parse(data["ProductId"]);
				this.EffectiveDate = data["EffectiveDate"];
				this.QuantityStart = int.Parse(data["QuantityStart"]);
				this.QuantityEnd = int.Parse(data["QuantityEnd"]);
				this.AffinityGroupPercent = float.Parse(data["AffinityGroupPercent"]);
				this.AffinityGroupAmount = float.Parse(data["AffinityGroupAmount"]);
				this.AgentPercent = float.Parse(data["AgentPercent"]);
				this.AgentAmount = float.Parse(data["AgentAmount"]);
				this.AutolandPercent = float.Parse(data["AutolandPercent"]);
				this.AutolandAmount = float.Parse(data["AutolandAmount"]);
				this.IsEnabled = bool.Parse(data["IsEnabled"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int productCommissionRateId = this.ProductCommissionRateId;
			data.AddParam("@ProductCommissionRateId", DataAccessParameterType.Numeric, productCommissionRateId.ToString());
			productCommissionRateId = this.OldId;
			data.AddParam("@OldId", DataAccessParameterType.Numeric, productCommissionRateId.ToString());
			productCommissionRateId = this.AffinityGroupId;
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, productCommissionRateId.ToString());
			productCommissionRateId = this.AffinityGroupBranchId;
			data.AddParam("@AffinityGroupBranchId", DataAccessParameterType.Numeric, productCommissionRateId.ToString());
			productCommissionRateId = this.ProductId;
			data.AddParam("@ProductId", DataAccessParameterType.Numeric, productCommissionRateId.ToString());
			data.AddParam("@EffectiveDate", DataAccessParameterType.DateTime, this.EffectiveDate);
			productCommissionRateId = this.QuantityStart;
			data.AddParam("@QuantityStart", DataAccessParameterType.Numeric, productCommissionRateId.ToString());
			productCommissionRateId = this.QuantityEnd;
			data.AddParam("@QuantityEnd", DataAccessParameterType.Numeric, productCommissionRateId.ToString());
			float affinityGroupPercent = this.AffinityGroupPercent;
			data.AddParam("@AffinityGroupPercent", DataAccessParameterType.Numeric, affinityGroupPercent.ToString());
			affinityGroupPercent = this.AffinityGroupAmount;
			data.AddParam("@AffinityGroupAmount", DataAccessParameterType.Numeric, affinityGroupPercent.ToString());
			affinityGroupPercent = this.AgentPercent;
			data.AddParam("@AgentPercent", DataAccessParameterType.Numeric, affinityGroupPercent.ToString());
			affinityGroupPercent = this.AgentAmount;
			data.AddParam("@AgentAmount", DataAccessParameterType.Numeric, affinityGroupPercent.ToString());
			affinityGroupPercent = this.AutolandPercent;
			data.AddParam("@AutolandPercent", DataAccessParameterType.Numeric, affinityGroupPercent.ToString());
			affinityGroupPercent = this.AutolandAmount;
			data.AddParam("@AutolandAmount", DataAccessParameterType.Numeric, affinityGroupPercent.ToString());
			data.AddParam("@IsEnabled", DataAccessParameterType.Bool, this.IsEnabled.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("PRODUCT_COMMISSION_RATE_InsertUpdate");
			if (!data.EOF)
			{
				this.ProductCommissionRateId = int.Parse(data["ProductCommissionRateId"]);
			}
			this.retrievedata();
		}

		public void SaveAll()
		{
			DataAccess data = new DataAccess(this.connection);
			int affinityGroupId = this.AffinityGroupId;
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, affinityGroupId.ToString());
			affinityGroupId = this.ProductId;
			data.AddParam("@ProductId", DataAccessParameterType.Numeric, affinityGroupId.ToString());
			data.AddParam("@EffectiveDate", DataAccessParameterType.DateTime, this.EffectiveDate);
			affinityGroupId = this.QuantityStart;
			data.AddParam("@QuantityStart", DataAccessParameterType.Numeric, affinityGroupId.ToString());
			affinityGroupId = this.QuantityEnd;
			data.AddParam("@QuantityEnd", DataAccessParameterType.Numeric, affinityGroupId.ToString());
			float affinityGroupPercent = this.AffinityGroupPercent;
			data.AddParam("@AffinityGroupPercent", DataAccessParameterType.Numeric, affinityGroupPercent.ToString());
			affinityGroupPercent = this.AffinityGroupAmount;
			data.AddParam("@AffinityGroupAmount", DataAccessParameterType.Numeric, affinityGroupPercent.ToString());
			affinityGroupPercent = this.AgentPercent;
			data.AddParam("@AgentPercent", DataAccessParameterType.Numeric, affinityGroupPercent.ToString());
			affinityGroupPercent = this.AgentAmount;
			data.AddParam("@AgentAmount", DataAccessParameterType.Numeric, affinityGroupPercent.ToString());
			affinityGroupPercent = this.AutolandPercent;
			data.AddParam("@AutolandPercent", DataAccessParameterType.Numeric, affinityGroupPercent.ToString());
			affinityGroupPercent = this.AutolandAmount;
			data.AddParam("@AutolandAmount", DataAccessParameterType.Numeric, affinityGroupPercent.ToString());
			data.AddParam("@IsEnabled", DataAccessParameterType.Bool, this.IsEnabled.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("PRODUCT_COMMISSION_RATE_InsertBatch");
			if (!data.EOF)
			{
				this.ProductCommissionRateId = int.Parse(data["ProductCommissionRateId"]);
			}
			this.retrievedata();
		}
	}
}