using System;

namespace Advocar.Data.Aime
{
	public enum LookupType
	{
		ProductPaymentType,
		ProductCancelReason,
		ProductSeller,
		ProductContractStatus,
		CertifiedPlusStatus,
		State,
		VendorType,
		WebProfile,
		WebTool,
		SurveyStatus,
		MbpMileageBand,
		MbpPlan,
		MbpStatus,
		VehicleEngine,
		VehicleTruckCab,
		VehicleTruckBed,
		VehicleDrivetrain,
		VehicleTransmission,
		VehicleStyle,
		VehicleFuel,
		DataProvider,
		ConfiguratorDataSource,
		JobExecutionStatus,
		DealerType,
		ConfiguratorColorType,
		ConfiguratorOptionGroup,
		ConfiguratorPictureType,
		InventoryFilterField,
		ConfiguratorFeatureCategory,
		ConfiguratorWarrantyType,
		InventoryStatus,
		GapPlan,
		GapStatus,
		WebPageMaster,
		LoanStatus,
		LoanStatusCancelReason,
		DealerCrmSystem
	}
}