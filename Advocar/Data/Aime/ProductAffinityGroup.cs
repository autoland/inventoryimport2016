using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class ProductAffinityGroup : Transaction
	{
		private int productAffinityGroupId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int affinityGroupId = 0;

		private int productId = 0;

		private int productAgentId = 0;

		private float productMaximumAmount = 0f;

		private string effectiveDate = "";

		public int AffinityGroupId
		{
			get
			{
				return this.affinityGroupId;
			}
			set
			{
				this.affinityGroupId = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string EffectiveDate
		{
			get
			{
				return this.effectiveDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.effectiveDate = "";
				}
				else
				{
					this.effectiveDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public int ProductAffinityGroupId
		{
			get
			{
				return this.productAffinityGroupId;
			}
			set
			{
				this.productAffinityGroupId = value;
			}
		}

		public int ProductAgentId
		{
			get
			{
				return this.productAgentId;
			}
			set
			{
				this.productAgentId = value;
			}
		}

		public int ProductId
		{
			get
			{
				return this.productId;
			}
			set
			{
				this.productId = value;
			}
		}

		public float ProductMaximumAmount
		{
			get
			{
				return this.productMaximumAmount;
			}
			set
			{
				this.productMaximumAmount = value;
			}
		}

		public ProductAffinityGroup()
		{
		}

		public ProductAffinityGroup(string connection, string modifiedUserID, int productAffinityGroupId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.ProductAffinityGroupId = productAffinityGroupId;
			if (this.ProductAffinityGroupId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ProductAffinityGroupId", DataAccessParameterType.Numeric, this.ProductAffinityGroupId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("PRODUCT_AFFINITY_GROUP_Delete");
			this.wipeout();
		}

		public static int GetRecordId(int productId, int affinityGroupId, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@ProductId", DataAccessParameterType.Numeric, productId.ToString());
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, affinityGroupId.ToString());
			data.ExecuteProcedure("PRODUCT_AFFINITY_GROUP_GetRecord");
			num = (!data.EOF ? int.Parse(data["ProductAffinityGroupId"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ProductAffinityGroupId", DataAccessParameterType.Numeric, this.ProductAffinityGroupId.ToString());
			data.ExecuteProcedure("PRODUCT_AFFINITY_GROUP_GetRecord");
			if (!data.EOF)
			{
				this.ProductAffinityGroupId = int.Parse(data["ProductAffinityGroupId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.AffinityGroupId = int.Parse(data["AffinityGroupId"]);
				this.ProductId = int.Parse(data["ProductId"]);
				this.ProductAgentId = int.Parse(data["ProductAgentId"]);
				this.ProductMaximumAmount = float.Parse(data["ProductMaximumAmount"]);
				this.EffectiveDate = data["EffectiveDate"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int productAffinityGroupId = this.ProductAffinityGroupId;
			data.AddParam("@ProductAffinityGroupId", DataAccessParameterType.Numeric, productAffinityGroupId.ToString());
			productAffinityGroupId = this.AffinityGroupId;
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, productAffinityGroupId.ToString());
			productAffinityGroupId = this.ProductId;
			data.AddParam("@ProductId", DataAccessParameterType.Numeric, productAffinityGroupId.ToString());
			productAffinityGroupId = this.ProductAgentId;
			data.AddParam("@ProductAgentId", DataAccessParameterType.Numeric, productAffinityGroupId.ToString());
			data.AddParam("@ProductMaximumAmount", DataAccessParameterType.Numeric, this.ProductMaximumAmount.ToString());
			data.AddParam("@EffectiveDate", DataAccessParameterType.DateTime, this.EffectiveDate);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("PRODUCT_AFFINITY_GROUP_InsertUpdate");
			if (!data.EOF)
			{
				this.ProductAffinityGroupId = int.Parse(data["ProductAffinityGroupId"]);
			}
			this.retrievedata();
		}
	}
}