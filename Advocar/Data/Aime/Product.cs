using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class Product : Transaction
	{
		private int productId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int productAdministratorId = 0;

		private int productFamilyId = 0;

		private int lookupId_ProductSeller = 0;

		private string code = "";

		private string description = "";

		private float costPercentOfSale = 0f;

		private float markupAmount = 0f;

		private float markupAmountOther1 = 0f;

		private bool isCommissionPercentOfSale = false;

		private bool isCommissionMarkup = false;

		public string Code
		{
			get
			{
				return this.code;
			}
			set
			{
				this.code = value;
			}
		}

		public float CostPercentOfSale
		{
			get
			{
				return this.costPercentOfSale;
			}
			set
			{
				this.costPercentOfSale = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

		public bool IsCommissionMarkup
		{
			get
			{
				return this.isCommissionMarkup;
			}
			set
			{
				this.isCommissionMarkup = value;
			}
		}

		public bool IsCommissionPercentOfSale
		{
			get
			{
				return this.isCommissionPercentOfSale;
			}
			set
			{
				this.isCommissionPercentOfSale = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public int LookupId_ProductSeller
		{
			get
			{
				return this.lookupId_ProductSeller;
			}
			set
			{
				this.lookupId_ProductSeller = value;
			}
		}

		public float MarkupAmount
		{
			get
			{
				return this.markupAmount;
			}
			set
			{
				this.markupAmount = value;
			}
		}

		public float MarkupAmountOther1
		{
			get
			{
				return this.markupAmountOther1;
			}
			set
			{
				this.markupAmountOther1 = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public int ProductAdministratorId
		{
			get
			{
				return this.productAdministratorId;
			}
			set
			{
				this.productAdministratorId = value;
			}
		}

		public int ProductFamilyId
		{
			get
			{
				return this.productFamilyId;
			}
			set
			{
				this.productFamilyId = value;
			}
		}

		public int ProductId
		{
			get
			{
				return this.productId;
			}
			set
			{
				this.productId = value;
			}
		}

		public Product()
		{
		}

		public Product(string connection, string modifiedUserID, int productId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.ProductId = productId;
			if (this.ProductId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ProductId", DataAccessParameterType.Numeric, this.ProductId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("PRODUCT_Delete");
			this.wipeout();
		}

		public static int GetRecordId(string code, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@Code", DataAccessParameterType.Text, code);
			data.ExecuteProcedure("PRODUCT_GetRecord");
			num = (!data.EOF ? int.Parse(data["ProductId"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ProductId", DataAccessParameterType.Numeric, this.ProductId.ToString());
			data.ExecuteProcedure("PRODUCT_GetRecord");
			if (!data.EOF)
			{
				this.ProductId = int.Parse(data["ProductId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.ProductAdministratorId = int.Parse(data["ProductAdministratorId"]);
				this.ProductFamilyId = int.Parse(data["ProductFamilyId"]);
				this.LookupId_ProductSeller = int.Parse(data["LookupId_ProductSeller"]);
				this.Code = data["Code"];
				this.Description = data["Description"];
				this.CostPercentOfSale = float.Parse(data["CostPercentOfSale"]);
				this.MarkupAmount = float.Parse(data["MarkupAmount"]);
				this.MarkupAmountOther1 = float.Parse(data["MarkupAmountOther1"]);
				this.IsCommissionPercentOfSale = bool.Parse(data["IsCommissionPercentOfSale"]);
				this.IsCommissionMarkup = bool.Parse(data["IsCommissionMarkup"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int productId = this.ProductId;
			data.AddParam("@ProductId", DataAccessParameterType.Numeric, productId.ToString());
			productId = this.ProductAdministratorId;
			data.AddParam("@ProductAdministratorId", DataAccessParameterType.Numeric, productId.ToString());
			productId = this.ProductFamilyId;
			data.AddParam("@ProductFamilyId", DataAccessParameterType.Numeric, productId.ToString());
			productId = this.LookupId_ProductSeller;
			data.AddParam("@LookupId_ProductSeller", DataAccessParameterType.Numeric, productId.ToString());
			data.AddParam("@Code", DataAccessParameterType.Text, this.Code);
			data.AddParam("@Description", DataAccessParameterType.Text, this.Description);
			float costPercentOfSale = this.CostPercentOfSale;
			data.AddParam("@CostPercentOfSale", DataAccessParameterType.Numeric, costPercentOfSale.ToString());
			costPercentOfSale = this.MarkupAmount;
			data.AddParam("@MarkupAmount", DataAccessParameterType.Numeric, costPercentOfSale.ToString());
			costPercentOfSale = this.MarkupAmountOther1;
			data.AddParam("@MarkupAmountOther1", DataAccessParameterType.Numeric, costPercentOfSale.ToString());
			bool isCommissionPercentOfSale = this.IsCommissionPercentOfSale;
			data.AddParam("@IsCommissionPercentOfSale", DataAccessParameterType.Bool, isCommissionPercentOfSale.ToString());
			isCommissionPercentOfSale = this.IsCommissionMarkup;
			data.AddParam("@IsCommissionMarkup", DataAccessParameterType.Bool, isCommissionPercentOfSale.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("PRODUCT_InsertUpdate");
			if (!data.EOF)
			{
				this.ProductId = int.Parse(data["ProductId"]);
			}
			this.retrievedata();
		}
	}
}