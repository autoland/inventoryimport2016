using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class AffinityGroupDealer : Transaction
	{
		private int affinityGroupDealerId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int affinityGroupId = 0;

		private int dealerId = 0;

		public int AffinityGroupDealerId
		{
			get
			{
				return this.affinityGroupDealerId;
			}
			set
			{
				this.affinityGroupDealerId = value;
			}
		}

		public int AffinityGroupId
		{
			get
			{
				return this.affinityGroupId;
			}
			set
			{
				this.affinityGroupId = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public int DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public AffinityGroupDealer()
		{
		}

		public AffinityGroupDealer(string connection, string modifiedUserID, int affinityGroupDealerId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.AffinityGroupDealerId = affinityGroupDealerId;
			if (this.AffinityGroupDealerId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@AffinityGroupDealerId", DataAccessParameterType.Numeric, this.AffinityGroupDealerId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("AFFINITY_GROUP_DEALER_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@AffinityGroupDealerId", DataAccessParameterType.Numeric, this.AffinityGroupDealerId.ToString());
			data.ExecuteProcedure("AFFINITY_GROUP_DEALER_GetRecord");
			if (!data.EOF)
			{
				this.AffinityGroupDealerId = int.Parse(data["AffinityGroupDealerId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.AffinityGroupId = int.Parse(data["AffinityGroupId"]);
				this.DealerId = int.Parse(data["DealerId"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int affinityGroupDealerId = this.AffinityGroupDealerId;
			data.AddParam("@AffinityGroupDealerId", DataAccessParameterType.Numeric, affinityGroupDealerId.ToString());
			affinityGroupDealerId = this.AffinityGroupId;
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, affinityGroupDealerId.ToString());
			affinityGroupDealerId = this.DealerId;
			data.AddParam("@DealerId", DataAccessParameterType.Numeric, affinityGroupDealerId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("AFFINITY_GROUP_DEALER_InsertUpdate");
			if (!data.EOF)
			{
				this.AffinityGroupDealerId = int.Parse(data["AffinityGroupDealerId"]);
			}
			this.retrievedata();
		}
	}
}