using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class ProductContract : Transaction
	{
		private int productContractId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int activityId = 0;

		private int memberId = 0;

		private string vehicleDescription = "";

		private int productCommissionRateId = 0;

		private float premiumRetail = 0f;

		private float premiumSoldAt = 0f;

		private float premiumCost = 0f;

		private float premiumProfit = 0f;

		private string soldAtDate = "";

		private string expirationDate = "";

		private int soldAtMiles = 0;

		private int expirationMiles = 0;

		private string vin = "";

		private string userId_Authorized = "";

		private int productReceiptId = 0;

		private bool isPaymentReceived = false;

		private string paymentReceivedDate = "";

		private float paymentReceivedAmount = 0f;

		private int productAdministratorId = 0;

		private bool isAdministratorPaid = false;

		private string administratorPaidDate = "";

		private float administratorPaidAmount = 0f;

		private int productPaymentId_Administrator = 0;

		private int affinityGroupId = 0;

		private int affinityGroupBranchId = 0;

		private bool isAffinityGroupPaid = false;

		private string affinityGroupPaidDate = "";

		private float affinityGroupPaidAmount = 0f;

		private float affinityGroupPaidAmountCalc = 0f;

		private float affinityGroupAmountWithheld = 0f;

		private bool isAffinityGroupCommissionFixed = false;

		private int productPaymentId_AffinityGroup = 0;

		private int productAgentId = 0;

		private bool isAgentPaid = false;

		private string agentPaidDate = "";

		private float agentPaidAmount = 0f;

		private float agentPaidAmountCalc = 0f;

		private int productPaymentId_Agent = 0;

		private bool isCancelled = false;

		private int productId = 0;

		private int lookupId_ProductContractStatus = 0;

		public int ActivityId
		{
			get
			{
				return this.activityId;
			}
			set
			{
				this.activityId = value;
			}
		}

		public float AdministratorPaidAmount
		{
			get
			{
				return this.administratorPaidAmount;
			}
			set
			{
				this.administratorPaidAmount = value;
			}
		}

		public string AdministratorPaidDate
		{
			get
			{
				return this.administratorPaidDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.administratorPaidDate = "";
				}
				else
				{
					this.administratorPaidDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public float AffinityGroupAmountWithheld
		{
			get
			{
				return this.affinityGroupAmountWithheld;
			}
			set
			{
				this.affinityGroupAmountWithheld = value;
			}
		}

		public int AffinityGroupBranchId
		{
			get
			{
				return this.affinityGroupBranchId;
			}
			set
			{
				this.affinityGroupBranchId = value;
			}
		}

		public int AffinityGroupId
		{
			get
			{
				return this.affinityGroupId;
			}
			set
			{
				this.affinityGroupId = value;
			}
		}

		public float AffinityGroupPaidAmount
		{
			get
			{
				return this.affinityGroupPaidAmount;
			}
			set
			{
				this.affinityGroupPaidAmount = value;
			}
		}

		public float AffinityGroupPaidAmountCalc
		{
			get
			{
				return this.affinityGroupPaidAmountCalc;
			}
		}

		public string AffinityGroupPaidDate
		{
			get
			{
				return this.affinityGroupPaidDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.affinityGroupPaidDate = "";
				}
				else
				{
					this.affinityGroupPaidDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public float AgentPaidAmount
		{
			get
			{
				return this.agentPaidAmount;
			}
			set
			{
				this.agentPaidAmount = value;
			}
		}

		public float AgentPaidAmountCalc
		{
			get
			{
				return this.agentPaidAmountCalc;
			}
		}

		public string AgentPaidDate
		{
			get
			{
				return this.agentPaidDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.agentPaidDate = "";
				}
				else
				{
					this.agentPaidDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string ExpirationDate
		{
			get
			{
				return this.expirationDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.expirationDate = "";
				}
				else
				{
					this.expirationDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public int ExpirationMiles
		{
			get
			{
				return this.expirationMiles;
			}
			set
			{
				this.expirationMiles = value;
			}
		}

		public bool IsAdministratorPaid
		{
			get
			{
				return this.isAdministratorPaid;
			}
			set
			{
				this.isAdministratorPaid = value;
			}
		}

		public bool IsAffinityGroupCommissionFixed
		{
			get
			{
				return this.isAffinityGroupCommissionFixed;
			}
			set
			{
				this.isAffinityGroupCommissionFixed = value;
			}
		}

		public bool IsAffinityGroupPaid
		{
			get
			{
				return this.isAffinityGroupPaid;
			}
			set
			{
				this.isAffinityGroupPaid = value;
			}
		}

		public bool IsAgentPaid
		{
			get
			{
				return this.isAgentPaid;
			}
			set
			{
				this.isAgentPaid = value;
			}
		}

		public bool IsCancelled
		{
			get
			{
				return this.isCancelled;
			}
			set
			{
				this.isCancelled = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public bool IsPaymentReceived
		{
			get
			{
				return this.isPaymentReceived;
			}
			set
			{
				this.isPaymentReceived = value;
			}
		}

		public int LookupId_ProductContractStatus
		{
			get
			{
				return this.lookupId_ProductContractStatus;
			}
			set
			{
				this.lookupId_ProductContractStatus = value;
			}
		}

		public int MemberId
		{
			get
			{
				return this.memberId;
			}
			set
			{
				this.memberId = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public float PaymentReceivedAmount
		{
			get
			{
				return this.paymentReceivedAmount;
			}
			set
			{
				this.paymentReceivedAmount = value;
			}
		}

		public string PaymentReceivedDate
		{
			get
			{
				return this.paymentReceivedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.paymentReceivedDate = "";
				}
				else
				{
					this.paymentReceivedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public float PremiumCost
		{
			get
			{
				return this.premiumCost;
			}
			set
			{
				this.premiumCost = value;
			}
		}

		public float PremiumProfit
		{
			get
			{
				return this.premiumProfit;
			}
			set
			{
				this.premiumProfit = value;
			}
		}

		public float PremiumRetail
		{
			get
			{
				return this.premiumRetail;
			}
			set
			{
				this.premiumRetail = value;
			}
		}

		public float PremiumSoldAt
		{
			get
			{
				return this.premiumSoldAt;
			}
			set
			{
				this.premiumSoldAt = value;
			}
		}

		public int ProductAdministratorId
		{
			get
			{
				return this.productAdministratorId;
			}
			set
			{
				this.productAdministratorId = value;
			}
		}

		public int ProductAgentId
		{
			get
			{
				return this.productAgentId;
			}
			set
			{
				this.productAgentId = value;
			}
		}

		public int ProductCommissionRateId
		{
			get
			{
				return this.productCommissionRateId;
			}
			set
			{
				this.productCommissionRateId = value;
			}
		}

		public int ProductContractId
		{
			get
			{
				return this.productContractId;
			}
			set
			{
				this.productContractId = value;
			}
		}

		public int ProductId
		{
			get
			{
				return this.productId;
			}
			set
			{
				this.productId = value;
			}
		}

		public int ProductPaymentId_Administrator
		{
			get
			{
				return this.productPaymentId_Administrator;
			}
			set
			{
				this.productPaymentId_Administrator = value;
			}
		}

		public int ProductPaymentId_AffinityGroup
		{
			get
			{
				return this.productPaymentId_AffinityGroup;
			}
			set
			{
				this.productPaymentId_AffinityGroup = value;
			}
		}

		public int ProductPaymentId_Agent
		{
			get
			{
				return this.productPaymentId_Agent;
			}
			set
			{
				this.productPaymentId_Agent = value;
			}
		}

		public int ProductReceiptId
		{
			get
			{
				return this.productReceiptId;
			}
			set
			{
				this.productReceiptId = value;
			}
		}

		public string SoldAtDate
		{
			get
			{
				return this.soldAtDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.soldAtDate = "";
				}
				else
				{
					this.soldAtDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public int SoldAtMiles
		{
			get
			{
				return this.soldAtMiles;
			}
			set
			{
				this.soldAtMiles = value;
			}
		}

		public string UserId_Authorized
		{
			get
			{
				return this.userId_Authorized;
			}
			set
			{
				this.userId_Authorized = value;
			}
		}

		public string VehicleDescription
		{
			get
			{
				return this.vehicleDescription;
			}
			set
			{
				this.vehicleDescription = value;
			}
		}

		public string Vin
		{
			get
			{
				return this.vin;
			}
			set
			{
				this.vin = value;
			}
		}

		public ProductContract()
		{
		}

		public ProductContract(string connection, string modifiedUserID, int productContractId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.ProductContractId = productContractId;
			if (this.ProductContractId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ProductContractId", DataAccessParameterType.Numeric, this.ProductContractId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("PRODUCT_CONTRACT_Delete");
			this.wipeout();
		}

		public static int GetRecordId(int activityId, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@ActivityId", DataAccessParameterType.Numeric, activityId.ToString());
			data.ExecuteProcedure("PRODUCT_CONTRACT_GetRecord");
			num = (!data.EOF ? int.Parse(data["ProductContractId"]) : 0);
			return num;
		}

		public static int GetRecordId(string vin, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@Vin", DataAccessParameterType.Text, vin.ToString());
			data.ExecuteProcedure("PRODUCT_CONTRACT_GetRecord");
			num = (!data.EOF ? int.Parse(data["ProductContractId"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			int productContractId = this.ProductContractId;
			data.AddParam("@ProductContractId", DataAccessParameterType.Numeric, productContractId.ToString());
			data.ExecuteProcedure("PRODUCT_CONTRACT_GetRecord");
			if (!data.EOF)
			{
				this.ProductContractId = int.Parse(data["ProductContractId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.ActivityId = int.Parse(data["ActivityId"]);
				this.MemberId = int.Parse(data["MemberId"]);
				this.VehicleDescription = data["VehicleDescription"];
				this.ProductCommissionRateId = int.Parse(data["ProductCommissionRateId"]);
				this.PremiumRetail = float.Parse(data["PremiumRetail"]);
				this.PremiumSoldAt = float.Parse(data["PremiumSoldAt"]);
				this.PremiumCost = float.Parse(data["PremiumCost"]);
				this.PremiumProfit = float.Parse(data["PremiumProfit"]);
				this.SoldAtDate = data["SoldAtDate"];
				this.ExpirationDate = data["ExpirationDate"];
				this.SoldAtMiles = int.Parse(data["SoldAtMiles"]);
				this.ExpirationMiles = int.Parse(data["ExpirationMiles"]);
				this.Vin = data["Vin"];
				this.UserId_Authorized = data["UserId_Authorized"];
				this.ProductReceiptId = int.Parse(data["ProductReceiptId"]);
				this.IsPaymentReceived = bool.Parse(data["IsPaymentReceived"]);
				this.PaymentReceivedDate = data["PaymentReceivedDate"];
				this.PaymentReceivedAmount = float.Parse(data["PaymentReceivedAmount"]);
				this.ProductAdministratorId = int.Parse(data["ProductAdministratorId"]);
				this.IsAdministratorPaid = bool.Parse(data["IsAdministratorPaid"]);
				this.AdministratorPaidDate = data["AdministratorPaidDate"];
				this.AdministratorPaidAmount = float.Parse(data["AdministratorPaidAmount"]);
				this.ProductPaymentId_Administrator = int.Parse(data["ProductPaymentId_Administrator"]);
				this.AffinityGroupId = int.Parse(data["AffinityGroupId"]);
				this.AffinityGroupBranchId = int.Parse(data["AffinityGroupBranchId"]);
				this.IsAffinityGroupPaid = bool.Parse(data["IsAffinityGroupPaid"]);
				this.AffinityGroupPaidDate = data["AffinityGroupPaidDate"];
				this.AffinityGroupPaidAmount = float.Parse(data["AffinityGroupPaidAmount"]);
				this.AffinityGroupAmountWithheld = float.Parse(data["AffinityGroupAmountWithheld"]);
				this.IsAffinityGroupCommissionFixed = bool.Parse(data["IsAffinityGroupCommissionFixed"]);
				this.ProductPaymentId_AffinityGroup = int.Parse(data["ProductPaymentId_AffinityGroup"]);
				this.ProductAgentId = int.Parse(data["ProductAgentId"]);
				this.IsAgentPaid = bool.Parse(data["IsAgentPaid"]);
				this.AgentPaidDate = data["AgentPaidDate"];
				this.AgentPaidAmount = float.Parse(data["AgentPaidAmount"]);
				this.ProductPaymentId_Agent = int.Parse(data["ProductPaymentId_Agent"]);
				this.IsCancelled = bool.Parse(data["IsCancelled"]);
				this.ProductId = int.Parse(data["ProductId"]);
				this.LookupId_ProductContractStatus = int.Parse(data["LookupId_ProductContractStatus"]);
				DataAccess dataAmountCalc = new DataAccess(this.connection);
				string[] str = new string[] { "SELECT TOP 1 AffinityGroupAmount, AffinityGroupPercent, AgentAmount, AgentPercent FROM dbo.GetProductCommissionRate(", null, null, null, null, null, null };
				productContractId = this.AffinityGroupId;
				str[1] = productContractId.ToString();
				str[2] = ",";
				productContractId = this.AffinityGroupBranchId;
				str[3] = productContractId.ToString();
				str[4] = ",";
				productContractId = this.ProductId;
				str[5] = productContractId.ToString();
				str[6] = ", -1, GETDATE())";
				dataAmountCalc.ExecuteStatement(string.Concat(str));
				if (!dataAmountCalc.EOF)
				{
					this.affinityGroupPaidAmountCalc = float.Parse(dataAmountCalc["AffinityGroupAmount"]) + this.PremiumSoldAt * float.Parse(dataAmountCalc["AffinityGroupPercent"]);
					this.agentPaidAmountCalc = float.Parse(dataAmountCalc["AgentAmount"]) + this.PremiumSoldAt * float.Parse(dataAmountCalc["AgentPercent"]);
				}
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int productContractId = this.ProductContractId;
			data.AddParam("@ProductContractId", DataAccessParameterType.Numeric, productContractId.ToString());
			productContractId = this.ActivityId;
			data.AddParam("@ActivityId", DataAccessParameterType.Numeric, productContractId.ToString());
			productContractId = this.MemberId;
			data.AddParam("@MemberId", DataAccessParameterType.Numeric, productContractId.ToString());
			data.AddParam("@VehicleDescription", DataAccessParameterType.Text, this.VehicleDescription);
			productContractId = this.ProductCommissionRateId;
			data.AddParam("@ProductCommissionRateId", DataAccessParameterType.Numeric, productContractId.ToString());
			float premiumRetail = this.PremiumRetail;
			data.AddParam("@PremiumRetail", DataAccessParameterType.Numeric, premiumRetail.ToString());
			premiumRetail = this.PremiumSoldAt;
			data.AddParam("@PremiumSoldAt", DataAccessParameterType.Numeric, premiumRetail.ToString());
			premiumRetail = this.PremiumCost;
			data.AddParam("@PremiumCost", DataAccessParameterType.Numeric, premiumRetail.ToString());
			premiumRetail = this.PremiumProfit;
			data.AddParam("@PremiumProfit", DataAccessParameterType.Numeric, premiumRetail.ToString());
			data.AddParam("@SoldAtDate", DataAccessParameterType.DateTime, this.SoldAtDate);
			data.AddParam("@ExpirationDate", DataAccessParameterType.DateTime, this.ExpirationDate);
			productContractId = this.SoldAtMiles;
			data.AddParam("@SoldAtMiles", DataAccessParameterType.Numeric, productContractId.ToString());
			productContractId = this.ExpirationMiles;
			data.AddParam("@ExpirationMiles", DataAccessParameterType.Numeric, productContractId.ToString());
			data.AddParam("@Vin", DataAccessParameterType.Text, this.Vin);
			data.AddParam("@UserId_Authorized", DataAccessParameterType.Text, this.UserId_Authorized);
			productContractId = this.ProductReceiptId;
			data.AddParam("@ProductReceiptId", DataAccessParameterType.Numeric, productContractId.ToString());
			bool isPaymentReceived = this.IsPaymentReceived;
			data.AddParam("@IsPaymentReceived", DataAccessParameterType.Bool, isPaymentReceived.ToString());
			data.AddParam("@PaymentReceivedDate", DataAccessParameterType.DateTime, this.PaymentReceivedDate);
			premiumRetail = this.PaymentReceivedAmount;
			data.AddParam("@PaymentReceivedAmount", DataAccessParameterType.Numeric, premiumRetail.ToString());
			productContractId = this.ProductAdministratorId;
			data.AddParam("@ProductAdministratorId", DataAccessParameterType.Numeric, productContractId.ToString());
			isPaymentReceived = this.IsAdministratorPaid;
			data.AddParam("@IsAdministratorPaid", DataAccessParameterType.Bool, isPaymentReceived.ToString());
			data.AddParam("@AdministratorPaidDate", DataAccessParameterType.DateTime, this.AdministratorPaidDate);
			premiumRetail = this.AdministratorPaidAmount;
			data.AddParam("@AdministratorPaidAmount", DataAccessParameterType.Numeric, premiumRetail.ToString());
			productContractId = this.ProductPaymentId_Administrator;
			data.AddParam("@ProductPaymentId_Administrator", DataAccessParameterType.Numeric, productContractId.ToString());
			productContractId = this.AffinityGroupId;
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, productContractId.ToString());
			productContractId = this.AffinityGroupBranchId;
			data.AddParam("@AffinityGroupBranchId", DataAccessParameterType.Numeric, productContractId.ToString());
			isPaymentReceived = this.IsAffinityGroupPaid;
			data.AddParam("@IsAffinityGroupPaid", DataAccessParameterType.Bool, isPaymentReceived.ToString());
			data.AddParam("@AffinityGroupPaidDate", DataAccessParameterType.DateTime, this.AffinityGroupPaidDate);
			premiumRetail = this.AffinityGroupPaidAmount;
			data.AddParam("@AffinityGroupPaidAmount", DataAccessParameterType.Numeric, premiumRetail.ToString());
			premiumRetail = this.AffinityGroupAmountWithheld;
			data.AddParam("@AffinityGroupAmountWithheld", DataAccessParameterType.Numeric, premiumRetail.ToString());
			isPaymentReceived = this.IsAffinityGroupCommissionFixed;
			data.AddParam("@IsAffinityGroupCommissionFixed", DataAccessParameterType.Bool, isPaymentReceived.ToString());
			productContractId = this.ProductPaymentId_AffinityGroup;
			data.AddParam("@ProductPaymentId_AffinityGroup", DataAccessParameterType.Numeric, productContractId.ToString());
			productContractId = this.ProductAgentId;
			data.AddParam("@ProductAgentId", DataAccessParameterType.Numeric, productContractId.ToString());
			isPaymentReceived = this.IsAgentPaid;
			data.AddParam("@IsAgentPaid", DataAccessParameterType.Bool, isPaymentReceived.ToString());
			data.AddParam("@AgentPaidDate", DataAccessParameterType.DateTime, this.AgentPaidDate);
			premiumRetail = this.AgentPaidAmount;
			data.AddParam("@AgentPaidAmount", DataAccessParameterType.Numeric, premiumRetail.ToString());
			productContractId = this.ProductPaymentId_Agent;
			data.AddParam("@ProductPaymentId_Agent", DataAccessParameterType.Numeric, productContractId.ToString());
			isPaymentReceived = this.IsCancelled;
			data.AddParam("@IsCancelled", DataAccessParameterType.Bool, isPaymentReceived.ToString());
			productContractId = this.ProductId;
			data.AddParam("@ProductId", DataAccessParameterType.Numeric, productContractId.ToString());
			productContractId = this.LookupId_ProductContractStatus;
			data.AddParam("@LookupId_ProductContractStatus", DataAccessParameterType.Numeric, productContractId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("PRODUCT_CONTRACT_InsertUpdate");
			if (!data.EOF)
			{
				this.ProductContractId = int.Parse(data["ProductContractId"]);
			}
			this.retrievedata();
		}
	}
}