using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class Territory : Transaction
	{
		private int territoryId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private string territoryName = "";

		private string zipCodeList = "";

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public int TerritoryId
		{
			get
			{
				return this.territoryId;
			}
			set
			{
				this.territoryId = value;
			}
		}

		public string TerritoryName
		{
			get
			{
				return this.territoryName;
			}
			set
			{
				this.territoryName = value;
			}
		}

		public string ZipCodeList
		{
			get
			{
				return this.zipCodeList;
			}
			set
			{
				this.zipCodeList = value;
			}
		}

		public Territory()
		{
		}

		public Territory(string connection, string modifiedUserID, int territoryId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "TERRITORY";
			this.TerritoryId = territoryId;
			if (this.TerritoryId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@TerritoryId", DataAccessParameterType.Numeric, this.TerritoryId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("TERRITORY_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@TerritoryId", DataAccessParameterType.Numeric, this.TerritoryId.ToString());
			data.ExecuteProcedure("TERRITORY_GetRecord");
			if (!data.EOF)
			{
				this.TerritoryId = int.Parse(data["TerritoryId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.TerritoryName = data["TerritoryName"];
				this.ZipCodeList = data["ZipCodeList"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@TerritoryId", DataAccessParameterType.Numeric, this.TerritoryId.ToString());
			data.AddParam("@TerritoryName", DataAccessParameterType.Text, this.TerritoryName);
			data.AddParam("@ZipCodeList", DataAccessParameterType.Text, this.ZipCodeList);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("TERRITORY_InsertUpdate");
			if (!data.EOF)
			{
				this.TerritoryId = int.Parse(data["TerritoryId"]);
			}
			this.retrievedata();
		}
	}
}