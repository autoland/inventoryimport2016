using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class SalesConsultantDaily : Transaction
	{
		private int salesConsultantDailyId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private string salesDate = "";

		private int companyEmployeeId = 0;

		private int companyOfficeId = 0;

		private int lookupId_DailySalesException = 0;

		private int pendingSalesNew = 0;

		private int pendingSalesUsed = 0;

		private int pendingSalesUsedOther = 0;

		private int salesNew = 0;

		private int salesUsed = 0;

		private int salesUsedOther = 0;

		private int salesGap = 0;

		private int salesLojack = 0;

		private int salesMbp = 0;

		private int salesSealant = 0;

		private int salesTradeIn = 0;

		private int salesDirectLending = 0;

		private int salesDiamonFusion = 0;

		private int salesRoadloans = 0;

		private int salesPdr = 0;

		private int noOfAppointments = 0;

		private int noOfContacts = 0;

		private int noOfOpenOrders = 0;

		private int monthSalesNew = 0;

		private int monthSalesUsed = 0;

		private int monthSalesUsedOther = 0;

		private int monthSalesGap = 0;

		private int monthSalesLojack = 0;

		private int monthSalesMbp = 0;

		private int monthSalesSealant = 0;

		private int monthSalesTradeIn = 0;

		private int monthSalesPdr = 0;

		private int monthSalesDirectLending = 0;

		private int monthSalesRoadloans = 0;

		private int monthSalesDiamonFusion = 0;

		private int monthNoOfAppointments = 0;

		private int monthNoOfContacts = 0;

		private int monthNoOfOpenOrders = 0;

		private bool isLastDayOfMonthFinalized = false;

		private bool isFirstDayOfMonth = false;

		public int CompanyEmployeeId
		{
			get
			{
				return this.companyEmployeeId;
			}
			set
			{
				this.companyEmployeeId = value;
			}
		}

		public int CompanyOfficeId
		{
			get
			{
				return this.companyOfficeId;
			}
			set
			{
				this.companyOfficeId = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public bool IsFirstDayOfMonth
		{
			get
			{
				return this.isFirstDayOfMonth;
			}
			set
			{
				this.isFirstDayOfMonth = value;
			}
		}

		public bool IsLastDayOfMonthFinalized
		{
			get
			{
				return this.isLastDayOfMonthFinalized;
			}
			set
			{
				this.isLastDayOfMonthFinalized = value;
			}
		}

		public int LookupId_DailySalesException
		{
			get
			{
				return this.lookupId_DailySalesException;
			}
			set
			{
				this.lookupId_DailySalesException = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public int MonthNoOfAppointments
		{
			get
			{
				return this.monthNoOfAppointments;
			}
			set
			{
				this.monthNoOfAppointments = value;
			}
		}

		public int MonthNoOfContacts
		{
			get
			{
				return this.monthNoOfContacts;
			}
			set
			{
				this.monthNoOfContacts = value;
			}
		}

		public int MonthNoOfOpenOrders
		{
			get
			{
				return this.monthNoOfOpenOrders;
			}
			set
			{
				this.monthNoOfOpenOrders = value;
			}
		}

		public int MonthSalesDiamonFusion
		{
			get
			{
				return this.monthSalesDiamonFusion;
			}
			set
			{
				this.monthSalesDiamonFusion = value;
			}
		}

		public int MonthSalesDirectLending
		{
			get
			{
				return this.monthSalesDirectLending;
			}
			set
			{
				this.monthSalesDirectLending = value;
			}
		}

		public int MonthSalesGap
		{
			get
			{
				return this.monthSalesGap;
			}
			set
			{
				this.monthSalesGap = value;
			}
		}

		public int MonthSalesLojack
		{
			get
			{
				return this.monthSalesLojack;
			}
			set
			{
				this.monthSalesLojack = value;
			}
		}

		public int MonthSalesMbp
		{
			get
			{
				return this.monthSalesMbp;
			}
			set
			{
				this.monthSalesMbp = value;
			}
		}

		public int MonthSalesNew
		{
			get
			{
				return this.monthSalesNew;
			}
			set
			{
				this.monthSalesNew = value;
			}
		}

		public int MonthSalesPdr
		{
			get
			{
				return this.monthSalesPdr;
			}
			set
			{
				this.monthSalesPdr = value;
			}
		}

		public int MonthSalesRoadloans
		{
			get
			{
				return this.monthSalesRoadloans;
			}
			set
			{
				this.monthSalesRoadloans = value;
			}
		}

		public int MonthSalesSealant
		{
			get
			{
				return this.monthSalesSealant;
			}
			set
			{
				this.monthSalesSealant = value;
			}
		}

		public int MonthSalesTradeIn
		{
			get
			{
				return this.monthSalesTradeIn;
			}
			set
			{
				this.monthSalesTradeIn = value;
			}
		}

		public int MonthSalesUsed
		{
			get
			{
				return this.monthSalesUsed;
			}
			set
			{
				this.monthSalesUsed = value;
			}
		}

		public int MonthSalesUsedOther
		{
			get
			{
				return this.monthSalesUsedOther;
			}
			set
			{
				this.monthSalesUsedOther = value;
			}
		}

		public int NoOfAppointments
		{
			get
			{
				return this.noOfAppointments;
			}
			set
			{
				this.noOfAppointments = value;
			}
		}

		public int NoOfContacts
		{
			get
			{
				return this.noOfContacts;
			}
			set
			{
				this.noOfContacts = value;
			}
		}

		public int NoOfOpenOrders
		{
			get
			{
				return this.noOfOpenOrders;
			}
			set
			{
				this.noOfOpenOrders = value;
			}
		}

		public int PendingSalesNew
		{
			get
			{
				return this.pendingSalesNew;
			}
			set
			{
				this.pendingSalesNew = value;
			}
		}

		public int PendingSalesUsed
		{
			get
			{
				return this.pendingSalesUsed;
			}
			set
			{
				this.pendingSalesUsed = value;
			}
		}

		public int PendingSalesUsedOther
		{
			get
			{
				return this.pendingSalesUsedOther;
			}
			set
			{
				this.pendingSalesUsedOther = value;
			}
		}

		public int SalesConsultantDailyId
		{
			get
			{
				return this.salesConsultantDailyId;
			}
			set
			{
				this.salesConsultantDailyId = value;
			}
		}

		public string SalesDate
		{
			get
			{
				return this.salesDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.salesDate = "";
				}
				else
				{
					this.salesDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public int SalesDiamonFusion
		{
			get
			{
				return this.salesDiamonFusion;
			}
			set
			{
				this.salesDiamonFusion = value;
			}
		}

		public int SalesDirectLending
		{
			get
			{
				return this.salesDirectLending;
			}
			set
			{
				this.salesDirectLending = value;
			}
		}

		public int SalesGap
		{
			get
			{
				return this.salesGap;
			}
			set
			{
				this.salesGap = value;
			}
		}

		public int SalesLojack
		{
			get
			{
				return this.salesLojack;
			}
			set
			{
				this.salesLojack = value;
			}
		}

		public int SalesMbp
		{
			get
			{
				return this.salesMbp;
			}
			set
			{
				this.salesMbp = value;
			}
		}

		public int SalesNew
		{
			get
			{
				return this.salesNew;
			}
			set
			{
				this.salesNew = value;
			}
		}

		public int SalesPdr
		{
			get
			{
				return this.salesPdr;
			}
			set
			{
				this.salesPdr = value;
			}
		}

		public int SalesRoadloans
		{
			get
			{
				return this.salesRoadloans;
			}
			set
			{
				this.salesRoadloans = value;
			}
		}

		public int SalesSealant
		{
			get
			{
				return this.salesSealant;
			}
			set
			{
				this.salesSealant = value;
			}
		}

		public int SalesTradeIn
		{
			get
			{
				return this.salesTradeIn;
			}
			set
			{
				this.salesTradeIn = value;
			}
		}

		public int SalesUsed
		{
			get
			{
				return this.salesUsed;
			}
			set
			{
				this.salesUsed = value;
			}
		}

		public int SalesUsedOther
		{
			get
			{
				return this.salesUsedOther;
			}
			set
			{
				this.salesUsedOther = value;
			}
		}

		public SalesConsultantDaily()
		{
		}

		public SalesConsultantDaily(string connection, string modifiedUserID, int salesConsultantDailyId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.SalesConsultantDailyId = salesConsultantDailyId;
			this.databaseObjectName = "SALES_CONSULTANT_DAILY";
			if (this.SalesConsultantDailyId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@SalesConsultantDailyId", DataAccessParameterType.Numeric, this.SalesConsultantDailyId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("SALES_CONSULTANT_DAILY_Delete");
			this.wipeout();
		}

		public void LoadMonthlyTotals(DataAccess data)
		{
			this.SalesDate = data["SalesDate"];
			this.MonthSalesNew = int.Parse(data["MonthSalesNew"]);
			this.MonthSalesUsed = int.Parse(data["MonthSalesUsed"]);
			this.MonthSalesUsedOther = int.Parse(data["MonthSalesUsedOther"]);
			this.MonthSalesGap = int.Parse(data["MonthSalesGap"]);
			this.MonthSalesLojack = int.Parse(data["MonthSalesLojack"]);
			this.MonthSalesMbp = int.Parse(data["MonthSalesMbp"]);
			this.MonthSalesSealant = int.Parse(data["MonthSalesSealant"]);
			this.MonthSalesTradeIn = int.Parse(data["MonthSalesTradeIn"]);
			this.MonthSalesDirectLending = int.Parse(data["MonthSalesDirectLending"]);
			this.MonthSalesRoadloans = int.Parse(data["MonthSalesRoadloans"]);
			this.MonthSalesPdr = int.Parse(data["MonthSalesPdr"]);
			this.MonthSalesDiamonFusion = int.Parse(data["MonthSalesDiamonFusion"]);
			this.MonthNoOfAppointments = int.Parse(data["MonthNoOfAppointments"]);
			this.MonthNoOfContacts = int.Parse(data["MonthNoOfContacts"]);
			this.MonthNoOfOpenOrders = int.Parse(data["MonthNoOfOpenOrders"]);
			this.IsLastDayOfMonthFinalized = bool.Parse(data["IsLastDayOfMonthFinalized"]);
			this.IsFirstDayOfMonth = bool.Parse(data["IsFirstDayOfMonth"]);
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@SalesConsultantDailyId", DataAccessParameterType.Numeric, this.SalesConsultantDailyId.ToString());
			data.ExecuteProcedure("SALES_CONSULTANT_DAILY_GetRecord");
			if (!data.EOF)
			{
				this.SalesConsultantDailyId = int.Parse(data["SalesConsultantDailyId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.SalesDate = data["SalesDate"];
				this.CompanyEmployeeId = int.Parse(data["CompanyEmployeeId"]);
				this.CompanyOfficeId = int.Parse(data["CompanyOfficeId"]);
				this.LookupId_DailySalesException = int.Parse(data["LookupId_DailySalesException"]);
				this.PendingSalesNew = int.Parse(data["PendingSalesNew"]);
				this.PendingSalesUsed = int.Parse(data["PendingSalesUsed"]);
				this.PendingSalesUsedOther = int.Parse(data["PendingSalesUsedOther"]);
				this.SalesNew = int.Parse(data["SalesNew"]);
				this.SalesUsed = int.Parse(data["SalesUsed"]);
				this.SalesUsedOther = int.Parse(data["SalesUsedOther"]);
				this.SalesGap = int.Parse(data["SalesGap"]);
				this.SalesLojack = int.Parse(data["SalesLojack"]);
				this.SalesMbp = int.Parse(data["SalesMbp"]);
				this.SalesSealant = int.Parse(data["SalesSealant"]);
				this.SalesTradeIn = int.Parse(data["SalesTradeIn"]);
				this.SalesDirectLending = int.Parse(data["SalesDirectLending"]);
				this.SalesDiamonFusion = int.Parse(data["SalesDiamonFusion"]);
				this.SalesRoadloans = int.Parse(data["SalesRoadloans"]);
				this.SalesPdr = int.Parse(data["SalesPdr"]);
				this.NoOfAppointments = int.Parse(data["NoOfAppointments"]);
				this.NoOfContacts = int.Parse(data["NoOfContacts"]);
				this.noOfOpenOrders = int.Parse(data["NoOfOpenOrders"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int salesConsultantDailyId = this.SalesConsultantDailyId;
			data.AddParam("@SalesConsultantDailyId", DataAccessParameterType.Numeric, salesConsultantDailyId.ToString());
			data.AddParam("@SalesDate", DataAccessParameterType.DateTime, this.SalesDate);
			salesConsultantDailyId = this.CompanyEmployeeId;
			data.AddParam("@CompanyEmployeeId", DataAccessParameterType.Numeric, salesConsultantDailyId.ToString());
			salesConsultantDailyId = this.CompanyOfficeId;
			data.AddParam("@CompanyOfficeId", DataAccessParameterType.Numeric, salesConsultantDailyId.ToString());
			salesConsultantDailyId = this.LookupId_DailySalesException;
			data.AddParam("@LookupId_DailySalesException", DataAccessParameterType.Numeric, salesConsultantDailyId.ToString());
			salesConsultantDailyId = this.PendingSalesNew;
			data.AddParam("@PendingSalesNew", DataAccessParameterType.Numeric, salesConsultantDailyId.ToString());
			salesConsultantDailyId = this.PendingSalesUsed;
			data.AddParam("@PendingSalesUsed", DataAccessParameterType.Numeric, salesConsultantDailyId.ToString());
			salesConsultantDailyId = this.PendingSalesUsedOther;
			data.AddParam("@PendingSalesUsedOther", DataAccessParameterType.Numeric, salesConsultantDailyId.ToString());
			salesConsultantDailyId = this.SalesNew;
			data.AddParam("@SalesNew", DataAccessParameterType.Numeric, salesConsultantDailyId.ToString());
			salesConsultantDailyId = this.SalesUsed;
			data.AddParam("@SalesUsed", DataAccessParameterType.Numeric, salesConsultantDailyId.ToString());
			salesConsultantDailyId = this.SalesUsedOther;
			data.AddParam("@SalesUsedOther", DataAccessParameterType.Numeric, salesConsultantDailyId.ToString());
			salesConsultantDailyId = this.SalesGap;
			data.AddParam("@SalesGap", DataAccessParameterType.Numeric, salesConsultantDailyId.ToString());
			salesConsultantDailyId = this.SalesLojack;
			data.AddParam("@SalesLojack", DataAccessParameterType.Numeric, salesConsultantDailyId.ToString());
			salesConsultantDailyId = this.SalesMbp;
			data.AddParam("@SalesMbp", DataAccessParameterType.Numeric, salesConsultantDailyId.ToString());
			salesConsultantDailyId = this.SalesSealant;
			data.AddParam("@SalesSealant", DataAccessParameterType.Numeric, salesConsultantDailyId.ToString());
			salesConsultantDailyId = this.SalesTradeIn;
			data.AddParam("@SalesTradeIn", DataAccessParameterType.Numeric, salesConsultantDailyId.ToString());
			salesConsultantDailyId = this.SalesDirectLending;
			data.AddParam("@SalesDirectLending", DataAccessParameterType.Numeric, salesConsultantDailyId.ToString());
			salesConsultantDailyId = this.SalesDiamonFusion;
			data.AddParam("@SalesDiamonFusion", DataAccessParameterType.Numeric, salesConsultantDailyId.ToString());
			salesConsultantDailyId = this.SalesRoadloans;
			data.AddParam("@SalesRoadloans", DataAccessParameterType.Numeric, salesConsultantDailyId.ToString());
			salesConsultantDailyId = this.SalesPdr;
			data.AddParam("@SalesPdr", DataAccessParameterType.Numeric, salesConsultantDailyId.ToString());
			salesConsultantDailyId = this.NoOfAppointments;
			data.AddParam("@NoOfAppointments", DataAccessParameterType.Numeric, salesConsultantDailyId.ToString());
			salesConsultantDailyId = this.NoOfContacts;
			data.AddParam("@NoOfContacts", DataAccessParameterType.Numeric, salesConsultantDailyId.ToString());
			salesConsultantDailyId = this.NoOfOpenOrders;
			data.AddParam("@NoOfOpenOrders", DataAccessParameterType.Numeric, salesConsultantDailyId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("SALES_CONSULTANT_DAILY_InsertUpdate");
			if (!data.EOF)
			{
				this.SalesConsultantDailyId = int.Parse(data["SalesConsultantDailyId"]);
			}
			this.retrievedata();
		}
	}
}