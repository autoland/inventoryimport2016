using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class ProductPayment : Transaction
	{
		private int productPaymentId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private string reference = "";

		private string payeeName = "";

		private int lookupId_ProductPaymentType = 0;

		private int productFamilyId = 0;

		private int affinityGroupId = 0;

		private int productAgentId = 0;

		private int productAdministratorId = 0;

		private string paymentDate = "";

		private float amountDue = 0f;

		private float amountAdjusted = 0f;

		private float amountPaid = 0f;

		private string notes = "";

		private string productContractList = "";

		private string productCancellationList = "";

		private bool isCompleted = false;

		public int AffinityGroupId
		{
			get
			{
				return this.affinityGroupId;
			}
			set
			{
				this.affinityGroupId = value;
			}
		}

		public float AmountAdjusted
		{
			get
			{
				return this.amountAdjusted;
			}
			set
			{
				this.amountAdjusted = value;
			}
		}

		public float AmountDue
		{
			get
			{
				return this.amountDue;
			}
			set
			{
				this.amountDue = value;
			}
		}

		public float AmountPaid
		{
			get
			{
				return this.amountPaid;
			}
			set
			{
				this.amountPaid = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public bool IsCompleted
		{
			get
			{
				return this.isCompleted;
			}
			set
			{
				this.isCompleted = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public int LookupId_ProductPaymentType
		{
			get
			{
				return this.lookupId_ProductPaymentType;
			}
			set
			{
				this.lookupId_ProductPaymentType = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public string Notes
		{
			get
			{
				return this.notes;
			}
			set
			{
				this.notes = value;
			}
		}

		public string PayeeName
		{
			get
			{
				return this.payeeName;
			}
			set
			{
				this.payeeName = value;
			}
		}

		public string PaymentDate
		{
			get
			{
				return this.paymentDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.paymentDate = "";
				}
				else
				{
					this.paymentDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public int ProductAdministratorId
		{
			get
			{
				return this.productAdministratorId;
			}
			set
			{
				this.productAdministratorId = value;
			}
		}

		public int ProductAgentId
		{
			get
			{
				return this.productAgentId;
			}
			set
			{
				this.productAgentId = value;
			}
		}

		public string ProductCancellationList
		{
			get
			{
				return this.productCancellationList;
			}
			set
			{
				this.productCancellationList = value;
			}
		}

		public string ProductContractList
		{
			get
			{
				return this.productContractList;
			}
			set
			{
				this.productContractList = value;
			}
		}

		public int ProductFamilyId
		{
			get
			{
				return this.productFamilyId;
			}
			set
			{
				this.productFamilyId = value;
			}
		}

		public int ProductPaymentId
		{
			get
			{
				return this.productPaymentId;
			}
			set
			{
				this.productPaymentId = value;
			}
		}

		public string Reference
		{
			get
			{
				return this.reference;
			}
			set
			{
				this.reference = value;
			}
		}

		public ProductPayment()
		{
		}

		public ProductPayment(string connection, string modifiedUserID, int productPaymentId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.ProductPaymentId = productPaymentId;
			if (this.ProductPaymentId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ProductPaymentId", DataAccessParameterType.Numeric, this.ProductPaymentId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("PRODUCT_PAYMENT_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ProductPaymentId", DataAccessParameterType.Numeric, this.ProductPaymentId.ToString());
			data.ExecuteProcedure("PRODUCT_PAYMENT_GetRecord");
			if (!data.EOF)
			{
				this.ProductPaymentId = int.Parse(data["ProductPaymentId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.Reference = data["Reference"];
				this.PayeeName = data["PayeeName"];
				this.LookupId_ProductPaymentType = int.Parse(data["LookupId_ProductPaymentType"]);
				this.ProductFamilyId = int.Parse(data["ProductFamilyId"]);
				this.AffinityGroupId = int.Parse(data["AffinityGroupId"]);
				this.ProductAgentId = int.Parse(data["ProductAgentId"]);
				this.ProductAdministratorId = int.Parse(data["ProductAdministratorId"]);
				this.PaymentDate = data["PaymentDate"];
				this.AmountDue = float.Parse(data["AmountDue"]);
				this.AmountAdjusted = float.Parse(data["AmountAdjusted"]);
				this.AmountPaid = float.Parse(data["AmountPaid"]);
				this.Notes = data["Notes"];
				this.IsCompleted = bool.Parse(data["IsCompleted"]);
				this.ProductContractList = data["ProductContractList"];
				this.productCancellationList = data["ProductCancellationList"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int productPaymentId = this.ProductPaymentId;
			data.AddParam("@ProductPaymentId", DataAccessParameterType.Numeric, productPaymentId.ToString());
			data.AddParam("@Reference", DataAccessParameterType.Text, this.Reference);
			data.AddParam("@PayeeName", DataAccessParameterType.Text, this.PayeeName);
			productPaymentId = this.LookupId_ProductPaymentType;
			data.AddParam("@LookupId_ProductPaymentType", DataAccessParameterType.Numeric, productPaymentId.ToString());
			productPaymentId = this.ProductFamilyId;
			data.AddParam("@ProductFamilyId", DataAccessParameterType.Numeric, productPaymentId.ToString());
			productPaymentId = this.AffinityGroupId;
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, productPaymentId.ToString());
			productPaymentId = this.ProductAgentId;
			data.AddParam("@ProductAgentId", DataAccessParameterType.Numeric, productPaymentId.ToString());
			productPaymentId = this.ProductAdministratorId;
			data.AddParam("@ProductAdministratorId", DataAccessParameterType.Numeric, productPaymentId.ToString());
			data.AddParam("@PaymentDate", DataAccessParameterType.DateTime, this.PaymentDate);
			float amountDue = this.AmountDue;
			data.AddParam("@AmountDue", DataAccessParameterType.Numeric, amountDue.ToString());
			amountDue = this.AmountAdjusted;
			data.AddParam("@AmountAdjusted", DataAccessParameterType.Numeric, amountDue.ToString());
			amountDue = this.AmountPaid;
			data.AddParam("@AmountPaid", DataAccessParameterType.Numeric, amountDue.ToString());
			data.AddParam("@Notes", DataAccessParameterType.Text, this.Notes);
			data.AddParam("@IsCompleted", DataAccessParameterType.Bool, this.IsCompleted.ToString());
			data.AddParam("@ProductContractList", DataAccessParameterType.Text, this.ProductContractList);
			data.AddParam("@ProductCancellationList", DataAccessParameterType.Text, this.ProductCancellationList);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("PRODUCT_PAYMENT_InsertUpdate");
			if (!data.EOF)
			{
				this.ProductPaymentId = int.Parse(data["ProductPaymentId"]);
			}
			this.retrievedata();
		}
	}
}