using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;
using System.Data.SqlTypes;

namespace Advocar.Data.Aime
{
	public class LeadSurvey : Transaction
	{
		private int leadSurveyId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int leadId = 0;

		private int lookupId_SurveyStatus = 0;

		private bool isPurchaseAutoland = false;

		private bool isFinancedCreditUnion = false;

		private bool isPurchaseOther = false;

		private string purchaseOtherFrom = "";

		private string purchaseOtherWhy = "";

		private string purchaseOtherHowSoon = "";

		private bool isFinancedCreditUnionPreapproved = false;

		private int cars_EmplId = 0;

		private int lookupId_SurveyBuyingFactor1 = 0;

		private int lookupId_SurveyBuyingFactor2 = 0;

		private int lookupId_SurveyBuyingFactor3 = 0;

		private int oneToFiveConsultantAvailability = 0;

		private int oneToFiveKnowledgeLevel = 0;

		private int oneToFivePrompness = 0;

		private int oneToFiveProfessionalism = 0;

		private int oneToFiveVehicleAppearance = 0;

		private int oneToFiveWouldReferFriend = 0;

		private int oneToFiveOverallAutolandExperience = 0;

		private int lookupId_SurveyLeadSource = 0;

		private string surveyLeadSourceOther = "";

		private int howMuchSaved = 0;

		private string comments = "";

		private bool afterMarketSealant = false;

		private bool afterMarketMbp = false;

		private bool consentGranted = false;

		public bool AfterMarketMbp
		{
			get
			{
				return this.afterMarketMbp;
			}
			set
			{
				this.afterMarketMbp = value;
			}
		}

		public bool AfterMarketSealant
		{
			get
			{
				return this.afterMarketSealant;
			}
			set
			{
				this.afterMarketSealant = value;
			}
		}

		public int Cars_EmplId
		{
			get
			{
				return this.cars_EmplId;
			}
			set
			{
				this.cars_EmplId = value;
			}
		}

		public string Comments
		{
			get
			{
				return this.comments;
			}
			set
			{
				this.comments = value;
			}
		}

		public bool ConsentGranted
		{
			get
			{
				return this.consentGranted;
			}
			set
			{
				this.consentGranted = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public int HowMuchSaved
		{
			get
			{
				return this.howMuchSaved;
			}
			set
			{
				this.howMuchSaved = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public bool IsFinancedCreditUnion
		{
			get
			{
				return this.isFinancedCreditUnion;
			}
			set
			{
				this.isFinancedCreditUnion = value;
			}
		}

		public bool IsFinancedCreditUnionPreapproved
		{
			get
			{
				return this.isFinancedCreditUnionPreapproved;
			}
			set
			{
				this.isFinancedCreditUnionPreapproved = value;
			}
		}

		public bool IsPurchaseAutoland
		{
			get
			{
				return this.isPurchaseAutoland;
			}
			set
			{
				this.isPurchaseAutoland = value;
			}
		}

		public bool IsPurchaseOther
		{
			get
			{
				return this.isPurchaseOther;
			}
			set
			{
				this.isPurchaseOther = value;
			}
		}

		public int LeadId
		{
			get
			{
				return this.leadId;
			}
			set
			{
				this.leadId = value;
			}
		}

		public int LeadSurveyId
		{
			get
			{
				return this.leadSurveyId;
			}
			set
			{
				this.leadSurveyId = value;
			}
		}

		public int LookupId_SurveyBuyingFactor1
		{
			get
			{
				return this.lookupId_SurveyBuyingFactor1;
			}
			set
			{
				this.lookupId_SurveyBuyingFactor1 = value;
			}
		}

		public int LookupId_SurveyBuyingFactor2
		{
			get
			{
				return this.lookupId_SurveyBuyingFactor2;
			}
			set
			{
				this.lookupId_SurveyBuyingFactor2 = value;
			}
		}

		public int LookupId_SurveyBuyingFactor3
		{
			get
			{
				return this.lookupId_SurveyBuyingFactor3;
			}
			set
			{
				this.lookupId_SurveyBuyingFactor3 = value;
			}
		}

		public int LookupId_SurveyLeadSource
		{
			get
			{
				return this.lookupId_SurveyLeadSource;
			}
			set
			{
				this.lookupId_SurveyLeadSource = value;
			}
		}

		public int LookupId_SurveyStatus
		{
			get
			{
				return this.lookupId_SurveyStatus;
			}
			set
			{
				this.lookupId_SurveyStatus = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public int OneToFiveConsultantAvailability
		{
			get
			{
				return this.oneToFiveConsultantAvailability;
			}
			set
			{
				this.oneToFiveConsultantAvailability = value;
			}
		}

		public int OneToFiveKnowledgeLevel
		{
			get
			{
				return this.oneToFiveKnowledgeLevel;
			}
			set
			{
				this.oneToFiveKnowledgeLevel = value;
			}
		}

		public int OneToFiveOverallAutolandExperience
		{
			get
			{
				return this.oneToFiveOverallAutolandExperience;
			}
			set
			{
				this.oneToFiveOverallAutolandExperience = value;
			}
		}

		public int OneToFiveProfessionalism
		{
			get
			{
				return this.oneToFiveProfessionalism;
			}
			set
			{
				this.oneToFiveProfessionalism = value;
			}
		}

		public int OneToFivePrompness
		{
			get
			{
				return this.oneToFivePrompness;
			}
			set
			{
				this.oneToFivePrompness = value;
			}
		}

		public int OneToFiveVehicleAppearance
		{
			get
			{
				return this.oneToFiveVehicleAppearance;
			}
			set
			{
				this.oneToFiveVehicleAppearance = value;
			}
		}

		public int OneToFiveWouldReferFriend
		{
			get
			{
				return this.oneToFiveWouldReferFriend;
			}
			set
			{
				this.oneToFiveWouldReferFriend = value;
			}
		}

		public string PurchaseOtherFrom
		{
			get
			{
				return this.purchaseOtherFrom;
			}
			set
			{
				this.purchaseOtherFrom = value;
			}
		}

		public string PurchaseOtherHowSoon
		{
			get
			{
				return this.purchaseOtherHowSoon;
			}
			set
			{
				this.purchaseOtherHowSoon = value;
			}
		}

		public string PurchaseOtherWhy
		{
			get
			{
				return this.purchaseOtherWhy;
			}
			set
			{
				this.purchaseOtherWhy = value;
			}
		}

		public string SurveyLeadSourceOther
		{
			get
			{
				return this.surveyLeadSourceOther;
			}
			set
			{
				this.surveyLeadSourceOther = value;
			}
		}

		public LeadSurvey()
		{
		}

		public LeadSurvey(string connection, string modifiedUserID, int leadSurveyId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "LEAD_SURVEY";
			this.LeadSurveyId = leadSurveyId;
			if (this.LeadSurveyId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@LeadSurveyId", DataAccessParameterType.Numeric, this.LeadSurveyId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("LEAD_SURVEY_Delete");
			this.wipeout();
		}

		public static int GetRecordId(int leadId, string connection)
		{
			int leadSurveyId = 0;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@LeadId", DataAccessParameterType.Numeric, leadId.ToString());
			data.ExecuteProcedure("LEAD_SURVEY_GetRecord");
			if (!data.EOF)
			{
				leadSurveyId = (Validation.IsNumeric(data["LeadSurveyId"]) ? Convert.ToInt32(data["LeadSurveyId"]) : 0);
			}
			return leadSurveyId;
		}

		protected override void retrievedata()
		{
			DateTime dateTime;
			SqlDateTime DefaultSqlDateTimeValue = new SqlDateTime(1900, 1, 1, 0, 0, 0);
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@LeadSurveyId", DataAccessParameterType.Numeric, this.LeadSurveyId.ToString());
			data.ExecuteProcedure("LEAD_SURVEY_GetRecord");
			if (!data.EOF)
			{
				this.LeadSurveyId = (Validation.IsNumeric(data["LeadSurveyId"]) ? Convert.ToInt32(data["LeadSurveyId"]) : 0);
				bool isDeleted = false;
				bool.TryParse(data["IsDeleted"], out isDeleted);
				this.IsDeleted = isDeleted;
				SqlDateTime sqlCreatedDate = (Validation.IsDate(data["CreatedDate"]) ? SqlDateTime.Parse(data["CreatedDate"]) : SqlDateTime.Null);
				if ((sqlCreatedDate.Equals(DefaultSqlDateTimeValue) ? false : !sqlCreatedDate.Equals(SqlDateTime.MinValue)))
				{
					dateTime = (DateTime)sqlCreatedDate;
					this.CreatedDate = dateTime.ToString();
				}
				this.CreatedUser = data["CreatedUser"];
				SqlDateTime sqlModifiedDate = (Validation.IsDate(data["ModifiedDate"]) ? SqlDateTime.Parse(data["ModifiedDate"]) : SqlDateTime.Null);
				if ((sqlModifiedDate.Equals(DefaultSqlDateTimeValue) ? false : !sqlModifiedDate.Equals(SqlDateTime.MinValue)))
				{
					dateTime = (DateTime)sqlModifiedDate;
					this.ModifiedDate = dateTime.ToString();
				}
				this.ModifiedUser = data["ModifiedUser"];
				this.LeadId = (Validation.IsNumeric(data["LeadId"]) ? Convert.ToInt32(data["LeadId"]) : 0);
				this.LookupId_SurveyStatus = (Validation.IsNumeric(data["LookupId_SurveyStatus"]) ? Convert.ToInt32(data["LookupId_SurveyStatus"]) : 0);
				bool isPurchaseAutoland = false;
				bool.TryParse(data["IsPurchaseAutoland"], out isPurchaseAutoland);
				this.IsPurchaseAutoland = isPurchaseAutoland;
				bool isFinancedCreditUnion = false;
				bool.TryParse(data["IsFinancedCreditUnion"], out isFinancedCreditUnion);
				this.IsFinancedCreditUnion = isFinancedCreditUnion;
				bool isPurchaseOther = false;
				bool.TryParse(data["IsPurchaseOther"], out isPurchaseOther);
				this.IsPurchaseOther = isPurchaseOther;
				this.PurchaseOtherFrom = data["PurchaseOtherFrom"];
				this.PurchaseOtherWhy = data["PurchaseOtherWhy"];
				this.PurchaseOtherHowSoon = data["PurchaseOtherHowSoon"];
				bool isFinancedCreditUnionPreapproved = false;
				bool.TryParse(data["IsFinancedCreditUnionPreapproved"], out isFinancedCreditUnionPreapproved);
				this.IsFinancedCreditUnionPreapproved = isFinancedCreditUnionPreapproved;
				this.Cars_EmplId = (Validation.IsNumeric(data["Cars_EmplId"]) ? Convert.ToInt32(data["Cars_EmplId"]) : 0);
				this.LookupId_SurveyBuyingFactor1 = (Validation.IsNumeric(data["LookupId_SurveyBuyingFactor1"]) ? Convert.ToInt32(data["LookupId_SurveyBuyingFactor1"]) : 0);
				this.LookupId_SurveyBuyingFactor2 = (Validation.IsNumeric(data["LookupId_SurveyBuyingFactor2"]) ? Convert.ToInt32(data["LookupId_SurveyBuyingFactor2"]) : 0);
				this.LookupId_SurveyBuyingFactor3 = (Validation.IsNumeric(data["LookupId_SurveyBuyingFactor3"]) ? Convert.ToInt32(data["LookupId_SurveyBuyingFactor3"]) : 0);
				this.OneToFiveConsultantAvailability = (Validation.IsNumeric(data["OneToFiveConsultantAvailability"]) ? Convert.ToInt32(data["OneToFiveConsultantAvailability"]) : 0);
				this.OneToFiveKnowledgeLevel = (Validation.IsNumeric(data["OneToFiveKnowledgeLevel"]) ? Convert.ToInt32(data["OneToFiveKnowledgeLevel"]) : 0);
				this.OneToFivePrompness = (Validation.IsNumeric(data["OneToFivePrompness"]) ? Convert.ToInt32(data["OneToFivePrompness"]) : 0);
				this.OneToFiveProfessionalism = (Validation.IsNumeric(data["OneToFiveProfessionalism"]) ? Convert.ToInt32(data["OneToFiveProfessionalism"]) : 0);
				this.OneToFiveVehicleAppearance = (Validation.IsNumeric(data["OneToFiveVehicleAppearance"]) ? Convert.ToInt32(data["OneToFiveVehicleAppearance"]) : 0);
				this.OneToFiveWouldReferFriend = (Validation.IsNumeric(data["OneToFiveWouldReferFriend"]) ? Convert.ToInt32(data["OneToFiveWouldReferFriend"]) : 0);
				this.OneToFiveOverallAutolandExperience = (Validation.IsNumeric(data["OneToFiveOverallAutolandExperience"]) ? Convert.ToInt32(data["OneToFiveOverallAutolandExperience"]) : 0);
				this.LookupId_SurveyLeadSource = (Validation.IsNumeric(data["LookupId_SurveyLeadSource"]) ? Convert.ToInt32(data["LookupId_SurveyLeadSource"]) : 0);
				this.SurveyLeadSourceOther = data["SurveyLeadSourceOther"];
				this.HowMuchSaved = (Validation.IsNumeric(data["HowMuchSaved"]) ? Convert.ToInt32(data["HowMuchSaved"]) : 0);
				this.Comments = data["Comments"];
				bool afterMarketSealant = false;
				bool.TryParse(data["AfterMarketSealant"], out afterMarketSealant);
				this.AfterMarketSealant = afterMarketSealant;
				bool afterMarketMbp = false;
				bool.TryParse(data["AfterMarketMbp"], out afterMarketMbp);
				this.AfterMarketMbp = afterMarketMbp;
				bool consentGranted = false;
				bool.TryParse(data["ConsentGranted"], out consentGranted);
				this.ConsentGranted = consentGranted;
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			string leadSurveyIdString = (this.LeadSurveyId > 0 ? this.LeadSurveyId.ToString() : string.Empty);
			string leadIdString = (this.LeadId > 0 ? this.LeadId.ToString() : string.Empty);
			string lookupIdSurveyStatusString = (this.LookupId_SurveyStatus > 0 ? this.LookupId_SurveyStatus.ToString() : string.Empty);
			string carsEmplIdString = (this.Cars_EmplId > 0 ? this.Cars_EmplId.ToString() : string.Empty);
			string lookupIdSurveyBuyingFactor1String = (this.LookupId_SurveyBuyingFactor1 > 0 ? this.LookupId_SurveyBuyingFactor1.ToString() : string.Empty);
			string lookupIdSurveyBuyingFactor2String = (this.LookupId_SurveyBuyingFactor2 > 0 ? this.LookupId_SurveyBuyingFactor2.ToString() : string.Empty);
			string lookupIdSurveyBuyingFactor3String = (this.LookupId_SurveyBuyingFactor3 > 0 ? this.LookupId_SurveyBuyingFactor3.ToString() : string.Empty);
			string oneToFiveConsultantAvailabilityString = (this.OneToFiveConsultantAvailability > 0 ? this.OneToFiveConsultantAvailability.ToString() : string.Empty);
			string oneToFiveKnowledgeLevelString = (this.OneToFiveKnowledgeLevel > 0 ? this.OneToFiveKnowledgeLevel.ToString() : string.Empty);
			string oneToFivePromptnessString = (this.OneToFivePrompness > 0 ? this.OneToFivePrompness.ToString() : string.Empty);
			string oneToFiveProfessionalismString = (this.OneToFiveProfessionalism > 0 ? this.OneToFiveProfessionalism.ToString() : string.Empty);
			string oneToFiveVehicleAppearanceString = (this.OneToFiveVehicleAppearance > 0 ? this.OneToFiveVehicleAppearance.ToString() : string.Empty);
			string oneToFiveWouldReferToFriendString = (this.OneToFiveWouldReferFriend > 0 ? this.OneToFiveWouldReferFriend.ToString() : string.Empty);
			string oneToFiveOverallAutolandExperienceString = (this.OneToFiveOverallAutolandExperience > 0 ? this.OneToFiveOverallAutolandExperience.ToString() : string.Empty);
			string lookupIdSurveyLeadSourceString = (this.LookupId_SurveyLeadSource > 0 ? this.LookupId_SurveyLeadSource.ToString() : string.Empty);
			string howMuchSavedString = (this.HowMuchSaved > 0 ? this.HowMuchSaved.ToString() : string.Empty);
			string isPurchaseAutolandString = (this.IsPurchaseAutoland ? "1" : "0");
			string isFinancedCreditUnionString = (this.IsFinancedCreditUnion ? "1" : "0");
			string isPurchaseOtherString = (this.IsPurchaseOther ? "1" : "0");
			string isFinancedCreditUnionPreApproved = (this.IsFinancedCreditUnionPreapproved ? "1" : "0");
			string afterMarketSealantString = (this.AfterMarketSealant ? "1" : "0");
			string afterMarketMbpString = (this.AfterMarketMbp ? "1" : "0");
			string consentGrantedString = (this.ConsentGranted ? "1" : "0");
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@LeadSurveyId", DataAccessParameterType.Numeric, leadSurveyIdString, true);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.ModifiedUser, true);
			data.AddParam("@LeadId", DataAccessParameterType.Numeric, leadIdString, true);
			data.AddParam("@LookupId_SurveyStatus", DataAccessParameterType.Numeric, lookupIdSurveyStatusString, true);
			data.AddParam("@IsPurchaseAutoland", DataAccessParameterType.Bool, isPurchaseAutolandString, true);
			data.AddParam("@IsFinancedCreditUnion", DataAccessParameterType.Bool, isFinancedCreditUnionString, true);
			data.AddParam("@IsPurchaseOther", DataAccessParameterType.Bool, isPurchaseOtherString, true);
			data.AddParam("@PurchaseOtherFrom", DataAccessParameterType.Text, this.PurchaseOtherFrom, true);
			data.AddParam("@PurchaseOtherWhy", DataAccessParameterType.Text, this.PurchaseOtherWhy, true);
			data.AddParam("@PurchaseOtherHowSoon", DataAccessParameterType.Text, this.PurchaseOtherWhy, true);
			data.AddParam("@IsFinancedCreditUnionPreapproved", DataAccessParameterType.Bool, isFinancedCreditUnionPreApproved, true);
			data.AddParam("@Cars_EmplId", DataAccessParameterType.Numeric, carsEmplIdString, true);
			data.AddParam("@LookupId_SurveyBuyingFactor1", DataAccessParameterType.Numeric, lookupIdSurveyBuyingFactor1String, true);
			data.AddParam("@LookupId_SurveyBuyingFactor2", DataAccessParameterType.Numeric, lookupIdSurveyBuyingFactor2String, true);
			data.AddParam("@LookupId_SurveyBuyingFactor3", DataAccessParameterType.Numeric, lookupIdSurveyBuyingFactor3String, true);
			data.AddParam("@OneToFiveConsultantAvailability", DataAccessParameterType.Numeric, oneToFiveConsultantAvailabilityString, true);
			data.AddParam("@OneToFiveKnowledgeLevel", DataAccessParameterType.Numeric, oneToFiveKnowledgeLevelString, true);
			data.AddParam("@OneToFivePrompness", DataAccessParameterType.Numeric, oneToFivePromptnessString, true);
			data.AddParam("@OneToFiveProfessionalism", DataAccessParameterType.Numeric, oneToFiveProfessionalismString, true);
			data.AddParam("@OneToFiveVehicleAppearance", DataAccessParameterType.Numeric, oneToFiveVehicleAppearanceString, true);
			data.AddParam("@OneToFiveWouldReferFriend", DataAccessParameterType.Numeric, oneToFiveWouldReferToFriendString, true);
			data.AddParam("@OneToFiveOverallAutolandExperience", DataAccessParameterType.Numeric, oneToFiveOverallAutolandExperienceString, true);
			data.AddParam("@LookupId_SurveyLeadSource", DataAccessParameterType.Numeric, lookupIdSurveyLeadSourceString, true);
			data.AddParam("@SurveyLeadSourceOther", DataAccessParameterType.Text, this.SurveyLeadSourceOther, true);
			data.AddParam("@HowMuchSaved", DataAccessParameterType.Numeric, howMuchSavedString, true);
			data.AddParam("@Comments", DataAccessParameterType.Text, this.Comments, true);
			data.AddParam("@AfterMarketSealant", DataAccessParameterType.Bool, afterMarketSealantString, true);
			data.AddParam("@AfterMarketMbp", DataAccessParameterType.Bool, afterMarketMbpString, true);
			data.AddParam("@ConsentGranted", DataAccessParameterType.Bool, consentGrantedString, true);
			data.ExecuteProcedure("LEAD_SURVEY_InsertUpdate");
			if (!data.EOF)
			{
				this.LeadSurveyId = (Validation.IsNumeric(data["LeadSurveyId"]) ? Convert.ToInt32(data["LeadSurveyId"]) : 0);
			}
			this.retrievedata();
		}
	}
}