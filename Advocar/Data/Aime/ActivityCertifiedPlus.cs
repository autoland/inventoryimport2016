using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class ActivityCertifiedPlus : Transaction
	{
		private int activityCertifiedPlusId = 0;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private bool isDeleted = false;

		private int activityId = 0;

		private int productId = 0;

		private int lookupId_CertifiedPlusStatus = 0;

		private string agreementNumber = "";

		private int configurator_YearId = 0;

		private int configurator_MakeId = 0;

		private int configurator_ModelId = 0;

		private string vin = "";

		private int termYears = 0;

		private int isCertPlusOrPDROnly = 0;

		private float premium = 0f;

		private float premiumCost = 0f;

		private string dateOfTransfer = "";

		private int affinityGroupId = 0;

		private int memberId = 0;

		public int ActivityCertifiedPlusId
		{
			get
			{
				return this.activityCertifiedPlusId;
			}
			set
			{
				this.activityCertifiedPlusId = value;
			}
		}

		public int ActivityId
		{
			get
			{
				return this.activityId;
			}
			set
			{
				this.activityId = value;
			}
		}

		public int AffinityGroupId
		{
			get
			{
				return this.affinityGroupId;
			}
			set
			{
				this.affinityGroupId = value;
			}
		}

		public string AgreementNumber
		{
			get
			{
				return this.agreementNumber;
			}
			set
			{
				this.agreementNumber = value;
			}
		}

		public int Configurator_MakeId
		{
			get
			{
				return this.configurator_MakeId;
			}
			set
			{
				this.configurator_MakeId = value;
			}
		}

		public int Configurator_ModelId
		{
			get
			{
				return this.configurator_ModelId;
			}
			set
			{
				this.configurator_ModelId = value;
			}
		}

		public int Configurator_YearId
		{
			get
			{
				return this.configurator_YearId;
			}
			set
			{
				this.configurator_YearId = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string DateOfTransfer
		{
			get
			{
				return this.dateOfTransfer;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.dateOfTransfer = "";
				}
				else
				{
					this.dateOfTransfer = DateTime.Parse(value).ToString();
				}
			}
		}

		public int IsCertPlusOrPDROnly
		{
			get
			{
				return this.isCertPlusOrPDROnly;
			}
			set
			{
				this.isCertPlusOrPDROnly = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public int LookupId_CertifiedPlusStatus
		{
			get
			{
				return this.lookupId_CertifiedPlusStatus;
			}
			set
			{
				this.lookupId_CertifiedPlusStatus = value;
			}
		}

		public int MemberId
		{
			get
			{
				return this.memberId;
			}
			set
			{
				this.memberId = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public float Premium
		{
			get
			{
				return this.premium;
			}
			set
			{
				this.premium = value;
			}
		}

		public float PremiumCost
		{
			get
			{
				return this.premiumCost;
			}
			set
			{
				this.premiumCost = value;
			}
		}

		public int ProductId
		{
			get
			{
				return this.productId;
			}
			set
			{
				this.productId = value;
			}
		}

		public int TermYears
		{
			get
			{
				return this.termYears;
			}
			set
			{
				this.termYears = value;
			}
		}

		public string Vin
		{
			get
			{
				return this.vin;
			}
			set
			{
				this.vin = value;
			}
		}

		public ActivityCertifiedPlus()
		{
		}

		public ActivityCertifiedPlus(string connection, string modifiedUserID, int activityCertifiedPlusId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.ActivityCertifiedPlusId = activityCertifiedPlusId;
			if (this.ActivityCertifiedPlusId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ActivityCertifiedPlusId", DataAccessParameterType.Numeric, this.ActivityCertifiedPlusId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("ACTIVITY_CERTIFIED_PLUS_Delete");
			this.wipeout();
		}

		public static int GetRecordId(int activityId, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@ActivityId", DataAccessParameterType.Numeric, activityId.ToString());
			data.ExecuteProcedure("ACTIVITY_CERTIFIED_PLUS_GetRecord");
			num = (!data.EOF ? int.Parse(data["ActivityCertifiedPlusId"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ActivityCertifiedPlusId", DataAccessParameterType.Numeric, this.ActivityCertifiedPlusId.ToString());
			data.ExecuteProcedure("ACTIVITY_CERTIFIED_PLUS_GetRecord");
			if (!data.EOF)
			{
				this.ActivityCertifiedPlusId = int.Parse(data["ActivityCertifiedPlusId"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.ActivityId = int.Parse(data["ActivityId"]);
				this.ProductId = int.Parse(data["ProductId"]);
				this.LookupId_CertifiedPlusStatus = int.Parse(data["LookupId_CertifiedPlusStatus"]);
				this.AgreementNumber = data["AgreementNumber"];
				this.Configurator_YearId = int.Parse(data["Configurator_YearId"]);
				this.Configurator_MakeId = int.Parse(data["Configurator_MakeId"]);
				this.Configurator_ModelId = int.Parse(data["Configurator_ModelId"]);
				this.Vin = data["Vin"];
				this.TermYears = int.Parse(data["TermYears"]);
				this.IsCertPlusOrPDROnly = int.Parse(data["IsCertPlusOrPDROnly"]);
				this.Premium = float.Parse(data["Premium"]);
				this.PremiumCost = float.Parse(data["PremiumCost"]);
				this.DateOfTransfer = data["DateOfTransfer"];
				this.MemberId = int.Parse(data["MemberId"]);
				this.AffinityGroupId = int.Parse(data["AffinityGroupId"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int activityCertifiedPlusId = this.ActivityCertifiedPlusId;
			data.AddParam("@ActivityCertifiedPlusId", DataAccessParameterType.Numeric, activityCertifiedPlusId.ToString());
			activityCertifiedPlusId = this.ActivityId;
			data.AddParam("@ActivityId", DataAccessParameterType.Numeric, activityCertifiedPlusId.ToString());
			activityCertifiedPlusId = this.ProductId;
			data.AddParam("@ProductId", DataAccessParameterType.Numeric, activityCertifiedPlusId.ToString());
			activityCertifiedPlusId = this.LookupId_CertifiedPlusStatus;
			data.AddParam("@LookupId_CertifiedPlusStatus", DataAccessParameterType.Numeric, activityCertifiedPlusId.ToString());
			data.AddParam("@AgreementNumber", DataAccessParameterType.Text, this.AgreementNumber);
			activityCertifiedPlusId = this.Configurator_YearId;
			data.AddParam("@Configurator_YearId", DataAccessParameterType.Numeric, activityCertifiedPlusId.ToString());
			activityCertifiedPlusId = this.Configurator_MakeId;
			data.AddParam("@Configurator_MakeId", DataAccessParameterType.Numeric, activityCertifiedPlusId.ToString());
			activityCertifiedPlusId = this.Configurator_ModelId;
			data.AddParam("@Configurator_ModelId", DataAccessParameterType.Numeric, activityCertifiedPlusId.ToString());
			data.AddParam("@Vin", DataAccessParameterType.Text, this.Vin);
			activityCertifiedPlusId = this.TermYears;
			data.AddParam("@TermYears", DataAccessParameterType.Numeric, activityCertifiedPlusId.ToString());
			activityCertifiedPlusId = this.IsCertPlusOrPDROnly;
			data.AddParam("@IsCertPlusOrPDROnly", DataAccessParameterType.Numeric, activityCertifiedPlusId.ToString());
			float premium = this.Premium;
			data.AddParam("@Premium", DataAccessParameterType.Numeric, premium.ToString());
			premium = this.PremiumCost;
			data.AddParam("@PremiumCost", DataAccessParameterType.Numeric, premium.ToString());
			data.AddParam("@DateOfTransfer", DataAccessParameterType.DateTime, this.DateOfTransfer);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			activityCertifiedPlusId = this.MemberId;
			data.AddParam("@MemberId", DataAccessParameterType.Numeric, activityCertifiedPlusId.ToString());
			activityCertifiedPlusId = this.AffinityGroupId;
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, activityCertifiedPlusId.ToString());
			data.ExecuteProcedure("ACTIVITY_CERTIFIED_PLUS_InsertUpdate");
			if (!data.EOF)
			{
				this.ActivityId = int.Parse(data["ActivityId"]);
				this.ActivityCertifiedPlusId = int.Parse(data["ActivityCertifiedPlusId"]);
			}
			this.retrievedata();
		}
	}
}