using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class DealerAccounting : Transaction
	{
		private int dealerAccountingId = 0;

		private bool isDeleted = false;

		private string modifiedDate = "";

		private string modifiedUser = "";

		private string createdDate = "";

		private string createdUser = "";

		private int dealerId = 0;

		private float vehiclePriceStart = 0f;

		private float vehiclePriceEnd = 0f;

		private float dealerProfit = 0f;

		private float consultantCommission = 0f;

		private string effectiveDate = "";

		public float ConsultantCommission
		{
			get
			{
				return this.consultantCommission;
			}
			set
			{
				this.consultantCommission = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public int DealerAccountingId
		{
			get
			{
				return this.dealerAccountingId;
			}
			set
			{
				this.dealerAccountingId = value;
			}
		}

		public int DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public float DealerProfit
		{
			get
			{
				return this.dealerProfit;
			}
			set
			{
				this.dealerProfit = value;
			}
		}

		public string EffectiveDate
		{
			get
			{
				return this.effectiveDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.effectiveDate = "";
				}
				else
				{
					this.effectiveDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public float VehiclePriceEnd
		{
			get
			{
				return this.vehiclePriceEnd;
			}
			set
			{
				this.vehiclePriceEnd = value;
			}
		}

		public float VehiclePriceStart
		{
			get
			{
				return this.vehiclePriceStart;
			}
			set
			{
				this.vehiclePriceStart = value;
			}
		}

		public DealerAccounting()
		{
		}

		public DealerAccounting(string connection, string modifiedUserID, int dealerAccountingId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.DealerAccountingId = dealerAccountingId;
			if (this.DealerAccountingId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@DealerAccountingId", DataAccessParameterType.Numeric, this.DealerAccountingId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("DEALER_ACCOUNTING_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@DealerAccountingId", DataAccessParameterType.Numeric, this.DealerAccountingId.ToString());
			data.ExecuteProcedure("DEALER_ACCOUNTING_GetRecord");
			if (!data.EOF)
			{
				this.DealerAccountingId = int.Parse(data["DealerAccountingId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.DealerId = int.Parse(data["DealerId"]);
				this.VehiclePriceStart = float.Parse(data["VehiclePriceStart"]);
				this.VehiclePriceEnd = float.Parse(data["VehiclePriceEnd"]);
				this.DealerProfit = float.Parse(data["DealerProfit"]);
				this.ConsultantCommission = float.Parse(data["ConsultantCommission"]);
				this.EffectiveDate = data["EffectiveDate"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int dealerAccountingId = this.DealerAccountingId;
			data.AddParam("@DealerAccountingId", DataAccessParameterType.Numeric, dealerAccountingId.ToString());
			dealerAccountingId = this.DealerId;
			data.AddParam("@DealerId", DataAccessParameterType.Numeric, dealerAccountingId.ToString());
			float vehiclePriceStart = this.VehiclePriceStart;
			data.AddParam("@VehiclePriceStart", DataAccessParameterType.Numeric, vehiclePriceStart.ToString());
			vehiclePriceStart = this.VehiclePriceEnd;
			data.AddParam("@VehiclePriceEnd", DataAccessParameterType.Numeric, vehiclePriceStart.ToString());
			vehiclePriceStart = this.DealerProfit;
			data.AddParam("@DealerProfit", DataAccessParameterType.Numeric, vehiclePriceStart.ToString());
			vehiclePriceStart = this.ConsultantCommission;
			data.AddParam("@ConsultantCommission", DataAccessParameterType.Numeric, vehiclePriceStart.ToString());
			data.AddParam("@EffectiveDate", DataAccessParameterType.DateTime, this.EffectiveDate);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("DEALER_ACCOUNTING_InsertUpdate");
			if (!data.EOF)
			{
				this.DealerAccountingId = int.Parse(data["DealerAccountingId"]);
			}
			this.retrievedata();
		}
	}
}