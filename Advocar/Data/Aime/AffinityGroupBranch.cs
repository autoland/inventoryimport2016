using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class AffinityGroupBranch : Transaction
	{
		private int affinityGroupBranchId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int affinityGroupId = 0;

		private string branchName = "";

		private string branchCode = "";

		private bool isLienholder = false;

		private bool isPolicyholder = false;

		private int addressId = 0;

		private int accountingControlId = 0;

		private int employeeId_Marketing = 0;

		private string directions = "";

		private bool isHolidaybasket = false;

		private bool isVscEligible = false;

		private bool isMbpEligible = false;

		public int AccountingControlId
		{
			get
			{
				return this.accountingControlId;
			}
			set
			{
				this.accountingControlId = value;
			}
		}

		public int AddressId
		{
			get
			{
				return this.addressId;
			}
			set
			{
				this.addressId = value;
			}
		}

		public int AffinityGroupBranchId
		{
			get
			{
				return this.affinityGroupBranchId;
			}
			set
			{
				this.affinityGroupBranchId = value;
			}
		}

		public int AffinityGroupId
		{
			get
			{
				return this.affinityGroupId;
			}
			set
			{
				this.affinityGroupId = value;
			}
		}

		public string BranchCode
		{
			get
			{
				return this.branchCode;
			}
			set
			{
				this.branchCode = value;
			}
		}

		public string BranchName
		{
			get
			{
				return this.branchName;
			}
			set
			{
				this.branchName = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string Directions
		{
			get
			{
				return this.directions;
			}
			set
			{
				this.directions = value;
			}
		}

		public int EmployeeId_Marketing
		{
			get
			{
				return this.employeeId_Marketing;
			}
			set
			{
				this.employeeId_Marketing = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public bool IsHolidaybasket
		{
			get
			{
				return this.isHolidaybasket;
			}
			set
			{
				this.isHolidaybasket = value;
			}
		}

		public bool IsLienholder
		{
			get
			{
				return this.isLienholder;
			}
			set
			{
				this.isLienholder = value;
			}
		}

		public bool IsMbpEligible
		{
			get
			{
				return this.isMbpEligible;
			}
			set
			{
				this.isMbpEligible = value;
			}
		}

		public bool IsPolicyholder
		{
			get
			{
				return this.isPolicyholder;
			}
			set
			{
				this.isPolicyholder = value;
			}
		}

		public bool IsVscEligible
		{
			get
			{
				return this.isVscEligible;
			}
			set
			{
				this.isVscEligible = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public AffinityGroupBranch()
		{
		}

		public AffinityGroupBranch(string connection, string modifiedUserID, int affinityGroupBranchId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.AffinityGroupBranchId = affinityGroupBranchId;
			if (this.AffinityGroupBranchId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@AffinityGroupBranchId", DataAccessParameterType.Numeric, this.AffinityGroupBranchId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("AFFINITY_GROUP_BRANCH_Delete");
			this.wipeout();
		}

		public static int GetRecordId(int affinityGroupId, AffinityGroupBranchType branchType, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, affinityGroupId.ToString());
			data.AddParam("@AffinityGroupBranchType", DataAccessParameterType.Text, branchType.ToString());
			data.ExecuteProcedure("AFFINITY_GROUP_BRANCH_GetRecord");
			num = (!data.EOF ? int.Parse(data["AffinityGroupBranchId"]) : 0);
			return num;
		}

		public static int GetRecordId(string userId, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@UserId", DataAccessParameterType.Text, userId);
			data.ExecuteProcedure("AFFINITY_GROUP_BRANCH_GetRecord");
			num = (!data.EOF ? int.Parse(data["AffinityGroupBranchId"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@AffinityGroupBranchId", DataAccessParameterType.Numeric, this.AffinityGroupBranchId.ToString());
			data.ExecuteProcedure("AFFINITY_GROUP_BRANCH_GetRecord");
			if (!data.EOF)
			{
				this.AffinityGroupBranchId = int.Parse(data["AffinityGroupBranchId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.AffinityGroupId = int.Parse(data["AffinityGroupId"]);
				this.BranchName = data["BranchName"];
				this.BranchCode = data["BranchCode"];
				this.IsLienholder = bool.Parse(data["IsLienholder"]);
				this.IsPolicyholder = bool.Parse(data["IsPolicyholder"]);
				this.AddressId = int.Parse(data["AddressId"]);
				this.AccountingControlId = int.Parse(data["AccountingControlId"]);
				this.EmployeeId_Marketing = int.Parse(data["EmployeeId_Marketing"]);
				this.Directions = data["Directions"];
				this.IsHolidaybasket = bool.Parse(data["IsHolidaybasket"]);
				this.IsVscEligible = bool.Parse(data["IsVscEligible"]);
				this.IsMbpEligible = bool.Parse(data["IsMbpEligible"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int affinityGroupBranchId = this.AffinityGroupBranchId;
			data.AddParam("@AffinityGroupBranchId", DataAccessParameterType.Numeric, affinityGroupBranchId.ToString());
			affinityGroupBranchId = this.AffinityGroupId;
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, affinityGroupBranchId.ToString());
			data.AddParam("@BranchName", DataAccessParameterType.Text, this.BranchName);
			data.AddParam("@BranchCode", DataAccessParameterType.Text, this.BranchCode);
			bool isLienholder = this.IsLienholder;
			data.AddParam("@IsLienholder", DataAccessParameterType.Bool, isLienholder.ToString());
			isLienholder = this.IsPolicyholder;
			data.AddParam("@IsPolicyholder", DataAccessParameterType.Bool, isLienholder.ToString());
			affinityGroupBranchId = this.AddressId;
			data.AddParam("@AddressId", DataAccessParameterType.Numeric, affinityGroupBranchId.ToString());
			affinityGroupBranchId = this.AccountingControlId;
			data.AddParam("@AccountingControlId", DataAccessParameterType.Numeric, affinityGroupBranchId.ToString());
			affinityGroupBranchId = this.EmployeeId_Marketing;
			data.AddParam("@EmployeeId_Marketing", DataAccessParameterType.Numeric, affinityGroupBranchId.ToString());
			data.AddParam("@Directions", DataAccessParameterType.Text, this.Directions);
			isLienholder = this.IsHolidaybasket;
			data.AddParam("@IsHolidaybasket", DataAccessParameterType.Bool, isLienholder.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("AFFINITY_GROUP_BRANCH_InsertUpdate");
			if (!data.EOF)
			{
				this.AffinityGroupBranchId = int.Parse(data["AffinityGroupBranchId"]);
			}
			this.retrievedata();
		}
	}
}