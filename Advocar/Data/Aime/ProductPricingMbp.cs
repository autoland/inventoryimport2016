using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class ProductPricingMbp : Transaction
	{
		private int productPricingMbpId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private string mbpVsc = "";

		private int lookupId_MbpPlan = 0;

		private int lookupId_MbpMileageBand = 0;

		private string mbpClass = "";

		private int termYears = 0;

		private int termMiles = 0;

		private float deductible = 0f;

		private float premium = 0f;

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public float Deductible
		{
			get
			{
				return this.deductible;
			}
			set
			{
				this.deductible = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public int LookupId_MbpMileageBand
		{
			get
			{
				return this.lookupId_MbpMileageBand;
			}
			set
			{
				this.lookupId_MbpMileageBand = value;
			}
		}

		public int LookupId_MbpPlan
		{
			get
			{
				return this.lookupId_MbpPlan;
			}
			set
			{
				this.lookupId_MbpPlan = value;
			}
		}

		public string MbpClass
		{
			get
			{
				return this.mbpClass;
			}
			set
			{
				this.mbpClass = value;
			}
		}

		public string MbpVsc
		{
			get
			{
				return this.mbpVsc;
			}
			set
			{
				this.mbpVsc = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public float Premium
		{
			get
			{
				return this.premium;
			}
			set
			{
				this.premium = value;
			}
		}

		public int ProductPricingMbpId
		{
			get
			{
				return this.productPricingMbpId;
			}
			set
			{
				this.productPricingMbpId = value;
			}
		}

		public int TermMiles
		{
			get
			{
				return this.termMiles;
			}
			set
			{
				this.termMiles = value;
			}
		}

		public int TermYears
		{
			get
			{
				return this.termYears;
			}
			set
			{
				this.termYears = value;
			}
		}

		public ProductPricingMbp()
		{
		}

		public ProductPricingMbp(string connection, string modifiedUserID, int productPricingMbpId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.ProductPricingMbpId = productPricingMbpId;
			if (this.ProductPricingMbpId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ProductPricingMbpId", DataAccessParameterType.Numeric, this.ProductPricingMbpId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("PRODUCT_PRICING_MBP_Delete");
			this.wipeout();
		}

		public static int GetRecordId(int ProductPricingMbpId, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@ProductPricingMbpId", DataAccessParameterType.Numeric, ProductPricingMbpId.ToString());
			data.ExecuteProcedure("PRODUCT_PRICING_MBP_GetRecord");
			num = (!data.EOF ? int.Parse(data["ProductPricingMbpId"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ProductPricingMbpId", DataAccessParameterType.Numeric, this.ProductPricingMbpId.ToString());
			data.ExecuteProcedure("PRODUCT_PRICING_MBP_GetRecord");
			if (!data.EOF)
			{
				this.ProductPricingMbpId = int.Parse(data["ProductPricingMbpId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.MbpVsc = data["MbpVsc"];
				this.LookupId_MbpPlan = int.Parse(data["LookupId_MbpPlan"]);
				this.LookupId_MbpMileageBand = int.Parse(data["LookupId_MbpMileageBand"]);
				this.MbpClass = data["MbpClass"];
				this.TermYears = int.Parse(data["TermYears"]);
				this.TermMiles = int.Parse(data["TermMiles"]);
				this.Deductible = float.Parse(data["Deductible"]);
				this.Premium = float.Parse(data["Premium"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int productPricingMbpId = this.ProductPricingMbpId;
			data.AddParam("@ProductPricingMbpId", DataAccessParameterType.Numeric, productPricingMbpId.ToString());
			data.AddParam("@MbpVsc", DataAccessParameterType.Text, this.MbpVsc);
			productPricingMbpId = this.LookupId_MbpPlan;
			data.AddParam("@LookupId_MbpPlan", DataAccessParameterType.Numeric, productPricingMbpId.ToString());
			productPricingMbpId = this.LookupId_MbpMileageBand;
			data.AddParam("@LookupId_MbpMileageBand", DataAccessParameterType.Numeric, productPricingMbpId.ToString());
			data.AddParam("@MbpClass", DataAccessParameterType.Text, this.MbpClass);
			productPricingMbpId = this.TermYears;
			data.AddParam("@TermYears", DataAccessParameterType.Numeric, productPricingMbpId.ToString());
			productPricingMbpId = this.TermMiles;
			data.AddParam("@TermMiles", DataAccessParameterType.Numeric, productPricingMbpId.ToString());
			float deductible = this.Deductible;
			data.AddParam("@Deductible", DataAccessParameterType.Numeric, deductible.ToString());
			deductible = this.Premium;
			data.AddParam("@Premium", DataAccessParameterType.Numeric, deductible.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("PRODUCT_PRICING_MBP_InsertUpdate");
			if (!data.EOF)
			{
				this.ProductPricingMbpId = int.Parse(data["ProductPricingMbpId"]);
			}
			this.retrievedata();
		}
	}
}