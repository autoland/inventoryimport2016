using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class AffinityGroupContact : Transaction
	{
		private int affinityGroupContactId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int affinityGroupId = 0;

		private int affinityGroupBranchId = 0;

		private string firstName = "";

		private string lastName = "";

		private string initials = "";

		private string jobTitle = "";

		private string dateOfBirth = "";

		private int addressId = 0;

		private string email = "";

		private string userId = "";

		private bool isProductMail = false;

		private bool isLoyaltyWorks = false;

		private bool isLoyaltyWorksStatements = false;

		private bool isLoyaltyWorksMailhome = false;

		private bool isLoanOfficer = false;

		private int noteId = 0;

		public int AddressId
		{
			get
			{
				return this.addressId;
			}
			set
			{
				this.addressId = value;
			}
		}

		public int AffinityGroupBranchId
		{
			get
			{
				return this.affinityGroupBranchId;
			}
			set
			{
				this.affinityGroupBranchId = value;
			}
		}

		public int AffinityGroupContactId
		{
			get
			{
				return this.affinityGroupContactId;
			}
			set
			{
				this.affinityGroupContactId = value;
			}
		}

		public int AffinityGroupId
		{
			get
			{
				return this.affinityGroupId;
			}
			set
			{
				this.affinityGroupId = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string DateOfBirth
		{
			get
			{
				return this.dateOfBirth;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.dateOfBirth = "";
				}
				else
				{
					this.dateOfBirth = DateTime.Parse(value).ToString();
				}
			}
		}

		public string Email
		{
			get
			{
				return this.email;
			}
			set
			{
				this.email = value;
			}
		}

		public string FirstName
		{
			get
			{
				return this.firstName;
			}
			set
			{
				this.firstName = value;
			}
		}

		public string Initials
		{
			get
			{
				return this.initials;
			}
			set
			{
				this.initials = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public bool IsLoanOfficer
		{
			get
			{
				return this.isLoanOfficer;
			}
			set
			{
				this.isLoanOfficer = value;
			}
		}

		public bool IsLoyaltyWorks
		{
			get
			{
				return this.isLoyaltyWorks;
			}
			set
			{
				this.isLoyaltyWorks = value;
			}
		}

		public bool IsLoyaltyWorksMailhome
		{
			get
			{
				return this.isLoyaltyWorksMailhome;
			}
			set
			{
				this.isLoyaltyWorksMailhome = value;
			}
		}

		public bool IsLoyaltyWorksStatements
		{
			get
			{
				return this.isLoyaltyWorksStatements;
			}
			set
			{
				this.isLoyaltyWorksStatements = value;
			}
		}

		public bool IsProductMail
		{
			get
			{
				return this.isProductMail;
			}
			set
			{
				this.isProductMail = value;
			}
		}

		public string JobTitle
		{
			get
			{
				return this.jobTitle;
			}
			set
			{
				this.jobTitle = value;
			}
		}

		public string LastName
		{
			get
			{
				return this.lastName;
			}
			set
			{
				this.lastName = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public int NoteId
		{
			get
			{
				return this.noteId;
			}
			set
			{
				this.noteId = value;
			}
		}

		public string UserId
		{
			get
			{
				return this.userId;
			}
			set
			{
				this.userId = value;
			}
		}

		public AffinityGroupContact()
		{
		}

		public AffinityGroupContact(string connection, string modifiedUserID, int affinityGroupContactId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.AffinityGroupContactId = affinityGroupContactId;
			if (this.AffinityGroupContactId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@AffinityGroupContactId", DataAccessParameterType.Numeric, this.AffinityGroupContactId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("AFFINITY_GROUP_CONTACT_Delete");
			this.wipeout();
		}

		public static int GetRecordId(int AffinityGroupBranchId, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			string query = string.Concat("SELECT TOP 1 AFFINITY_GROUP_CONTACT_ID FROM AFFINITY_GROUP_CONTACT WHERE AFFINITY_GROUP_BRANCH_ID = ", AffinityGroupBranchId.ToString(), " AND IsProductMail = 1 AND IsDeleted = 0 ORDER BY AFFINITY_GROUP_CONTACT_ID");
			data.ExecuteStatement(query);
			num = (!data.EOF ? int.Parse(data["AFFINITY_GROUP_CONTACT_ID"]) : 0);
			return num;
		}

		public static int GetRecordId(string userId, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.ExecuteStatement(string.Concat("SELECT TOP 1 AFFINITY_GROUP_CONTACT_ID FROM AFFINITY_GROUP_CONTACT WHERE USER_ID = '", userId, "' AND IsDeleted = 0"));
			num = (!data.EOF ? int.Parse(data["AFFINITY_GROUP_CONTACT_ID"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@AffinityGroupContactId", DataAccessParameterType.Numeric, this.AffinityGroupContactId.ToString());
			data.ExecuteProcedure("AFFINITY_GROUP_CONTACT_GetRecord");
			if (!data.EOF)
			{
				this.AffinityGroupContactId = int.Parse(data["AffinityGroupContactId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.AffinityGroupId = int.Parse(data["AffinityGroupId"]);
				this.AffinityGroupBranchId = int.Parse(data["AffinityGroupBranchId"]);
				this.FirstName = data["FirstName"];
				this.LastName = data["LastName"];
				this.Initials = data["Initials"];
				this.JobTitle = data["JobTitle"];
				this.DateOfBirth = data["DateOfBirth"];
				this.AddressId = int.Parse(data["AddressId"]);
				this.Email = data["Email"];
				this.UserId = data["UserId"];
				this.IsProductMail = bool.Parse(data["IsProductMail"]);
				this.IsLoyaltyWorks = bool.Parse(data["IsLoyaltyWorks"]);
				this.IsLoyaltyWorksStatements = bool.Parse(data["IsLoyaltyWorksStatements"]);
				this.IsLoyaltyWorksMailhome = bool.Parse(data["IsLoyaltyWorksMailhome"]);
				this.IsLoanOfficer = bool.Parse(data["IsLoanOfficer"]);
				this.NoteId = int.Parse(data["NoteId"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int affinityGroupContactId = this.AffinityGroupContactId;
			data.AddParam("@AffinityGroupContactId", DataAccessParameterType.Numeric, affinityGroupContactId.ToString());
			affinityGroupContactId = this.AffinityGroupId;
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, affinityGroupContactId.ToString());
			affinityGroupContactId = this.AffinityGroupBranchId;
			data.AddParam("@AffinityGroupBranchId", DataAccessParameterType.Numeric, affinityGroupContactId.ToString());
			data.AddParam("@FirstName", DataAccessParameterType.Text, this.FirstName);
			data.AddParam("@LastName", DataAccessParameterType.Text, this.LastName);
			data.AddParam("@Initials", DataAccessParameterType.Text, this.Initials);
			data.AddParam("@JobTitle", DataAccessParameterType.Text, this.JobTitle);
			data.AddParam("@DateOfBirth", DataAccessParameterType.DateTime, this.DateOfBirth);
			affinityGroupContactId = this.AddressId;
			data.AddParam("@AddressId", DataAccessParameterType.Numeric, affinityGroupContactId.ToString());
			data.AddParam("@Email", DataAccessParameterType.Text, this.Email);
			data.AddParam("@UserId", DataAccessParameterType.Text, this.UserId);
			bool isProductMail = this.IsProductMail;
			data.AddParam("@IsProductMail", DataAccessParameterType.Bool, isProductMail.ToString());
			isProductMail = this.IsLoyaltyWorks;
			data.AddParam("@IsLoyaltyWorks", DataAccessParameterType.Bool, isProductMail.ToString());
			isProductMail = this.IsLoyaltyWorksStatements;
			data.AddParam("@IsLoyaltyWorksStatements", DataAccessParameterType.Bool, isProductMail.ToString());
			isProductMail = this.IsLoyaltyWorksMailhome;
			data.AddParam("@IsLoyaltyWorksMailhome", DataAccessParameterType.Bool, isProductMail.ToString());
			isProductMail = this.IsLoanOfficer;
			data.AddParam("@IsLoanOfficer", DataAccessParameterType.Bool, isProductMail.ToString());
			affinityGroupContactId = this.NoteId;
			data.AddParam("@NoteId", DataAccessParameterType.Numeric, affinityGroupContactId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("AFFINITY_GROUP_CONTACT_InsertUpdate");
			if (!data.EOF)
			{
				this.AffinityGroupContactId = int.Parse(data["AffinityGroupContactId"]);
			}
			this.retrievedata();
		}
	}
}