using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class ProductMbpModel : Transaction
	{
		private int productMbpModelId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int productMbpMakeId = 0;

		private string modelName = "";

		private string classNew = "";

		private string classUsed = "";

		private string mbpVsc = "";

		private bool isActive = false;

		public string ClassNew
		{
			get
			{
				return this.classNew;
			}
			set
			{
				this.classNew = value;
			}
		}

		public string ClassUsed
		{
			get
			{
				return this.classUsed;
			}
			set
			{
				this.classUsed = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public bool IsActive
		{
			get
			{
				return this.isActive;
			}
			set
			{
				this.isActive = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public string MbpVsc
		{
			get
			{
				return this.mbpVsc;
			}
			set
			{
				this.mbpVsc = value;
			}
		}

		public string ModelName
		{
			get
			{
				return this.modelName;
			}
			set
			{
				this.modelName = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public int ProductMbpMakeId
		{
			get
			{
				return this.productMbpMakeId;
			}
			set
			{
				this.productMbpMakeId = value;
			}
		}

		public int ProductMbpModelId
		{
			get
			{
				return this.productMbpModelId;
			}
			set
			{
				this.productMbpModelId = value;
			}
		}

		public ProductMbpModel()
		{
		}

		public ProductMbpModel(string connection, string modifiedUserID, int productMbpModelId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.ProductMbpModelId = productMbpModelId;
			if (this.ProductMbpModelId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ProductMbpModelId", DataAccessParameterType.Numeric, this.ProductMbpModelId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("PRODUCT_MBP_MODEL_Delete");
			this.wipeout();
		}

		public static int GetRecordId(string connection, string modelName, int productMbpMakeId)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@ModelName", DataAccessParameterType.Text, modelName);
			data.AddParam("@ProductMbpMakeId", DataAccessParameterType.Numeric, productMbpMakeId.ToString());
			data.ExecuteProcedure("PRODUCT_MBP_MODEL_GetRecord");
			num = (!data.EOF ? int.Parse(data["ProductMbpModelId"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ProductMbpModelId", DataAccessParameterType.Numeric, this.ProductMbpModelId.ToString());
			data.ExecuteProcedure("PRODUCT_MBP_MODEL_GetRecord");
			if (!data.EOF)
			{
				this.ProductMbpModelId = int.Parse(data["ProductMbpModelId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.ProductMbpMakeId = int.Parse(data["ProductMbpMakeId"]);
				this.ModelName = data["ModelName"];
				this.ClassNew = data["ClassNew"];
				this.ClassUsed = data["ClassUsed"];
				this.MbpVsc = data["MbpVsc"];
				if (Validation.IsNumeric(data["IsActive"]))
				{
					this.IsActive = Convert.ToInt32(data["IsActive"]) > 0;
				}
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int productMbpModelId = this.ProductMbpModelId;
			data.AddParam("@ProductMbpModelId", DataAccessParameterType.Numeric, productMbpModelId.ToString());
			productMbpModelId = this.ProductMbpMakeId;
			data.AddParam("@ProductMbpMakeId", DataAccessParameterType.Numeric, productMbpModelId.ToString());
			data.AddParam("@ModelName", DataAccessParameterType.Text, this.ModelName);
			data.AddParam("@ClassNew", DataAccessParameterType.Text, this.ClassNew);
			data.AddParam("@ClassUsed", DataAccessParameterType.Text, this.ClassUsed);
			data.AddParam("@MbpVsc", DataAccessParameterType.Text, this.MbpVsc);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.AddParam("@IsActive", DataAccessParameterType.Numeric, (this.IsActive ? "1" : "0"));
			data.ExecuteProcedure("PRODUCT_MBP_MODEL_InsertUpdate");
			if (!data.EOF)
			{
				this.ProductMbpModelId = int.Parse(data["ProductMbpModelId"]);
			}
			this.retrievedata();
		}
	}
}