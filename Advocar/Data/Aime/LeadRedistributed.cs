using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class LeadRedistributed : Transaction
	{
		private int leadRedistributedId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int leadId = 0;

		private bool isDistributedToDealer = false;

		private int dealerId = 0;

		private bool isDistributedToConsultant = false;

		private int cars_EmplId = 0;

		private string notes = "";

		public int Cars_EmplId
		{
			get
			{
				return this.cars_EmplId;
			}
			set
			{
				this.cars_EmplId = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public int DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public bool IsDistributedToConsultant
		{
			get
			{
				return this.isDistributedToConsultant;
			}
			set
			{
				this.isDistributedToConsultant = value;
			}
		}

		public bool IsDistributedToDealer
		{
			get
			{
				return this.isDistributedToDealer;
			}
			set
			{
				this.isDistributedToDealer = value;
			}
		}

		public int LeadId
		{
			get
			{
				return this.leadId;
			}
			set
			{
				this.leadId = value;
			}
		}

		public int LeadRedistributedId
		{
			get
			{
				return this.leadRedistributedId;
			}
			set
			{
				this.leadRedistributedId = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public string Notes
		{
			get
			{
				return this.notes;
			}
			set
			{
				this.notes = value;
			}
		}

		public LeadRedistributed()
		{
		}

		public LeadRedistributed(string connection, string modifiedUserID, int leadRedistributedId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "LEAD_REDISTRIBUTED";
			this.LeadRedistributedId = leadRedistributedId;
			if (this.LeadRedistributedId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@LeadRedistributedId", DataAccessParameterType.Numeric, this.LeadRedistributedId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("LEAD_REDISTRIBUTED_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@LeadRedistributedId", DataAccessParameterType.Numeric, this.LeadRedistributedId.ToString());
			data.ExecuteProcedure("LEAD_REDISTRIBUTED_GetRecord");
			if (!data.EOF)
			{
				this.LeadRedistributedId = int.Parse(data["LeadRedistributedId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.LeadId = int.Parse(data["LeadId"]);
				this.IsDistributedToDealer = bool.Parse(data["IsDistributedToDealer"]);
				this.DealerId = int.Parse(data["DealerId"]);
				this.IsDistributedToConsultant = bool.Parse(data["IsDistributedToConsultant"]);
				this.Cars_EmplId = int.Parse(data["Cars_EmplId"]);
				this.Notes = data["Notes"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int leadRedistributedId = this.LeadRedistributedId;
			data.AddParam("@LeadRedistributedId", DataAccessParameterType.Numeric, leadRedistributedId.ToString());
			leadRedistributedId = this.LeadId;
			data.AddParam("@LeadId", DataAccessParameterType.Numeric, leadRedistributedId.ToString());
			bool isDistributedToDealer = this.IsDistributedToDealer;
			data.AddParam("@IsDistributedToDealer", DataAccessParameterType.Bool, isDistributedToDealer.ToString());
			leadRedistributedId = this.DealerId;
			data.AddParam("@DealerId", DataAccessParameterType.Numeric, leadRedistributedId.ToString());
			isDistributedToDealer = this.IsDistributedToConsultant;
			data.AddParam("@IsDistributedToConsultant", DataAccessParameterType.Bool, isDistributedToDealer.ToString());
			leadRedistributedId = this.Cars_EmplId;
			data.AddParam("@Cars_EmplId", DataAccessParameterType.Numeric, leadRedistributedId.ToString());
			data.AddParam("@Notes", DataAccessParameterType.Text, this.Notes);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("LEAD_REDISTRIBUTED_InsertUpdate");
			if (!data.EOF)
			{
				this.LeadRedistributedId = int.Parse(data["LeadRedistributedId"]);
			}
			this.retrievedata();
		}
	}
}