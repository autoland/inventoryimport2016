using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class SalesDivisionQuota : Transaction
	{
		private int salesDivisionQuotaId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int companyDivisionId = 0;

		private int companyRegionId = 0;

		private string monthYear = "";

		private int headCount = 0;

		private int salesNew = 0;

		private int salesUsed = 0;

		private int salesMbp = 0;

		private int salesSealant = 0;

		private int salesTradeIn = 0;

		public int CompanyDivisionId
		{
			get
			{
				return this.companyDivisionId;
			}
			set
			{
				this.companyDivisionId = value;
			}
		}

		public int CompanyRegionId
		{
			get
			{
				return this.companyRegionId;
			}
			set
			{
				this.companyRegionId = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public int HeadCount
		{
			get
			{
				return this.headCount;
			}
			set
			{
				this.headCount = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public string MonthYear
		{
			get
			{
				return this.monthYear;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.monthYear = "";
				}
				else
				{
					this.monthYear = DateTime.Parse(value).ToString();
				}
			}
		}

		public int SalesDivisionQuotaId
		{
			get
			{
				return this.salesDivisionQuotaId;
			}
			set
			{
				this.salesDivisionQuotaId = value;
			}
		}

		public int SalesMbp
		{
			get
			{
				return this.salesMbp;
			}
			set
			{
				this.salesMbp = value;
			}
		}

		public int SalesNew
		{
			get
			{
				return this.salesNew;
			}
			set
			{
				this.salesNew = value;
			}
		}

		public int SalesSealant
		{
			get
			{
				return this.salesSealant;
			}
			set
			{
				this.salesSealant = value;
			}
		}

		public int SalesTradeIn
		{
			get
			{
				return this.salesTradeIn;
			}
			set
			{
				this.salesTradeIn = value;
			}
		}

		public int SalesUsed
		{
			get
			{
				return this.salesUsed;
			}
			set
			{
				this.salesUsed = value;
			}
		}

		public SalesDivisionQuota()
		{
		}

		public SalesDivisionQuota(string connection, string modifiedUserID, int salesDivisionQuotaId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.SalesDivisionQuotaId = salesDivisionQuotaId;
			if (this.SalesDivisionQuotaId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@SalesDivisionQuotaId", DataAccessParameterType.Numeric, this.SalesDivisionQuotaId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("SALES_DIVISION_QUOTA_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@SalesDivisionQuotaId", DataAccessParameterType.Numeric, this.SalesDivisionQuotaId.ToString());
			data.ExecuteProcedure("SALES_DIVISION_QUOTA_GetRecord");
			if (!data.EOF)
			{
				this.SalesDivisionQuotaId = int.Parse(data["SalesDivisionQuotaId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.CompanyDivisionId = int.Parse(data["CompanyDivisionId"]);
				this.CompanyRegionId = int.Parse(data["CompanyRegionId"]);
				this.MonthYear = data["MonthYear"];
				this.HeadCount = int.Parse(data["HeadCount"]);
				this.SalesNew = int.Parse(data["SalesNew"]);
				this.SalesUsed = int.Parse(data["SalesUsed"]);
				this.SalesMbp = int.Parse(data["SalesMbp"]);
				this.SalesSealant = int.Parse(data["SalesSealant"]);
				this.SalesTradeIn = int.Parse(data["SalesTradeIn"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int salesDivisionQuotaId = this.SalesDivisionQuotaId;
			data.AddParam("@SalesDivisionQuotaId", DataAccessParameterType.Numeric, salesDivisionQuotaId.ToString());
			salesDivisionQuotaId = this.CompanyDivisionId;
			data.AddParam("@CompanyDivisionId", DataAccessParameterType.Numeric, salesDivisionQuotaId.ToString());
			salesDivisionQuotaId = this.CompanyRegionId;
			data.AddParam("@CompanyRegionId", DataAccessParameterType.Numeric, salesDivisionQuotaId.ToString());
			data.AddParam("@MonthYear", DataAccessParameterType.DateTime, this.MonthYear);
			salesDivisionQuotaId = this.HeadCount;
			data.AddParam("@HeadCount", DataAccessParameterType.Numeric, salesDivisionQuotaId.ToString());
			salesDivisionQuotaId = this.SalesNew;
			data.AddParam("@SalesNew", DataAccessParameterType.Numeric, salesDivisionQuotaId.ToString());
			salesDivisionQuotaId = this.SalesUsed;
			data.AddParam("@SalesUsed", DataAccessParameterType.Numeric, salesDivisionQuotaId.ToString());
			salesDivisionQuotaId = this.SalesMbp;
			data.AddParam("@SalesMbp", DataAccessParameterType.Numeric, salesDivisionQuotaId.ToString());
			salesDivisionQuotaId = this.SalesSealant;
			data.AddParam("@SalesSealant", DataAccessParameterType.Numeric, salesDivisionQuotaId.ToString());
			salesDivisionQuotaId = this.SalesTradeIn;
			data.AddParam("@SalesTradeIn", DataAccessParameterType.Numeric, salesDivisionQuotaId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("SALES_DIVISION_QUOTA_InsertUpdate");
			if (!data.EOF)
			{
				this.SalesDivisionQuotaId = int.Parse(data["SalesDivisionQuotaId"]);
			}
			this.retrievedata();
		}
	}
}