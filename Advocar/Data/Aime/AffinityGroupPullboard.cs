using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Aime
{
	public class AffinityGroupPullboard : Transaction
	{
		private int affinityGroupPullboardId = 0;

		private int affinityGroupId = 0;

		private string startDate = "";

		private string endDate = "";

		private int noOfReferralsForPull = 0;

		private bool isActive = false;

		private bool isDeleted = false;

		private string modifiedDate = "";

		private string modifiedUser = "";

		private string createdDate = "";

		private string createdUser = "";

		public int AffinityGroupId
		{
			get
			{
				return this.affinityGroupId;
			}
			set
			{
				this.affinityGroupId = value;
			}
		}

		public int AffinityGroupPullboardId
		{
			get
			{
				return this.affinityGroupPullboardId;
			}
			set
			{
				this.affinityGroupPullboardId = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string EndDate
		{
			get
			{
				return this.endDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.endDate = "";
				}
				else
				{
					this.endDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public bool IsActive
		{
			get
			{
				return this.isActive;
			}
			set
			{
				this.isActive = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public int NoOfReferralsForPull
		{
			get
			{
				return this.noOfReferralsForPull;
			}
			set
			{
				this.noOfReferralsForPull = value;
			}
		}

		public string StartDate
		{
			get
			{
				return this.startDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.startDate = "";
				}
				else
				{
					this.startDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public AffinityGroupPullboard()
		{
		}

		public AffinityGroupPullboard(string connection, string modifiedUserID, int affinityGroupPullboardId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.AffinityGroupPullboardId = affinityGroupPullboardId;
			if (this.AffinityGroupPullboardId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@AffinityGroupPullboardId", DataAccessParameterType.Numeric, this.AffinityGroupPullboardId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("AFFINITY_GROUP_PULLBOARD_Delete");
			this.wipeout();
		}

		public static int GetRecordId(int affinityGroupId, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, affinityGroupId.ToString());
			data.ExecuteProcedure("AFFINITY_GROUP_PULLBOARD_GetRecord");
			num = (!data.EOF ? int.Parse(data["AffinityGroupPullboardId"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@AffinityGroupPullboardId", DataAccessParameterType.Numeric, this.AffinityGroupPullboardId.ToString());
			data.ExecuteProcedure("AFFINITY_GROUP_PULLBOARD_GetRecord");
			if (!data.EOF)
			{
				this.AffinityGroupPullboardId = int.Parse(data["AffinityGroupPullboardId"]);
				this.AffinityGroupId = int.Parse(data["AffinityGroupId"]);
				this.StartDate = data["StartDate"];
				this.EndDate = data["EndDate"];
				this.NoOfReferralsForPull = int.Parse(data["NoOfReferralsForPull"]);
				this.IsActive = bool.Parse(data["IsActive"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int affinityGroupPullboardId = this.AffinityGroupPullboardId;
			data.AddParam("@AffinityGroupPullboardId", DataAccessParameterType.Numeric, affinityGroupPullboardId.ToString());
			affinityGroupPullboardId = this.AffinityGroupId;
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, affinityGroupPullboardId.ToString());
			data.AddParam("@StartDate", DataAccessParameterType.DateTime, this.StartDate);
			data.AddParam("@EndDate", DataAccessParameterType.DateTime, this.EndDate);
			affinityGroupPullboardId = this.NoOfReferralsForPull;
			data.AddParam("@NoOfReferralsForPull", DataAccessParameterType.Numeric, affinityGroupPullboardId.ToString());
			data.AddParam("@IsActive", DataAccessParameterType.Bool, this.IsActive.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("AFFINITY_GROUP_PULLBOARD_InsertUpdate");
			if (!data.EOF)
			{
				this.AffinityGroupPullboardId = int.Parse(data["AffinityGroupPullboardId"]);
			}
			this.retrievedata();
		}
	}
}