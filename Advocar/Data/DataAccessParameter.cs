using System;

namespace Advocar.Data
{
	public class DataAccessParameter
	{
		private string name;

		private string @value;

		private DataAccessParameterType type;

		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		public DataAccessParameterType Type
		{
			get
			{
				return this.type;
			}
			set
			{
				this.type = value;
			}
		}

		public string Value
		{
			get
			{
				return this.@value;
			}
			set
			{
				this.@value = value;
			}
		}

		public DataAccessParameter()
		{
		}

		public DataAccessParameter(string name, string value, DataAccessParameterType type)
		{
			this.Name = name;
			this.Value = value;
			this.Type = type;
		}
	}
}