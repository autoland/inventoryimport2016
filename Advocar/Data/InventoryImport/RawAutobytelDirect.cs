using Advocar.Data;
using Advocar.Interface;
using System;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace Advocar.Data.InventoryImport
{
	public class RawAutobytelDirect : Transaction
	{
		private int rawAutobytelDirectId = 0;

		private int jobExecutionRawId = 0;

		private string vehicleID = "";

		private string dealerID = "";

		private string programID = "";

		private string vIN = "";

		private string dealerStock = "";

		private string year = "";

		private string make = "";

		private string model = "";

		private string series = "";

		private string price = "";

		private string mileage = "";

		private string doors = "";

		private string cylinders = "";

		private string transmission = "";

		private string interiorColor = "";

		private string exteriorColor = "";

		private string imageURL = "";

		private string features = "";

		private DataTable destTable = new DataTable();

		private SqlDataAdapter dataAdapter = new SqlDataAdapter();

		public string Cylinders
		{
			get
			{
				return this.cylinders;
			}
			set
			{
				this.cylinders = value;
			}
		}

		public string DealerID
		{
			get
			{
				return this.dealerID;
			}
			set
			{
				this.dealerID = value;
			}
		}

		public string DealerStock
		{
			get
			{
				return this.dealerStock;
			}
			set
			{
				this.dealerStock = value;
			}
		}

		public string Doors
		{
			get
			{
				return this.doors;
			}
			set
			{
				this.doors = value;
			}
		}

		public string ExteriorColor
		{
			get
			{
				return this.exteriorColor;
			}
			set
			{
				this.exteriorColor = value;
			}
		}

		public string Features
		{
			get
			{
				return this.features;
			}
			set
			{
				this.features = value;
			}
		}

		public string ImageURL
		{
			get
			{
				return this.imageURL;
			}
			set
			{
				this.imageURL = value;
			}
		}

		public string InteriorColor
		{
			get
			{
				return this.interiorColor;
			}
			set
			{
				this.interiorColor = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public string Make
		{
			get
			{
				return this.make;
			}
			set
			{
				this.make = value;
			}
		}

		public string Mileage
		{
			get
			{
				return this.mileage;
			}
			set
			{
				this.mileage = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public string Price
		{
			get
			{
				return this.price;
			}
			set
			{
				this.price = value;
			}
		}

		public string ProgramID
		{
			get
			{
				return this.programID;
			}
			set
			{
				this.programID = value;
			}
		}

		public int RawAutobytelDirectId
		{
			get
			{
				return this.rawAutobytelDirectId;
			}
			set
			{
				this.rawAutobytelDirectId = value;
			}
		}

		public string Series
		{
			get
			{
				return this.series;
			}
			set
			{
				this.series = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string VehicleID
		{
			get
			{
				return this.vehicleID;
			}
			set
			{
				this.vehicleID = value;
			}
		}

		public string VIN
		{
			get
			{
				return this.vIN;
			}
			set
			{
				this.vIN = value;
			}
		}

		public string Year
		{
			get
			{
				return this.year;
			}
			set
			{
				this.year = value;
			}
		}

		public RawAutobytelDirect()
		{
		}

		public RawAutobytelDirect(string connection, string modifiedUserID, int rawAutobytelDirectId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "RAW_AUTOBYTEL_DIRECT";
			this.RawAutobytelDirectId = rawAutobytelDirectId;
			if (this.RawAutobytelDirectId > 0)
			{
				this.retrievedata();
			}
		}

		public void BuilInsertCommit()
		{
			SqlConnection connect = new SqlConnection(this.connection);
			connect.Open();
			SqlBulkCopy bulkCopy = new SqlBulkCopy(connect)
			{
				DestinationTableName = this.databaseObjectName,
				BulkCopyTimeout = 2400
			};
			bulkCopy.WriteToServer(this.destTable);
			bulkCopy.Close();
			GC.Collect();
			this.destTable = new DataTable();
			this.dataAdapter = new SqlDataAdapter();
		}

		public void BulkInsertSave()
		{
			DataRow destRow = this.destTable.NewRow();
			destRow["JOB_EXECUTION_RAW_ID"] = this.JobExecutionRawId;
			destRow["VehicleID"] = this.VehicleID;
			destRow["DealerID"] = this.DealerID;
			destRow["ProgramID"] = this.ProgramID;
			destRow["VIN"] = this.VIN;
			destRow["DealerStock"] = this.DealerStock;
			destRow["Year"] = this.Year;
			destRow["Make"] = this.Make;
			destRow["Model"] = this.Model;
			destRow["Series"] = this.Series;
			destRow["Price"] = this.Price;
			destRow["Mileage"] = this.Mileage;
			destRow["Doors"] = this.Doors;
			destRow["Cylinders"] = this.Cylinders;
			destRow["Transmission"] = this.Transmission;
			destRow["InteriorColor"] = this.InteriorColor;
			destRow["ExteriorColor"] = this.ExteriorColor;
			destRow["ImageURL"] = this.ImageURL;
			destRow["Features"] = this.Features;
			this.destTable.Rows.Add(destRow);
		}

		public void BulkInsertStart()
		{
			this.destTable = new DataTable(this.databaseObjectName);
			this.dataAdapter = new SqlDataAdapter(string.Concat("SELECT * FROM ", this.databaseObjectName, " WHERE 1=2"), this.connection);
			this.dataAdapter.Fill(this.destTable);
			this.dataAdapter.Dispose();
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawAutobytelDirectId", DataAccessParameterType.Numeric, this.RawAutobytelDirectId.ToString());
			data.ExecuteProcedure("RAW_AUTOBYTEL_DIRECT_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawAutobytelDirectId", DataAccessParameterType.Numeric, this.RawAutobytelDirectId.ToString());
			data.ExecuteProcedure("RAW_AUTOBYTEL_DIRECT_GetRecord");
			if (!data.EOF)
			{
				this.RawAutobytelDirectId = int.Parse(data["RawAutobytelDirectId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.VehicleID = data["VehicleID"];
				this.DealerID = data["DealerID"];
				this.ProgramID = data["ProgramID"];
				this.VIN = data["VIN"];
				this.DealerStock = data["DealerStock"];
				this.Year = data["Year"];
				this.Make = data["Make"];
				this.Model = data["Model"];
				this.Series = data["Series"];
				this.Price = data["Price"];
				this.Mileage = data["Mileage"];
				this.Doors = data["Doors"];
				this.Cylinders = data["Cylinders"];
				this.Transmission = data["Transmission"];
				this.InteriorColor = data["InteriorColor"];
				this.ExteriorColor = data["ExteriorColor"];
				this.ImageURL = data["ImageURL"];
				this.Features = data["Features"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawAutobytelDirectId = this.RawAutobytelDirectId;
			data.AddParam("@RawAutobytelDirectId", DataAccessParameterType.Numeric, rawAutobytelDirectId.ToString());
			rawAutobytelDirectId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawAutobytelDirectId.ToString());
			data.AddParam("@VehicleID", DataAccessParameterType.Text, this.VehicleID);
			data.AddParam("@DealerID", DataAccessParameterType.Text, this.DealerID);
			data.AddParam("@ProgramID", DataAccessParameterType.Text, this.ProgramID);
			data.AddParam("@VIN", DataAccessParameterType.Text, this.VIN);
			data.AddParam("@DealerStock", DataAccessParameterType.Text, this.DealerStock);
			data.AddParam("@Year", DataAccessParameterType.Text, this.Year);
			data.AddParam("@Make", DataAccessParameterType.Text, this.Make);
			data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
			data.AddParam("@Series", DataAccessParameterType.Text, this.Series);
			data.AddParam("@Price", DataAccessParameterType.Text, this.Price);
			data.AddParam("@Mileage", DataAccessParameterType.Text, this.Mileage);
			data.AddParam("@Doors", DataAccessParameterType.Text, this.Doors);
			data.AddParam("@Cylinders", DataAccessParameterType.Text, this.Cylinders);
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			data.AddParam("@InteriorColor", DataAccessParameterType.Text, this.InteriorColor);
			data.AddParam("@ExteriorColor", DataAccessParameterType.Text, this.ExteriorColor);
			data.AddParam("@ImageURL", DataAccessParameterType.Text, this.ImageURL);
			data.AddParam("@Features", DataAccessParameterType.Text, this.Features);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("RAW_AUTOBYTEL_DIRECT_InsertUpdate");
			if (!data.EOF)
			{
				this.RawAutobytelDirectId = int.Parse(data["RawAutobytelDirectId"]);
			}
			this.retrievedata();
		}
	}
}