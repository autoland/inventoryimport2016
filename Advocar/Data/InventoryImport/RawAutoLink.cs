using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawAutoLink : Transaction
	{
		private int rawAutoLinkId = 0;

		private int jobExecutionRawId = 0;

		private string dealerID = "";

		private string stockID = "";

		private int year = 0;

		private string vIN = "";

		private string newUsed = "";

		private string make = "";

		private string model = "";

		private string series = "";

		private string modelNumber = "";

		private string body = "";

		private int mileage = 0;

		private string extColor = "";

		private string intColor = "";

		private string transmission = "";

		private int doors = 0;

		private float askingPrice = 0f;

		private float mSRP = 0f;

		private float invoice = 0f;

		private string dateEntered = "";

		private string certifiedYesNo = "";

		private string engine = "";

		private string fuelCode = "";

		private string driveTrain = "";

		private string restraint = "";

		private string dealerComments = "";

		private string vehicleComments = "";

		private string imageUrls = "";

		private string packageCode = "";

		private string options = "";

		public float AskingPrice
		{
			get
			{
				return this.askingPrice;
			}
			set
			{
				this.askingPrice = value;
			}
		}

		public string Body
		{
			get
			{
				return this.body;
			}
			set
			{
				this.body = value;
			}
		}

		public string CertifiedYesNo
		{
			get
			{
				return this.certifiedYesNo;
			}
			set
			{
				this.certifiedYesNo = value;
			}
		}

		public string DateEntered
		{
			get
			{
				return this.dateEntered;
			}
			set
			{
				this.dateEntered = value;
			}
		}

		public string DealerComments
		{
			get
			{
				return this.dealerComments;
			}
			set
			{
				this.dealerComments = value;
			}
		}

		public string DealerID
		{
			get
			{
				return this.dealerID;
			}
			set
			{
				this.dealerID = value;
			}
		}

		public int Doors
		{
			get
			{
				return this.doors;
			}
			set
			{
				this.doors = value;
			}
		}

		public string DriveTrain
		{
			get
			{
				return this.driveTrain;
			}
			set
			{
				this.driveTrain = value;
			}
		}

		public string Engine
		{
			get
			{
				return this.engine;
			}
			set
			{
				this.engine = value;
			}
		}

		public string ExtColor
		{
			get
			{
				return this.extColor;
			}
			set
			{
				this.extColor = value;
			}
		}

		public string FuelCode
		{
			get
			{
				return this.fuelCode;
			}
			set
			{
				this.fuelCode = value;
			}
		}

		public string ImageUrls
		{
			get
			{
				return this.imageUrls;
			}
			set
			{
				this.imageUrls = value;
			}
		}

		public string IntColor
		{
			get
			{
				return this.intColor;
			}
			set
			{
				this.intColor = value;
			}
		}

		public float Invoice
		{
			get
			{
				return this.invoice;
			}
			set
			{
				this.invoice = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public string Make
		{
			get
			{
				return this.make;
			}
			set
			{
				this.make = value;
			}
		}

		public int Mileage
		{
			get
			{
				return this.mileage;
			}
			set
			{
				this.mileage = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public string ModelNumber
		{
			get
			{
				return this.modelNumber;
			}
			set
			{
				this.modelNumber = value;
			}
		}

		public float MSRP
		{
			get
			{
				return this.mSRP;
			}
			set
			{
				this.mSRP = value;
			}
		}

		public string NewUsed
		{
			get
			{
				return this.newUsed;
			}
			set
			{
				this.newUsed = value;
			}
		}

		public string Options
		{
			get
			{
				return this.options;
			}
			set
			{
				this.options = value;
			}
		}

		public string PackageCode
		{
			get
			{
				return this.packageCode;
			}
			set
			{
				this.packageCode = value;
			}
		}

		public int RawAutoLinkId
		{
			get
			{
				return this.rawAutoLinkId;
			}
			set
			{
				this.rawAutoLinkId = value;
			}
		}

		public string Restraint
		{
			get
			{
				return this.restraint;
			}
			set
			{
				this.restraint = value;
			}
		}

		public string Series
		{
			get
			{
				return this.series;
			}
			set
			{
				this.series = value;
			}
		}

		public string StockID
		{
			get
			{
				return this.stockID;
			}
			set
			{
				this.stockID = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string VehicleComments
		{
			get
			{
				return this.vehicleComments;
			}
			set
			{
				this.vehicleComments = value;
			}
		}

		public string VIN
		{
			get
			{
				return this.vIN;
			}
			set
			{
				this.vIN = value;
			}
		}

		public int Year
		{
			get
			{
				return this.year;
			}
			set
			{
				this.year = value;
			}
		}

		public RawAutoLink()
		{
		}

		public RawAutoLink(string connection, string modifiedUserID, int rawAutoLinkId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.RawAutoLinkId = rawAutoLinkId;
			this.databaseObjectName = "RAW_AUTO_LINK";
			if (this.RawAutoLinkId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawAutoLinkId", DataAccessParameterType.Numeric, this.RawAutoLinkId.ToString());
			data.ExecuteProcedure("RAW_AUTO_LINK_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawAutoLinkId", DataAccessParameterType.Numeric, this.RawAutoLinkId.ToString());
			data.ExecuteProcedure("RAW_AUTO_LINK_GetRecord");
			if (!data.EOF)
			{
				this.RawAutoLinkId = int.Parse(data["RawAutoLinkId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerID = data["DealerID"];
				this.StockID = data["StockID"];
				this.Year = int.Parse(data["Year"]);
				this.VIN = data["VIN"];
				this.NewUsed = data["NewUsed"];
				this.Make = data["Make"];
				this.Model = data["Model"];
				this.Series = data["Series"];
				this.ModelNumber = data["ModelNumber"];
				this.Body = data["Body"];
				this.Mileage = int.Parse(data["Mileage"]);
				this.ExtColor = data["ExtColor"];
				this.IntColor = data["IntColor"];
				this.Transmission = data["Transmission"];
				this.Doors = int.Parse(data["Doors"]);
				this.AskingPrice = float.Parse(data["AskingPrice"]);
				this.MSRP = float.Parse(data["MSRP"]);
				this.Invoice = float.Parse(data["Invoice"]);
				this.DateEntered = data["DateEntered"];
				this.CertifiedYesNo = data["CertifiedYesNo"];
				this.Engine = data["Engine"];
				this.FuelCode = data["FuelCode"];
				this.DriveTrain = data["DriveTrain"];
				this.Restraint = data["Restraint"];
				this.DealerComments = data["DealerComments"];
				this.VehicleComments = data["VehicleComments"];
				this.ImageUrls = data["ImageUrls"];
				this.PackageCode = data["PackageCode"];
				this.Options = data["Options"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawAutoLinkId = this.RawAutoLinkId;
			data.AddParam("@RawAutoLinkId", DataAccessParameterType.Numeric, rawAutoLinkId.ToString());
			rawAutoLinkId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawAutoLinkId.ToString());
			data.AddParam("@DealerID", DataAccessParameterType.Text, this.DealerID);
			data.AddParam("@StockID", DataAccessParameterType.Text, this.StockID);
			rawAutoLinkId = this.Year;
			data.AddParam("@Year", DataAccessParameterType.Numeric, rawAutoLinkId.ToString());
			data.AddParam("@VIN", DataAccessParameterType.Text, this.VIN);
			data.AddParam("@NewUsed", DataAccessParameterType.Text, this.NewUsed);
			data.AddParam("@Make", DataAccessParameterType.Text, this.Make);
			data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
			data.AddParam("@Series", DataAccessParameterType.Text, this.Series);
			data.AddParam("@ModelNumber", DataAccessParameterType.Text, this.ModelNumber);
			data.AddParam("@Body", DataAccessParameterType.Text, this.Body);
			rawAutoLinkId = this.Mileage;
			data.AddParam("@Mileage", DataAccessParameterType.Numeric, rawAutoLinkId.ToString());
			data.AddParam("@ExtColor", DataAccessParameterType.Text, this.ExtColor);
			data.AddParam("@IntColor", DataAccessParameterType.Text, this.IntColor);
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			rawAutoLinkId = this.Doors;
			data.AddParam("@Doors", DataAccessParameterType.Numeric, rawAutoLinkId.ToString());
			float askingPrice = this.AskingPrice;
			data.AddParam("@AskingPrice", DataAccessParameterType.Numeric, askingPrice.ToString());
			askingPrice = this.MSRP;
			data.AddParam("@MSRP", DataAccessParameterType.Numeric, askingPrice.ToString());
			askingPrice = this.Invoice;
			data.AddParam("@Invoice", DataAccessParameterType.Numeric, askingPrice.ToString());
			data.AddParam("@DateEntered", DataAccessParameterType.Text, this.DateEntered);
			data.AddParam("@CertifiedYesNo", DataAccessParameterType.Text, this.CertifiedYesNo);
			data.AddParam("@Engine", DataAccessParameterType.Text, this.Engine);
			data.AddParam("@FuelCode", DataAccessParameterType.Text, this.FuelCode);
			data.AddParam("@DriveTrain", DataAccessParameterType.Text, this.DriveTrain);
			data.AddParam("@Restraint", DataAccessParameterType.Text, this.Restraint);
			data.AddParam("@DealerComments", DataAccessParameterType.Text, this.DealerComments);
			data.AddParam("@VehicleComments", DataAccessParameterType.Text, this.VehicleComments);
			data.AddParam("@ImageUrls", DataAccessParameterType.Text, this.ImageUrls);
			data.AddParam("@PackageCode", DataAccessParameterType.Text, this.PackageCode);
			data.AddParam("@Options", DataAccessParameterType.Text, this.Options);
			data.ExecuteProcedure("RAW_AUTO_LINK_InsertUpdate");
			if (!data.EOF)
			{
				this.RawAutoLinkId = int.Parse(data["RawAutoLinkId"]);
			}
			this.retrievedata();
		}
	}
}