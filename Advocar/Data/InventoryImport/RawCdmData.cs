using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawCdmData : Transaction
	{
		private int rawCdmDataId = 0;

		private int jobExecutionRawId = 0;

		private string dealerID = "";

		private string vIN = "";

		private string stockID = "";

		private int mileage = 0;

		private int year = 0;

		private string make = "";

		private string model = "";

		private string body = "";

		private string transmission = "";

		private string engine = "";

		private string driveTrain = "";

		private string intColor = "";

		private string extColor = "";

		private float webPrice = 0f;

		private float askingPrice = 0f;

		private float currentCostPrice = 0f;

		private string certified = "";

		private string isUsed = "";

		private string bookInDate = "";

		private string comments = "";

		private string features = "";

		private string imageURLs = "";

		public float AskingPrice
		{
			get
			{
				return this.askingPrice;
			}
			set
			{
				this.askingPrice = value;
			}
		}

		public string Body
		{
			get
			{
				return this.body;
			}
			set
			{
				this.body = value;
			}
		}

		public string BookInDate
		{
			get
			{
				return this.bookInDate;
			}
			set
			{
				this.bookInDate = value;
			}
		}

		public string Certified
		{
			get
			{
				return this.certified;
			}
			set
			{
				this.certified = value;
			}
		}

		public string Comments
		{
			get
			{
				return this.comments;
			}
			set
			{
				this.comments = value;
			}
		}

		public float CurrentCostPrice
		{
			get
			{
				return this.currentCostPrice;
			}
			set
			{
				this.currentCostPrice = value;
			}
		}

		public string DealerID
		{
			get
			{
				return this.dealerID;
			}
			set
			{
				this.dealerID = value;
			}
		}

		public string DriveTrain
		{
			get
			{
				return this.driveTrain;
			}
			set
			{
				this.driveTrain = value;
			}
		}

		public string Engine
		{
			get
			{
				return this.engine;
			}
			set
			{
				this.engine = value;
			}
		}

		public string ExtColor
		{
			get
			{
				return this.extColor;
			}
			set
			{
				this.extColor = value;
			}
		}

		public string Features
		{
			get
			{
				return this.features;
			}
			set
			{
				this.features = value;
			}
		}

		public string ImageURLs
		{
			get
			{
				return this.imageURLs;
			}
			set
			{
				this.imageURLs = value;
			}
		}

		public string IntColor
		{
			get
			{
				return this.intColor;
			}
			set
			{
				this.intColor = value;
			}
		}

		public string IsUsed
		{
			get
			{
				return this.isUsed;
			}
			set
			{
				this.isUsed = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public string Make
		{
			get
			{
				return this.make;
			}
			set
			{
				this.make = value;
			}
		}

		public int Mileage
		{
			get
			{
				return this.mileage;
			}
			set
			{
				this.mileage = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public int RawCdmDataId
		{
			get
			{
				return this.rawCdmDataId;
			}
			set
			{
				this.rawCdmDataId = value;
			}
		}

		public string StockID
		{
			get
			{
				return this.stockID;
			}
			set
			{
				this.stockID = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string VIN
		{
			get
			{
				return this.vIN;
			}
			set
			{
				this.vIN = value;
			}
		}

		public float WebPrice
		{
			get
			{
				return this.webPrice;
			}
			set
			{
				this.webPrice = value;
			}
		}

		public int Year
		{
			get
			{
				return this.year;
			}
			set
			{
				this.year = value;
			}
		}

		public RawCdmData()
		{
		}

		public RawCdmData(string connection, string modifiedUserID, int rawCdmDataId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.RawCdmDataId = rawCdmDataId;
			this.databaseObjectName = "RAW_CDM_DATA";
			if (this.RawCdmDataId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawCdmDataId", DataAccessParameterType.Numeric, this.RawCdmDataId.ToString());
			data.ExecuteProcedure("RAW_CDM_DATA_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawCdmDataId", DataAccessParameterType.Numeric, this.RawCdmDataId.ToString());
			data.ExecuteProcedure("RAW_CDM_DATA_GetRecord");
			if (!data.EOF)
			{
				this.RawCdmDataId = int.Parse(data["RawCdmDataId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerID = data["DealerID"];
				this.VIN = data["VIN"];
				this.StockID = data["StockID"];
				this.Mileage = int.Parse(data["Mileage"]);
				this.Year = int.Parse(data["Year"]);
				this.Make = data["Make"];
				this.Model = data["Model"];
				this.Body = data["Body"];
				this.Transmission = data["Transmission"];
				this.Engine = data["Engine"];
				this.DriveTrain = data["DriveTrain"];
				this.IntColor = data["IntColor"];
				this.ExtColor = data["ExtColor"];
				this.WebPrice = float.Parse(data["WebPrice"]);
				this.AskingPrice = float.Parse(data["AskingPrice"]);
				this.CurrentCostPrice = float.Parse(data["CurrentCostPrice"]);
				this.Certified = data["Certified"];
				this.IsUsed = data["IsUsed"];
				this.BookInDate = data["BookInDate"];
				this.Comments = data["Comments"];
				this.Features = data["Features"];
				this.ImageURLs = data["ImageURLs"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawCdmDataId = this.RawCdmDataId;
			data.AddParam("@RawCdmDataId", DataAccessParameterType.Numeric, rawCdmDataId.ToString());
			rawCdmDataId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawCdmDataId.ToString());
			data.AddParam("@DealerID", DataAccessParameterType.Text, this.DealerID);
			data.AddParam("@VIN", DataAccessParameterType.Text, this.VIN);
			data.AddParam("@StockID", DataAccessParameterType.Text, this.StockID);
			rawCdmDataId = this.Mileage;
			data.AddParam("@Mileage", DataAccessParameterType.Numeric, rawCdmDataId.ToString());
			rawCdmDataId = this.Year;
			data.AddParam("@Year", DataAccessParameterType.Numeric, rawCdmDataId.ToString());
			data.AddParam("@Make", DataAccessParameterType.Text, this.Make);
			data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
			data.AddParam("@Body", DataAccessParameterType.Text, this.Body);
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			data.AddParam("@Engine", DataAccessParameterType.Text, this.Engine);
			data.AddParam("@DriveTrain", DataAccessParameterType.Text, this.DriveTrain);
			data.AddParam("@IntColor", DataAccessParameterType.Text, this.IntColor);
			data.AddParam("@ExtColor", DataAccessParameterType.Text, this.ExtColor);
			float webPrice = this.WebPrice;
			data.AddParam("@WebPrice", DataAccessParameterType.Numeric, webPrice.ToString());
			webPrice = this.AskingPrice;
			data.AddParam("@AskingPrice", DataAccessParameterType.Numeric, webPrice.ToString());
			webPrice = this.CurrentCostPrice;
			data.AddParam("@CurrentCostPrice", DataAccessParameterType.Numeric, webPrice.ToString());
			data.AddParam("@Certified", DataAccessParameterType.Text, this.Certified);
			data.AddParam("@IsUsed", DataAccessParameterType.Text, this.IsUsed);
			data.AddParam("@BookInDate", DataAccessParameterType.Text, this.BookInDate);
			data.AddParam("@Comments", DataAccessParameterType.Text, this.Comments);
			data.AddParam("@Features", DataAccessParameterType.Text, this.Features);
			data.AddParam("@ImageURLs", DataAccessParameterType.Text, this.ImageURLs);
			data.ExecuteProcedure("RAW_CDM_DATA_InsertUpdate");
			if (!data.EOF)
			{
				this.RawCdmDataId = int.Parse(data["RawCdmDataId"]);
			}
			this.retrievedata();
		}
	}
}