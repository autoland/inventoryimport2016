using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawAutonexus : Transaction
	{
		private int rawAutonexusId = 0;

		private int jobExecutionRawId = 0;

		private string dealerId = "";

		private string stockNumber = "";

		private string vIN = "";

		private string year = "";

		private string make = "";

		private string model = "";

		private string trim = "";

		private string engine = "";

		private string transmission = "";

		private string doors = "";

		private string bodyStyle = "";

		private string mileage = "";

		private string exteriorColor = "";

		private string interiorColor = "";

		private string equipment = "";

		private string comments = "";

		private string sellingPrice = "";

		private string imageURLs = "";

		public string BodyStyle
		{
			get
			{
				return this.bodyStyle;
			}
			set
			{
				this.bodyStyle = value;
			}
		}

		public string Comments
		{
			get
			{
				return this.comments;
			}
			set
			{
				this.comments = value;
			}
		}

		public string DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public string Doors
		{
			get
			{
				return this.doors;
			}
			set
			{
				this.doors = value;
			}
		}

		public string Engine
		{
			get
			{
				return this.engine;
			}
			set
			{
				this.engine = value;
			}
		}

		public string Equipment
		{
			get
			{
				return this.equipment;
			}
			set
			{
				this.equipment = value;
			}
		}

		public string ExteriorColor
		{
			get
			{
				return this.exteriorColor;
			}
			set
			{
				this.exteriorColor = value;
			}
		}

		public string ImageURLs
		{
			get
			{
				return this.imageURLs;
			}
			set
			{
				this.imageURLs = value;
			}
		}

		public string InteriorColor
		{
			get
			{
				return this.interiorColor;
			}
			set
			{
				this.interiorColor = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public string Make
		{
			get
			{
				return this.make;
			}
			set
			{
				this.make = value;
			}
		}

		public string Mileage
		{
			get
			{
				return this.mileage;
			}
			set
			{
				this.mileage = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public int RawAutonexusId
		{
			get
			{
				return this.rawAutonexusId;
			}
			set
			{
				this.rawAutonexusId = value;
			}
		}

		public string SellingPrice
		{
			get
			{
				return this.sellingPrice;
			}
			set
			{
				this.sellingPrice = value;
			}
		}

		public string StockNumber
		{
			get
			{
				return this.stockNumber;
			}
			set
			{
				this.stockNumber = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string Trim
		{
			get
			{
				return this.trim;
			}
			set
			{
				this.trim = value;
			}
		}

		public string VIN
		{
			get
			{
				return this.vIN;
			}
			set
			{
				this.vIN = value;
			}
		}

		public string Year
		{
			get
			{
				return this.year;
			}
			set
			{
				this.year = value;
			}
		}

		public RawAutonexus()
		{
		}

		public RawAutonexus(string connection, string modifiedUserID, int rawAutonexusId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "RAW_AUTONEXUS";
			this.RawAutonexusId = rawAutonexusId;
			if (this.RawAutonexusId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawAutonexusId", DataAccessParameterType.Numeric, this.RawAutonexusId.ToString());
			data.ExecuteProcedure("RAW_AUTONEXUS_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawAutonexusId", DataAccessParameterType.Numeric, this.RawAutonexusId.ToString());
			data.ExecuteProcedure("RAW_AUTONEXUS_GetRecord");
			if (!data.EOF)
			{
				this.RawAutonexusId = int.Parse(data["RawAutonexusId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerId = data["DealerId"];
				this.StockNumber = data["StockNumber"];
				this.VIN = data["VIN"];
				this.Year = data["Year"];
				this.Make = data["Make"];
				this.Model = data["Model"];
				this.Trim = data["Trim"];
				this.Engine = data["Engine"];
				this.Transmission = data["Transmission"];
				this.Doors = data["Doors"];
				this.BodyStyle = data["BodyStyle"];
				this.Mileage = data["Mileage"];
				this.ExteriorColor = data["ExteriorColor"];
				this.InteriorColor = data["InteriorColor"];
				this.Equipment = data["Equipment"];
				this.Comments = data["Comments"];
				this.SellingPrice = data["SellingPrice"];
				this.ImageURLs = data["ImageURLs"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawAutonexusId = this.RawAutonexusId;
			data.AddParam("@RawAutonexusId", DataAccessParameterType.Numeric, rawAutonexusId.ToString());
			rawAutonexusId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawAutonexusId.ToString());
			data.AddParam("@DealerId", DataAccessParameterType.Text, this.DealerId);
			data.AddParam("@StockNumber", DataAccessParameterType.Text, this.StockNumber);
			data.AddParam("@VIN", DataAccessParameterType.Text, this.VIN);
			data.AddParam("@Year", DataAccessParameterType.Text, this.Year);
			data.AddParam("@Make", DataAccessParameterType.Text, this.Make);
			data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
			data.AddParam("@Trim", DataAccessParameterType.Text, this.Trim);
			data.AddParam("@Engine", DataAccessParameterType.Text, this.Engine);
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			data.AddParam("@Doors", DataAccessParameterType.Text, this.Doors);
			data.AddParam("@BodyStyle", DataAccessParameterType.Text, this.BodyStyle);
			data.AddParam("@Mileage", DataAccessParameterType.Text, this.Mileage);
			data.AddParam("@ExteriorColor", DataAccessParameterType.Text, this.ExteriorColor);
			data.AddParam("@InteriorColor", DataAccessParameterType.Text, this.InteriorColor);
			data.AddParam("@Equipment", DataAccessParameterType.Text, this.Equipment);
			data.AddParam("@Comments", DataAccessParameterType.Text, this.Comments);
			data.AddParam("@SellingPrice", DataAccessParameterType.Text, this.SellingPrice);
			data.AddParam("@ImageURLs", DataAccessParameterType.Text, this.ImageURLs);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("RAW_AUTONEXUS_InsertUpdate");
			if (!data.EOF)
			{
				this.RawAutonexusId = int.Parse(data["RawAutonexusId"]);
			}
			this.retrievedata();
		}
	}
}