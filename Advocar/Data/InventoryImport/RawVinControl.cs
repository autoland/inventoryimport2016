using Advocar.Data;
using Advocar.Interface;
using System;
using System.Runtime.CompilerServices;

namespace Advocar.Data.InventoryImport
{
	public class RawVinControl : Transaction
	{
		public string Body
		{
			get;
			set;
		}

		public string BookValue
		{
			get;
			set;
		}

		public string Certified
		{
			get;
			set;
		}

		public string DateInStock
		{
			get;
			set;
		}

		public string DealerDemonstrator
		{
			get;
			set;
		}

		public string DealerId
		{
			get;
			set;
		}

		public string Description
		{
			get;
			set;
		}

		public int Doors
		{
			get;
			set;
		}

		public string Drivetrain
		{
			get;
			set;
		}

		public int EngineCylinders
		{
			get;
			set;
		}

		public string EngineDisplacement
		{
			get;
			set;
		}

		public string ExteriorColor
		{
			get;
			set;
		}

		public string InteriorColor
		{
			get;
			set;
		}

		public string Invoice
		{
			get;
			set;
		}

		public int JobExecutionRawId
		{
			get;
			set;
		}

		public string Make
		{
			get;
			set;
		}

		public int Miles
		{
			get;
			set;
		}

		public string Model
		{
			get;
			set;
		}

		public string ModelNumber
		{
			get;
			set;
		}

		public float Msrp
		{
			get;
			set;
		}

		public string NewUsed
		{
			get;
			set;
		}

		public string Options
		{
			get;
			set;
		}

		public string PhotoUrLs
		{
			get;
			set;
		}

		public string PriorRental
		{
			get;
			set;
		}

		public int RawVinControlId
		{
			get;
			set;
		}

		public float SellingPrice
		{
			get;
			set;
		}

		public string Stock
		{
			get;
			set;
		}

		public string Transmission
		{
			get;
			set;
		}

		public string Trim
		{
			get;
			set;
		}

		public string Unwind
		{
			get;
			set;
		}

		public int VehicleYear
		{
			get;
			set;
		}

		public string Vin
		{
			get;
			set;
		}

		public RawVinControl()
		{
		}

		public RawVinControl(string connectionString, string modifiedUser, int rawVinControlId) : base(connectionString, modifiedUser)
		{
			this.databaseObjectName = "RAW_VIN_CONTROL";
			this.RawVinControlId = rawVinControlId;
			if (this.RawVinControlId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawVinControlId", DataAccessParameterType.Numeric, this.RawVinControlId.ToString());
			data.ExecuteProcedure("RAW_VIN_CONTROL_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawVinControlId", DataAccessParameterType.Numeric, this.RawVinControlId.ToString());
			data.ExecuteProcedure("RAW_VIN_CONTROL_GetRecord");
			if (data.EOF)
			{
				this.wipeout();
			}
			else
			{
				this.RawVinControlId = int.Parse(data["RawVinControlId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerId = data["DealerId"];
				this.NewUsed = data["NewUsed"];
				this.Stock = data["Stock"];
				this.Vin = data["Vin"];
				this.VehicleYear = int.Parse(data["VehicleYear"]);
				this.Make = data["Make"];
				this.Model = data["Model"];
				this.ModelNumber = data["ModelNumber"];
				this.Body = data["Body"];
				this.Trim = data["Trim"];
				this.Doors = int.Parse(data["Doors"]);
				this.ExteriorColor = data["ExteriorColor"];
				this.InteriorColor = data["InteriorColor"];
				this.EngineCylinders = int.Parse(data["EngineCylinders"]);
				this.EngineDisplacement = data["EngineDisplacement"];
				this.Transmission = data["Transmission"];
				this.Miles = int.Parse(data["Miles"]);
				this.SellingPrice = float.Parse(data["SellingPrice"]);
				this.Msrp = float.Parse(data["MSRP"]);
				this.BookValue = data["BookValue"];
				this.Invoice = data["Invoice"];
				this.Certified = data["Certified"];
				this.DateInStock = data["DateInStock"];
				this.Description = data["Description"];
				this.Options = data["Options"];
				this.Drivetrain = data["Drivetrain"];
				this.PhotoUrLs = data["PhotoURLs"];
				this.PriorRental = data["PriorRental"];
				this.DealerDemonstrator = data["DealerDemonstrator"];
				this.Unwind = data["Unwind"];
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawVinControlId = this.RawVinControlId;
			data.AddParam("@RawVinControlId", DataAccessParameterType.Numeric, rawVinControlId.ToString());
			rawVinControlId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawVinControlId.ToString());
			data.AddParam("@DealerId", DataAccessParameterType.Text, this.DealerId);
			data.AddParam("@NewUsed", DataAccessParameterType.Text, this.NewUsed);
			data.AddParam("@Stock", DataAccessParameterType.Text, this.Stock);
			data.AddParam("@Vin", DataAccessParameterType.Text, this.Vin);
			rawVinControlId = this.VehicleYear;
			data.AddParam("@VehicleYear", DataAccessParameterType.Text, rawVinControlId.ToString());
			data.AddParam("@Make", DataAccessParameterType.Text, this.Make);
			data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
			data.AddParam("@ModelNumber", DataAccessParameterType.Text, this.ModelNumber);
			data.AddParam("@Body", DataAccessParameterType.Text, this.Body);
			data.AddParam("@Trim", DataAccessParameterType.Text, this.Trim);
			rawVinControlId = this.Doors;
			data.AddParam("@Doors", DataAccessParameterType.Text, rawVinControlId.ToString());
			data.AddParam("@ExteriorColor", DataAccessParameterType.Text, this.ExteriorColor);
			data.AddParam("@InteriorColor", DataAccessParameterType.Text, this.InteriorColor);
			rawVinControlId = this.EngineCylinders;
			data.AddParam("@EngineCylinders", DataAccessParameterType.Text, rawVinControlId.ToString());
			data.AddParam("@EngineDisplacement", DataAccessParameterType.Text, this.EngineDisplacement);
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			rawVinControlId = this.Miles;
			data.AddParam("@Miles", DataAccessParameterType.Text, rawVinControlId.ToString());
			float sellingPrice = this.SellingPrice;
			data.AddParam("@SellingPrice", DataAccessParameterType.Text, sellingPrice.ToString());
			sellingPrice = this.Msrp;
			data.AddParam("@MSRP", DataAccessParameterType.Text, sellingPrice.ToString());
			data.AddParam("@BookValue", DataAccessParameterType.Text, this.BookValue);
			data.AddParam("@Invoice", DataAccessParameterType.Text, this.Invoice);
			data.AddParam("@Certified", DataAccessParameterType.Text, this.Certified);
			data.AddParam("@DateInStock", DataAccessParameterType.Text, this.DateInStock);
			data.AddParam("@Description", DataAccessParameterType.Text, this.Description);
			data.AddParam("@Options", DataAccessParameterType.Text, this.Options);
			data.AddParam("@Drivetrain", DataAccessParameterType.Text, this.Drivetrain);
			data.AddParam("@PhotoURLs", DataAccessParameterType.Text, this.PhotoUrLs);
			data.AddParam("@PriorRental", DataAccessParameterType.Text, this.PriorRental);
			data.AddParam("@DealerDemonstrator", DataAccessParameterType.Text, this.DealerDemonstrator);
			data.AddParam("@Unwind", DataAccessParameterType.Text, this.Unwind);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("RAW_VIN_CONTROL_InsertUpdate");
			if (!data.EOF)
			{
				this.RawVinControlId = int.Parse(data["RawVinControlId"]);
			}
			this.retrievedata();
		}
	}
}