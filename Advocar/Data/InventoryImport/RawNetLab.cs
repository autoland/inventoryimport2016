using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawNetLab : Transaction
	{
		private int rawNetLabId = 0;

		private int jobExecutionRawId = 0;

		private string dealerID = "";

		private string newUsed = "";

		private string stockID = "";

		private string vehicleDescription = "";

		private int year = 0;

		private string make = "";

		private string model = "";

		private string trim = "";

		private string vIN = "";

		private string style = "";

		private string engine = "";

		private string driveTrain = "";

		private string transmission = "";

		private string options = "";

		private int mileage = 0;

		private float sellingPrice = 0f;

		private float retailPrice = 0f;

		private float retailPriceKbb = 0f;

		private string imageUrls = "";

		public string DealerID
		{
			get
			{
				return this.dealerID;
			}
			set
			{
				this.dealerID = value;
			}
		}

		public string DriveTrain
		{
			get
			{
				return this.driveTrain;
			}
			set
			{
				this.driveTrain = value;
			}
		}

		public string Engine
		{
			get
			{
				return this.engine;
			}
			set
			{
				this.engine = value;
			}
		}

		public string ImageUrls
		{
			get
			{
				return this.imageUrls;
			}
			set
			{
				this.imageUrls = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public string Make
		{
			get
			{
				return this.make;
			}
			set
			{
				this.make = value;
			}
		}

		public int Mileage
		{
			get
			{
				return this.mileage;
			}
			set
			{
				this.mileage = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public string NewUsed
		{
			get
			{
				return this.newUsed;
			}
			set
			{
				this.newUsed = value;
			}
		}

		public string Options
		{
			get
			{
				return this.options;
			}
			set
			{
				this.options = value;
			}
		}

		public int RawNetLabId
		{
			get
			{
				return this.rawNetLabId;
			}
			set
			{
				this.rawNetLabId = value;
			}
		}

		public float RetailPrice
		{
			get
			{
				return this.retailPrice;
			}
			set
			{
				this.retailPrice = value;
			}
		}

		public float RetailPriceKbb
		{
			get
			{
				return this.retailPriceKbb;
			}
			set
			{
				this.retailPriceKbb = value;
			}
		}

		public float SellingPrice
		{
			get
			{
				return this.sellingPrice;
			}
			set
			{
				this.sellingPrice = value;
			}
		}

		public string StockID
		{
			get
			{
				return this.stockID;
			}
			set
			{
				this.stockID = value;
			}
		}

		public string Style
		{
			get
			{
				return this.style;
			}
			set
			{
				this.style = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string Trim
		{
			get
			{
				return this.trim;
			}
			set
			{
				this.trim = value;
			}
		}

		public string VehicleDescription
		{
			get
			{
				return this.vehicleDescription;
			}
			set
			{
				this.vehicleDescription = value;
			}
		}

		public string VIN
		{
			get
			{
				return this.vIN;
			}
			set
			{
				this.vIN = value;
			}
		}

		public int Year
		{
			get
			{
				return this.year;
			}
			set
			{
				this.year = value;
			}
		}

		public RawNetLab()
		{
		}

		public RawNetLab(string connection, string modifiedUserID, int rawNetLabId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.RawNetLabId = rawNetLabId;
			this.databaseObjectName = "RAW_NET_LAB";
			if (this.RawNetLabId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawNetLabId", DataAccessParameterType.Numeric, this.RawNetLabId.ToString());
			data.ExecuteProcedure("RAW_NET_LAB_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawNetLabId", DataAccessParameterType.Numeric, this.RawNetLabId.ToString());
			data.ExecuteProcedure("RAW_NET_LAB_GetRecord");
			if (!data.EOF)
			{
				this.RawNetLabId = int.Parse(data["RawNetLabId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerID = data["DealerID"];
				this.NewUsed = data["NewUsed"];
				this.StockID = data["StockID"];
				this.VehicleDescription = data["VehicleDescription"];
				this.Year = int.Parse(data["Year"]);
				this.Make = data["Make"];
				this.Model = data["Model"];
				this.Trim = data["Trim"];
				this.VIN = data["VIN"];
				this.Style = data["Style"];
				this.Engine = data["Engine"];
				this.DriveTrain = data["DriveTrain"];
				this.Transmission = data["Transmission"];
				this.Options = data["Options"];
				this.Mileage = int.Parse(data["Mileage"]);
				this.SellingPrice = float.Parse(data["SellingPrice"]);
				this.RetailPrice = float.Parse(data["RetailPrice"]);
				this.RetailPriceKbb = float.Parse(data["RetailPriceKbb"]);
				this.ImageUrls = data["ImageUrls"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawNetLabId = this.RawNetLabId;
			data.AddParam("@RawNetLabId", DataAccessParameterType.Numeric, rawNetLabId.ToString());
			rawNetLabId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawNetLabId.ToString());
			data.AddParam("@DealerID", DataAccessParameterType.Text, this.DealerID);
			data.AddParam("@NewUsed", DataAccessParameterType.Text, this.NewUsed);
			data.AddParam("@StockID", DataAccessParameterType.Text, this.StockID);
			data.AddParam("@VehicleDescription", DataAccessParameterType.Text, this.VehicleDescription);
			rawNetLabId = this.Year;
			data.AddParam("@Year", DataAccessParameterType.Numeric, rawNetLabId.ToString());
			data.AddParam("@Make", DataAccessParameterType.Text, this.Make);
			data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
			data.AddParam("@Trim", DataAccessParameterType.Text, this.Trim);
			data.AddParam("@VIN", DataAccessParameterType.Text, this.VIN);
			data.AddParam("@Style", DataAccessParameterType.Text, this.Style);
			data.AddParam("@Engine", DataAccessParameterType.Text, this.Engine);
			data.AddParam("@DriveTrain", DataAccessParameterType.Text, this.DriveTrain);
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			data.AddParam("@Options", DataAccessParameterType.Text, this.Options);
			rawNetLabId = this.Mileage;
			data.AddParam("@Mileage", DataAccessParameterType.Numeric, rawNetLabId.ToString());
			float sellingPrice = this.SellingPrice;
			data.AddParam("@SellingPrice", DataAccessParameterType.Numeric, sellingPrice.ToString());
			sellingPrice = this.RetailPrice;
			data.AddParam("@RetailPrice", DataAccessParameterType.Numeric, sellingPrice.ToString());
			sellingPrice = this.RetailPriceKbb;
			data.AddParam("@RetailPriceKbb", DataAccessParameterType.Numeric, sellingPrice.ToString());
			data.AddParam("@ImageUrls", DataAccessParameterType.Text, this.ImageUrls);
			data.ExecuteProcedure("RAW_NET_LAB_InsertUpdate");
			if (!data.EOF)
			{
				this.RawNetLabId = int.Parse(data["RawNetLabId"]);
			}
			this.retrievedata();
		}
	}
}