using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.InventoryImport
{
	public class MappingVehicleImage : Transaction
	{
		private int mappingVehicleImageId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int mappingVehicleId = 0;

		private string imageSourceUrlOrFile = "";

		private string imageSourcePath = "";

		private bool isImageUploadedToDestination = false;

		private string imageDestinationPath = "";

		private bool isImageThumbnailedToDestination = false;

		private string imageDestinationThumbnailPath = "";

		private bool isTransferred = false;

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string ImageDestinationPath
		{
			get
			{
				return this.imageDestinationPath;
			}
			set
			{
				this.imageDestinationPath = value;
			}
		}

		public string ImageDestinationThumbnailPath
		{
			get
			{
				return this.imageDestinationThumbnailPath;
			}
			set
			{
				this.imageDestinationThumbnailPath = value;
			}
		}

		public string ImageSourcePath
		{
			get
			{
				return this.imageSourcePath;
			}
			set
			{
				this.imageSourcePath = value;
			}
		}

		public string ImageSourceUrlOrFile
		{
			get
			{
				return this.imageSourceUrlOrFile;
			}
			set
			{
				this.imageSourceUrlOrFile = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public bool IsImageThumbnailedToDestination
		{
			get
			{
				return this.isImageThumbnailedToDestination;
			}
			set
			{
				this.isImageThumbnailedToDestination = value;
			}
		}

		public bool IsImageUploadedToDestination
		{
			get
			{
				return this.isImageUploadedToDestination;
			}
			set
			{
				this.isImageUploadedToDestination = value;
			}
		}

		public bool IsTransferred
		{
			get
			{
				return this.isTransferred;
			}
			set
			{
				this.isTransferred = value;
			}
		}

		public int MappingVehicleId
		{
			get
			{
				return this.mappingVehicleId;
			}
			set
			{
				this.mappingVehicleId = value;
			}
		}

		public int MappingVehicleImageId
		{
			get
			{
				return this.mappingVehicleImageId;
			}
			set
			{
				this.mappingVehicleImageId = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public MappingVehicleImage()
		{
		}

		public MappingVehicleImage(string connection, string modifiedUserID, int mappingVehicleImageId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.MappingVehicleImageId = mappingVehicleImageId;
			if (this.MappingVehicleImageId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@MappingVehicleImageId", DataAccessParameterType.Numeric, this.MappingVehicleImageId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("MAPPING_VEHICLE_IMAGE_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@MappingVehicleImageId", DataAccessParameterType.Numeric, this.MappingVehicleImageId.ToString());
			data.ExecuteProcedure("MAPPING_VEHICLE_IMAGE_GetRecord");
			if (!data.EOF)
			{
				this.MappingVehicleImageId = int.Parse(data["MappingVehicleImageId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.MappingVehicleId = int.Parse(data["MappingVehicleId"]);
				this.ImageSourceUrlOrFile = data["ImageSourceUrlOrFile"];
				this.ImageSourcePath = data["ImageSourcePath"];
				this.IsImageUploadedToDestination = bool.Parse(data["IsImageUploadedToDestination"]);
				this.ImageDestinationPath = data["ImageDestinationPath"];
				this.IsImageThumbnailedToDestination = bool.Parse(data["IsImageThumbnailedToDestination"]);
				this.ImageDestinationThumbnailPath = data["ImageDestinationThumbnailPath"];
				this.IsTransferred = bool.Parse(data["IsTransferred"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int mappingVehicleImageId = this.MappingVehicleImageId;
			data.AddParam("@MappingVehicleImageId", DataAccessParameterType.Numeric, mappingVehicleImageId.ToString());
			mappingVehicleImageId = this.MappingVehicleId;
			data.AddParam("@MappingVehicleId", DataAccessParameterType.Numeric, mappingVehicleImageId.ToString());
			data.AddParam("@ImageSourceUrlOrFile", DataAccessParameterType.Text, this.ImageSourceUrlOrFile);
			data.AddParam("@ImageSourcePath", DataAccessParameterType.Text, this.ImageSourcePath);
			bool isImageUploadedToDestination = this.IsImageUploadedToDestination;
			data.AddParam("@IsImageUploadedToDestination", DataAccessParameterType.Bool, isImageUploadedToDestination.ToString());
			data.AddParam("@ImageDestinationPath", DataAccessParameterType.Text, this.ImageDestinationPath);
			isImageUploadedToDestination = this.IsImageThumbnailedToDestination;
			data.AddParam("@IsImageThumbnailedToDestination", DataAccessParameterType.Bool, isImageUploadedToDestination.ToString());
			data.AddParam("@ImageDestinationThumbnailPath", DataAccessParameterType.Text, this.ImageDestinationThumbnailPath);
			isImageUploadedToDestination = this.IsTransferred;
			data.AddParam("@IsTransferred", DataAccessParameterType.Bool, isImageUploadedToDestination.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("MAPPING_VEHICLE_IMAGE_InsertUpdate");
			if (!data.EOF)
			{
				this.MappingVehicleImageId = int.Parse(data["MappingVehicleImageId"]);
			}
			this.retrievedata();
		}
	}
}