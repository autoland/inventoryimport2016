using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawDealerFusion : Transaction
	{
		private int rawDealerFusionId = 0;

		private int jobExecutionRawId = 0;

		private string dealerName = "";

		private string dealerID = "";

		private int year = 0;

		private string make = "";

		private string model = "";

		private string vIN = "";

		private string stockID = "";

		private string engine = "";

		private string transmission = "";

		private string description = "";

		private int mileage = 0;

		private float price = 0f;

		private string extColor = "";

		private string dealerAddress = "";

		private string dealerCity = "";

		private string dealerState = "";

		private string dealerZip = "";

		private string dealerPhone = "";

		private string emailLeadsTo = "";

		private string photoUrl = "";

		private string equipment = "";

		private string dealerMessage = "";

		private string certifiedYesNo = "";

		private string highOctane = "";

		private string highOctane360 = "";

		private string highOctaneMultiPhotos = "";

		private float retailPrice = 0f;

		private string dealerBlurb = "";

		public string CertifiedYesNo
		{
			get
			{
				return this.certifiedYesNo;
			}
			set
			{
				this.certifiedYesNo = value;
			}
		}

		public string DealerAddress
		{
			get
			{
				return this.dealerAddress;
			}
			set
			{
				this.dealerAddress = value;
			}
		}

		public string DealerBlurb
		{
			get
			{
				return this.dealerBlurb;
			}
			set
			{
				this.dealerBlurb = value;
			}
		}

		public string DealerCity
		{
			get
			{
				return this.dealerCity;
			}
			set
			{
				this.dealerCity = value;
			}
		}

		public string DealerID
		{
			get
			{
				return this.dealerID;
			}
			set
			{
				this.dealerID = value;
			}
		}

		public string DealerMessage
		{
			get
			{
				return this.dealerMessage;
			}
			set
			{
				this.dealerMessage = value;
			}
		}

		public string DealerName
		{
			get
			{
				return this.dealerName;
			}
			set
			{
				this.dealerName = value;
			}
		}

		public string DealerPhone
		{
			get
			{
				return this.dealerPhone;
			}
			set
			{
				this.dealerPhone = value;
			}
		}

		public string DealerState
		{
			get
			{
				return this.dealerState;
			}
			set
			{
				this.dealerState = value;
			}
		}

		public string DealerZip
		{
			get
			{
				return this.dealerZip;
			}
			set
			{
				this.dealerZip = value;
			}
		}

		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

		public string EmailLeadsTo
		{
			get
			{
				return this.emailLeadsTo;
			}
			set
			{
				this.emailLeadsTo = value;
			}
		}

		public string Engine
		{
			get
			{
				return this.engine;
			}
			set
			{
				this.engine = value;
			}
		}

		public string Equipment
		{
			get
			{
				return this.equipment;
			}
			set
			{
				this.equipment = value;
			}
		}

		public string ExtColor
		{
			get
			{
				return this.extColor;
			}
			set
			{
				this.extColor = value;
			}
		}

		public string HighOctane
		{
			get
			{
				return this.highOctane;
			}
			set
			{
				this.highOctane = value;
			}
		}

		public string HighOctane360
		{
			get
			{
				return this.highOctane360;
			}
			set
			{
				this.highOctane360 = value;
			}
		}

		public string HighOctaneMultiPhotos
		{
			get
			{
				return this.highOctaneMultiPhotos;
			}
			set
			{
				this.highOctaneMultiPhotos = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public string Make
		{
			get
			{
				return this.make;
			}
			set
			{
				this.make = value;
			}
		}

		public int Mileage
		{
			get
			{
				return this.mileage;
			}
			set
			{
				this.mileage = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public string PhotoUrl
		{
			get
			{
				return this.photoUrl;
			}
			set
			{
				this.photoUrl = value;
			}
		}

		public float Price
		{
			get
			{
				return this.price;
			}
			set
			{
				this.price = value;
			}
		}

		public int RawDealerFusionId
		{
			get
			{
				return this.rawDealerFusionId;
			}
			set
			{
				this.rawDealerFusionId = value;
			}
		}

		public float RetailPrice
		{
			get
			{
				return this.retailPrice;
			}
			set
			{
				this.retailPrice = value;
			}
		}

		public string StockID
		{
			get
			{
				return this.stockID;
			}
			set
			{
				this.stockID = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string VIN
		{
			get
			{
				return this.vIN;
			}
			set
			{
				this.vIN = value;
			}
		}

		public int Year
		{
			get
			{
				return this.year;
			}
			set
			{
				this.year = value;
			}
		}

		public RawDealerFusion()
		{
		}

		public RawDealerFusion(string connection, string modifiedUserID, int rawDealerFusionId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.RawDealerFusionId = rawDealerFusionId;
			this.databaseObjectName = "RAW_DEALER_FUSION";
			if (this.RawDealerFusionId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawDealerFusionId", DataAccessParameterType.Numeric, this.RawDealerFusionId.ToString());
			data.ExecuteProcedure("RAW_DEALER_FUSION_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawDealerFusionId", DataAccessParameterType.Numeric, this.RawDealerFusionId.ToString());
			data.ExecuteProcedure("RAW_DEALER_FUSION_GetRecord");
			if (!data.EOF)
			{
				this.RawDealerFusionId = int.Parse(data["RawDealerFusionId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerName = data["DealerName"];
				this.DealerID = data["DealerID"];
				this.Year = int.Parse(data["Year"]);
				this.Make = data["Make"];
				this.Model = data["Model"];
				this.VIN = data["VIN"];
				this.StockID = data["StockID"];
				this.Engine = data["Engine"];
				this.Transmission = data["Transmission"];
				this.Description = data["Description"];
				this.Mileage = int.Parse(data["Mileage"]);
				this.Price = float.Parse(data["Price"]);
				this.ExtColor = data["ExtColor"];
				this.DealerAddress = data["DealerAddress"];
				this.DealerCity = data["DealerCity"];
				this.DealerState = data["DealerState"];
				this.DealerZip = data["DealerZip"];
				this.DealerPhone = data["DealerPhone"];
				this.EmailLeadsTo = data["EmailLeadsTo"];
				this.PhotoUrl = data["PhotoUrl"];
				this.Equipment = data["Equipment"];
				this.DealerMessage = data["DealerMessage"];
				this.CertifiedYesNo = data["CertifiedYesNo"];
				this.HighOctane = data["HighOctane"];
				this.HighOctane360 = data["HighOctane360"];
				this.HighOctaneMultiPhotos = data["HighOctaneMultiPhotos"];
				this.RetailPrice = float.Parse(data["RetailPrice"]);
				this.DealerBlurb = data["DealerBlurb"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawDealerFusionId = this.RawDealerFusionId;
			data.AddParam("@RawDealerFusionId", DataAccessParameterType.Numeric, rawDealerFusionId.ToString());
			rawDealerFusionId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawDealerFusionId.ToString());
			data.AddParam("@DealerName", DataAccessParameterType.Text, this.DealerName);
			data.AddParam("@DealerID", DataAccessParameterType.Text, this.DealerID);
			rawDealerFusionId = this.Year;
			data.AddParam("@Year", DataAccessParameterType.Numeric, rawDealerFusionId.ToString());
			data.AddParam("@Make", DataAccessParameterType.Text, this.Make);
			data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
			data.AddParam("@VIN", DataAccessParameterType.Text, this.VIN);
			data.AddParam("@StockID", DataAccessParameterType.Text, this.StockID);
			data.AddParam("@Engine", DataAccessParameterType.Text, this.Engine);
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			data.AddParam("@Description", DataAccessParameterType.Text, this.Description);
			rawDealerFusionId = this.Mileage;
			data.AddParam("@Mileage", DataAccessParameterType.Numeric, rawDealerFusionId.ToString());
			float price = this.Price;
			data.AddParam("@Price", DataAccessParameterType.Numeric, price.ToString());
			data.AddParam("@ExtColor", DataAccessParameterType.Text, this.ExtColor);
			data.AddParam("@DealerAddress", DataAccessParameterType.Text, this.DealerAddress);
			data.AddParam("@DealerCity", DataAccessParameterType.Text, this.DealerCity);
			data.AddParam("@DealerState", DataAccessParameterType.Text, this.DealerState);
			data.AddParam("@DealerZip", DataAccessParameterType.Text, this.DealerZip);
			data.AddParam("@DealerPhone", DataAccessParameterType.Text, this.DealerPhone);
			data.AddParam("@EmailLeadsTo", DataAccessParameterType.Text, this.EmailLeadsTo);
			data.AddParam("@PhotoUrl", DataAccessParameterType.Text, this.PhotoUrl);
			data.AddParam("@Equipment", DataAccessParameterType.Text, this.Equipment);
			data.AddParam("@DealerMessage", DataAccessParameterType.Text, this.DealerMessage);
			data.AddParam("@CertifiedYesNo", DataAccessParameterType.Text, this.CertifiedYesNo);
			data.AddParam("@HighOctane", DataAccessParameterType.Text, this.HighOctane);
			data.AddParam("@HighOctane360", DataAccessParameterType.Text, this.HighOctane360);
			data.AddParam("@HighOctaneMultiPhotos", DataAccessParameterType.Text, this.HighOctaneMultiPhotos);
			price = this.RetailPrice;
			data.AddParam("@RetailPrice", DataAccessParameterType.Numeric, price.ToString());
			data.AddParam("@DealerBlurb", DataAccessParameterType.Text, this.DealerBlurb);
			data.ExecuteProcedure("RAW_DEALER_FUSION_InsertUpdate");
			if (!data.EOF)
			{
				this.RawDealerFusionId = int.Parse(data["RawDealerFusionId"]);
			}
			this.retrievedata();
		}
	}
}