using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawVauto : Transaction
	{
		private int rawVautoId = 0;

		private int jobExecutionRawId = 0;

		private string dealerId = "";

		private string vin = "";

		private string stockNumber = "";

		private string newUsed = "";

		private int year = 0;

		private string make = "";

		private string model = "";

		private string modelNumber = "";

		private string body = "";

		private string transmission = "";

		private string series = "";

		private int bodyDoorCount = 0;

		private int odometer = 0;

		private string engineCylinderCount = "";

		private string engineDisplacement = "";

		private string driveTrain = "";

		private string color = "";

		private string interiorColor = "";

		private float invoice = 0f;

		private float msrp = 0f;

		private float bookValue = 0f;

		private float price = 0f;

		private string inventoryDate = "";

		private string certified = "";

		private string description = "";

		private string features = "";

		private string photoUrlList = "";

		public string Body
		{
			get
			{
				return this.body;
			}
			set
			{
				this.body = value;
			}
		}

		public int BodyDoorCount
		{
			get
			{
				return this.bodyDoorCount;
			}
			set
			{
				this.bodyDoorCount = value;
			}
		}

		public float BookValue
		{
			get
			{
				return this.bookValue;
			}
			set
			{
				this.bookValue = value;
			}
		}

		public string Certified
		{
			get
			{
				return this.certified;
			}
			set
			{
				this.certified = value;
			}
		}

		public string Color
		{
			get
			{
				return this.color;
			}
			set
			{
				this.color = value;
			}
		}

		public string DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

		public string DriveTrain
		{
			get
			{
				return this.driveTrain;
			}
			set
			{
				this.driveTrain = value;
			}
		}

		public string EngineCylinderCount
		{
			get
			{
				return this.engineCylinderCount;
			}
			set
			{
				this.engineCylinderCount = value;
			}
		}

		public string EngineDisplacement
		{
			get
			{
				return this.engineDisplacement;
			}
			set
			{
				this.engineDisplacement = value;
			}
		}

		public string Features
		{
			get
			{
				return this.features;
			}
			set
			{
				this.features = value;
			}
		}

		public string InteriorColor
		{
			get
			{
				return this.interiorColor;
			}
			set
			{
				this.interiorColor = value;
			}
		}

		public string InventoryDate
		{
			get
			{
				return this.inventoryDate;
			}
			set
			{
				this.inventoryDate = value;
			}
		}

		public float Invoice
		{
			get
			{
				return this.invoice;
			}
			set
			{
				this.invoice = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public string Make
		{
			get
			{
				return this.make;
			}
			set
			{
				this.make = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public string ModelNumber
		{
			get
			{
				return this.modelNumber;
			}
			set
			{
				this.modelNumber = value;
			}
		}

		public float Msrp
		{
			get
			{
				return this.msrp;
			}
			set
			{
				this.msrp = value;
			}
		}

		public string NewUsed
		{
			get
			{
				return this.newUsed;
			}
			set
			{
				this.newUsed = value;
			}
		}

		public int Odometer
		{
			get
			{
				return this.odometer;
			}
			set
			{
				this.odometer = value;
			}
		}

		public string PhotoUrlList
		{
			get
			{
				return this.photoUrlList;
			}
			set
			{
				this.photoUrlList = value;
			}
		}

		public float Price
		{
			get
			{
				return this.price;
			}
			set
			{
				this.price = value;
			}
		}

		public int RawVautoId
		{
			get
			{
				return this.rawVautoId;
			}
			set
			{
				this.rawVautoId = value;
			}
		}

		public string Series
		{
			get
			{
				return this.series;
			}
			set
			{
				this.series = value;
			}
		}

		public string StockNumber
		{
			get
			{
				return this.stockNumber;
			}
			set
			{
				this.stockNumber = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string Vin
		{
			get
			{
				return this.vin;
			}
			set
			{
				this.vin = value;
			}
		}

		public int Year
		{
			get
			{
				return this.year;
			}
			set
			{
				this.year = value;
			}
		}

		public RawVauto()
		{
		}

		public RawVauto(string connection, string modifiedUserID, int rawVautoId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.RawVautoId = rawVautoId;
			this.databaseObjectName = "RAW_VAUTO";
			if (this.RawVautoId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawVautoId", DataAccessParameterType.Numeric, this.RawVautoId.ToString());
			data.ExecuteProcedure("RAW_VAUTO_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawVautoId", DataAccessParameterType.Numeric, this.RawVautoId.ToString());
			data.ExecuteProcedure("RAW_VAUTO_GetRecord");
			if (!data.EOF)
			{
				this.RawVautoId = int.Parse(data["RawVautoId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerId = data["DealerId"];
				this.Vin = data["Vin"];
				this.StockNumber = data["StockNumber"];
				this.NewUsed = data["NewUsed"];
				this.Year = int.Parse(data["Year"]);
				this.Make = data["Make"];
				this.Model = data["Model"];
				this.ModelNumber = data["ModelNumber"];
				this.Body = data["Body"];
				this.Transmission = data["Transmission"];
				this.Series = data["Series"];
				this.BodyDoorCount = int.Parse(data["BodyDoorCount"]);
				this.Odometer = int.Parse(data["Odometer"]);
				this.EngineCylinderCount = data["EngineCylinderCount"];
				this.EngineDisplacement = data["EngineDisplacement"];
				this.DriveTrain = data["DriveTrain"];
				this.Color = data["Color"];
				this.InteriorColor = data["InteriorColor"];
				this.Invoice = float.Parse(data["Invoice"]);
				this.Msrp = float.Parse(data["MSRP"]);
				this.BookValue = float.Parse(data["BookValue"]);
				this.Price = float.Parse(data["Price"]);
				this.InventoryDate = data["InventoryDate"];
				this.Certified = data["Certified"];
				this.Description = data["Description"];
				this.Features = data["Features"];
				this.PhotoUrlList = data["PhotoUrlList"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawVautoId = this.RawVautoId;
			data.AddParam("@RawVautoId", DataAccessParameterType.Numeric, rawVautoId.ToString());
			rawVautoId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawVautoId.ToString());
			data.AddParam("@DealerId", DataAccessParameterType.Text, this.DealerId);
			data.AddParam("@Vin", DataAccessParameterType.Text, this.Vin);
			data.AddParam("@StockNumber", DataAccessParameterType.Text, this.StockNumber);
			data.AddParam("@NewUsed", DataAccessParameterType.Text, this.NewUsed);
			rawVautoId = this.Year;
			data.AddParam("@Year", DataAccessParameterType.Numeric, rawVautoId.ToString());
			data.AddParam("@Make", DataAccessParameterType.Text, this.Make);
			data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
			data.AddParam("@ModelNumber", DataAccessParameterType.Text, this.ModelNumber);
			data.AddParam("@Body", DataAccessParameterType.Text, this.Body);
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			data.AddParam("@Series", DataAccessParameterType.Text, this.Series);
			rawVautoId = this.BodyDoorCount;
			data.AddParam("@BodyDoorCount", DataAccessParameterType.Numeric, rawVautoId.ToString());
			rawVautoId = this.Odometer;
			data.AddParam("@Odometer", DataAccessParameterType.Numeric, rawVautoId.ToString());
			data.AddParam("@EngineCylinderCount", DataAccessParameterType.Text, this.EngineCylinderCount);
			data.AddParam("@EngineDisplacement", DataAccessParameterType.Text, this.EngineDisplacement);
			data.AddParam("@DriveTrain", DataAccessParameterType.Text, this.DriveTrain);
			data.AddParam("@Color", DataAccessParameterType.Text, this.Color);
			data.AddParam("@InteriorColor", DataAccessParameterType.Text, this.InteriorColor);
			float invoice = this.Invoice;
			data.AddParam("@Invoice", DataAccessParameterType.Numeric, invoice.ToString());
			invoice = this.Msrp;
			data.AddParam("@MSRP", DataAccessParameterType.Numeric, invoice.ToString());
			invoice = this.BookValue;
			data.AddParam("@BookValue", DataAccessParameterType.Numeric, invoice.ToString());
			invoice = this.Price;
			data.AddParam("@Price", DataAccessParameterType.Numeric, invoice.ToString());
			data.AddParam("@InventoryDate", DataAccessParameterType.Text, this.InventoryDate);
			data.AddParam("@Certified", DataAccessParameterType.Text, this.Certified);
			data.AddParam("@Description", DataAccessParameterType.Text, this.Description);
			data.AddParam("@Features", DataAccessParameterType.Text, this.Features);
			data.AddParam("@PhotoUrlList", DataAccessParameterType.Text, this.PhotoUrlList);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("RAW_VAUTO_InsertUpdate");
			if (!data.EOF)
			{
				this.RawVautoId = int.Parse(data["RawVautoId"]);
			}
			this.retrievedata();
		}
	}
}