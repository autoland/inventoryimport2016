using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawMchsi : Transaction
	{
		private int rawMchsiId = 0;

		private int jobExecutionRawId = 0;

		private string dealerId = "";

		private string stockNo = "";

		private string vin = "";

		private string vehicleYear = "";

		private string manufacturer = "";

		private string model = "";

		private string trim = "";

		private string bodyType = "";

		private string price = "";

		private string mileage = "";

		private string engine = "";

		private string transmission = "";

		private string exteriorColor = "";

		private string interiorColor = "";

		private string options = "";

		private string images = "";

		public string BodyType
		{
			get
			{
				return this.bodyType;
			}
			set
			{
				this.bodyType = value;
			}
		}

		public string DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public string Engine
		{
			get
			{
				return this.engine;
			}
			set
			{
				this.engine = value;
			}
		}

		public string ExteriorColor
		{
			get
			{
				return this.exteriorColor;
			}
			set
			{
				this.exteriorColor = value;
			}
		}

		public string Images
		{
			get
			{
				return this.images;
			}
			set
			{
				this.images = value;
			}
		}

		public string InteriorColor
		{
			get
			{
				return this.interiorColor;
			}
			set
			{
				this.interiorColor = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public string Manufacturer
		{
			get
			{
				return this.manufacturer;
			}
			set
			{
				this.manufacturer = value;
			}
		}

		public string Mileage
		{
			get
			{
				return this.mileage;
			}
			set
			{
				this.mileage = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public string Options
		{
			get
			{
				return this.options;
			}
			set
			{
				this.options = value;
			}
		}

		public string Price
		{
			get
			{
				return this.price;
			}
			set
			{
				this.price = value;
			}
		}

		public int RawMchsiId
		{
			get
			{
				return this.rawMchsiId;
			}
			set
			{
				this.rawMchsiId = value;
			}
		}

		public string StockNo
		{
			get
			{
				return this.stockNo;
			}
			set
			{
				this.stockNo = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string Trim
		{
			get
			{
				return this.trim;
			}
			set
			{
				this.trim = value;
			}
		}

		public string VehicleYear
		{
			get
			{
				return this.vehicleYear;
			}
			set
			{
				this.vehicleYear = value;
			}
		}

		public string Vin
		{
			get
			{
				return this.vin;
			}
			set
			{
				this.vin = value;
			}
		}

		public RawMchsi()
		{
		}

		public RawMchsi(string connection, string modifiedUserID, int rawMchsiId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "RAW_MCHSI";
			this.RawMchsiId = rawMchsiId;
			if (this.RawMchsiId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawMchsiId", DataAccessParameterType.Numeric, this.RawMchsiId.ToString());
			data.ExecuteProcedure("RAW_MCHSI_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawMchsiId", DataAccessParameterType.Numeric, this.RawMchsiId.ToString());
			data.ExecuteProcedure("RAW_MCHSI_GetRecord");
			if (!data.EOF)
			{
				this.RawMchsiId = int.Parse(data["RawMchsiId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerId = data["DealerId"];
				this.StockNo = data["StockNo"];
				this.Vin = data["Vin"];
				this.VehicleYear = data["VehicleYear"];
				this.Manufacturer = data["Manufacturer"];
				this.Model = data["Model"];
				this.Trim = data["Trim"];
				this.BodyType = data["BodyType"];
				this.Price = data["Price"];
				this.Mileage = data["Mileage"];
				this.Engine = data["Engine"];
				this.Transmission = data["Transmission"];
				this.ExteriorColor = data["ExteriorColor"];
				this.InteriorColor = data["InteriorColor"];
				this.Options = data["Options"];
				this.Images = data["Images"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawMchsiId = this.RawMchsiId;
			data.AddParam("@RawMchsiId", DataAccessParameterType.Numeric, rawMchsiId.ToString());
			rawMchsiId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawMchsiId.ToString());
			data.AddParam("@DealerId", DataAccessParameterType.Text, this.DealerId);
			data.AddParam("@StockNo", DataAccessParameterType.Text, this.StockNo);
			data.AddParam("@Vin", DataAccessParameterType.Text, this.Vin);
			data.AddParam("@VehicleYear", DataAccessParameterType.Text, this.VehicleYear);
			data.AddParam("@Manufacturer", DataAccessParameterType.Text, this.Manufacturer);
			data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
			data.AddParam("@Trim", DataAccessParameterType.Text, this.Trim);
			data.AddParam("@BodyType", DataAccessParameterType.Text, this.BodyType);
			data.AddParam("@Price", DataAccessParameterType.Text, this.Price);
			data.AddParam("@Mileage", DataAccessParameterType.Text, this.Mileage);
			data.AddParam("@Engine", DataAccessParameterType.Text, this.Engine);
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			data.AddParam("@ExteriorColor", DataAccessParameterType.Text, this.ExteriorColor);
			data.AddParam("@InteriorColor", DataAccessParameterType.Text, this.InteriorColor);
			data.AddParam("@Options", DataAccessParameterType.Text, this.Options);
			data.AddParam("@Images", DataAccessParameterType.Text, this.Images);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("RAW_MCHSI_InsertUpdate");
			if (!data.EOF)
			{
				this.RawMchsiId = int.Parse(data["RawMchsiId"]);
			}
			this.retrievedata();
		}
	}
}