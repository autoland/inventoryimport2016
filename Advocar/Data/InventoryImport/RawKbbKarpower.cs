using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawKbbKarpower : Transaction
	{
		private int rawKbbKarpowerId = 0;

		private int jobExecutionRawId = 0;

		private string dealerID = "";

		private int karpowerID = 0;

		private string stockNo = "";

		private string dateStored = "";

		private int year = 0;

		private string make = "";

		private string model = "";

		private string engine = "";

		private string transmission = "";

		private string driveTrain = "";

		private string color = "";

		private int mileage = 0;

		private string vIN = "";

		private string license = "";

		private float priceRecon = 0f;

		private float priceSelling = 0f;

		private float priceCost = 0f;

		private string dateBook = "";

		private string unknownField18 = "";

		private string classic = "";

		private float baseWholesale = 0f;

		private float baseRetail = 0f;

		private float engineWholesale = 0f;

		private float engineRetail = 0f;

		private float transWholesale = 0f;

		private float transRetail = 0f;

		private float driveWholesale = 0f;

		private float driveRetail = 0f;

		private float mileAdjust = 0f;

		private float noMilesWholesale = 0f;

		private float noMilesRetail = 0f;

		private float maxDeduct = 0f;

		private string unknownField32 = "";

		private string unknownField33 = "";

		private float fullWholesale = 0f;

		private float fullRetail = 0f;

		private string dateUpdated = "";

		private string optionList = "";

		private string imageUrlList = "";

		public float BaseRetail
		{
			get
			{
				return this.baseRetail;
			}
			set
			{
				this.baseRetail = value;
			}
		}

		public float BaseWholesale
		{
			get
			{
				return this.baseWholesale;
			}
			set
			{
				this.baseWholesale = value;
			}
		}

		public string Classic
		{
			get
			{
				return this.classic;
			}
			set
			{
				this.classic = value;
			}
		}

		public string Color
		{
			get
			{
				return this.color;
			}
			set
			{
				this.color = value;
			}
		}

		public string DateBook
		{
			get
			{
				return this.dateBook;
			}
			set
			{
				this.dateBook = value;
			}
		}

		public string DateStored
		{
			get
			{
				return this.dateStored;
			}
			set
			{
				this.dateStored = value;
			}
		}

		public string DateUpdated
		{
			get
			{
				return this.dateUpdated;
			}
			set
			{
				this.dateUpdated = value;
			}
		}

		public string DealerID
		{
			get
			{
				return this.dealerID;
			}
			set
			{
				this.dealerID = value;
			}
		}

		public float DriveRetail
		{
			get
			{
				return this.driveRetail;
			}
			set
			{
				this.driveRetail = value;
			}
		}

		public string DriveTrain
		{
			get
			{
				return this.driveTrain;
			}
			set
			{
				this.driveTrain = value;
			}
		}

		public float DriveWholesale
		{
			get
			{
				return this.driveWholesale;
			}
			set
			{
				this.driveWholesale = value;
			}
		}

		public string Engine
		{
			get
			{
				return this.engine;
			}
			set
			{
				this.engine = value;
			}
		}

		public float EngineRetail
		{
			get
			{
				return this.engineRetail;
			}
			set
			{
				this.engineRetail = value;
			}
		}

		public float EngineWholesale
		{
			get
			{
				return this.engineWholesale;
			}
			set
			{
				this.engineWholesale = value;
			}
		}

		public float FullRetail
		{
			get
			{
				return this.fullRetail;
			}
			set
			{
				this.fullRetail = value;
			}
		}

		public float FullWholesale
		{
			get
			{
				return this.fullWholesale;
			}
			set
			{
				this.fullWholesale = value;
			}
		}

		public string ImageUrlList
		{
			get
			{
				return this.imageUrlList;
			}
			set
			{
				this.imageUrlList = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public int KarpowerID
		{
			get
			{
				return this.karpowerID;
			}
			set
			{
				this.karpowerID = value;
			}
		}

		public string License
		{
			get
			{
				return this.license;
			}
			set
			{
				this.license = value;
			}
		}

		public string Make
		{
			get
			{
				return this.make;
			}
			set
			{
				this.make = value;
			}
		}

		public float MaxDeduct
		{
			get
			{
				return this.maxDeduct;
			}
			set
			{
				this.maxDeduct = value;
			}
		}

		public float MileAdjust
		{
			get
			{
				return this.mileAdjust;
			}
			set
			{
				this.mileAdjust = value;
			}
		}

		public int Mileage
		{
			get
			{
				return this.mileage;
			}
			set
			{
				this.mileage = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public float NoMilesRetail
		{
			get
			{
				return this.noMilesRetail;
			}
			set
			{
				this.noMilesRetail = value;
			}
		}

		public float NoMilesWholesale
		{
			get
			{
				return this.noMilesWholesale;
			}
			set
			{
				this.noMilesWholesale = value;
			}
		}

		public string OptionList
		{
			get
			{
				return this.optionList;
			}
			set
			{
				this.optionList = value;
			}
		}

		public float PriceCost
		{
			get
			{
				return this.priceCost;
			}
			set
			{
				this.priceCost = value;
			}
		}

		public float PriceRecon
		{
			get
			{
				return this.priceRecon;
			}
			set
			{
				this.priceRecon = value;
			}
		}

		public float PriceSelling
		{
			get
			{
				return this.priceSelling;
			}
			set
			{
				this.priceSelling = value;
			}
		}

		public int RawKbbKarpowerId
		{
			get
			{
				return this.rawKbbKarpowerId;
			}
			set
			{
				this.rawKbbKarpowerId = value;
			}
		}

		public string StockNo
		{
			get
			{
				return this.stockNo;
			}
			set
			{
				this.stockNo = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public float TransRetail
		{
			get
			{
				return this.transRetail;
			}
			set
			{
				this.transRetail = value;
			}
		}

		public float TransWholesale
		{
			get
			{
				return this.transWholesale;
			}
			set
			{
				this.transWholesale = value;
			}
		}

		public string UnknownField18
		{
			get
			{
				return this.unknownField18;
			}
			set
			{
				this.unknownField18 = value;
			}
		}

		public string UnknownField32
		{
			get
			{
				return this.unknownField32;
			}
			set
			{
				this.unknownField32 = value;
			}
		}

		public string UnknownField33
		{
			get
			{
				return this.unknownField33;
			}
			set
			{
				this.unknownField33 = value;
			}
		}

		public string VIN
		{
			get
			{
				return this.vIN;
			}
			set
			{
				this.vIN = value;
			}
		}

		public int Year
		{
			get
			{
				return this.year;
			}
			set
			{
				this.year = value;
			}
		}

		public RawKbbKarpower()
		{
		}

		public RawKbbKarpower(string connection, string modifiedUserID, int rawKbbKarpowerId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.RawKbbKarpowerId = rawKbbKarpowerId;
			this.databaseObjectName = "RAW_KBB_KARPOWER";
			if (this.RawKbbKarpowerId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawKbbKarpowerId", DataAccessParameterType.Numeric, this.RawKbbKarpowerId.ToString());
			data.ExecuteProcedure("RAW_KBB_KARPOWER_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawKbbKarpowerId", DataAccessParameterType.Numeric, this.RawKbbKarpowerId.ToString());
			data.ExecuteProcedure("RAW_KBB_KARPOWER_GetRecord");
			if (!data.EOF)
			{
				this.RawKbbKarpowerId = int.Parse(data["RawKbbKarpowerId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerID = data["DealerID"];
				this.KarpowerID = int.Parse(data["KarpowerID"]);
				this.StockNo = data["StockNo"];
				this.DateStored = data["DateStored"];
				this.Year = int.Parse(data["Year"]);
				this.Make = data["Make"];
				this.Model = data["Model"];
				this.Engine = data["Engine"];
				this.Transmission = data["Transmission"];
				this.DriveTrain = data["DriveTrain"];
				this.Color = data["Color"];
				this.Mileage = int.Parse(data["Mileage"]);
				this.VIN = data["VIN"];
				this.License = data["License"];
				this.PriceRecon = float.Parse(data["PriceRecon"]);
				this.PriceSelling = float.Parse(data["PriceSelling"]);
				this.PriceCost = float.Parse(data["PriceCost"]);
				this.DateBook = data["DateBook"];
				this.UnknownField18 = data["UnknownField18"];
				this.Classic = data["Classic"];
				this.BaseWholesale = float.Parse(data["BaseWholesale"]);
				this.BaseRetail = float.Parse(data["BaseRetail"]);
				this.EngineWholesale = float.Parse(data["EngineWholesale"]);
				this.EngineRetail = float.Parse(data["EngineRetail"]);
				this.TransWholesale = float.Parse(data["TransWholesale"]);
				this.TransRetail = float.Parse(data["TransRetail"]);
				this.DriveWholesale = float.Parse(data["DriveWholesale"]);
				this.DriveRetail = float.Parse(data["DriveRetail"]);
				this.MileAdjust = float.Parse(data["MileAdjust"]);
				this.NoMilesWholesale = float.Parse(data["NoMilesWholesale"]);
				this.NoMilesRetail = float.Parse(data["NoMilesRetail"]);
				this.MaxDeduct = float.Parse(data["MaxDeduct"]);
				this.UnknownField32 = data["UnknownField32"];
				this.UnknownField33 = data["UnknownField33"];
				this.FullWholesale = float.Parse(data["FullWholesale"]);
				this.FullRetail = float.Parse(data["FullRetail"]);
				this.DateUpdated = data["DateUpdated"];
				this.OptionList = data["OptionList"];
				this.ImageUrlList = data["ImageUrlList"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawKbbKarpowerId = this.RawKbbKarpowerId;
			data.AddParam("@RawKbbKarpowerId", DataAccessParameterType.Numeric, rawKbbKarpowerId.ToString());
			rawKbbKarpowerId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawKbbKarpowerId.ToString());
			data.AddParam("@DealerID", DataAccessParameterType.Text, this.DealerID);
			rawKbbKarpowerId = this.KarpowerID;
			data.AddParam("@KarpowerID", DataAccessParameterType.Numeric, rawKbbKarpowerId.ToString());
			data.AddParam("@StockNo", DataAccessParameterType.Text, this.StockNo);
			data.AddParam("@DateStored", DataAccessParameterType.Text, this.DateStored);
			rawKbbKarpowerId = this.Year;
			data.AddParam("@Year", DataAccessParameterType.Numeric, rawKbbKarpowerId.ToString());
			data.AddParam("@Make", DataAccessParameterType.Text, this.Make);
			data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
			data.AddParam("@Engine", DataAccessParameterType.Text, this.Engine);
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			data.AddParam("@DriveTrain", DataAccessParameterType.Text, this.DriveTrain);
			data.AddParam("@Color", DataAccessParameterType.Text, this.Color);
			rawKbbKarpowerId = this.Mileage;
			data.AddParam("@Mileage", DataAccessParameterType.Numeric, rawKbbKarpowerId.ToString());
			data.AddParam("@VIN", DataAccessParameterType.Text, this.VIN);
			data.AddParam("@License", DataAccessParameterType.Text, this.License);
			float priceRecon = this.PriceRecon;
			data.AddParam("@PriceRecon", DataAccessParameterType.Numeric, priceRecon.ToString());
			priceRecon = this.PriceSelling;
			data.AddParam("@PriceSelling", DataAccessParameterType.Numeric, priceRecon.ToString());
			priceRecon = this.PriceCost;
			data.AddParam("@PriceCost", DataAccessParameterType.Numeric, priceRecon.ToString());
			data.AddParam("@DateBook", DataAccessParameterType.Text, this.DateBook);
			data.AddParam("@UnknownField18", DataAccessParameterType.Text, this.UnknownField18);
			data.AddParam("@Classic", DataAccessParameterType.Text, this.Classic);
			priceRecon = this.BaseWholesale;
			data.AddParam("@BaseWholesale", DataAccessParameterType.Numeric, priceRecon.ToString());
			priceRecon = this.BaseRetail;
			data.AddParam("@BaseRetail", DataAccessParameterType.Numeric, priceRecon.ToString());
			priceRecon = this.EngineWholesale;
			data.AddParam("@EngineWholesale", DataAccessParameterType.Numeric, priceRecon.ToString());
			priceRecon = this.EngineRetail;
			data.AddParam("@EngineRetail", DataAccessParameterType.Numeric, priceRecon.ToString());
			priceRecon = this.TransWholesale;
			data.AddParam("@TransWholesale", DataAccessParameterType.Numeric, priceRecon.ToString());
			priceRecon = this.TransRetail;
			data.AddParam("@TransRetail", DataAccessParameterType.Numeric, priceRecon.ToString());
			priceRecon = this.DriveWholesale;
			data.AddParam("@DriveWholesale", DataAccessParameterType.Numeric, priceRecon.ToString());
			priceRecon = this.DriveRetail;
			data.AddParam("@DriveRetail", DataAccessParameterType.Numeric, priceRecon.ToString());
			priceRecon = this.MileAdjust;
			data.AddParam("@MileAdjust", DataAccessParameterType.Numeric, priceRecon.ToString());
			priceRecon = this.NoMilesWholesale;
			data.AddParam("@NoMilesWholesale", DataAccessParameterType.Numeric, priceRecon.ToString());
			priceRecon = this.NoMilesRetail;
			data.AddParam("@NoMilesRetail", DataAccessParameterType.Numeric, priceRecon.ToString());
			priceRecon = this.MaxDeduct;
			data.AddParam("@MaxDeduct", DataAccessParameterType.Numeric, priceRecon.ToString());
			data.AddParam("@UnknownField32", DataAccessParameterType.Text, this.UnknownField32);
			data.AddParam("@UnknownField33", DataAccessParameterType.Text, this.UnknownField33);
			priceRecon = this.FullWholesale;
			data.AddParam("@FullWholesale", DataAccessParameterType.Numeric, priceRecon.ToString());
			priceRecon = this.FullRetail;
			data.AddParam("@FullRetail", DataAccessParameterType.Numeric, priceRecon.ToString());
			data.AddParam("@DateUpdated", DataAccessParameterType.Text, this.DateUpdated);
			data.AddParam("@OptionList", DataAccessParameterType.Text, this.OptionList);
			data.AddParam("@ImageUrlList", DataAccessParameterType.Text, this.ImageUrlList);
			data.ExecuteProcedure("RAW_KBB_KARPOWER_InsertUpdate");
			if (!data.EOF)
			{
				this.RawKbbKarpowerId = int.Parse(data["RawKbbKarpowerId"]);
			}
			this.retrievedata();
		}
	}
}