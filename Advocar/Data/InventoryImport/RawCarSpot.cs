using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawCarSpot : Transaction
	{
		private int rawCarSpotId = 0;

		private int jobExecutionRawId = 0;

		private string dealerID = "";

		private string stockID = "";

		private int year = 0;

		private string vIN = "";

		private string newUsed = "";

		private string make = "";

		private string model = "";

		private string trim = "";

		private string modelNumber = "";

		private string bodyStyle = "";

		private int mileage = 0;

		private string extColor = "";

		private string intColor = "";

		private string transmission = "";

		private int doors = 0;

		private float sellingPrice = 0f;

		private float mSRPPrice = 0f;

		private float invoicePrice = 0f;

		private string dateEntered = "";

		private string certifiedYN = "N";

		private string engine = "";

		private string fuelCode = "";

		private string driveTrain = "";

		private string restraint = "";

		private string dealerComment = "";

		private string vehicleComment = "";

		private string imageUrls = "";

		private string packages = "";

		private string options = "";

		public string BodyStyle
		{
			get
			{
				return this.bodyStyle;
			}
			set
			{
				this.bodyStyle = value;
			}
		}

		public string CertifiedYN
		{
			get
			{
				return this.certifiedYN;
			}
			set
			{
				this.certifiedYN = (value == "Y" ? "Y" : "N");
			}
		}

		public string DateEntered
		{
			get
			{
				return this.dateEntered;
			}
			set
			{
				this.dateEntered = value;
			}
		}

		public string DealerComment
		{
			get
			{
				return this.dealerComment;
			}
			set
			{
				this.dealerComment = value;
			}
		}

		public string DealerID
		{
			get
			{
				return this.dealerID;
			}
			set
			{
				this.dealerID = value;
			}
		}

		public int Doors
		{
			get
			{
				return this.doors;
			}
			set
			{
				this.doors = value;
			}
		}

		public string DriveTrain
		{
			get
			{
				return this.driveTrain;
			}
			set
			{
				this.driveTrain = value;
			}
		}

		public string Engine
		{
			get
			{
				return this.engine;
			}
			set
			{
				this.engine = value;
			}
		}

		public string ExtColor
		{
			get
			{
				return this.extColor;
			}
			set
			{
				this.extColor = value;
			}
		}

		public string FuelCode
		{
			get
			{
				return this.fuelCode;
			}
			set
			{
				this.fuelCode = value;
			}
		}

		public string ImageUrls
		{
			get
			{
				return this.imageUrls;
			}
			set
			{
				this.imageUrls = value;
			}
		}

		public string IntColor
		{
			get
			{
				return this.intColor;
			}
			set
			{
				this.intColor = value;
			}
		}

		public float InvoicePrice
		{
			get
			{
				return this.invoicePrice;
			}
			set
			{
				this.invoicePrice = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public string Make
		{
			get
			{
				return this.make;
			}
			set
			{
				this.make = value;
			}
		}

		public int Mileage
		{
			get
			{
				return this.mileage;
			}
			set
			{
				this.mileage = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public string ModelNumber
		{
			get
			{
				return this.modelNumber;
			}
			set
			{
				this.modelNumber = value;
			}
		}

		public float MSRPPrice
		{
			get
			{
				return this.mSRPPrice;
			}
			set
			{
				this.mSRPPrice = value;
			}
		}

		public string NewUsed
		{
			get
			{
				return this.newUsed;
			}
			set
			{
				this.newUsed = value;
			}
		}

		public string Options
		{
			get
			{
				return this.options;
			}
			set
			{
				this.options = value;
			}
		}

		public string Packages
		{
			get
			{
				return this.packages;
			}
			set
			{
				this.packages = value;
			}
		}

		public int RawCarSpotId
		{
			get
			{
				return this.rawCarSpotId;
			}
			set
			{
				this.rawCarSpotId = value;
			}
		}

		public string Restraint
		{
			get
			{
				return this.restraint;
			}
			set
			{
				this.restraint = value;
			}
		}

		public float SellingPrice
		{
			get
			{
				return this.sellingPrice;
			}
			set
			{
				this.sellingPrice = value;
			}
		}

		public string StockID
		{
			get
			{
				return this.stockID;
			}
			set
			{
				this.stockID = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string Trim
		{
			get
			{
				return this.trim;
			}
			set
			{
				this.trim = value;
			}
		}

		public string VehicleComment
		{
			get
			{
				return this.vehicleComment;
			}
			set
			{
				this.vehicleComment = value;
			}
		}

		public string VIN
		{
			get
			{
				return this.vIN;
			}
			set
			{
				this.vIN = value;
			}
		}

		public int Year
		{
			get
			{
				return this.year;
			}
			set
			{
				this.year = value;
			}
		}

		public RawCarSpot()
		{
		}

		public RawCarSpot(string connection, string modifiedUserID, int rawCarSpotId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.RawCarSpotId = rawCarSpotId;
			this.databaseObjectName = "RAW_CAR_SPOT";
			if (this.RawCarSpotId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawCarSpotId", DataAccessParameterType.Numeric, this.RawCarSpotId.ToString());
			data.ExecuteProcedure("RAW_CAR_SPOT_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawCarSpotId", DataAccessParameterType.Numeric, this.RawCarSpotId.ToString());
			data.ExecuteProcedure("RAW_CAR_SPOT_GetRecord");
			if (!data.EOF)
			{
				this.RawCarSpotId = int.Parse(data["RawCarSpotId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerID = data["DealerID"];
				this.StockID = data["StockID"];
				this.Year = int.Parse(data["Year"]);
				this.VIN = data["VIN"];
				this.NewUsed = data["NewUsed"];
				this.Make = data["Make"];
				this.Model = data["Model"];
				this.Trim = data["Trim"];
				this.ModelNumber = data["ModelNumber"];
				this.BodyStyle = data["BodyStyle"];
				this.Mileage = int.Parse(data["Mileage"]);
				this.ExtColor = data["ExtColor"];
				this.IntColor = data["IntColor"];
				this.Transmission = data["Transmission"];
				this.Doors = int.Parse(data["Doors"]);
				this.SellingPrice = float.Parse(data["SellingPrice"]);
				this.MSRPPrice = float.Parse(data["MSRPPrice"]);
				this.InvoicePrice = float.Parse(data["InvoicePrice"]);
				this.DateEntered = data["DateEntered"];
				this.CertifiedYN = data["CertifiedYN"];
				this.Engine = data["Engine"];
				this.FuelCode = data["FuelCode"];
				this.DriveTrain = data["DriveTrain"];
				this.Restraint = data["Restraint"];
				this.DealerComment = data["DealerComment"];
				this.VehicleComment = data["VehicleComment"];
				this.ImageUrls = data["ImageUrls"];
				this.Packages = data["Packages"];
				this.Options = data["Options"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawCarSpotId = this.RawCarSpotId;
			data.AddParam("@RawCarSpotId", DataAccessParameterType.Numeric, rawCarSpotId.ToString());
			rawCarSpotId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawCarSpotId.ToString());
			data.AddParam("@DealerID", DataAccessParameterType.Text, this.DealerID);
			data.AddParam("@StockID", DataAccessParameterType.Text, this.StockID);
			rawCarSpotId = this.Year;
			data.AddParam("@Year", DataAccessParameterType.Numeric, rawCarSpotId.ToString());
			data.AddParam("@VIN", DataAccessParameterType.Text, this.VIN);
			data.AddParam("@NewUsed", DataAccessParameterType.Text, this.NewUsed);
			data.AddParam("@Make", DataAccessParameterType.Text, this.Make);
			data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
			data.AddParam("@Trim", DataAccessParameterType.Text, this.Trim);
			data.AddParam("@ModelNumber", DataAccessParameterType.Text, this.ModelNumber);
			data.AddParam("@BodyStyle", DataAccessParameterType.Text, this.BodyStyle);
			rawCarSpotId = this.Mileage;
			data.AddParam("@Mileage", DataAccessParameterType.Numeric, rawCarSpotId.ToString());
			data.AddParam("@ExtColor", DataAccessParameterType.Text, this.ExtColor);
			data.AddParam("@IntColor", DataAccessParameterType.Text, this.IntColor);
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			rawCarSpotId = this.Doors;
			data.AddParam("@Doors", DataAccessParameterType.Numeric, rawCarSpotId.ToString());
			float sellingPrice = this.SellingPrice;
			data.AddParam("@SellingPrice", DataAccessParameterType.Numeric, sellingPrice.ToString());
			sellingPrice = this.MSRPPrice;
			data.AddParam("@MSRPPrice", DataAccessParameterType.Numeric, sellingPrice.ToString());
			sellingPrice = this.InvoicePrice;
			data.AddParam("@InvoicePrice", DataAccessParameterType.Numeric, sellingPrice.ToString());
			data.AddParam("@DateEntered", DataAccessParameterType.Text, this.DateEntered);
			data.AddParam("@CertifiedYN", DataAccessParameterType.Text, this.CertifiedYN);
			data.AddParam("@Engine", DataAccessParameterType.Text, this.Engine);
			data.AddParam("@FuelCode", DataAccessParameterType.Text, this.FuelCode);
			data.AddParam("@DriveTrain", DataAccessParameterType.Text, this.DriveTrain);
			data.AddParam("@Restraint", DataAccessParameterType.Text, this.Restraint);
			data.AddParam("@DealerComment", DataAccessParameterType.Text, this.DealerComment);
			data.AddParam("@VehicleComment", DataAccessParameterType.Text, this.VehicleComment);
			data.AddParam("@ImageUrls", DataAccessParameterType.Text, this.ImageUrls);
			data.AddParam("@Packages", DataAccessParameterType.Text, this.Packages);
			data.AddParam("@Options", DataAccessParameterType.Text, this.Options);
			data.ExecuteProcedure("RAW_CAR_SPOT_InsertUpdate");
			if (!data.EOF)
			{
				this.RawCarSpotId = int.Parse(data["RawCarSpotId"]);
			}
			this.retrievedata();
		}
	}
}