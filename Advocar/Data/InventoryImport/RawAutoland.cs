using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawAutoland : Transaction
	{
		private int rawAutolandId = 0;

		private int processingTransactionId = 0;

		private string dealerID = "";

		private string newUsed = "";

		private string dealerStockNo = "";

		private string vehicleDescription = "";

		private int vehicleYear = 0;

		private string vehicleMake = "";

		private string vehicleModel = "";

		private string vehicleTrim = "";

		private string vin = "";

		private string vehicleStyle = "";

		private string engine = "";

		private string drivetrain = "";

		private string transmission = "";

		private string exteriorColor = "";

		private string interiorColor = "";

		private string optionList = "";

		private int miles = 0;

		private float priceAcquisition = 0f;

		private float priceInvoice = 0f;

		private float priceSelling = 0f;

		private float priceRetail = 0f;

		private string priceMarketIndex = "";

		private float priceMsrp = 0f;

		private string imageUrls = "";

		private string warranty = "";

		public string DealerID
		{
			get
			{
				return this.dealerID;
			}
			set
			{
				this.dealerID = value;
			}
		}

		public string DealerStockNo
		{
			get
			{
				return this.dealerStockNo;
			}
			set
			{
				this.dealerStockNo = value;
			}
		}

		public string Drivetrain
		{
			get
			{
				return this.drivetrain;
			}
			set
			{
				this.drivetrain = value;
			}
		}

		public string Engine
		{
			get
			{
				return this.engine;
			}
			set
			{
				this.engine = value;
			}
		}

		public string ExteriorColor
		{
			get
			{
				return this.exteriorColor;
			}
			set
			{
				this.exteriorColor = value;
			}
		}

		public string ImageUrls
		{
			get
			{
				return this.imageUrls;
			}
			set
			{
				this.imageUrls = value;
			}
		}

		public string InteriorColor
		{
			get
			{
				return this.interiorColor;
			}
			set
			{
				this.interiorColor = value;
			}
		}

		public int Miles
		{
			get
			{
				return this.miles;
			}
			set
			{
				this.miles = value;
			}
		}

		public string NewUsed
		{
			get
			{
				return this.newUsed;
			}
			set
			{
				this.newUsed = value;
			}
		}

		public string OptionList
		{
			get
			{
				return this.optionList;
			}
			set
			{
				this.optionList = value;
			}
		}

		public float PriceAcquisition
		{
			get
			{
				return this.priceAcquisition;
			}
			set
			{
				this.priceAcquisition = value;
			}
		}

		public float PriceInvoice
		{
			get
			{
				return this.priceInvoice;
			}
			set
			{
				this.priceInvoice = value;
			}
		}

		public string PriceMarketIndex
		{
			get
			{
				return this.priceMarketIndex;
			}
			set
			{
				this.priceMarketIndex = value;
			}
		}

		public float PriceMsrp
		{
			get
			{
				return this.priceMsrp;
			}
			set
			{
				this.priceMsrp = value;
			}
		}

		public float PriceRetail
		{
			get
			{
				return this.priceRetail;
			}
			set
			{
				this.priceRetail = value;
			}
		}

		public float PriceSelling
		{
			get
			{
				return this.priceSelling;
			}
			set
			{
				this.priceSelling = value;
			}
		}

		public int ProcessingTransactionId
		{
			get
			{
				return this.processingTransactionId;
			}
			set
			{
				this.processingTransactionId = value;
			}
		}

		public int RawAutolandId
		{
			get
			{
				return this.rawAutolandId;
			}
			set
			{
				this.rawAutolandId = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string VehicleDescription
		{
			get
			{
				return this.vehicleDescription;
			}
			set
			{
				this.vehicleDescription = value;
			}
		}

		public string VehicleMake
		{
			get
			{
				return this.vehicleMake;
			}
			set
			{
				this.vehicleMake = value;
			}
		}

		public string VehicleModel
		{
			get
			{
				return this.vehicleModel;
			}
			set
			{
				this.vehicleModel = value;
			}
		}

		public string VehicleStyle
		{
			get
			{
				return this.vehicleStyle;
			}
			set
			{
				this.vehicleStyle = value;
			}
		}

		public string VehicleTrim
		{
			get
			{
				return this.vehicleTrim;
			}
			set
			{
				this.vehicleTrim = value;
			}
		}

		public int VehicleYear
		{
			get
			{
				return this.vehicleYear;
			}
			set
			{
				this.vehicleYear = value;
			}
		}

		public string Vin
		{
			get
			{
				return this.vin;
			}
			set
			{
				this.vin = value;
			}
		}

		public string Warranty
		{
			get
			{
				return this.warranty;
			}
			set
			{
				this.warranty = value;
			}
		}

		public RawAutoland()
		{
		}

		public RawAutoland(string connection, string modifiedUserID, int rawAutolandId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.RawAutolandId = rawAutolandId;
			if (this.RawAutolandId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawAutolandId", DataAccessParameterType.Numeric, this.RawAutolandId.ToString());
			data.ExecuteProcedure("RAW_AUTOLAND_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawAutolandId", DataAccessParameterType.Numeric, this.RawAutolandId.ToString());
			data.ExecuteProcedure("RAW_AUTOLAND_GetRecord");
			if (!data.EOF)
			{
				this.RawAutolandId = int.Parse(data["RawAutolandId"]);
				this.ProcessingTransactionId = int.Parse(data["ProcessingTransactionId"]);
				this.DealerID = data["DealerID"];
				this.NewUsed = data["NewUsed"];
				this.DealerStockNo = data["DealerStockNo"];
				this.VehicleDescription = data["VehicleDescription"];
				this.VehicleYear = int.Parse(data["VehicleYear"]);
				this.VehicleMake = data["VehicleMake"];
				this.VehicleModel = data["VehicleModel"];
				this.VehicleTrim = data["VehicleTrim"];
				this.Vin = data["Vin"];
				this.VehicleStyle = data["VehicleStyle"];
				this.Engine = data["Engine"];
				this.Drivetrain = data["Drivetrain"];
				this.Transmission = data["Transmission"];
				this.ExteriorColor = data["ExteriorColor"];
				this.InteriorColor = data["InteriorColor"];
				this.OptionList = data["OptionList"];
				this.Miles = int.Parse(data["Miles"]);
				this.PriceAcquisition = float.Parse(data["PriceAcquisition"]);
				this.PriceInvoice = float.Parse(data["PriceInvoice"]);
				this.PriceSelling = float.Parse(data["PriceSelling"]);
				this.PriceRetail = float.Parse(data["PriceRetail"]);
				this.PriceMarketIndex = data["PriceMarketIndex"];
				this.PriceMsrp = float.Parse(data["PriceMsrp"]);
				this.ImageUrls = data["ImageUrls"];
				this.Warranty = data["Warranty"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawAutolandId = this.RawAutolandId;
			data.AddParam("@RawAutolandId", DataAccessParameterType.Numeric, rawAutolandId.ToString());
			rawAutolandId = this.ProcessingTransactionId;
			data.AddParam("@ProcessingTransactionId", DataAccessParameterType.Numeric, rawAutolandId.ToString());
			data.AddParam("@DealerID", DataAccessParameterType.Text, this.DealerID);
			data.AddParam("@NewUsed", DataAccessParameterType.Text, this.NewUsed);
			data.AddParam("@DealerStockNo", DataAccessParameterType.Text, this.DealerStockNo);
			data.AddParam("@VehicleDescription", DataAccessParameterType.Text, this.VehicleDescription);
			rawAutolandId = this.VehicleYear;
			data.AddParam("@VehicleYear", DataAccessParameterType.Numeric, rawAutolandId.ToString());
			data.AddParam("@VehicleMake", DataAccessParameterType.Text, this.VehicleMake);
			data.AddParam("@VehicleModel", DataAccessParameterType.Text, this.VehicleModel);
			data.AddParam("@VehicleTrim", DataAccessParameterType.Text, this.VehicleTrim);
			data.AddParam("@Vin", DataAccessParameterType.Text, this.Vin);
			data.AddParam("@VehicleStyle", DataAccessParameterType.Text, this.VehicleStyle);
			data.AddParam("@Engine", DataAccessParameterType.Text, this.Engine);
			data.AddParam("@Drivetrain", DataAccessParameterType.Text, this.Drivetrain);
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			data.AddParam("@ExteriorColor", DataAccessParameterType.Text, this.ExteriorColor);
			data.AddParam("@InteriorColor", DataAccessParameterType.Text, this.InteriorColor);
			data.AddParam("@OptionList", DataAccessParameterType.Text, this.OptionList);
			rawAutolandId = this.Miles;
			data.AddParam("@Miles", DataAccessParameterType.Numeric, rawAutolandId.ToString());
			float priceAcquisition = this.PriceAcquisition;
			data.AddParam("@PriceAcquisition", DataAccessParameterType.Numeric, priceAcquisition.ToString());
			priceAcquisition = this.PriceInvoice;
			data.AddParam("@PriceInvoice", DataAccessParameterType.Numeric, priceAcquisition.ToString());
			priceAcquisition = this.PriceSelling;
			data.AddParam("@PriceSelling", DataAccessParameterType.Numeric, priceAcquisition.ToString());
			priceAcquisition = this.PriceRetail;
			data.AddParam("@PriceRetail", DataAccessParameterType.Numeric, priceAcquisition.ToString());
			data.AddParam("@PriceMarketIndex", DataAccessParameterType.Text, this.PriceMarketIndex);
			priceAcquisition = this.PriceMsrp;
			data.AddParam("@PriceMsrp", DataAccessParameterType.Numeric, priceAcquisition.ToString());
			data.AddParam("@ImageUrls", DataAccessParameterType.Text, this.ImageUrls);
			data.AddParam("@Warranty", DataAccessParameterType.Text, this.Warranty);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("RAW_AUTOLAND_InsertUpdate");
			if (!data.EOF)
			{
				this.RawAutolandId = int.Parse(data["RawAutolandId"]);
			}
			this.retrievedata();
		}
	}
}