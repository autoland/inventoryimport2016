using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawAutobytel : Transaction
	{
		private int rawAutobytelId = 0;

		private int jobExecutionRawId = 0;

		private string dealerId = "";

		private int vehicleId = 0;

		private string vIN = "";

		private string vehicleYear = "";

		private string vehicleMake = "";

		private string vehicleModel = "";

		private string vehicleTrim = "";

		private float priceSelling = 0f;

		private int mileage = 0;

		private string exteriorColor = "";

		private string options = "";

		private string features = "";

		private string city = "";

		private string state = "";

		private string zipCode = "";

		private string imageUrl = "";

		private bool hasOtherImages = false;

		public string City
		{
			get
			{
				return this.city;
			}
			set
			{
				this.city = value;
			}
		}

		public string DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public string ExteriorColor
		{
			get
			{
				return this.exteriorColor;
			}
			set
			{
				this.exteriorColor = value;
			}
		}

		public string Features
		{
			get
			{
				return this.features;
			}
			set
			{
				this.features = value;
			}
		}

		public bool HasOtherImages
		{
			get
			{
				return this.hasOtherImages;
			}
			set
			{
				this.hasOtherImages = value;
			}
		}

		public string ImageUrl
		{
			get
			{
				return this.imageUrl;
			}
			set
			{
				this.imageUrl = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public int Mileage
		{
			get
			{
				return this.mileage;
			}
			set
			{
				this.mileage = value;
			}
		}

		public string Options
		{
			get
			{
				return this.options;
			}
			set
			{
				this.options = value;
			}
		}

		public float PriceSelling
		{
			get
			{
				return this.priceSelling;
			}
			set
			{
				this.priceSelling = value;
			}
		}

		public int RawAutobytelId
		{
			get
			{
				return this.rawAutobytelId;
			}
			set
			{
				this.rawAutobytelId = value;
			}
		}

		public string State
		{
			get
			{
				return this.state;
			}
			set
			{
				this.state = value;
			}
		}

		public int VehicleId
		{
			get
			{
				return this.vehicleId;
			}
			set
			{
				this.vehicleId = value;
			}
		}

		public string VehicleMake
		{
			get
			{
				return this.vehicleMake;
			}
			set
			{
				this.vehicleMake = value;
			}
		}

		public string VehicleModel
		{
			get
			{
				return this.vehicleModel;
			}
			set
			{
				this.vehicleModel = value;
			}
		}

		public string VehicleTrim
		{
			get
			{
				return this.vehicleTrim;
			}
			set
			{
				this.vehicleTrim = value;
			}
		}

		public string VehicleYear
		{
			get
			{
				return this.vehicleYear;
			}
			set
			{
				this.vehicleYear = value;
			}
		}

		public string VIN
		{
			get
			{
				return this.vIN;
			}
			set
			{
				this.vIN = value;
			}
		}

		public string ZipCode
		{
			get
			{
				return this.zipCode;
			}
			set
			{
				this.zipCode = value;
			}
		}

		public RawAutobytel()
		{
		}

		public RawAutobytel(string connection, string modifiedUserID, int rawAutobytelId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "RAW_AUTOBYTEL";
			this.RawAutobytelId = rawAutobytelId;
			if (this.RawAutobytelId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawAutobytelId", DataAccessParameterType.Numeric, this.RawAutobytelId.ToString());
			data.ExecuteProcedure("RAW_AUTOBYTEL_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawAutobytelId", DataAccessParameterType.Numeric, this.RawAutobytelId.ToString());
			data.ExecuteProcedure("RAW_AUTOBYTEL_GetRecord");
			if (!data.EOF)
			{
				this.RawAutobytelId = int.Parse(data["RawAutobytelId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerId = data["DealerId"];
				this.VehicleId = int.Parse(data["VehicleId"]);
				this.VIN = data["VIN"];
				this.VehicleYear = data["VehicleYear"];
				this.VehicleMake = data["VehicleMake"];
				this.VehicleModel = data["VehicleModel"];
				this.VehicleTrim = data["VehicleTrim"];
				this.PriceSelling = float.Parse(data["PriceSelling"]);
				this.Mileage = int.Parse(data["Mileage"]);
				this.ExteriorColor = data["ExteriorColor"];
				this.Options = data["Options"];
				this.Features = data["Features"];
				this.City = data["City"];
				this.State = data["State"];
				this.ZipCode = data["ZipCode"];
				this.ImageUrl = data["ImageUrl"];
				this.HasOtherImages = bool.Parse(data["HasOtherImages"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawAutobytelId = this.RawAutobytelId;
			data.AddParam("@RawAutobytelId", DataAccessParameterType.Numeric, rawAutobytelId.ToString());
			rawAutobytelId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawAutobytelId.ToString());
			data.AddParam("@DealerId", DataAccessParameterType.Text, this.DealerId);
			rawAutobytelId = this.VehicleId;
			data.AddParam("@VehicleId", DataAccessParameterType.Numeric, rawAutobytelId.ToString());
			data.AddParam("@VIN", DataAccessParameterType.Text, this.VIN);
			data.AddParam("@VehicleYear", DataAccessParameterType.Text, this.VehicleYear);
			data.AddParam("@VehicleMake", DataAccessParameterType.Text, this.VehicleMake);
			data.AddParam("@VehicleModel", DataAccessParameterType.Text, this.VehicleModel);
			data.AddParam("@VehicleTrim", DataAccessParameterType.Text, this.VehicleTrim);
			data.AddParam("@PriceSelling", DataAccessParameterType.Numeric, this.PriceSelling.ToString());
			rawAutobytelId = this.Mileage;
			data.AddParam("@Mileage", DataAccessParameterType.Numeric, rawAutobytelId.ToString());
			data.AddParam("@ExteriorColor", DataAccessParameterType.Text, this.ExteriorColor);
			data.AddParam("@Options", DataAccessParameterType.Text, this.Options);
			data.AddParam("@Features", DataAccessParameterType.Text, this.Features);
			data.AddParam("@City", DataAccessParameterType.Text, this.City);
			data.AddParam("@State", DataAccessParameterType.Text, this.State);
			data.AddParam("@ZipCode", DataAccessParameterType.Text, this.ZipCode);
			data.AddParam("@ImageUrl", DataAccessParameterType.Text, this.ImageUrl);
			data.AddParam("@HasOtherImages", DataAccessParameterType.Bool, this.HasOtherImages.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("RAW_AUTOBYTEL_InsertUpdate");
			if (!data.EOF)
			{
				this.RawAutobytelId = int.Parse(data["RawAutobytelId"]);
			}
			this.retrievedata();
		}
	}
}