using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawKbbKarpowerWeb : Transaction
	{
		private int rawKbbKarpowerWebId = 0;

		private int jobExecutionRawId = 0;

		private string dealerId = "";

		private int karpowerId = 0;

		private string dealerName = "";

		private string dealerAddress = "";

		private string dealerCity = "";

		private string dealerState = "";

		private string dealerZipCode = "";

		private string dealerCountry = "";

		private string dealerPhone = "";

		private string dealerFax = "";

		private string dealerWebsite = "";

		private string dealerEmailAddress = "";

		private string vin = "";

		private string stockNumber = "";

		private int year = 0;

		private string make = "";

		private string model = "";

		private string trim = "";

		private string modelAndTrim = "";

		private string engine = "";

		private string drivetrain = "";

		private string transmission = "";

		private int mileage = 0;

		private int mileageAdjustment = 0;

		private string currentDate = "";

		private string kbbValuationDate = "";

		private string lastChangeDate = "";

		private string bookingDate = "";

		private bool isUsed = false;

		private bool isCertified = false;

		private bool isClassic = false;

		private string exteriorColor = "";

		private string interiorColor = "";

		private float webPrice = 0f;

		private float sellingPrice = 0f;

		private float cost = 0f;

		private float tradeInValue = 0f;

		private string inventoryCategory = "";

		private float retailPrice = 0f;

		private float wholesalePrice = 0f;

		private float maxDeductRetail = 0f;

		private float maxDeductWholesale = 0f;

		private float estimatedReconditioningCost = 0f;

		private float fullDealerCost = 0f;

		private string equipment = "";

		private string dealerAdditions = "";

		private string dealerNoChargeAdditions = "";

		private string missingEquipment = "";

		private string licenseNumber = "";

		private string licensePlateExpiration = "";

		private string licensePlateState = "";

		private float soldPrice = 0f;

		private string soldDate = "";

		private string notes = "";

		private string merchandisingText = "";

		private string vehicleType = "";

		private string tradeInDate = "";

		private float invoice = 0f;

		private string imageUrlList = "";

		public string BookingDate
		{
			get
			{
				return this.bookingDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.bookingDate = "";
				}
				else
				{
					this.bookingDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public float Cost
		{
			get
			{
				return this.cost;
			}
			set
			{
				this.cost = value;
			}
		}

		public string CurrentDate
		{
			get
			{
				return this.currentDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.currentDate = "";
				}
				else
				{
					this.currentDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string DealerAdditions
		{
			get
			{
				return this.dealerAdditions;
			}
			set
			{
				this.dealerAdditions = value;
			}
		}

		public string DealerAddress
		{
			get
			{
				return this.dealerAddress;
			}
			set
			{
				this.dealerAddress = value;
			}
		}

		public string DealerCity
		{
			get
			{
				return this.dealerCity;
			}
			set
			{
				this.dealerCity = value;
			}
		}

		public string DealerCountry
		{
			get
			{
				return this.dealerCountry;
			}
			set
			{
				this.dealerCountry = value;
			}
		}

		public string DealerEmailAddress
		{
			get
			{
				return this.dealerEmailAddress;
			}
			set
			{
				this.dealerEmailAddress = value;
			}
		}

		public string DealerFax
		{
			get
			{
				return this.dealerFax;
			}
			set
			{
				this.dealerFax = value;
			}
		}

		public string DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public string DealerName
		{
			get
			{
				return this.dealerName;
			}
			set
			{
				this.dealerName = value;
			}
		}

		public string DealerNoChargeAdditions
		{
			get
			{
				return this.dealerNoChargeAdditions;
			}
			set
			{
				this.dealerNoChargeAdditions = value;
			}
		}

		public string DealerPhone
		{
			get
			{
				return this.dealerPhone;
			}
			set
			{
				this.dealerPhone = value;
			}
		}

		public string DealerState
		{
			get
			{
				return this.dealerState;
			}
			set
			{
				this.dealerState = value;
			}
		}

		public string DealerWebsite
		{
			get
			{
				return this.dealerWebsite;
			}
			set
			{
				this.dealerWebsite = value;
			}
		}

		public string DealerZipCode
		{
			get
			{
				return this.dealerZipCode;
			}
			set
			{
				this.dealerZipCode = value;
			}
		}

		public string Drivetrain
		{
			get
			{
				return this.drivetrain;
			}
			set
			{
				this.drivetrain = value;
			}
		}

		public string Engine
		{
			get
			{
				return this.engine;
			}
			set
			{
				this.engine = value;
			}
		}

		public string Equipment
		{
			get
			{
				return this.equipment;
			}
			set
			{
				this.equipment = value;
			}
		}

		public float EstimatedReconditioningCost
		{
			get
			{
				return this.estimatedReconditioningCost;
			}
			set
			{
				this.estimatedReconditioningCost = value;
			}
		}

		public string ExteriorColor
		{
			get
			{
				return this.exteriorColor;
			}
			set
			{
				this.exteriorColor = value;
			}
		}

		public float FullDealerCost
		{
			get
			{
				return this.fullDealerCost;
			}
			set
			{
				this.fullDealerCost = value;
			}
		}

		public string ImageUrlList
		{
			get
			{
				return this.imageUrlList;
			}
			set
			{
				this.imageUrlList = value;
			}
		}

		public string InteriorColor
		{
			get
			{
				return this.interiorColor;
			}
			set
			{
				this.interiorColor = value;
			}
		}

		public string InventoryCategory
		{
			get
			{
				return this.inventoryCategory;
			}
			set
			{
				this.inventoryCategory = value;
			}
		}

		public float Invoice
		{
			get
			{
				return this.invoice;
			}
			set
			{
				this.invoice = value;
			}
		}

		public bool IsCertified
		{
			get
			{
				return this.isCertified;
			}
			set
			{
				this.isCertified = value;
			}
		}

		public bool IsClassic
		{
			get
			{
				return this.isClassic;
			}
			set
			{
				this.isClassic = value;
			}
		}

		public bool IsUsed
		{
			get
			{
				return this.isUsed;
			}
			set
			{
				this.isUsed = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public int KarpowerId
		{
			get
			{
				return this.karpowerId;
			}
			set
			{
				this.karpowerId = value;
			}
		}

		public string KbbValuationDate
		{
			get
			{
				return this.kbbValuationDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.kbbValuationDate = "";
				}
				else
				{
					this.kbbValuationDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string LastChangeDate
		{
			get
			{
				return this.lastChangeDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.lastChangeDate = "";
				}
				else
				{
					this.lastChangeDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string LicenseNumber
		{
			get
			{
				return this.licenseNumber;
			}
			set
			{
				this.licenseNumber = value;
			}
		}

		public string LicensePlateExpiration
		{
			get
			{
				return this.licensePlateExpiration;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.licensePlateExpiration = "";
				}
				else
				{
					this.licensePlateExpiration = DateTime.Parse(value).ToString();
				}
			}
		}

		public string LicensePlateState
		{
			get
			{
				return this.licensePlateState;
			}
			set
			{
				this.licensePlateState = value;
			}
		}

		public string Make
		{
			get
			{
				return this.make;
			}
			set
			{
				this.make = value;
			}
		}

		public float MaxDeductRetail
		{
			get
			{
				return this.maxDeductRetail;
			}
			set
			{
				this.maxDeductRetail = value;
			}
		}

		public float MaxDeductWholesale
		{
			get
			{
				return this.maxDeductWholesale;
			}
			set
			{
				this.maxDeductWholesale = value;
			}
		}

		public string MerchandisingText
		{
			get
			{
				return this.merchandisingText;
			}
			set
			{
				this.merchandisingText = value;
			}
		}

		public int Mileage
		{
			get
			{
				return this.mileage;
			}
			set
			{
				this.mileage = value;
			}
		}

		public int MileageAdjustment
		{
			get
			{
				return this.mileageAdjustment;
			}
			set
			{
				this.mileageAdjustment = value;
			}
		}

		public string MissingEquipment
		{
			get
			{
				return this.missingEquipment;
			}
			set
			{
				this.missingEquipment = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public string ModelAndTrim
		{
			get
			{
				return this.modelAndTrim;
			}
			set
			{
				this.modelAndTrim = value;
			}
		}

		public string Notes
		{
			get
			{
				return this.notes;
			}
			set
			{
				this.notes = value;
			}
		}

		public int RawKbbKarpowerWebId
		{
			get
			{
				return this.rawKbbKarpowerWebId;
			}
			set
			{
				this.rawKbbKarpowerWebId = value;
			}
		}

		public float RetailPrice
		{
			get
			{
				return this.retailPrice;
			}
			set
			{
				this.retailPrice = value;
			}
		}

		public float SellingPrice
		{
			get
			{
				return this.sellingPrice;
			}
			set
			{
				this.sellingPrice = value;
			}
		}

		public string SoldDate
		{
			get
			{
				return this.soldDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.soldDate = "";
				}
				else
				{
					this.soldDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public float SoldPrice
		{
			get
			{
				return this.soldPrice;
			}
			set
			{
				this.soldPrice = value;
			}
		}

		public string StockNumber
		{
			get
			{
				return this.stockNumber;
			}
			set
			{
				this.stockNumber = value;
			}
		}

		public string TradeInDate
		{
			get
			{
				return this.tradeInDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.tradeInDate = "";
				}
				else
				{
					this.tradeInDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public float TradeInValue
		{
			get
			{
				return this.tradeInValue;
			}
			set
			{
				this.tradeInValue = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string Trim
		{
			get
			{
				return this.trim;
			}
			set
			{
				this.trim = value;
			}
		}

		public string VehicleType
		{
			get
			{
				return this.vehicleType;
			}
			set
			{
				this.vehicleType = value;
			}
		}

		public string Vin
		{
			get
			{
				return this.vin;
			}
			set
			{
				this.vin = value;
			}
		}

		public float WebPrice
		{
			get
			{
				return this.webPrice;
			}
			set
			{
				this.webPrice = value;
			}
		}

		public float WholesalePrice
		{
			get
			{
				return this.wholesalePrice;
			}
			set
			{
				this.wholesalePrice = value;
			}
		}

		public int Year
		{
			get
			{
				return this.year;
			}
			set
			{
				this.year = value;
			}
		}

		public RawKbbKarpowerWeb()
		{
		}

		public RawKbbKarpowerWeb(string connection, string modifiedUserID, int rawKbbKarpowerWebId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.RawKbbKarpowerWebId = rawKbbKarpowerWebId;
			this.databaseObjectName = "RAW_KBB_KARPOWER_WEB";
			if (this.RawKbbKarpowerWebId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawKbbKarpowerWebId", DataAccessParameterType.Numeric, this.RawKbbKarpowerWebId.ToString());
			data.ExecuteProcedure("RAW_KBB_KARPOWER_WEB_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawKbbKarpowerWebId", DataAccessParameterType.Numeric, this.RawKbbKarpowerWebId.ToString());
			data.ExecuteProcedure("RAW_KBB_KARPOWER_WEB_GetRecord");
			if (!data.EOF)
			{
				this.RawKbbKarpowerWebId = int.Parse(data["RawKbbKarpowerWebId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerId = data["DealerId"];
				this.KarpowerId = int.Parse(data["KarpowerId"]);
				this.DealerName = data["DealerName"];
				this.DealerAddress = data["DealerAddress"];
				this.DealerCity = data["DealerCity"];
				this.DealerState = data["DealerState"];
				this.DealerZipCode = data["DealerZipCode"];
				this.DealerCountry = data["DealerCountry"];
				this.DealerPhone = data["DealerPhone"];
				this.DealerFax = data["DealerFax"];
				this.DealerWebsite = data["DealerWebsite"];
				this.DealerEmailAddress = data["DealerEmailAddress"];
				this.Vin = data["Vin"];
				this.StockNumber = data["StockNumber"];
				this.Year = int.Parse(data["Year"]);
				this.Make = data["Make"];
				this.Model = data["Model"];
				this.Trim = data["Trim"];
				this.ModelAndTrim = data["ModelAndTrim"];
				this.Engine = data["Engine"];
				this.Drivetrain = data["Drivetrain"];
				this.Transmission = data["Transmission"];
				this.Mileage = int.Parse(data["Mileage"]);
				this.MileageAdjustment = int.Parse(data["MileageAdjustment"]);
				this.CurrentDate = data["CurrentDate"];
				this.KbbValuationDate = data["KbbValuationDate"];
				this.LastChangeDate = data["LastChangeDate"];
				this.BookingDate = data["BookingDate"];
				this.IsUsed = bool.Parse(data["IsUsed"]);
				this.IsCertified = bool.Parse(data["IsCertified"]);
				this.IsClassic = bool.Parse(data["IsClassic"]);
				this.ExteriorColor = data["ExteriorColor"];
				this.InteriorColor = data["InteriorColor"];
				this.WebPrice = float.Parse(data["WebPrice"]);
				this.SellingPrice = float.Parse(data["SellingPrice"]);
				this.Cost = float.Parse(data["Cost"]);
				this.TradeInValue = float.Parse(data["TradeInValue"]);
				this.InventoryCategory = data["InventoryCategory"];
				this.RetailPrice = float.Parse(data["RetailPrice"]);
				this.WholesalePrice = float.Parse(data["WholesalePrice"]);
				this.MaxDeductRetail = float.Parse(data["MaxDeductRetail"]);
				this.MaxDeductWholesale = float.Parse(data["MaxDeductWholesale"]);
				this.EstimatedReconditioningCost = float.Parse(data["EstimatedReconditioningCost"]);
				this.FullDealerCost = float.Parse(data["FullDealerCost"]);
				this.Equipment = data["Equipment"];
				this.DealerAdditions = data["DealerAdditions"];
				this.DealerNoChargeAdditions = data["DealerNoChargeAdditions"];
				this.MissingEquipment = data["MissingEquipment"];
				this.LicenseNumber = data["LicenseNumber"];
				this.LicensePlateExpiration = data["LicensePlateExpiration"];
				this.LicensePlateState = data["LicensePlateState"];
				this.SoldPrice = float.Parse(data["SoldPrice"]);
				this.SoldDate = data["SoldDate"];
				this.Notes = data["Notes"];
				this.MerchandisingText = data["MerchandisingText"];
				this.VehicleType = data["VehicleType"];
				this.TradeInDate = data["TradeInDate"];
				this.Invoice = float.Parse(data["Invoice"]);
				this.ImageUrlList = data["ImageUrlList"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawKbbKarpowerWebId = this.RawKbbKarpowerWebId;
			data.AddParam("@RawKbbKarpowerWebId", DataAccessParameterType.Numeric, rawKbbKarpowerWebId.ToString());
			rawKbbKarpowerWebId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawKbbKarpowerWebId.ToString());
			data.AddParam("@DealerId", DataAccessParameterType.Text, this.DealerId);
			rawKbbKarpowerWebId = this.KarpowerId;
			data.AddParam("@KarpowerId", DataAccessParameterType.Numeric, rawKbbKarpowerWebId.ToString());
			data.AddParam("@DealerName", DataAccessParameterType.Text, this.DealerName);
			data.AddParam("@DealerAddress", DataAccessParameterType.Text, this.DealerAddress);
			data.AddParam("@DealerCity", DataAccessParameterType.Text, this.DealerCity);
			data.AddParam("@DealerState", DataAccessParameterType.Text, this.DealerState);
			data.AddParam("@DealerZipCode", DataAccessParameterType.Text, this.DealerZipCode);
			data.AddParam("@DealerCountry", DataAccessParameterType.Text, this.DealerCountry);
			data.AddParam("@DealerPhone", DataAccessParameterType.Text, this.DealerPhone);
			data.AddParam("@DealerFax", DataAccessParameterType.Text, this.DealerFax);
			data.AddParam("@DealerWebsite", DataAccessParameterType.Text, this.DealerWebsite);
			data.AddParam("@DealerEmailAddress", DataAccessParameterType.Text, this.DealerEmailAddress);
			data.AddParam("@Vin", DataAccessParameterType.Text, this.Vin);
			data.AddParam("@StockNumber", DataAccessParameterType.Text, this.StockNumber);
			rawKbbKarpowerWebId = this.Year;
			data.AddParam("@Year", DataAccessParameterType.Numeric, rawKbbKarpowerWebId.ToString());
			data.AddParam("@Make", DataAccessParameterType.Text, this.Make);
			data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
			data.AddParam("@Trim", DataAccessParameterType.Text, this.Trim);
			data.AddParam("@ModelAndTrim", DataAccessParameterType.Text, this.ModelAndTrim);
			data.AddParam("@Engine", DataAccessParameterType.Text, this.Engine);
			data.AddParam("@Drivetrain", DataAccessParameterType.Text, this.Drivetrain);
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			rawKbbKarpowerWebId = this.Mileage;
			data.AddParam("@Mileage", DataAccessParameterType.Numeric, rawKbbKarpowerWebId.ToString());
			rawKbbKarpowerWebId = this.MileageAdjustment;
			data.AddParam("@MileageAdjustment", DataAccessParameterType.Numeric, rawKbbKarpowerWebId.ToString());
			data.AddParam("@CurrentDate", DataAccessParameterType.DateTime, this.CurrentDate);
			data.AddParam("@KbbValuationDate", DataAccessParameterType.DateTime, this.KbbValuationDate);
			data.AddParam("@LastChangeDate", DataAccessParameterType.DateTime, this.LastChangeDate);
			data.AddParam("@BookingDate", DataAccessParameterType.DateTime, this.BookingDate);
			bool isUsed = this.IsUsed;
			data.AddParam("@IsUsed", DataAccessParameterType.Bool, isUsed.ToString());
			isUsed = this.IsCertified;
			data.AddParam("@IsCertified", DataAccessParameterType.Bool, isUsed.ToString());
			isUsed = this.IsClassic;
			data.AddParam("@IsClassic", DataAccessParameterType.Bool, isUsed.ToString());
			data.AddParam("@ExteriorColor", DataAccessParameterType.Text, this.ExteriorColor);
			data.AddParam("@InteriorColor", DataAccessParameterType.Text, this.InteriorColor);
			float webPrice = this.WebPrice;
			data.AddParam("@WebPrice", DataAccessParameterType.Numeric, webPrice.ToString());
			webPrice = this.SellingPrice;
			data.AddParam("@SellingPrice", DataAccessParameterType.Numeric, webPrice.ToString());
			webPrice = this.Cost;
			data.AddParam("@Cost", DataAccessParameterType.Numeric, webPrice.ToString());
			webPrice = this.TradeInValue;
			data.AddParam("@TradeInValue", DataAccessParameterType.Numeric, webPrice.ToString());
			data.AddParam("@InventoryCategory", DataAccessParameterType.Text, this.InventoryCategory);
			webPrice = this.RetailPrice;
			data.AddParam("@RetailPrice", DataAccessParameterType.Numeric, webPrice.ToString());
			webPrice = this.WholesalePrice;
			data.AddParam("@WholesalePrice", DataAccessParameterType.Numeric, webPrice.ToString());
			webPrice = this.MaxDeductRetail;
			data.AddParam("@MaxDeductRetail", DataAccessParameterType.Numeric, webPrice.ToString());
			webPrice = this.MaxDeductWholesale;
			data.AddParam("@MaxDeductWholesale", DataAccessParameterType.Numeric, webPrice.ToString());
			webPrice = this.EstimatedReconditioningCost;
			data.AddParam("@EstimatedReconditioningCost", DataAccessParameterType.Numeric, webPrice.ToString());
			webPrice = this.FullDealerCost;
			data.AddParam("@FullDealerCost", DataAccessParameterType.Numeric, webPrice.ToString());
			data.AddParam("@Equipment", DataAccessParameterType.Text, this.Equipment);
			data.AddParam("@DealerAdditions", DataAccessParameterType.Text, this.DealerAdditions);
			data.AddParam("@DealerNoChargeAdditions", DataAccessParameterType.Text, this.DealerNoChargeAdditions);
			data.AddParam("@MissingEquipment", DataAccessParameterType.Text, this.MissingEquipment);
			data.AddParam("@LicenseNumber", DataAccessParameterType.Text, this.LicenseNumber);
			data.AddParam("@LicensePlateExpiration", DataAccessParameterType.DateTime, this.LicensePlateExpiration);
			data.AddParam("@LicensePlateState", DataAccessParameterType.Text, this.LicensePlateState);
			webPrice = this.SoldPrice;
			data.AddParam("@SoldPrice", DataAccessParameterType.Numeric, webPrice.ToString());
			data.AddParam("@SoldDate", DataAccessParameterType.DateTime, this.SoldDate);
			data.AddParam("@Notes", DataAccessParameterType.Text, this.Notes);
			data.AddParam("@MerchandisingText", DataAccessParameterType.Text, this.MerchandisingText);
			data.AddParam("@VehicleType", DataAccessParameterType.Text, this.VehicleType);
			data.AddParam("@TradeInDate", DataAccessParameterType.DateTime, this.TradeInDate);
			webPrice = this.Invoice;
			data.AddParam("@Invoice", DataAccessParameterType.Numeric, webPrice.ToString());
			data.AddParam("@ImageUrlList", DataAccessParameterType.Text, this.ImageUrlList);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("RAW_KBB_KARPOWER_WEB_InsertUpdate");
			if (!data.EOF)
			{
				this.RawKbbKarpowerWebId = int.Parse(data["RawKbbKarpowerWebId"]);
			}
			this.retrievedata();
		}
	}
}