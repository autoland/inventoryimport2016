using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawEbizautos : Transaction
	{
		private int rawEbizautosId = 0;

		private int jobExecutionRawId = 0;

		private int dealerId = 0;

		private string companyName = "";

		private string companyAddress = "";

		private string companyCity = "";

		private string companyState = "";

		private string companyZip = "";

		private string companyPhone = "";

		private int listingId = 0;

		private string vinNo = "";

		private string newUsed = "";

		private string stockNo = "";

		private string vehicleYear = "";

		private string make = "";

		private string model = "";

		private string bodyStyle = "";

		private string doors = "";

		private string trim = "";

		private string extColor = "";

		private string intColor = "";

		private string intSurface = "";

		private string engine = "";

		private string fuel = "";

		private string drivetrain = "";

		private string transmission = "";

		private int mileage = 0;

		private int internetPrice = 0;

		private bool isCertified = false;

		private string options = "";

		private string description = "";

		private string photoUrl = "";

		private string dateInStock = "";

		public string BodyStyle
		{
			get
			{
				return this.bodyStyle;
			}
			set
			{
				this.bodyStyle = value;
			}
		}

		public string CompanyAddress
		{
			get
			{
				return this.companyAddress;
			}
			set
			{
				this.companyAddress = value;
			}
		}

		public string CompanyCity
		{
			get
			{
				return this.companyCity;
			}
			set
			{
				this.companyCity = value;
			}
		}

		public string CompanyName
		{
			get
			{
				return this.companyName;
			}
			set
			{
				this.companyName = value;
			}
		}

		public string CompanyPhone
		{
			get
			{
				return this.companyPhone;
			}
			set
			{
				this.companyPhone = value;
			}
		}

		public string CompanyState
		{
			get
			{
				return this.companyState;
			}
			set
			{
				this.companyState = value;
			}
		}

		public string CompanyZip
		{
			get
			{
				return this.companyZip;
			}
			set
			{
				this.companyZip = value;
			}
		}

		public string DateInStock
		{
			get
			{
				return this.dateInStock;
			}
			set
			{
				this.dateInStock = value;
			}
		}

		public int DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

		public string Doors
		{
			get
			{
				return this.doors;
			}
			set
			{
				this.doors = value;
			}
		}

		public string Drivetrain
		{
			get
			{
				return this.drivetrain;
			}
			set
			{
				this.drivetrain = value;
			}
		}

		public string Engine
		{
			get
			{
				return this.engine;
			}
			set
			{
				this.engine = value;
			}
		}

		public string ExtColor
		{
			get
			{
				return this.extColor;
			}
			set
			{
				this.extColor = value;
			}
		}

		public string Fuel
		{
			get
			{
				return this.fuel;
			}
			set
			{
				this.fuel = value;
			}
		}

		public string IntColor
		{
			get
			{
				return this.intColor;
			}
			set
			{
				this.intColor = value;
			}
		}

		public int InternetPrice
		{
			get
			{
				return this.internetPrice;
			}
			set
			{
				this.internetPrice = value;
			}
		}

		public string IntSurface
		{
			get
			{
				return this.intSurface;
			}
			set
			{
				this.intSurface = value;
			}
		}

		public bool IsCertified
		{
			get
			{
				return this.isCertified;
			}
			set
			{
				this.isCertified = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public int ListingId
		{
			get
			{
				return this.listingId;
			}
			set
			{
				this.listingId = value;
			}
		}

		public string Make
		{
			get
			{
				return this.make;
			}
			set
			{
				this.make = value;
			}
		}

		public int Mileage
		{
			get
			{
				return this.mileage;
			}
			set
			{
				this.mileage = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public string NewUsed
		{
			get
			{
				return this.newUsed;
			}
			set
			{
				this.newUsed = value;
			}
		}

		public string Options
		{
			get
			{
				return this.options;
			}
			set
			{
				this.options = value;
			}
		}

		public string PhotoUrl
		{
			get
			{
				return this.photoUrl;
			}
			set
			{
				this.photoUrl = value;
			}
		}

		public int RawEbizautosId
		{
			get
			{
				return this.rawEbizautosId;
			}
			set
			{
				this.rawEbizautosId = value;
			}
		}

		public string StockNo
		{
			get
			{
				return this.stockNo;
			}
			set
			{
				this.stockNo = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string Trim
		{
			get
			{
				return this.trim;
			}
			set
			{
				this.trim = value;
			}
		}

		public string VehicleYear
		{
			get
			{
				return this.vehicleYear;
			}
			set
			{
				this.vehicleYear = value;
			}
		}

		public string VinNo
		{
			get
			{
				return this.vinNo;
			}
			set
			{
				this.vinNo = value;
			}
		}

		public RawEbizautos()
		{
		}

		public RawEbizautos(string connection, string modifiedUserID, int rawEbizautosId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.RawEbizautosId = rawEbizautosId;
			this.databaseObjectName = "RAW_EBIZAUTOS";
			if (this.RawEbizautosId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawEbizautosId", DataAccessParameterType.Numeric, this.RawEbizautosId.ToString());
			data.ExecuteProcedure("RAW_EBIZAUTOS_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawEbizautosId", DataAccessParameterType.Numeric, this.RawEbizautosId.ToString());
			data.ExecuteProcedure("RAW_EBIZAUTOS_GetRecord");
			if (!data.EOF)
			{
				this.RawEbizautosId = int.Parse(data["RawEbizautosId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerId = int.Parse(data["DealerId"]);
				this.CompanyName = data["CompanyName"];
				this.CompanyAddress = data["CompanyAddress"];
				this.CompanyCity = data["CompanyCity"];
				this.CompanyState = data["CompanyState"];
				this.CompanyZip = data["CompanyZip"];
				this.CompanyPhone = data["CompanyPhone"];
				this.ListingId = int.Parse(data["ListingId"]);
				this.VinNo = data["VinNo"];
				this.NewUsed = data["NewUsed"];
				this.StockNo = data["StockNo"];
				this.VehicleYear = data["VehicleYear"];
				this.Make = data["Make"];
				this.Model = data["Model"];
				this.BodyStyle = data["BodyStyle"];
				this.Doors = data["Doors"];
				this.Trim = data["Trim"];
				this.ExtColor = data["ExtColor"];
				this.IntColor = data["IntColor"];
				this.IntSurface = data["IntSurface"];
				this.Engine = data["Engine"];
				this.Fuel = data["Fuel"];
				this.Drivetrain = data["Drivetrain"];
				this.Transmission = data["Transmission"];
				this.Mileage = int.Parse(data["Mileage"]);
				this.InternetPrice = int.Parse(data["InternetPrice"]);
				this.IsCertified = bool.Parse(data["IsCertified"]);
				this.Options = data["Options"];
				this.Description = data["Description"];
				this.PhotoUrl = data["PhotoUrl"];
				this.DateInStock = data["DateInStock"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawEbizautosId = this.RawEbizautosId;
			data.AddParam("@RawEbizautosId", DataAccessParameterType.Numeric, rawEbizautosId.ToString());
			rawEbizautosId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawEbizautosId.ToString());
			rawEbizautosId = this.DealerId;
			data.AddParam("@DealerId", DataAccessParameterType.Numeric, rawEbizautosId.ToString());
			data.AddParam("@CompanyName", DataAccessParameterType.Text, this.CompanyName);
			data.AddParam("@CompanyAddress", DataAccessParameterType.Text, this.CompanyAddress);
			data.AddParam("@CompanyCity", DataAccessParameterType.Text, this.CompanyCity);
			data.AddParam("@CompanyState", DataAccessParameterType.Text, this.CompanyState);
			data.AddParam("@CompanyZip", DataAccessParameterType.Text, this.CompanyZip);
			data.AddParam("@CompanyPhone", DataAccessParameterType.Text, this.CompanyPhone);
			rawEbizautosId = this.ListingId;
			data.AddParam("@ListingId", DataAccessParameterType.Numeric, rawEbizautosId.ToString());
			data.AddParam("@VinNo", DataAccessParameterType.Text, this.VinNo);
			data.AddParam("@NewUsed", DataAccessParameterType.Text, this.NewUsed);
			data.AddParam("@StockNo", DataAccessParameterType.Text, this.StockNo);
			data.AddParam("@VehicleYear", DataAccessParameterType.Text, this.VehicleYear);
			data.AddParam("@Make", DataAccessParameterType.Text, this.Make);
			data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
			data.AddParam("@BodyStyle", DataAccessParameterType.Text, this.BodyStyle);
			data.AddParam("@Doors", DataAccessParameterType.Text, this.Doors);
			data.AddParam("@Trim", DataAccessParameterType.Text, this.Trim);
			data.AddParam("@ExtColor", DataAccessParameterType.Text, this.ExtColor);
			data.AddParam("@IntColor", DataAccessParameterType.Text, this.IntColor);
			data.AddParam("@IntSurface", DataAccessParameterType.Text, this.IntSurface);
			data.AddParam("@Engine", DataAccessParameterType.Text, this.Engine);
			data.AddParam("@Fuel", DataAccessParameterType.Text, this.Fuel);
			data.AddParam("@Drivetrain", DataAccessParameterType.Text, this.Drivetrain);
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			rawEbizautosId = this.Mileage;
			data.AddParam("@Mileage", DataAccessParameterType.Numeric, rawEbizautosId.ToString());
			rawEbizautosId = this.InternetPrice;
			data.AddParam("@InternetPrice", DataAccessParameterType.Numeric, rawEbizautosId.ToString());
			data.AddParam("@IsCertified", DataAccessParameterType.Bool, this.IsCertified.ToString());
			data.AddParam("@Options", DataAccessParameterType.Text, this.Options);
			data.AddParam("@Description", DataAccessParameterType.Text, this.Description);
			data.AddParam("@PhotoUrl", DataAccessParameterType.Text, this.PhotoUrl);
			data.AddParam("@DateInStock", DataAccessParameterType.Text, this.DateInStock);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("RAW_EBIZAUTOS_InsertUpdate");
			if (!data.EOF)
			{
				this.RawEbizautosId = int.Parse(data["RawEbizautosId"]);
			}
			this.retrievedata();
		}
	}
}