using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawAutouplinkusa : Transaction
	{
		private int rawAutouplinkusaId = 0;

		private int jobExecutionRawId = 0;

		private int dealerId = 0;

		private string stockNo = "";

		private string vIN = "";

		private string vehicleMake = "";

		private string vehicleModel = "";

		private string vehicleYear = "";

		private int mileage = 0;

		private string bodyStyle = "";

		private string extColor = "";

		private string intColor = "";

		private string transmission = "";

		private string engineCid = "";

		private string engineSize = "";

		private string engineType = "";

		private float price = 0f;

		private float salesCost = 0f;

		private string options = "";

		private string imageUrl = "";

		private string videoUrl = "";

		public string BodyStyle
		{
			get
			{
				return this.bodyStyle;
			}
			set
			{
				this.bodyStyle = value;
			}
		}

		public int DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public string EngineCid
		{
			get
			{
				return this.engineCid;
			}
			set
			{
				this.engineCid = value;
			}
		}

		public string EngineSize
		{
			get
			{
				return this.engineSize;
			}
			set
			{
				this.engineSize = value;
			}
		}

		public string EngineType
		{
			get
			{
				return this.engineType;
			}
			set
			{
				this.engineType = value;
			}
		}

		public string ExtColor
		{
			get
			{
				return this.extColor;
			}
			set
			{
				this.extColor = value;
			}
		}

		public string ImageUrl
		{
			get
			{
				return this.imageUrl;
			}
			set
			{
				this.imageUrl = value;
			}
		}

		public string IntColor
		{
			get
			{
				return this.intColor;
			}
			set
			{
				this.intColor = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public int Mileage
		{
			get
			{
				return this.mileage;
			}
			set
			{
				this.mileage = value;
			}
		}

		public string Options
		{
			get
			{
				return this.options;
			}
			set
			{
				this.options = value;
			}
		}

		public float Price
		{
			get
			{
				return this.price;
			}
			set
			{
				this.price = value;
			}
		}

		public int RawAutouplinkusaId
		{
			get
			{
				return this.rawAutouplinkusaId;
			}
			set
			{
				this.rawAutouplinkusaId = value;
			}
		}

		public float SalesCost
		{
			get
			{
				return this.salesCost;
			}
			set
			{
				this.salesCost = value;
			}
		}

		public string StockNo
		{
			get
			{
				return this.stockNo;
			}
			set
			{
				this.stockNo = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string VehicleMake
		{
			get
			{
				return this.vehicleMake;
			}
			set
			{
				this.vehicleMake = value;
			}
		}

		public string VehicleModel
		{
			get
			{
				return this.vehicleModel;
			}
			set
			{
				this.vehicleModel = value;
			}
		}

		public string VehicleYear
		{
			get
			{
				return this.vehicleYear;
			}
			set
			{
				this.vehicleYear = value;
			}
		}

		public string VideoUrl
		{
			get
			{
				return this.videoUrl;
			}
			set
			{
				this.videoUrl = value;
			}
		}

		public string VIN
		{
			get
			{
				return this.vIN;
			}
			set
			{
				this.vIN = value;
			}
		}

		public RawAutouplinkusa()
		{
		}

		public RawAutouplinkusa(string connection, string modifiedUserID, int rawAutouplinkusaId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "RAW_AUTOUPLINKUSA";
			this.RawAutouplinkusaId = rawAutouplinkusaId;
			if (this.RawAutouplinkusaId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawAutouplinkusaId", DataAccessParameterType.Numeric, this.RawAutouplinkusaId.ToString());
			data.ExecuteProcedure("RAW_AUTOUPLINKUSA_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawAutouplinkusaId", DataAccessParameterType.Numeric, this.RawAutouplinkusaId.ToString());
			data.ExecuteProcedure("RAW_AUTOUPLINKUSA_GetRecord");
			if (!data.EOF)
			{
				this.RawAutouplinkusaId = int.Parse(data["RawAutouplinkusaId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerId = int.Parse(data["DealerId"]);
				this.StockNo = data["StockNo"];
				this.VIN = data["VIN"];
				this.VehicleMake = data["VehicleMake"];
				this.VehicleModel = data["VehicleModel"];
				this.VehicleYear = data["VehicleYear"];
				this.Mileage = int.Parse(data["Mileage"]);
				this.BodyStyle = data["BodyStyle"];
				this.ExtColor = data["ExtColor"];
				this.IntColor = data["IntColor"];
				this.Transmission = data["Transmission"];
				this.EngineCid = data["EngineCid"];
				this.EngineSize = data["EngineSize"];
				this.EngineType = data["EngineType"];
				this.Price = float.Parse(data["Price"]);
				this.SalesCost = float.Parse(data["SalesCost"]);
				this.Options = data["Options"];
				this.ImageUrl = data["ImageUrl"];
				this.VideoUrl = data["VideoUrl"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawAutouplinkusaId = this.RawAutouplinkusaId;
			data.AddParam("@RawAutouplinkusaId", DataAccessParameterType.Numeric, rawAutouplinkusaId.ToString());
			rawAutouplinkusaId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawAutouplinkusaId.ToString());
			rawAutouplinkusaId = this.DealerId;
			data.AddParam("@DealerId", DataAccessParameterType.Numeric, rawAutouplinkusaId.ToString());
			data.AddParam("@StockNo", DataAccessParameterType.Text, this.StockNo);
			data.AddParam("@VIN", DataAccessParameterType.Text, this.VIN);
			data.AddParam("@VehicleMake", DataAccessParameterType.Text, this.VehicleMake);
			data.AddParam("@VehicleModel", DataAccessParameterType.Text, this.VehicleModel);
			data.AddParam("@VehicleYear", DataAccessParameterType.Text, this.VehicleYear);
			rawAutouplinkusaId = this.Mileage;
			data.AddParam("@Mileage", DataAccessParameterType.Numeric, rawAutouplinkusaId.ToString());
			data.AddParam("@BodyStyle", DataAccessParameterType.Text, this.BodyStyle);
			data.AddParam("@ExtColor", DataAccessParameterType.Text, this.ExtColor);
			data.AddParam("@IntColor", DataAccessParameterType.Text, this.IntColor);
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			data.AddParam("@EngineCid", DataAccessParameterType.Text, this.EngineCid);
			data.AddParam("@EngineSize", DataAccessParameterType.Text, this.EngineSize);
			data.AddParam("@EngineType", DataAccessParameterType.Text, this.EngineType);
			float price = this.Price;
			data.AddParam("@Price", DataAccessParameterType.Numeric, price.ToString());
			price = this.SalesCost;
			data.AddParam("@SalesCost", DataAccessParameterType.Numeric, price.ToString());
			data.AddParam("@Options", DataAccessParameterType.Text, this.Options);
			data.AddParam("@ImageUrl", DataAccessParameterType.Text, this.ImageUrl);
			data.AddParam("@VideoUrl", DataAccessParameterType.Text, this.VideoUrl);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("RAW_AUTOUPLINKUSA_InsertUpdate");
			if (!data.EOF)
			{
				this.RawAutouplinkusaId = int.Parse(data["RawAutouplinkusaId"]);
			}
			this.retrievedata();
		}
	}
}