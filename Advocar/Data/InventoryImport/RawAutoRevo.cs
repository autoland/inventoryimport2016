using Advocar.Data;
using Advocar.Interface;
using System;
using System.Runtime.CompilerServices;

namespace Advocar.Data.InventoryImport
{
	public class RawAutoRevo : Transaction
	{
		public string Body
		{
			get;
			set;
		}

		public string Certified
		{
			get;
			set;
		}

		public string DealerId
		{
			get;
			set;
		}

		public string Description
		{
			get;
			set;
		}

		public string DriveTrain
		{
			get;
			set;
		}

		public string ExteriorColor
		{
			get;
			set;
		}

		public string ImageUrl
		{
			get;
			set;
		}

		public string InteriorColor
		{
			get;
			set;
		}

		public string InteriorType
		{
			get;
			set;
		}

		public int JobExecutionRawId
		{
			get;
			set;
		}

		public string Make
		{
			get;
			set;
		}

		public int Mileage
		{
			get;
			set;
		}

		public string Model
		{
			get;
			set;
		}

		public string NewUsed
		{
			get;
			set;
		}

		public int NumberOfCylinders
		{
			get;
			set;
		}

		public int NumberOfDoors
		{
			get;
			set;
		}

		public string Optiones
		{
			get;
			set;
		}

		public int RawAutoRevoId
		{
			get;
			set;
		}

		public float RetailPrice
		{
			get;
			set;
		}

		public string StockNumber
		{
			get;
			set;
		}

		public string Transmission
		{
			get;
			set;
		}

		public string Trim
		{
			get;
			set;
		}

		public string Vin
		{
			get;
			set;
		}

		public float WebPrice
		{
			get;
			set;
		}

		public int Year
		{
			get;
			set;
		}

		public RawAutoRevo()
		{
		}

		public RawAutoRevo(string connectionString, string modifiedUser, int rawAutoRevoId) : base(connectionString, modifiedUser)
		{
			this.databaseObjectName = "RAW_AUTO_REVO";
			this.RawAutoRevoId = rawAutoRevoId;
			if (this.RawAutoRevoId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawAutoRevoId", DataAccessParameterType.Numeric, this.RawAutoRevoId.ToString());
			data.ExecuteProcedure("RAW_AUTO_REVO_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawAutoRevoId", DataAccessParameterType.Numeric, this.RawAutoRevoId.ToString());
			data.ExecuteProcedure("RAW_AUTO_REVO_GetRecord");
			if (data.EOF)
			{
				this.wipeout();
			}
			else
			{
				this.RawAutoRevoId = int.Parse(data["Raw_AutoRevo_Id"]);
				this.JobExecutionRawId = int.Parse(data["Job_Execution_Raw_Id"]);
				this.DealerId = data["DealerID"];
				this.Vin = data["VIN"];
				this.StockNumber = data["StockNumber"];
				this.Year = int.Parse(data["Year"]);
				this.Make = data["Make"];
				this.Model = data["Model"];
				this.Trim = data["Trim"];
				this.Body = data["Body"];
				this.Mileage = int.Parse(data["Mileage"]);
				this.WebPrice = float.Parse(data["WebPrice"]);
				this.RetailPrice = float.Parse(data["RetailPrice"]);
				this.Certified = data["Certified"];
				this.ExteriorColor = data["ExteriorColor"];
				this.InteriorColor = data["InteriorColor"];
				this.Description = data["Description"];
				this.Optiones = data["Options"];
				this.ImageUrl = data["ImageURLs"];
				this.InteriorType = data["InteriorType"];
				this.NumberOfCylinders = int.Parse(data["NumberOfCylinders"]);
				this.DriveTrain = data["DriveTrain"];
				this.NumberOfDoors = int.Parse(data["NumberOfDoors"]);
				this.Transmission = data["Transmission"];
				this.NewUsed = data["NewUsed"];
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawAutoRevoId = this.RawAutoRevoId;
			data.AddParam("@RawAutoRevoId", DataAccessParameterType.Numeric, rawAutoRevoId.ToString());
			rawAutoRevoId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawAutoRevoId.ToString());
			data.AddParam("@DealerId", DataAccessParameterType.Text, this.DealerId);
			data.AddParam("@Vin", DataAccessParameterType.Text, this.Vin);
			data.AddParam("@StockNumber", DataAccessParameterType.Numeric, this.StockNumber);
			rawAutoRevoId = this.Year;
			data.AddParam("@Year", DataAccessParameterType.Text, rawAutoRevoId.ToString());
			data.AddParam("@Make", DataAccessParameterType.Text, this.Make);
			data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
			data.AddParam("@Trim", DataAccessParameterType.Text, this.Trim);
			data.AddParam("@body", DataAccessParameterType.Numeric, this.Body);
			rawAutoRevoId = this.Mileage;
			data.AddParam("@Mileage", DataAccessParameterType.Numeric, rawAutoRevoId.ToString());
			float webPrice = this.WebPrice;
			data.AddParam("@webprice", DataAccessParameterType.Text, webPrice.ToString());
			webPrice = this.RetailPrice;
			data.AddParam("@Price", DataAccessParameterType.Numeric, webPrice.ToString());
			data.AddParam("@certified", DataAccessParameterType.Text, this.Certified);
			data.AddParam("@ExteriorColor", DataAccessParameterType.Text, this.ExteriorColor);
			data.AddParam("@InteriorColor", DataAccessParameterType.Text, this.InteriorColor);
			data.AddParam("@Description", DataAccessParameterType.Text, this.Description);
			data.AddParam("@Options", DataAccessParameterType.Text, this.Optiones);
			data.AddParam("@ImageURLs", DataAccessParameterType.Text, this.ImageUrl);
			data.AddParam("@InteriorType", DataAccessParameterType.Text, this.InteriorType);
			rawAutoRevoId = this.NumberOfCylinders;
			data.AddParam("@NumberOfCylinders", DataAccessParameterType.Text, rawAutoRevoId.ToString());
			data.AddParam("@DriveTrain", DataAccessParameterType.Text, this.DriveTrain);
			rawAutoRevoId = this.NumberOfDoors;
			data.AddParam("@NumberOfDoors", DataAccessParameterType.Numeric, rawAutoRevoId.ToString());
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			data.AddParam("@NewUsed", DataAccessParameterType.Text, this.NewUsed);
			data.ExecuteProcedure("RAW_AUTO_REVO_InsertUpdate");
			if (!data.EOF)
			{
				this.RawAutoRevoId = int.Parse(data["RawAutoRevoId"]);
			}
			this.retrievedata();
		}
	}
}