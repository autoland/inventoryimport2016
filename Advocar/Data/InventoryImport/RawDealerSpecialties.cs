using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawDealerSpecialties : Transaction
	{
		private int rawDealerSpecialtiesId = 0;

		private int jobExecutionRawId = 0;

		private string dealerId = "";

		private string vin = "";

		private string stockNumber = "";

		private string status = "";

		private int vehicleType = 0;

		private int year = 0;

		private string make = "";

		private string model = "";

		private string trim = "";

		private string body = "";

		private string vehicleCategory = "";

		private int mileage = 0;

		private string transmission = "";

		private string engineDisplacement = "";

		private string engineSize = "";

		private string induction = "";

		private string drivetrain = "";

		private string fuelType = "";

		private float fuelEconomyCity = 0f;

		private float fuelEconomyHighway = 0f;

		private float fuelEconomyCombined = 0f;

		private int doors = 0;

		private string oemColorCode = "";

		private string externalColor = "";

		private string internalColor = "";

		private string genericColor = "";

		private int internetPrice = 0;

		private int comparisonPrice = 0;

		private string oemModelCode = "";

		private string hasWarranty = "";

		private int certificationWarranty = 0;

		private int warrantyMonths = 0;

		private int warrantyMiles = 0;

		private string certificationNumber = "";

		private string serviceContract = "";

		private string inServiceDate = "";

		private string certificationDate = "";

		private string dateManufactured = "";

		private string dateCreated = "";

		private string dateUpdated = "";

		private string dateRemoved = "";

		private string photos = "";

		private int superSizePhotos = 0;

		private string addendumDetails = "";

		private string departmentComments = "";

		private string vehicleComments = "";

		private string options = "";

		private float purchasePayment = 0f;

		private float purchaseDownPayment = 0f;

		private int purchaseTerm = 0;

		private string purchaseDiclosure = "";

		private int purchaseRate = 0;

		private float leasePayment = 0f;

		private float leaseDownPayment = 0f;

		private int leaseTerm = 0;

		private string leaseDisclosure = "";

		private int leaseRate = 0;

		private string reserved1 = "";

		private string reserved2 = "";

		private string reserved3 = "";

		private string reserved4 = "";

		private string reserved5 = "";

		private string reserved6 = "";

		public string AddendumDetails
		{
			get
			{
				return this.addendumDetails;
			}
			set
			{
				this.addendumDetails = value;
			}
		}

		public string Body
		{
			get
			{
				return this.body;
			}
			set
			{
				this.body = value;
			}
		}

		public string CertificationDate
		{
			get
			{
				return this.certificationDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.certificationDate = "";
				}
				else
				{
					this.certificationDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CertificationNumber
		{
			get
			{
				return this.certificationNumber;
			}
			set
			{
				this.certificationNumber = value;
			}
		}

		public int CertificationWarranty
		{
			get
			{
				return this.certificationWarranty;
			}
			set
			{
				this.certificationWarranty = value;
			}
		}

		public int ComparisonPrice
		{
			get
			{
				return this.comparisonPrice;
			}
			set
			{
				this.comparisonPrice = value;
			}
		}

		public string DateCreated
		{
			get
			{
				return this.dateCreated;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.dateCreated = "";
				}
				else
				{
					this.dateCreated = DateTime.Parse(value).ToString();
				}
			}
		}

		public string DateManufactured
		{
			get
			{
				return this.dateManufactured;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.dateManufactured = "";
				}
				else
				{
					this.dateManufactured = DateTime.Parse(value).ToString();
				}
			}
		}

		public string DateRemoved
		{
			get
			{
				return this.dateRemoved;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.dateRemoved = "";
				}
				else
				{
					this.dateRemoved = DateTime.Parse(value).ToString();
				}
			}
		}

		public string DateUpdated
		{
			get
			{
				return this.dateUpdated;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.dateUpdated = "";
				}
				else
				{
					this.dateUpdated = DateTime.Parse(value).ToString();
				}
			}
		}

		public string DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public string DepartmentComments
		{
			get
			{
				return this.departmentComments;
			}
			set
			{
				this.departmentComments = value;
			}
		}

		public int Doors
		{
			get
			{
				return this.doors;
			}
			set
			{
				this.doors = value;
			}
		}

		public string Drivetrain
		{
			get
			{
				return this.drivetrain;
			}
			set
			{
				this.drivetrain = value;
			}
		}

		public string EngineDisplacement
		{
			get
			{
				return this.engineDisplacement;
			}
			set
			{
				this.engineDisplacement = value;
			}
		}

		public string EngineSize
		{
			get
			{
				return this.engineSize;
			}
			set
			{
				this.engineSize = value;
			}
		}

		public string ExternalColor
		{
			get
			{
				return this.externalColor;
			}
			set
			{
				this.externalColor = value;
			}
		}

		public float FuelEconomyCity
		{
			get
			{
				return this.fuelEconomyCity;
			}
			set
			{
				this.fuelEconomyCity = value;
			}
		}

		public float FuelEconomyCombined
		{
			get
			{
				return this.fuelEconomyCombined;
			}
			set
			{
				this.fuelEconomyCombined = value;
			}
		}

		public float FuelEconomyHighway
		{
			get
			{
				return this.fuelEconomyHighway;
			}
			set
			{
				this.fuelEconomyHighway = value;
			}
		}

		public string FuelType
		{
			get
			{
				return this.fuelType;
			}
			set
			{
				this.fuelType = value;
			}
		}

		public string GenericColor
		{
			get
			{
				return this.genericColor;
			}
			set
			{
				this.genericColor = value;
			}
		}

		public string HasWarranty
		{
			get
			{
				return this.hasWarranty;
			}
			set
			{
				this.hasWarranty = value;
			}
		}

		public string Induction
		{
			get
			{
				return this.induction;
			}
			set
			{
				this.induction = value;
			}
		}

		public string InServiceDate
		{
			get
			{
				return this.inServiceDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.inServiceDate = "";
				}
				else
				{
					this.inServiceDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string InternalColor
		{
			get
			{
				return this.internalColor;
			}
			set
			{
				this.internalColor = value;
			}
		}

		public int InternetPrice
		{
			get
			{
				return this.internetPrice;
			}
			set
			{
				this.internetPrice = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public string LeaseDisclosure
		{
			get
			{
				return this.leaseDisclosure;
			}
			set
			{
				this.leaseDisclosure = value;
			}
		}

		public float LeaseDownPayment
		{
			get
			{
				return this.leaseDownPayment;
			}
			set
			{
				this.leaseDownPayment = value;
			}
		}

		public float LeasePayment
		{
			get
			{
				return this.leasePayment;
			}
			set
			{
				this.leasePayment = value;
			}
		}

		public int LeaseRate
		{
			get
			{
				return this.leaseRate;
			}
			set
			{
				this.leaseRate = value;
			}
		}

		public int LeaseTerm
		{
			get
			{
				return this.leaseTerm;
			}
			set
			{
				this.leaseTerm = value;
			}
		}

		public string Make
		{
			get
			{
				return this.make;
			}
			set
			{
				this.make = value;
			}
		}

		public int Mileage
		{
			get
			{
				return this.mileage;
			}
			set
			{
				this.mileage = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public string OemColorCode
		{
			get
			{
				return this.oemColorCode;
			}
			set
			{
				this.oemColorCode = value;
			}
		}

		public string OemModelCode
		{
			get
			{
				return this.oemModelCode;
			}
			set
			{
				this.oemModelCode = value;
			}
		}

		public string Options
		{
			get
			{
				return this.options;
			}
			set
			{
				this.options = value;
			}
		}

		public string Photos
		{
			get
			{
				return this.photos;
			}
			set
			{
				this.photos = value;
			}
		}

		public string PurchaseDiclosure
		{
			get
			{
				return this.purchaseDiclosure;
			}
			set
			{
				this.purchaseDiclosure = value;
			}
		}

		public float PurchaseDownPayment
		{
			get
			{
				return this.purchaseDownPayment;
			}
			set
			{
				this.purchaseDownPayment = value;
			}
		}

		public float PurchasePayment
		{
			get
			{
				return this.purchasePayment;
			}
			set
			{
				this.purchasePayment = value;
			}
		}

		public int PurchaseRate
		{
			get
			{
				return this.purchaseRate;
			}
			set
			{
				this.purchaseRate = value;
			}
		}

		public int PurchaseTerm
		{
			get
			{
				return this.purchaseTerm;
			}
			set
			{
				this.purchaseTerm = value;
			}
		}

		public int RawDealerSpecialtiesId
		{
			get
			{
				return this.rawDealerSpecialtiesId;
			}
			set
			{
				this.rawDealerSpecialtiesId = value;
			}
		}

		public string Reserved1
		{
			get
			{
				return this.reserved1;
			}
			set
			{
				this.reserved1 = value;
			}
		}

		public string Reserved2
		{
			get
			{
				return this.reserved2;
			}
			set
			{
				this.reserved2 = value;
			}
		}

		public string Reserved3
		{
			get
			{
				return this.reserved3;
			}
			set
			{
				this.reserved3 = value;
			}
		}

		public string Reserved4
		{
			get
			{
				return this.reserved4;
			}
			set
			{
				this.reserved4 = value;
			}
		}

		public string Reserved5
		{
			get
			{
				return this.reserved5;
			}
			set
			{
				this.reserved5 = value;
			}
		}

		public string Reserved6
		{
			get
			{
				return this.reserved6;
			}
			set
			{
				this.reserved6 = value;
			}
		}

		public string ServiceContract
		{
			get
			{
				return this.serviceContract;
			}
			set
			{
				this.serviceContract = value;
			}
		}

		public string Status
		{
			get
			{
				return this.status;
			}
			set
			{
				this.status = value;
			}
		}

		public string StockNumber
		{
			get
			{
				return this.stockNumber;
			}
			set
			{
				this.stockNumber = value;
			}
		}

		public int SuperSizePhotos
		{
			get
			{
				return this.superSizePhotos;
			}
			set
			{
				this.superSizePhotos = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string Trim
		{
			get
			{
				return this.trim;
			}
			set
			{
				this.trim = value;
			}
		}

		public string VehicleCategory
		{
			get
			{
				return this.vehicleCategory;
			}
			set
			{
				this.vehicleCategory = value;
			}
		}

		public string VehicleComments
		{
			get
			{
				return this.vehicleComments;
			}
			set
			{
				this.vehicleComments = value;
			}
		}

		public int VehicleType
		{
			get
			{
				return this.vehicleType;
			}
			set
			{
				this.vehicleType = value;
			}
		}

		public string Vin
		{
			get
			{
				return this.vin;
			}
			set
			{
				this.vin = value;
			}
		}

		public int WarrantyMiles
		{
			get
			{
				return this.warrantyMiles;
			}
			set
			{
				this.warrantyMiles = value;
			}
		}

		public int WarrantyMonths
		{
			get
			{
				return this.warrantyMonths;
			}
			set
			{
				this.warrantyMonths = value;
			}
		}

		public int Year
		{
			get
			{
				return this.year;
			}
			set
			{
				this.year = value;
			}
		}

		public RawDealerSpecialties()
		{
		}

		public RawDealerSpecialties(string connection, string modifiedUserID, int rawDealerSpecialtiesId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "RAW_DEALER_SPECIALTIES";
			this.RawDealerSpecialtiesId = rawDealerSpecialtiesId;
			if (this.RawDealerSpecialtiesId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawDealerSpecialtiesId", DataAccessParameterType.Numeric, this.RawDealerSpecialtiesId.ToString());
			data.ExecuteProcedure("RAW_DEALER_SPECIALTIES_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawDealerSpecialtiesId", DataAccessParameterType.Numeric, this.RawDealerSpecialtiesId.ToString());
			data.ExecuteProcedure("RAW_DEALER_SPECIALTIES_GetRecord");
			if (!data.EOF)
			{
				this.RawDealerSpecialtiesId = int.Parse(data["RawDealerSpecialtiesId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerId = data["DealerId"];
				this.Vin = data["Vin"];
				this.StockNumber = data["StockNumber"];
				this.Status = data["Status"];
				this.VehicleType = int.Parse(data["VehicleType"]);
				this.Year = int.Parse(data["Year"]);
				this.Make = data["Make"];
				this.Model = data["Model"];
				this.Trim = data["Trim"];
				this.Body = data["Body"];
				this.VehicleCategory = data["VehicleCategory"];
				this.Mileage = int.Parse(data["Mileage"]);
				this.Transmission = data["Transmission"];
				this.EngineDisplacement = data["EngineDisplacement"];
				this.EngineSize = data["EngineSize"];
				this.Induction = data["Induction"];
				this.Drivetrain = data["Drivetrain"];
				this.FuelType = data["FuelType"];
				this.FuelEconomyCity = float.Parse(data["FuelEconomyCity"]);
				this.FuelEconomyHighway = float.Parse(data["FuelEconomyHighway"]);
				this.FuelEconomyCombined = float.Parse(data["FuelEconomyCombined"]);
				this.Doors = int.Parse(data["Doors"]);
				this.OemColorCode = data["OemColorCode"];
				this.ExternalColor = data["ExternalColor"];
				this.InternalColor = data["InternalColor"];
				this.GenericColor = data["GenericColor"];
				this.InternetPrice = int.Parse(data["InternetPrice"]);
				this.ComparisonPrice = int.Parse(data["ComparisonPrice"]);
				this.OemModelCode = data["OemModelCode"];
				this.HasWarranty = data["HasWarranty"];
				this.CertificationWarranty = int.Parse(data["CertificationWarranty"]);
				this.WarrantyMonths = int.Parse(data["WarrantyMonths"]);
				this.WarrantyMiles = int.Parse(data["WarrantyMiles"]);
				this.CertificationNumber = data["CertificationNumber"];
				this.ServiceContract = data["ServiceContract"];
				this.InServiceDate = data["InServiceDate"];
				this.CertificationDate = data["CertificationDate"];
				this.DateManufactured = data["DateManufactured"];
				this.DateCreated = data["DateCreated"];
				this.DateUpdated = data["DateUpdated"];
				this.DateRemoved = data["DateRemoved"];
				this.Photos = data["Photos"];
				this.SuperSizePhotos = int.Parse(data["SuperSizePhotos"]);
				this.AddendumDetails = data["AddendumDetails"];
				this.DepartmentComments = data["DepartmentComments"];
				this.VehicleComments = data["VehicleComments"];
				this.Options = data["Options"];
				this.PurchasePayment = float.Parse(data["PurchasePayment"]);
				this.PurchaseDownPayment = float.Parse(data["PurchaseDownPayment"]);
				this.PurchaseTerm = int.Parse(data["PurchaseTerm"]);
				this.PurchaseDiclosure = data["PurchaseDiclosure"];
				this.PurchaseRate = int.Parse(data["PurchaseRate"]);
				this.LeasePayment = float.Parse(data["LeasePayment"]);
				this.LeaseDownPayment = float.Parse(data["LeaseDownPayment"]);
				this.LeaseTerm = int.Parse(data["LeaseTerm"]);
				this.LeaseDisclosure = data["LeaseDisclosure"];
				this.LeaseRate = int.Parse(data["LeaseRate"]);
				this.Reserved1 = data["Reserved1"];
				this.Reserved2 = data["Reserved2"];
				this.Reserved3 = data["Reserved3"];
				this.Reserved4 = data["Reserved4"];
				this.Reserved5 = data["Reserved5"];
				this.Reserved6 = data["Reserved6"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawDealerSpecialtiesId = this.RawDealerSpecialtiesId;
			data.AddParam("@RawDealerSpecialtiesId", DataAccessParameterType.Numeric, rawDealerSpecialtiesId.ToString());
			rawDealerSpecialtiesId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawDealerSpecialtiesId.ToString());
			data.AddParam("@DealerId", DataAccessParameterType.Text, this.DealerId);
			data.AddParam("@Vin", DataAccessParameterType.Text, this.Vin);
			data.AddParam("@StockNumber", DataAccessParameterType.Text, this.StockNumber);
			data.AddParam("@Status", DataAccessParameterType.Text, this.Status);
			rawDealerSpecialtiesId = this.VehicleType;
			data.AddParam("@VehicleType", DataAccessParameterType.Numeric, rawDealerSpecialtiesId.ToString());
			rawDealerSpecialtiesId = this.Year;
			data.AddParam("@Year", DataAccessParameterType.Numeric, rawDealerSpecialtiesId.ToString());
			data.AddParam("@Make", DataAccessParameterType.Text, this.Make);
			data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
			data.AddParam("@Trim", DataAccessParameterType.Text, this.Trim);
			data.AddParam("@Body", DataAccessParameterType.Text, this.Body);
			data.AddParam("@VehicleCategory", DataAccessParameterType.Text, this.VehicleCategory);
			rawDealerSpecialtiesId = this.Mileage;
			data.AddParam("@Mileage", DataAccessParameterType.Numeric, rawDealerSpecialtiesId.ToString());
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			data.AddParam("@EngineDisplacement", DataAccessParameterType.Text, this.EngineDisplacement);
			data.AddParam("@EngineSize", DataAccessParameterType.Text, this.EngineSize);
			data.AddParam("@Induction", DataAccessParameterType.Text, this.Induction);
			data.AddParam("@Drivetrain", DataAccessParameterType.Text, this.Drivetrain);
			data.AddParam("@FuelType", DataAccessParameterType.Text, this.FuelType);
			float fuelEconomyCity = this.FuelEconomyCity;
			data.AddParam("@FuelEconomyCity", DataAccessParameterType.Numeric, fuelEconomyCity.ToString());
			fuelEconomyCity = this.FuelEconomyHighway;
			data.AddParam("@FuelEconomyHighway", DataAccessParameterType.Numeric, fuelEconomyCity.ToString());
			fuelEconomyCity = this.FuelEconomyCombined;
			data.AddParam("@FuelEconomyCombined", DataAccessParameterType.Numeric, fuelEconomyCity.ToString());
			rawDealerSpecialtiesId = this.Doors;
			data.AddParam("@Doors", DataAccessParameterType.Numeric, rawDealerSpecialtiesId.ToString());
			data.AddParam("@OemColorCode", DataAccessParameterType.Text, this.OemColorCode);
			data.AddParam("@ExternalColor", DataAccessParameterType.Text, this.ExternalColor);
			data.AddParam("@InternalColor", DataAccessParameterType.Text, this.InternalColor);
			data.AddParam("@GenericColor", DataAccessParameterType.Text, this.GenericColor);
			rawDealerSpecialtiesId = this.InternetPrice;
			data.AddParam("@InternetPrice", DataAccessParameterType.Numeric, rawDealerSpecialtiesId.ToString());
			rawDealerSpecialtiesId = this.ComparisonPrice;
			data.AddParam("@ComparisonPrice", DataAccessParameterType.Numeric, rawDealerSpecialtiesId.ToString());
			data.AddParam("@OemModelCode", DataAccessParameterType.Text, this.OemModelCode);
			data.AddParam("@HasWarranty", DataAccessParameterType.Text, this.HasWarranty);
			rawDealerSpecialtiesId = this.CertificationWarranty;
			data.AddParam("@CertificationWarranty", DataAccessParameterType.Numeric, rawDealerSpecialtiesId.ToString());
			rawDealerSpecialtiesId = this.WarrantyMonths;
			data.AddParam("@WarrantyMonths", DataAccessParameterType.Numeric, rawDealerSpecialtiesId.ToString());
			rawDealerSpecialtiesId = this.WarrantyMiles;
			data.AddParam("@WarrantyMiles", DataAccessParameterType.Numeric, rawDealerSpecialtiesId.ToString());
			data.AddParam("@CertificationNumber", DataAccessParameterType.Text, this.CertificationNumber);
			data.AddParam("@ServiceContract", DataAccessParameterType.Text, this.ServiceContract);
			data.AddParam("@InServiceDate", DataAccessParameterType.DateTime, this.InServiceDate);
			data.AddParam("@CertificationDate", DataAccessParameterType.DateTime, this.CertificationDate);
			data.AddParam("@DateManufactured", DataAccessParameterType.DateTime, this.DateManufactured);
			data.AddParam("@DateCreated", DataAccessParameterType.DateTime, this.DateCreated);
			data.AddParam("@DateUpdated", DataAccessParameterType.DateTime, this.DateUpdated);
			data.AddParam("@DateRemoved", DataAccessParameterType.DateTime, this.DateRemoved);
			data.AddParam("@Photos", DataAccessParameterType.Text, this.Photos);
			rawDealerSpecialtiesId = this.SuperSizePhotos;
			data.AddParam("@SuperSizePhotos", DataAccessParameterType.Numeric, rawDealerSpecialtiesId.ToString());
			data.AddParam("@AddendumDetails", DataAccessParameterType.Text, this.AddendumDetails);
			data.AddParam("@DepartmentComments", DataAccessParameterType.Text, this.DepartmentComments);
			data.AddParam("@VehicleComments", DataAccessParameterType.Text, this.VehicleComments);
			data.AddParam("@Options", DataAccessParameterType.Text, this.Options);
			fuelEconomyCity = this.PurchasePayment;
			data.AddParam("@PurchasePayment", DataAccessParameterType.Numeric, fuelEconomyCity.ToString());
			fuelEconomyCity = this.PurchaseDownPayment;
			data.AddParam("@PurchaseDownPayment", DataAccessParameterType.Numeric, fuelEconomyCity.ToString());
			rawDealerSpecialtiesId = this.PurchaseTerm;
			data.AddParam("@PurchaseTerm", DataAccessParameterType.Numeric, rawDealerSpecialtiesId.ToString());
			data.AddParam("@PurchaseDiclosure", DataAccessParameterType.Text, this.PurchaseDiclosure);
			rawDealerSpecialtiesId = this.PurchaseRate;
			data.AddParam("@PurchaseRate", DataAccessParameterType.Numeric, rawDealerSpecialtiesId.ToString());
			fuelEconomyCity = this.LeasePayment;
			data.AddParam("@LeasePayment", DataAccessParameterType.Numeric, fuelEconomyCity.ToString());
			fuelEconomyCity = this.LeaseDownPayment;
			data.AddParam("@LeaseDownPayment", DataAccessParameterType.Numeric, fuelEconomyCity.ToString());
			rawDealerSpecialtiesId = this.LeaseTerm;
			data.AddParam("@LeaseTerm", DataAccessParameterType.Numeric, rawDealerSpecialtiesId.ToString());
			data.AddParam("@LeaseDisclosure", DataAccessParameterType.Text, this.LeaseDisclosure);
			rawDealerSpecialtiesId = this.LeaseRate;
			data.AddParam("@LeaseRate", DataAccessParameterType.Numeric, rawDealerSpecialtiesId.ToString());
			data.AddParam("@Reserved1", DataAccessParameterType.Text, this.Reserved1);
			data.AddParam("@Reserved2", DataAccessParameterType.Text, this.Reserved2);
			data.AddParam("@Reserved3", DataAccessParameterType.Text, this.Reserved3);
			data.AddParam("@Reserved4", DataAccessParameterType.Text, this.Reserved4);
			data.AddParam("@Reserved5", DataAccessParameterType.Text, this.Reserved5);
			data.AddParam("@Reserved6", DataAccessParameterType.Text, this.Reserved6);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("RAW_DEALER_SPECIALTIES_InsertUpdate");
			if (!data.EOF)
			{
				this.RawDealerSpecialtiesId = int.Parse(data["RawDealerSpecialtiesId"]);
			}
			this.retrievedata();
		}
	}
}