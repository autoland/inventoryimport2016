using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawDealerCarSearch : Transaction
	{
		private int rawDealerCarSearchId = 0;

		private int jobExecutionRawId = 0;

		private string dealerId = string.Empty;

		private string vin = string.Empty;

		private string make = string.Empty;

		private string model = string.Empty;

		private string trim = string.Empty;

		private string driveType = string.Empty;

		private string transmissionType = string.Empty;

		private int year = 0;

		private string stockNumber = string.Empty;

		private string interiorType = string.Empty;

		private string interiorColor = string.Empty;

		private string exteriorColor = string.Empty;

		private int cylinders = 0;

		private int cost = 0;

		private int wholesale = 0;

		private int retail = 0;

		private int mileage = 0;

		private string purchaseDate = string.Empty;

		private string videoUrl = string.Empty;

		private string options = string.Empty;

		private string images = string.Empty;

		private string lastModifiedDate = string.Empty;

		private string bodyType = string.Empty;

		private string engine = string.Empty;

		private int mpgCity = 0;

		private int mpgHighway = 0;

		private string newUsed = string.Empty;

		private int msrp = 0;

		private string imageLastModifiedDate = string.Empty;

		private string comments = string.Empty;

		private bool certifiedPreOwned = false;

		public string BodyType
		{
			get
			{
				return this.bodyType;
			}
			set
			{
				this.bodyType = value;
			}
		}

		public bool CertifiedPreOwned
		{
			get
			{
				return this.certifiedPreOwned;
			}
			set
			{
				this.certifiedPreOwned = value;
			}
		}

		public string Comments
		{
			get
			{
				return this.comments;
			}
			set
			{
				this.comments = value;
			}
		}

		public int Cost
		{
			get
			{
				return this.cost;
			}
			set
			{
				this.cost = value;
			}
		}

		public int Cylinders
		{
			get
			{
				return this.cylinders;
			}
			set
			{
				this.cylinders = value;
			}
		}

		public string DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public string DriveType
		{
			get
			{
				return this.driveType;
			}
			set
			{
				this.driveType = value;
			}
		}

		public string Engine
		{
			get
			{
				return this.engine;
			}
			set
			{
				this.engine = value;
			}
		}

		public string ExteriorColor
		{
			get
			{
				return this.exteriorColor;
			}
			set
			{
				this.exteriorColor = value;
			}
		}

		public string ImageLastModifiedDate
		{
			get
			{
				return this.imageLastModifiedDate;
			}
			set
			{
				this.imageLastModifiedDate = value;
			}
		}

		public string Images
		{
			get
			{
				return this.images;
			}
			set
			{
				this.images = value;
			}
		}

		public string InteriorColor
		{
			get
			{
				return this.interiorColor;
			}
			set
			{
				this.interiorColor = value;
			}
		}

		public string InteriorType
		{
			get
			{
				return this.interiorType;
			}
			set
			{
				this.interiorType = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public string LastModifiedDate
		{
			get
			{
				return this.lastModifiedDate;
			}
			set
			{
				this.lastModifiedDate = value;
			}
		}

		public string Make
		{
			get
			{
				return this.make;
			}
			set
			{
				this.make = value;
			}
		}

		public int Mileage
		{
			get
			{
				return this.mileage;
			}
			set
			{
				this.mileage = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public int MpgCity
		{
			get
			{
				return this.mpgCity;
			}
			set
			{
				this.mpgCity = value;
			}
		}

		public int MpgHighway
		{
			get
			{
				return this.mpgHighway;
			}
			set
			{
				this.mpgHighway = value;
			}
		}

		public int Msrp
		{
			get
			{
				return this.msrp;
			}
			set
			{
				this.msrp = value;
			}
		}

		public string NewUsed
		{
			get
			{
				return this.newUsed;
			}
			set
			{
				this.newUsed = value;
			}
		}

		public string Options
		{
			get
			{
				return this.options;
			}
			set
			{
				this.options = value;
			}
		}

		public string PurchaseDate
		{
			get
			{
				return this.purchaseDate;
			}
			set
			{
				this.purchaseDate = value;
			}
		}

		public int RawDealerCarSearchId
		{
			get
			{
				return this.rawDealerCarSearchId;
			}
			set
			{
				this.rawDealerCarSearchId = value;
			}
		}

		public int Retail
		{
			get
			{
				return this.retail;
			}
			set
			{
				this.retail = value;
			}
		}

		public string StockNumber
		{
			get
			{
				return this.stockNumber;
			}
			set
			{
				this.stockNumber = value;
			}
		}

		public string TransmissionType
		{
			get
			{
				return this.transmissionType;
			}
			set
			{
				this.transmissionType = value;
			}
		}

		public string Trim
		{
			get
			{
				return this.trim;
			}
			set
			{
				this.trim = value;
			}
		}

		public string VideoUrl
		{
			get
			{
				return this.videoUrl;
			}
			set
			{
				this.videoUrl = value;
			}
		}

		public string Vin
		{
			get
			{
				return this.vin;
			}
			set
			{
				this.vin = value;
			}
		}

		public int Wholesale
		{
			get
			{
				return this.wholesale;
			}
			set
			{
				this.wholesale = value;
			}
		}

		public int Year
		{
			get
			{
				return this.year;
			}
			set
			{
				this.year = value;
			}
		}

		public RawDealerCarSearch()
		{
		}

		public RawDealerCarSearch(string connection, string modifiedUser, int rawDealerCarSearchId) : base(connection, modifiedUser)
		{
			this.rawDealerCarSearchId = rawDealerCarSearchId;
			this.databaseObjectName = "RAW_DEALER_CAR_SEARCH";
			if (this.rawDealerCarSearchId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			string rawDealerCarSearchId = (this.rawDealerCarSearchId > 0 ? this.rawDealerCarSearchId.ToString() : string.Empty);
			DataAccess dataAccess = new DataAccess(this.connection);
			dataAccess.AddParam("@RawDealerCarSearchId", DataAccessParameterType.Numeric, rawDealerCarSearchId, true);
			dataAccess.ExecuteProcedure("RAW_DEALER_CAR_SEARCH_Delete");
		}

		protected override void retrievedata()
		{
			string rawDealerCarSearchId = (this.rawDealerCarSearchId > 0 ? this.rawDealerCarSearchId.ToString() : string.Empty);
			DataAccess dataAccess = new DataAccess(this.connection);
			dataAccess.AddParam("@RawDealerCarSearchId", DataAccessParameterType.Numeric, rawDealerCarSearchId, true);
			dataAccess.ExecuteProcedure("RAW_DEALER_CAR_SEARCH_GetRecord");
			if (!dataAccess.EOF)
			{
				this.rawDealerCarSearchId = (Validation.IsNumeric(dataAccess["RawDealerCarSearchId"]) ? Convert.ToInt32(dataAccess["RawDealerCarSearchId"]) : 0);
				this.jobExecutionRawId = (Validation.IsNumeric(dataAccess["JobExecutionRawId"]) ? Convert.ToInt32(dataAccess["JobExecutionRawId"]) : 0);
				this.dealerId = dataAccess["DealerId"];
				this.vin = dataAccess["VIN"];
				this.make = dataAccess["Make"];
				this.model = dataAccess["Model"];
				this.trim = dataAccess["Trim"];
				this.driveType = dataAccess["DriveType"];
				this.transmissionType = dataAccess["TransmissionType"];
				this.year = (Validation.IsNumeric(dataAccess["Year"]) ? Convert.ToInt32(dataAccess["Year"]) : 0);
				this.stockNumber = dataAccess["StockNumber"];
				this.interiorType = dataAccess["InteriorType"];
				this.interiorColor = dataAccess["InteriorColor"];
				this.exteriorColor = dataAccess["ExteriorColor"];
				this.cylinders = (Validation.IsNumeric(dataAccess["Cylinders"]) ? Convert.ToInt32(dataAccess["Cylinders"]) : 0);
				this.cost = (Validation.IsNumeric(dataAccess["Cost"]) ? Convert.ToInt32(dataAccess["Cost"]) : 0);
				this.wholesale = (Validation.IsNumeric(dataAccess["Wholesale"]) ? Convert.ToInt32(dataAccess["Wholesale"]) : 0);
				this.retail = (Validation.IsNumeric(dataAccess["Retail"]) ? Convert.ToInt32(dataAccess["Retail"]) : 0);
				this.mileage = (Validation.IsNumeric(dataAccess["Mileage"]) ? Convert.ToInt32(dataAccess["Mileage"]) : 0);
				this.purchaseDate = dataAccess["PurchaseDate"];
				this.videoUrl = dataAccess["VideoUrl"];
				this.options = dataAccess["Options"];
				this.images = dataAccess["Images"];
				this.lastModifiedDate = dataAccess["LastModifiedDate"];
				this.bodyType = dataAccess["BodyType"];
				this.engine = dataAccess["Engine"];
				this.mpgCity = (Validation.IsNumeric(dataAccess["MpgCity"]) ? Convert.ToInt32(dataAccess["MpgCity"]) : 0);
				this.mpgHighway = (Validation.IsNumeric(dataAccess["MpgHighway"]) ? Convert.ToInt32(dataAccess["MpgHighway"]) : 0);
				this.newUsed = dataAccess["NewUsed"];
				this.msrp = (Validation.IsNumeric(dataAccess["MSRP"]) ? Convert.ToInt32(dataAccess["MSRP"]) : 0);
				this.imageLastModifiedDate = dataAccess["ImageLastModifiedDate"];
				this.comments = dataAccess["Comments"];
				this.certifiedPreOwned = (Validation.IsNumeric(dataAccess["CertifiedPreOwned"]) ? Convert.ToInt32(dataAccess["CertifiedPreOwned"]) : 0) > 0;
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			string rawDealerCarSearchId = (this.rawDealerCarSearchId > 0 ? this.rawDealerCarSearchId.ToString() : string.Empty);
			string jobExecutionRawId = (this.jobExecutionRawId > 0 ? this.jobExecutionRawId.ToString() : string.Empty);
			string year = (this.year > 0 ? this.year.ToString() : string.Empty);
			string cylinders = (this.cylinders > 0 ? this.cylinders.ToString() : string.Empty);
			string cost = (this.cost > 0 ? this.cost.ToString() : string.Empty);
			string wholesale = (this.wholesale > 0 ? this.wholesale.ToString() : string.Empty);
			string retail = (this.retail > 0 ? this.retail.ToString() : string.Empty);
			string mileage = (this.mileage > 0 ? this.mileage.ToString() : string.Empty);
			string mpgCity = (this.mpgCity > 0 ? this.mpgCity.ToString() : string.Empty);
			string mpgHighway = (this.mpgHighway > 0 ? this.mpgHighway.ToString() : string.Empty);
			string msrp = (this.msrp > 0 ? this.msrp.ToString() : string.Empty);
			DataAccess dataAccess = new DataAccess(this.connection);
			dataAccess.AddParam("@RawDealerCarSearchId", DataAccessParameterType.Numeric, rawDealerCarSearchId, true);
			dataAccess.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, jobExecutionRawId, true);
			dataAccess.AddParam("@DealerId", DataAccessParameterType.Text, this.dealerId, true);
			dataAccess.AddParam("@VIN", DataAccessParameterType.Text, this.vin, true);
			dataAccess.AddParam("@Make", DataAccessParameterType.Text, this.make, true);
			dataAccess.AddParam("@Model", DataAccessParameterType.Text, this.model, true);
			dataAccess.AddParam("@Trim", DataAccessParameterType.Text, this.trim, true);
			dataAccess.AddParam("@DriveType", DataAccessParameterType.Text, this.driveType, true);
			dataAccess.AddParam("@TransmissionType", DataAccessParameterType.Text, this.transmissionType, true);
			dataAccess.AddParam("@Year", DataAccessParameterType.Numeric, year, true);
			dataAccess.AddParam("@StockNumber", DataAccessParameterType.Text, this.stockNumber, true);
			dataAccess.AddParam("@InteriorType", DataAccessParameterType.Text, this.interiorType, true);
			dataAccess.AddParam("@InteriorColor", DataAccessParameterType.Text, this.interiorColor, true);
			dataAccess.AddParam("@ExteriorColor", DataAccessParameterType.Text, this.exteriorColor, true);
			dataAccess.AddParam("@Cylinders", DataAccessParameterType.Numeric, cylinders, true);
			dataAccess.AddParam("@Cost", DataAccessParameterType.Numeric, cost, true);
			dataAccess.AddParam("@Wholesale", DataAccessParameterType.Numeric, wholesale, true);
			dataAccess.AddParam("@Retail", DataAccessParameterType.Numeric, retail, true);
			dataAccess.AddParam("@Mileage", DataAccessParameterType.Numeric, mileage, true);
			dataAccess.AddParam("@PurchaseDate", DataAccessParameterType.DateTime, this.purchaseDate, true);
			dataAccess.AddParam("@VideoUrl", DataAccessParameterType.Text, this.videoUrl, true);
			dataAccess.AddParam("@Options", DataAccessParameterType.Text, this.options, true);
			dataAccess.AddParam("@Images", DataAccessParameterType.Text, this.images, true);
			dataAccess.AddParam("@LastModifiedDate", DataAccessParameterType.DateTime, this.lastModifiedDate, true);
			dataAccess.AddParam("@BodyType", DataAccessParameterType.Text, this.bodyType, true);
			dataAccess.AddParam("@Engine", DataAccessParameterType.Text, this.engine, true);
			dataAccess.AddParam("@MpgCity", DataAccessParameterType.Numeric, mpgCity, true);
			dataAccess.AddParam("@MpgHighway", DataAccessParameterType.Numeric, mpgHighway, true);
			dataAccess.AddParam("@NewUsed", DataAccessParameterType.Text, this.newUsed, true);
			dataAccess.AddParam("@MSRP", DataAccessParameterType.Numeric, msrp, true);
			dataAccess.AddParam("@ImageLastModifiedDate", DataAccessParameterType.DateTime, this.imageLastModifiedDate, true);
			dataAccess.AddParam("@Comments", DataAccessParameterType.Text, this.comments, true);
			dataAccess.AddParam("@CertifiedPreOwned", DataAccessParameterType.Numeric, this.certifiedPreOwned.ToString(), true);
			dataAccess.ExecuteProcedure("RAW_DEALER_CAR_SEARCH_InsertUpdate");
			if (!dataAccess.EOF)
			{
				this.rawDealerCarSearchId = (Validation.IsNumeric(dataAccess["RawDealerCarSearchId"]) ? Convert.ToInt32(dataAccess["RawDealerCarSearchId"]) : 0);
			}
			this.retrievedata();
		}
	}
}