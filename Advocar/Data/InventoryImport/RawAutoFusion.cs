using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawAutoFusion : Transaction
	{
		private int rawAutoFusionId = 0;

		private int jobExecutionRawId = 0;

		private string dealerId = "";

		private string type = "";

		private string vIN = "";

		private string stockNumber = "";

		private int year = 0;

		private string make = "";

		private string model = "";

		private string trim = "";

		private string engine = "";

		private string transmission = "";

		private string bodyStyle = "";

		private string exterior = "";

		private string interior = "";

		private int mileage = 0;

		private float price = 0f;

		private float specialPrice = 0f;

		private string certified = "";

		private string images = "";

		private string options = "";

		private string tagLines = "";

		public string BodyStyle
		{
			get
			{
				return this.bodyStyle;
			}
			set
			{
				this.bodyStyle = value;
			}
		}

		public string Certified
		{
			get
			{
				return this.certified;
			}
			set
			{
				this.certified = value;
			}
		}

		public string DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public string Engine
		{
			get
			{
				return this.engine;
			}
			set
			{
				this.engine = value;
			}
		}

		public string Exterior
		{
			get
			{
				return this.exterior;
			}
			set
			{
				this.exterior = value;
			}
		}

		public string Images
		{
			get
			{
				return this.images;
			}
			set
			{
				this.images = value;
			}
		}

		public string Interior
		{
			get
			{
				return this.interior;
			}
			set
			{
				this.interior = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public string Make
		{
			get
			{
				return this.make;
			}
			set
			{
				this.make = value;
			}
		}

		public int Mileage
		{
			get
			{
				return this.mileage;
			}
			set
			{
				this.mileage = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public string Options
		{
			get
			{
				return this.options;
			}
			set
			{
				this.options = value;
			}
		}

		public float Price
		{
			get
			{
				return this.price;
			}
			set
			{
				this.price = value;
			}
		}

		public int RawAutoFusionId
		{
			get
			{
				return this.rawAutoFusionId;
			}
			set
			{
				this.rawAutoFusionId = value;
			}
		}

		public float SpecialPrice
		{
			get
			{
				return this.specialPrice;
			}
			set
			{
				this.specialPrice = value;
			}
		}

		public string StockNumber
		{
			get
			{
				return this.stockNumber;
			}
			set
			{
				this.stockNumber = value;
			}
		}

		public string TagLines
		{
			get
			{
				return this.tagLines;
			}
			set
			{
				this.tagLines = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string Trim
		{
			get
			{
				return this.trim;
			}
			set
			{
				this.trim = value;
			}
		}

		public string Type
		{
			get
			{
				return this.type;
			}
			set
			{
				this.type = value;
			}
		}

		public string VIN
		{
			get
			{
				return this.vIN;
			}
			set
			{
				this.vIN = value;
			}
		}

		public int Year
		{
			get
			{
				return this.year;
			}
			set
			{
				this.year = value;
			}
		}

		public RawAutoFusion()
		{
		}

		public RawAutoFusion(string connection, string modifiedUserID, int rawAutoFusionId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "RAW_AUTO_FUSION";
			this.RawAutoFusionId = rawAutoFusionId;
			if (this.RawAutoFusionId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawAutoFusionId", DataAccessParameterType.Numeric, this.RawAutoFusionId.ToString());
			data.ExecuteProcedure("RAW_AUTO_FUSION_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawAutoFusionId", DataAccessParameterType.Numeric, this.RawAutoFusionId.ToString());
			data.ExecuteProcedure("RAW_AUTO_FUSION_GetRecord");
			if (!data.EOF)
			{
				this.RawAutoFusionId = int.Parse(data["RawAutoFusionId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerId = data["DealerId"];
				this.Type = data["Type"];
				this.VIN = data["VIN"];
				this.StockNumber = data["StockNumber"];
				this.Year = int.Parse(data["Year"]);
				this.Make = data["Make"];
				this.Model = data["Model"];
				this.Trim = data["Trim"];
				this.Engine = data["Engine"];
				this.Transmission = data["Transmission"];
				this.BodyStyle = data["BodyStyle"];
				this.Exterior = data["Exterior"];
				this.Interior = data["Interior"];
				this.Mileage = int.Parse(data["Mileage"]);
				this.Price = float.Parse(data["Price"]);
				this.SpecialPrice = float.Parse(data["SpecialPrice"]);
				this.Certified = data["Certified"];
				this.Images = data["Images"];
				this.Options = data["Options"];
				this.TagLines = data["TagLines"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawAutoFusionId = this.RawAutoFusionId;
			data.AddParam("@RawAutoFusionId", DataAccessParameterType.Numeric, rawAutoFusionId.ToString());
			rawAutoFusionId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawAutoFusionId.ToString());
			data.AddParam("@DealerId", DataAccessParameterType.Text, this.DealerId);
			data.AddParam("@Type", DataAccessParameterType.Text, this.Type);
			data.AddParam("@VIN", DataAccessParameterType.Text, this.VIN);
			data.AddParam("@StockNumber", DataAccessParameterType.Text, this.StockNumber);
			rawAutoFusionId = this.Year;
			data.AddParam("@Year", DataAccessParameterType.Numeric, rawAutoFusionId.ToString());
			data.AddParam("@Make", DataAccessParameterType.Text, this.Make);
			data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
			data.AddParam("@Trim", DataAccessParameterType.Text, this.Trim);
			data.AddParam("@Engine", DataAccessParameterType.Text, this.Engine);
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			data.AddParam("@BodyStyle", DataAccessParameterType.Text, this.BodyStyle);
			data.AddParam("@Exterior", DataAccessParameterType.Text, this.Exterior);
			data.AddParam("@Interior", DataAccessParameterType.Text, this.Interior);
			rawAutoFusionId = this.Mileage;
			data.AddParam("@Mileage", DataAccessParameterType.Numeric, rawAutoFusionId.ToString());
			float price = this.Price;
			data.AddParam("@Price", DataAccessParameterType.Numeric, price.ToString());
			price = this.SpecialPrice;
			data.AddParam("@SpecialPrice", DataAccessParameterType.Numeric, price.ToString());
			data.AddParam("@Certified", DataAccessParameterType.Text, this.Certified);
			data.AddParam("@Images", DataAccessParameterType.Text, this.Images);
			data.AddParam("@Options", DataAccessParameterType.Text, this.Options);
			data.AddParam("@TagLines", DataAccessParameterType.Text, this.TagLines);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("RAW_AUTO_FUSION_InsertUpdate");
			if (!data.EOF)
			{
				this.RawAutoFusionId = int.Parse(data["RawAutoFusionId"]);
			}
			this.retrievedata();
		}
	}
}