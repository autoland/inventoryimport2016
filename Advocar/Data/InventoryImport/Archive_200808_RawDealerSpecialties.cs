using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class Archive_200808_RawDealerSpecialties : Transaction
	{
		private int rawDealerSpecialtiesId = 0;

		private int jobExecutionRawId = 0;

		private string dealerID = "";

		private string vin = "";

		private string dealerStockNo = "";

		private string disposition = "";

		private string makeName = "";

		private string modelName = "";

		private string year = "";

		private string miles = "";

		private string bodyType = "";

		private string engine = "";

		private string engineSize = "";

		private string induction = "";

		private string transmission = "";

		private string color = "";

		private string price = "";

		private string cost = "";

		private string warrantyC = "";

		private string warrantyCT = "";

		private string warrantyF = "";

		private string warrantyFT = "";

		private string warrantyFC = "";

		private string warrantyLabor = "";

		private string warrantyParts = "";

		private string warrantyMonths = "";

		private string warrantyMiles = "";

		private string manufacturerMonths = "";

		private string manufacturerMiles = "";

		private string serviceAgreement = "";

		private string dateIn = "";

		private string dateRemodeled = "";

		private string options = "";

		private string imageUrlList = "";

		private string dateUpdatedFile = "";

		private string dateCreatedFile = "";

		private string customPricing = "";

		public string BodyType
		{
			get
			{
				return this.bodyType;
			}
			set
			{
				this.bodyType = value;
			}
		}

		public string Color
		{
			get
			{
				return this.color;
			}
			set
			{
				this.color = value;
			}
		}

		public string Cost
		{
			get
			{
				return this.cost;
			}
			set
			{
				this.cost = value;
			}
		}

		public string CustomPricing
		{
			get
			{
				return this.customPricing;
			}
			set
			{
				this.customPricing = value;
			}
		}

		public string DateCreatedFile
		{
			get
			{
				return this.dateCreatedFile;
			}
			set
			{
				this.dateCreatedFile = value;
			}
		}

		public string DateIn
		{
			get
			{
				return this.dateIn;
			}
			set
			{
				this.dateIn = value;
			}
		}

		public string DateRemodeled
		{
			get
			{
				return this.dateRemodeled;
			}
			set
			{
				this.dateRemodeled = value;
			}
		}

		public string DateUpdatedFile
		{
			get
			{
				return this.dateUpdatedFile;
			}
			set
			{
				this.dateUpdatedFile = value;
			}
		}

		public string DealerID
		{
			get
			{
				return this.dealerID;
			}
			set
			{
				this.dealerID = value;
			}
		}

		public string DealerStockNo
		{
			get
			{
				return this.dealerStockNo;
			}
			set
			{
				this.dealerStockNo = value;
			}
		}

		public string Disposition
		{
			get
			{
				return this.disposition;
			}
			set
			{
				this.disposition = value;
			}
		}

		public string Engine
		{
			get
			{
				return this.engine;
			}
			set
			{
				this.engine = value;
			}
		}

		public string EngineSize
		{
			get
			{
				return this.engineSize;
			}
			set
			{
				this.engineSize = value;
			}
		}

		public string ImageUrlList
		{
			get
			{
				return this.imageUrlList;
			}
			set
			{
				this.imageUrlList = value;
			}
		}

		public string Induction
		{
			get
			{
				return this.induction;
			}
			set
			{
				this.induction = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public string MakeName
		{
			get
			{
				return this.makeName;
			}
			set
			{
				this.makeName = value;
			}
		}

		public string ManufacturerMiles
		{
			get
			{
				return this.manufacturerMiles;
			}
			set
			{
				this.manufacturerMiles = value;
			}
		}

		public string ManufacturerMonths
		{
			get
			{
				return this.manufacturerMonths;
			}
			set
			{
				this.manufacturerMonths = value;
			}
		}

		public string Miles
		{
			get
			{
				return this.miles;
			}
			set
			{
				this.miles = value;
			}
		}

		public string ModelName
		{
			get
			{
				return this.modelName;
			}
			set
			{
				this.modelName = value;
			}
		}

		public string Options
		{
			get
			{
				return this.options;
			}
			set
			{
				this.options = value;
			}
		}

		public string Price
		{
			get
			{
				return this.price;
			}
			set
			{
				this.price = value;
			}
		}

		public int RawDealerSpecialtiesId
		{
			get
			{
				return this.rawDealerSpecialtiesId;
			}
			set
			{
				this.rawDealerSpecialtiesId = value;
			}
		}

		public string ServiceAgreement
		{
			get
			{
				return this.serviceAgreement;
			}
			set
			{
				this.serviceAgreement = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string Vin
		{
			get
			{
				return this.vin;
			}
			set
			{
				this.vin = value;
			}
		}

		public string WarrantyC
		{
			get
			{
				return this.warrantyC;
			}
			set
			{
				this.warrantyC = value;
			}
		}

		public string WarrantyCT
		{
			get
			{
				return this.warrantyCT;
			}
			set
			{
				this.warrantyCT = value;
			}
		}

		public string WarrantyF
		{
			get
			{
				return this.warrantyF;
			}
			set
			{
				this.warrantyF = value;
			}
		}

		public string WarrantyFC
		{
			get
			{
				return this.warrantyFC;
			}
			set
			{
				this.warrantyFC = value;
			}
		}

		public string WarrantyFT
		{
			get
			{
				return this.warrantyFT;
			}
			set
			{
				this.warrantyFT = value;
			}
		}

		public string WarrantyLabor
		{
			get
			{
				return this.warrantyLabor;
			}
			set
			{
				this.warrantyLabor = value;
			}
		}

		public string WarrantyMiles
		{
			get
			{
				return this.warrantyMiles;
			}
			set
			{
				this.warrantyMiles = value;
			}
		}

		public string WarrantyMonths
		{
			get
			{
				return this.warrantyMonths;
			}
			set
			{
				this.warrantyMonths = value;
			}
		}

		public string WarrantyParts
		{
			get
			{
				return this.warrantyParts;
			}
			set
			{
				this.warrantyParts = value;
			}
		}

		public string Year
		{
			get
			{
				return this.year;
			}
			set
			{
				this.year = value;
			}
		}

		public Archive_200808_RawDealerSpecialties()
		{
		}

		public Archive_200808_RawDealerSpecialties(string connection, string modifiedUserID, int rawDealerSpecialtiesId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.RawDealerSpecialtiesId = rawDealerSpecialtiesId;
			this.databaseObjectName = "RAW_DEALER_SPECIALTIES";
			if (this.RawDealerSpecialtiesId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawDealerSpecialtiesId", DataAccessParameterType.Numeric, this.RawDealerSpecialtiesId.ToString());
			data.ExecuteProcedure("RAW_DEALER_SPECIALTIES_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawDealerSpecialtiesId", DataAccessParameterType.Numeric, this.RawDealerSpecialtiesId.ToString());
			data.ExecuteProcedure("RAW_DEALER_SPECIALTIES_GetRecord");
			if (!data.EOF)
			{
				this.RawDealerSpecialtiesId = int.Parse(data["RawDealerSpecialtiesId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerID = data["DealerID"];
				this.Vin = data["Vin"];
				this.DealerStockNo = data["DealerStockNo"];
				this.Disposition = data["Disposition"];
				this.MakeName = data["MakeName"];
				this.ModelName = data["ModelName"];
				this.Year = data["Year"];
				this.Miles = data["Miles"];
				this.BodyType = data["BodyType"];
				this.Engine = data["Engine"];
				this.EngineSize = data["EngineSize"];
				this.Induction = data["Induction"];
				this.Transmission = data["Transmission"];
				this.Color = data["Color"];
				this.Price = data["Price"];
				this.Cost = data["Cost"];
				this.WarrantyC = data["WarrantyC"];
				this.WarrantyCT = data["WarrantyCT"];
				this.WarrantyF = data["WarrantyF"];
				this.WarrantyFT = data["WarrantyFT"];
				this.WarrantyFC = data["WarrantyFC"];
				this.WarrantyLabor = data["WarrantyLabor"];
				this.WarrantyParts = data["WarrantyParts"];
				this.WarrantyMonths = data["WarrantyMonths"];
				this.WarrantyMiles = data["WarrantyMiles"];
				this.ManufacturerMonths = data["ManufacturerMonths"];
				this.ManufacturerMiles = data["ManufacturerMiles"];
				this.ServiceAgreement = data["ServiceAgreement"];
				this.DateIn = data["DateIn"];
				this.DateRemodeled = data["DateRemodeled"];
				this.Options = data["Options"];
				this.ImageUrlList = data["ImageUrlList"];
				this.DateUpdatedFile = data["DateUpdatedFile"];
				this.DateCreatedFile = data["DateCreatedFile"];
				this.CustomPricing = data["CustomPricing"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawDealerSpecialtiesId = this.RawDealerSpecialtiesId;
			data.AddParam("@RawDealerSpecialtiesId", DataAccessParameterType.Numeric, rawDealerSpecialtiesId.ToString());
			rawDealerSpecialtiesId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawDealerSpecialtiesId.ToString());
			data.AddParam("@DealerID", DataAccessParameterType.Text, this.DealerID);
			data.AddParam("@Vin", DataAccessParameterType.Text, this.Vin);
			data.AddParam("@DealerStockNo", DataAccessParameterType.Text, this.DealerStockNo);
			data.AddParam("@Disposition", DataAccessParameterType.Text, this.Disposition);
			data.AddParam("@MakeName", DataAccessParameterType.Text, this.MakeName);
			data.AddParam("@ModelName", DataAccessParameterType.Text, this.ModelName);
			data.AddParam("@Year", DataAccessParameterType.Text, this.Year);
			data.AddParam("@Miles", DataAccessParameterType.Text, this.Miles);
			data.AddParam("@BodyType", DataAccessParameterType.Text, this.BodyType);
			data.AddParam("@Engine", DataAccessParameterType.Text, this.Engine);
			data.AddParam("@EngineSize", DataAccessParameterType.Text, this.EngineSize);
			data.AddParam("@Induction", DataAccessParameterType.Text, this.Induction);
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			data.AddParam("@Color", DataAccessParameterType.Text, this.Color);
			data.AddParam("@Price", DataAccessParameterType.Text, this.Price);
			data.AddParam("@Cost", DataAccessParameterType.Text, this.Cost);
			data.AddParam("@WarrantyC", DataAccessParameterType.Text, this.WarrantyC);
			data.AddParam("@WarrantyCT", DataAccessParameterType.Text, this.WarrantyCT);
			data.AddParam("@WarrantyF", DataAccessParameterType.Text, this.WarrantyF);
			data.AddParam("@WarrantyFT", DataAccessParameterType.Text, this.WarrantyFT);
			data.AddParam("@WarrantyFC", DataAccessParameterType.Text, this.WarrantyFC);
			data.AddParam("@WarrantyLabor", DataAccessParameterType.Text, this.WarrantyLabor);
			data.AddParam("@WarrantyParts", DataAccessParameterType.Text, this.WarrantyParts);
			data.AddParam("@WarrantyMonths", DataAccessParameterType.Text, this.WarrantyMonths);
			data.AddParam("@WarrantyMiles", DataAccessParameterType.Text, this.WarrantyMiles);
			data.AddParam("@ManufacturerMonths", DataAccessParameterType.Text, this.ManufacturerMonths);
			data.AddParam("@ManufacturerMiles", DataAccessParameterType.Text, this.ManufacturerMiles);
			data.AddParam("@ServiceAgreement", DataAccessParameterType.Text, this.ServiceAgreement);
			data.AddParam("@DateIn", DataAccessParameterType.Text, this.DateIn);
			data.AddParam("@DateRemodeled", DataAccessParameterType.Text, this.DateRemodeled);
			data.AddParam("@Options", DataAccessParameterType.Text, this.Options);
			data.AddParam("@ImageUrlList", DataAccessParameterType.Text, this.ImageUrlList);
			data.AddParam("@DateUpdatedFile", DataAccessParameterType.Text, this.DateUpdatedFile);
			data.AddParam("@DateCreatedFile", DataAccessParameterType.Text, this.DateCreatedFile);
			data.AddParam("@CustomPricing", DataAccessParameterType.Text, this.CustomPricing);
			data.ExecuteProcedure("RAW_DEALER_SPECIALTIES_InsertUpdate");
			if (!data.EOF)
			{
				this.RawDealerSpecialtiesId = int.Parse(data["RawDealerSpecialtiesId"]);
			}
			this.retrievedata();
		}
	}
}