using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawDealerCom : Transaction
	{
		private int rawDealerComId = 0;

		private int jobExecutionRawId = 0;

		private string dealerId = "";

		private string vin = "";

		private string stockNo = "";

		private string year = "";

		private string make = "";

		private string model = "";

		private string modelCode = "";

		private string trim = "";

		private string bodyStyle = "";

		private string transmission = "";

		private string drivetrain = "";

		private string doors = "";

		private string exteriorColor = "";

		private string interiorColor = "";

		private string truckCab = "";

		private string truckBed = "";

		private float msrp = 0f;

		private float invoice = 0f;

		private float price = 0f;

		private float wholesalePrice = 0f;

		private float retailPrice = 0f;

		private float salesPrice = 0f;

		private int mileage = 0;

		private string comments = "";

		private string options = "";

		private string images = "";

		private string type = "";

		private string certified = "";

		private string engine = "";

		private string engineType = "";

		private string fuel = "";

		private string liveDate = "";

		private string dealership = "";

		private string dealershipAddress = "";

		private string dealershipAddress2 = "";

		private string dealershipCity = "";

		private string dealershipState = "";

		private string dealershipPostalCode = "";

		private string dealershipCountry = "";

		private string dealershipPhone = "";

		private string dealershipUrl = "";

		public string BodyStyle
		{
			get
			{
				return this.bodyStyle;
			}
			set
			{
				this.bodyStyle = value;
			}
		}

		public string Certified
		{
			get
			{
				return this.certified;
			}
			set
			{
				this.certified = value;
			}
		}

		public string Comments
		{
			get
			{
				return this.comments;
			}
			set
			{
				this.comments = value;
			}
		}

		public string DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public string Dealership
		{
			get
			{
				return this.dealership;
			}
			set
			{
				this.dealership = value;
			}
		}

		public string DealershipAddress
		{
			get
			{
				return this.dealershipAddress;
			}
			set
			{
				this.dealershipAddress = value;
			}
		}

		public string DealershipAddress2
		{
			get
			{
				return this.dealershipAddress2;
			}
			set
			{
				this.dealershipAddress2 = value;
			}
		}

		public string DealershipCity
		{
			get
			{
				return this.dealershipCity;
			}
			set
			{
				this.dealershipCity = value;
			}
		}

		public string DealershipCountry
		{
			get
			{
				return this.dealershipCountry;
			}
			set
			{
				this.dealershipCountry = value;
			}
		}

		public string DealershipPhone
		{
			get
			{
				return this.dealershipPhone;
			}
			set
			{
				this.dealershipPhone = value;
			}
		}

		public string DealershipPostalCode
		{
			get
			{
				return this.dealershipPostalCode;
			}
			set
			{
				this.dealershipPostalCode = value;
			}
		}

		public string DealershipState
		{
			get
			{
				return this.dealershipState;
			}
			set
			{
				this.dealershipState = value;
			}
		}

		public string DealershipUrl
		{
			get
			{
				return this.dealershipUrl;
			}
			set
			{
				this.dealershipUrl = value;
			}
		}

		public string Doors
		{
			get
			{
				return this.doors;
			}
			set
			{
				this.doors = value;
			}
		}

		public string Drivetrain
		{
			get
			{
				return this.drivetrain;
			}
			set
			{
				this.drivetrain = value;
			}
		}

		public string Engine
		{
			get
			{
				return this.engine;
			}
			set
			{
				this.engine = value;
			}
		}

		public string EngineType
		{
			get
			{
				return this.engineType;
			}
			set
			{
				this.engineType = value;
			}
		}

		public string ExteriorColor
		{
			get
			{
				return this.exteriorColor;
			}
			set
			{
				this.exteriorColor = value;
			}
		}

		public string Fuel
		{
			get
			{
				return this.fuel;
			}
			set
			{
				this.fuel = value;
			}
		}

		public string Images
		{
			get
			{
				return this.images;
			}
			set
			{
				this.images = value;
			}
		}

		public string InteriorColor
		{
			get
			{
				return this.interiorColor;
			}
			set
			{
				this.interiorColor = value;
			}
		}

		public float Invoice
		{
			get
			{
				return this.invoice;
			}
			set
			{
				this.invoice = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public string LiveDate
		{
			get
			{
				return this.liveDate;
			}
			set
			{
				this.liveDate = value;
			}
		}

		public string Make
		{
			get
			{
				return this.make;
			}
			set
			{
				this.make = value;
			}
		}

		public int Mileage
		{
			get
			{
				return this.mileage;
			}
			set
			{
				this.mileage = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public string ModelCode
		{
			get
			{
				return this.modelCode;
			}
			set
			{
				this.modelCode = value;
			}
		}

		public float Msrp
		{
			get
			{
				return this.msrp;
			}
			set
			{
				this.msrp = value;
			}
		}

		public string Options
		{
			get
			{
				return this.options;
			}
			set
			{
				this.options = value;
			}
		}

		public float Price
		{
			get
			{
				return this.price;
			}
			set
			{
				this.price = value;
			}
		}

		public int RawDealerComId
		{
			get
			{
				return this.rawDealerComId;
			}
			set
			{
				this.rawDealerComId = value;
			}
		}

		public float RetailPrice
		{
			get
			{
				return this.retailPrice;
			}
			set
			{
				this.retailPrice = value;
			}
		}

		public float SalesPrice
		{
			get
			{
				return this.salesPrice;
			}
			set
			{
				this.salesPrice = value;
			}
		}

		public string StockNo
		{
			get
			{
				return this.stockNo;
			}
			set
			{
				this.stockNo = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string Trim
		{
			get
			{
				return this.trim;
			}
			set
			{
				this.trim = value;
			}
		}

		public string TruckBed
		{
			get
			{
				return this.truckBed;
			}
			set
			{
				this.truckBed = value;
			}
		}

		public string TruckCab
		{
			get
			{
				return this.truckCab;
			}
			set
			{
				this.truckCab = value;
			}
		}

		public string Type
		{
			get
			{
				return this.type;
			}
			set
			{
				this.type = value;
			}
		}

		public string Vin
		{
			get
			{
				return this.vin;
			}
			set
			{
				this.vin = value;
			}
		}

		public float WholesalePrice
		{
			get
			{
				return this.wholesalePrice;
			}
			set
			{
				this.wholesalePrice = value;
			}
		}

		public string Year
		{
			get
			{
				return this.year;
			}
			set
			{
				this.year = value;
			}
		}

		public RawDealerCom()
		{
		}

		public RawDealerCom(string connection, string modifiedUserID, int rawDealerComId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "RAW_DEALER_COM";
			this.RawDealerComId = rawDealerComId;
			if (this.RawDealerComId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawDealerComId", DataAccessParameterType.Numeric, this.RawDealerComId.ToString());
			data.ExecuteProcedure("RAW_DEALER_COM_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawDealerComId", DataAccessParameterType.Numeric, this.RawDealerComId.ToString());
			data.ExecuteProcedure("RAW_DEALER_COM_GetRecord");
			if (!data.EOF)
			{
				this.RawDealerComId = int.Parse(data["RawDealerComId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerId = data["DealerId"];
				this.Vin = data["Vin"];
				this.StockNo = data["StockNo"];
				this.Year = data["Year"];
				this.Make = data["Make"];
				this.Model = data["Model"];
				this.ModelCode = data["ModelCode"];
				this.Trim = data["Trim"];
				this.BodyStyle = data["BodyStyle"];
				this.Transmission = data["Transmission"];
				this.Drivetrain = data["Drivetrain"];
				this.Doors = data["Doors"];
				this.ExteriorColor = data["ExteriorColor"];
				this.InteriorColor = data["InteriorColor"];
				this.TruckCab = data["TruckCab"];
				this.TruckBed = data["TruckBed"];
				this.Msrp = float.Parse(data["Msrp"]);
				this.Invoice = float.Parse(data["Invoice"]);
				this.Price = float.Parse(data["Price"]);
				this.WholesalePrice = float.Parse(data["WholesalePrice"]);
				this.RetailPrice = float.Parse(data["RetailPrice"]);
				this.SalesPrice = float.Parse(data["SalesPrice"]);
				this.Mileage = int.Parse(data["Mileage"]);
				this.Comments = data["Comments"];
				this.Options = data["Options"];
				this.Images = data["Images"];
				this.Type = data["Type"];
				this.Certified = data["Certified"];
				this.Engine = data["Engine"];
				this.EngineType = data["EngineType"];
				this.Fuel = data["Fuel"];
				this.LiveDate = data["LiveDate"];
				this.Dealership = data["Dealership"];
				this.DealershipAddress = data["DealershipAddress"];
				this.DealershipAddress2 = data["DealershipAddress2"];
				this.DealershipCity = data["DealershipCity"];
				this.DealershipState = data["DealershipState"];
				this.DealershipPostalCode = data["DealershipPostalCode"];
				this.DealershipCountry = data["DealershipCountry"];
				this.DealershipPhone = data["DealershipPhone"];
				this.DealershipUrl = data["DealershipUrl"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawDealerComId = this.RawDealerComId;
			data.AddParam("@RawDealerComId", DataAccessParameterType.Numeric, rawDealerComId.ToString());
			rawDealerComId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawDealerComId.ToString());
			data.AddParam("@DealerId", DataAccessParameterType.Text, this.DealerId);
			data.AddParam("@Vin", DataAccessParameterType.Text, this.Vin);
			data.AddParam("@StockNo", DataAccessParameterType.Text, this.StockNo);
			data.AddParam("@Year", DataAccessParameterType.Text, this.Year);
			data.AddParam("@Make", DataAccessParameterType.Text, this.Make);
			data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
			data.AddParam("@ModelCode", DataAccessParameterType.Text, this.ModelCode);
			data.AddParam("@Trim", DataAccessParameterType.Text, this.Trim);
			data.AddParam("@BodyStyle", DataAccessParameterType.Text, this.BodyStyle);
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			data.AddParam("@Drivetrain", DataAccessParameterType.Text, this.Drivetrain);
			data.AddParam("@Doors", DataAccessParameterType.Text, this.Doors);
			data.AddParam("@ExteriorColor", DataAccessParameterType.Text, this.ExteriorColor);
			data.AddParam("@InteriorColor", DataAccessParameterType.Text, this.InteriorColor);
			data.AddParam("@TruckCab", DataAccessParameterType.Text, this.TruckCab);
			data.AddParam("@TruckBed", DataAccessParameterType.Text, this.TruckBed);
			float msrp = this.Msrp;
			data.AddParam("@Msrp", DataAccessParameterType.Numeric, msrp.ToString());
			msrp = this.Invoice;
			data.AddParam("@Invoice", DataAccessParameterType.Numeric, msrp.ToString());
			msrp = this.Price;
			data.AddParam("@Price", DataAccessParameterType.Numeric, msrp.ToString());
			msrp = this.WholesalePrice;
			data.AddParam("@WholesalePrice", DataAccessParameterType.Numeric, msrp.ToString());
			msrp = this.RetailPrice;
			data.AddParam("@RetailPrice", DataAccessParameterType.Numeric, msrp.ToString());
			msrp = this.SalesPrice;
			data.AddParam("@SalesPrice", DataAccessParameterType.Numeric, msrp.ToString());
			rawDealerComId = this.Mileage;
			data.AddParam("@Mileage", DataAccessParameterType.Numeric, rawDealerComId.ToString());
			data.AddParam("@Comments", DataAccessParameterType.Text, this.Comments);
			data.AddParam("@Options", DataAccessParameterType.Text, this.Options);
			data.AddParam("@Images", DataAccessParameterType.Text, this.Images);
			data.AddParam("@Type", DataAccessParameterType.Text, this.Type);
			data.AddParam("@Certified", DataAccessParameterType.Text, this.Certified);
			data.AddParam("@Engine", DataAccessParameterType.Text, this.Engine);
			data.AddParam("@EngineType", DataAccessParameterType.Text, this.EngineType);
			data.AddParam("@Fuel", DataAccessParameterType.Text, this.Fuel);
			data.AddParam("@LiveDate", DataAccessParameterType.Text, this.LiveDate);
			data.AddParam("@Dealership", DataAccessParameterType.Text, this.Dealership);
			data.AddParam("@DealershipAddress", DataAccessParameterType.Text, this.DealershipAddress);
			data.AddParam("@DealershipAddress2", DataAccessParameterType.Text, this.DealershipAddress2);
			data.AddParam("@DealershipCity", DataAccessParameterType.Text, this.DealershipCity);
			data.AddParam("@DealershipState", DataAccessParameterType.Text, this.DealershipState);
			data.AddParam("@DealershipPostalCode", DataAccessParameterType.Text, this.DealershipPostalCode);
			data.AddParam("@DealershipCountry", DataAccessParameterType.Text, this.DealershipCountry);
			data.AddParam("@DealershipPhone", DataAccessParameterType.Text, this.DealershipPhone);
			data.AddParam("@DealershipUrl", DataAccessParameterType.Text, this.DealershipUrl);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("RAW_DEALER_COM_InsertUpdate");
			if (!data.EOF)
			{
				this.RawDealerComId = int.Parse(data["RawDealerComId"]);
			}
			this.retrievedata();
		}
	}
}