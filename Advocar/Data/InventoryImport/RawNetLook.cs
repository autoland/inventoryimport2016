using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawNetLook : Transaction
	{
		private int rawNetLookId = 0;

		private int jobExecutionRawId = 0;

		private string dealerId = "";

		private string stockNo = "";

		private string newUsed = "";

		private string vehicleYear = "";

		private string vehicleMake = "";

		private string vehicleModel = "";

		private string series = "";

		private string vin = "";

		private int mileage = 0;

		private float price = 0f;

		private string exteriorColor = "";

		private string interiorColor = "";

		private string engine = "";

		private string transmission = "";

		private string features = "";

		private string imageLinks = "";

		private string sellersNotes = "";

		public string DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public string Engine
		{
			get
			{
				return this.engine;
			}
			set
			{
				this.engine = value;
			}
		}

		public string ExteriorColor
		{
			get
			{
				return this.exteriorColor;
			}
			set
			{
				this.exteriorColor = value;
			}
		}

		public string Features
		{
			get
			{
				return this.features;
			}
			set
			{
				this.features = value;
			}
		}

		public string ImageLinks
		{
			get
			{
				return this.imageLinks;
			}
			set
			{
				this.imageLinks = value;
			}
		}

		public string InteriorColor
		{
			get
			{
				return this.interiorColor;
			}
			set
			{
				this.interiorColor = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public int Mileage
		{
			get
			{
				return this.mileage;
			}
			set
			{
				this.mileage = value;
			}
		}

		public string NewUsed
		{
			get
			{
				return this.newUsed;
			}
			set
			{
				this.newUsed = value;
			}
		}

		public float Price
		{
			get
			{
				return this.price;
			}
			set
			{
				this.price = value;
			}
		}

		public int RawNetLookId
		{
			get
			{
				return this.rawNetLookId;
			}
			set
			{
				this.rawNetLookId = value;
			}
		}

		public string SellersNotes
		{
			get
			{
				return this.sellersNotes;
			}
			set
			{
				this.sellersNotes = value;
			}
		}

		public string Series
		{
			get
			{
				return this.series;
			}
			set
			{
				this.series = value;
			}
		}

		public string StockNo
		{
			get
			{
				return this.stockNo;
			}
			set
			{
				this.stockNo = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string VehicleMake
		{
			get
			{
				return this.vehicleMake;
			}
			set
			{
				this.vehicleMake = value;
			}
		}

		public string VehicleModel
		{
			get
			{
				return this.vehicleModel;
			}
			set
			{
				this.vehicleModel = value;
			}
		}

		public string VehicleYear
		{
			get
			{
				return this.vehicleYear;
			}
			set
			{
				this.vehicleYear = value;
			}
		}

		public string Vin
		{
			get
			{
				return this.vin;
			}
			set
			{
				this.vin = value;
			}
		}

		public RawNetLook()
		{
		}

		public RawNetLook(string connection, string modifiedUserID, int rawNetLookId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.RawNetLookId = rawNetLookId;
			this.databaseObjectName = "RAW_NET_LOOK";
			if (this.RawNetLookId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawNetLookId", DataAccessParameterType.Numeric, this.RawNetLookId.ToString());
			data.ExecuteProcedure("RAW_NET_LOOK_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawNetLookId", DataAccessParameterType.Numeric, this.RawNetLookId.ToString());
			data.ExecuteProcedure("RAW_NET_LOOK_GetRecord");
			if (!data.EOF)
			{
				this.RawNetLookId = int.Parse(data["RawNetLookId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerId = data["DealerId"];
				this.StockNo = data["StockNo"];
				this.NewUsed = data["NewUsed"];
				this.VehicleYear = data["VehicleYear"];
				this.VehicleMake = data["VehicleMake"];
				this.VehicleModel = data["VehicleModel"];
				this.Series = data["Series"];
				this.Vin = data["Vin"];
				this.Mileage = int.Parse(data["Mileage"]);
				this.Price = float.Parse(data["Price"]);
				this.ExteriorColor = data["ExteriorColor"];
				this.InteriorColor = data["InteriorColor"];
				this.Engine = data["Engine"];
				this.Transmission = data["Transmission"];
				this.Features = data["Features"];
				this.ImageLinks = data["ImageLinks"];
				this.SellersNotes = data["SellersNotes"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawNetLookId = this.RawNetLookId;
			data.AddParam("@RawNetLookId", DataAccessParameterType.Numeric, rawNetLookId.ToString());
			rawNetLookId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawNetLookId.ToString());
			data.AddParam("@DealerId", DataAccessParameterType.Text, this.DealerId);
			data.AddParam("@StockNo", DataAccessParameterType.Text, this.StockNo);
			data.AddParam("@NewUsed", DataAccessParameterType.Text, this.NewUsed);
			data.AddParam("@VehicleYear", DataAccessParameterType.Text, this.VehicleYear);
			data.AddParam("@VehicleMake", DataAccessParameterType.Text, this.VehicleMake);
			data.AddParam("@VehicleModel", DataAccessParameterType.Text, this.VehicleModel);
			data.AddParam("@Series", DataAccessParameterType.Text, this.Series);
			data.AddParam("@Vin", DataAccessParameterType.Text, this.Vin);
			rawNetLookId = this.Mileage;
			data.AddParam("@Mileage", DataAccessParameterType.Numeric, rawNetLookId.ToString());
			data.AddParam("@Price", DataAccessParameterType.Numeric, this.Price.ToString());
			data.AddParam("@ExteriorColor", DataAccessParameterType.Text, this.ExteriorColor);
			data.AddParam("@InteriorColor", DataAccessParameterType.Text, this.InteriorColor);
			data.AddParam("@Engine", DataAccessParameterType.Text, this.Engine);
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			data.AddParam("@Features", DataAccessParameterType.Text, this.Features);
			data.AddParam("@ImageLinks", DataAccessParameterType.Text, this.ImageLinks);
			data.AddParam("@SellersNotes", DataAccessParameterType.Text, this.SellersNotes);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("RAW_NET_LOOK_InsertUpdate");
			if (!data.EOF)
			{
				this.RawNetLookId = int.Parse(data["RawNetLookId"]);
			}
			this.retrievedata();
		}
	}
}