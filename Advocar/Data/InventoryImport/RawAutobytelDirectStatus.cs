using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawAutobytelDirectStatus : Transaction
	{
		private int rawAutobytelDirectStatusId = 0;

		private int jobExecutionRawId = 0;

		private string statusID = "";

		private string statusDesc = "";

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public int RawAutobytelDirectStatusId
		{
			get
			{
				return this.rawAutobytelDirectStatusId;
			}
			set
			{
				this.rawAutobytelDirectStatusId = value;
			}
		}

		public string StatusDesc
		{
			get
			{
				return this.statusDesc;
			}
			set
			{
				this.statusDesc = value;
			}
		}

		public string StatusID
		{
			get
			{
				return this.statusID;
			}
			set
			{
				this.statusID = value;
			}
		}

		public RawAutobytelDirectStatus()
		{
		}

		public RawAutobytelDirectStatus(string connection, string modifiedUserID, int rawAutobytelDirectStatusId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "RAW_AUTOBYTEL_DIRECT_STATUS";
			this.RawAutobytelDirectStatusId = rawAutobytelDirectStatusId;
			if (this.RawAutobytelDirectStatusId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawAutobytelDirectStatusId", DataAccessParameterType.Numeric, this.RawAutobytelDirectStatusId.ToString());
			data.ExecuteProcedure("RAW_AUTOBYTEL_DIRECT_STATUS_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawAutobytelDirectStatusId", DataAccessParameterType.Numeric, this.RawAutobytelDirectStatusId.ToString());
			data.ExecuteProcedure("RAW_AUTOBYTEL_DIRECT_STATUS_GetRecord");
			if (!data.EOF)
			{
				this.RawAutobytelDirectStatusId = int.Parse(data["RawAutobytelDirectStatusId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.StatusID = data["StatusID"];
				this.StatusDesc = data["StatusDesc"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawAutobytelDirectStatusId = this.RawAutobytelDirectStatusId;
			data.AddParam("@RawAutobytelDirectStatusId", DataAccessParameterType.Numeric, rawAutobytelDirectStatusId.ToString());
			rawAutobytelDirectStatusId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawAutobytelDirectStatusId.ToString());
			data.AddParam("@StatusID", DataAccessParameterType.Text, this.StatusID);
			data.AddParam("@StatusDesc", DataAccessParameterType.Text, this.StatusDesc);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("RAW_AUTOBYTEL_DIRECT_STATUS_InsertUpdate");
			if (!data.EOF)
			{
				this.RawAutobytelDirectStatusId = int.Parse(data["RawAutobytelDirectStatusId"]);
			}
			this.retrievedata();
		}
	}
}