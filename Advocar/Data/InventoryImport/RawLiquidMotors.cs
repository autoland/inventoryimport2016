using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawLiquidMotors : Transaction
	{
		private int rawLiquidMotorsId = 0;

		private int jobExecutionRawId = 0;

		private string dealerId = "";

		private string dealerStockNo = "";

		private int year = 0;

		private string make = "";

		private string model = "";

		private string trim = "";

		private string vin = "";

		private int mileage = 0;

		private float price = 0f;

		private float msrp = 0f;

		private string exteriorColor = "";

		private string interiorColor = "";

		private string transmission = "";

		private string image = "";

		private string description = "";

		private string notes = "";

		private string bodyType = "";

		private string engineType = "";

		private string driveType = "";

		private string fuelType = "";

		private string newUsedCertified = "";

		private float internetPrice = 0f;

		public string BodyType
		{
			get
			{
				return this.bodyType;
			}
			set
			{
				this.bodyType = value;
			}
		}

		public string DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public string DealerStockNo
		{
			get
			{
				return this.dealerStockNo;
			}
			set
			{
				this.dealerStockNo = value;
			}
		}

		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

		public string DriveType
		{
			get
			{
				return this.driveType;
			}
			set
			{
				this.driveType = value;
			}
		}

		public string EngineType
		{
			get
			{
				return this.engineType;
			}
			set
			{
				this.engineType = value;
			}
		}

		public string ExteriorColor
		{
			get
			{
				return this.exteriorColor;
			}
			set
			{
				this.exteriorColor = value;
			}
		}

		public string FuelType
		{
			get
			{
				return this.fuelType;
			}
			set
			{
				this.fuelType = value;
			}
		}

		public string Image
		{
			get
			{
				return this.image;
			}
			set
			{
				this.image = value;
			}
		}

		public string InteriorColor
		{
			get
			{
				return this.interiorColor;
			}
			set
			{
				this.interiorColor = value;
			}
		}

		public float InternetPrice
		{
			get
			{
				return this.internetPrice;
			}
			set
			{
				this.internetPrice = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public string Make
		{
			get
			{
				return this.make;
			}
			set
			{
				this.make = value;
			}
		}

		public int Mileage
		{
			get
			{
				return this.mileage;
			}
			set
			{
				this.mileage = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public float Msrp
		{
			get
			{
				return this.msrp;
			}
			set
			{
				this.msrp = value;
			}
		}

		public string NewUsedCertified
		{
			get
			{
				return this.newUsedCertified;
			}
			set
			{
				this.newUsedCertified = value;
			}
		}

		public string Notes
		{
			get
			{
				return this.notes;
			}
			set
			{
				this.notes = value;
			}
		}

		public float Price
		{
			get
			{
				return this.price;
			}
			set
			{
				this.price = value;
			}
		}

		public int RawLiquidMotorsId
		{
			get
			{
				return this.rawLiquidMotorsId;
			}
			set
			{
				this.rawLiquidMotorsId = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string Trim
		{
			get
			{
				return this.trim;
			}
			set
			{
				this.trim = value;
			}
		}

		public string Vin
		{
			get
			{
				return this.vin;
			}
			set
			{
				this.vin = value;
			}
		}

		public int Year
		{
			get
			{
				return this.year;
			}
			set
			{
				this.year = value;
			}
		}

		public RawLiquidMotors()
		{
		}

		public RawLiquidMotors(string connection, string modifiedUserID, int rawLiquidMotorsId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "RAW_LIQUID_MOTORS";
			this.RawLiquidMotorsId = rawLiquidMotorsId;
			if (this.RawLiquidMotorsId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawLiquidMotorsId", DataAccessParameterType.Numeric, this.RawLiquidMotorsId.ToString());
			data.ExecuteProcedure("RAW_LIQUID_MOTORS_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawLiquidMotorsId", DataAccessParameterType.Numeric, this.RawLiquidMotorsId.ToString());
			data.ExecuteProcedure("RAW_LIQUID_MOTORS_GetRecord");
			if (!data.EOF)
			{
				this.RawLiquidMotorsId = int.Parse(data["RawLiquidMotorsId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerId = data["DealerId"];
				this.DealerStockNo = data["DealerStockNo"];
				this.Year = int.Parse(data["Year"]);
				this.Make = data["Make"];
				this.Model = data["Model"];
				this.Trim = data["Trim"];
				this.Vin = data["Vin"];
				this.Mileage = int.Parse(data["Mileage"]);
				this.Price = float.Parse(data["Price"]);
				this.Msrp = float.Parse(data["Msrp"]);
				this.ExteriorColor = data["ExteriorColor"];
				this.InteriorColor = data["InteriorColor"];
				this.Transmission = data["Transmission"];
				this.Image = data["Image"];
				this.Description = data["Description"];
				this.Notes = data["Notes"];
				this.BodyType = data["BodyType"];
				this.EngineType = data["EngineType"];
				this.DriveType = data["DriveType"];
				this.FuelType = data["FuelType"];
				this.NewUsedCertified = data["NewUsedCertified"];
				this.InternetPrice = float.Parse(data["InternetPrice"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawLiquidMotorsId = this.RawLiquidMotorsId;
			data.AddParam("@RawLiquidMotorsId", DataAccessParameterType.Numeric, rawLiquidMotorsId.ToString());
			rawLiquidMotorsId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawLiquidMotorsId.ToString());
			data.AddParam("@DealerId", DataAccessParameterType.Text, this.DealerId);
			data.AddParam("@DealerStockNo", DataAccessParameterType.Text, this.DealerStockNo);
			rawLiquidMotorsId = this.Year;
			data.AddParam("@Year", DataAccessParameterType.Numeric, rawLiquidMotorsId.ToString());
			data.AddParam("@Make", DataAccessParameterType.Text, this.Make);
			data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
			data.AddParam("@Trim", DataAccessParameterType.Text, this.Trim);
			data.AddParam("@Vin", DataAccessParameterType.Text, this.Vin);
			rawLiquidMotorsId = this.Mileage;
			data.AddParam("@Mileage", DataAccessParameterType.Numeric, rawLiquidMotorsId.ToString());
			float price = this.Price;
			data.AddParam("@Price", DataAccessParameterType.Numeric, price.ToString());
			price = this.Msrp;
			data.AddParam("@Msrp", DataAccessParameterType.Numeric, price.ToString());
			data.AddParam("@ExteriorColor", DataAccessParameterType.Text, this.ExteriorColor);
			data.AddParam("@InteriorColor", DataAccessParameterType.Text, this.InteriorColor);
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			data.AddParam("@Image", DataAccessParameterType.Text, this.Image);
			data.AddParam("@Description", DataAccessParameterType.Text, this.Description);
			data.AddParam("@Notes", DataAccessParameterType.Text, this.Notes);
			data.AddParam("@BodyType", DataAccessParameterType.Text, this.BodyType);
			data.AddParam("@EngineType", DataAccessParameterType.Text, this.EngineType);
			data.AddParam("@DriveType", DataAccessParameterType.Text, this.DriveType);
			data.AddParam("@FuelType", DataAccessParameterType.Text, this.FuelType);
			data.AddParam("@NewUsedCertified", DataAccessParameterType.Text, this.NewUsedCertified);
			price = this.InternetPrice;
			data.AddParam("@InternetPrice", DataAccessParameterType.Numeric, price.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("RAW_LIQUID_MOTORS_InsertUpdate");
			if (!data.EOF)
			{
				this.RawLiquidMotorsId = int.Parse(data["RawLiquidMotorsId"]);
			}
			this.retrievedata();
		}
	}
}