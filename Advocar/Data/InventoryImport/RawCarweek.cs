using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawCarweek : Transaction
	{
		private int rawCarweekId = 0;

		private int jobExecutionRawId = 0;

		private int dealerId = 0;

		private string newUsed = "";

		private string vin = "";

        private string stockNumber = "";

		private string make = "";

		private string model = "";

        private int modelYear = 0;

		private string trim = "";

		private string bodyStyle = "";

		private int mileage = 0;

		private string engineDescription = "";

		private int cylinders = 0;

		private string fuleType = "";

		private string transmission = "";

		private float price = 0f;

		private float specialPrice = 0f;

		private string exteriorColor = "";

		private string interiorColor = "";

		private string options = "";

		private string description = "";

		private string images = "";



        public int RawCarweekId
        {
            get
            {
                return this.rawCarweekId;
            }
            set
            {
                this.rawCarweekId = value;
            }
        }


       public int JobExecutionRawID
        {
            get
            {
                return this.jobExecutionRawId;
            }
            set
            {
                this.jobExecutionRawId = value;
            }
        }

	  public int DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public string NewUsed
		{
			get
			{
				return this.newUsed;
			}
			set
			{
				this.newUsed = value;
			}
		}

		public string Vin
		{
			get
			{
				return this.vin;
			}
			set
			{
				this.vin = value;
			}
		}

		public string StockNumber
		{
			get
			{
				return this.stockNumber;
			}
			set
			{
				this.stockNumber = value;
			}
		}

		public string Make
		{
			get
			{
				return this.make;
			}
			set
			{
				this.make = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public int ModelYear
		{
			get
			{
				return this.modelYear;
			}
			set
			{
				this.modelYear = value;
			}
		}

		public string Trim
		{
			get
			{
				return this.trim;
			}
			set
			{
				this.trim = value;
			}
		}

		public string BodyStyle
		{
			get
			{
				return this.bodyStyle;
			}
			set
			{
				this.bodyStyle = value;
			}
		}

		public int Mileage
		{
			get
			{
				return this.mileage;
			}
			set
			{
				this.mileage = value;
			}
		}

		public string EngineDescription
		{
			get
			{
				return this.engineDescription;
			}
			set
			{
				this.engineDescription = value;
			}
		}

		public int Cylinders
		{
			get
			{
				return this.cylinders;
			}
			set
			{
				this.cylinders = value;
			}
		}

		public string FuelType
		{
			get
			{
				return this.fuleType;
			}
			set
			{
				this.fuleType = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public float Price
		{
			get
			{
				return this.price;
			}
			set
			{
				this.price = value;
			}
		}

		public float SpecialPrice
		{
			get
			{
				return this.specialPrice;
			}
			set
			{
				this.specialPrice = value;
			}
		}

		public string ExteriorColor
		{
			get
			{
				return this.exteriorColor;
			}
			set
			{
				this.exteriorColor = value;
			}
		}

		public string InteriorColor
		{
			get
			{
				return this.interiorColor;
			}
			set
			{
				this.interiorColor = value;
			}
		}

		public string Options
		{
			get
			{
				return this.options;
			}
			set
			{
				this.options = value;
			}
		}

		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

        public string Images
		{
			get
			{
				return this.images;
			}
			set
			{
				this.images = value;
			}
		}

		

		public RawCarweek()
		{
		}

		public RawCarweek(string connection, string modifiedUserID, int rawCarWeekId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "RAW_CARWEEK";
            this.rawCarweekId = rawCarWeekId;
            if (this.rawCarweekId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
            data.AddParam("@RawCarWeekId", DataAccessParameterType.Numeric, this.rawCarweekId.ToString());
            data.ExecuteProcedure("RAW_CARWEEK_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
            data.AddParam("@@RawCarWeekId", DataAccessParameterType.Numeric, this.rawCarweekId.ToString());
            data.ExecuteProcedure("RAW_CARWEEK_GetRecord");
			if (!data.EOF)
			{
                this.rawCarweekId = int.Parse(data["RawCarWeekId"]);
                this.JobExecutionRawID = int.Parse(data["JobExecutionRawId"]);
                this.DealerId = int.Parse(data["DealerId"]);
                this.NewUsed = data["NewUsed"];
                this.Vin = data["Vin"];
                this.StockNumber = data["StockNumber"];
                this.Make = data["Make"];
                this.Model = data["Model"];
                this.ModelYear = int.Parse(data["ModelYear"]);
                this.Trim = data["Trim"];
                this.BodyStyle = data["BodyStyle"];
                this.Mileage = int.Parse(data["Mileage"]);
                this.EngineDescription = data["EngineDescription"];
                this.Cylinders = int.Parse(data["Cylinders"]);
                this.Transmission = data["Transmission"];
                this.Price = float.Parse(data["Price"]);
                this.SpecialPrice = float.Parse(data["specialprice"]);
                this.ExteriorColor = data["ExteriorColor"];
                this.InteriorColor = data["InteriorColor"];
                this.Options = data["Options"];
                this.Description = data["Description"];
                this.Images = data["images"];
				
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawCarweekId = this.RawCarweekId;
            data.AddParam("@@RawCarweekId", DataAccessParameterType.Numeric, rawCarweekId.ToString());
            rawCarweekId = this.JobExecutionRawID;
            data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawCarweekId.ToString());
            data.AddParam("@DealerId", DataAccessParameterType.Numeric, this.DealerId.ToString());
            data.AddParam("@NewUsed", DataAccessParameterType.Text, this.NewUsed);
			data.AddParam("@Vin", DataAccessParameterType.Numeric, this.Vin);
            data.AddParam("@StockNumber", DataAccessParameterType.Text, this.StockNumber);
            data.AddParam("@Make", DataAccessParameterType.Text, this.Make);
            data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
            data.AddParam("@ModelYear", DataAccessParameterType.Numeric, this.ModelYear.ToString());
			data.AddParam("@Trim", DataAccessParameterType.Text, this.Trim);
			data.AddParam("@@BodyStyle", DataAccessParameterType.Text, BodyStyle.ToString());
			data.AddParam("@Mileage", DataAccessParameterType.Numeric, Mileage.ToString());
            data.AddParam("@EngineDescription", DataAccessParameterType.Text, this.EngineDescription);
            data.AddParam("@Cylinders", DataAccessParameterType.Numeric, this.Cylinders.ToString());
            data.AddParam("@FuelType", DataAccessParameterType.Text, this.FuelType);
            data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			data.AddParam("@Description", DataAccessParameterType.Text, this.Description);
            data.AddParam("@Price", DataAccessParameterType.Numeric, this.Price.ToString());
            data.AddParam("@specialprice", DataAccessParameterType.Numeric, this.SpecialPrice.ToString());
            data.AddParam("@ExteriorColor", DataAccessParameterType.Text, this.ExteriorColor);
            data.AddParam("@InteriorColor", DataAccessParameterType.Text, this.InteriorColor);
            data.AddParam("@Options", DataAccessParameterType.Text, this.Options);
            data.AddParam("@Description", DataAccessParameterType.Text, this.Description);
		    data.AddParam("@images", DataAccessParameterType.Numeric, this.Images);
            data.ExecuteProcedure("RAW_CARWEEK_InsertUpdate");
			if (!data.EOF)
			{
                this.RawCarweekId = int.Parse(data["RawCarWeekId"]);
			}
			this.retrievedata();
		}
	}
}