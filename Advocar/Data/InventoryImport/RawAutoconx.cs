using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawAutoconx : Transaction
	{
		private int rawAutoconxId = 0;

		private int jobExecutionRawId = 0;

		private string dealerId = "";

		private string stockNumber = "";

		private string bodyStyle = "";

		private string vin = "";

		private int vehicleYear = 0;

		private string make = "";

		private string model = "";

		private string trim = "";

		private int miles = 0;

		private string transmission = "";

		private string engine = "";

		private string drivetrain = "";

		private int cylinders = 0;

		private string fuelType = "";

		private float price = 0f;

		private float priceInvoice = 0f;

		private float priceMsrp = 0f;

		private float priceRebate = 0f;

		private string exteriorColor = "";

		private string interiorColor = "";

		private string certification = "";

		private string options = "";

		private string description = "";

		private bool isNew = false;

		private string photoUrl = "";

		public string BodyStyle
		{
			get
			{
				return this.bodyStyle;
			}
			set
			{
				this.bodyStyle = value;
			}
		}

		public string Certification
		{
			get
			{
				return this.certification;
			}
			set
			{
				this.certification = value;
			}
		}

		public int Cylinders
		{
			get
			{
				return this.cylinders;
			}
			set
			{
				this.cylinders = value;
			}
		}

		public string DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

		public string Drivetrain
		{
			get
			{
				return this.drivetrain;
			}
			set
			{
				this.drivetrain = value;
			}
		}

		public string Engine
		{
			get
			{
				return this.engine;
			}
			set
			{
				this.engine = value;
			}
		}

		public string ExteriorColor
		{
			get
			{
				return this.exteriorColor;
			}
			set
			{
				this.exteriorColor = value;
			}
		}

		public string FuelType
		{
			get
			{
				return this.fuelType;
			}
			set
			{
				this.fuelType = value;
			}
		}

		public string InteriorColor
		{
			get
			{
				return this.interiorColor;
			}
			set
			{
				this.interiorColor = value;
			}
		}

		public bool IsNew
		{
			get
			{
				return this.isNew;
			}
			set
			{
				this.isNew = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public string Make
		{
			get
			{
				return this.make;
			}
			set
			{
				this.make = value;
			}
		}

		public int Miles
		{
			get
			{
				return this.miles;
			}
			set
			{
				this.miles = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public string Options
		{
			get
			{
				return this.options;
			}
			set
			{
				this.options = value;
			}
		}

		public string PhotoUrl
		{
			get
			{
				return this.photoUrl;
			}
			set
			{
				this.photoUrl = value;
			}
		}

		public float Price
		{
			get
			{
				return this.price;
			}
			set
			{
				this.price = value;
			}
		}

		public float PriceInvoice
		{
			get
			{
				return this.priceInvoice;
			}
			set
			{
				this.priceInvoice = value;
			}
		}

		public float PriceMsrp
		{
			get
			{
				return this.priceMsrp;
			}
			set
			{
				this.priceMsrp = value;
			}
		}

		public float PriceRebate
		{
			get
			{
				return this.priceRebate;
			}
			set
			{
				this.priceRebate = value;
			}
		}

		public int RawAutoconxId
		{
			get
			{
				return this.rawAutoconxId;
			}
			set
			{
				this.rawAutoconxId = value;
			}
		}

		public string StockNumber
		{
			get
			{
				return this.stockNumber;
			}
			set
			{
				this.stockNumber = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string Trim
		{
			get
			{
				return this.trim;
			}
			set
			{
				this.trim = value;
			}
		}

		public int VehicleYear
		{
			get
			{
				return this.vehicleYear;
			}
			set
			{
				this.vehicleYear = value;
			}
		}

		public string Vin
		{
			get
			{
				return this.vin;
			}
			set
			{
				this.vin = value;
			}
		}

		public RawAutoconx()
		{
		}

		public RawAutoconx(string connection, string modifiedUserID, int rawAutoconxId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "RAW_AUTOCONX";
			this.RawAutoconxId = rawAutoconxId;
			if (this.RawAutoconxId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawAutoconxId", DataAccessParameterType.Numeric, this.RawAutoconxId.ToString());
			data.ExecuteProcedure("RAW_AUTOCONX_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawAutoconxId", DataAccessParameterType.Numeric, this.RawAutoconxId.ToString());
			data.ExecuteProcedure("RAW_AUTOCONX_GetRecord");
			if (!data.EOF)
			{
				this.RawAutoconxId = int.Parse(data["RawAutoconxId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerId = data["DealerId"];
				this.StockNumber = data["StockNumber"];
				this.BodyStyle = data["BodyStyle"];
				this.Vin = data["Vin"];
				this.VehicleYear = int.Parse(data["VehicleYear"]);
				this.Make = data["Make"];
				this.Model = data["Model"];
				this.Trim = data["Trim"];
				this.Miles = int.Parse(data["Miles"]);
				this.Transmission = data["Transmission"];
				this.Engine = data["Engine"];
				this.Drivetrain = data["Drivetrain"];
				this.Cylinders = int.Parse(data["Cylinders"]);
				this.FuelType = data["FuelType"];
				this.Price = float.Parse(data["Price"]);
				this.PriceInvoice = float.Parse(data["PriceInvoice"]);
				this.PriceMsrp = float.Parse(data["PriceMsrp"]);
				this.PriceRebate = float.Parse(data["PriceRebate"]);
				this.ExteriorColor = data["ExteriorColor"];
				this.InteriorColor = data["InteriorColor"];
				this.Certification = data["Certification"];
				this.Options = data["Options"];
				this.Description = data["Description"];
				this.IsNew = bool.Parse(data["IsNew"]);
				this.PhotoUrl = data["PhotoUrl"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawAutoconxId = this.RawAutoconxId;
			data.AddParam("@RawAutoconxId", DataAccessParameterType.Numeric, rawAutoconxId.ToString());
			rawAutoconxId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawAutoconxId.ToString());
			data.AddParam("@DealerId", DataAccessParameterType.Text, this.DealerId);
			data.AddParam("@StockNumber", DataAccessParameterType.Text, this.StockNumber);
			data.AddParam("@BodyStyle", DataAccessParameterType.Text, this.BodyStyle);
			data.AddParam("@Vin", DataAccessParameterType.Text, this.Vin);
			rawAutoconxId = this.VehicleYear;
			data.AddParam("@VehicleYear", DataAccessParameterType.Numeric, rawAutoconxId.ToString());
			data.AddParam("@Make", DataAccessParameterType.Text, this.Make);
			data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
			data.AddParam("@Trim", DataAccessParameterType.Text, this.Trim);
			rawAutoconxId = this.Miles;
			data.AddParam("@Miles", DataAccessParameterType.Numeric, rawAutoconxId.ToString());
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			data.AddParam("@Engine", DataAccessParameterType.Text, this.Engine);
			data.AddParam("@Drivetrain", DataAccessParameterType.Text, this.Drivetrain);
			rawAutoconxId = this.Cylinders;
			data.AddParam("@Cylinders", DataAccessParameterType.Numeric, rawAutoconxId.ToString());
			data.AddParam("@FuelType", DataAccessParameterType.Text, this.FuelType);
			float price = this.Price;
			data.AddParam("@Price", DataAccessParameterType.Numeric, price.ToString());
			price = this.PriceInvoice;
			data.AddParam("@PriceInvoice", DataAccessParameterType.Numeric, price.ToString());
			price = this.PriceMsrp;
			data.AddParam("@PriceMsrp", DataAccessParameterType.Numeric, price.ToString());
			price = this.PriceRebate;
			data.AddParam("@PriceRebate", DataAccessParameterType.Numeric, price.ToString());
			data.AddParam("@ExteriorColor", DataAccessParameterType.Text, this.ExteriorColor);
			data.AddParam("@InteriorColor", DataAccessParameterType.Text, this.InteriorColor);
			data.AddParam("@Certification", DataAccessParameterType.Text, this.Certification);
			data.AddParam("@Options", DataAccessParameterType.Text, this.Options);
			data.AddParam("@Description", DataAccessParameterType.Text, this.Description);
			data.AddParam("@IsNew", DataAccessParameterType.Bool, this.IsNew.ToString());
			data.AddParam("@PhotoUrl", DataAccessParameterType.Text, this.PhotoUrl);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("RAW_AUTOCONX_InsertUpdate");
			if (!data.EOF)
			{
				this.RawAutoconxId = int.Parse(data["RawAutoconxId"]);
			}
			this.retrievedata();
		}
	}
}