using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawVinSolutions : Transaction
	{
		private int rawVinSolutionsId = 0;

		private int jobExecutionRawId = 0;

		private string dealerId = "";

		private string newUsed = "";

		private int year = 0;

		private string make = "";

		private string model = "";

		private string trim = "";

		private string bodyStyle = "";

		private string stockNumber = "";

		private string vin = "";

		private int mileage = 0;

		private float price = 0f;

		private string engine = "";

		private string transmission = "";

		private string color = "";

		private string interior = "";

		private string comment = "";

		private string options = "";

		private float lotPrice = 0f;

		private float msrp = 0f;

		private float invoice = 0f;

		private string imageURLs = "";

		private string certified = "";

		private int doors = 0;

		private string modelSeries = "";

		private string engineCylinders = "";

		public string BodyStyle
		{
			get
			{
				return this.bodyStyle;
			}
			set
			{
				this.bodyStyle = value;
			}
		}

		public string Certified
		{
			get
			{
				return this.certified;
			}
			set
			{
				this.certified = value;
			}
		}

		public string Color
		{
			get
			{
				return this.color;
			}
			set
			{
				this.color = value;
			}
		}

		public string Comment
		{
			get
			{
				return this.comment;
			}
			set
			{
				this.comment = value;
			}
		}

		public string DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public int Doors
		{
			get
			{
				return this.doors;
			}
			set
			{
				this.doors = value;
			}
		}

		public string Engine
		{
			get
			{
				return this.engine;
			}
			set
			{
				this.engine = value;
			}
		}

		public string EngineCylinders
		{
			get
			{
				return this.engineCylinders;
			}
			set
			{
				this.engineCylinders = value;
			}
		}

		public string ImageURLs
		{
			get
			{
				return this.imageURLs;
			}
			set
			{
				this.imageURLs = value;
			}
		}

		public string Interior
		{
			get
			{
				return this.interior;
			}
			set
			{
				this.interior = value;
			}
		}

		public float Invoice
		{
			get
			{
				return this.invoice;
			}
			set
			{
				this.invoice = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public float LotPrice
		{
			get
			{
				return this.lotPrice;
			}
			set
			{
				this.lotPrice = value;
			}
		}

		public string Make
		{
			get
			{
				return this.make;
			}
			set
			{
				this.make = value;
			}
		}

		public int Mileage
		{
			get
			{
				return this.mileage;
			}
			set
			{
				this.mileage = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public string ModelSeries
		{
			get
			{
				return this.modelSeries;
			}
			set
			{
				this.modelSeries = value;
			}
		}

		public float Msrp
		{
			get
			{
				return this.msrp;
			}
			set
			{
				this.msrp = value;
			}
		}

		public string NewUsed
		{
			get
			{
				return this.newUsed;
			}
			set
			{
				this.newUsed = value;
			}
		}

		public string Options
		{
			get
			{
				return this.options;
			}
			set
			{
				this.options = value;
			}
		}

		public float Price
		{
			get
			{
				return this.price;
			}
			set
			{
				this.price = value;
			}
		}

		public int RawVinSolutionsId
		{
			get
			{
				return this.rawVinSolutionsId;
			}
			set
			{
				this.rawVinSolutionsId = value;
			}
		}

		public string StockNumber
		{
			get
			{
				return this.stockNumber;
			}
			set
			{
				this.stockNumber = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string Trim
		{
			get
			{
				return this.trim;
			}
			set
			{
				this.trim = value;
			}
		}

		public string Vin
		{
			get
			{
				return this.vin;
			}
			set
			{
				this.vin = value;
			}
		}

		public int Year
		{
			get
			{
				return this.year;
			}
			set
			{
				this.year = value;
			}
		}

		public RawVinSolutions()
		{
		}

		public RawVinSolutions(string connection, string modifiedUserID, int rawVinSolutionsId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.RawVinSolutionsId = rawVinSolutionsId;
			this.databaseObjectName = "RAW_VIN_SOLUTIONS";
			if (this.RawVinSolutionsId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawVinSolutionsId", DataAccessParameterType.Numeric, this.RawVinSolutionsId.ToString());
			data.ExecuteProcedure("RAW_VinSolutions_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawVinSolutionsId", DataAccessParameterType.Numeric, this.RawVinSolutionsId.ToString());
			data.ExecuteProcedure("RAW_VIN_SOLUTIONS_GetRecord");
			if (!data.EOF)
			{
				this.RawVinSolutionsId = int.Parse(data["RawVinSolutionsId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerId = data["DealerId"];
				this.NewUsed = data["NewUsed"];
				this.Year = int.Parse(data["Year"]);
				this.Make = data["Make"];
				this.Model = data["Model"];
				this.Trim = data["Trim"];
				this.BodyStyle = data["BodyStyle"];
				this.StockNumber = data["StockNumber"];
				this.Vin = data["Vin"];
				this.Mileage = int.Parse(data["Mileage"]);
				this.Price = float.Parse(data["Price"]);
				this.Engine = data["Engine"];
				this.Transmission = data["Transmission"];
				this.Color = data["Color"];
				this.Interior = data["Interior"];
				this.Comment = data["Comment"];
				this.Options = data["Options"];
				this.LotPrice = float.Parse(data["LotPrice"]);
				this.Msrp = float.Parse(data["MSRP"]);
				this.Invoice = float.Parse(data["Invoice"]);
				this.ImageURLs = data["ImageURLs"];
				this.Certified = data["Certified"];
				this.Doors = int.Parse(data["Doors"]);
				this.ModelSeries = data["ModelSeries"];
				this.EngineCylinders = data["EngineCylinders"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawVinSolutionsId = this.RawVinSolutionsId;
			data.AddParam("@RawVinSolutionsId", DataAccessParameterType.Numeric, rawVinSolutionsId.ToString());
			rawVinSolutionsId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawVinSolutionsId.ToString());
			data.AddParam("@DealerId", DataAccessParameterType.Text, this.DealerId);
			data.AddParam("@NewUsed", DataAccessParameterType.Text, this.NewUsed);
			rawVinSolutionsId = this.Year;
			data.AddParam("@Year", DataAccessParameterType.Numeric, rawVinSolutionsId.ToString());
			data.AddParam("@Make", DataAccessParameterType.Text, this.Make);
			data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
			data.AddParam("@Trim", DataAccessParameterType.Text, this.Trim);
			data.AddParam("@BodyStyle", DataAccessParameterType.Text, this.BodyStyle);
			data.AddParam("@StockNumber", DataAccessParameterType.Text, this.StockNumber);
			data.AddParam("@Vin", DataAccessParameterType.Text, this.Vin);
			rawVinSolutionsId = this.Mileage;
			data.AddParam("@Mileage", DataAccessParameterType.Numeric, rawVinSolutionsId.ToString());
			float price = this.Price;
			data.AddParam("@Price", DataAccessParameterType.Numeric, price.ToString());
			data.AddParam("@Engine", DataAccessParameterType.Text, this.Engine);
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			data.AddParam("@Color", DataAccessParameterType.Text, this.Color);
			data.AddParam("@Interior", DataAccessParameterType.Text, this.Interior);
			data.AddParam("@Comment", DataAccessParameterType.Text, this.Comment);
			data.AddParam("@Options", DataAccessParameterType.Text, this.Options);
			price = this.LotPrice;
			data.AddParam("@LotPrice", DataAccessParameterType.Numeric, price.ToString());
			price = this.Msrp;
			data.AddParam("@MSRP", DataAccessParameterType.Numeric, price.ToString());
			price = this.Invoice;
			data.AddParam("@Invoice", DataAccessParameterType.Numeric, price.ToString());
			data.AddParam("@ImageURLs", DataAccessParameterType.Text, this.ImageURLs);
			data.AddParam("@Certified", DataAccessParameterType.Text, this.Certified);
			rawVinSolutionsId = this.Doors;
			data.AddParam("@Doors", DataAccessParameterType.Numeric, rawVinSolutionsId.ToString());
			data.AddParam("@ModelSeries", DataAccessParameterType.Text, this.ModelSeries);
			data.AddParam("@EngineCylinder", DataAccessParameterType.Text, this.EngineCylinders);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("RAW_VinSolutions_InsertUpdate");
			if (!data.EOF)
			{
				this.RawVinSolutionsId = int.Parse(data["RawVinSolutionsId"]);
			}
			this.retrievedata();
		}
	}
}