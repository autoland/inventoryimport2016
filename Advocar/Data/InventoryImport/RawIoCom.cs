using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawIoCom : Transaction
	{
		private int rawIoComId = 0;

		private int jobExecutionRawId = 0;

		private string dealerId = "";

		private string newUsed = "";

		private string vin = "";

		private int miles = 0;

		private int doors = 0;

		private string dealerStockNo = "";

		private string feedVehicleDescription = "";

		private int feedVehicleYear = 0;

		private string feedVehicleMake = "";

		private string feedVehicleModel = "";

		private string feedVehicleTrim = "";

		private string feedVehicleStyle = "";

		private string feedVehicleEngine = "";

		private string feedVehicleDrivetrain = "";

		private string feedVehicleTransmission = "";

		private string exteriorColor = "";

		private string interiorColor = "";

		private string optionList = "";

		private int noOfOptions = 0;

		private string imageList = "";

		private int noOfImages = 0;

		private float feedPriceRetailMsrp = 0f;

		private float feedPriceAcquisition = 0f;

		private float feedPriceInvoiceWholesale = 0f;

		private float feedPriceSelling = 0f;

		public string DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public string DealerStockNo
		{
			get
			{
				return this.dealerStockNo;
			}
			set
			{
				this.dealerStockNo = value;
			}
		}

		public int Doors
		{
			get
			{
				return this.doors;
			}
			set
			{
				this.doors = value;
			}
		}

		public string ExteriorColor
		{
			get
			{
				return this.exteriorColor;
			}
			set
			{
				this.exteriorColor = value;
			}
		}

		public float FeedPriceAcquisition
		{
			get
			{
				return this.feedPriceAcquisition;
			}
			set
			{
				this.feedPriceAcquisition = value;
			}
		}

		public float FeedPriceInvoiceWholesale
		{
			get
			{
				return this.feedPriceInvoiceWholesale;
			}
			set
			{
				this.feedPriceInvoiceWholesale = value;
			}
		}

		public float FeedPriceRetailMsrp
		{
			get
			{
				return this.feedPriceRetailMsrp;
			}
			set
			{
				this.feedPriceRetailMsrp = value;
			}
		}

		public float FeedPriceSelling
		{
			get
			{
				return this.feedPriceSelling;
			}
			set
			{
				this.feedPriceSelling = value;
			}
		}

		public string FeedVehicleDescription
		{
			get
			{
				return this.feedVehicleDescription;
			}
			set
			{
				this.feedVehicleDescription = value;
			}
		}

		public string FeedVehicleDrivetrain
		{
			get
			{
				return this.feedVehicleDrivetrain;
			}
			set
			{
				this.feedVehicleDrivetrain = value;
			}
		}

		public string FeedVehicleEngine
		{
			get
			{
				return this.feedVehicleEngine;
			}
			set
			{
				this.feedVehicleEngine = value;
			}
		}

		public string FeedVehicleMake
		{
			get
			{
				return this.feedVehicleMake;
			}
			set
			{
				this.feedVehicleMake = value;
			}
		}

		public string FeedVehicleModel
		{
			get
			{
				return this.feedVehicleModel;
			}
			set
			{
				this.feedVehicleModel = value;
			}
		}

		public string FeedVehicleStyle
		{
			get
			{
				return this.feedVehicleStyle;
			}
			set
			{
				this.feedVehicleStyle = value;
			}
		}

		public string FeedVehicleTransmission
		{
			get
			{
				return this.feedVehicleTransmission;
			}
			set
			{
				this.feedVehicleTransmission = value;
			}
		}

		public string FeedVehicleTrim
		{
			get
			{
				return this.feedVehicleTrim;
			}
			set
			{
				this.feedVehicleTrim = value;
			}
		}

		public int FeedVehicleYear
		{
			get
			{
				return this.feedVehicleYear;
			}
			set
			{
				this.feedVehicleYear = value;
			}
		}

		public string ImageList
		{
			get
			{
				return this.imageList;
			}
			set
			{
				this.imageList = value;
			}
		}

		public string InteriorColor
		{
			get
			{
				return this.interiorColor;
			}
			set
			{
				this.interiorColor = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public int Miles
		{
			get
			{
				return this.miles;
			}
			set
			{
				this.miles = value;
			}
		}

		public string NewUsed
		{
			get
			{
				return this.newUsed;
			}
			set
			{
				this.newUsed = value;
			}
		}

		public int NoOfImages
		{
			get
			{
				return this.noOfImages;
			}
			set
			{
				this.noOfImages = value;
			}
		}

		public int NoOfOptions
		{
			get
			{
				return this.noOfOptions;
			}
			set
			{
				this.noOfOptions = value;
			}
		}

		public string OptionList
		{
			get
			{
				return this.optionList;
			}
			set
			{
				this.optionList = value;
			}
		}

		public int RawIoComId
		{
			get
			{
				return this.rawIoComId;
			}
			set
			{
				this.rawIoComId = value;
			}
		}

		public string Vin
		{
			get
			{
				return this.vin;
			}
			set
			{
				this.vin = value;
			}
		}

		public RawIoCom()
		{
		}

		public RawIoCom(string connection, string modifiedUserID, int rawIoComId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.RawIoComId = rawIoComId;
			this.databaseObjectName = "RAW_IO_COM";
			if (this.RawIoComId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawIoComId", DataAccessParameterType.Numeric, this.RawIoComId.ToString());
			data.ExecuteProcedure("RAW_IO_COM_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawIoComId", DataAccessParameterType.Numeric, this.RawIoComId.ToString());
			data.ExecuteProcedure("RAW_IO_COM_GetRecord");
			if (!data.EOF)
			{
				this.RawIoComId = int.Parse(data["RawIoComId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerId = data["DealerId"];
				this.NewUsed = data["NewUsed"];
				this.Vin = data["Vin"];
				this.Miles = int.Parse(data["Miles"]);
				this.Doors = int.Parse(data["Doors"]);
				this.DealerStockNo = data["DealerStockNo"];
				this.FeedVehicleDescription = data["FeedVehicleDescription"];
				this.FeedVehicleYear = int.Parse(data["FeedVehicleYear"]);
				this.FeedVehicleMake = data["FeedVehicleMake"];
				this.FeedVehicleModel = data["FeedVehicleModel"];
				this.FeedVehicleTrim = data["FeedVehicleTrim"];
				this.FeedVehicleStyle = data["FeedVehicleStyle"];
				this.FeedVehicleEngine = data["FeedVehicleEngine"];
				this.FeedVehicleDrivetrain = data["FeedVehicleDrivetrain"];
				this.FeedVehicleTransmission = data["FeedVehicleTransmission"];
				this.ExteriorColor = data["ExteriorColor"];
				this.InteriorColor = data["InteriorColor"];
				this.OptionList = data["OptionList"];
				this.NoOfOptions = int.Parse(data["NoOfOptions"]);
				this.ImageList = data["ImageList"];
				this.NoOfImages = int.Parse(data["NoOfImages"]);
				this.FeedPriceRetailMsrp = float.Parse(data["FeedPriceRetailMsrp"]);
				this.FeedPriceAcquisition = float.Parse(data["FeedPriceAcquisition"]);
				this.FeedPriceInvoiceWholesale = float.Parse(data["FeedPriceInvoiceWholesale"]);
				this.FeedPriceSelling = float.Parse(data["FeedPriceSelling"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawIoComId = this.RawIoComId;
			data.AddParam("@RawIoComId", DataAccessParameterType.Numeric, rawIoComId.ToString());
			rawIoComId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawIoComId.ToString());
			data.AddParam("@DealerId", DataAccessParameterType.Text, this.DealerId);
			data.AddParam("@NewUsed", DataAccessParameterType.Text, this.NewUsed);
			data.AddParam("@Vin", DataAccessParameterType.Text, this.Vin);
			rawIoComId = this.Miles;
			data.AddParam("@Miles", DataAccessParameterType.Numeric, rawIoComId.ToString());
			rawIoComId = this.Doors;
			data.AddParam("@Doors", DataAccessParameterType.Numeric, rawIoComId.ToString());
			data.AddParam("@DealerStockNo", DataAccessParameterType.Text, this.DealerStockNo);
			data.AddParam("@FeedVehicleDescription", DataAccessParameterType.Text, this.FeedVehicleDescription);
			rawIoComId = this.FeedVehicleYear;
			data.AddParam("@FeedVehicleYear", DataAccessParameterType.Numeric, rawIoComId.ToString());
			data.AddParam("@FeedVehicleMake", DataAccessParameterType.Text, this.FeedVehicleMake);
			data.AddParam("@FeedVehicleModel", DataAccessParameterType.Text, this.FeedVehicleModel);
			data.AddParam("@FeedVehicleTrim", DataAccessParameterType.Text, this.FeedVehicleTrim);
			data.AddParam("@FeedVehicleStyle", DataAccessParameterType.Text, this.FeedVehicleStyle);
			data.AddParam("@FeedVehicleEngine", DataAccessParameterType.Text, this.FeedVehicleEngine);
			data.AddParam("@FeedVehicleDrivetrain", DataAccessParameterType.Text, this.FeedVehicleDrivetrain);
			data.AddParam("@FeedVehicleTransmission", DataAccessParameterType.Text, this.FeedVehicleTransmission);
			data.AddParam("@ExteriorColor", DataAccessParameterType.Text, this.ExteriorColor);
			data.AddParam("@InteriorColor", DataAccessParameterType.Text, this.InteriorColor);
			data.AddParam("@OptionList", DataAccessParameterType.Text, this.OptionList);
			rawIoComId = this.NoOfOptions;
			data.AddParam("@NoOfOptions", DataAccessParameterType.Numeric, rawIoComId.ToString());
			data.AddParam("@ImageList", DataAccessParameterType.Text, this.ImageList);
			rawIoComId = this.NoOfImages;
			data.AddParam("@NoOfImages", DataAccessParameterType.Numeric, rawIoComId.ToString());
			float feedPriceRetailMsrp = this.FeedPriceRetailMsrp;
			data.AddParam("@FeedPriceRetailMsrp", DataAccessParameterType.Numeric, feedPriceRetailMsrp.ToString());
			feedPriceRetailMsrp = this.FeedPriceAcquisition;
			data.AddParam("@FeedPriceAcquisition", DataAccessParameterType.Numeric, feedPriceRetailMsrp.ToString());
			feedPriceRetailMsrp = this.FeedPriceInvoiceWholesale;
			data.AddParam("@FeedPriceInvoiceWholesale", DataAccessParameterType.Numeric, feedPriceRetailMsrp.ToString());
			feedPriceRetailMsrp = this.FeedPriceSelling;
			data.AddParam("@FeedPriceSelling", DataAccessParameterType.Numeric, feedPriceRetailMsrp.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("RAW_IO_COM_InsertUpdate");
			if (!data.EOF)
			{
				this.RawIoComId = int.Parse(data["RawIoComId"]);
			}
			this.retrievedata();
		}
	}
}