using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawCobalt : Transaction
	{
		private int rawCobaltId = 0;

		private int jobExecutionRawId = 0;

		private string dealerId = "";

		private string newUsed = "";

		private string dealerStockNo = "";

		private string vehicleDescription = "";

		private string vehicleYear = "";

		private string vehicleMake = "";

		private string vehicleModel = "";

		private string vehicleTrim = "";

		private string vin = "";

		private string vehicleStyle = "";

		private string engine = "";

		private string drivetrain = "";

		private string transmission = "";

		private string exteriorColor = "";

		private string interiorColor = "";

		private string optionList = "";

		private int miles = 0;

		private float priceAquisition = 0f;

		private float priceInvoice = 0f;

		private float priceSelling = 0f;

		private float priceRetail = 0f;

		private float priceMarketIndex = 0f;

		private float priceMsrp = 0f;

		private string imageLocationList = "";

		private string warranty = "";

		private int doors = 0;

		private string fuelType = "";

		public string DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public string DealerStockNo
		{
			get
			{
				return this.dealerStockNo;
			}
			set
			{
				this.dealerStockNo = value;
			}
		}

		public int Doors
		{
			get
			{
				return this.doors;
			}
			set
			{
				this.doors = value;
			}
		}

		public string Drivetrain
		{
			get
			{
				return this.drivetrain;
			}
			set
			{
				this.drivetrain = value;
			}
		}

		public string Engine
		{
			get
			{
				return this.engine;
			}
			set
			{
				this.engine = value;
			}
		}

		public string ExteriorColor
		{
			get
			{
				return this.exteriorColor;
			}
			set
			{
				this.exteriorColor = value;
			}
		}

		public string FuelType
		{
			get
			{
				return this.fuelType;
			}
			set
			{
				this.fuelType = value;
			}
		}

		public string ImageLocationList
		{
			get
			{
				return this.imageLocationList;
			}
			set
			{
				this.imageLocationList = value;
			}
		}

		public string InteriorColor
		{
			get
			{
				return this.interiorColor;
			}
			set
			{
				this.interiorColor = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public int Miles
		{
			get
			{
				return this.miles;
			}
			set
			{
				this.miles = value;
			}
		}

		public string NewUsed
		{
			get
			{
				return this.newUsed;
			}
			set
			{
				this.newUsed = value;
			}
		}

		public string OptionList
		{
			get
			{
				return this.optionList;
			}
			set
			{
				this.optionList = value;
			}
		}

		public float PriceAquisition
		{
			get
			{
				return this.priceAquisition;
			}
			set
			{
				this.priceAquisition = value;
			}
		}

		public float PriceInvoice
		{
			get
			{
				return this.priceInvoice;
			}
			set
			{
				this.priceInvoice = value;
			}
		}

		public float PriceMarketIndex
		{
			get
			{
				return this.priceMarketIndex;
			}
			set
			{
				this.priceMarketIndex = value;
			}
		}

		public float PriceMsrp
		{
			get
			{
				return this.priceMsrp;
			}
			set
			{
				this.priceMsrp = value;
			}
		}

		public float PriceRetail
		{
			get
			{
				return this.priceRetail;
			}
			set
			{
				this.priceRetail = value;
			}
		}

		public float PriceSelling
		{
			get
			{
				return this.priceSelling;
			}
			set
			{
				this.priceSelling = value;
			}
		}

		public int RawCobaltId
		{
			get
			{
				return this.rawCobaltId;
			}
			set
			{
				this.rawCobaltId = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string VehicleDescription
		{
			get
			{
				return this.vehicleDescription;
			}
			set
			{
				this.vehicleDescription = value;
			}
		}

		public string VehicleMake
		{
			get
			{
				return this.vehicleMake;
			}
			set
			{
				this.vehicleMake = value;
			}
		}

		public string VehicleModel
		{
			get
			{
				return this.vehicleModel;
			}
			set
			{
				this.vehicleModel = value;
			}
		}

		public string VehicleStyle
		{
			get
			{
				return this.vehicleStyle;
			}
			set
			{
				this.vehicleStyle = value;
			}
		}

		public string VehicleTrim
		{
			get
			{
				return this.vehicleTrim;
			}
			set
			{
				this.vehicleTrim = value;
			}
		}

		public string VehicleYear
		{
			get
			{
				return this.vehicleYear;
			}
			set
			{
				this.vehicleYear = value;
			}
		}

		public string Vin
		{
			get
			{
				return this.vin;
			}
			set
			{
				this.vin = value;
			}
		}

		public string Warranty
		{
			get
			{
				return this.warranty;
			}
			set
			{
				this.warranty = value;
			}
		}

		public RawCobalt()
		{
		}

		public RawCobalt(string connection, string modifiedUserID, int rawCobaltId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "RAW_COBALT";
			this.RawCobaltId = rawCobaltId;
			if (this.RawCobaltId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawCobaltId", DataAccessParameterType.Numeric, this.RawCobaltId.ToString());
			data.ExecuteProcedure("RAW_COBALT_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawCobaltId", DataAccessParameterType.Numeric, this.RawCobaltId.ToString());
			data.ExecuteProcedure("RAW_COBALT_GetRecord");
			if (!data.EOF)
			{
				this.RawCobaltId = int.Parse(data["RawCobaltId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerId = data["DealerId"];
				this.NewUsed = data["NewUsed"];
				this.DealerStockNo = data["DealerStockNo"];
				this.VehicleDescription = data["VehicleDescription"];
				this.VehicleYear = data["VehicleYear"];
				this.VehicleMake = data["VehicleMake"];
				this.VehicleModel = data["VehicleModel"];
				this.VehicleTrim = data["VehicleTrim"];
				this.Vin = data["Vin"];
				this.VehicleStyle = data["VehicleStyle"];
				this.Engine = data["Engine"];
				this.Drivetrain = data["Drivetrain"];
				this.Transmission = data["Transmission"];
				this.ExteriorColor = data["ExteriorColor"];
				this.InteriorColor = data["InteriorColor"];
				this.OptionList = data["OptionList"];
				this.Miles = int.Parse(data["Miles"]);
				this.PriceAquisition = float.Parse(data["PriceAquisition"]);
				this.PriceInvoice = float.Parse(data["PriceInvoice"]);
				this.PriceSelling = float.Parse(data["PriceSelling"]);
				this.PriceRetail = float.Parse(data["PriceRetail"]);
				this.PriceMarketIndex = float.Parse(data["PriceMarketIndex"]);
				this.PriceMsrp = float.Parse(data["PriceMsrp"]);
				this.ImageLocationList = data["ImageLocationList"];
				this.Warranty = data["Warranty"];
				this.Doors = int.Parse(data["Doors"]);
				this.FuelType = data["FuelType"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawCobaltId = this.RawCobaltId;
			data.AddParam("@RawCobaltId", DataAccessParameterType.Numeric, rawCobaltId.ToString());
			rawCobaltId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawCobaltId.ToString());
			data.AddParam("@DealerId", DataAccessParameterType.Text, this.DealerId);
			data.AddParam("@NewUsed", DataAccessParameterType.Text, this.NewUsed);
			data.AddParam("@DealerStockNo", DataAccessParameterType.Text, this.DealerStockNo);
			data.AddParam("@VehicleDescription", DataAccessParameterType.Text, this.VehicleDescription);
			data.AddParam("@VehicleYear", DataAccessParameterType.Text, this.VehicleYear);
			data.AddParam("@VehicleMake", DataAccessParameterType.Text, this.VehicleMake);
			data.AddParam("@VehicleModel", DataAccessParameterType.Text, this.VehicleModel);
			data.AddParam("@VehicleTrim", DataAccessParameterType.Text, this.VehicleTrim);
			data.AddParam("@Vin", DataAccessParameterType.Text, this.Vin);
			data.AddParam("@VehicleStyle", DataAccessParameterType.Text, this.VehicleStyle);
			data.AddParam("@Engine", DataAccessParameterType.Text, this.Engine);
			data.AddParam("@Drivetrain", DataAccessParameterType.Text, this.Drivetrain);
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			data.AddParam("@ExteriorColor", DataAccessParameterType.Text, this.ExteriorColor);
			data.AddParam("@InteriorColor", DataAccessParameterType.Text, this.InteriorColor);
			data.AddParam("@OptionList", DataAccessParameterType.Text, this.OptionList);
			rawCobaltId = this.Miles;
			data.AddParam("@Miles", DataAccessParameterType.Numeric, rawCobaltId.ToString());
			float priceAquisition = this.PriceAquisition;
			data.AddParam("@PriceAquisition", DataAccessParameterType.Numeric, priceAquisition.ToString());
			priceAquisition = this.PriceInvoice;
			data.AddParam("@PriceInvoice", DataAccessParameterType.Numeric, priceAquisition.ToString());
			priceAquisition = this.PriceSelling;
			data.AddParam("@PriceSelling", DataAccessParameterType.Numeric, priceAquisition.ToString());
			priceAquisition = this.PriceRetail;
			data.AddParam("@PriceRetail", DataAccessParameterType.Numeric, priceAquisition.ToString());
			priceAquisition = this.PriceMarketIndex;
			data.AddParam("@PriceMarketIndex", DataAccessParameterType.Numeric, priceAquisition.ToString());
			priceAquisition = this.PriceMsrp;
			data.AddParam("@PriceMsrp", DataAccessParameterType.Numeric, priceAquisition.ToString());
			data.AddParam("@ImageLocationList", DataAccessParameterType.Text, this.ImageLocationList);
			data.AddParam("@Warranty", DataAccessParameterType.Text, this.Warranty);
			rawCobaltId = this.Doors;
			data.AddParam("@Doors", DataAccessParameterType.Numeric, rawCobaltId.ToString());
			data.AddParam("@FuelType", DataAccessParameterType.Text, this.FuelType);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("RAW_COBALT_InsertUpdate");
			if (!data.EOF)
			{
				this.RawCobaltId = int.Parse(data["RawCobaltId"]);
			}
			this.retrievedata();
		}
	}
}