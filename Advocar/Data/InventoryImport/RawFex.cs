﻿using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
    public class RawFex : Transaction
    {
        private int rawFexId = 0;
        private int jobExecutionRawId = 0;
        private int provider_ID = 0;
        private string inst_Name = "";
        private string lot_Add1 = "";
        private string lot_City = "";
        private string lot_State = "";
        private int lot_Zip = 0;
        private string lot_Email = "";
        private string stockNumber = "";
        private int year = 0;
        private string make = "";
        private string model = "";
        private string vin = "";
        private int mileage = 0;
        private DateTime acquiredDate;
        private float askPrice_Low = 0f;
        private string bodyStyle = "";
        private string comments = "";
        private string driveType = "";
        private string engine = "";
        private string extColor = "";
        private string intColor = "";
        private string transmission = "";
        private string images = "";

        public int RawFexId
        {
            get
            {
                return this.rawFexId;
            }
            set
            {
                this.rawFexId = value;
            }
        }

        public int JobExecutionRawId
        {
            get
            {
                return this.jobExecutionRawId;
            }
            set
            {
                this.jobExecutionRawId = value;
            }
        }

        public int Provider_ID
        {
            get
            {
                return this.provider_ID;
            }
            set
            {
                this.provider_ID = value;
            }
        }

        public string Inst_Name
        {
            get
            {
                return this.inst_Name;
            }
            set
            {
                this.inst_Name = value;
            }
        }

        public string Lot_Add1
        {
            get
            {
                return this.lot_Add1;
            }
            set
            {
                this.lot_Add1 = value;
            }
        }

        public string Lot_City
        {
            get
            {
                return this.lot_City;
            }
            set
            {
                this.lot_City = value;
            }
        }

        public string Lot_State
        {
            get
            {
                return this.lot_State;
            }
            set
            {
                this.lot_State = value;
            }
        }

        public int Lot_Zip
        {
            get
            {
                return this.lot_Zip;
            }
            set
            {
                this.lot_Zip = value;
            }
        }

        public string Lot_Email
        {
            get
            {
                return this.lot_Email;
            }
            set
            {
                this.lot_Email = value;
            }
        }

        public string StockNumber
        {
            get
            {
                return this.stockNumber;
            }
            set
            {
                this.stockNumber = value;
            }
        }

        public int Year
        {
            get
            {
                return this.year;
            }
            set
            {
                this.year = value;
            }
        }

        public string Make
        {
            get
            {
                return this.make;
            }
            set
            {
                this.make = value;
            }
        }

        public string Model
        {
            get
            {
                return this.model;
            }
            set
            {
                this.model = value;
            }
        }

        public string Vin
        {
            get
            {
                return this.vin;
            }
            set
            {
                this.vin = value;
            }
        }

        public int Mileage
        {
            get
            {
                return this.mileage;
            }
            set
            {
                this.mileage = value;
            }
        }

        public DateTime AcquiredDate
        {
            get
            {
                return this.acquiredDate;
            }
            set
            {
                this.acquiredDate = value;
            }
        }

        public float AskPrice_Low
        {
            get
            {
                return this.askPrice_Low;
            }
            set
            {
                this.askPrice_Low = value;
            }
        }

        public string BodyStyle
        {
            get
            {
                return this.bodyStyle;
            }
            set
            {
                this.bodyStyle = value;
            }
        }

        public string Comments
        {
            get
            {
                return this.comments;
            }
            set
            {
                this.comments = value;
            }
        }

        public string DriveType
        {
            get
            {
                return this.driveType;
            }
            set
            {
                this.driveType = value;
            }
        }

        public string Engine
        {
            get
            {
                return this.engine;
            }
            set
            {
                this.engine = value;
            }
        }

        public string ExtColor
        {
            get
            {
                return this.extColor;
            }
            set
            {
                this.extColor = value;
            }
        }

        public string IntColor
        {
            get
            {
                return this.intColor;
            }
            set
            {
                this.intColor = value;
            }
        }

        public string Transmission
        {
            get
            {
                return this.transmission;
            }
            set
            {
                this.transmission = value;
            }
        }

        public string Images
        {
            get
            {
                return this.images;
            }
            set
            {
                this.images = value;
            }
        }

        public RawFex()
        {
        }

        public RawFex(string connection, string modifiedUserID, int rawFexId)
            : base(connection, modifiedUserID)
        {
            this.connection = connection;
            this.modifiedUserID = modifiedUserID;
            this.databaseObjectName = "RAW_FEX";
            this.rawFexId = rawFexId;
            if (this.rawFexId > 0)
            {
                this.retrievedata();
            }
        }

        public override void Delete()
        {
            DataAccess data = new DataAccess(this.connection);
            data.AddParam("@RAWFEXID", DataAccessParameterType.Numeric, this.RawFexId.ToString());
            data.ExecuteProcedure("RAW_FEX_Delete");
            this.wipeout();
        }

        protected override void retrievedata()
        {
            DataAccess data = new DataAccess(this.connection);
            data.AddParam("@RAWFEXID", DataAccessParameterType.Numeric, this.RawFexId.ToString());
            data.ExecuteProcedure("RAW_FEX_GetRecord");
            if (!data.EOF)
            {
                this.RawFexId = int.Parse(data["RAW_FEX_ID"]);
                this.JobExecutionRawId = int.Parse(data["JOB_EXECUTION_RAW_ID"]);
                this.Provider_ID = int.Parse(data["Provider_ID"]); ;
                this.Inst_Name = data["Inst_Name"];
                this.Lot_Add1 = data["Lot_Add1"];
                this.Lot_City = data["Lot_City"];
                this.Lot_State = data["Lot_State"];
                this.Lot_Zip = int.Parse(data["Lot_Zip"]); ;
                this.Lot_Email = data["Lot_Email"];
                this.StockNumber = data["StockNumber"];
                this.Year = int.Parse(data["Year"]); ;
                this.Make = data["Make"];
                this.Model = data["Model"];
                this.Vin = data["VIN"];
                this.Mileage = int.Parse(data["Mileage"]); ;
                this.AcquiredDate = Convert.ToDateTime(data["AcquiredDate"]);
                this.AskPrice_Low = float.Parse(data["AskPrice_Low"]);
                this.BodyStyle = data["BodyStyle"];
                this.Comments = data["Comments"];
                this.DriveType = data["DriveType"];
                this.Engine = data["Engine"];
                this.ExtColor = data["ExtColor"];
                this.IntColor = data["IntColor"];
                this.Transmission = data["Transmission"];
                this.Images = data["Images"];
            }
            else
            {
                this.wipeout();
            }
        }

        public override void Save()
        {
            DataAccess data = new DataAccess(this.connection);
            
            data.AddParam("@RAWFEXID", DataAccessParameterType.Numeric, this.RawFexId.ToString());
            data.AddParam("@JOBEXECUTIONRAWID", DataAccessParameterType.Numeric, this.JobExecutionRawId.ToString());
            data.AddParam("@Provider_ID", DataAccessParameterType.Text, this.Provider_ID.ToString());
            data.AddParam("@Inst_Name", DataAccessParameterType.Text, this.Inst_Name);
            data.AddParam("@Lot_Add1", DataAccessParameterType.Text, this.Lot_Add1);
            data.AddParam("@Lot_City", DataAccessParameterType.Text, this.Lot_City);
            data.AddParam("@Lot_State", DataAccessParameterType.Text, this.Lot_State);
            data.AddParam("@Lot_Zip", DataAccessParameterType.Text, this.Lot_Zip.ToString());
            data.AddParam("@Lot_Email", DataAccessParameterType.Text, this.Lot_Email);
            data.AddParam("@StockNumber", DataAccessParameterType.Text, this.StockNumber);
            data.AddParam("@Year", DataAccessParameterType.Text, this.Year.ToString());
            data.AddParam("@Make", DataAccessParameterType.Text, this.Make);
            data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
            data.AddParam("@VIN", DataAccessParameterType.Text, this.Vin);
            data.AddParam("@Mileage", DataAccessParameterType.Text, this.Mileage.ToString());
            data.AddParam("@AcquiredDate", DataAccessParameterType.Numeric, this.AcquiredDate.ToString());
            data.AddParam("@AskPrice_Low", DataAccessParameterType.Numeric, this.AskPrice_Low.ToString());
            data.AddParam("@BodyStyle", DataAccessParameterType.Numeric, this.BodyStyle.ToString());
            data.AddParam("@Comments", DataAccessParameterType.Text, this.Comments);
            data.AddParam("@DriveType", DataAccessParameterType.Text, this.DriveType);
            data.AddParam("@Engine", DataAccessParameterType.Text, this.Engine);
            data.AddParam("@ExtColor", DataAccessParameterType.Text, this.ExtColor);
            data.AddParam("@IntColor", DataAccessParameterType.Text, this.IntColor);
            data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
            data.AddParam("@Images", DataAccessParameterType.Text, this.Images);
            data.ExecuteProcedure("RAW_FEX_InsertUpdate");
            if (!data.EOF)
            {
                this.RawFexId = int.Parse(data["RAW_FEX_ID"]);
            }
            this.retrievedata();
        }
    }
}