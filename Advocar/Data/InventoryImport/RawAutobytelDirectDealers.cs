using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawAutobytelDirectDealers : Transaction
	{
		private int rawAutobytelDirectDealersId = 0;

		private int jobExecutionRawId = 0;

		private string dealerID = "";

		private string name = "";

		private string address = "";

		private string city = "";

		private string state = "";

		private string zipCode = "";

		private string phone = "";

		private string latitude = "";

		private string longitude = "";

		private string contactFirstName = "";

		private string contactLastName = "";

		private string contactPhone = "";

		private string financialRoutingPlatformName = "";

		private string financialRoutingAccountID = "";

		private string monOpen = "";

		private string monClose = "";

		private string tueOpen = "";

		private string tueClose = "";

		private string wedOpen = "";

		private string wedClose = "";

		private string thrOpen = "";

		private string thrClose = "";

		private string friOpen = "";

		private string friClose = "";

		private string satOpen = "";

		private string satClose = "";

		private string sunOpen = "";

		private string sunClose = "";

		public string Address
		{
			get
			{
				return this.address;
			}
			set
			{
				this.address = value;
			}
		}

		public string City
		{
			get
			{
				return this.city;
			}
			set
			{
				this.city = value;
			}
		}

		public string ContactFirstName
		{
			get
			{
				return this.contactFirstName;
			}
			set
			{
				this.contactFirstName = value;
			}
		}

		public string ContactLastName
		{
			get
			{
				return this.contactLastName;
			}
			set
			{
				this.contactLastName = value;
			}
		}

		public string ContactPhone
		{
			get
			{
				return this.contactPhone;
			}
			set
			{
				this.contactPhone = value;
			}
		}

		public string DealerID
		{
			get
			{
				return this.dealerID;
			}
			set
			{
				this.dealerID = value;
			}
		}

		public string FinancialRoutingAccountID
		{
			get
			{
				return this.financialRoutingAccountID;
			}
			set
			{
				this.financialRoutingAccountID = value;
			}
		}

		public string FinancialRoutingPlatformName
		{
			get
			{
				return this.financialRoutingPlatformName;
			}
			set
			{
				this.financialRoutingPlatformName = value;
			}
		}

		public string FriClose
		{
			get
			{
				return this.friClose;
			}
			set
			{
				this.friClose = value;
			}
		}

		public string FriOpen
		{
			get
			{
				return this.friOpen;
			}
			set
			{
				this.friOpen = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public string Latitude
		{
			get
			{
				return this.latitude;
			}
			set
			{
				this.latitude = value;
			}
		}

		public string Longitude
		{
			get
			{
				return this.longitude;
			}
			set
			{
				this.longitude = value;
			}
		}

		public string MonClose
		{
			get
			{
				return this.monClose;
			}
			set
			{
				this.monClose = value;
			}
		}

		public string MonOpen
		{
			get
			{
				return this.monOpen;
			}
			set
			{
				this.monOpen = value;
			}
		}

		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		public string Phone
		{
			get
			{
				return this.phone;
			}
			set
			{
				this.phone = value;
			}
		}

		public int RawAutobytelDirectDealersId
		{
			get
			{
				return this.rawAutobytelDirectDealersId;
			}
			set
			{
				this.rawAutobytelDirectDealersId = value;
			}
		}

		public string SatClose
		{
			get
			{
				return this.satClose;
			}
			set
			{
				this.satClose = value;
			}
		}

		public string SatOpen
		{
			get
			{
				return this.satOpen;
			}
			set
			{
				this.satOpen = value;
			}
		}

		public string State
		{
			get
			{
				return this.state;
			}
			set
			{
				this.state = value;
			}
		}

		public string SunClose
		{
			get
			{
				return this.sunClose;
			}
			set
			{
				this.sunClose = value;
			}
		}

		public string SunOpen
		{
			get
			{
				return this.sunOpen;
			}
			set
			{
				this.sunOpen = value;
			}
		}

		public string ThrClose
		{
			get
			{
				return this.thrClose;
			}
			set
			{
				this.thrClose = value;
			}
		}

		public string ThrOpen
		{
			get
			{
				return this.thrOpen;
			}
			set
			{
				this.thrOpen = value;
			}
		}

		public string TueClose
		{
			get
			{
				return this.tueClose;
			}
			set
			{
				this.tueClose = value;
			}
		}

		public string TueOpen
		{
			get
			{
				return this.tueOpen;
			}
			set
			{
				this.tueOpen = value;
			}
		}

		public string WedClose
		{
			get
			{
				return this.wedClose;
			}
			set
			{
				this.wedClose = value;
			}
		}

		public string WedOpen
		{
			get
			{
				return this.wedOpen;
			}
			set
			{
				this.wedOpen = value;
			}
		}

		public string ZipCode
		{
			get
			{
				return this.zipCode;
			}
			set
			{
				this.zipCode = value;
			}
		}

		public RawAutobytelDirectDealers()
		{
		}

		public RawAutobytelDirectDealers(string connection, string modifiedUserID, int rawAutobytelDirectDealersId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "RAW_AUTOBYTEL_DIRECT_DEALERS";
			this.RawAutobytelDirectDealersId = rawAutobytelDirectDealersId;
			if (this.RawAutobytelDirectDealersId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawAutobytelDirectDealersId", DataAccessParameterType.Numeric, this.RawAutobytelDirectDealersId.ToString());
			data.ExecuteProcedure("RAW_AUTOBYTEL_DIRECT_DEALERS_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawAutobytelDirectDealersId", DataAccessParameterType.Numeric, this.RawAutobytelDirectDealersId.ToString());
			data.ExecuteProcedure("RAW_AUTOBYTEL_DIRECT_DEALERS_GetRecord");
			if (!data.EOF)
			{
				this.RawAutobytelDirectDealersId = int.Parse(data["RawAutobytelDirectDealersId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerID = data["DealerID"];
				this.Name = data["Name"];
				this.Address = data["Address"];
				this.City = data["City"];
				this.State = data["State"];
				this.ZipCode = data["ZipCode"];
				this.Phone = data["Phone"];
				this.Latitude = data["Latitude"];
				this.Longitude = data["Longitude"];
				this.ContactFirstName = data["ContactFirstName"];
				this.ContactLastName = data["ContactLastName"];
				this.ContactPhone = data["ContactPhone"];
				this.FinancialRoutingPlatformName = data["FinancialRoutingPlatformName"];
				this.FinancialRoutingAccountID = data["FinancialRoutingAccountID"];
				this.MonOpen = data["MonOpen"];
				this.MonClose = data["MonClose"];
				this.TueOpen = data["TueOpen"];
				this.TueClose = data["TueClose"];
				this.WedOpen = data["WedOpen"];
				this.WedClose = data["WedClose"];
				this.ThrOpen = data["ThrOpen"];
				this.ThrClose = data["ThrClose"];
				this.FriOpen = data["FriOpen"];
				this.FriClose = data["FriClose"];
				this.SatOpen = data["SatOpen"];
				this.SatClose = data["SatClose"];
				this.SunOpen = data["SunOpen"];
				this.SunClose = data["SunClose"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawAutobytelDirectDealersId = this.RawAutobytelDirectDealersId;
			data.AddParam("@RawAutobytelDirectDealersId", DataAccessParameterType.Numeric, rawAutobytelDirectDealersId.ToString());
			rawAutobytelDirectDealersId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawAutobytelDirectDealersId.ToString());
			data.AddParam("@DealerID", DataAccessParameterType.Text, this.DealerID);
			data.AddParam("@Name", DataAccessParameterType.Text, this.Name);
			data.AddParam("@Address", DataAccessParameterType.Text, this.Address);
			data.AddParam("@City", DataAccessParameterType.Text, this.City);
			data.AddParam("@State", DataAccessParameterType.Text, this.State);
			data.AddParam("@ZipCode", DataAccessParameterType.Text, this.ZipCode);
			data.AddParam("@Phone", DataAccessParameterType.Text, this.Phone);
			data.AddParam("@Latitude", DataAccessParameterType.Text, this.Latitude);
			data.AddParam("@Longitude", DataAccessParameterType.Text, this.Longitude);
			data.AddParam("@ContactFirstName", DataAccessParameterType.Text, this.ContactFirstName);
			data.AddParam("@ContactLastName", DataAccessParameterType.Text, this.ContactLastName);
			data.AddParam("@ContactPhone", DataAccessParameterType.Text, this.ContactPhone);
			data.AddParam("@FinancialRoutingPlatformName", DataAccessParameterType.Text, this.FinancialRoutingPlatformName);
			data.AddParam("@FinancialRoutingAccountID", DataAccessParameterType.Text, this.FinancialRoutingAccountID);
			data.AddParam("@MonOpen", DataAccessParameterType.Text, this.MonOpen);
			data.AddParam("@MonClose", DataAccessParameterType.Text, this.MonClose);
			data.AddParam("@TueOpen", DataAccessParameterType.Text, this.TueOpen);
			data.AddParam("@TueClose", DataAccessParameterType.Text, this.TueClose);
			data.AddParam("@WedOpen", DataAccessParameterType.Text, this.WedOpen);
			data.AddParam("@WedClose", DataAccessParameterType.Text, this.WedClose);
			data.AddParam("@ThrOpen", DataAccessParameterType.Text, this.ThrOpen);
			data.AddParam("@ThrClose", DataAccessParameterType.Text, this.ThrClose);
			data.AddParam("@FriOpen", DataAccessParameterType.Text, this.FriOpen);
			data.AddParam("@FriClose", DataAccessParameterType.Text, this.FriClose);
			data.AddParam("@SatOpen", DataAccessParameterType.Text, this.SatOpen);
			data.AddParam("@SatClose", DataAccessParameterType.Text, this.SatClose);
			data.AddParam("@SunOpen", DataAccessParameterType.Text, this.SunOpen);
			data.AddParam("@SunClose", DataAccessParameterType.Text, this.SunClose);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("RAW_AUTOBYTEL_DIRECT_DEALERS_InsertUpdate");
			if (!data.EOF)
			{
				this.RawAutobytelDirectDealersId = int.Parse(data["RawAutobytelDirectDealersId"]);
			}
			this.retrievedata();
		}
	}
}