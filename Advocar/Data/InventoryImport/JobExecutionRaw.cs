using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.InventoryImport
{
	public class JobExecutionRaw : Transaction
	{
		private int jobExecutionRawId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int lookupId_DataProvider = 0;

		private int lookupId_JobExecutionStatus = 0;

		private string startedOn = "";

		private string endedOn = "";

		private int processingTimeInSeconds = 0;

		private int noOfRecords = 0;

		private int noOfRecordsInFile = 0;

		private int noOfDealers = 0;

		private int noOfRowErrors = 0;

		private string logFileName = "";

		private string archiveFileName = "";

		private int noOfRawNotMapped = 0;

		public string ArchiveFileName
		{
			get
			{
				return this.archiveFileName;
			}
			set
			{
				this.archiveFileName = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string EndedOn
		{
			get
			{
				return this.endedOn;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.endedOn = "";
				}
				else
				{
					this.endedOn = DateTime.Parse(value).ToString();
				}
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public string LogFileName
		{
			get
			{
				return this.logFileName;
			}
			set
			{
				this.logFileName = value;
			}
		}

		public int LookupId_DataProvider
		{
			get
			{
				return this.lookupId_DataProvider;
			}
			set
			{
				this.lookupId_DataProvider = value;
			}
		}

		public int LookupId_JobExecutionStatus
		{
			get
			{
				return this.lookupId_JobExecutionStatus;
			}
			set
			{
				this.lookupId_JobExecutionStatus = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public int NoOfDealers
		{
			get
			{
				return this.noOfDealers;
			}
			set
			{
				this.noOfDealers = value;
			}
		}

		public int NoOfRawNotMapped
		{
			get
			{
				return this.noOfRawNotMapped;
			}
		}

		public int NoOfRecords
		{
			get
			{
				return this.noOfRecords;
			}
			set
			{
				this.noOfRecords = value;
			}
		}

		public int NoOfRecordsInFile
		{
			get
			{
				return this.noOfRecordsInFile;
			}
			set
			{
				this.noOfRecordsInFile = value;
			}
		}

		public int NoOfRowErrors
		{
			get
			{
				return this.noOfRowErrors;
			}
			set
			{
				this.noOfRowErrors = value;
			}
		}

		public int ProcessingTimeInSeconds
		{
			get
			{
				return this.processingTimeInSeconds;
			}
			set
			{
				this.processingTimeInSeconds = value;
			}
		}

		public string StartedOn
		{
			get
			{
				return this.startedOn;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.startedOn = "";
				}
				else
				{
					this.startedOn = DateTime.Parse(value).ToString();
                    //this.startedOn = DateTime.ParseExact("01-01-1900 00:00:00", "dd-MM-YYYY hh:mm:ss", System.Globalization.CultureInfo.InvariantCulture).ToString("dd-MM-yyyy hh:mm:ss");
				}
			}
		}

		public JobExecutionRaw()
		{
		}

		public JobExecutionRaw(string connection, string modifiedUserID, int jobExecutionRawId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.JobExecutionRawId = jobExecutionRawId;
			if (this.JobExecutionRawId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, this.JobExecutionRawId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("JOB_EXECUTION_RAW_Delete");
			this.wipeout();
		}

		public static int GetRecordId(string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@IsGetNextReadyForMapping", DataAccessParameterType.Bool, "1");
			data.ExecuteProcedure("JOB_EXECUTION_RAW_GetRecord");
			num = (!data.EOF ? int.Parse(data["JobExecutionRawId"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, this.JobExecutionRawId.ToString());
			data.ExecuteProcedure("JOB_EXECUTION_RAW_GetRecord");
			if (!data.EOF)
			{
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.LookupId_DataProvider = int.Parse(data["LookupId_DataProvider"]);
				this.LookupId_JobExecutionStatus = int.Parse(data["LookupId_JobExecutionStatus"]);
				this.StartedOn = data["StartedOn"];
				this.EndedOn = data["EndedOn"];
				this.ProcessingTimeInSeconds = int.Parse(data["ProcessingTimeInSeconds"]);
				this.NoOfRecords = int.Parse(data["NoOfRecords"]);
				this.NoOfRecordsInFile = int.Parse(data["NoOfRecordsInFile"]);
				this.NoOfDealers = int.Parse(data["NoOfDealers"]);
				this.NoOfRowErrors = int.Parse(data["NoOfRowErrors"]);
				this.LogFileName = data["LogFileName"];
				this.ArchiveFileName = data["ArchiveFileName"];
				this.noOfRawNotMapped = int.Parse(data["NoOfRawNotMapped"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
            
			DataAccess data = new DataAccess(this.connection);
			int jobExecutionRawId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, jobExecutionRawId.ToString());
			jobExecutionRawId = this.LookupId_DataProvider;
			data.AddParam("@LookupId_DataProvider", DataAccessParameterType.Numeric, jobExecutionRawId.ToString());
			jobExecutionRawId = this.LookupId_JobExecutionStatus;
			data.AddParam("@LookupId_JobExecutionStatus", DataAccessParameterType.Numeric, jobExecutionRawId.ToString());
			data.AddParam("@StartedOn", DataAccessParameterType.DateTime, this.StartedOn);
            data.AddParam("@EndedOn", DataAccessParameterType.DateTime, this.EndedOn);
			jobExecutionRawId = this.ProcessingTimeInSeconds;
			data.AddParam("@ProcessingTimeInSeconds", DataAccessParameterType.Numeric, jobExecutionRawId.ToString());
			jobExecutionRawId = this.NoOfRecords;
			data.AddParam("@NoOfRecords", DataAccessParameterType.Numeric, jobExecutionRawId.ToString());
			jobExecutionRawId = this.NoOfRecordsInFile;
			data.AddParam("@NoOfRecordsInFile", DataAccessParameterType.Numeric, jobExecutionRawId.ToString());
			jobExecutionRawId = this.NoOfDealers;
			data.AddParam("@NoOfDealers", DataAccessParameterType.Numeric, jobExecutionRawId.ToString());
			jobExecutionRawId = this.NoOfRowErrors;
			data.AddParam("@NoOfRowErrors", DataAccessParameterType.Numeric, jobExecutionRawId.ToString());
			data.AddParam("@LogFileName", DataAccessParameterType.Text, this.LogFileName);
			data.AddParam("@ArchiveFileName", DataAccessParameterType.Text, this.ArchiveFileName);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("JOB_EXECUTION_RAW_InsertUpdate");
			if (!data.EOF)
			{
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
			}
			this.retrievedata();
		}
	}
}