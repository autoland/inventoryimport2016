using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawMaxSystems : Transaction
	{
		private int rawMaxSystemId = 0;

		private int jobExecutionRawId = 0;

		private string dealerId = string.Empty;

		private string stockNumber = string.Empty;

		private string vin = string.Empty;

		private string make = string.Empty;

		private string model = string.Empty;

		private int year = 0;

		private int mileage = 0;

		private string bodyStyle = string.Empty;

		private string exteriorColor = string.Empty;

		private string interiorColor = string.Empty;

		private string transmission = string.Empty;

		private string engineCid = string.Empty;

		private string engineSize = string.Empty;

		private string engineType = string.Empty;

		private int price = 0;

		private int salesCost = 0;

		private string options = string.Empty;

		private string imageUrls = string.Empty;

		private string videoUrl = string.Empty;

		public string BodyStyle
		{
			get
			{
				return this.bodyStyle;
			}
			set
			{
				this.bodyStyle = value;
			}
		}

		public string DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public string EngineCid
		{
			get
			{
				return this.engineCid;
			}
			set
			{
				this.engineCid = value;
			}
		}

		public string EngineSize
		{
			get
			{
				return this.engineSize;
			}
			set
			{
				this.engineSize = value;
			}
		}

		public string EngineType
		{
			get
			{
				return this.engineType;
			}
			set
			{
				this.engineType = value;
			}
		}

		public string ExteriorColor
		{
			get
			{
				return this.exteriorColor;
			}
			set
			{
				this.exteriorColor = value;
			}
		}

		public string ImageUrls
		{
			get
			{
				return this.imageUrls;
			}
			set
			{
				this.imageUrls = value;
			}
		}

		public string InteriorColor
		{
			get
			{
				return this.interiorColor;
			}
			set
			{
				this.interiorColor = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public string Make
		{
			get
			{
				return this.make;
			}
			set
			{
				this.make = value;
			}
		}

		public int Mileage
		{
			get
			{
				return this.mileage;
			}
			set
			{
				this.mileage = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public string Options
		{
			get
			{
				return this.options;
			}
			set
			{
				this.options = value;
			}
		}

		public int Price
		{
			get
			{
				return this.price;
			}
			set
			{
				this.price = value;
			}
		}

		public int RawMaxSystemId
		{
			get
			{
				return this.rawMaxSystemId;
			}
			set
			{
				this.rawMaxSystemId = value;
			}
		}

		public int SalesCost
		{
			get
			{
				return this.salesCost;
			}
			set
			{
				this.salesCost = value;
			}
		}

		public string StockNumber
		{
			get
			{
				return this.stockNumber;
			}
			set
			{
				this.stockNumber = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string VideoUrl
		{
			get
			{
				return this.videoUrl;
			}
			set
			{
				this.videoUrl = value;
			}
		}

		public string Vin
		{
			get
			{
				return this.vin;
			}
			set
			{
				this.vin = value;
			}
		}

		public int Year
		{
			get
			{
				return this.year;
			}
			set
			{
				this.year = value;
			}
		}

		public RawMaxSystems()
		{
		}

		public RawMaxSystems(string connection, string modifiedUser, int rawMaxSystemId) : base(connection, modifiedUser)
		{
			this.rawMaxSystemId = rawMaxSystemId;
			this.databaseObjectName = "RAW_MAX_SYSTEM";
			if (this.rawMaxSystemId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			string rawMaxSystemId = (this.rawMaxSystemId > 0 ? this.rawMaxSystemId.ToString() : string.Empty);
			DataAccess dataAccess = new DataAccess(this.connection);
			dataAccess.AddParam("@RawMaxSystemId", DataAccessParameterType.Numeric, rawMaxSystemId, true);
			dataAccess.ExecuteProcedure("RAW_MAX_SYSTEM_Delete");
		}

		protected override void retrievedata()
		{
			DataAccess dataAccess = new DataAccess(this.connection);
			dataAccess.AddParam("@RawMaxSystemId", DataAccessParameterType.Numeric, (this.rawMaxSystemId > 0 ? this.rawMaxSystemId.ToString() : string.Empty), true);
			dataAccess.ExecuteProcedure("RAW_MAX_SYSTEM_GetRecord");
			if (!dataAccess.EOF)
			{
				this.RawMaxSystemId = (Validation.IsNumeric(dataAccess["RawMaxSystemId"]) ? Convert.ToInt32(dataAccess["RawMaxSystemId"]) : 0);
				this.JobExecutionRawId = (Validation.IsNumeric(dataAccess["JobExecutionRawId"]) ? Convert.ToInt32(dataAccess["JobExecutionRawId"]) : 0);
				this.DealerId = dataAccess["DealerId"];
				this.StockNumber = dataAccess["StockNumber"];
				this.Vin = dataAccess["Vin"];
				this.Make = dataAccess["Make"];
				this.Model = dataAccess["Model"];
				this.Year = (Validation.IsNumeric(dataAccess["Year"]) ? Convert.ToInt32(dataAccess["Year"]) : 0);
				this.Mileage = (Validation.IsNumeric(dataAccess["Mileage"]) ? Convert.ToInt32(dataAccess["Mileage"]) : 0);
				this.BodyStyle = dataAccess["BodyStyle"];
				this.ExteriorColor = dataAccess["ExteriorColor"];
				this.InteriorColor = dataAccess["InteriorColor"];
				this.Transmission = dataAccess["Transmission"];
				this.EngineCid = dataAccess["EngineCid"];
				this.EngineSize = dataAccess["EngineSize"];
				this.EngineType = dataAccess["EngineType"];
				this.Price = (Validation.IsNumeric(dataAccess["Price"]) ? Convert.ToInt32(dataAccess["Price"]) : 0);
				this.SalesCost = (Validation.IsNumeric(dataAccess["SalesCost"]) ? Convert.ToInt32(dataAccess["SalesCost"]) : 0);
				this.Options = dataAccess["Options"];
				this.ImageUrls = dataAccess["ImageUrls"];
				this.VideoUrl = dataAccess["VideoUrl"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			string rawMaxSystemId = (this.rawMaxSystemId > 0 ? this.rawMaxSystemId.ToString() : string.Empty);
			string jobExecutionRawId = (this.jobExecutionRawId > 0 ? this.jobExecutionRawId.ToString() : string.Empty);
			string year = (this.year > 0 ? this.year.ToString() : string.Empty);
			string mileage = (this.mileage > 0 ? this.mileage.ToString() : string.Empty);
			string price = (this.price > 0 ? this.price.ToString() : string.Empty);
			string salesCost = (this.salesCost > 0 ? this.salesCost.ToString() : string.Empty);
			DataAccess dataAccess = new DataAccess(this.connection);
			dataAccess.AddParam("@RawMaxSystemId", DataAccessParameterType.Numeric, rawMaxSystemId, true);
			dataAccess.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, jobExecutionRawId, true);
			dataAccess.AddParam("@DealerId", DataAccessParameterType.Text, this.dealerId, true);
			dataAccess.AddParam("@StockNumber", DataAccessParameterType.Text, this.stockNumber, true);
			dataAccess.AddParam("@Vin", DataAccessParameterType.Text, this.vin, true);
			dataAccess.AddParam("@Make", DataAccessParameterType.Text, this.make, true);
			dataAccess.AddParam("@Model", DataAccessParameterType.Text, this.model, true);
			dataAccess.AddParam("@Year", DataAccessParameterType.Numeric, year, true);
			dataAccess.AddParam("@Mileage", DataAccessParameterType.Numeric, mileage, true);
			dataAccess.AddParam("@BodyStyle", DataAccessParameterType.Text, this.bodyStyle, true);
			dataAccess.AddParam("@ExteriorColor", DataAccessParameterType.Text, this.exteriorColor, true);
			dataAccess.AddParam("@InteriorColor", DataAccessParameterType.Text, this.interiorColor, true);
			dataAccess.AddParam("@Transmission", DataAccessParameterType.Text, this.transmission, true);
			dataAccess.AddParam("@EngineCid", DataAccessParameterType.Text, this.engineCid, true);
			dataAccess.AddParam("@EngineSize", DataAccessParameterType.Text, this.engineSize, true);
			dataAccess.AddParam("@EngineType", DataAccessParameterType.Text, this.engineType, true);
			dataAccess.AddParam("@Price", DataAccessParameterType.Numeric, price, true);
			dataAccess.AddParam("@SalesCost", DataAccessParameterType.Numeric, salesCost, true);
			dataAccess.AddParam("@ImageUrls", DataAccessParameterType.Text, this.imageUrls, true);
			dataAccess.AddParam("@Options", DataAccessParameterType.Text, this.options, true);
			dataAccess.AddParam("@VideoUrl", DataAccessParameterType.Text, this.videoUrl, true);
			dataAccess.ExecuteProcedure("RAW_MAX_SYSTEM_InsertUpdate");
			if (!dataAccess.EOF)
			{
				this.RawMaxSystemId = (Validation.IsNumeric(dataAccess["RawMaxSystemId"]) ? Convert.ToInt32(dataAccess["RawMaxSystemId"]) : 0);
			}
			this.retrievedata();
		}
	}
}