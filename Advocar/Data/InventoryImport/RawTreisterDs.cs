using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawTreisterDs : Transaction
	{
		private int rawTreisterDsId = 0;

		private int jobExecutionRawId = 0;

		private string dealerId = "";

		private string stockNo = "";

		private int year = 0;

		private string make = "";

		private string model = "";

		private string trim = "";

		private string vin = "";

		private int mileage = 0;

		private float sellingPrice = 0f;

		private string exteriorColor = "";

		private string interiorColor = "";

		private string transmission = "";

		private string imageUrls = "";

		private string options = "";

		public string DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public string ExteriorColor
		{
			get
			{
				return this.exteriorColor;
			}
			set
			{
				this.exteriorColor = value;
			}
		}

		public string ImageUrls
		{
			get
			{
				return this.imageUrls;
			}
			set
			{
				this.imageUrls = value;
			}
		}

		public string InteriorColor
		{
			get
			{
				return this.interiorColor;
			}
			set
			{
				this.interiorColor = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public string Make
		{
			get
			{
				return this.make;
			}
			set
			{
				this.make = value;
			}
		}

		public int Mileage
		{
			get
			{
				return this.mileage;
			}
			set
			{
				this.mileage = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public string Options
		{
			get
			{
				return this.options;
			}
			set
			{
				this.options = value;
			}
		}

		public int RawTreisterDsId
		{
			get
			{
				return this.rawTreisterDsId;
			}
			set
			{
				this.rawTreisterDsId = value;
			}
		}

		public float SellingPrice
		{
			get
			{
				return this.sellingPrice;
			}
			set
			{
				this.sellingPrice = value;
			}
		}

		public string StockNo
		{
			get
			{
				return this.stockNo;
			}
			set
			{
				this.stockNo = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string Trim
		{
			get
			{
				return this.trim;
			}
			set
			{
				this.trim = value;
			}
		}

		public string Vin
		{
			get
			{
				return this.vin;
			}
			set
			{
				this.vin = value;
			}
		}

		public int Year
		{
			get
			{
				return this.year;
			}
			set
			{
				this.year = value;
			}
		}

		public RawTreisterDs()
		{
		}

		public RawTreisterDs(string connection, string modifiedUserID, int rawTreisterDsId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.RawTreisterDsId = rawTreisterDsId;
			this.databaseObjectName = "RAW_TREISTER_DS";
			if (this.RawTreisterDsId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawTreisterDsId", DataAccessParameterType.Numeric, this.RawTreisterDsId.ToString());
			data.ExecuteProcedure("RAW_TREISTER_DS_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawTreisterDsId", DataAccessParameterType.Numeric, this.RawTreisterDsId.ToString());
			data.ExecuteProcedure("RAW_TREISTER_DS_GetRecord");
			if (!data.EOF)
			{
				this.RawTreisterDsId = int.Parse(data["RawTreisterDsId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerId = data["DealerId"];
				this.StockNo = data["StockNo"];
				this.Year = int.Parse(data["Year"]);
				this.Make = data["Make"];
				this.Model = data["Model"];
				this.Trim = data["Trim"];
				this.Vin = data["Vin"];
				this.Mileage = int.Parse(data["Mileage"]);
				this.SellingPrice = float.Parse(data["SellingPrice"]);
				this.ExteriorColor = data["ExteriorColor"];
				this.InteriorColor = data["InteriorColor"];
				this.Transmission = data["Transmission"];
				this.ImageUrls = data["ImageUrls"];
				this.Options = data["Options"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawTreisterDsId = this.RawTreisterDsId;
			data.AddParam("@RawTreisterDsId", DataAccessParameterType.Numeric, rawTreisterDsId.ToString());
			rawTreisterDsId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawTreisterDsId.ToString());
			data.AddParam("@DealerId", DataAccessParameterType.Text, this.DealerId);
			data.AddParam("@StockNo", DataAccessParameterType.Text, this.StockNo);
			rawTreisterDsId = this.Year;
			data.AddParam("@Year", DataAccessParameterType.Numeric, rawTreisterDsId.ToString());
			data.AddParam("@Make", DataAccessParameterType.Text, this.Make);
			data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
			data.AddParam("@Trim", DataAccessParameterType.Text, this.Trim);
			data.AddParam("@Vin", DataAccessParameterType.Text, this.Vin);
			rawTreisterDsId = this.Mileage;
			data.AddParam("@Mileage", DataAccessParameterType.Numeric, rawTreisterDsId.ToString());
			data.AddParam("@SellingPrice", DataAccessParameterType.Numeric, this.SellingPrice.ToString());
			data.AddParam("@ExteriorColor", DataAccessParameterType.Text, this.ExteriorColor);
			data.AddParam("@InteriorColor", DataAccessParameterType.Text, this.InteriorColor);
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			data.AddParam("@ImageUrls", DataAccessParameterType.Text, this.ImageUrls);
			data.AddParam("@Options", DataAccessParameterType.Text, this.Options);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("RAW_TREISTER_DS_InsertUpdate");
			if (!data.EOF)
			{
				this.RawTreisterDsId = int.Parse(data["RawTreisterDsId"]);
			}
			this.retrievedata();
		}
	}
}