using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawVirtualDealer : Transaction
	{
		private int rawVirtualDealerId = 0;

		private int jobExecutionRawId = 0;

		private string dealerId = "";

		private string newUsed = "";

		private string vin = "";

		private string stockNum = "";

		private string vehicleYear = "";

		private string make = "";

		private string model = "";

		private string trim = "";

		private string engine = "";

		private string transmission = "";

		private string bodyStyle = "";

		private string exterior = "";

		private string interior = "";

		private int mileage = 0;

		private float price = 0f;

		private float specialPrice = 0f;

		private string certified = "";

		private string images = "";

		private string options = "";

		public string BodyStyle
		{
			get
			{
				return this.bodyStyle;
			}
			set
			{
				this.bodyStyle = value;
			}
		}

		public string Certified
		{
			get
			{
				return this.certified;
			}
			set
			{
				this.certified = value;
			}
		}

		public string DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public string Engine
		{
			get
			{
				return this.engine;
			}
			set
			{
				this.engine = value;
			}
		}

		public string Exterior
		{
			get
			{
				return this.exterior;
			}
			set
			{
				this.exterior = value;
			}
		}

		public string Images
		{
			get
			{
				return this.images;
			}
			set
			{
				this.images = value;
			}
		}

		public string Interior
		{
			get
			{
				return this.interior;
			}
			set
			{
				this.interior = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public string Make
		{
			get
			{
				return this.make;
			}
			set
			{
				this.make = value;
			}
		}

		public int Mileage
		{
			get
			{
				return this.mileage;
			}
			set
			{
				this.mileage = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public string NewUsed
		{
			get
			{
				return this.newUsed;
			}
			set
			{
				this.newUsed = value;
			}
		}

		public string Options
		{
			get
			{
				return this.options;
			}
			set
			{
				this.options = value;
			}
		}

		public float Price
		{
			get
			{
				return this.price;
			}
			set
			{
				this.price = value;
			}
		}

		public int RawVirtualDealerId
		{
			get
			{
				return this.rawVirtualDealerId;
			}
			set
			{
				this.rawVirtualDealerId = value;
			}
		}

		public float SpecialPrice
		{
			get
			{
				return this.specialPrice;
			}
			set
			{
				this.specialPrice = value;
			}
		}

		public string StockNum
		{
			get
			{
				return this.stockNum;
			}
			set
			{
				this.stockNum = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string Trim
		{
			get
			{
				return this.trim;
			}
			set
			{
				this.trim = value;
			}
		}

		public string VehicleYear
		{
			get
			{
				return this.vehicleYear;
			}
			set
			{
				this.vehicleYear = value;
			}
		}

		public string Vin
		{
			get
			{
				return this.vin;
			}
			set
			{
				this.vin = value;
			}
		}

		public RawVirtualDealer()
		{
		}

		public RawVirtualDealer(string connection, string modifiedUserID, int rawVirtualDealerId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "RAW_VIRTUAL_DEALER";
			this.RawVirtualDealerId = rawVirtualDealerId;
			if (this.RawVirtualDealerId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawVirtualDealerId", DataAccessParameterType.Numeric, this.RawVirtualDealerId.ToString());
			data.ExecuteProcedure("RAW_VIRTUAL_DEALER_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawVirtualDealerId", DataAccessParameterType.Numeric, this.RawVirtualDealerId.ToString());
			data.ExecuteProcedure("RAW_VIRTUAL_DEALER_GetRecord");
			if (!data.EOF)
			{
				this.RawVirtualDealerId = int.Parse(data["RawVirtualDealerId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerId = data["DealerId"];
				this.NewUsed = data["NewUsed"];
				this.Vin = data["Vin"];
				this.StockNum = data["StockNum"];
				this.VehicleYear = data["VehicleYear"];
				this.Make = data["Make"];
				this.Model = data["Model"];
				this.Trim = data["Trim"];
				this.Engine = data["Engine"];
				this.Transmission = data["Transmission"];
				this.BodyStyle = data["BodyStyle"];
				this.Exterior = data["Exterior"];
				this.Interior = data["Interior"];
				this.Mileage = int.Parse(data["Mileage"]);
				this.Price = float.Parse(data["Price"]);
				this.SpecialPrice = float.Parse(data["SpecialPrice"]);
				this.Certified = data["Certified"];
				this.Images = data["Images"];
				this.Options = data["Options"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawVirtualDealerId = this.RawVirtualDealerId;
			data.AddParam("@RawVirtualDealerId", DataAccessParameterType.Numeric, rawVirtualDealerId.ToString());
			rawVirtualDealerId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawVirtualDealerId.ToString());
			data.AddParam("@DealerId", DataAccessParameterType.Text, this.DealerId);
			data.AddParam("@NewUsed", DataAccessParameterType.Text, this.NewUsed);
			data.AddParam("@Vin", DataAccessParameterType.Text, this.Vin);
			data.AddParam("@StockNum", DataAccessParameterType.Text, this.StockNum);
			data.AddParam("@VehicleYear", DataAccessParameterType.Text, this.VehicleYear);
			data.AddParam("@Make", DataAccessParameterType.Text, this.Make);
			data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
			data.AddParam("@Trim", DataAccessParameterType.Text, this.Trim);
			data.AddParam("@Engine", DataAccessParameterType.Text, this.Engine);
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			data.AddParam("@BodyStyle", DataAccessParameterType.Text, this.BodyStyle);
			data.AddParam("@Exterior", DataAccessParameterType.Text, this.Exterior);
			data.AddParam("@Interior", DataAccessParameterType.Text, this.Interior);
			rawVirtualDealerId = this.Mileage;
			data.AddParam("@Mileage", DataAccessParameterType.Numeric, rawVirtualDealerId.ToString());
			float price = this.Price;
			data.AddParam("@Price", DataAccessParameterType.Numeric, price.ToString());
			price = this.SpecialPrice;
			data.AddParam("@SpecialPrice", DataAccessParameterType.Numeric, price.ToString());
			data.AddParam("@Certified", DataAccessParameterType.Text, this.Certified);
			data.AddParam("@Images", DataAccessParameterType.Text, this.Images);
			data.AddParam("@Options", DataAccessParameterType.Text, this.Options);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("RAW_VIRTUAL_DEALER_InsertUpdate");
			if (!data.EOF)
			{
				this.RawVirtualDealerId = int.Parse(data["RawVirtualDealerId"]);
			}
			this.retrievedata();
		}
	}
}