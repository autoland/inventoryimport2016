using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawAutobytelDirectNewinfo : Transaction
	{
		private int rawAutobytelDirectNewinfoId = 0;

		private int jobExecutionRawId = 0;

		private string year = "";

		private string make = "";

		private string model = "";

		private string series = "";

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public string Make
		{
			get
			{
				return this.make;
			}
			set
			{
				this.make = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public int RawAutobytelDirectNewinfoId
		{
			get
			{
				return this.rawAutobytelDirectNewinfoId;
			}
			set
			{
				this.rawAutobytelDirectNewinfoId = value;
			}
		}

		public string Series
		{
			get
			{
				return this.series;
			}
			set
			{
				this.series = value;
			}
		}

		public string Year
		{
			get
			{
				return this.year;
			}
			set
			{
				this.year = value;
			}
		}

		public RawAutobytelDirectNewinfo()
		{
		}

		public RawAutobytelDirectNewinfo(string connection, string modifiedUserID, int rawAutobytelDirectNewinfoId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "RAW_AUTOBYTEL_DIRECT_NEWINFO";
			this.RawAutobytelDirectNewinfoId = rawAutobytelDirectNewinfoId;
			if (this.RawAutobytelDirectNewinfoId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawAutobytelDirectNewinfoId", DataAccessParameterType.Numeric, this.RawAutobytelDirectNewinfoId.ToString());
			data.ExecuteProcedure("RAW_AUTOBYTEL_DIRECT_NEWINFO_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawAutobytelDirectNewinfoId", DataAccessParameterType.Numeric, this.RawAutobytelDirectNewinfoId.ToString());
			data.ExecuteProcedure("RAW_AUTOBYTEL_DIRECT_NEWINFO_GetRecord");
			if (!data.EOF)
			{
				this.RawAutobytelDirectNewinfoId = int.Parse(data["RawAutobytelDirectNewinfoId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.Year = data["Year"];
				this.Make = data["Make"];
				this.Model = data["Model"];
				this.Series = data["Series"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawAutobytelDirectNewinfoId = this.RawAutobytelDirectNewinfoId;
			data.AddParam("@RawAutobytelDirectNewinfoId", DataAccessParameterType.Numeric, rawAutobytelDirectNewinfoId.ToString());
			rawAutobytelDirectNewinfoId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawAutobytelDirectNewinfoId.ToString());
			data.AddParam("@Year", DataAccessParameterType.Text, this.Year);
			data.AddParam("@Make", DataAccessParameterType.Text, this.Make);
			data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
			data.AddParam("@Series", DataAccessParameterType.Text, this.Series);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("RAW_AUTOBYTEL_DIRECT_NEWINFO_InsertUpdate");
			if (!data.EOF)
			{
				this.RawAutobytelDirectNewinfoId = int.Parse(data["RawAutobytelDirectNewinfoId"]);
			}
			this.retrievedata();
		}
	}
}