using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class Archive_200808_RawDealerSpecialtiesCarpart : Transaction
	{
		private int rawDealerSpecialtiesCarpartId = 0;

		private int jobExecutionRawId = 0;

		private string carpart = "";

		private string carpartType = "";

		private string description = "";

		public string Carpart
		{
			get
			{
				return this.carpart;
			}
			set
			{
				this.carpart = value;
			}
		}

		public string CarpartType
		{
			get
			{
				return this.carpartType;
			}
			set
			{
				this.carpartType = value;
			}
		}

		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public int RawDealerSpecialtiesCarpartId
		{
			get
			{
				return this.rawDealerSpecialtiesCarpartId;
			}
			set
			{
				this.rawDealerSpecialtiesCarpartId = value;
			}
		}

		public Archive_200808_RawDealerSpecialtiesCarpart()
		{
		}

		public Archive_200808_RawDealerSpecialtiesCarpart(string connection, string modifiedUserID, int rawDealerSpecialtiesCarpartId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.RawDealerSpecialtiesCarpartId = rawDealerSpecialtiesCarpartId;
			this.databaseObjectName = "RAW_DEALER_SPECIALTIES_CARPART";
			if (this.RawDealerSpecialtiesCarpartId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawDealerSpecialtiesCarpartId", DataAccessParameterType.Numeric, this.RawDealerSpecialtiesCarpartId.ToString());
			data.ExecuteProcedure("RAW_DEALER_SPECIALTIES_CARPART_Delete");
			this.wipeout();
		}

		public static string GetCarpartDescription(int jobExecutionRawId, string code, string connection)
		{
			DataAccess data = new DataAccess(connection);
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, jobExecutionRawId.ToString());
			data.AddParam("@Carpart", DataAccessParameterType.Text, code);
			data.ExecuteProcedure("RAW_DEALER_SPECIALTIES_CARPART_GetRecord");
			return (!data.EOF ? data["Description"] : "");
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawDealerSpecialtiesCarpartId", DataAccessParameterType.Numeric, this.RawDealerSpecialtiesCarpartId.ToString());
			data.ExecuteProcedure("RAW_DEALER_SPECIALTIES_CARPART_GetRecord");
			if (!data.EOF)
			{
				this.RawDealerSpecialtiesCarpartId = int.Parse(data["RawDealerSpecialtiesCarpartId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.Carpart = data["Carpart"];
				this.CarpartType = data["CarpartType"];
				this.Description = data["Description"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawDealerSpecialtiesCarpartId = this.RawDealerSpecialtiesCarpartId;
			data.AddParam("@RawDealerSpecialtiesCarpartId", DataAccessParameterType.Numeric, rawDealerSpecialtiesCarpartId.ToString());
			rawDealerSpecialtiesCarpartId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawDealerSpecialtiesCarpartId.ToString());
			data.AddParam("@Carpart", DataAccessParameterType.Text, this.Carpart);
			data.AddParam("@CarpartType", DataAccessParameterType.Text, this.CarpartType);
			data.AddParam("@Description", DataAccessParameterType.Text, this.Description);
			data.ExecuteProcedure("RAW_DEALER_SPECIALTIES_CARPART_InsertUpdate");
			if (!data.EOF)
			{
				this.RawDealerSpecialtiesCarpartId = int.Parse(data["RawDealerSpecialtiesCarpartId"]);
			}
			this.retrievedata();
		}
	}
}