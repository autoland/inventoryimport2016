using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawAutoBase : Transaction
	{
		private int rawAutoBaseId = 0;

		private int jobExecutionRawId = 0;

		private string dealerID = "";

		private string newUsed = "";

		private string stockID = "";

		private string vehicleDescription = "";

		private int year = 0;

		private string make = "";

		private string model = "";

		private string trim = "";

		private string vIN = "";

		private string style = "";

		private string engine = "";

		private string driveTrain = "";

		private string transmission = "";

		private string extColor = "";

		private string intColor = "";

		private string options = "";

		private int mileage = 0;

		private float acquisitionPrice = 0f;

		private float invoicePrice = 0f;

		private float sellingPrice = 0f;

		private float retailPrice = 0f;

		private string marketIndex = "";

		private float mSRPPrice = 0f;

		private string imageUrls = "";

		public float AcquisitionPrice
		{
			get
			{
				return this.acquisitionPrice;
			}
			set
			{
				this.acquisitionPrice = value;
			}
		}

		public string DealerID
		{
			get
			{
				return this.dealerID;
			}
			set
			{
				this.dealerID = value;
			}
		}

		public string DriveTrain
		{
			get
			{
				return this.driveTrain;
			}
			set
			{
				this.driveTrain = value;
			}
		}

		public string Engine
		{
			get
			{
				return this.engine;
			}
			set
			{
				this.engine = value;
			}
		}

		public string ExtColor
		{
			get
			{
				return this.extColor;
			}
			set
			{
				this.extColor = value;
			}
		}

		public string ImageUrls
		{
			get
			{
				return this.imageUrls;
			}
			set
			{
				this.imageUrls = value;
			}
		}

		public string IntColor
		{
			get
			{
				return this.intColor;
			}
			set
			{
				this.intColor = value;
			}
		}

		public float InvoicePrice
		{
			get
			{
				return this.invoicePrice;
			}
			set
			{
				this.invoicePrice = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public string Make
		{
			get
			{
				return this.make;
			}
			set
			{
				this.make = value;
			}
		}

		public string MarketIndex
		{
			get
			{
				return this.marketIndex;
			}
			set
			{
				this.marketIndex = value;
			}
		}

		public int Mileage
		{
			get
			{
				return this.mileage;
			}
			set
			{
				this.mileage = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public float MSRPPrice
		{
			get
			{
				return this.mSRPPrice;
			}
			set
			{
				this.mSRPPrice = value;
			}
		}

		public string NewUsed
		{
			get
			{
				return this.newUsed;
			}
			set
			{
				this.newUsed = value;
			}
		}

		public string Options
		{
			get
			{
				return this.options;
			}
			set
			{
				this.options = value;
			}
		}

		public int RawAutoBaseId
		{
			get
			{
				return this.rawAutoBaseId;
			}
			set
			{
				this.rawAutoBaseId = value;
			}
		}

		public float RetailPrice
		{
			get
			{
				return this.retailPrice;
			}
			set
			{
				this.retailPrice = value;
			}
		}

		public float SellingPrice
		{
			get
			{
				return this.sellingPrice;
			}
			set
			{
				this.sellingPrice = value;
			}
		}

		public string StockID
		{
			get
			{
				return this.stockID;
			}
			set
			{
				this.stockID = value;
			}
		}

		public string Style
		{
			get
			{
				return this.style;
			}
			set
			{
				this.style = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string Trim
		{
			get
			{
				return this.trim;
			}
			set
			{
				this.trim = value;
			}
		}

		public string VehicleDescription
		{
			get
			{
				return this.vehicleDescription;
			}
			set
			{
				this.vehicleDescription = value;
			}
		}

		public string VIN
		{
			get
			{
				return this.vIN;
			}
			set
			{
				this.vIN = value;
			}
		}

		public int Year
		{
			get
			{
				return this.year;
			}
			set
			{
				this.year = value;
			}
		}

		public RawAutoBase()
		{
		}

		public RawAutoBase(string connection, string modifiedUserID, int rawAutoBaseId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.RawAutoBaseId = rawAutoBaseId;
			this.databaseObjectName = "RAW_AUTO_BASE";
			if (this.RawAutoBaseId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawAutoBaseId", DataAccessParameterType.Numeric, this.RawAutoBaseId.ToString());
			data.ExecuteProcedure("RAW_AUTO_BASE_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawAutoBaseId", DataAccessParameterType.Numeric, this.RawAutoBaseId.ToString());
			data.ExecuteProcedure("RAW_AUTO_BASE_GetRecord");
			if (!data.EOF)
			{
				this.RawAutoBaseId = int.Parse(data["RawAutoBaseId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerID = data["DealerID"];
				this.NewUsed = data["NewUsed"];
				this.StockID = data["StockID"];
				this.VehicleDescription = data["VehicleDescription"];
				this.Year = int.Parse(data["Year"]);
				this.Make = data["Make"];
				this.Model = data["Model"];
				this.Trim = data["Trim"];
				this.VIN = data["VIN"];
				this.Style = data["Style"];
				this.Engine = data["Engine"];
				this.DriveTrain = data["DriveTrain"];
				this.Transmission = data["Transmission"];
				this.ExtColor = data["ExtColor"];
				this.IntColor = data["IntColor"];
				this.Options = data["Options"];
				this.Mileage = int.Parse(data["Mileage"]);
				this.AcquisitionPrice = float.Parse(data["AcquisitionPrice"]);
				this.InvoicePrice = float.Parse(data["InvoicePrice"]);
				this.SellingPrice = float.Parse(data["SellingPrice"]);
				this.RetailPrice = float.Parse(data["RetailPrice"]);
				this.MarketIndex = data["MarketIndex"];
				this.MSRPPrice = float.Parse(data["MSRPPrice"]);
				this.ImageUrls = data["ImageUrls"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawAutoBaseId = this.RawAutoBaseId;
			data.AddParam("@RawAutoBaseId", DataAccessParameterType.Numeric, rawAutoBaseId.ToString());
			rawAutoBaseId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawAutoBaseId.ToString());
			data.AddParam("@DealerID", DataAccessParameterType.Text, this.DealerID);
			data.AddParam("@NewUsed", DataAccessParameterType.Text, this.NewUsed);
			data.AddParam("@StockID", DataAccessParameterType.Text, this.StockID);
			data.AddParam("@VehicleDescription", DataAccessParameterType.Text, this.VehicleDescription);
			rawAutoBaseId = this.Year;
			data.AddParam("@Year", DataAccessParameterType.Numeric, rawAutoBaseId.ToString());
			data.AddParam("@Make", DataAccessParameterType.Text, this.Make);
			data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
			data.AddParam("@Trim", DataAccessParameterType.Text, this.Trim);
			data.AddParam("@VIN", DataAccessParameterType.Text, this.VIN);
			data.AddParam("@Style", DataAccessParameterType.Text, this.Style);
			data.AddParam("@Engine", DataAccessParameterType.Text, this.Engine);
			data.AddParam("@DriveTrain", DataAccessParameterType.Text, this.DriveTrain);
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			data.AddParam("@ExtColor", DataAccessParameterType.Text, this.ExtColor);
			data.AddParam("@IntColor", DataAccessParameterType.Text, this.IntColor);
			data.AddParam("@Options", DataAccessParameterType.Text, this.Options);
			rawAutoBaseId = this.Mileage;
			data.AddParam("@Mileage", DataAccessParameterType.Numeric, rawAutoBaseId.ToString());
			float acquisitionPrice = this.AcquisitionPrice;
			data.AddParam("@AcquisitionPrice", DataAccessParameterType.Numeric, acquisitionPrice.ToString());
			acquisitionPrice = this.InvoicePrice;
			data.AddParam("@InvoicePrice", DataAccessParameterType.Numeric, acquisitionPrice.ToString());
			acquisitionPrice = this.SellingPrice;
			data.AddParam("@SellingPrice", DataAccessParameterType.Numeric, acquisitionPrice.ToString());
			acquisitionPrice = this.RetailPrice;
			data.AddParam("@RetailPrice", DataAccessParameterType.Numeric, acquisitionPrice.ToString());
			data.AddParam("@MarketIndex", DataAccessParameterType.Text, this.MarketIndex);
			acquisitionPrice = this.MSRPPrice;
			data.AddParam("@MSRPPrice", DataAccessParameterType.Numeric, acquisitionPrice.ToString());
			data.AddParam("@ImageUrls", DataAccessParameterType.Text, this.ImageUrls);
			data.ExecuteProcedure("RAW_AUTO_BASE_InsertUpdate");
			if (!data.EOF)
			{
				this.RawAutoBaseId = int.Parse(data["RawAutoBaseId"]);
			}
			this.retrievedata();
		}
	}
}