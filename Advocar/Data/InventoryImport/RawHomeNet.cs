using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawHomeNet : Transaction
	{
		private int rawHomeNetId = 0;

		private int jobExecutionRawId = 0;

		private string dealerId = "";

		private string newUsed = "";

		private string stock = "";

		private string vin = "";

		private string vehicleYear = "";

		private string make = "";

		private string model = "";

		private string modelNumber = "";

		private string body = "";

		private string trim = "";

		private string doors = "";

		private string exteriorColor = "";

		private string interiorColor = "";

		private string engineCylinders = "";

		private string engineDisplacement = "";

		private string transmission = "";

		private string miles = "";

		private string sellingPrice = "";

		private string mSRP = "";

		private string bookValue = "";

		private string invoice = "";

		private string certified = "";

		private string dateInStock = "";

		private string description = "";

		private string options = "";

		private string styleDescription = "";

		private string extColorGeneric = "";

		private string extColorCode = "";

		private string intColorGeneric = "";

		private string intColorCode = "";

		private string intUpholstery = "";

		private string engineBlockType = "";

		private string engineAspirationType = "";

		private string engineDescription = "";

		private string transmissionSpeed = "";

		private string transmissionDescription = "";

		private string drivetrain = "";

		private string fuelType = "";

		private string cityMPG = "";

		private string highwayMPG = "";

		private string ePAClassification = "";

		private string wheelbaseCode = "";

		private string internetPrice = "";

		private string miscPrice1 = "";

		private string miscPrice2 = "";

		private string miscPrice3 = "";

		private string factoryCodes = "";

		private string marketClass = "";

		private string passengerCapacity = "";

		private string extColorHexCode = "";

		private string intColorHexCode = "";

		private string engineDisplacementCubicInches = "";

		private string photoURLs = "";

		public string Body
		{
			get
			{
				return this.body;
			}
			set
			{
				this.body = value;
			}
		}

		public string BookValue
		{
			get
			{
				return this.bookValue;
			}
			set
			{
				this.bookValue = value;
			}
		}

		public string Certified
		{
			get
			{
				return this.certified;
			}
			set
			{
				this.certified = value;
			}
		}

		public string CityMPG
		{
			get
			{
				return this.cityMPG;
			}
			set
			{
				this.cityMPG = value;
			}
		}

		public string DateInStock
		{
			get
			{
				return this.dateInStock;
			}
			set
			{
				this.dateInStock = value;
			}
		}

		public string DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

		public string Doors
		{
			get
			{
				return this.doors;
			}
			set
			{
				this.doors = value;
			}
		}

		public string Drivetrain
		{
			get
			{
				return this.drivetrain;
			}
			set
			{
				this.drivetrain = value;
			}
		}

		public string EngineAspirationType
		{
			get
			{
				return this.engineAspirationType;
			}
			set
			{
				this.engineAspirationType = value;
			}
		}

		public string EngineBlockType
		{
			get
			{
				return this.engineBlockType;
			}
			set
			{
				this.engineBlockType = value;
			}
		}

		public string EngineCylinders
		{
			get
			{
				return this.engineCylinders;
			}
			set
			{
				this.engineCylinders = value;
			}
		}

		public string EngineDescription
		{
			get
			{
				return this.engineDescription;
			}
			set
			{
				this.engineDescription = value;
			}
		}

		public string EngineDisplacement
		{
			get
			{
				return this.engineDisplacement;
			}
			set
			{
				this.engineDisplacement = value;
			}
		}

		public string EngineDisplacementCubicInches
		{
			get
			{
				return this.engineDisplacementCubicInches;
			}
			set
			{
				this.engineDisplacementCubicInches = value;
			}
		}

		public string EPAClassification
		{
			get
			{
				return this.ePAClassification;
			}
			set
			{
				this.ePAClassification = value;
			}
		}

		public string ExtColorCode
		{
			get
			{
				return this.extColorCode;
			}
			set
			{
				this.extColorCode = value;
			}
		}

		public string ExtColorGeneric
		{
			get
			{
				return this.extColorGeneric;
			}
			set
			{
				this.extColorGeneric = value;
			}
		}

		public string ExtColorHexCode
		{
			get
			{
				return this.extColorHexCode;
			}
			set
			{
				this.extColorHexCode = value;
			}
		}

		public string ExteriorColor
		{
			get
			{
				return this.exteriorColor;
			}
			set
			{
				this.exteriorColor = value;
			}
		}

		public string FactoryCodes
		{
			get
			{
				return this.factoryCodes;
			}
			set
			{
				this.factoryCodes = value;
			}
		}

		public string FuelType
		{
			get
			{
				return this.fuelType;
			}
			set
			{
				this.fuelType = value;
			}
		}

		public string HighwayMPG
		{
			get
			{
				return this.highwayMPG;
			}
			set
			{
				this.highwayMPG = value;
			}
		}

		public string IntColorCode
		{
			get
			{
				return this.intColorCode;
			}
			set
			{
				this.intColorCode = value;
			}
		}

		public string IntColorGeneric
		{
			get
			{
				return this.intColorGeneric;
			}
			set
			{
				this.intColorGeneric = value;
			}
		}

		public string IntColorHexCode
		{
			get
			{
				return this.intColorHexCode;
			}
			set
			{
				this.intColorHexCode = value;
			}
		}

		public string InteriorColor
		{
			get
			{
				return this.interiorColor;
			}
			set
			{
				this.interiorColor = value;
			}
		}

		public string InternetPrice
		{
			get
			{
				return this.internetPrice;
			}
			set
			{
				this.internetPrice = value;
			}
		}

		public string IntUpholstery
		{
			get
			{
				return this.intUpholstery;
			}
			set
			{
				this.intUpholstery = value;
			}
		}

		public string Invoice
		{
			get
			{
				return this.invoice;
			}
			set
			{
				this.invoice = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public string Make
		{
			get
			{
				return this.make;
			}
			set
			{
				this.make = value;
			}
		}

		public string MarketClass
		{
			get
			{
				return this.marketClass;
			}
			set
			{
				this.marketClass = value;
			}
		}

		public string Miles
		{
			get
			{
				return this.miles;
			}
			set
			{
				this.miles = value;
			}
		}

		public string MiscPrice1
		{
			get
			{
				return this.miscPrice1;
			}
			set
			{
				this.miscPrice1 = value;
			}
		}

		public string MiscPrice2
		{
			get
			{
				return this.miscPrice2;
			}
			set
			{
				this.miscPrice2 = value;
			}
		}

		public string MiscPrice3
		{
			get
			{
				return this.miscPrice3;
			}
			set
			{
				this.miscPrice3 = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public string ModelNumber
		{
			get
			{
				return this.modelNumber;
			}
			set
			{
				this.modelNumber = value;
			}
		}

		public string MSRP
		{
			get
			{
				return this.mSRP;
			}
			set
			{
				this.mSRP = value;
			}
		}

		public string NewUsed
		{
			get
			{
				return this.newUsed;
			}
			set
			{
				this.newUsed = value;
			}
		}

		public string Options
		{
			get
			{
				return this.options;
			}
			set
			{
				this.options = value;
			}
		}

		public string PassengerCapacity
		{
			get
			{
				return this.passengerCapacity;
			}
			set
			{
				this.passengerCapacity = value;
			}
		}

		public string PhotoURLs
		{
			get
			{
				return this.photoURLs;
			}
			set
			{
				this.photoURLs = value;
			}
		}

		public int RawHomeNetId
		{
			get
			{
				return this.rawHomeNetId;
			}
			set
			{
				this.rawHomeNetId = value;
			}
		}

		public string SellingPrice
		{
			get
			{
				return this.sellingPrice;
			}
			set
			{
				this.sellingPrice = value;
			}
		}

		public string Stock
		{
			get
			{
				return this.stock;
			}
			set
			{
				this.stock = value;
			}
		}

		public string StyleDescription
		{
			get
			{
				return this.styleDescription;
			}
			set
			{
				this.styleDescription = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string TransmissionDescription
		{
			get
			{
				return this.transmissionDescription;
			}
			set
			{
				this.transmissionDescription = value;
			}
		}

		public string TransmissionSpeed
		{
			get
			{
				return this.transmissionSpeed;
			}
			set
			{
				this.transmissionSpeed = value;
			}
		}

		public string Trim
		{
			get
			{
				return this.trim;
			}
			set
			{
				this.trim = value;
			}
		}

		public string VehicleYear
		{
			get
			{
				return this.vehicleYear;
			}
			set
			{
				this.vehicleYear = value;
			}
		}

		public string Vin
		{
			get
			{
				return this.vin;
			}
			set
			{
				this.vin = value;
			}
		}

		public string WheelbaseCode
		{
			get
			{
				return this.wheelbaseCode;
			}
			set
			{
				this.wheelbaseCode = value;
			}
		}

		public RawHomeNet()
		{
		}

		public RawHomeNet(string connection, string modifiedUserID, int rawHomeNetId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "RAW_HOME_NET";
			this.RawHomeNetId = rawHomeNetId;
			if (this.RawHomeNetId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawHomeNetId", DataAccessParameterType.Numeric, this.RawHomeNetId.ToString());
			data.ExecuteProcedure("RAW_HOME_NET_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawHomeNetId", DataAccessParameterType.Numeric, this.RawHomeNetId.ToString());
			data.ExecuteProcedure("RAW_HOME_NET_GetRecord");
			if (!data.EOF)
			{
				this.RawHomeNetId = int.Parse(data["RawHomeNetId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerId = data["DealerId"];
				this.NewUsed = data["NewUsed"];
				this.Stock = data["Stock"];
				this.Vin = data["Vin"];
				this.VehicleYear = data["VehicleYear"];
				this.Make = data["Make"];
				this.Model = data["Model"];
				this.ModelNumber = data["ModelNumber"];
				this.Body = data["Body"];
				this.Trim = data["Trim"];
				this.Doors = data["Doors"];
				this.ExteriorColor = data["ExteriorColor"];
				this.InteriorColor = data["InteriorColor"];
				this.EngineCylinders = data["EngineCylinders"];
				this.EngineDisplacement = data["EngineDisplacement"];
				this.Transmission = data["Transmission"];
				this.Miles = data["Miles"];
				this.SellingPrice = data["SellingPrice"];
				this.MSRP = data["MSRP"];
				this.BookValue = data["BookValue"];
				this.Invoice = data["Invoice"];
				this.Certified = data["Certified"];
				this.DateInStock = data["DateInStock"];
				this.Description = data["Description"];
				this.Options = data["Options"];
				this.StyleDescription = data["StyleDescription"];
				this.ExtColorGeneric = data["ExtColorGeneric"];
				this.ExtColorCode = data["ExtColorCode"];
				this.IntColorGeneric = data["IntColorGeneric"];
				this.IntColorCode = data["IntColorCode"];
				this.IntUpholstery = data["IntUpholstery"];
				this.EngineBlockType = data["EngineBlockType"];
				this.EngineAspirationType = data["EngineAspirationType"];
				this.EngineDescription = data["EngineDescription"];
				this.TransmissionSpeed = data["TransmissionSpeed"];
				this.TransmissionDescription = data["TransmissionDescription"];
				this.Drivetrain = data["Drivetrain"];
				this.FuelType = data["FuelType"];
				this.CityMPG = data["CityMPG"];
				this.HighwayMPG = data["HighwayMPG"];
				this.EPAClassification = data["EPAClassification"];
				this.WheelbaseCode = data["WheelbaseCode"];
				this.InternetPrice = data["InternetPrice"];
				this.MiscPrice1 = data["MiscPrice1"];
				this.MiscPrice2 = data["MiscPrice2"];
				this.MiscPrice3 = data["MiscPrice3"];
				this.FactoryCodes = data["FactoryCodes"];
				this.MarketClass = data["MarketClass"];
				this.PassengerCapacity = data["PassengerCapacity"];
				this.ExtColorHexCode = data["ExtColorHexCode"];
				this.IntColorHexCode = data["IntColorHexCode"];
				this.EngineDisplacementCubicInches = data["EngineDisplacementCubicInches"];
				this.PhotoURLs = data["PhotoURLs"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawHomeNetId = this.RawHomeNetId;
			data.AddParam("@RawHomeNetId", DataAccessParameterType.Numeric, rawHomeNetId.ToString());
			rawHomeNetId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawHomeNetId.ToString());
			data.AddParam("@DealerId", DataAccessParameterType.Text, this.DealerId);
			data.AddParam("@NewUsed", DataAccessParameterType.Text, this.NewUsed);
			data.AddParam("@Stock", DataAccessParameterType.Text, this.Stock);
			data.AddParam("@Vin", DataAccessParameterType.Text, this.Vin);
			data.AddParam("@VehicleYear", DataAccessParameterType.Text, this.VehicleYear);
			data.AddParam("@Make", DataAccessParameterType.Text, this.Make);
			data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
			data.AddParam("@ModelNumber", DataAccessParameterType.Text, this.ModelNumber);
			data.AddParam("@Body", DataAccessParameterType.Text, this.Body);
			data.AddParam("@Trim", DataAccessParameterType.Text, this.Trim);
			data.AddParam("@Doors", DataAccessParameterType.Text, this.Doors);
			data.AddParam("@ExteriorColor", DataAccessParameterType.Text, this.ExteriorColor);
			data.AddParam("@InteriorColor", DataAccessParameterType.Text, this.InteriorColor);
			data.AddParam("@EngineCylinders", DataAccessParameterType.Text, this.EngineCylinders);
			data.AddParam("@EngineDisplacement", DataAccessParameterType.Text, this.EngineDisplacement);
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			data.AddParam("@Miles", DataAccessParameterType.Text, this.Miles);
			data.AddParam("@SellingPrice", DataAccessParameterType.Text, this.SellingPrice);
			data.AddParam("@MSRP", DataAccessParameterType.Text, this.MSRP);
			data.AddParam("@BookValue", DataAccessParameterType.Text, this.BookValue);
			data.AddParam("@Invoice", DataAccessParameterType.Text, this.Invoice);
			data.AddParam("@Certified", DataAccessParameterType.Text, this.Certified);
			data.AddParam("@DateInStock", DataAccessParameterType.Text, this.DateInStock);
			data.AddParam("@Description", DataAccessParameterType.Text, this.Description);
			data.AddParam("@Options", DataAccessParameterType.Text, this.Options);
			data.AddParam("@StyleDescription", DataAccessParameterType.Text, this.StyleDescription);
			data.AddParam("@ExtColorGeneric", DataAccessParameterType.Text, this.ExtColorGeneric);
			data.AddParam("@ExtColorCode", DataAccessParameterType.Text, this.ExtColorCode);
			data.AddParam("@IntColorGeneric", DataAccessParameterType.Text, this.IntColorGeneric);
			data.AddParam("@IntColorCode", DataAccessParameterType.Text, this.IntColorCode);
			data.AddParam("@IntUpholstery", DataAccessParameterType.Text, this.IntUpholstery);
			data.AddParam("@EngineBlockType", DataAccessParameterType.Text, this.EngineBlockType);
			data.AddParam("@EngineAspirationType", DataAccessParameterType.Text, this.EngineAspirationType);
			data.AddParam("@EngineDescription", DataAccessParameterType.Text, this.EngineDescription);
			data.AddParam("@TransmissionSpeed", DataAccessParameterType.Text, this.TransmissionSpeed);
			data.AddParam("@TransmissionDescription", DataAccessParameterType.Text, this.TransmissionDescription);
			data.AddParam("@Drivetrain", DataAccessParameterType.Text, this.Drivetrain);
			data.AddParam("@FuelType", DataAccessParameterType.Text, this.FuelType);
			data.AddParam("@CityMPG", DataAccessParameterType.Text, this.CityMPG);
			data.AddParam("@HighwayMPG", DataAccessParameterType.Text, this.HighwayMPG);
			data.AddParam("@EPAClassification", DataAccessParameterType.Text, this.EPAClassification);
			data.AddParam("@WheelbaseCode", DataAccessParameterType.Text, this.WheelbaseCode);
			data.AddParam("@InternetPrice", DataAccessParameterType.Text, this.InternetPrice);
			data.AddParam("@MiscPrice1", DataAccessParameterType.Text, this.MiscPrice1);
			data.AddParam("@MiscPrice2", DataAccessParameterType.Text, this.MiscPrice2);
			data.AddParam("@MiscPrice3", DataAccessParameterType.Text, this.MiscPrice3);
			data.AddParam("@FactoryCodes", DataAccessParameterType.Text, this.FactoryCodes);
			data.AddParam("@MarketClass", DataAccessParameterType.Text, this.MarketClass);
			data.AddParam("@PassengerCapacity", DataAccessParameterType.Text, this.PassengerCapacity);
			data.AddParam("@ExtColorHexCode", DataAccessParameterType.Text, this.ExtColorHexCode);
			data.AddParam("@IntColorHexCode", DataAccessParameterType.Text, this.IntColorHexCode);
			data.AddParam("@EngineDisplacementCubicInches", DataAccessParameterType.Text, this.EngineDisplacementCubicInches);
			data.AddParam("@PhotoURLs", DataAccessParameterType.Text, this.PhotoURLs);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("RAW_HOME_NET_InsertUpdate");
			if (!data.EOF)
			{
				this.RawHomeNetId = int.Parse(data["RawHomeNetId"]);
			}
			this.retrievedata();
		}
	}
}