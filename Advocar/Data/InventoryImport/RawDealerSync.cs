﻿using System;
using System.Collections.Generic;
using System.Text;
using Advocar.Interface;


namespace Advocar.Data.InventoryImport
{
    public class RawDealerSync : Transaction
    {
        public int Raw_DealerSync_Id
        {
            get;
            set;
        }
        public int JobExecutionRawId
        {
            get;
            set;
        }

        public string AutolandDealerID
        {
            get;
            set;
        }

        public string NewUsed
        {
            get;
            set;
        }

        public string VIN
        {
            get;
            set;
        }

        public string StockNumber
        {
            get;
            set;
        }

        public string Make
        {
            get;
            set;
        }

        public string Model
        {
            get;
            set;
        }

        public int ModelYear
        {
            get;
            set;
        }

        public string TrimPackage
        {
            get;
            set;
        }

        public string BodyStyle
        {
            get;
            set;
        }

        public int Miles
        {
            get;
            set;
        }

        public string Engine
        {
            get;
            set;
        }

        public string Cylinders
        {
            get;
            set;
        }

        public string FuelType
        {
            get;
            set;
        }

        public string Transmission
        {
            get;
            set;
        }

        public float Price
        {
            get;
            set;
        }

        public string ExteriorColor
        {
            get;
            set;
        }

        public string InteriorColor
        {
            get;
            set;
        }

        public string Options
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public string PhotoURL
        {
            get;
            set;
        }



        public RawDealerSync()
        {
        }

        public RawDealerSync(string connectionString, string modifiedUser, int rawDealerSyncId)
            : base(connectionString, modifiedUser)
        {
            this.databaseObjectName = "RAW_DealerSync";
            this.Raw_DealerSync_Id = rawDealerSyncId;
            if (this.Raw_DealerSync_Id > 0)
            {
                this.retrievedata();
            }
        }

        public override void Delete()
        {
            DataAccess data = new DataAccess(this.connection);
            data.AddParam("@RAW_DealerSync_ID", DataAccessParameterType.Numeric, this.Raw_DealerSync_Id.ToString());
            data.ExecuteProcedure("RAW_DealerSync_Delete");
            this.wipeout();
        }

        protected override void retrievedata()
        {
            DataAccess data = new DataAccess(this.connection);
            data.AddParam("@RAW_DealerSync", DataAccessParameterType.Numeric, this.Raw_DealerSync_Id.ToString());
            data.ExecuteProcedure("RAW_DealerSync_GetRecord");
            if (data.EOF)
            {
                this.wipeout();
            }
            else
            {
                this.Raw_DealerSync_Id = int.Parse(data["Raw_DealerSync_Id"]);
                this.JobExecutionRawId = int.Parse(data["JOB_EXECUTION_RAW_ID"]);
                this.AutolandDealerID = data["AutolandDealerID"];
                this.NewUsed = data["NewUsed"];
                this.VIN = data["VIN"];
                this.StockNumber = data["StockNumber"];
                this.Make = data["Make"];
                this.Model = data["Model"];
                this.ModelYear = int.Parse(data["ModelYear"]);
                this.TrimPackage = data["TrimPackage"];
                this.BodyStyle = data["BodyStyle"];
                this.Miles = int.Parse(data["Miles"]);
                this.Engine = data["Engine"];
                this.Cylinders = data["Cylinders"];
                this.FuelType = data["FuelType"];
                this.Transmission = data["Transmission"];
                this.Price = float.Parse(data["Price"]);
                this.ExteriorColor = data["ExteriorColor"];
                this.InteriorColor = data["InteriorColor"];
                this.Options = data["Options"];
                this.Description = data["Description"];
                this.PhotoURL = data["PhotoURL"];
                //Import.MailNotification.Email("retrievedata Run");

            }
        }

        public override void Save()
        {
            DataAccess data = new DataAccess(this.connection);

            data.AddParam("@Raw_DealerSync_Id", DataAccessParameterType.Numeric, Raw_DealerSync_Id.ToString());
            data.AddParam("@JOB_EXECUTION_RAW_ID", DataAccessParameterType.Numeric, JobExecutionRawId.ToString());
            data.AddParam("@AutolandDealerID", DataAccessParameterType.Text, this.AutolandDealerID);
            data.AddParam("@NewUsed", DataAccessParameterType.Text, this.NewUsed);
            data.AddParam("@VIN", DataAccessParameterType.Text, this.VIN);
            data.AddParam("@StockNumber", DataAccessParameterType.Text, this.StockNumber);
            data.AddParam("@Make", DataAccessParameterType.Text, this.Make);
            data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
            data.AddParam("@ModelYear", DataAccessParameterType.Text, this.ModelYear.ToString());
            data.AddParam("@TrimPackage", DataAccessParameterType.Text, this.TrimPackage);
            data.AddParam("@BodyStyle", DataAccessParameterType.Text, this.BodyStyle);
            data.AddParam("@Miles", DataAccessParameterType.Text, this.Miles.ToString());
            data.AddParam("@Engine", DataAccessParameterType.Text, this.Engine);
            data.AddParam("@Cylinders", DataAccessParameterType.Text, this.Cylinders);
            data.AddParam("@FuelType", DataAccessParameterType.Text, this.FuelType.ToString());
            data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
            data.AddParam("@Price", DataAccessParameterType.Text, this.Price.ToString());
            data.AddParam("@ExteriorColor", DataAccessParameterType.Text, this.ExteriorColor.ToString());
            data.AddParam("@InteriorColor", DataAccessParameterType.Text, this.InteriorColor.ToString());
            data.AddParam("@Options", DataAccessParameterType.Text, this.Options.ToString());
            data.AddParam("@Description", DataAccessParameterType.Text, this.Description);
            data.AddParam("@PhotoURL", DataAccessParameterType.Text, this.PhotoURL);

            //Import.MailNotification.Email("Save Method Run");
            data.ExecuteProcedure("RAW_DealerSync_InsertUpdate");
            if (!data.EOF)
            {
                this.Raw_DealerSync_Id = int.Parse(data["AutolandDealerID"]);
            }
            this.retrievedata();
        }
    }
}
