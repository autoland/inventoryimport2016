using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawDealerSpecialtiesLink : Transaction
	{
		private int rawDealerSpecialtiesLinkId = 0;

		private int jobExecutionRawId = 0;

		private string dealerId = "";

		private string vin = "";

		private string vevo = "";

		private string altVideo = "";

		private string photoUrls = "";

		private string windowSticker = "";

		private string buyersGuide = "";

		private string auctionLink = "";

		private string vehicleHistoryReport = "";

		private string reserved1 = "";

		private string reserved2 = "";

		private string reserved3 = "";

		private string reserved4 = "";

		private string reserved5 = "";

		private string reserved6 = "";

		public string AltVideo
		{
			get
			{
				return this.altVideo;
			}
			set
			{
				this.altVideo = value;
			}
		}

		public string AuctionLink
		{
			get
			{
				return this.auctionLink;
			}
			set
			{
				this.auctionLink = value;
			}
		}

		public string BuyersGuide
		{
			get
			{
				return this.buyersGuide;
			}
			set
			{
				this.buyersGuide = value;
			}
		}

		public string DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public string PhotoUrls
		{
			get
			{
				return this.photoUrls;
			}
			set
			{
				this.photoUrls = value;
			}
		}

		public int RawDealerSpecialtiesLinkId
		{
			get
			{
				return this.rawDealerSpecialtiesLinkId;
			}
			set
			{
				this.rawDealerSpecialtiesLinkId = value;
			}
		}

		public string Reserved1
		{
			get
			{
				return this.reserved1;
			}
			set
			{
				this.reserved1 = value;
			}
		}

		public string Reserved2
		{
			get
			{
				return this.reserved2;
			}
			set
			{
				this.reserved2 = value;
			}
		}

		public string Reserved3
		{
			get
			{
				return this.reserved3;
			}
			set
			{
				this.reserved3 = value;
			}
		}

		public string Reserved4
		{
			get
			{
				return this.reserved4;
			}
			set
			{
				this.reserved4 = value;
			}
		}

		public string Reserved5
		{
			get
			{
				return this.reserved5;
			}
			set
			{
				this.reserved5 = value;
			}
		}

		public string Reserved6
		{
			get
			{
				return this.reserved6;
			}
			set
			{
				this.reserved6 = value;
			}
		}

		public string VehicleHistoryReport
		{
			get
			{
				return this.vehicleHistoryReport;
			}
			set
			{
				this.vehicleHistoryReport = value;
			}
		}

		public string Vevo
		{
			get
			{
				return this.vevo;
			}
			set
			{
				this.vevo = value;
			}
		}

		public string Vin
		{
			get
			{
				return this.vin;
			}
			set
			{
				this.vin = value;
			}
		}

		public string WindowSticker
		{
			get
			{
				return this.windowSticker;
			}
			set
			{
				this.windowSticker = value;
			}
		}

		public RawDealerSpecialtiesLink()
		{
		}

		public RawDealerSpecialtiesLink(string connection, string modifiedUserID, int rawDealerSpecialtiesLinkId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "RAW_DEALER_SPECIALTIES_LINK";
			this.RawDealerSpecialtiesLinkId = rawDealerSpecialtiesLinkId;
			if (this.RawDealerSpecialtiesLinkId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawDealerSpecialtiesLinkId", DataAccessParameterType.Numeric, this.RawDealerSpecialtiesLinkId.ToString());
			data.ExecuteProcedure("RAW_DEALER_SPECIALTIES_LINK_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawDealerSpecialtiesLinkId", DataAccessParameterType.Numeric, this.RawDealerSpecialtiesLinkId.ToString());
			data.ExecuteProcedure("RAW_DEALER_SPECIALTIES_LINK_GetRecord");
			if (!data.EOF)
			{
				this.RawDealerSpecialtiesLinkId = int.Parse(data["RawDealerSpecialtiesLinkId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerId = data["DealerId"];
				this.Vin = data["Vin"];
				this.Vevo = data["Vevo"];
				this.AltVideo = data["AltVideo"];
				this.PhotoUrls = data["PhotoUrls"];
				this.WindowSticker = data["WindowSticker"];
				this.BuyersGuide = data["BuyersGuide"];
				this.AuctionLink = data["AuctionLink"];
				this.VehicleHistoryReport = data["VehicleHistoryReport"];
				this.Reserved1 = data["Reserved1"];
				this.Reserved2 = data["Reserved2"];
				this.Reserved3 = data["Reserved3"];
				this.Reserved4 = data["Reserved4"];
				this.Reserved5 = data["Reserved5"];
				this.Reserved6 = data["Reserved6"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawDealerSpecialtiesLinkId = this.RawDealerSpecialtiesLinkId;
			data.AddParam("@RawDealerSpecialtiesLinkId", DataAccessParameterType.Numeric, rawDealerSpecialtiesLinkId.ToString());
			rawDealerSpecialtiesLinkId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawDealerSpecialtiesLinkId.ToString());
			data.AddParam("@DealerId", DataAccessParameterType.Text, this.DealerId);
			data.AddParam("@Vin", DataAccessParameterType.Text, this.Vin);
			data.AddParam("@Vevo", DataAccessParameterType.Text, this.Vevo);
			data.AddParam("@AltVideo", DataAccessParameterType.Text, this.AltVideo);
			data.AddParam("@PhotoUrls", DataAccessParameterType.Text, this.PhotoUrls);
			data.AddParam("@WindowSticker", DataAccessParameterType.Text, this.WindowSticker);
			data.AddParam("@BuyersGuide", DataAccessParameterType.Text, this.BuyersGuide);
			data.AddParam("@AuctionLink", DataAccessParameterType.Text, this.AuctionLink);
			data.AddParam("@VehicleHistoryReport", DataAccessParameterType.Text, this.VehicleHistoryReport);
			data.AddParam("@Reserved1", DataAccessParameterType.Text, this.Reserved1);
			data.AddParam("@Reserved2", DataAccessParameterType.Text, this.Reserved2);
			data.AddParam("@Reserved3", DataAccessParameterType.Text, this.Reserved3);
			data.AddParam("@Reserved4", DataAccessParameterType.Text, this.Reserved4);
			data.AddParam("@Reserved5", DataAccessParameterType.Text, this.Reserved5);
			data.AddParam("@Reserved6", DataAccessParameterType.Text, this.Reserved6);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("RAW_DEALER_SPECIALTIES_LINK_InsertUpdate");
			if (!data.EOF)
			{
				this.RawDealerSpecialtiesLinkId = int.Parse(data["RawDealerSpecialtiesLinkId"]);
			}
			this.retrievedata();
		}
	}
}