using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawAutoRevolution : Transaction
	{
		private int rawAutoRevolutionId = 0;

		private int jobExecutionRawId = 0;

		private string matchID = "";

		private string dealerID = "";

		private int newUsed = 0;

		private string vIN = "";

		private string stockID = "";

		private string category = "";

		private string type = "";

		private int year = 0;

		private string make = "";

		private string model = "";

		private string body = "";

		private int doors = 0;

		private string extColor = "";

		private string intColor = "";

		private string engine = "";

		private string fuel = "";

		private string transmission = "";

		private string cylinders = "";

		private string driveTrain = "";

		private int mileage = 0;

		private float retailPrice = 0f;

		private float wholesalePrice = 0f;

		private int certified = 0;

		private int special = 0;

		private string notes = "";

		private string options = "";

		private string imageURLs = "";

		public string Body
		{
			get
			{
				return this.body;
			}
			set
			{
				this.body = value;
			}
		}

		public string Category
		{
			get
			{
				return this.category;
			}
			set
			{
				this.category = value;
			}
		}

		public int Certified
		{
			get
			{
				return this.certified;
			}
			set
			{
				this.certified = value;
			}
		}

		public string Cylinders
		{
			get
			{
				return this.cylinders;
			}
			set
			{
				this.cylinders = value;
			}
		}

		public string DealerID
		{
			get
			{
				return this.dealerID;
			}
			set
			{
				this.dealerID = value;
			}
		}

		public int Doors
		{
			get
			{
				return this.doors;
			}
			set
			{
				this.doors = value;
			}
		}

		public string DriveTrain
		{
			get
			{
				return this.driveTrain;
			}
			set
			{
				this.driveTrain = value;
			}
		}

		public string Engine
		{
			get
			{
				return this.engine;
			}
			set
			{
				this.engine = value;
			}
		}

		public string ExtColor
		{
			get
			{
				return this.extColor;
			}
			set
			{
				this.extColor = value;
			}
		}

		public string Fuel
		{
			get
			{
				return this.fuel;
			}
			set
			{
				this.fuel = value;
			}
		}

		public string ImageURLs
		{
			get
			{
				return this.imageURLs;
			}
			set
			{
				this.imageURLs = value;
			}
		}

		public string IntColor
		{
			get
			{
				return this.intColor;
			}
			set
			{
				this.intColor = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public string Make
		{
			get
			{
				return this.make;
			}
			set
			{
				this.make = value;
			}
		}

		public string MatchID
		{
			get
			{
				return this.matchID;
			}
			set
			{
				this.matchID = value;
			}
		}

		public int Mileage
		{
			get
			{
				return this.mileage;
			}
			set
			{
				this.mileage = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public int NewUsed
		{
			get
			{
				return this.newUsed;
			}
			set
			{
				this.newUsed = value;
			}
		}

		public string Notes
		{
			get
			{
				return this.notes;
			}
			set
			{
				this.notes = value;
			}
		}

		public string Options
		{
			get
			{
				return this.options;
			}
			set
			{
				this.options = value;
			}
		}

		public int RawAutoRevolutionId
		{
			get
			{
				return this.rawAutoRevolutionId;
			}
			set
			{
				this.rawAutoRevolutionId = value;
			}
		}

		public float RetailPrice
		{
			get
			{
				return this.retailPrice;
			}
			set
			{
				this.retailPrice = value;
			}
		}

		public int Special
		{
			get
			{
				return this.special;
			}
			set
			{
				this.special = value;
			}
		}

		public string StockID
		{
			get
			{
				return this.stockID;
			}
			set
			{
				this.stockID = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string Type
		{
			get
			{
				return this.type;
			}
			set
			{
				this.type = value;
			}
		}

		public string VIN
		{
			get
			{
				return this.vIN;
			}
			set
			{
				this.vIN = value;
			}
		}

		public float WholesalePrice
		{
			get
			{
				return this.wholesalePrice;
			}
			set
			{
				this.wholesalePrice = value;
			}
		}

		public int Year
		{
			get
			{
				return this.year;
			}
			set
			{
				this.year = value;
			}
		}

		public RawAutoRevolution()
		{
		}

		public RawAutoRevolution(string connection, string modifiedUserID, int rawAutoRevolutionId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.RawAutoRevolutionId = rawAutoRevolutionId;
			this.databaseObjectName = "RAW_AUTO_REVOLUTION";
			if (this.RawAutoRevolutionId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawAutoRevolutionId", DataAccessParameterType.Numeric, this.RawAutoRevolutionId.ToString());
			data.ExecuteProcedure("RAW_AUTO_REVOLUTION_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawAutoRevolutionId", DataAccessParameterType.Numeric, this.RawAutoRevolutionId.ToString());
			data.ExecuteProcedure("RAW_AUTO_REVOLUTION_GetRecord");
			if (!data.EOF)
			{
				this.RawAutoRevolutionId = int.Parse(data["RawAutoRevolutionId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.MatchID = data["MatchID"];
				this.DealerID = data["DealerID"];
				this.NewUsed = int.Parse(data["NewUsed"]);
				this.VIN = data["VIN"];
				this.StockID = data["StockID"];
				this.Category = data["Category"];
				this.Type = data["Type"];
				this.Year = int.Parse(data["Year"]);
				this.Make = data["Make"];
				this.Model = data["Model"];
				this.Body = data["Body"];
				this.Doors = int.Parse(data["Doors"]);
				this.ExtColor = data["ExtColor"];
				this.IntColor = data["IntColor"];
				this.Engine = data["Engine"];
				this.Fuel = data["Fuel"];
				this.Transmission = data["Transmission"];
				this.Cylinders = data["Cylinders"];
				this.DriveTrain = data["DriveTrain"];
				this.Mileage = int.Parse(data["Mileage"]);
				this.RetailPrice = float.Parse(data["RetailPrice"]);
				this.WholesalePrice = float.Parse(data["WholesalePrice"]);
				this.Certified = int.Parse(data["Certified"]);
				this.Special = int.Parse(data["Special"]);
				this.Notes = data["Notes"];
				this.Options = data["Options"];
				this.ImageURLs = data["ImageURLs"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawAutoRevolutionId = this.RawAutoRevolutionId;
			data.AddParam("@RawAutoRevolutionId", DataAccessParameterType.Numeric, rawAutoRevolutionId.ToString());
			rawAutoRevolutionId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawAutoRevolutionId.ToString());
			data.AddParam("@MatchID", DataAccessParameterType.Text, this.MatchID);
			data.AddParam("@DealerID", DataAccessParameterType.Text, this.DealerID);
			rawAutoRevolutionId = this.NewUsed;
			data.AddParam("@NewUsed", DataAccessParameterType.Numeric, rawAutoRevolutionId.ToString());
			data.AddParam("@VIN", DataAccessParameterType.Text, this.VIN);
			data.AddParam("@StockID", DataAccessParameterType.Text, this.StockID);
			data.AddParam("@Category", DataAccessParameterType.Text, this.Category);
			data.AddParam("@Type", DataAccessParameterType.Text, this.Type);
			rawAutoRevolutionId = this.Year;
			data.AddParam("@Year", DataAccessParameterType.Numeric, rawAutoRevolutionId.ToString());
			data.AddParam("@Make", DataAccessParameterType.Text, this.Make);
			data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
			data.AddParam("@Body", DataAccessParameterType.Text, this.Body);
			rawAutoRevolutionId = this.Doors;
			data.AddParam("@Doors", DataAccessParameterType.Numeric, rawAutoRevolutionId.ToString());
			data.AddParam("@ExtColor", DataAccessParameterType.Text, this.ExtColor);
			data.AddParam("@IntColor", DataAccessParameterType.Text, this.IntColor);
			data.AddParam("@Engine", DataAccessParameterType.Text, this.Engine);
			data.AddParam("@Fuel", DataAccessParameterType.Text, this.Fuel);
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			data.AddParam("@Cylinders", DataAccessParameterType.Text, this.Cylinders);
			data.AddParam("@DriveTrain", DataAccessParameterType.Text, this.DriveTrain);
			rawAutoRevolutionId = this.Mileage;
			data.AddParam("@Mileage", DataAccessParameterType.Numeric, rawAutoRevolutionId.ToString());
			float retailPrice = this.RetailPrice;
			data.AddParam("@RetailPrice", DataAccessParameterType.Numeric, retailPrice.ToString());
			retailPrice = this.WholesalePrice;
			data.AddParam("@WholesalePrice", DataAccessParameterType.Numeric, retailPrice.ToString());
			rawAutoRevolutionId = this.Certified;
			data.AddParam("@Certified", DataAccessParameterType.Numeric, rawAutoRevolutionId.ToString());
			rawAutoRevolutionId = this.Special;
			data.AddParam("@Special", DataAccessParameterType.Numeric, rawAutoRevolutionId.ToString());
			data.AddParam("@Notes", DataAccessParameterType.Text, this.Notes);
			data.AddParam("@Options", DataAccessParameterType.Text, this.Options);
			data.AddParam("@ImageURLs", DataAccessParameterType.Text, this.ImageURLs);
			data.ExecuteProcedure("RAW_AUTO_REVOLUTION_InsertUpdate");
			if (!data.EOF)
			{
				this.RawAutoRevolutionId = int.Parse(data["RawAutoRevolutionId"]);
			}
			this.retrievedata();
		}
	}
}