using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawECarlist : Transaction
	{
		private int rawECarlistId = 0;

		private int jobExecutionRawId = 0;

		private int dealerId = 0;

		private int feedId = 0;

		private string vin = string.Empty;

		private string stock = string.Empty;

		private int mileage = 0;

		private int year = 0;

		private string make = string.Empty;

		private string model = string.Empty;

		private string trim = string.Empty;

		private string body = string.Empty;

		private string transmission = string.Empty;

		private string engine = string.Empty;

		private string interiorColor = string.Empty;

		private string exteriorColor = string.Empty;

		private string genericInteriorColor = string.Empty;

		private string genericExteriorColor = string.Empty;

		private double price = 0;

		private double retailPrice = 0;

		private double dealerCost = 0;

		private string modelCode = string.Empty;

		private string exteriorColorCode = string.Empty;

		private string interiorColorCode = string.Empty;

		private bool certified = false;

		private string condition = string.Empty;

		private DateTime inDate = DateTime.MinValue;

		private string comments = string.Empty;

		private string imageUrls = string.Empty;

		private string options = string.Empty;

		private string standardEquipment = string.Empty;

		private string optionCodes = string.Empty;

		private int mpgCity = 0;

		private int mpgHighway = 0;

		private int doors = 0;

		public string Body
		{
			get
			{
				return this.body;
			}
			set
			{
				this.body = value;
			}
		}

		public bool Certified
		{
			get
			{
				return this.certified;
			}
			set
			{
				this.certified = value;
			}
		}

		public string Comments
		{
			get
			{
				return this.comments;
			}
			set
			{
				this.comments = value;
			}
		}

		public string Condition
		{
			get
			{
				return this.condition;
			}
			set
			{
				this.condition = value;
			}
		}

		public double DealerCost
		{
			get
			{
				return this.dealerCost;
			}
			set
			{
				this.dealerCost = value;
			}
		}

		public int DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public int Doors
		{
			get
			{
				return this.doors;
			}
			set
			{
				this.doors = value;
			}
		}

		public string Engine
		{
			get
			{
				return this.engine;
			}
			set
			{
				this.engine = value;
			}
		}

		public string ExteriorColor
		{
			get
			{
				return this.exteriorColor;
			}
			set
			{
				this.exteriorColor = value;
			}
		}

		public string ExteriorColorCode
		{
			get
			{
				return this.exteriorColorCode;
			}
			set
			{
				this.exteriorColorCode = value;
			}
		}

		public int FeedId
		{
			get
			{
				return this.feedId;
			}
			set
			{
				this.feedId = value;
			}
		}

		public string GenericExteriorColor
		{
			get
			{
				return this.genericExteriorColor;
			}
			set
			{
				this.genericExteriorColor = value;
			}
		}

		public string GenericInteriorColor
		{
			get
			{
				return this.genericInteriorColor;
			}
			set
			{
				this.genericInteriorColor = value;
			}
		}

		public string ImageUrls
		{
			get
			{
				return this.imageUrls;
			}
			set
			{
				this.imageUrls = value;
			}
		}

		public DateTime InDate
		{
			get
			{
				return this.inDate;
			}
			set
			{
				this.inDate = value;
			}
		}

		public string InteriorColor
		{
			get
			{
				return this.interiorColor;
			}
			set
			{
				this.interiorColor = value;
			}
		}

		public string InteriorColorCode
		{
			get
			{
				return this.interiorColorCode;
			}
			set
			{
				this.interiorColorCode = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public string Make
		{
			get
			{
				return this.make;
			}
			set
			{
				this.make = value;
			}
		}

		public int Mileage
		{
			get
			{
				return this.mileage;
			}
			set
			{
				this.mileage = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public string ModelCode
		{
			get
			{
				return this.modelCode;
			}
			set
			{
				this.modelCode = value;
			}
		}

		public int MpgCity
		{
			get
			{
				return this.mpgCity;
			}
			set
			{
				this.mpgCity = value;
			}
		}

		public int MpgHighway
		{
			get
			{
				return this.mpgHighway;
			}
			set
			{
				this.mpgHighway = value;
			}
		}

		public string OptionCodes
		{
			get
			{
				return this.optionCodes;
			}
			set
			{
				this.optionCodes = value;
			}
		}

		public string Options
		{
			get
			{
				return this.options;
			}
			set
			{
				this.options = value;
			}
		}

		public double Price
		{
			get
			{
				return this.price;
			}
			set
			{
				this.price = value;
			}
		}

		public int RawECarlistId
		{
			get
			{
				return this.rawECarlistId;
			}
			set
			{
				this.rawECarlistId = value;
			}
		}

		public double RetailPrice
		{
			get
			{
				return this.retailPrice;
			}
			set
			{
				this.retailPrice = value;
			}
		}

		public string StandardEquipment
		{
			get
			{
				return this.standardEquipment;
			}
			set
			{
				this.standardEquipment = value;
			}
		}

		public string Stock
		{
			get
			{
				return this.stock;
			}
			set
			{
				this.stock = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string Trim
		{
			get
			{
				return this.trim;
			}
			set
			{
				this.trim = value;
			}
		}

		public string Vin
		{
			get
			{
				return this.vin;
			}
			set
			{
				this.vin = value;
			}
		}

		public int Year
		{
			get
			{
				return this.year;
			}
			set
			{
				this.year = value;
			}
		}

		public RawECarlist()
		{
		}

		public RawECarlist(string connection, string modifiedUserID, int rawECarlistId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.rawECarlistId = rawECarlistId;
			this.databaseObjectName = "RAW_ECARLIST";
			if (this.rawECarlistId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawECarlistId", DataAccessParameterType.Numeric, this.rawECarlistId.ToString());
			data.ExecuteProcedure("RAW_ECARLIST_Delete");
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawECarlistId", DataAccessParameterType.Numeric, this.rawECarlistId.ToString());
			data.ExecuteProcedure("RAW_ECARLIST_GetRecord");
			if (!data.EOF)
			{
				this.jobExecutionRawId = Convert.ToInt32(data["JobExecutionRawId"]);
				this.dealerId = Convert.ToInt32(data["DealerId"]);
				this.feedId = Convert.ToInt32(data["FeedId"]);
				this.vin = data["Vin"];
				this.stock = data["Stock"];
				this.mileage = Convert.ToInt32(data["Stock"]);
				this.year = Convert.ToInt32(data["Year"]);
				this.make = data["Make"];
				this.model = data["Model"];
				this.trim = data["Trim"];
				this.body = data["Body"];
				this.transmission = data["Transmission"];
				this.engine = data["Engine"];
				this.interiorColor = data["InteriorColor"];
				this.exteriorColor = data["ExteriorColor"];
				this.genericInteriorColor = data["GenericInteriorColor"];
				this.genericExteriorColor = data["GenericExteriorColor"];
				this.price = Convert.ToDouble(data["Price"]);
				this.RetailPrice = Convert.ToDouble(data["RetailPrice"]);
				this.dealerCost = Convert.ToDouble(data["DealerCost"]);
				this.modelCode = data["ModelCode"];
				this.exteriorColorCode = data["ExteriorColorCode"];
				this.interiorColorCode = data["InteriorColorCode"];
				this.Certified = Convert.ToInt32(data["Certified"]) > 0;
				this.condition = data["Condition"];
				this.inDate = Convert.ToDateTime(data["InDate"]);
				this.comments = data["Comments"];
				this.imageUrls = data["ImageUrls"];
				this.options = data["Options"];
				this.standardEquipment = data["StandardEquipment"];
				this.optionCodes = data["OptionCodes"];
				this.mpgCity = Convert.ToInt32(data["MpgCity"]);
				this.mpgHighway = Convert.ToInt32(data["MpgHighway"]);
				this.doors = Convert.ToInt32(data["Doors"]);
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawECarlistId", DataAccessParameterType.Numeric, this.rawECarlistId.ToString());
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, this.jobExecutionRawId.ToString());
			data.AddParam("@DealerId", DataAccessParameterType.Numeric, this.dealerId.ToString());
			data.AddParam("@FeedId", DataAccessParameterType.Numeric, this.feedId.ToString());
			data.AddParam("@Vin", DataAccessParameterType.Text, this.vin);
			data.AddParam("@Stock", DataAccessParameterType.Text, this.stock);
			data.AddParam("@Mileage", DataAccessParameterType.Numeric, this.mileage.ToString());
			data.AddParam("@Year", DataAccessParameterType.Numeric, this.year.ToString());
			data.AddParam("@Make", DataAccessParameterType.Text, this.make);
			data.AddParam("@Model", DataAccessParameterType.Text, this.model);
			data.AddParam("@Trim", DataAccessParameterType.Text, this.trim);
			data.AddParam("@Body", DataAccessParameterType.Text, this.body);
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.transmission);
			data.AddParam("@Engine", DataAccessParameterType.Text, this.engine);
			data.AddParam("@InteriorColor", DataAccessParameterType.Text, this.interiorColor);
			data.AddParam("@ExteriorColor", DataAccessParameterType.Text, this.exteriorColor);
			data.AddParam("@GenericInteriorColor", DataAccessParameterType.Text, this.genericInteriorColor);
			data.AddParam("@GenericExteriorColor", DataAccessParameterType.Text, this.genericExteriorColor);
			data.AddParam("@Price", DataAccessParameterType.Numeric, this.price.ToString());
			data.AddParam("@RetailPrice", DataAccessParameterType.Numeric, this.retailPrice.ToString());
			data.AddParam("@DealerCost", DataAccessParameterType.Numeric, this.dealerCost.ToString());
			data.AddParam("@ModelCode", DataAccessParameterType.Text, this.modelCode);
			data.AddParam("@ExteriorColorCode", DataAccessParameterType.Text, this.exteriorColorCode);
			data.AddParam("@InteriorColorCode", DataAccessParameterType.Text, this.interiorColorCode);
			data.AddParam("@Certified", DataAccessParameterType.Numeric, this.certified.ToString());
			data.AddParam("@Condition", DataAccessParameterType.Text, this.condition);
			data.AddParam("@InDate", DataAccessParameterType.DateTime, this.inDate.ToString());
			data.AddParam("@Comments", DataAccessParameterType.Text, this.comments);
			data.AddParam("@ImageUrls", DataAccessParameterType.Text, this.imageUrls);
			data.AddParam("@Options", DataAccessParameterType.Text, this.options);
			data.AddParam("@StandardEquipment", DataAccessParameterType.Text, this.standardEquipment);
			data.AddParam("@OptionCodes", DataAccessParameterType.Text, this.optionCodes);
			data.AddParam("@MpgCity", DataAccessParameterType.Numeric, this.mpgCity.ToString());
			data.AddParam("@MpgHighway", DataAccessParameterType.Numeric, this.mpgHighway.ToString());
			data.AddParam("@Doors", DataAccessParameterType.Numeric, this.doors.ToString());
			data.ExecuteProcedure("RAW_ECARLIST_InsertUpdate");
		}
	}
}