using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawDigitalMotorWorks : Transaction
	{
		private int rawDigitalMotorWorksId = 0;

		private int jobExecutionRawId = 0;

		private string dealerId = "";

		private string stockNumber = "";

		private string inventoryDate = "";

		private string newUsed = "";

		private string status = "";

		private float invoicePrice = 0f;

		private float packAmount = 0f;

		private float cost = 0f;

		private float listPrice = 0f;

		private float msrp = 0f;

		private string lotLocation = "";

		private string condition = "";

		private string tagline = "";

		private string isCertified = "";

		private string certificationNumber = "";

		private string vIN = "";

		private string make = "";

		private string model = "";

		private int modelYear = 0;

		private string modelCode = "";

		private string trimLevel = "";

		private string subTrimLevel = "";

		private string classification = "";

		private string vehicleTypeCode = "";

		private int odometer = 0;

		private int payloadCapcacity = 0;

		private int seatingCapacity = 0;

		private float wheelBase = 0f;

		private string bodyDescription = "";

		private int bodyDoorCount = 0;

		private string drivetrainDescription = "";

		private string engineDescription = "";

		private int engineCylinderCount = 0;

		private string transmissionDescription = "";

		private string transmissionType = "";

		private string exteriorColorDescription = "";

		private string exteriorColorBaseColor = "";

		private string interiorDescription = "";

		private string interiorColor = "";

		private string standardFeatures = "";

		private string dealerAddedFeatures = "";

		private string lastModifiedDate = "";

		private string modifiedFlag = "";

		private string dealerName = "";

		private string dealerAddress = "";

		private string dealerCity = "";

		private string dealerState = "";

		private string dealerPostalCode = "";

		private string dealerPhoneNumber = "";

		private string mediaId = "";

		private string imageUrls = "";

		public string BodyDescription
		{
			get
			{
				return this.bodyDescription;
			}
			set
			{
				this.bodyDescription = value;
			}
		}

		public int BodyDoorCount
		{
			get
			{
				return this.bodyDoorCount;
			}
			set
			{
				this.bodyDoorCount = value;
			}
		}

		public string CertificationNumber
		{
			get
			{
				return this.certificationNumber;
			}
			set
			{
				this.certificationNumber = value;
			}
		}

		public string Classification
		{
			get
			{
				return this.classification;
			}
			set
			{
				this.classification = value;
			}
		}

		public string Condition
		{
			get
			{
				return this.condition;
			}
			set
			{
				this.condition = value;
			}
		}

		public float Cost
		{
			get
			{
				return this.cost;
			}
			set
			{
				this.cost = value;
			}
		}

		public string DealerAddedFeatures
		{
			get
			{
				return this.dealerAddedFeatures;
			}
			set
			{
				this.dealerAddedFeatures = value;
			}
		}

		public string DealerAddress
		{
			get
			{
				return this.dealerAddress;
			}
			set
			{
				this.dealerAddress = value;
			}
		}

		public string DealerCity
		{
			get
			{
				return this.dealerCity;
			}
			set
			{
				this.dealerCity = value;
			}
		}

		public string DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public string DealerName
		{
			get
			{
				return this.dealerName;
			}
			set
			{
				this.dealerName = value;
			}
		}

		public string DealerPhoneNumber
		{
			get
			{
				return this.dealerPhoneNumber;
			}
			set
			{
				this.dealerPhoneNumber = value;
			}
		}

		public string DealerPostalCode
		{
			get
			{
				return this.dealerPostalCode;
			}
			set
			{
				this.dealerPostalCode = value;
			}
		}

		public string DealerState
		{
			get
			{
				return this.dealerState;
			}
			set
			{
				this.dealerState = value;
			}
		}

		public string DrivetrainDescription
		{
			get
			{
				return this.drivetrainDescription;
			}
			set
			{
				this.drivetrainDescription = value;
			}
		}

		public int EngineCylinderCount
		{
			get
			{
				return this.engineCylinderCount;
			}
			set
			{
				this.engineCylinderCount = value;
			}
		}

		public string EngineDescription
		{
			get
			{
				return this.engineDescription;
			}
			set
			{
				this.engineDescription = value;
			}
		}

		public string ExteriorColorBaseColor
		{
			get
			{
				return this.exteriorColorBaseColor;
			}
			set
			{
				this.exteriorColorBaseColor = value;
			}
		}

		public string ExteriorColorDescription
		{
			get
			{
				return this.exteriorColorDescription;
			}
			set
			{
				this.exteriorColorDescription = value;
			}
		}

		public string ImageUrls
		{
			get
			{
				return this.imageUrls;
			}
			set
			{
				this.imageUrls = value;
			}
		}

		public string InteriorColor
		{
			get
			{
				return this.interiorColor;
			}
			set
			{
				this.interiorColor = value;
			}
		}

		public string InteriorDescription
		{
			get
			{
				return this.interiorDescription;
			}
			set
			{
				this.interiorDescription = value;
			}
		}

		public string InventoryDate
		{
			get
			{
				return this.inventoryDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.inventoryDate = "";
				}
				else
				{
					this.inventoryDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public float InvoicePrice
		{
			get
			{
				return this.invoicePrice;
			}
			set
			{
				this.invoicePrice = value;
			}
		}

		public string IsCertified
		{
			get
			{
				return this.isCertified;
			}
			set
			{
				this.isCertified = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public string LastModifiedDate
		{
			get
			{
				return this.lastModifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.lastModifiedDate = "";
				}
				else
				{
					this.lastModifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public float ListPrice
		{
			get
			{
				return this.listPrice;
			}
			set
			{
				this.listPrice = value;
			}
		}

		public string LotLocation
		{
			get
			{
				return this.lotLocation;
			}
			set
			{
				this.lotLocation = value;
			}
		}

		public string Make
		{
			get
			{
				return this.make;
			}
			set
			{
				this.make = value;
			}
		}

		public string MediaId
		{
			get
			{
				return this.mediaId;
			}
			set
			{
				this.mediaId = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public string ModelCode
		{
			get
			{
				return this.modelCode;
			}
			set
			{
				this.modelCode = value;
			}
		}

		public int ModelYear
		{
			get
			{
				return this.modelYear;
			}
			set
			{
				this.modelYear = value;
			}
		}

		public string ModifiedFlag
		{
			get
			{
				return this.modifiedFlag;
			}
			set
			{
				this.modifiedFlag = value;
			}
		}

		public float Msrp
		{
			get
			{
				return this.msrp;
			}
			set
			{
				this.msrp = value;
			}
		}

		public string NewUsed
		{
			get
			{
				return this.newUsed;
			}
			set
			{
				this.newUsed = value;
			}
		}

		public int Odometer
		{
			get
			{
				return this.odometer;
			}
			set
			{
				this.odometer = value;
			}
		}

		public float PackAmount
		{
			get
			{
				return this.packAmount;
			}
			set
			{
				this.packAmount = value;
			}
		}

		public int PayloadCapcacity
		{
			get
			{
				return this.payloadCapcacity;
			}
			set
			{
				this.payloadCapcacity = value;
			}
		}

		public int RawDigitalMotorWorksId
		{
			get
			{
				return this.rawDigitalMotorWorksId;
			}
			set
			{
				this.rawDigitalMotorWorksId = value;
			}
		}

		public int SeatingCapacity
		{
			get
			{
				return this.seatingCapacity;
			}
			set
			{
				this.seatingCapacity = value;
			}
		}

		public string StandardFeatures
		{
			get
			{
				return this.standardFeatures;
			}
			set
			{
				this.standardFeatures = value;
			}
		}

		public string Status
		{
			get
			{
				return this.status;
			}
			set
			{
				this.status = value;
			}
		}

		public string StockNumber
		{
			get
			{
				return this.stockNumber;
			}
			set
			{
				this.stockNumber = value;
			}
		}

		public string SubTrimLevel
		{
			get
			{
				return this.subTrimLevel;
			}
			set
			{
				this.subTrimLevel = value;
			}
		}

		public string Tagline
		{
			get
			{
				return this.tagline;
			}
			set
			{
				this.tagline = value;
			}
		}

		public string TransmissionDescription
		{
			get
			{
				return this.transmissionDescription;
			}
			set
			{
				this.transmissionDescription = value;
			}
		}

		public string TransmissionType
		{
			get
			{
				return this.transmissionType;
			}
			set
			{
				this.transmissionType = value;
			}
		}

		public string TrimLevel
		{
			get
			{
				return this.trimLevel;
			}
			set
			{
				this.trimLevel = value;
			}
		}

		public string VehicleTypeCode
		{
			get
			{
				return this.vehicleTypeCode;
			}
			set
			{
				this.vehicleTypeCode = value;
			}
		}

		public string VIN
		{
			get
			{
				return this.vIN;
			}
			set
			{
				this.vIN = value;
			}
		}

		public float WheelBase
		{
			get
			{
				return this.wheelBase;
			}
			set
			{
				this.wheelBase = value;
			}
		}

		public RawDigitalMotorWorks()
		{
		}

		public RawDigitalMotorWorks(string connection, string modifiedUserID, int rawDigitalMotorWorksId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "RAW_DIGITAL_MOTOR_WORKS";
			this.RawDigitalMotorWorksId = rawDigitalMotorWorksId;
			if (this.RawDigitalMotorWorksId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawDigitalMotorWorksId", DataAccessParameterType.Numeric, this.RawDigitalMotorWorksId.ToString());
			data.ExecuteProcedure("RAW_DIGITAL_MOTOR_WORKS_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawDigitalMotorWorksId", DataAccessParameterType.Numeric, this.RawDigitalMotorWorksId.ToString());
			data.ExecuteProcedure("RAW_DIGITAL_MOTOR_WORKS_GetRecord");
			if (!data.EOF)
			{
				this.RawDigitalMotorWorksId = int.Parse(data["RawDigitalMotorWorksId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerId = data["DealerId"];
				this.StockNumber = data["StockNumber"];
				this.InventoryDate = data["InventoryDate"];
				this.NewUsed = data["NewUsed"];
				this.Status = data["Status"];
				this.InvoicePrice = float.Parse(data["InvoicePrice"]);
				this.PackAmount = float.Parse(data["PackAmount"]);
				this.Cost = float.Parse(data["Cost"]);
				this.ListPrice = float.Parse(data["ListPrice"]);
				this.Msrp = float.Parse(data["Msrp"]);
				this.LotLocation = data["LotLocation"];
				this.Condition = data["Condition"];
				this.Tagline = data["Tagline"];
				this.IsCertified = data["IsCertified"];
				this.CertificationNumber = data["CertificationNumber"];
				this.VIN = data["VIN"];
				this.Make = data["Make"];
				this.Model = data["Model"];
				this.ModelYear = int.Parse(data["ModelYear"]);
				this.ModelCode = data["ModelCode"];
				this.TrimLevel = data["TrimLevel"];
				this.SubTrimLevel = data["SubTrimLevel"];
				this.Classification = data["Classification"];
				this.VehicleTypeCode = data["VehicleTypeCode"];
				this.Odometer = int.Parse(data["Odometer"]);
				this.PayloadCapcacity = int.Parse(data["PayloadCapcacity"]);
				this.SeatingCapacity = int.Parse(data["SeatingCapacity"]);
				this.WheelBase = float.Parse(data["WheelBase"]);
				this.BodyDescription = data["BodyDescription"];
				this.BodyDoorCount = int.Parse(data["BodyDoorCount"]);
				this.DrivetrainDescription = data["DrivetrainDescription"];
				this.EngineDescription = data["EngineDescription"];
				this.EngineCylinderCount = int.Parse(data["EngineCylinderCount"]);
				this.TransmissionDescription = data["TransmissionDescription"];
				this.TransmissionType = data["TransmissionType"];
				this.ExteriorColorDescription = data["ExteriorColorDescription"];
				this.ExteriorColorBaseColor = data["ExteriorColorBaseColor"];
				this.InteriorDescription = data["InteriorDescription"];
				this.InteriorColor = data["InteriorColor"];
				this.StandardFeatures = data["StandardFeatures"];
				this.DealerAddedFeatures = data["DealerAddedFeatures"];
				this.LastModifiedDate = data["LastModifiedDate"];
				this.ModifiedFlag = data["ModifiedFlag"];
				this.DealerName = data["DealerName"];
				this.DealerAddress = data["DealerAddress"];
				this.DealerCity = data["DealerCity"];
				this.DealerState = data["DealerState"];
				this.DealerPostalCode = data["DealerPostalCode"];
				this.DealerPhoneNumber = data["DealerPhoneNumber"];
				this.MediaId = data["MediaId"];
				this.ImageUrls = data["ImageUrls"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawDigitalMotorWorksId = this.RawDigitalMotorWorksId;
			data.AddParam("@RawDigitalMotorWorksId", DataAccessParameterType.Numeric, rawDigitalMotorWorksId.ToString());
			rawDigitalMotorWorksId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawDigitalMotorWorksId.ToString());
			data.AddParam("@DealerId", DataAccessParameterType.Text, this.DealerId);
			data.AddParam("@StockNumber", DataAccessParameterType.Text, this.StockNumber);
			data.AddParam("@InventoryDate", DataAccessParameterType.DateTime, this.InventoryDate);
			data.AddParam("@NewUsed", DataAccessParameterType.Text, this.NewUsed);
			data.AddParam("@Status", DataAccessParameterType.Text, this.Status);
			float invoicePrice = this.InvoicePrice;
			data.AddParam("@InvoicePrice", DataAccessParameterType.Numeric, invoicePrice.ToString());
			invoicePrice = this.PackAmount;
			data.AddParam("@PackAmount", DataAccessParameterType.Numeric, invoicePrice.ToString());
			invoicePrice = this.Cost;
			data.AddParam("@Cost", DataAccessParameterType.Numeric, invoicePrice.ToString());
			invoicePrice = this.ListPrice;
			data.AddParam("@ListPrice", DataAccessParameterType.Numeric, invoicePrice.ToString());
			invoicePrice = this.Msrp;
			data.AddParam("@Msrp", DataAccessParameterType.Numeric, invoicePrice.ToString());
			data.AddParam("@LotLocation", DataAccessParameterType.Text, this.LotLocation);
			data.AddParam("@Condition", DataAccessParameterType.Text, this.Condition);
			data.AddParam("@Tagline", DataAccessParameterType.Text, this.Tagline);
			data.AddParam("@IsCertified", DataAccessParameterType.Text, this.IsCertified);
			data.AddParam("@CertificationNumber", DataAccessParameterType.Text, this.CertificationNumber);
			data.AddParam("@VIN", DataAccessParameterType.Text, this.VIN);
			data.AddParam("@Make", DataAccessParameterType.Text, this.Make);
			data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
			rawDigitalMotorWorksId = this.ModelYear;
			data.AddParam("@ModelYear", DataAccessParameterType.Numeric, rawDigitalMotorWorksId.ToString());
			data.AddParam("@ModelCode", DataAccessParameterType.Text, this.ModelCode);
			data.AddParam("@TrimLevel", DataAccessParameterType.Text, this.TrimLevel);
			data.AddParam("@SubTrimLevel", DataAccessParameterType.Text, this.SubTrimLevel);
			data.AddParam("@Classification", DataAccessParameterType.Text, this.Classification);
			data.AddParam("@VehicleTypeCode", DataAccessParameterType.Text, this.VehicleTypeCode);
			rawDigitalMotorWorksId = this.Odometer;
			data.AddParam("@Odometer", DataAccessParameterType.Numeric, rawDigitalMotorWorksId.ToString());
			rawDigitalMotorWorksId = this.PayloadCapcacity;
			data.AddParam("@PayloadCapcacity", DataAccessParameterType.Numeric, rawDigitalMotorWorksId.ToString());
			rawDigitalMotorWorksId = this.SeatingCapacity;
			data.AddParam("@SeatingCapacity", DataAccessParameterType.Numeric, rawDigitalMotorWorksId.ToString());
			invoicePrice = this.WheelBase;
			data.AddParam("@WheelBase", DataAccessParameterType.Numeric, invoicePrice.ToString());
			data.AddParam("@BodyDescription", DataAccessParameterType.Text, this.BodyDescription);
			rawDigitalMotorWorksId = this.BodyDoorCount;
			data.AddParam("@BodyDoorCount", DataAccessParameterType.Numeric, rawDigitalMotorWorksId.ToString());
			data.AddParam("@DrivetrainDescription", DataAccessParameterType.Text, this.DrivetrainDescription);
			data.AddParam("@EngineDescription", DataAccessParameterType.Text, this.EngineDescription);
			rawDigitalMotorWorksId = this.EngineCylinderCount;
			data.AddParam("@EngineCylinderCount", DataAccessParameterType.Numeric, rawDigitalMotorWorksId.ToString());
			data.AddParam("@TransmissionDescription", DataAccessParameterType.Text, this.TransmissionDescription);
			data.AddParam("@TransmissionType", DataAccessParameterType.Text, this.TransmissionType);
			data.AddParam("@ExteriorColorDescription", DataAccessParameterType.Text, this.ExteriorColorDescription);
			data.AddParam("@ExteriorColorBaseColor", DataAccessParameterType.Text, this.ExteriorColorBaseColor);
			data.AddParam("@InteriorDescription", DataAccessParameterType.Text, this.InteriorDescription);
			data.AddParam("@InteriorColor", DataAccessParameterType.Text, this.InteriorColor);
			data.AddParam("@StandardFeatures", DataAccessParameterType.Text, this.StandardFeatures);
			data.AddParam("@DealerAddedFeatures", DataAccessParameterType.Text, this.DealerAddedFeatures);
			data.AddParam("@LastModifiedDate", DataAccessParameterType.DateTime, this.LastModifiedDate);
			data.AddParam("@ModifiedFlag", DataAccessParameterType.Text, this.ModifiedFlag);
			data.AddParam("@DealerName", DataAccessParameterType.Text, this.DealerName);
			data.AddParam("@DealerAddress", DataAccessParameterType.Text, this.DealerAddress);
			data.AddParam("@DealerCity", DataAccessParameterType.Text, this.DealerCity);
			data.AddParam("@DealerState", DataAccessParameterType.Text, this.DealerState);
			data.AddParam("@DealerPostalCode", DataAccessParameterType.Text, this.DealerPostalCode);
			data.AddParam("@DealerPhoneNumber", DataAccessParameterType.Text, this.DealerPhoneNumber);
			data.AddParam("@MediaId", DataAccessParameterType.Text, this.MediaId);
			data.AddParam("@ImageUrls", DataAccessParameterType.Text, this.ImageUrls);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("RAW_DIGITAL_MOTOR_WORKS_InsertUpdate");
			if (!data.EOF)
			{
				this.RawDigitalMotorWorksId = int.Parse(data["RawDigitalMotorWorksId"]);
			}
			this.retrievedata();
		}
	}
}