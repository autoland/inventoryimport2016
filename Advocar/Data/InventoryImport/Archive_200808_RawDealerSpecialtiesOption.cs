using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class Archive_200808_RawDealerSpecialtiesOption : Transaction
	{
		private int rawDealerSpecialtiesOptionId = 0;

		private int jobExecutionRawId = 0;

		private string option = "";

		private string description = "";

		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public string Option
		{
			get
			{
				return this.option;
			}
			set
			{
				this.option = value;
			}
		}

		public int RawDealerSpecialtiesOptionId
		{
			get
			{
				return this.rawDealerSpecialtiesOptionId;
			}
			set
			{
				this.rawDealerSpecialtiesOptionId = value;
			}
		}

		public Archive_200808_RawDealerSpecialtiesOption()
		{
		}

		public Archive_200808_RawDealerSpecialtiesOption(string connection, string modifiedUserID, int rawDealerSpecialtiesOptionId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.RawDealerSpecialtiesOptionId = rawDealerSpecialtiesOptionId;
			this.databaseObjectName = "RAW_DEALER_SPECIALTIES_OPTION";
			if (this.RawDealerSpecialtiesOptionId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawDealerSpecialtiesOptionId", DataAccessParameterType.Numeric, this.RawDealerSpecialtiesOptionId.ToString());
			data.ExecuteProcedure("RAW_DEALER_SPECIALTIES_OPTION_Delete");
			this.wipeout();
		}

		public static string GetOptionDescription(int jobExecutionRawId, string code, string connection)
		{
			DataAccess data = new DataAccess(connection);
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, jobExecutionRawId.ToString());
			data.AddParam("@Option", DataAccessParameterType.Text, code);
			data.ExecuteProcedure("RAW_DEALER_SPECIALTIES_OPTION_GetRecord");
			return (!data.EOF ? data["Description"] : "");
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawDealerSpecialtiesOptionId", DataAccessParameterType.Numeric, this.RawDealerSpecialtiesOptionId.ToString());
			data.ExecuteProcedure("RAW_DEALER_SPECIALTIES_OPTION_GetRecord");
			if (!data.EOF)
			{
				this.RawDealerSpecialtiesOptionId = int.Parse(data["RawDealerSpecialtiesOptionId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.Option = data["Option"];
				this.Description = data["Description"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawDealerSpecialtiesOptionId = this.RawDealerSpecialtiesOptionId;
			data.AddParam("@RawDealerSpecialtiesOptionId", DataAccessParameterType.Numeric, rawDealerSpecialtiesOptionId.ToString());
			rawDealerSpecialtiesOptionId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawDealerSpecialtiesOptionId.ToString());
			data.AddParam("@Option", DataAccessParameterType.Text, this.Option);
			data.AddParam("@Description", DataAccessParameterType.Text, this.Description);
			data.ExecuteProcedure("RAW_DEALER_SPECIALTIES_OPTION_InsertUpdate");
			if (!data.EOF)
			{
				this.RawDealerSpecialtiesOptionId = int.Parse(data["RawDealerSpecialtiesOptionId"]);
			}
			this.retrievedata();
		}
	}
}