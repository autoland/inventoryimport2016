using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.InventoryImport
{
	public class RawBeavertonToyota : Transaction
	{
		private int rawBeavertonToyotaId = 0;

		private int jobExecutionRawId = 0;

		private string dealerID = "";

		private string newUsed = "";

		private string stockID = "";

		private string vehicleDescription = "";

		private int year = 0;

		private string make = "";

		private string model = "";

		private string trim = "";

		private string vIN = "";

		private string style = "";

		private string engine = "";

		private string transmission = "";

		private string options = "";

		private int mileage = 0;

		private float sellingPrice = 0f;

		private float retailPrice = 0f;

		private string extColor = "";

		private string imageUrls = "";

		public string DealerID
		{
			get
			{
				return this.dealerID;
			}
			set
			{
				this.dealerID = value;
			}
		}

		public string Engine
		{
			get
			{
				return this.engine;
			}
			set
			{
				this.engine = value;
			}
		}

		public string ExtColor
		{
			get
			{
				return this.extColor;
			}
			set
			{
				this.extColor = value;
			}
		}

		public string ImageUrls
		{
			get
			{
				return this.imageUrls;
			}
			set
			{
				this.imageUrls = value;
			}
		}

		public int JobExecutionRawId
		{
			get
			{
				return this.jobExecutionRawId;
			}
			set
			{
				this.jobExecutionRawId = value;
			}
		}

		public string Make
		{
			get
			{
				return this.make;
			}
			set
			{
				this.make = value;
			}
		}

		public int Mileage
		{
			get
			{
				return this.mileage;
			}
			set
			{
				this.mileage = value;
			}
		}

		public string Model
		{
			get
			{
				return this.model;
			}
			set
			{
				this.model = value;
			}
		}

		public string NewUsed
		{
			get
			{
				return this.newUsed;
			}
			set
			{
				this.newUsed = value;
			}
		}

		public string Options
		{
			get
			{
				return this.options;
			}
			set
			{
				this.options = value;
			}
		}

		public int RawBeavertonToyotaId
		{
			get
			{
				return this.rawBeavertonToyotaId;
			}
			set
			{
				this.rawBeavertonToyotaId = value;
			}
		}

		public float RetailPrice
		{
			get
			{
				return this.retailPrice;
			}
			set
			{
				this.retailPrice = value;
			}
		}

		public float SellingPrice
		{
			get
			{
				return this.sellingPrice;
			}
			set
			{
				this.sellingPrice = value;
			}
		}

		public string StockID
		{
			get
			{
				return this.stockID;
			}
			set
			{
				this.stockID = value;
			}
		}

		public string Style
		{
			get
			{
				return this.style;
			}
			set
			{
				this.style = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string Trim
		{
			get
			{
				return this.trim;
			}
			set
			{
				this.trim = value;
			}
		}

		public string VehicleDescription
		{
			get
			{
				return this.vehicleDescription;
			}
			set
			{
				this.vehicleDescription = value;
			}
		}

		public string VIN
		{
			get
			{
				return this.vIN;
			}
			set
			{
				this.vIN = value;
			}
		}

		public int Year
		{
			get
			{
				return this.year;
			}
			set
			{
				this.year = value;
			}
		}

		public RawBeavertonToyota()
		{
		}

		public RawBeavertonToyota(string connection, string modifiedUserID, int rawBeavertonToyotaId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.RawBeavertonToyotaId = rawBeavertonToyotaId;
			this.databaseObjectName = "RAW_BEAVERTON_TOYOTA";
			if (this.RawBeavertonToyotaId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawBeavertonToyotaId", DataAccessParameterType.Numeric, this.RawBeavertonToyotaId.ToString());
			data.ExecuteProcedure("RAW_BEAVERTON_TOYOTA_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@RawBeavertonToyotaId", DataAccessParameterType.Numeric, this.RawBeavertonToyotaId.ToString());
			data.ExecuteProcedure("RAW_BEAVERTON_TOYOTA_GetRecord");
			if (!data.EOF)
			{
				this.RawBeavertonToyotaId = int.Parse(data["RawBeavertonToyotaId"]);
				this.JobExecutionRawId = int.Parse(data["JobExecutionRawId"]);
				this.DealerID = data["DealerID"];
				this.NewUsed = data["NewUsed"];
				this.StockID = data["StockID"];
				this.VehicleDescription = data["VehicleDescription"];
				this.Year = int.Parse(data["Year"]);
				this.Make = data["Make"];
				this.Model = data["Model"];
				this.Trim = data["Trim"];
				this.VIN = data["VIN"];
				this.Style = data["Style"];
				this.Engine = data["Engine"];
				this.Transmission = data["Transmission"];
				this.Options = data["Options"];
				this.Mileage = int.Parse(data["Mileage"]);
				this.SellingPrice = float.Parse(data["SellingPrice"]);
				this.RetailPrice = float.Parse(data["RetailPrice"]);
				this.ExtColor = data["ExtColor"];
				this.ImageUrls = data["ImageUrls"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int rawBeavertonToyotaId = this.RawBeavertonToyotaId;
			data.AddParam("@RawBeavertonToyotaId", DataAccessParameterType.Numeric, rawBeavertonToyotaId.ToString());
			rawBeavertonToyotaId = this.JobExecutionRawId;
			data.AddParam("@JobExecutionRawId", DataAccessParameterType.Numeric, rawBeavertonToyotaId.ToString());
			data.AddParam("@DealerID", DataAccessParameterType.Text, this.DealerID);
			data.AddParam("@NewUsed", DataAccessParameterType.Text, this.NewUsed);
			data.AddParam("@StockID", DataAccessParameterType.Text, this.StockID);
			data.AddParam("@VehicleDescription", DataAccessParameterType.Text, this.VehicleDescription);
			rawBeavertonToyotaId = this.Year;
			data.AddParam("@Year", DataAccessParameterType.Numeric, rawBeavertonToyotaId.ToString());
			data.AddParam("@Make", DataAccessParameterType.Text, this.Make);
			data.AddParam("@Model", DataAccessParameterType.Text, this.Model);
			data.AddParam("@Trim", DataAccessParameterType.Text, this.Trim);
			data.AddParam("@VIN", DataAccessParameterType.Text, this.VIN);
			data.AddParam("@Style", DataAccessParameterType.Text, this.Style);
			data.AddParam("@Engine", DataAccessParameterType.Text, this.Engine);
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			data.AddParam("@Options", DataAccessParameterType.Text, this.Options);
			rawBeavertonToyotaId = this.Mileage;
			data.AddParam("@Mileage", DataAccessParameterType.Numeric, rawBeavertonToyotaId.ToString());
			float sellingPrice = this.SellingPrice;
			data.AddParam("@SellingPrice", DataAccessParameterType.Numeric, sellingPrice.ToString());
			sellingPrice = this.RetailPrice;
			data.AddParam("@RetailPrice", DataAccessParameterType.Numeric, sellingPrice.ToString());
			data.AddParam("@ExtColor", DataAccessParameterType.Text, this.ExtColor);
			data.AddParam("@ImageUrls", DataAccessParameterType.Text, this.ImageUrls);
			data.ExecuteProcedure("RAW_BEAVERTON_TOYOTA_InsertUpdate");
			if (!data.EOF)
			{
				this.RawBeavertonToyotaId = int.Parse(data["RawBeavertonToyotaId"]);
			}
			this.retrievedata();
		}
	}
}