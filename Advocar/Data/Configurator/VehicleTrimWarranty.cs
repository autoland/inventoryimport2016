using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Configurator
{
	public class VehicleTrimWarranty : Transaction
	{
		private int vehicleTrimWarrantyId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int vehicleTrimId = 0;

		private string warranty = "";

		private int lookupId_WarrantyType = 0;

		private int months = 0;

		private int miles = 0;

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public int LookupId_WarrantyType
		{
			get
			{
				return this.lookupId_WarrantyType;
			}
			set
			{
				this.lookupId_WarrantyType = value;
			}
		}

		public int Miles
		{
			get
			{
				return this.miles;
			}
			set
			{
				this.miles = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public int Months
		{
			get
			{
				return this.months;
			}
			set
			{
				this.months = value;
			}
		}

		public int VehicleTrimId
		{
			get
			{
				return this.vehicleTrimId;
			}
			set
			{
				this.vehicleTrimId = value;
			}
		}

		public int VehicleTrimWarrantyId
		{
			get
			{
				return this.vehicleTrimWarrantyId;
			}
			set
			{
				this.vehicleTrimWarrantyId = value;
			}
		}

		public string Warranty
		{
			get
			{
				return this.warranty;
			}
			set
			{
				this.warranty = value;
			}
		}

		public VehicleTrimWarranty()
		{
		}

		public VehicleTrimWarranty(string connection, string modifiedUserID, int vehicleTrimWarrantyId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.VehicleTrimWarrantyId = vehicleTrimWarrantyId;
			if (this.VehicleTrimWarrantyId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleTrimWarrantyId", DataAccessParameterType.Numeric, this.VehicleTrimWarrantyId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_TRIM_WARRANTY_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleTrimWarrantyId", DataAccessParameterType.Numeric, this.VehicleTrimWarrantyId.ToString());
			data.ExecuteProcedure("VEHICLE_TRIM_WARRANTY_GetRecord");
			if (!data.EOF)
			{
				this.VehicleTrimWarrantyId = int.Parse(data["VehicleTrimWarrantyId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.VehicleTrimId = int.Parse(data["VehicleTrimId"]);
				this.Warranty = data["Warranty"];
				this.LookupId_WarrantyType = int.Parse(data["LookupId_WarrantyType"]);
				this.Months = int.Parse(data["Months"]);
				this.Miles = int.Parse(data["Miles"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int vehicleTrimWarrantyId = this.VehicleTrimWarrantyId;
			data.AddParam("@VehicleTrimWarrantyId", DataAccessParameterType.Numeric, vehicleTrimWarrantyId.ToString());
			vehicleTrimWarrantyId = this.VehicleTrimId;
			data.AddParam("@VehicleTrimId", DataAccessParameterType.Numeric, vehicleTrimWarrantyId.ToString());
			data.AddParam("@Warranty", DataAccessParameterType.Text, this.Warranty);
			vehicleTrimWarrantyId = this.LookupId_WarrantyType;
			data.AddParam("@LookupId_WarrantyType", DataAccessParameterType.Numeric, vehicleTrimWarrantyId.ToString());
			vehicleTrimWarrantyId = this.Months;
			data.AddParam("@Months", DataAccessParameterType.Numeric, vehicleTrimWarrantyId.ToString());
			vehicleTrimWarrantyId = this.Miles;
			data.AddParam("@Miles", DataAccessParameterType.Numeric, vehicleTrimWarrantyId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_TRIM_WARRANTY_InsertUpdate");
			if (!data.EOF)
			{
				this.VehicleTrimWarrantyId = int.Parse(data["VehicleTrimWarrantyId"]);
			}
			this.retrievedata();
		}
	}
}