using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Configurator
{
	public class VehicleTrimColor : Transaction
	{
		private int vehicleTrimColorId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int vehicleTrimId = 0;

		private string trimColorName = "";

		private int rgbRed = 0;

		private int rgbGreen = 0;

		private int rgbBlue = 0;

		private int lookupId_ColorType = 0;

		private float priceMsrp = 0f;

		private float priceInvoice = 0f;

		private float priceMarket = 0f;

		private string dataSourceId = "";

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string DataSourceId
		{
			get
			{
				return this.dataSourceId;
			}
			set
			{
				this.dataSourceId = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public int LookupId_ColorType
		{
			get
			{
				return this.lookupId_ColorType;
			}
			set
			{
				this.lookupId_ColorType = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public float PriceInvoice
		{
			get
			{
				return this.priceInvoice;
			}
			set
			{
				this.priceInvoice = value;
			}
		}

		public float PriceMarket
		{
			get
			{
				return this.priceMarket;
			}
			set
			{
				this.priceMarket = value;
			}
		}

		public float PriceMsrp
		{
			get
			{
				return this.priceMsrp;
			}
			set
			{
				this.priceMsrp = value;
			}
		}

		public int RgbBlue
		{
			get
			{
				return this.rgbBlue;
			}
			set
			{
				this.rgbBlue = value;
			}
		}

		public int RgbGreen
		{
			get
			{
				return this.rgbGreen;
			}
			set
			{
				this.rgbGreen = value;
			}
		}

		public int RgbRed
		{
			get
			{
				return this.rgbRed;
			}
			set
			{
				this.rgbRed = value;
			}
		}

		public string TrimColorName
		{
			get
			{
				return this.trimColorName;
			}
			set
			{
				this.trimColorName = value;
			}
		}

		public int VehicleTrimColorId
		{
			get
			{
				return this.vehicleTrimColorId;
			}
			set
			{
				this.vehicleTrimColorId = value;
			}
		}

		public int VehicleTrimId
		{
			get
			{
				return this.vehicleTrimId;
			}
			set
			{
				this.vehicleTrimId = value;
			}
		}

		public VehicleTrimColor()
		{
		}

		public VehicleTrimColor(string connection, string modifiedUserID, int vehicleTrimColorId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.VehicleTrimColorId = vehicleTrimColorId;
			if (this.VehicleTrimColorId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleTrimColorId", DataAccessParameterType.Numeric, this.VehicleTrimColorId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_TRIM_COLOR_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleTrimColorId", DataAccessParameterType.Numeric, this.VehicleTrimColorId.ToString());
			data.ExecuteProcedure("VEHICLE_TRIM_COLOR_GetRecord");
			if (!data.EOF)
			{
				this.VehicleTrimColorId = int.Parse(data["VehicleTrimColorId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.VehicleTrimId = int.Parse(data["VehicleTrimId"]);
				this.TrimColorName = data["TrimColorName"];
				this.RgbRed = int.Parse(data["RgbRed"]);
				this.RgbGreen = int.Parse(data["RgbGreen"]);
				this.RgbBlue = int.Parse(data["RgbBlue"]);
				this.LookupId_ColorType = int.Parse(data["LookupId_ColorType"]);
				this.PriceMsrp = float.Parse(data["PriceMsrp"]);
				this.PriceInvoice = float.Parse(data["PriceInvoice"]);
				this.PriceMarket = float.Parse(data["PriceMarket"]);
				this.DataSourceId = data["DataSourceId"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int vehicleTrimColorId = this.VehicleTrimColorId;
			data.AddParam("@VehicleTrimColorId", DataAccessParameterType.Numeric, vehicleTrimColorId.ToString());
			vehicleTrimColorId = this.VehicleTrimId;
			data.AddParam("@VehicleTrimId", DataAccessParameterType.Numeric, vehicleTrimColorId.ToString());
			data.AddParam("@TrimColorName", DataAccessParameterType.Text, this.TrimColorName);
			vehicleTrimColorId = this.RgbRed;
			data.AddParam("@RgbRed", DataAccessParameterType.Numeric, vehicleTrimColorId.ToString());
			vehicleTrimColorId = this.RgbGreen;
			data.AddParam("@RgbGreen", DataAccessParameterType.Numeric, vehicleTrimColorId.ToString());
			vehicleTrimColorId = this.RgbBlue;
			data.AddParam("@RgbBlue", DataAccessParameterType.Numeric, vehicleTrimColorId.ToString());
			vehicleTrimColorId = this.LookupId_ColorType;
			data.AddParam("@LookupId_ColorType", DataAccessParameterType.Numeric, vehicleTrimColorId.ToString());
			float priceMsrp = this.PriceMsrp;
			data.AddParam("@PriceMsrp", DataAccessParameterType.Numeric, priceMsrp.ToString());
			priceMsrp = this.PriceInvoice;
			data.AddParam("@PriceInvoice", DataAccessParameterType.Numeric, priceMsrp.ToString());
			priceMsrp = this.PriceMarket;
			data.AddParam("@PriceMarket", DataAccessParameterType.Numeric, priceMsrp.ToString());
			data.AddParam("@DataSourceId", DataAccessParameterType.Text, this.DataSourceId);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_TRIM_COLOR_InsertUpdate");
			if (!data.EOF)
			{
				this.VehicleTrimColorId = int.Parse(data["VehicleTrimColorId"]);
			}
			this.retrievedata();
		}
	}
}