using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Configurator
{
	public class VehicleTrim : Transaction
	{
		private int vehicleTrimId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int lookupId_ConfiguratorDataSource = 0;

		private string dataSourceId = "";

		private int vehicleYearId = 0;

		private int vehicleMakeId = 0;

		private int vehicleModelId = 0;

		private int vehicleModelYearId = 0;

		private string trimName = "";

		private string trimOtherInfo = "";

		private string trimOtherInfo2 = "";

		private int doors = 0;

		private string style = "";

		private int lookupId_Style = 0;

		private string drivetrain = "";

		private int lookupId_Drivetrain = 0;

		private string fuel = "";

		private int lookupId_Fuel = 0;

		private bool isTurbo = false;

		private int lookupId_Engine = 0;

		private string engine = "";

		private int lookupId_Transmission = 0;

		private string transmission = "";

		private int lookupId_TruckCab = 0;

		private string truckCab = "";

		private int lookupId_TruckBed = 0;

		private string truckBed = "";

		private float priceMsrp = 0f;

		private float priceInvoice = 0f;

		private float priceFreight = 0f;

		private float priceMarket = 0f;

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string DataSourceId
		{
			get
			{
				return this.dataSourceId;
			}
			set
			{
				this.dataSourceId = value;
			}
		}

		public int Doors
		{
			get
			{
				return this.doors;
			}
			set
			{
				this.doors = value;
			}
		}

		public string Drivetrain
		{
			get
			{
				return this.drivetrain;
			}
			set
			{
				this.drivetrain = value;
			}
		}

		public string Engine
		{
			get
			{
				return this.engine;
			}
			set
			{
				this.engine = value;
			}
		}

		public string Fuel
		{
			get
			{
				return this.fuel;
			}
			set
			{
				this.fuel = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public bool IsTurbo
		{
			get
			{
				return this.isTurbo;
			}
			set
			{
				this.isTurbo = value;
			}
		}

		public int LookupId_ConfiguratorDataSource
		{
			get
			{
				return this.lookupId_ConfiguratorDataSource;
			}
			set
			{
				this.lookupId_ConfiguratorDataSource = value;
			}
		}

		public int LookupId_Drivetrain
		{
			get
			{
				return this.lookupId_Drivetrain;
			}
			set
			{
				this.lookupId_Drivetrain = value;
			}
		}

		public int LookupId_Engine
		{
			get
			{
				return this.lookupId_Engine;
			}
			set
			{
				this.lookupId_Engine = value;
			}
		}

		public int LookupId_Fuel
		{
			get
			{
				return this.lookupId_Fuel;
			}
			set
			{
				this.lookupId_Fuel = value;
			}
		}

		public int LookupId_Style
		{
			get
			{
				return this.lookupId_Style;
			}
			set
			{
				this.lookupId_Style = value;
			}
		}

		public int LookupId_Transmission
		{
			get
			{
				return this.lookupId_Transmission;
			}
			set
			{
				this.lookupId_Transmission = value;
			}
		}

		public int LookupId_TruckBed
		{
			get
			{
				return this.lookupId_TruckBed;
			}
			set
			{
				this.lookupId_TruckBed = value;
			}
		}

		public int LookupId_TruckCab
		{
			get
			{
				return this.lookupId_TruckCab;
			}
			set
			{
				this.lookupId_TruckCab = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public float PriceFreight
		{
			get
			{
				return this.priceFreight;
			}
			set
			{
				this.priceFreight = value;
			}
		}

		public float PriceInvoice
		{
			get
			{
				return this.priceInvoice;
			}
			set
			{
				this.priceInvoice = value;
			}
		}

		public float PriceMarket
		{
			get
			{
				return this.priceMarket;
			}
			set
			{
				this.priceMarket = value;
			}
		}

		public float PriceMsrp
		{
			get
			{
				return this.priceMsrp;
			}
			set
			{
				this.priceMsrp = value;
			}
		}

		public string Style
		{
			get
			{
				return this.style;
			}
			set
			{
				this.style = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string TrimName
		{
			get
			{
				return this.trimName;
			}
			set
			{
				this.trimName = value;
			}
		}

		public string TrimOtherInfo
		{
			get
			{
				return this.trimOtherInfo;
			}
			set
			{
				this.trimOtherInfo = value;
			}
		}

		public string TrimOtherInfo2
		{
			get
			{
				return this.trimOtherInfo2;
			}
			set
			{
				this.trimOtherInfo2 = value;
			}
		}

		public string TruckBed
		{
			get
			{
				return this.truckBed;
			}
			set
			{
				this.truckBed = value;
			}
		}

		public string TruckCab
		{
			get
			{
				return this.truckCab;
			}
			set
			{
				this.truckCab = value;
			}
		}

		public int VehicleMakeId
		{
			get
			{
				return this.vehicleMakeId;
			}
			set
			{
				this.vehicleMakeId = value;
			}
		}

		public int VehicleModelId
		{
			get
			{
				return this.vehicleModelId;
			}
			set
			{
				this.vehicleModelId = value;
			}
		}

		public int VehicleModelYearId
		{
			get
			{
				return this.vehicleModelYearId;
			}
			set
			{
				this.vehicleModelYearId = value;
			}
		}

		public int VehicleTrimId
		{
			get
			{
				return this.vehicleTrimId;
			}
			set
			{
				this.vehicleTrimId = value;
			}
		}

		public int VehicleYearId
		{
			get
			{
				return this.vehicleYearId;
			}
			set
			{
				this.vehicleYearId = value;
			}
		}

		public VehicleTrim()
		{
		}

		public VehicleTrim(string connection, string modifiedUserID, int vehicleTrimId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.VehicleTrimId = vehicleTrimId;
			if (this.VehicleTrimId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleTrimId", DataAccessParameterType.Numeric, this.VehicleTrimId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_TRIM_Delete");
			this.wipeout();
		}

		public static int GetRecordId(string LookupIdConfiguratorDataSource, string DataSourceId, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@LookupIdConfiguratorDataSource", DataAccessParameterType.Numeric, LookupIdConfiguratorDataSource);
			data.AddParam("@DataSourceId", DataAccessParameterType.Text, DataSourceId);
			data.ExecuteProcedure("VEHICLE_TRIM_GetRecord");
			num = (!data.EOF ? int.Parse(data["VehicleTrimId"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleTrimId", DataAccessParameterType.Numeric, this.VehicleTrimId.ToString());
			data.ExecuteProcedure("VEHICLE_TRIM_GetRecord");
			if (!data.EOF)
			{
				this.VehicleTrimId = int.Parse(data["VehicleTrimId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.LookupId_ConfiguratorDataSource = int.Parse(data["LookupId_ConfiguratorDataSource"]);
				this.DataSourceId = data["DataSourceId"];
				this.VehicleYearId = int.Parse(data["VehicleYearId"]);
				this.VehicleMakeId = int.Parse(data["VehicleMakeId"]);
				this.VehicleModelId = int.Parse(data["VehicleModelId"]);
				this.VehicleModelYearId = int.Parse(data["VehicleModelYearId"]);
				this.TrimName = data["TrimName"];
				this.TrimOtherInfo = data["TrimOtherInfo"];
				this.TrimOtherInfo2 = data["TrimOtherInfo2"];
				this.Doors = int.Parse(data["Doors"]);
				this.Style = data["Style"];
				this.LookupId_Style = int.Parse(data["LookupId_Style"]);
				this.Drivetrain = data["Drivetrain"];
				this.LookupId_Drivetrain = int.Parse(data["LookupId_Drivetrain"]);
				this.Fuel = data["Fuel"];
				this.LookupId_Fuel = int.Parse(data["LookupId_Fuel"]);
				this.IsTurbo = bool.Parse(data["IsTurbo"]);
				this.LookupId_Engine = int.Parse(data["LookupId_Engine"]);
				this.Engine = data["Engine"];
				this.LookupId_Transmission = int.Parse(data["LookupId_Transmission"]);
				this.Transmission = data["Transmission"];
				this.LookupId_TruckCab = int.Parse(data["LookupId_TruckCab"]);
				this.TruckCab = data["TruckCab"];
				this.LookupId_TruckBed = int.Parse(data["LookupId_TruckBed"]);
				this.TruckBed = data["TruckBed"];
				this.PriceMsrp = float.Parse(data["PriceMsrp"]);
				this.PriceInvoice = float.Parse(data["PriceInvoice"]);
				this.PriceFreight = float.Parse(data["PriceFreight"]);
				this.PriceMarket = float.Parse(data["PriceMarket"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int vehicleTrimId = this.VehicleTrimId;
			data.AddParam("@VehicleTrimId", DataAccessParameterType.Numeric, vehicleTrimId.ToString());
			vehicleTrimId = this.LookupId_ConfiguratorDataSource;
			data.AddParam("@LookupId_ConfiguratorDataSource", DataAccessParameterType.Numeric, vehicleTrimId.ToString());
			data.AddParam("@DataSourceId", DataAccessParameterType.Text, this.DataSourceId);
			vehicleTrimId = this.VehicleYearId;
			data.AddParam("@VehicleYearId", DataAccessParameterType.Numeric, vehicleTrimId.ToString());
			vehicleTrimId = this.VehicleMakeId;
			data.AddParam("@VehicleMakeId", DataAccessParameterType.Numeric, vehicleTrimId.ToString());
			vehicleTrimId = this.VehicleModelId;
			data.AddParam("@VehicleModelId", DataAccessParameterType.Numeric, vehicleTrimId.ToString());
			vehicleTrimId = this.VehicleModelYearId;
			data.AddParam("@VehicleModelYearId", DataAccessParameterType.Numeric, vehicleTrimId.ToString());
			data.AddParam("@TrimName", DataAccessParameterType.Text, this.TrimName);
			data.AddParam("@TrimOtherInfo", DataAccessParameterType.Text, this.TrimOtherInfo);
			data.AddParam("@TrimOtherInfo2", DataAccessParameterType.Text, this.TrimOtherInfo2);
			vehicleTrimId = this.Doors;
			data.AddParam("@Doors", DataAccessParameterType.Numeric, vehicleTrimId.ToString());
			data.AddParam("@Style", DataAccessParameterType.Text, this.Style);
			vehicleTrimId = this.LookupId_Style;
			data.AddParam("@LookupId_Style", DataAccessParameterType.Numeric, vehicleTrimId.ToString());
			data.AddParam("@Drivetrain", DataAccessParameterType.Text, this.Drivetrain);
			vehicleTrimId = this.LookupId_Drivetrain;
			data.AddParam("@LookupId_Drivetrain", DataAccessParameterType.Numeric, vehicleTrimId.ToString());
			data.AddParam("@Fuel", DataAccessParameterType.Text, this.Fuel);
			vehicleTrimId = this.LookupId_Fuel;
			data.AddParam("@LookupId_Fuel", DataAccessParameterType.Numeric, vehicleTrimId.ToString());
			data.AddParam("@IsTurbo", DataAccessParameterType.Bool, this.IsTurbo.ToString());
			vehicleTrimId = this.LookupId_Engine;
			data.AddParam("@LookupId_Engine", DataAccessParameterType.Numeric, vehicleTrimId.ToString());
			data.AddParam("@Engine", DataAccessParameterType.Text, this.Engine);
			vehicleTrimId = this.LookupId_Transmission;
			data.AddParam("@LookupId_Transmission", DataAccessParameterType.Numeric, vehicleTrimId.ToString());
			data.AddParam("@Transmission", DataAccessParameterType.Text, this.Transmission);
			vehicleTrimId = this.LookupId_TruckCab;
			data.AddParam("@LookupId_TruckCab", DataAccessParameterType.Numeric, vehicleTrimId.ToString());
			data.AddParam("@TruckCab", DataAccessParameterType.Text, this.TruckCab);
			vehicleTrimId = this.LookupId_TruckBed;
			data.AddParam("@LookupId_TruckBed", DataAccessParameterType.Numeric, vehicleTrimId.ToString());
			data.AddParam("@TruckBed", DataAccessParameterType.Text, this.TruckBed);
			float priceMsrp = this.PriceMsrp;
			data.AddParam("@PriceMsrp", DataAccessParameterType.Numeric, priceMsrp.ToString());
			priceMsrp = this.PriceInvoice;
			data.AddParam("@PriceInvoice", DataAccessParameterType.Numeric, priceMsrp.ToString());
			priceMsrp = this.PriceFreight;
			data.AddParam("@PriceFreight", DataAccessParameterType.Numeric, priceMsrp.ToString());
			priceMsrp = this.PriceMarket;
			data.AddParam("@PriceMarket", DataAccessParameterType.Numeric, priceMsrp.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_TRIM_InsertUpdate");
			if (!data.EOF)
			{
				this.VehicleTrimId = int.Parse(data["VehicleTrimId"]);
			}
			this.retrievedata();
		}
	}
}