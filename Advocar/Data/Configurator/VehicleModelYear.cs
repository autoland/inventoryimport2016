using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Configurator
{
	public class VehicleModelYear : Transaction
	{
		private int vehicleModelYearId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int vehicleYearId = 0;

		private int vehicleMakeId = 0;

		private int vehicleModelId = 0;

		private string modelName = "";

		private int lookupId_ConfiguratorDataSource = 0;

		private string dataSourceId = "";

		private bool isActive = false;

		private string logoFile = "";

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string DataSourceId
		{
			get
			{
				return this.dataSourceId;
			}
			set
			{
				this.dataSourceId = value;
			}
		}

		public bool IsActive
		{
			get
			{
				return this.isActive;
			}
			set
			{
				this.isActive = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public string LogoFile
		{
			get
			{
				return this.logoFile;
			}
			set
			{
				this.logoFile = value;
			}
		}

		public int LookupId_ConfiguratorDataSource
		{
			get
			{
				return this.lookupId_ConfiguratorDataSource;
			}
			set
			{
				this.lookupId_ConfiguratorDataSource = value;
			}
		}

		public string ModelName
		{
			get
			{
				return this.modelName;
			}
			set
			{
				this.modelName = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public int VehicleMakeId
		{
			get
			{
				return this.vehicleMakeId;
			}
			set
			{
				this.vehicleMakeId = value;
			}
		}

		public int VehicleModelId
		{
			get
			{
				return this.vehicleModelId;
			}
			set
			{
				this.vehicleModelId = value;
			}
		}

		public int VehicleModelYearId
		{
			get
			{
				return this.vehicleModelYearId;
			}
			set
			{
				this.vehicleModelYearId = value;
			}
		}

		public int VehicleYearId
		{
			get
			{
				return this.vehicleYearId;
			}
			set
			{
				this.vehicleYearId = value;
			}
		}

		public VehicleModelYear()
		{
		}

		public VehicleModelYear(string connection, string modifiedUserID, int vehicleModelYearId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.VehicleModelYearId = vehicleModelYearId;
			if (this.VehicleModelYearId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleModelYearId", DataAccessParameterType.Numeric, this.VehicleModelYearId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_MODEL_YEAR_Delete");
			this.wipeout();
		}

		public static int GetRecordId(string LookupIdConfiguratorDataSource, string DataSourceId, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@LookupId_ConfiguratorDataSource", DataAccessParameterType.Numeric, LookupIdConfiguratorDataSource);
			data.AddParam("@DataSourceId", DataAccessParameterType.Text, DataSourceId);
			data.ExecuteProcedure("VEHICLE_MODEL_YEAR_GetRecord");
			num = (!data.EOF ? int.Parse(data["VehicleModelYearId"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleModelYearId", DataAccessParameterType.Numeric, this.VehicleModelYearId.ToString());
			data.ExecuteProcedure("VEHICLE_MODEL_YEAR_GetRecord");
			if (!data.EOF)
			{
				this.VehicleModelYearId = int.Parse(data["VehicleModelYearId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.VehicleYearId = int.Parse(data["VehicleYearId"]);
				this.VehicleMakeId = int.Parse(data["VehicleMakeId"]);
				this.VehicleModelId = int.Parse(data["VehicleModelId"]);
				this.ModelName = data["ModelName"];
				this.LookupId_ConfiguratorDataSource = int.Parse(data["LookupId_ConfiguratorDataSource"]);
				this.DataSourceId = data["DataSourceId"];
				this.IsActive = bool.Parse(data["IsActive"]);
				this.LogoFile = data["LogoFile"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int vehicleModelYearId = this.VehicleModelYearId;
			data.AddParam("@VehicleModelYearId", DataAccessParameterType.Numeric, vehicleModelYearId.ToString());
			vehicleModelYearId = this.VehicleYearId;
			data.AddParam("@VehicleYearId", DataAccessParameterType.Numeric, vehicleModelYearId.ToString());
			vehicleModelYearId = this.VehicleMakeId;
			data.AddParam("@VehicleMakeId", DataAccessParameterType.Numeric, vehicleModelYearId.ToString());
			vehicleModelYearId = this.VehicleModelId;
			data.AddParam("@VehicleModelId", DataAccessParameterType.Numeric, vehicleModelYearId.ToString());
			data.AddParam("@ModelName", DataAccessParameterType.Text, this.ModelName);
			vehicleModelYearId = this.LookupId_ConfiguratorDataSource;
			data.AddParam("@LookupId_ConfiguratorDataSource", DataAccessParameterType.Numeric, vehicleModelYearId.ToString());
			data.AddParam("@DataSourceId", DataAccessParameterType.Text, this.DataSourceId);
			data.AddParam("@IsActive", DataAccessParameterType.Bool, this.IsActive.ToString());
			data.AddParam("@LogoFile", DataAccessParameterType.Text, this.LogoFile);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_MODEL_YEAR_InsertUpdate");
			if (!data.EOF)
			{
				this.VehicleModelYearId = int.Parse(data["VehicleModelYearId"]);
			}
			this.retrievedata();
		}
	}
}