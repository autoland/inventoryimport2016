using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Configurator
{
	public class VehicleMake : Transaction
	{
		private int vehicleMakeId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private string makeName = "";

		private int lookupId_ConfiguratorDataSource = 0;

		private string dataSourceId = "";

		private string logoFile = "";

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string DataSourceId
		{
			get
			{
				return this.dataSourceId;
			}
			set
			{
				this.dataSourceId = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public string LogoFile
		{
			get
			{
				return this.logoFile;
			}
			set
			{
				this.logoFile = value;
			}
		}

		public int LookupId_ConfiguratorDataSource
		{
			get
			{
				return this.lookupId_ConfiguratorDataSource;
			}
			set
			{
				this.lookupId_ConfiguratorDataSource = value;
			}
		}

		public string MakeName
		{
			get
			{
				return this.makeName;
			}
			set
			{
				this.makeName = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public int VehicleMakeId
		{
			get
			{
				return this.vehicleMakeId;
			}
			set
			{
				this.vehicleMakeId = value;
			}
		}

		public VehicleMake()
		{
		}

		public VehicleMake(string connection, string modifiedUserID, int vehicleMakeId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.VehicleMakeId = vehicleMakeId;
			if (this.VehicleMakeId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleMakeId", DataAccessParameterType.Numeric, this.VehicleMakeId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_MAKE_Delete");
			this.wipeout();
		}

		public static int GetRecordId(string LookupIdConfiguratorDataSource, string DataSourceId, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@LookupIdConfiguratorDataSource", DataAccessParameterType.Numeric, LookupIdConfiguratorDataSource);
			data.AddParam("@DataSourceId", DataAccessParameterType.Text, DataSourceId);
			data.ExecuteProcedure("VEHICLE_MAKE_GetRecord");
			num = (!data.EOF ? int.Parse(data["VehicleMakeId"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleMakeId", DataAccessParameterType.Numeric, this.VehicleMakeId.ToString());
			data.ExecuteProcedure("VEHICLE_MAKE_GetRecord");
			if (!data.EOF)
			{
				this.VehicleMakeId = int.Parse(data["VehicleMakeId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.MakeName = data["MakeName"];
				this.LookupId_ConfiguratorDataSource = int.Parse(data["LookupId_ConfiguratorDataSource"]);
				this.DataSourceId = data["DataSourceId"];
				this.LogoFile = data["LogoFile"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int vehicleMakeId = this.VehicleMakeId;
			data.AddParam("@VehicleMakeId", DataAccessParameterType.Numeric, vehicleMakeId.ToString());
			data.AddParam("@MakeName", DataAccessParameterType.Text, this.MakeName);
			vehicleMakeId = this.LookupId_ConfiguratorDataSource;
			data.AddParam("@LookupId_ConfiguratorDataSource", DataAccessParameterType.Numeric, vehicleMakeId.ToString());
			data.AddParam("@DataSourceId", DataAccessParameterType.Text, this.DataSourceId);
			data.AddParam("@LogoFile", DataAccessParameterType.Text, this.LogoFile);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_MAKE_InsertUpdate");
			if (!data.EOF)
			{
				this.VehicleMakeId = int.Parse(data["VehicleMakeId"]);
			}
			this.retrievedata();
		}
	}
}