using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Configurator
{
	public class VehicleModel : Transaction
	{
		private int vehicleModelId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int vehicleMakeId = 0;

		private string modelName = "";

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public string ModelName
		{
			get
			{
				return this.modelName;
			}
			set
			{
				this.modelName = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public int VehicleMakeId
		{
			get
			{
				return this.vehicleMakeId;
			}
			set
			{
				this.vehicleMakeId = value;
			}
		}

		public int VehicleModelId
		{
			get
			{
				return this.vehicleModelId;
			}
			set
			{
				this.vehicleModelId = value;
			}
		}

		public VehicleModel()
		{
		}

		public VehicleModel(string connection, string modifiedUserID, int vehicleModelId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.VehicleModelId = vehicleModelId;
			if (this.VehicleModelId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleModelId", DataAccessParameterType.Numeric, this.VehicleModelId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_MODEL_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleModelId", DataAccessParameterType.Numeric, this.VehicleModelId.ToString());
			data.ExecuteProcedure("VEHICLE_MODEL_GetRecord");
			if (!data.EOF)
			{
				this.VehicleModelId = int.Parse(data["VehicleModelId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.VehicleMakeId = int.Parse(data["VehicleMakeId"]);
				this.ModelName = data["ModelName"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int vehicleModelId = this.VehicleModelId;
			data.AddParam("@VehicleModelId", DataAccessParameterType.Numeric, vehicleModelId.ToString());
			vehicleModelId = this.VehicleMakeId;
			data.AddParam("@VehicleMakeId", DataAccessParameterType.Numeric, vehicleModelId.ToString());
			data.AddParam("@ModelName", DataAccessParameterType.Text, this.ModelName);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_MODEL_InsertUpdate");
			if (!data.EOF)
			{
				this.VehicleModelId = int.Parse(data["VehicleModelId"]);
			}
			this.retrievedata();
		}
	}
}