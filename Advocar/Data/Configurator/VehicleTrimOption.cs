using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Configurator
{
	public class VehicleTrimOption : Transaction
	{
		private int vehicleTrimOptionId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int vehicleTrimId = 0;

		private int lookupId_OptionGroup = 0;

		private string optionName = "";

		private string optionNotes = "";

		private float priceMsrp = 0f;

		private float priceInvoice = 0f;

		private float priceMarket = 0f;

		private string priceMarketFormula = "";

		private int sequenceNo = 0;

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public int LookupId_OptionGroup
		{
			get
			{
				return this.lookupId_OptionGroup;
			}
			set
			{
				this.lookupId_OptionGroup = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public string OptionName
		{
			get
			{
				return this.optionName;
			}
			set
			{
				this.optionName = value;
			}
		}

		public string OptionNotes
		{
			get
			{
				return this.optionNotes;
			}
			set
			{
				this.optionNotes = value;
			}
		}

		public float PriceInvoice
		{
			get
			{
				return this.priceInvoice;
			}
			set
			{
				this.priceInvoice = value;
			}
		}

		public float PriceMarket
		{
			get
			{
				return this.priceMarket;
			}
			set
			{
				this.priceMarket = value;
			}
		}

		public string PriceMarketFormula
		{
			get
			{
				return this.priceMarketFormula;
			}
			set
			{
				this.priceMarketFormula = value;
			}
		}

		public float PriceMsrp
		{
			get
			{
				return this.priceMsrp;
			}
			set
			{
				this.priceMsrp = value;
			}
		}

		public int SequenceNo
		{
			get
			{
				return this.sequenceNo;
			}
			set
			{
				this.sequenceNo = value;
			}
		}

		public int VehicleTrimId
		{
			get
			{
				return this.vehicleTrimId;
			}
			set
			{
				this.vehicleTrimId = value;
			}
		}

		public int VehicleTrimOptionId
		{
			get
			{
				return this.vehicleTrimOptionId;
			}
			set
			{
				this.vehicleTrimOptionId = value;
			}
		}

		public VehicleTrimOption()
		{
		}

		public VehicleTrimOption(string connection, string modifiedUserID, int vehicleTrimOptionId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.VehicleTrimOptionId = vehicleTrimOptionId;
			if (this.VehicleTrimOptionId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleTrimOptionId", DataAccessParameterType.Numeric, this.VehicleTrimOptionId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_TRIM_OPTION_Delete");
			this.wipeout();
		}

		public static int GetRecordId(string LookupIdConfiguratorDataSource, string DataSourceId, int vehicleTrimId, int lookupIdConfiguratorOptionGroup, string optionName, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			string[] str = new string[] { "SELECT VEHICLE_OPTION_TRIM_ID FROM VEHICLE_OPTION_TRIM OPT INNER JOIN VEHICLE_TRIM TRIM ON TRIM.VEHICLE_TRIM_ID = OPT.VEHICLE_TRIM_ID  WHERE TRIM.LOOKUP_ID_ConfiguratorDataSource = ", LookupIdConfiguratorDataSource.ToString(), " AND OPT.VEHICLE_TRIM_ID = ", vehicleTrimId.ToString(), " AND OPTION.OptionName = '", optionName, "' AND  LOOKUP_ID_OptionGroup = ", lookupIdConfiguratorOptionGroup.ToString() };
			data.ExecuteStatement(string.Concat(str));
			num = (!data.EOF ? int.Parse(data["VEHICLE_TRIM_OPTION_ID"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleTrimOptionId", DataAccessParameterType.Numeric, this.VehicleTrimOptionId.ToString());
			data.ExecuteProcedure("VEHICLE_TRIM_OPTION_GetRecord");
			if (!data.EOF)
			{
				this.VehicleTrimOptionId = int.Parse(data["VehicleTrimOptionId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.VehicleTrimId = int.Parse(data["VehicleTrimId"]);
				this.LookupId_OptionGroup = int.Parse(data["LookupId_OptionGroup"]);
				this.OptionName = data["OptionName"];
				this.OptionNotes = data["OptionNotes"];
				this.PriceMsrp = float.Parse(data["PriceMsrp"]);
				this.PriceInvoice = float.Parse(data["PriceInvoice"]);
				this.PriceMarket = float.Parse(data["PriceMarket"]);
				this.PriceMarketFormula = data["PriceMarketFormula"];
				this.SequenceNo = int.Parse(data["SequenceNo"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int vehicleTrimOptionId = this.VehicleTrimOptionId;
			data.AddParam("@VehicleTrimOptionId", DataAccessParameterType.Numeric, vehicleTrimOptionId.ToString());
			vehicleTrimOptionId = this.VehicleTrimId;
			data.AddParam("@VehicleTrimId", DataAccessParameterType.Numeric, vehicleTrimOptionId.ToString());
			vehicleTrimOptionId = this.LookupId_OptionGroup;
			data.AddParam("@LookupId_OptionGroup", DataAccessParameterType.Numeric, vehicleTrimOptionId.ToString());
			data.AddParam("@OptionName", DataAccessParameterType.Text, this.OptionName);
			data.AddParam("@OptionNotes", DataAccessParameterType.Text, this.OptionNotes);
			float priceMsrp = this.PriceMsrp;
			data.AddParam("@PriceMsrp", DataAccessParameterType.Numeric, priceMsrp.ToString());
			priceMsrp = this.PriceInvoice;
			data.AddParam("@PriceInvoice", DataAccessParameterType.Numeric, priceMsrp.ToString());
			priceMsrp = this.PriceMarket;
			data.AddParam("@PriceMarket", DataAccessParameterType.Numeric, priceMsrp.ToString());
			data.AddParam("@PriceMarketFormula", DataAccessParameterType.Text, this.PriceMarketFormula);
			vehicleTrimOptionId = this.SequenceNo;
			data.AddParam("@SequenceNo", DataAccessParameterType.Numeric, vehicleTrimOptionId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_TRIM_OPTION_InsertUpdate");
			if (!data.EOF)
			{
				this.VehicleTrimOptionId = int.Parse(data["VehicleTrimOptionId"]);
			}
			this.retrievedata();
		}
	}
}