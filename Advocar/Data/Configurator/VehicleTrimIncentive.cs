using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Configurator
{
	public class VehicleTrimIncentive : Transaction
	{
		private int vehicleTrimIncentiveId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int vehicleTrimId = 0;

		private string dataSourceId = "";

		private string incentive = "";

		private string incentiveNotes = "";

		private string startDate = "";

		private string endDate = "";

		private float priceMsrp = 0f;

		private float priceInvoice = 0f;

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string DataSourceId
		{
			get
			{
				return this.dataSourceId;
			}
			set
			{
				this.dataSourceId = value;
			}
		}

		public string EndDate
		{
			get
			{
				return this.endDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.endDate = "";
				}
				else
				{
					this.endDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string Incentive
		{
			get
			{
				return this.incentive;
			}
			set
			{
				this.incentive = value;
			}
		}

		public string IncentiveNotes
		{
			get
			{
				return this.incentiveNotes;
			}
			set
			{
				this.incentiveNotes = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public float PriceInvoice
		{
			get
			{
				return this.priceInvoice;
			}
			set
			{
				this.priceInvoice = value;
			}
		}

		public float PriceMsrp
		{
			get
			{
				return this.priceMsrp;
			}
			set
			{
				this.priceMsrp = value;
			}
		}

		public string StartDate
		{
			get
			{
				return this.startDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.startDate = "";
				}
				else
				{
					this.startDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public int VehicleTrimId
		{
			get
			{
				return this.vehicleTrimId;
			}
			set
			{
				this.vehicleTrimId = value;
			}
		}

		public int VehicleTrimIncentiveId
		{
			get
			{
				return this.vehicleTrimIncentiveId;
			}
			set
			{
				this.vehicleTrimIncentiveId = value;
			}
		}

		public VehicleTrimIncentive()
		{
		}

		public VehicleTrimIncentive(string connection, string modifiedUserID, int vehicleTrimIncentiveId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.VehicleTrimIncentiveId = vehicleTrimIncentiveId;
			if (this.VehicleTrimIncentiveId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleTrimIncentiveId", DataAccessParameterType.Numeric, this.VehicleTrimIncentiveId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_TRIM_INCENTIVE_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleTrimIncentiveId", DataAccessParameterType.Numeric, this.VehicleTrimIncentiveId.ToString());
			data.ExecuteProcedure("VEHICLE_TRIM_INCENTIVE_GetRecord");
			if (!data.EOF)
			{
				this.VehicleTrimIncentiveId = int.Parse(data["VehicleTrimIncentiveId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.VehicleTrimId = int.Parse(data["VehicleTrimId"]);
				this.DataSourceId = data["DataSourceId"];
				this.Incentive = data["Incentive"];
				this.IncentiveNotes = data["IncentiveNotes"];
				this.StartDate = data["StartDate"];
				this.EndDate = data["EndDate"];
				this.PriceMsrp = float.Parse(data["PriceMsrp"]);
				this.PriceInvoice = float.Parse(data["PriceInvoice"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int vehicleTrimIncentiveId = this.VehicleTrimIncentiveId;
			data.AddParam("@VehicleTrimIncentiveId", DataAccessParameterType.Numeric, vehicleTrimIncentiveId.ToString());
			vehicleTrimIncentiveId = this.VehicleTrimId;
			data.AddParam("@VehicleTrimId", DataAccessParameterType.Numeric, vehicleTrimIncentiveId.ToString());
			data.AddParam("@DataSourceId", DataAccessParameterType.Text, this.DataSourceId);
			data.AddParam("@Incentive", DataAccessParameterType.Text, this.Incentive);
			data.AddParam("@IncentiveNotes", DataAccessParameterType.Text, this.IncentiveNotes);
			data.AddParam("@StartDate", DataAccessParameterType.DateTime, this.StartDate);
			data.AddParam("@EndDate", DataAccessParameterType.DateTime, this.EndDate);
			float priceMsrp = this.PriceMsrp;
			data.AddParam("@PriceMsrp", DataAccessParameterType.Numeric, priceMsrp.ToString());
			priceMsrp = this.PriceInvoice;
			data.AddParam("@PriceInvoice", DataAccessParameterType.Numeric, priceMsrp.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_TRIM_INCENTIVE_InsertUpdate");
			if (!data.EOF)
			{
				this.VehicleTrimIncentiveId = int.Parse(data["VehicleTrimIncentiveId"]);
			}
			this.retrievedata();
		}
	}
}