using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Configurator
{
	public class VehicleTrimImage : Transaction
	{
		private int vehicleTrimImageId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int vehicleTrimId = 0;

		private int lookupId_PictureType = 0;

		private string dataSourceId = "";

		private string imageCaption = "";

		private string imageFileName = "";

		private string fullsizeLocation = "";

		private string thumbnailLocation = "";

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string DataSourceId
		{
			get
			{
				return this.dataSourceId;
			}
			set
			{
				this.dataSourceId = value;
			}
		}

		public string FullsizeLocation
		{
			get
			{
				return this.fullsizeLocation;
			}
			set
			{
				this.fullsizeLocation = value;
			}
		}

		public string ImageCaption
		{
			get
			{
				return this.imageCaption;
			}
			set
			{
				this.imageCaption = value;
			}
		}

		public string ImageFileName
		{
			get
			{
				return this.imageFileName;
			}
			set
			{
				this.imageFileName = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public int LookupId_PictureType
		{
			get
			{
				return this.lookupId_PictureType;
			}
			set
			{
				this.lookupId_PictureType = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public string ThumbnailLocation
		{
			get
			{
				return this.thumbnailLocation;
			}
			set
			{
				this.thumbnailLocation = value;
			}
		}

		public int VehicleTrimId
		{
			get
			{
				return this.vehicleTrimId;
			}
			set
			{
				this.vehicleTrimId = value;
			}
		}

		public int VehicleTrimImageId
		{
			get
			{
				return this.vehicleTrimImageId;
			}
			set
			{
				this.vehicleTrimImageId = value;
			}
		}

		public VehicleTrimImage()
		{
		}

		public VehicleTrimImage(string connection, string modifiedUserID, int vehicleTrimImageId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.VehicleTrimImageId = vehicleTrimImageId;
			if (this.VehicleTrimImageId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleTrimImageId", DataAccessParameterType.Numeric, this.VehicleTrimImageId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_TRIM_IMAGE_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleTrimImageId", DataAccessParameterType.Numeric, this.VehicleTrimImageId.ToString());
			data.ExecuteProcedure("VEHICLE_TRIM_IMAGE_GetRecord");
			if (!data.EOF)
			{
				this.VehicleTrimImageId = int.Parse(data["VehicleTrimImageId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.VehicleTrimId = int.Parse(data["VehicleTrimId"]);
				this.LookupId_PictureType = int.Parse(data["LookupId_PictureType"]);
				this.DataSourceId = data["DataSourceId"];
				this.ImageCaption = data["ImageCaption"];
				this.ImageFileName = data["ImageFileName"];
				this.FullsizeLocation = data["FullsizeLocation"];
				this.ThumbnailLocation = data["ThumbnailLocation"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int vehicleTrimImageId = this.VehicleTrimImageId;
			data.AddParam("@VehicleTrimImageId", DataAccessParameterType.Numeric, vehicleTrimImageId.ToString());
			vehicleTrimImageId = this.VehicleTrimId;
			data.AddParam("@VehicleTrimId", DataAccessParameterType.Numeric, vehicleTrimImageId.ToString());
			vehicleTrimImageId = this.LookupId_PictureType;
			data.AddParam("@LookupId_PictureType", DataAccessParameterType.Numeric, vehicleTrimImageId.ToString());
			data.AddParam("@DataSourceId", DataAccessParameterType.Text, this.DataSourceId);
			data.AddParam("@ImageCaption", DataAccessParameterType.Text, this.ImageCaption);
			data.AddParam("@ImageFileName", DataAccessParameterType.Text, this.ImageFileName);
			data.AddParam("@FullsizeLocation", DataAccessParameterType.Text, this.FullsizeLocation);
			data.AddParam("@ThumbnailLocation", DataAccessParameterType.Text, this.ThumbnailLocation);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_TRIM_IMAGE_InsertUpdate");
			if (!data.EOF)
			{
				this.VehicleTrimImageId = int.Parse(data["VehicleTrimImageId"]);
			}
			this.retrievedata();
		}
	}
}