using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Configurator
{
	public class VehicleTrimFeature : Transaction
	{
		private int vehicleTrimFeatureId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int vehicleTrimId = 0;

		private string dataSourceId = "";

		private int lookupId_FeatureCategory = 0;

		private string feature = "";

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string DataSourceId
		{
			get
			{
				return this.dataSourceId;
			}
			set
			{
				this.dataSourceId = value;
			}
		}

		public string Feature
		{
			get
			{
				return this.feature;
			}
			set
			{
				this.feature = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public int LookupId_FeatureCategory
		{
			get
			{
				return this.lookupId_FeatureCategory;
			}
			set
			{
				this.lookupId_FeatureCategory = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public int VehicleTrimFeatureId
		{
			get
			{
				return this.vehicleTrimFeatureId;
			}
			set
			{
				this.vehicleTrimFeatureId = value;
			}
		}

		public int VehicleTrimId
		{
			get
			{
				return this.vehicleTrimId;
			}
			set
			{
				this.vehicleTrimId = value;
			}
		}

		public VehicleTrimFeature()
		{
		}

		public VehicleTrimFeature(string connection, string modifiedUserID, int vehicleTrimFeatureId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.VehicleTrimFeatureId = vehicleTrimFeatureId;
			if (this.VehicleTrimFeatureId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleTrimFeatureId", DataAccessParameterType.Numeric, this.VehicleTrimFeatureId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_TRIM_FEATURE_Delete");
			this.wipeout();
		}

		public static int GetRecordId(string connection, int lookupId_ConfiguratorSource, string configuratorSourceID, int vcmtID, int lookupIdID_FeatureCategory)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			string[] str = new string[] { "SELECT VEHICLE_TRIM_FEATURE_ID FROM VEHICLE_TRIM_FEATURE FEAT INNER JOIN VEHICLE_TRIM TRIM ON TRIM.VEHICLE_TRIM_ID = FEAT.VEHICLE_TRIM_ID  WHERE TRIM.LOOKUP_ID_ConfiguratorDataSource = ", lookupId_ConfiguratorSource.ToString(), " AND FEAT.VEHICLE_TRIM_ID = ", vcmtID.ToString(), " AND FEAT.DataSourceId = '", configuratorSourceID.Replace("'", "''"), "' AND  LOOKUP_ID_FeatureCategory = ", lookupIdID_FeatureCategory.ToString() };
			data.ExecuteStatement(string.Concat(str));
			num = (!data.EOF ? int.Parse(data["VEHICLE_TRIM_FEATURE_ID"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleTrimFeatureId", DataAccessParameterType.Numeric, this.VehicleTrimFeatureId.ToString());
			data.ExecuteProcedure("VEHICLE_TRIM_FEATURE_GetRecord");
			if (!data.EOF)
			{
				this.VehicleTrimFeatureId = int.Parse(data["VehicleTrimFeatureId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.VehicleTrimId = int.Parse(data["VehicleTrimId"]);
				this.DataSourceId = data["DataSourceId"];
				this.LookupId_FeatureCategory = int.Parse(data["LookupId_FeatureCategory"]);
				this.Feature = data["Feature"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int vehicleTrimFeatureId = this.VehicleTrimFeatureId;
			data.AddParam("@VehicleTrimFeatureId", DataAccessParameterType.Numeric, vehicleTrimFeatureId.ToString());
			vehicleTrimFeatureId = this.VehicleTrimId;
			data.AddParam("@VehicleTrimId", DataAccessParameterType.Numeric, vehicleTrimFeatureId.ToString());
			data.AddParam("@DataSourceId", DataAccessParameterType.Text, this.DataSourceId);
			vehicleTrimFeatureId = this.LookupId_FeatureCategory;
			data.AddParam("@LookupId_FeatureCategory", DataAccessParameterType.Numeric, vehicleTrimFeatureId.ToString());
			data.AddParam("@Feature", DataAccessParameterType.Text, this.Feature);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_TRIM_FEATURE_InsertUpdate");
			if (!data.EOF)
			{
				this.VehicleTrimFeatureId = int.Parse(data["VehicleTrimFeatureId"]);
			}
			this.retrievedata();
		}
	}
}