using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Configurator
{
	public class VehicleTrimReview : Transaction
	{
		private int vehicleTrimReviewId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int vehicleTrimId = 0;

		private string dataSourceId = "";

		private string reviewName = "";

		private string reviewNotes = "";

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string DataSourceId
		{
			get
			{
				return this.dataSourceId;
			}
			set
			{
				this.dataSourceId = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public string ReviewName
		{
			get
			{
				return this.reviewName;
			}
			set
			{
				this.reviewName = value;
			}
		}

		public string ReviewNotes
		{
			get
			{
				return this.reviewNotes;
			}
			set
			{
				this.reviewNotes = value;
			}
		}

		public int VehicleTrimId
		{
			get
			{
				return this.vehicleTrimId;
			}
			set
			{
				this.vehicleTrimId = value;
			}
		}

		public int VehicleTrimReviewId
		{
			get
			{
				return this.vehicleTrimReviewId;
			}
			set
			{
				this.vehicleTrimReviewId = value;
			}
		}

		public VehicleTrimReview()
		{
		}

		public VehicleTrimReview(string connection, string modifiedUserID, int vehicleTrimReviewId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.VehicleTrimReviewId = vehicleTrimReviewId;
			if (this.VehicleTrimReviewId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleTrimReviewId", DataAccessParameterType.Numeric, this.VehicleTrimReviewId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_TRIM_REVIEW_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleTrimReviewId", DataAccessParameterType.Numeric, this.VehicleTrimReviewId.ToString());
			data.ExecuteProcedure("VEHICLE_TRIM_REVIEW_GetRecord");
			if (!data.EOF)
			{
				this.VehicleTrimReviewId = int.Parse(data["VehicleTrimReviewId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.VehicleTrimId = int.Parse(data["VehicleTrimId"]);
				this.DataSourceId = data["DataSourceId"];
				this.ReviewName = data["ReviewName"];
				this.ReviewNotes = data["ReviewNotes"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int vehicleTrimReviewId = this.VehicleTrimReviewId;
			data.AddParam("@VehicleTrimReviewId", DataAccessParameterType.Numeric, vehicleTrimReviewId.ToString());
			vehicleTrimReviewId = this.VehicleTrimId;
			data.AddParam("@VehicleTrimId", DataAccessParameterType.Numeric, vehicleTrimReviewId.ToString());
			data.AddParam("@DataSourceId", DataAccessParameterType.Text, this.DataSourceId);
			data.AddParam("@ReviewName", DataAccessParameterType.Text, this.ReviewName);
			data.AddParam("@ReviewNotes", DataAccessParameterType.Text, this.ReviewNotes);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_TRIM_REVIEW_InsertUpdate");
			if (!data.EOF)
			{
				this.VehicleTrimReviewId = int.Parse(data["VehicleTrimReviewId"]);
			}
			this.retrievedata();
		}
	}
}