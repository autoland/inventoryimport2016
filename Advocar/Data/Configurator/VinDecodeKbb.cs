using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Configurator
{
	public class VinDecodeKbb : Transaction
	{
		private int vinDecodeKbbId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int vehicleYearId = 0;

		private int vehicleMakeId = 0;

		private int vehicleModelId = 0;

		private int vehicleTrimId = 0;

		private string vinPattern = "";

		private string kbbModelCode = "";

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public string KbbModelCode
		{
			get
			{
				return this.kbbModelCode;
			}
			set
			{
				this.kbbModelCode = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public int VehicleMakeId
		{
			get
			{
				return this.vehicleMakeId;
			}
			set
			{
				this.vehicleMakeId = value;
			}
		}

		public int VehicleModelId
		{
			get
			{
				return this.vehicleModelId;
			}
			set
			{
				this.vehicleModelId = value;
			}
		}

		public int VehicleTrimId
		{
			get
			{
				return this.vehicleTrimId;
			}
			set
			{
				this.vehicleTrimId = value;
			}
		}

		public int VehicleYearId
		{
			get
			{
				return this.vehicleYearId;
			}
			set
			{
				this.vehicleYearId = value;
			}
		}

		public int VinDecodeKbbId
		{
			get
			{
				return this.vinDecodeKbbId;
			}
			set
			{
				this.vinDecodeKbbId = value;
			}
		}

		public string VinPattern
		{
			get
			{
				return this.vinPattern;
			}
			set
			{
				this.vinPattern = value;
			}
		}

		public VinDecodeKbb()
		{
		}

		public VinDecodeKbb(string connection, string modifiedUserID, int vinDecodeKbbId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.VinDecodeKbbId = vinDecodeKbbId;
			if (this.VinDecodeKbbId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VinDecodeKbbId", DataAccessParameterType.Numeric, this.VinDecodeKbbId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VIN_DECODE_KBB_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VinDecodeKbbId", DataAccessParameterType.Numeric, this.VinDecodeKbbId.ToString());
			data.ExecuteProcedure("VIN_DECODE_KBB_GetRecord");
			if (!data.EOF)
			{
				this.VinDecodeKbbId = int.Parse(data["VinDecodeKbbId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.VehicleYearId = int.Parse(data["VehicleYearId"]);
				this.VehicleMakeId = int.Parse(data["VehicleMakeId"]);
				this.VehicleModelId = int.Parse(data["VehicleModelId"]);
				this.VehicleTrimId = int.Parse(data["VehicleTrimId"]);
				this.VinPattern = data["VinPattern"];
				this.KbbModelCode = data["KbbModelCode"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int vinDecodeKbbId = this.VinDecodeKbbId;
			data.AddParam("@VinDecodeKbbId", DataAccessParameterType.Numeric, vinDecodeKbbId.ToString());
			vinDecodeKbbId = this.VehicleYearId;
			data.AddParam("@VehicleYearId", DataAccessParameterType.Numeric, vinDecodeKbbId.ToString());
			vinDecodeKbbId = this.VehicleMakeId;
			data.AddParam("@VehicleMakeId", DataAccessParameterType.Numeric, vinDecodeKbbId.ToString());
			vinDecodeKbbId = this.VehicleModelId;
			data.AddParam("@VehicleModelId", DataAccessParameterType.Numeric, vinDecodeKbbId.ToString());
			vinDecodeKbbId = this.VehicleTrimId;
			data.AddParam("@VehicleTrimId", DataAccessParameterType.Numeric, vinDecodeKbbId.ToString());
			data.AddParam("@VinPattern", DataAccessParameterType.Text, this.VinPattern);
			data.AddParam("@KbbModelCode", DataAccessParameterType.Text, this.KbbModelCode);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VIN_DECODE_KBB_InsertUpdate");
			if (!data.EOF)
			{
				this.VinDecodeKbbId = int.Parse(data["VinDecodeKbbId"]);
			}
			this.retrievedata();
		}
	}
}