using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Configurator
{
	public class VehicleTrimSpecification : Transaction
	{
		private int vehicleTrimSpecificationId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int vehicleTrimId = 0;

		private string dataSourceId = "";

		private string specification = "";

		private string specificationValue = "";

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string DataSourceId
		{
			get
			{
				return this.dataSourceId;
			}
			set
			{
				this.dataSourceId = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public string Specification
		{
			get
			{
				return this.specification;
			}
			set
			{
				this.specification = value;
			}
		}

		public string SpecificationValue
		{
			get
			{
				return this.specificationValue;
			}
			set
			{
				this.specificationValue = value;
			}
		}

		public int VehicleTrimId
		{
			get
			{
				return this.vehicleTrimId;
			}
			set
			{
				this.vehicleTrimId = value;
			}
		}

		public int VehicleTrimSpecificationId
		{
			get
			{
				return this.vehicleTrimSpecificationId;
			}
			set
			{
				this.vehicleTrimSpecificationId = value;
			}
		}

		public VehicleTrimSpecification()
		{
		}

		public VehicleTrimSpecification(string connection, string modifiedUserID, int vehicleTrimSpecificationId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.VehicleTrimSpecificationId = vehicleTrimSpecificationId;
			if (this.VehicleTrimSpecificationId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleTrimSpecificationId", DataAccessParameterType.Numeric, this.VehicleTrimSpecificationId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_TRIM_SPECIFICATION_Delete");
			this.wipeout();
		}

		public static int GetRecordId(string connection, int lookupID_ConfiguratorSource, string configuratorSourceID, int vcmtID)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			string[] str = new string[] { "SELECT VEHICLE_TRIM_SPECIFICATION_ID FROM VEHICLE_TRIM_SPECIFICATION SPEC INNER JOIN VEHICLE_TRIM TRIM ON TRIM.VEHICLE_TRIM_ID = SPEC.VEHICLE_TRIM_ID WHERE TRIM.LOOKUP_ID_ConfiguratorDataSource = ", lookupID_ConfiguratorSource.ToString(), " AND SPEC.VEHICLE_TRIM_ID = ", vcmtID.ToString(), " AND SPEC.DataSourceId = '", configuratorSourceID, "'" };
			data.ExecuteStatement(string.Concat(str));
			num = (!data.EOF ? int.Parse(data["VEHICLE_TRIM_SPECIFICATION_ID"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleTrimSpecificationId", DataAccessParameterType.Numeric, this.VehicleTrimSpecificationId.ToString());
			data.ExecuteProcedure("VEHICLE_TRIM_SPECIFICATION_GetRecord");
			if (!data.EOF)
			{
				this.VehicleTrimSpecificationId = int.Parse(data["VehicleTrimSpecificationId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.VehicleTrimId = int.Parse(data["VehicleTrimId"]);
				this.DataSourceId = data["DataSourceId"];
				this.Specification = data["Specification"];
				this.SpecificationValue = data["SpecificationValue"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int vehicleTrimSpecificationId = this.VehicleTrimSpecificationId;
			data.AddParam("@VehicleTrimSpecificationId", DataAccessParameterType.Numeric, vehicleTrimSpecificationId.ToString());
			vehicleTrimSpecificationId = this.VehicleTrimId;
			data.AddParam("@VehicleTrimId", DataAccessParameterType.Numeric, vehicleTrimSpecificationId.ToString());
			data.AddParam("@DataSourceId", DataAccessParameterType.Text, this.DataSourceId);
			data.AddParam("@Specification", DataAccessParameterType.Text, this.Specification);
			data.AddParam("@SpecificationValue", DataAccessParameterType.Text, this.SpecificationValue);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_TRIM_SPECIFICATION_InsertUpdate");
			if (!data.EOF)
			{
				this.VehicleTrimSpecificationId = int.Parse(data["VehicleTrimSpecificationId"]);
			}
			this.retrievedata();
		}
	}
}