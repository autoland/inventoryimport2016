using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Cudl
{
	public class CreditUnion : Transaction
	{
		private int creditUnionId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private string cudlCode = "";

		private string creditUnionName = "";

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public int CreditUnionId
		{
			get
			{
				return this.creditUnionId;
			}
			set
			{
				this.creditUnionId = value;
			}
		}

		public string CreditUnionName
		{
			get
			{
				return this.creditUnionName;
			}
			set
			{
				this.creditUnionName = value;
			}
		}

		public string CudlCode
		{
			get
			{
				return this.cudlCode;
			}
			set
			{
				this.cudlCode = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public CreditUnion()
		{
		}

		public CreditUnion(string connection, string modifiedUserID, int creditUnionId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "CREDIT_UNION";
			this.CreditUnionId = creditUnionId;
			if (this.CreditUnionId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@CreditUnionId", DataAccessParameterType.Numeric, this.CreditUnionId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("CREDIT_UNION_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@CreditUnionId", DataAccessParameterType.Numeric, this.CreditUnionId.ToString());
			data.ExecuteProcedure("CREDIT_UNION_GetRecord");
			if (!data.EOF)
			{
				this.CreditUnionId = int.Parse(data["CreditUnionId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.CudlCode = data["CudlCode"];
				this.CreditUnionName = data["CreditUnionName"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@CreditUnionId", DataAccessParameterType.Numeric, this.CreditUnionId.ToString());
			data.AddParam("@CudlCode", DataAccessParameterType.Text, this.CudlCode);
			data.AddParam("@CreditUnionName", DataAccessParameterType.Text, this.CreditUnionName);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("CREDIT_UNION_InsertUpdate");
			if (!data.EOF)
			{
				this.CreditUnionId = int.Parse(data["CreditUnionId"]);
			}
			this.retrievedata();
		}
	}
}