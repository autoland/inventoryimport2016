using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Cudl
{
	public class NoteLog : Transaction
	{
		private int noteLogId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int noteId = 0;

		private string notes = "";

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public int NoteId
		{
			get
			{
				return this.noteId;
			}
			set
			{
				this.noteId = value;
			}
		}

		public int NoteLogId
		{
			get
			{
				return this.noteLogId;
			}
			set
			{
				this.noteLogId = value;
			}
		}

		public string Notes
		{
			get
			{
				return this.notes;
			}
			set
			{
				this.notes = value;
			}
		}

		public NoteLog()
		{
		}

		public NoteLog(string connection, string modifiedUserID, int noteLogId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "NOTE_LOG";
			this.NoteLogId = noteLogId;
			if (this.NoteLogId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@NoteLogId", DataAccessParameterType.Numeric, this.NoteLogId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("NOTE_LOG_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@NoteLogId", DataAccessParameterType.Numeric, this.NoteLogId.ToString());
			data.ExecuteProcedure("NOTE_LOG_GetRecord");
			if (!data.EOF)
			{
				this.NoteLogId = int.Parse(data["NoteLogId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.NoteId = int.Parse(data["NoteId"]);
				this.Notes = data["Notes"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int noteLogId = this.NoteLogId;
			data.AddParam("@NoteLogId", DataAccessParameterType.Numeric, noteLogId.ToString());
			noteLogId = this.NoteId;
			data.AddParam("@NoteId", DataAccessParameterType.Numeric, noteLogId.ToString());
			data.AddParam("@Notes", DataAccessParameterType.Text, this.Notes);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("NOTE_LOG_InsertUpdate");
			if (!data.EOF)
			{
				this.NoteLogId = int.Parse(data["NoteLogId"]);
			}
			this.retrievedata();
		}
	}
}