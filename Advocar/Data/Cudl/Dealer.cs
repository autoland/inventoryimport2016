using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Cudl
{
	public class Dealer : Transaction
	{
		private int dealerId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private string cudlCode = "";

		private string dealerName = "";

		private string address = "";

		private string city = "";

		private string stateCode = "";

		private string zipCode = "";

		private int lookupId_DealerType = 0;

		public string Address
		{
			get
			{
				return this.address;
			}
			set
			{
				this.address = value;
			}
		}

		public string City
		{
			get
			{
				return this.city;
			}
			set
			{
				this.city = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string CudlCode
		{
			get
			{
				return this.cudlCode;
			}
			set
			{
				this.cudlCode = value;
			}
		}

		public int DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public string DealerName
		{
			get
			{
				return this.dealerName;
			}
			set
			{
				this.dealerName = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public int LookupId_DealerType
		{
			get
			{
				return this.lookupId_DealerType;
			}
			set
			{
				this.lookupId_DealerType = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public string StateCode
		{
			get
			{
				return this.stateCode;
			}
			set
			{
				this.stateCode = value;
			}
		}

		public string ZipCode
		{
			get
			{
				return this.zipCode;
			}
			set
			{
				this.zipCode = value;
			}
		}

		public Dealer()
		{
		}

		public Dealer(string connection, string modifiedUserID, int dealerId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "DEALER";
			this.DealerId = dealerId;
			if (this.DealerId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@DealerId", DataAccessParameterType.Numeric, this.DealerId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("DEALER_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@DealerId", DataAccessParameterType.Numeric, this.DealerId.ToString());
			data.ExecuteProcedure("DEALER_GetRecord");
			if (!data.EOF)
			{
				this.DealerId = int.Parse(data["DealerId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.CudlCode = data["CudlCode"];
				this.DealerName = data["DealerName"];
				this.Address = data["Address"];
				this.City = data["City"];
				this.StateCode = data["StateCode"];
				this.ZipCode = data["ZipCode"];
				this.LookupId_DealerType = int.Parse(data["LookupId_DealerType"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int dealerId = this.DealerId;
			data.AddParam("@DealerId", DataAccessParameterType.Numeric, dealerId.ToString());
			data.AddParam("@CudlCode", DataAccessParameterType.Text, this.CudlCode);
			data.AddParam("@DealerName", DataAccessParameterType.Text, this.DealerName);
			data.AddParam("@Address", DataAccessParameterType.Text, this.Address);
			data.AddParam("@City", DataAccessParameterType.Text, this.City);
			data.AddParam("@StateCode", DataAccessParameterType.Text, this.StateCode);
			data.AddParam("@ZipCode", DataAccessParameterType.Text, this.ZipCode);
			dealerId = this.LookupId_DealerType;
			data.AddParam("@LookupId_DealerType", DataAccessParameterType.Numeric, dealerId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("DEALER_InsertUpdate");
			if (!data.EOF)
			{
				this.DealerId = int.Parse(data["DealerId"]);
			}
			this.retrievedata();
		}
	}
}