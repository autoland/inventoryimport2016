using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Cudl
{
	public class Lead : Transaction
	{
		private int leadId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int creditUnionId = 0;

		private string firstName = "";

		private string lastName = "";

		private string phone = "";

		private string phoneOther = "";

		private string email = "";

		private string city = "";

		private string stateCode = "";

		private string zipCode = "";

		private int lookupId_ReferralSource = 0;

		private string newOrUsed = "NEW";

		private string makeName = "";

		private string modelName = "";

		private int dealerId = 0;

		private int lookupId_LeadStatus = 0;

		private bool isCallback = false;

		private string callBackDate = "";

		private bool isSurveyRequired = false;

		private int lookupId_SurveyStatus = 0;

		private int lookupId_CallBackTime = 0;

		private int noteId = 0;

		public string CallBackDate
		{
			get
			{
				return this.callBackDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.callBackDate = "";
				}
				else
				{
					this.callBackDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string City
		{
			get
			{
				return this.city;
			}
			set
			{
				this.city = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public int CreditUnionId
		{
			get
			{
				return this.creditUnionId;
			}
			set
			{
				this.creditUnionId = value;
			}
		}

		public int DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public string Email
		{
			get
			{
				return this.email;
			}
			set
			{
				this.email = value;
			}
		}

		public string FirstName
		{
			get
			{
				return this.firstName;
			}
			set
			{
				this.firstName = value;
			}
		}

		public bool IsCallback
		{
			get
			{
				return this.isCallback;
			}
			set
			{
				this.isCallback = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public bool IsSurveyRequired
		{
			get
			{
				return this.isSurveyRequired;
			}
			set
			{
				this.isSurveyRequired = value;
			}
		}

		public string LastName
		{
			get
			{
				return this.lastName;
			}
			set
			{
				this.lastName = value;
			}
		}

		public int LeadId
		{
			get
			{
				return this.leadId;
			}
			set
			{
				this.leadId = value;
			}
		}

		public int LookupId_CallBackTime
		{
			get
			{
				return this.lookupId_CallBackTime;
			}
			set
			{
				this.lookupId_CallBackTime = value;
			}
		}

		public int LookupId_LeadStatus
		{
			get
			{
				return this.lookupId_LeadStatus;
			}
			set
			{
				this.lookupId_LeadStatus = value;
			}
		}

		public int LookupId_ReferralSource
		{
			get
			{
				return this.lookupId_ReferralSource;
			}
			set
			{
				this.lookupId_ReferralSource = value;
			}
		}

		public int LookupId_SurveyStatus
		{
			get
			{
				return this.lookupId_SurveyStatus;
			}
			set
			{
				this.lookupId_SurveyStatus = value;
			}
		}

		public string MakeName
		{
			get
			{
				return this.makeName;
			}
			set
			{
				this.makeName = value;
			}
		}

		public string ModelName
		{
			get
			{
				return this.modelName;
			}
			set
			{
				this.modelName = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public string NewOrUsed
		{
			get
			{
				return this.newOrUsed;
			}
			set
			{
				this.newOrUsed = value;
			}
		}

		public int NoteId
		{
			get
			{
				return this.noteId;
			}
			set
			{
				this.noteId = value;
			}
		}

		public string Phone
		{
			get
			{
				return this.phone;
			}
			set
			{
				this.phone = value;
			}
		}

		public string PhoneOther
		{
			get
			{
				return this.phoneOther;
			}
			set
			{
				this.phoneOther = value;
			}
		}

		public string StateCode
		{
			get
			{
				return this.stateCode;
			}
			set
			{
				this.stateCode = value;
			}
		}

		public string ZipCode
		{
			get
			{
				return this.zipCode;
			}
			set
			{
				this.zipCode = value;
			}
		}

		public Lead()
		{
		}

		public Lead(string connection, string modifiedUserID, int leadId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "LEAD";
			this.LeadId = leadId;
			if (this.LeadId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@LeadId", DataAccessParameterType.Numeric, this.LeadId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("LEAD_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@LeadId", DataAccessParameterType.Numeric, this.LeadId.ToString());
			data.ExecuteProcedure("LEAD_GetRecord");
			if (!data.EOF)
			{
				this.LeadId = int.Parse(data["LeadId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.CreditUnionId = int.Parse(data["CreditUnionId"]);
				this.FirstName = data["FirstName"];
				this.LastName = data["LastName"];
				this.Phone = data["Phone"];
				this.PhoneOther = data["PhoneOther"];
				this.Email = data["Email"];
				this.City = data["City"];
				this.StateCode = data["StateCode"];
				this.ZipCode = data["ZipCode"];
				this.LookupId_ReferralSource = int.Parse(data["LookupId_ReferralSource"]);
				this.NewOrUsed = data["NewOrUsed"];
				this.MakeName = data["MakeName"];
				this.ModelName = data["ModelName"];
				this.DealerId = int.Parse(data["DealerId"]);
				this.LookupId_LeadStatus = int.Parse(data["LookupId_LeadStatus"]);
				this.IsCallback = bool.Parse(data["IsCallback"]);
				this.CallBackDate = data["CallBackDate"];
				this.IsSurveyRequired = bool.Parse(data["IsSurveyRequired"]);
				this.LookupId_SurveyStatus = int.Parse(data["LookupId_SurveyStatus"]);
				this.LookupId_CallBackTime = int.Parse(data["LookupId_CallBackTime"]);
				this.NoteId = int.Parse(data["NoteId"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int leadId = this.LeadId;
			data.AddParam("@LeadId", DataAccessParameterType.Numeric, leadId.ToString());
			leadId = this.CreditUnionId;
			data.AddParam("@CreditUnionId", DataAccessParameterType.Numeric, leadId.ToString());
			data.AddParam("@FirstName", DataAccessParameterType.Text, this.FirstName);
			data.AddParam("@LastName", DataAccessParameterType.Text, this.LastName);
			data.AddParam("@Phone", DataAccessParameterType.Text, this.Phone);
			data.AddParam("@PhoneOther", DataAccessParameterType.Text, this.PhoneOther);
			data.AddParam("@Email", DataAccessParameterType.Text, this.Email);
			data.AddParam("@City", DataAccessParameterType.Text, this.City);
			data.AddParam("@StateCode", DataAccessParameterType.Text, this.StateCode);
			data.AddParam("@ZipCode", DataAccessParameterType.Text, this.ZipCode);
			leadId = this.LookupId_ReferralSource;
			data.AddParam("@LookupId_ReferralSource", DataAccessParameterType.Numeric, leadId.ToString());
			data.AddParam("@NewOrUsed", DataAccessParameterType.Text, this.NewOrUsed);
			data.AddParam("@MakeName", DataAccessParameterType.Text, this.MakeName);
			data.AddParam("@ModelName", DataAccessParameterType.Text, this.ModelName);
			leadId = this.DealerId;
			data.AddParam("@DealerId", DataAccessParameterType.Numeric, leadId.ToString());
			leadId = this.LookupId_LeadStatus;
			data.AddParam("@LookupId_LeadStatus", DataAccessParameterType.Numeric, leadId.ToString());
			bool isCallback = this.IsCallback;
			data.AddParam("@IsCallback", DataAccessParameterType.Bool, isCallback.ToString());
			data.AddParam("@CallBackDate", DataAccessParameterType.DateTime, this.CallBackDate);
			isCallback = this.IsSurveyRequired;
			data.AddParam("@IsSurveyRequired", DataAccessParameterType.Bool, isCallback.ToString());
			leadId = this.LookupId_SurveyStatus;
			data.AddParam("@LookupId_SurveyStatus", DataAccessParameterType.Numeric, leadId.ToString());
			leadId = this.LookupId_CallBackTime;
			data.AddParam("@LookupId_CallBackTime", DataAccessParameterType.Numeric, leadId.ToString());
			leadId = this.NoteId;
			data.AddParam("@NoteId", DataAccessParameterType.Numeric, leadId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("LEAD_InsertUpdate");
			if (!data.EOF)
			{
				this.LeadId = int.Parse(data["LeadId"]);
			}
			this.retrievedata();
		}
	}
}