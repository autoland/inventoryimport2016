using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.Cars
{
	public class DealerMake : Transaction
	{
		private int dlrmId = 0;

		private int dlraId = 0;

		private int configurator_VehicleMakeId = 0;

		public int Configurator_VehicleMakeId
		{
			get
			{
				return this.configurator_VehicleMakeId;
			}
			set
			{
				this.configurator_VehicleMakeId = value;
			}
		}

		public int DlraId
		{
			get
			{
				return this.dlraId;
			}
			set
			{
				this.dlraId = value;
			}
		}

		public int DlrmId
		{
			get
			{
				return this.dlrmId;
			}
			set
			{
				this.dlrmId = value;
			}
		}

		public DealerMake()
		{
		}

		public DealerMake(string connection, string modifiedUserID, int dlrmId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "DLRM_DealersMakes";
			this.DlrmId = dlrmId;
			if (this.DlrmId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@DlrmId", DataAccessParameterType.Numeric, this.DlrmId.ToString());
			data.ExecuteProcedure("DLRM_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@DlrmId", DataAccessParameterType.Numeric, this.DlrmId.ToString());
			data.ExecuteProcedure("DLRM_GetRecord");
			if (!data.EOF)
			{
				this.DlrmId = int.Parse(data["DlrmId"]);
				this.DlraId = int.Parse(data["DlraId"]);
				this.Configurator_VehicleMakeId = int.Parse(data["Configurator_VehicleMakeId"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int dlrmId = this.DlrmId;
			data.AddParam("@DlrmId", DataAccessParameterType.Numeric, dlrmId.ToString());
			dlrmId = this.DlraId;
			data.AddParam("@DlraId", DataAccessParameterType.Numeric, dlrmId.ToString());
			dlrmId = this.Configurator_VehicleMakeId;
			data.AddParam("@Configurator_VehicleMakeId", DataAccessParameterType.Numeric, dlrmId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("DLRM_InsertUpdate");
			if (!data.EOF)
			{
				this.DlrmId = int.Parse(data["DlrmId"]);
			}
			this.retrievedata();
		}
	}
}