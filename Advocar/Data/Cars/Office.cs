using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.Cars
{
	public class Office : Transaction
	{
		private int cmeoId = 0;

		private int oldcode = 0;

		private int cmedId = 0;

		private int cmerId = 0;

		private int agrbId = 0;

		private string description = "";

		private string address1 = "";

		private string address2 = "";

		private string city = "";

		private string state = "";

		private string zipcode = "";

		private string phone = "";

		private string fax = "";

		private string officeCode = "";

		private string displayLocationYN = "";

		public string Address1
		{
			get
			{
				return this.address1;
			}
			set
			{
				this.address1 = value;
			}
		}

		public string Address2
		{
			get
			{
				return this.address2;
			}
			set
			{
				this.address2 = value;
			}
		}

		public int AgrbId
		{
			get
			{
				return this.agrbId;
			}
			set
			{
				this.agrbId = value;
			}
		}

		public string City
		{
			get
			{
				return this.city;
			}
			set
			{
				this.city = value;
			}
		}

		public int CmedId
		{
			get
			{
				return this.cmedId;
			}
			set
			{
				this.cmedId = value;
			}
		}

		public int CmeoId
		{
			get
			{
				return this.cmeoId;
			}
			set
			{
				this.cmeoId = value;
			}
		}

		public int CmerId
		{
			get
			{
				return this.cmerId;
			}
			set
			{
				this.cmerId = value;
			}
		}

		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

		public string DisplayLocationYN
		{
			get
			{
				return this.displayLocationYN;
			}
			set
			{
				this.displayLocationYN = value;
			}
		}

		public string Fax
		{
			get
			{
				return this.fax;
			}
			set
			{
				this.fax = value;
			}
		}

		public string OfficeCode
		{
			get
			{
				return this.officeCode;
			}
			set
			{
				this.officeCode = value;
			}
		}

		public int Oldcode
		{
			get
			{
				return this.oldcode;
			}
			set
			{
				this.oldcode = value;
			}
		}

		public string Phone
		{
			get
			{
				return this.phone;
			}
			set
			{
				this.phone = value;
			}
		}

		public string State
		{
			get
			{
				return this.state;
			}
			set
			{
				this.state = value;
			}
		}

		public string Zipcode
		{
			get
			{
				return this.zipcode;
			}
			set
			{
				this.zipcode = value;
			}
		}

		public Office()
		{
		}

		public Office(string connection, string modifiedUserID, int CmeoId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.CmeoId = CmeoId;
			if (this.CmeoId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@CmeoId", DataAccessParameterType.Numeric, this.CmeoId.ToString());
			data.ExecuteProcedure("CMEO_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@CmeoId", DataAccessParameterType.Numeric, this.CmeoId.ToString());
			data.ExecuteProcedure("CMEO_GetRecord");
			if (!data.EOF)
			{
				this.CmeoId = int.Parse(data["CmeoId"]);
				this.Oldcode = int.Parse(data["Oldcode"]);
				this.CmedId = int.Parse(data["CmedId"]);
				this.CmerId = int.Parse(data["CmerId"]);
				this.AgrbId = int.Parse(data["AgrbId"]);
				this.Description = data["Description"];
				this.Address1 = data["Address1"];
				this.Address2 = data["Address2"];
				this.City = data["City"];
				this.State = data["State"];
				this.Zipcode = data["Zipcode"];
				this.Phone = data["Phone"];
				this.Fax = data["Fax"];
				this.DisplayLocationYN = data["Displaylocationyn"];
				this.OfficeCode = data["OfficeCode"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int cmeoId = this.CmeoId;
			data.AddParam("@CmeoId", DataAccessParameterType.Numeric, cmeoId.ToString());
			cmeoId = this.Oldcode;
			data.AddParam("@Oldcode", DataAccessParameterType.Numeric, cmeoId.ToString());
			cmeoId = this.CmedId;
			data.AddParam("@CmedId", DataAccessParameterType.Numeric, cmeoId.ToString());
			cmeoId = this.CmerId;
			data.AddParam("@CmerId", DataAccessParameterType.Numeric, cmeoId.ToString());
			cmeoId = this.AgrbId;
			data.AddParam("@AgrbId", DataAccessParameterType.Numeric, cmeoId.ToString());
			data.AddParam("@Description", DataAccessParameterType.Text, this.Description);
			data.AddParam("@Address1", DataAccessParameterType.Text, this.Address1);
			data.AddParam("@Address2", DataAccessParameterType.Text, this.Address2);
			data.AddParam("@City", DataAccessParameterType.Text, this.City);
			data.AddParam("@State", DataAccessParameterType.Text, this.State);
			data.AddParam("@Zipcode", DataAccessParameterType.Text, this.Zipcode);
			data.AddParam("@Phone", DataAccessParameterType.Text, this.Phone);
			data.AddParam("@Fax", DataAccessParameterType.Text, this.Fax);
			data.AddParam("@DisplayLocationYN", DataAccessParameterType.Text, this.DisplayLocationYN);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("CMEO_InsertUpdate");
			if (!data.EOF)
			{
				this.CmeoId = int.Parse(data["CmeoId"]);
			}
			this.retrievedata();
		}
	}
}