using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Cars
{
	public class Member : Transaction
	{
		private int membID = 0;

		private int agraID = 0;

		private int oldCode = 0;

		private string code = "";

		private string firstName = "";

		private string lastName = "";

		private string middleInitial = "";

		private string address1 = "";

		private string address2 = "";

		private string city = "";

		private string state = "";

		private string zipCode = "";

		private string phone = "";

		private string phoneOther = "";

		private string email = "";

		private string sendEmailYN = "N";

		private string dateOfBirth = "";

		private string licenseNo = "";

		private string licenseState = "";

		private string socialSecNo = "";

		private int worksheetCount = 0;

		private string worksheetList = "";

		public string Address1
		{
			get
			{
				return this.address1;
			}
			set
			{
				this.address1 = value;
			}
		}

		public string Address2
		{
			get
			{
				return this.address2;
			}
			set
			{
				this.address2 = value;
			}
		}

		public string AgraID
		{
			get
			{
				return this.agraID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.agraID = 0;
				}
				else
				{
					this.agraID = int.Parse(value);
				}
			}
		}

		public string City
		{
			get
			{
				return this.city;
			}
			set
			{
				this.city = value;
			}
		}

		public string Code
		{
			get
			{
				return this.code;
			}
			set
			{
				this.code = value;
			}
		}

		public string DateOfBirth
		{
			get
			{
				return this.dateOfBirth;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.dateOfBirth = "";
				}
				else
				{
					this.dateOfBirth = DateTime.Parse(value).ToString();
				}
			}
		}

		public string Email
		{
			get
			{
				return this.email;
			}
			set
			{
				this.email = value;
			}
		}

		public string FirstName
		{
			get
			{
				return this.firstName;
			}
			set
			{
				this.firstName = value;
			}
		}

		public string LastName
		{
			get
			{
				return this.lastName;
			}
			set
			{
				this.lastName = value;
			}
		}

		public string LicenseNo
		{
			get
			{
				return this.licenseNo;
			}
			set
			{
				this.licenseNo = value;
			}
		}

		public string LicenseState
		{
			get
			{
				return this.licenseState;
			}
			set
			{
				this.licenseState = value;
			}
		}

		public string MemberCompleteMinimalYN
		{
			get
			{
				string completedYN = "Y";
				if (this.firstName == "")
				{
					completedYN = "N";
				}
				if (this.lastName == "")
				{
					completedYN = "N";
				}
				return completedYN;
			}
		}

		public string MemberCompleteYN
		{
			get
			{
				string completedYN = "Y";
				if (this.firstName == "")
				{
					completedYN = "N";
				}
				if (this.lastName == "")
				{
					completedYN = "N";
				}
				if (this.city == "")
				{
					completedYN = "N";
				}
				if (this.address1 == "")
				{
					completedYN = "N";
				}
				if (this.state == "")
				{
					completedYN = "N";
				}
				if (this.zipCode == "")
				{
					completedYN = "N";
				}
				if (this.phone == "")
				{
					completedYN = "N";
				}
				return completedYN;
			}
		}

		public string MembID
		{
			get
			{
				return this.membID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.membID = 0;
				}
				else
				{
					this.membID = int.Parse(value);
				}
			}
		}

		public string MiddleInitial
		{
			get
			{
				return this.middleInitial;
			}
			set
			{
				this.middleInitial = value;
			}
		}

		public string OldCode
		{
			get
			{
				return this.oldCode.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.oldCode = 0;
				}
				else
				{
					this.oldCode = int.Parse(value);
				}
			}
		}

		public string Phone
		{
			get
			{
				return this.phone;
			}
			set
			{
				this.phone = value;
			}
		}

		public string PhoneOther
		{
			get
			{
				return this.phoneOther;
			}
			set
			{
				this.phoneOther = value;
			}
		}

		public string SendEmailYN
		{
			get
			{
				return this.sendEmailYN;
			}
			set
			{
				this.sendEmailYN = (value == "Y" ? "Y" : "N");
			}
		}

		public string SocialSecNo
		{
			get
			{
				return this.socialSecNo;
			}
			set
			{
				this.socialSecNo = value;
			}
		}

		public string State
		{
			get
			{
				return this.state;
			}
			set
			{
				this.state = value;
			}
		}

		public string WorksheetCount
		{
			get
			{
				return this.worksheetCount.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.worksheetCount = 0;
				}
				else
				{
					this.worksheetCount = int.Parse(value);
				}
			}
		}

		public string WorksheetList
		{
			get
			{
				return this.worksheetList;
			}
			set
			{
				this.worksheetList = value;
			}
		}

		public string ZipCode
		{
			get
			{
				return this.zipCode;
			}
			set
			{
				this.zipCode = value;
			}
		}

		public Member()
		{
		}

		public Member(string connection, string modifiedUserID, int membID) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.membID = membID;
			if (this.membID > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@MembID", DataAccessParameterType.Numeric, this.MembID);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("MEMB_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@MembID", DataAccessParameterType.Numeric, this.membID.ToString());
			data.ExecuteProcedure("MEMB_GetRecord");
			if (!data.EOF)
			{
				this.MembID = data["MembID"];
				this.AgraID = data["AgraID"];
				this.OldCode = data["OldCode"];
				this.Code = data["Code"];
				this.FirstName = data["FirstName"];
				this.LastName = data["LastName"];
				this.MiddleInitial = data["MiddleInitial"];
				this.Address1 = data["Address1"];
				this.Address2 = data["Address2"];
				this.City = data["City"];
				this.State = data["State"];
				this.ZipCode = data["ZipCode"];
				this.Phone = data["Phone"];
				this.PhoneOther = data["PhoneOther"];
				this.Email = data["Email"];
				this.SendEmailYN = data["SendEmailYN"];
				this.DateOfBirth = data["DateOfBirth"];
				this.LicenseNo = data["LicenseNo"];
				this.LicenseState = data["LicenseState"];
				this.SocialSecNo = data["SocialSecNo"];
				this.WorksheetList = data["worksheetList"];
				this.WorksheetCount = data["worksheetCount"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@MembID", DataAccessParameterType.Numeric, this.MembID.ToString());
			data.AddParam("@AgraID", DataAccessParameterType.Numeric, this.AgraID.ToString());
			data.AddParam("@OldCode", DataAccessParameterType.Numeric, this.OldCode.ToString());
			data.AddParam("@Code", DataAccessParameterType.Text, this.Code);
			data.AddParam("@FirstName", DataAccessParameterType.Text, this.FirstName);
			data.AddParam("@LastName", DataAccessParameterType.Text, this.LastName);
			data.AddParam("@MiddleInitial", DataAccessParameterType.Text, this.MiddleInitial);
			data.AddParam("@Address1", DataAccessParameterType.Text, this.Address1);
			data.AddParam("@Address2", DataAccessParameterType.Text, this.Address2);
			data.AddParam("@City", DataAccessParameterType.Text, this.City);
			data.AddParam("@State", DataAccessParameterType.Text, this.State);
			data.AddParam("@ZipCode", DataAccessParameterType.Text, this.ZipCode);
			data.AddParam("@Phone", DataAccessParameterType.Text, this.Phone);
			data.AddParam("@PhoneOther", DataAccessParameterType.Text, this.PhoneOther);
			data.AddParam("@Email", DataAccessParameterType.Text, this.Email);
			data.AddParam("@SendEmailYN", DataAccessParameterType.Text, this.SendEmailYN);
			data.AddParam("@DateOfBirth", DataAccessParameterType.DateTime, this.DateOfBirth.ToString());
			data.AddParam("@LicenseNo", DataAccessParameterType.Text, this.LicenseNo);
			data.AddParam("@LicenseState", DataAccessParameterType.Text, this.LicenseState);
			data.AddParam("@SocialSecNo", DataAccessParameterType.Text, this.SocialSecNo);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("MEMB_InsertUpdate");
			if (!data.EOF)
			{
				this.MembID = data["MembID"];
			}
			this.retrievedata();
		}
	}
}