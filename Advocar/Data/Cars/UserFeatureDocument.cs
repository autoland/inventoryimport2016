using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Cars
{
	public class UserFeatureDocument : Transaction
	{
		private int usefID = 0;

		private string oldCode = "";

		private int cmftID = 0;

		private int cousID = 0;

		private string description = "";

		private string title = "";

		private string details = "";

		private string fileName = "";

		private int cddtID = 0;

		public string CddtID
		{
			get
			{
				return this.cddtID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.cddtID = 0;
				}
				else
				{
					this.cddtID = int.Parse(value);
				}
			}
		}

		public string CmftID
		{
			get
			{
				return this.cmftID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.cmftID = 0;
				}
				else
				{
					this.cmftID = int.Parse(value);
				}
			}
		}

		public string CousID
		{
			get
			{
				return this.cousID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.cousID = 0;
				}
				else
				{
					this.cousID = int.Parse(value);
				}
			}
		}

		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

		public string Details
		{
			get
			{
				return this.details;
			}
			set
			{
				this.details = value;
			}
		}

		public string FileName
		{
			get
			{
				return this.fileName;
			}
			set
			{
				this.fileName = value;
			}
		}

		public string OldCode
		{
			get
			{
				return this.oldCode;
			}
			set
			{
				this.oldCode = value;
			}
		}

		public string Title
		{
			get
			{
				return this.title;
			}
			set
			{
				this.title = value;
			}
		}

		public string UsefID
		{
			get
			{
				return this.usefID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.usefID = 0;
				}
				else
				{
					this.usefID = int.Parse(value);
				}
			}
		}

		public UserFeatureDocument()
		{
		}

		public UserFeatureDocument(string connection, string modifiedUserID, int usefID) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.usefID = usefID;
			if (this.usefID > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@UsefID", DataAccessParameterType.Numeric, this.UsefID);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("USEF_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@UsefID", DataAccessParameterType.Numeric, this.usefID.ToString());
			data.ExecuteProcedure("USEF_GetRecordDocument");
			if (!data.EOF)
			{
				this.UsefID = data["UsefID"];
				this.OldCode = data["OldCode"];
				this.CmftID = data["CmftID"];
				this.CousID = "1";
				this.Description = data["Description"];
				this.Title = data["Title"];
				this.Details = data["Details"];
				this.FileName = data["FileName"];
				this.CddtID = data["CddtID"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@UsefID", DataAccessParameterType.Numeric, this.UsefID.ToString());
			data.AddParam("@OldCode", DataAccessParameterType.Text, this.OldCode);
			data.AddParam("@CousID", DataAccessParameterType.Numeric, this.CousID.ToString());
			data.AddParam("@Description", DataAccessParameterType.Text, this.Description);
			data.AddParam("@Title", DataAccessParameterType.Text, this.Title);
			data.AddParam("@Details", DataAccessParameterType.Text, this.Details);
			data.AddParam("@FileName", DataAccessParameterType.Text, this.FileName);
			data.AddParam("@CddtID", DataAccessParameterType.Numeric, this.CddtID.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("USEF_InsertUpdateDocument");
			if (!data.EOF)
			{
				this.UsefID = data["UsefID"];
			}
			this.retrievedata();
		}
	}
}