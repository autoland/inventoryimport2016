using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Cars
{
	public class Worksheet : Transaction
	{
		private int workID = 0;

		private int agraID = 0;

		private int membID = 0;

		private int emplID = 0;

		private int cmeoID = 0;

		private int cmedID = 0;

		private int cmerID = 0;

		private int cowsID = 0;

		private int agrbID = 0;

		private int codeID_LeadReferralSource = 0;

		private int lookupId_ReasonsForLoanDecline = 0;

		private string creditUnionEmployeeName = "";

		private int covyID = 0;

		private string makeName = "";

		private string modelName = "";

		private string sectionShow = "";

		private string dealNumber = "";

		private string agraGroupName = "";

		private string membFirstName = "";

		private string emplFirstName = "";

		private string cmeoDescription = "";

		private string cmedDescription = "";

		private string cmerDescription = "";

		private string cowsDescription = "";

		private string agrbDescription = "";

		private string codeDescription = "";

		private string covyDescription = "";

		public string AgraGroupName
		{
			get
			{
				return this.agraGroupName;
			}
		}

		public string AgraID
		{
			get
			{
				return this.agraID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.agraID = 0;
				}
				else
				{
					this.agraID = int.Parse(value);
				}
			}
		}

		public string AgrbDescription
		{
			get
			{
				return this.agrbDescription;
			}
		}

		public string AgrbID
		{
			get
			{
				return this.agrbID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.agrbID = 0;
				}
				else
				{
					this.agrbID = int.Parse(value);
				}
			}
		}

		public string CmedDescription
		{
			get
			{
				return this.cmedDescription;
			}
		}

		public string CmedID
		{
			get
			{
				return this.cmedID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.cmedID = 0;
				}
				else
				{
					this.cmedID = int.Parse(value);
				}
			}
		}

		public string CmeoDescription
		{
			get
			{
				return this.cmeoDescription;
			}
		}

		public string CmeoID
		{
			get
			{
				return this.cmeoID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.cmeoID = 0;
				}
				else
				{
					this.cmeoID = int.Parse(value);
				}
			}
		}

		public string CmerDescription
		{
			get
			{
				return this.cmerDescription;
			}
		}

		public string CmerID
		{
			get
			{
				return this.cmerID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.cmerID = 0;
				}
				else
				{
					this.cmerID = int.Parse(value);
				}
			}
		}

		public string CodeDescription
		{
			get
			{
				return this.codeDescription;
			}
		}

		public string CodeID_LeadReferralSource
		{
			get
			{
				return this.codeID_LeadReferralSource.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.codeID_LeadReferralSource = 0;
				}
				else
				{
					this.codeID_LeadReferralSource = int.Parse(value);
				}
			}
		}

		public string CovyDescription
		{
			get
			{
				return this.covyDescription;
			}
		}

		public string CovyID
		{
			get
			{
				return this.covyID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.covyID = 0;
				}
				else
				{
					this.covyID = int.Parse(value);
				}
			}
		}

		public string CowsDescription
		{
			get
			{
				return this.cowsDescription;
			}
		}

		public string CowsID
		{
			get
			{
				return this.cowsID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.cowsID = 0;
				}
				else
				{
					this.cowsID = int.Parse(value);
				}
			}
		}

		public string CreditUnionEmployeeName
		{
			get
			{
				return this.creditUnionEmployeeName;
			}
			set
			{
				this.creditUnionEmployeeName = value;
			}
		}

		public string DealNumber
		{
			get
			{
				return this.dealNumber;
			}
			set
			{
				this.dealNumber = value;
			}
		}

		public string EmplFirstName
		{
			get
			{
				return this.emplFirstName;
			}
		}

		public string EmplID
		{
			get
			{
				return this.emplID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.emplID = 0;
				}
				else
				{
					this.emplID = int.Parse(value);
				}
			}
		}

		public string LookupId_ReasonsForLoanDecline
		{
			get
			{
				return this.lookupId_ReasonsForLoanDecline.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.lookupId_ReasonsForLoanDecline = 0;
				}
				else
				{
					this.lookupId_ReasonsForLoanDecline = int.Parse(value);
				}
			}
		}

		public string MakeName
		{
			get
			{
				return this.makeName;
			}
			set
			{
				this.makeName = value;
			}
		}

		public string MembFirstName
		{
			get
			{
				return this.membFirstName;
			}
		}

		public string MembID
		{
			get
			{
				return this.membID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.membID = 0;
				}
				else
				{
					this.membID = int.Parse(value);
				}
			}
		}

		public string ModelName
		{
			get
			{
				return this.modelName;
			}
			set
			{
				this.modelName = value;
			}
		}

		public string SectionShow
		{
			get
			{
				return this.sectionShow;
			}
			set
			{
				this.sectionShow = value;
			}
		}

		public string WorkID
		{
			get
			{
				return this.workID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.workID = 0;
				}
				else
				{
					this.workID = int.Parse(value);
				}
			}
		}

		public Worksheet()
		{
		}

		public Worksheet(string connection, string modifiedUserID, int workID) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.workID = workID;
			if (this.workID > 0)
			{
				this.retrievedata();
			}
		}

		public int AddNotes(string notes, string dealer, int wornID, string postitYN, int postitX, int postitY)
		{
			int num;
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@WornID", DataAccessParameterType.Numeric, wornID.ToString());
			data.AddParam("@WorkID", DataAccessParameterType.Numeric, this.workID.ToString());
			data.AddParam("@Notes", DataAccessParameterType.Text, notes, false);
			data.AddParam("@Dealer", DataAccessParameterType.Text, dealer, true);
			data.AddParam("@PostitYN", DataAccessParameterType.Text, postitYN);
			data.AddParam("@PostitX", DataAccessParameterType.Numeric, postitX.ToString());
			data.AddParam("@PostitY", DataAccessParameterType.Numeric, postitY.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("WORN_InsertUpdate");
			num = (data.EOF ? 0 : int.Parse(data["WornID"]));
			return num;
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@WorkID", DataAccessParameterType.Numeric, this.WorkID);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("WORK_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@WorkID", DataAccessParameterType.Numeric, this.workID.ToString());
			data.ExecuteProcedure("WORK_GetRecord");
			if (!data.EOF)
			{
				this.WorkID = data["WorkID"];
				this.AgraID = data["AgraID"];
				this.MembID = data["MembID"];
				this.EmplID = data["EmplID"];
				this.CmeoID = data["CmeoID"];
				this.CmedID = data["CmedID"];
				this.CmerID = data["CmerID"];
				this.CowsID = data["CowsID"];
				this.AgrbID = data["AgrbID"];
				this.CodeID_LeadReferralSource = data["CodeID_LeadReferralSource"];
				this.LookupId_ReasonsForLoanDecline = data["LookupId_ReasonsForLoanDecline"];
				this.CreditUnionEmployeeName = data["CreditUnionEmployeeName"];
				this.CovyID = data["CovyID"];
				this.MakeName = data["MakeName"];
				this.ModelName = data["ModelName"];
				this.SectionShow = data["SectionShow"];
				this.DealNumber = data["DealNumber"];
				this.agraGroupName = data["AgraGroupName"];
				this.membFirstName = data["MembFirstName"];
				this.emplFirstName = data["EmplFirstName"];
				this.cmeoDescription = data["CmeoDescription"];
				this.cmedDescription = data["CmedDescription"];
				this.cmerDescription = data["CmerDescription"];
				this.cowsDescription = data["CowsDescription"];
				this.agrbDescription = data["AgrbDescription"];
				this.codeDescription = data["CodeDescription"];
				this.covyDescription = data["CovyDescription"];
			}
			else
			{
				this.wipeout();
				int emplID = Employee.GetRecordId(this.modifiedUserID, this.connection);
				Employee empl = new Employee(this.connection, this.modifiedUserID, emplID);
				this.CmedID = empl.CmedID;
				this.CmeoID = empl.CmeoID;
				this.CmerID = empl.CmerID;
				this.EmplID = empl.EmplID;
				this.CowsID = 1.ToString();
			}
		}

		public override void Save()
		{
			if (this.emplID == 0)
			{
				int emplID = Employee.GetRecordId(this.modifiedUserID, this.connection);
				Employee empl = new Employee(this.connection, this.modifiedUserID, emplID);
				this.CmedID = empl.CmedID;
				this.CmeoID = empl.CmeoID;
				this.CmerID = empl.CmerID;
				this.EmplID = empl.EmplID;
			}
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@WorkID", DataAccessParameterType.Numeric, this.WorkID.ToString());
			data.AddParam("@AgraID", DataAccessParameterType.Numeric, this.AgraID.ToString());
			data.AddParam("@MembID", DataAccessParameterType.Numeric, this.MembID.ToString());
			data.AddParam("@EmplID", DataAccessParameterType.Numeric, this.EmplID.ToString());
			data.AddParam("@CmeoID", DataAccessParameterType.Numeric, this.CmeoID.ToString());
			data.AddParam("@CmedID", DataAccessParameterType.Numeric, this.CmedID.ToString());
			data.AddParam("@CmerID", DataAccessParameterType.Numeric, this.CmerID.ToString());
			data.AddParam("@CowsID", DataAccessParameterType.Numeric, this.CowsID.ToString());
			data.AddParam("@AgrbID", DataAccessParameterType.Numeric, this.AgrbID.ToString());
			data.AddParam("@CodeID_LeadReferralSource", DataAccessParameterType.Numeric, this.CodeID_LeadReferralSource.ToString());
			data.AddParam("@LookupId_ReasonsForLoanDecline", DataAccessParameterType.Numeric, this.lookupId_ReasonsForLoanDecline.ToString());
			data.AddParam("@CreditUnionEmployeeName", DataAccessParameterType.Text, this.CreditUnionEmployeeName);
			data.AddParam("@CovyID", DataAccessParameterType.Numeric, this.CovyID.ToString());
			data.AddParam("@MakeName", DataAccessParameterType.Text, this.MakeName);
			data.AddParam("@ModelName", DataAccessParameterType.Text, this.ModelName);
			data.AddParam("@SectionShow", DataAccessParameterType.Text, this.SectionShow);
			data.AddParam("@DealNumber", DataAccessParameterType.Text, this.DealNumber);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("WORK_InsertUpdate");
			if (!data.EOF)
			{
				this.WorkID = data["WorkID"];
			}
			this.retrievedata();
		}
	}
}