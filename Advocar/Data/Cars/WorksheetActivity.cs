using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Cars
{
	public class WorksheetActivity : Transaction
	{
		private int worvID = 0;

		private int workID = 0;

		private int actaID = 0;

		public string ActaID
		{
			get
			{
				return this.actaID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.actaID = 0;
				}
				else
				{
					this.actaID = int.Parse(value);
				}
			}
		}

		public string WorkID
		{
			get
			{
				return this.workID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.workID = 0;
				}
				else
				{
					this.workID = int.Parse(value);
				}
			}
		}

		public string WorvID
		{
			get
			{
				return this.worvID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.worvID = 0;
				}
				else
				{
					this.worvID = int.Parse(value);
				}
			}
		}

		public WorksheetActivity()
		{
		}

		public WorksheetActivity(string connection, string modifiedUserID, int worvID) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.worvID = worvID;
			if (this.worvID > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@WorvID", DataAccessParameterType.Numeric, this.WorvID);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("WORV_Delete");
			this.wipeout();
		}

		public static int GetRecordId(int activityId, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@ActivityId", DataAccessParameterType.Numeric, activityId.ToString());
			data.ExecuteProcedure("WORV_GetRecord");
			num = (!data.EOF ? int.Parse(data["WorvId"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@WorvID", DataAccessParameterType.Numeric, this.worvID.ToString());
			data.ExecuteProcedure("WORV_GetRecord");
			if (!data.EOF)
			{
				this.WorvID = data["WorvID"];
				this.WorkID = data["WorkID"];
				this.ActaID = data["ActaID"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@WorvID", DataAccessParameterType.Numeric, this.WorvID.ToString());
			data.AddParam("@WorkID", DataAccessParameterType.Numeric, this.WorkID.ToString());
			data.AddParam("@ActaID", DataAccessParameterType.Numeric, this.ActaID.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("WORV_InsertUpdate");
			if (!data.EOF)
			{
				this.WorvID = data["WorvID"];
			}
			this.retrievedata();
		}
	}
}