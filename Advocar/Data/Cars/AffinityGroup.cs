using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Cars
{
	public class AffinityGroup : Transaction
	{
		private int agraID = 0;

		private int oldCode = 0;

		private int agrbID_MainBranch = 0;

		private string groupName = "";

		private string activatedYN = "N";

		private string tollFreePhone = "";

		private int cddfID = 0;

		private string emailClassifieds = "";

		private string emailContactUs = "";

		private string emailMbp = "";

		private string emailGetQuote = "";

		private string emailLease = "";

		private string emailLoanApp = "";

		private string emailLocates = "";

		private string emailBids = "";

		private string toolMbpUrl = "";

		private string toolLeaseUrl = "";

		private string toolLoanAppUrl = "";

		private string toolClassSearchUrl = "";

		private string toolClassPostUrl = "";

		private string usedAutolandOnlyYN = "N";

		private string webConfig = "";

		private string webLogoYN = "N";

		private string webHomeUrl = "";

		private string webLink1Text = "";

		private string webLink1Url = "";

		private string webLink2Text = "";

		private string webLink2Url = "";

		private string webLink3Text = "";

		private string webLink3Url = "";

		private string kbbYN = "N";

		private string displayTopYN = "N";

		private string promoYN = "N";

		private string mbpPromoYN = "N";

		private int commID = 0;

		private string restrictions = "";

		private float interestRate1 = 0f;

		private float interestRate2 = 0f;

		private float interestRate3 = 0f;

		private float interestRate4 = 0f;

		private string anonymousUserYN = "N";

		private int anonymousUserCount = 0;

		private string anonymousUserName = "";

		private float mbpMaxAmount = 0f;

		public string ActivatedYN
		{
			get
			{
				return this.activatedYN;
			}
			set
			{
				this.activatedYN = (value == "Y" ? "Y" : "N");
			}
		}

		public string AgraID
		{
			get
			{
				return this.agraID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.agraID = 0;
				}
				else
				{
					this.agraID = int.Parse(value);
				}
			}
		}

		public string AgrbID_MainBranch
		{
			get
			{
				return this.agrbID_MainBranch.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.agrbID_MainBranch = 0;
				}
				else
				{
					this.agrbID_MainBranch = int.Parse(value);
				}
			}
		}

		public string AnonymousUserCount
		{
			get
			{
				return this.anonymousUserCount.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.anonymousUserCount = 0;
				}
				else
				{
					this.anonymousUserCount = int.Parse(value);
				}
			}
		}

		public string AnonymousUserName
		{
			get
			{
				return this.anonymousUserName;
			}
			set
			{
				this.anonymousUserName = value;
			}
		}

		public string AnonymousUserYN
		{
			get
			{
				return this.anonymousUserYN;
			}
			set
			{
				this.anonymousUserYN = (value == "Y" ? "Y" : "N");
			}
		}

		public string CddfID
		{
			get
			{
				return this.cddfID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.cddfID = 0;
				}
				else
				{
					this.cddfID = int.Parse(value);
				}
			}
		}

		public string CommID
		{
			get
			{
				return this.commID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.commID = 0;
				}
				else
				{
					this.commID = int.Parse(value);
				}
			}
		}

		public string DisplayTopYN
		{
			get
			{
				return this.displayTopYN;
			}
			set
			{
				this.displayTopYN = (value == "Y" ? "Y" : "N");
			}
		}

		public string EmailBids
		{
			get
			{
				return this.emailBids;
			}
			set
			{
				this.emailBids = value;
			}
		}

		public string EmailClassifieds
		{
			get
			{
				return this.emailClassifieds;
			}
			set
			{
				this.emailClassifieds = value;
			}
		}

		public string EmailContactUs
		{
			get
			{
				return this.emailContactUs;
			}
			set
			{
				this.emailContactUs = value;
			}
		}

		public string EmailGetQuote
		{
			get
			{
				return this.emailGetQuote;
			}
			set
			{
				this.emailGetQuote = value;
			}
		}

		public string EmailLease
		{
			get
			{
				return this.emailLease;
			}
			set
			{
				this.emailLease = value;
			}
		}

		public string EmailLoanApp
		{
			get
			{
				return this.emailLoanApp;
			}
			set
			{
				this.emailLoanApp = value;
			}
		}

		public string EmailLocates
		{
			get
			{
				return this.emailLocates;
			}
			set
			{
				this.emailLocates = value;
			}
		}

		public string EmailMbp
		{
			get
			{
				return this.emailMbp;
			}
			set
			{
				this.emailMbp = value;
			}
		}

		public string GroupName
		{
			get
			{
				return this.groupName;
			}
			set
			{
				this.groupName = value;
			}
		}

		public string InterestRate1
		{
			get
			{
				return string.Format("{0:#,##0.00000000}", this.interestRate1);
			}
			set
			{
				if (!Validation.IsNumeric(value.Replace("$", "")))
				{
					this.interestRate1 = 0f;
				}
				else
				{
					this.interestRate1 = float.Parse(value.Replace("$", ""));
				}
			}
		}

		public string InterestRate2
		{
			get
			{
				return string.Format("{0:#,##0.00000000}", this.interestRate2);
			}
			set
			{
				if (!Validation.IsNumeric(value.Replace("$", "")))
				{
					this.interestRate2 = 0f;
				}
				else
				{
					this.interestRate2 = float.Parse(value.Replace("$", ""));
				}
			}
		}

		public string InterestRate3
		{
			get
			{
				return string.Format("{0:#,##0.00000000}", this.interestRate3);
			}
			set
			{
				if (!Validation.IsNumeric(value.Replace("$", "")))
				{
					this.interestRate3 = 0f;
				}
				else
				{
					this.interestRate3 = float.Parse(value.Replace("$", ""));
				}
			}
		}

		public string InterestRate4
		{
			get
			{
				return string.Format("{0:#,##0.00000000}", this.interestRate4);
			}
			set
			{
				if (!Validation.IsNumeric(value.Replace("$", "")))
				{
					this.interestRate4 = 0f;
				}
				else
				{
					this.interestRate4 = float.Parse(value.Replace("$", ""));
				}
			}
		}

		public string KbbYN
		{
			get
			{
				return this.kbbYN;
			}
			set
			{
				this.kbbYN = (value == "Y" ? "Y" : "N");
			}
		}

		public string MbpMaxAmount
		{
			get
			{
				return string.Format("{0:$#,##0.00}", this.mbpMaxAmount);
			}
			set
			{
				if (!Validation.IsNumeric(value.Replace("$", "")))
				{
					this.mbpMaxAmount = 0f;
				}
				else
				{
					this.mbpMaxAmount = float.Parse(value.Replace("$", ""));
				}
			}
		}

		public string MbpPromoYN
		{
			get
			{
				return this.mbpPromoYN;
			}
			set
			{
				this.mbpPromoYN = (value == "Y" ? "Y" : "N");
			}
		}

		public string OldCode
		{
			get
			{
				return this.oldCode.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.oldCode = 0;
				}
				else
				{
					this.oldCode = int.Parse(value);
				}
			}
		}

		public string PromoYN
		{
			get
			{
				return this.promoYN;
			}
			set
			{
				this.promoYN = (value == "Y" ? "Y" : "N");
			}
		}

		public string Restrictions
		{
			get
			{
				return this.restrictions;
			}
			set
			{
				this.restrictions = value;
			}
		}

		public string TollFreePhone
		{
			get
			{
				return this.tollFreePhone;
			}
			set
			{
				this.tollFreePhone = value;
			}
		}

		public string ToolClassPostUrl
		{
			get
			{
				return this.toolClassPostUrl;
			}
			set
			{
				this.toolClassPostUrl = value;
			}
		}

		public string ToolClassSearchUrl
		{
			get
			{
				return this.toolClassSearchUrl;
			}
			set
			{
				this.toolClassSearchUrl = value;
			}
		}

		public string ToolLeaseUrl
		{
			get
			{
				return this.toolLeaseUrl;
			}
			set
			{
				this.toolLeaseUrl = value;
			}
		}

		public string ToolLoanAppUrl
		{
			get
			{
				return this.toolLoanAppUrl;
			}
			set
			{
				this.toolLoanAppUrl = value;
			}
		}

		public string ToolMbpUrl
		{
			get
			{
				return this.toolMbpUrl;
			}
			set
			{
				this.toolMbpUrl = value;
			}
		}

		public string UsedAutolandOnlyYN
		{
			get
			{
				return this.usedAutolandOnlyYN;
			}
			set
			{
				this.usedAutolandOnlyYN = (value == "Y" ? "Y" : "N");
			}
		}

		public string WebConfig
		{
			get
			{
				return this.webConfig;
			}
			set
			{
				this.webConfig = value;
			}
		}

		public string WebHomeUrl
		{
			get
			{
				return this.webHomeUrl;
			}
			set
			{
				this.webHomeUrl = value;
			}
		}

		public string WebLink1Text
		{
			get
			{
				return this.webLink1Text;
			}
			set
			{
				this.webLink1Text = value;
			}
		}

		public string WebLink1Url
		{
			get
			{
				return this.webLink1Url;
			}
			set
			{
				this.webLink1Url = value;
			}
		}

		public string WebLink2Text
		{
			get
			{
				return this.webLink2Text;
			}
			set
			{
				this.webLink2Text = value;
			}
		}

		public string WebLink2Url
		{
			get
			{
				return this.webLink2Url;
			}
			set
			{
				this.webLink2Url = value;
			}
		}

		public string WebLink3Text
		{
			get
			{
				return this.webLink3Text;
			}
			set
			{
				this.webLink3Text = value;
			}
		}

		public string WebLink3Url
		{
			get
			{
				return this.webLink3Url;
			}
			set
			{
				this.webLink3Url = value;
			}
		}

		public string WebLogoYN
		{
			get
			{
				return this.webLogoYN;
			}
			set
			{
				this.webLogoYN = (value == "Y" ? "Y" : "N");
			}
		}

		public AffinityGroup()
		{
		}

		public AffinityGroup(string connection, string modifiedUserID, int agraID) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.agraID = agraID;
			if (this.agraID > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@AgraId", DataAccessParameterType.Numeric, this.AgraID);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("AGRA_Delete");
			this.wipeout();
		}

		public static int GetRecordId(int affinityGroupBranchId, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@AgrbId", DataAccessParameterType.Numeric, affinityGroupBranchId.ToString());
			data.ExecuteProcedure("AGRA_GetRecord");
			num = (!data.EOF ? int.Parse(data["AgraId"]) : 0);
			return num;
		}

		public static int GetRecordId(string extranetUserID, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@UserID_CuEmployee", DataAccessParameterType.Text, extranetUserID);
			data.ExecuteProcedure("AGRA_GetRecord");
			num = (!data.EOF ? int.Parse(data["AgraId"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@AgraID", DataAccessParameterType.Numeric, this.agraID.ToString());
			data.ExecuteProcedure("AGRA_GetRecord");
			if (!data.EOF)
			{
				this.AgraID = data["AgraID"];
				this.OldCode = data["OldCode"];
				this.AgrbID_MainBranch = data["AgrbID_MainBranch"];
				this.GroupName = data["GroupName"];
				this.ActivatedYN = data["ActivatedYN"];
				this.TollFreePhone = data["TollFreePhone"];
				this.CddfID = data["CddfID"];
				this.EmailClassifieds = data["EmailClassifieds"];
				this.EmailContactUs = data["EmailContactUs"];
				this.EmailMbp = data["EmailMbp"];
				this.EmailGetQuote = data["EmailGetQuote"];
				this.EmailLease = data["EmailLease"];
				this.EmailLoanApp = data["EmailLoanApp"];
				this.EmailLocates = data["EmailLocates"];
				this.EmailBids = data["EmailBids"];
				this.ToolMbpUrl = data["ToolMbpUrl"];
				this.ToolLeaseUrl = data["ToolLeaseUrl"];
				this.ToolLoanAppUrl = data["ToolLoanAppUrl"];
				this.ToolClassSearchUrl = data["ToolClassSearchUrl"];
				this.ToolClassPostUrl = data["ToolClassPostUrl"];
				this.UsedAutolandOnlyYN = data["UsedAutolandOnlyYN"];
				this.WebConfig = data["WebConfig"];
				this.WebLogoYN = data["WebLogoYN"];
				this.WebHomeUrl = data["WebHomeUrl"];
				this.WebLink1Text = data["WebLink1Text"];
				this.WebLink1Url = data["WebLink1Url"];
				this.WebLink2Text = data["WebLink2Text"];
				this.WebLink2Url = data["WebLink2Url"];
				this.WebLink3Text = data["WebLink3Text"];
				this.WebLink3Url = data["WebLink3Url"];
				this.KbbYN = data["KbbYN"];
				this.DisplayTopYN = data["DisplayTopYN"];
				this.PromoYN = data["PromoYN"];
				this.MbpPromoYN = data["MbpPromoYN"];
				this.CommID = data["CommID"];
				this.Restrictions = data["Restrictions"];
				this.InterestRate1 = data["InterestRate1"];
				this.InterestRate2 = data["InterestRate2"];
				this.InterestRate3 = data["InterestRate3"];
				this.InterestRate4 = data["InterestRate4"];
				this.AnonymousUserYN = data["AnonymousUserYN"];
				this.AnonymousUserCount = data["AnonymousUserCount"];
				this.AnonymousUserName = data["AnonymousUserName"];
				this.MbpMaxAmount = data["MbpMaxAmount"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@AgraID", DataAccessParameterType.Numeric, this.AgraID.ToString());
			data.AddParam("@OldCode", DataAccessParameterType.Numeric, this.OldCode.ToString());
			data.AddParam("@AgrbID_MainBranch", DataAccessParameterType.Numeric, this.AgrbID_MainBranch.ToString());
			data.AddParam("@GroupName", DataAccessParameterType.Text, this.GroupName);
			data.AddParam("@ActivatedYN", DataAccessParameterType.Text, this.ActivatedYN);
			data.AddParam("@TollFreePhone", DataAccessParameterType.Text, this.TollFreePhone);
			data.AddParam("@CddfID", DataAccessParameterType.Numeric, this.CddfID.ToString());
			data.AddParam("@EmailClassifieds", DataAccessParameterType.Text, this.EmailClassifieds);
			data.AddParam("@EmailContactUs", DataAccessParameterType.Text, this.EmailContactUs);
			data.AddParam("@EmailMbp", DataAccessParameterType.Text, this.EmailMbp);
			data.AddParam("@EmailGetQuote", DataAccessParameterType.Text, this.EmailGetQuote);
			data.AddParam("@EmailLease", DataAccessParameterType.Text, this.EmailLease);
			data.AddParam("@EmailLoanApp", DataAccessParameterType.Text, this.EmailLoanApp);
			data.AddParam("@EmailLocates", DataAccessParameterType.Text, this.EmailLocates);
			data.AddParam("@EmailBids", DataAccessParameterType.Text, this.EmailBids);
			data.AddParam("@ToolMbpUrl", DataAccessParameterType.Text, this.ToolMbpUrl);
			data.AddParam("@ToolLeaseUrl", DataAccessParameterType.Text, this.ToolLeaseUrl);
			data.AddParam("@ToolLoanAppUrl", DataAccessParameterType.Text, this.ToolLoanAppUrl);
			data.AddParam("@ToolClassSearchUrl", DataAccessParameterType.Text, this.ToolClassSearchUrl);
			data.AddParam("@ToolClassPostUrl", DataAccessParameterType.Text, this.ToolClassPostUrl);
			data.AddParam("@UsedAutolandOnlyYN", DataAccessParameterType.Text, this.UsedAutolandOnlyYN);
			data.AddParam("@WebConfig", DataAccessParameterType.Text, this.WebConfig);
			data.AddParam("@WebLogoYN", DataAccessParameterType.Text, this.WebLogoYN);
			data.AddParam("@WebHomeUrl", DataAccessParameterType.Text, this.WebHomeUrl);
			data.AddParam("@WebLink1Text", DataAccessParameterType.Text, this.WebLink1Text);
			data.AddParam("@WebLink1Url", DataAccessParameterType.Text, this.WebLink1Url);
			data.AddParam("@WebLink2Text", DataAccessParameterType.Text, this.WebLink2Text);
			data.AddParam("@WebLink2Url", DataAccessParameterType.Text, this.WebLink2Url);
			data.AddParam("@WebLink3Text", DataAccessParameterType.Text, this.WebLink3Text);
			data.AddParam("@WebLink3Url", DataAccessParameterType.Text, this.WebLink3Url);
			data.AddParam("@KbbYN", DataAccessParameterType.Text, this.KbbYN);
			data.AddParam("@DisplayTopYN", DataAccessParameterType.Text, this.DisplayTopYN);
			data.AddParam("@PromoYN", DataAccessParameterType.Text, this.PromoYN);
			data.AddParam("@MbpPromoYN", DataAccessParameterType.Text, this.MbpPromoYN);
			data.AddParam("@CommID", DataAccessParameterType.Numeric, this.CommID.ToString());
			data.AddParam("@Restrictions", DataAccessParameterType.Text, this.Restrictions);
			data.AddParam("@InterestRate1", DataAccessParameterType.Numeric, this.InterestRate1.ToString());
			data.AddParam("@InterestRate2", DataAccessParameterType.Numeric, this.InterestRate2.ToString());
			data.AddParam("@InterestRate3", DataAccessParameterType.Numeric, this.InterestRate3.ToString());
			data.AddParam("@InterestRate4", DataAccessParameterType.Numeric, this.InterestRate4.ToString());
			data.AddParam("@AnonymousUserYN", DataAccessParameterType.Text, this.AnonymousUserYN);
			data.AddParam("@AnonymousUserCount", DataAccessParameterType.Numeric, this.AnonymousUserCount.ToString());
			data.AddParam("@AnonymousUserName", DataAccessParameterType.Text, this.AnonymousUserName);
			data.AddParam("@MbpMaxAmount", DataAccessParameterType.Numeric, this.mbpMaxAmount.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("AGRA_InsertUpdate");
			if (!data.EOF)
			{
				this.AgraID = data["AgraID"];
			}
			this.retrievedata();
		}
	}
}