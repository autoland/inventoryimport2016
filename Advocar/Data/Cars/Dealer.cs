using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Cars
{
	public class Dealer : Transaction
	{
		private int dlraID = 0;

		private int dlraID_Parent = 0;

		private string hierarchy = "";

		private int codeID_DealerType = 0;

		private int codeID_JobType = 0;

		private string jobTypeProviderCode = "";

		private int codeID_JobType_Used = 0;

		private string jobTypeProviderCode_Used = "";

		private string dealerName = "";

		private string address1 = "";

		private string address2 = "";

		private string city = "";

		private string state = "";

		private string zipCode = "";

		private string phone = "";

		private string fax = "";

		private string email = "";

		private string contact = "";

		private float latitude = 0f;

		private float longitude = 0f;

		private int codeID_DealerCrmSystem = 0;

		private string crmDealershipID = "";

		private string crmToEmail = "";

		private int codeID_DealerCrmSystem_Used = 0;

		private string crmDealershipID_Used = "";

		private string crmToEmail_Used = "";

		private int leadsMax = 0;

		private string leadsOldestAccepted = "";

		private int emplID_AcctMgr = 0;

		private string activeYN = "N";

		private string salesforceid = "";

		private bool isCARS = false;

		private bool isWholesaler = false;

		public string ActiveYN
		{
			get
			{
				return this.activeYN;
			}
			set
			{
				this.activeYN = (value == "Y" ? "Y" : "N");
			}
		}

		public string Address1
		{
			get
			{
				return this.address1;
			}
			set
			{
				this.address1 = value;
			}
		}

		public string Address2
		{
			get
			{
				return this.address2;
			}
			set
			{
				this.address2 = value;
			}
		}

		public string City
		{
			get
			{
				return this.city;
			}
			set
			{
				this.city = value;
			}
		}

		public string CodeID_DealerCrmSystem
		{
			get
			{
				return this.codeID_DealerCrmSystem.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.codeID_DealerCrmSystem = 0;
				}
				else
				{
					this.codeID_DealerCrmSystem = int.Parse(value);
				}
			}
		}

		public string CodeID_DealerCrmSystem_Used
		{
			get
			{
				return this.codeID_DealerCrmSystem_Used.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.codeID_DealerCrmSystem_Used = 0;
				}
				else
				{
					this.codeID_DealerCrmSystem_Used = int.Parse(value);
				}
			}
		}

		public string CodeID_DealerType
		{
			get
			{
				return this.codeID_DealerType.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.codeID_DealerType = 0;
				}
				else
				{
					this.codeID_DealerType = int.Parse(value);
				}
			}
		}

		public string CodeID_JobType
		{
			get
			{
				return this.codeID_JobType.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.codeID_JobType = 0;
				}
				else
				{
					this.codeID_JobType = int.Parse(value);
				}
			}
		}

		public string CodeID_JobType_Used
		{
			get
			{
				return this.codeID_JobType_Used.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.codeID_JobType_Used = 0;
				}
				else
				{
					this.codeID_JobType_Used = int.Parse(value);
				}
			}
		}

		public string Contact
		{
			get
			{
				return this.contact;
			}
			set
			{
				this.contact = value;
			}
		}

		public string CrmDealershipID
		{
			get
			{
				return this.crmDealershipID;
			}
			set
			{
				this.crmDealershipID = value;
			}
		}

		public string CrmDealershipID_Used
		{
			get
			{
				return this.crmDealershipID_Used;
			}
			set
			{
				this.crmDealershipID_Used = value;
			}
		}

		public string CrmToEmail
		{
			get
			{
				return this.crmToEmail;
			}
			set
			{
				this.crmToEmail = value;
			}
		}

		public string CrmToEmail_Used
		{
			get
			{
				return this.crmToEmail_Used;
			}
			set
			{
				this.crmToEmail_Used = value;
			}
		}

		public string DealerName
		{
			get
			{
				return this.dealerName;
			}
			set
			{
				this.dealerName = value;
			}
		}

		public string DlraID
		{
			get
			{
				return this.dlraID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.dlraID = 0;
				}
				else
				{
					this.dlraID = int.Parse(value);
				}
			}
		}

		public string DlraID_Parent
		{
			get
			{
				return this.dlraID_Parent.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.dlraID_Parent = 0;
				}
				else
				{
					this.dlraID_Parent = int.Parse(value);
				}
			}
		}

		public string Email
		{
			get
			{
				return this.email;
			}
			set
			{
				this.email = value;
			}
		}

		public string EmplID_AcctMgr
		{
			get
			{
				return this.emplID_AcctMgr.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.emplID_AcctMgr = 0;
				}
				else
				{
					this.emplID_AcctMgr = int.Parse(value);
				}
			}
		}

		public string Fax
		{
			get
			{
				return this.fax;
			}
			set
			{
				this.fax = value;
			}
		}

		public string Hierarchy
		{
			get
			{
				return this.hierarchy;
			}
			set
			{
				this.hierarchy = value;
			}
		}

		public bool IsCARS
		{
			get
			{
				return this.isCARS;
			}
			set
			{
				this.isCARS = value;
			}
		}

		public bool IsWholesaler
		{
			get
			{
				return this.isWholesaler;
			}
			set
			{
				this.isWholesaler = value;
			}
		}

		public string JobTypeProviderCode
		{
			get
			{
				return this.jobTypeProviderCode;
			}
			set
			{
				this.jobTypeProviderCode = value;
			}
		}

		public string JobTypeProviderCode_Used
		{
			get
			{
				return this.jobTypeProviderCode_Used;
			}
			set
			{
				this.jobTypeProviderCode_Used = value;
			}
		}

		public string Latitude
		{
			get
			{
				return string.Format("{0:#,##0.00000000}", this.latitude);
			}
			set
			{
				if (!Validation.IsNumeric(value.Replace("$", "")))
				{
					this.latitude = 0f;
				}
				else
				{
					this.latitude = float.Parse(value.Replace("$", ""));
				}
			}
		}

		public string LeadsMax
		{
			get
			{
				return this.leadsMax.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.leadsMax = 0;
				}
				else
				{
					this.leadsMax = int.Parse(value);
				}
			}
		}

		public string LeadsOldestAccepted
		{
			get
			{
				return this.leadsOldestAccepted;
			}
			set
			{
				this.leadsOldestAccepted = value;
			}
		}

		public string Longitude
		{
			get
			{
				return string.Format("{0:#,##0.00000000}", this.longitude);
			}
			set
			{
				if (!Validation.IsNumeric(value.Replace("$", "")))
				{
					this.longitude = 0f;
				}
				else
				{
					this.longitude = float.Parse(value.Replace("$", ""));
				}
			}
		}

		public string Phone
		{
			get
			{
				return this.phone;
			}
			set
			{
				this.phone = value;
			}
		}

		public string Salesforceid
		{
			get
			{
				return this.salesforceid;
			}
			set
			{
				this.salesforceid = value;
			}
		}

		public string State
		{
			get
			{
				return this.state;
			}
			set
			{
				this.state = value;
			}
		}

		public string ZipCode
		{
			get
			{
				return this.zipCode;
			}
			set
			{
				this.zipCode = value;
			}
		}

		public Dealer()
		{
		}

		public Dealer(string connection, string modifiedUserID, int dlraID) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.dlraID = dlraID;
			if (this.dlraID > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@DlraID", DataAccessParameterType.Numeric, this.DlraID);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("DLRA_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@DlraID", DataAccessParameterType.Numeric, this.dlraID.ToString());
			data.ExecuteProcedure("DLRA_GetRecord");
			if (!data.EOF)
			{
				this.DlraID = data["DlraID"];
				this.DlraID_Parent = data["DlraID_Parent"];
				this.Hierarchy = data["Hierarchy"];
				this.CodeID_DealerType = data["CodeID_DealerType"];
				this.CodeID_JobType = data["CodeID_JobType"];
				this.JobTypeProviderCode = data["JobTypeProviderCode"];
				this.CodeID_JobType_Used = data["CodeID_JobType_Used"];
				this.JobTypeProviderCode_Used = data["JobTypeProviderCode_Used"];
				this.DealerName = data["DealerName"];
				this.Address1 = data["Address1"];
				this.Address2 = data["Address2"];
				this.City = data["City"];
				this.State = data["State"];
				this.ZipCode = data["ZipCode"];
				this.Phone = data["Phone"];
				this.Fax = data["Fax"];
				this.Email = data["Email"];
				this.Contact = data["Contact"];
				this.Latitude = data["Latitude"];
				this.Longitude = data["Longitude"];
				this.CodeID_DealerCrmSystem = data["CodeID_DealerCrmSystem"];
				this.CrmDealershipID = data["CrmDealershipID"];
				this.CrmToEmail = data["CrmToEmail"];
				this.CodeID_DealerCrmSystem_Used = data["CodeID_DealerCrmSystem_Used"];
				this.CrmDealershipID_Used = data["CrmDealershipID_Used"];
				this.CrmToEmail_Used = data["CrmToEmail_Used"];
				this.LeadsMax = data["LeadsMax"];
				this.LeadsOldestAccepted = data["LeadsOldestAccepted"];
				this.EmplID_AcctMgr = data["EmplID_AcctMgr"];
				this.ActiveYN = data["ActiveYN"];
				this.Salesforceid = data["salesforceid"];
				this.IsCARS = bool.Parse(data["IsCARS"]);
				this.IsWholesaler = Convert.ToBoolean(data["IsWholesaler"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@DlraID", DataAccessParameterType.Numeric, this.DlraID.ToString());
			data.AddParam("@DlraID_Parent", DataAccessParameterType.Numeric, this.DlraID_Parent.ToString());
			data.AddParam("@Hierarchy", DataAccessParameterType.Text, this.Hierarchy);
			data.AddParam("@CodeID_DealerType", DataAccessParameterType.Numeric, this.CodeID_DealerType.ToString());
			data.AddParam("@CodeID_JobType", DataAccessParameterType.Numeric, this.CodeID_JobType.ToString());
			data.AddParam("@JobTypeProviderCode", DataAccessParameterType.Text, this.JobTypeProviderCode);
			data.AddParam("@CodeID_JobType_Used", DataAccessParameterType.Numeric, this.CodeID_JobType_Used.ToString());
			data.AddParam("@JobTypeProviderCode_Used", DataAccessParameterType.Text, this.JobTypeProviderCode_Used);
			data.AddParam("@DealerName", DataAccessParameterType.Text, this.DealerName);
			data.AddParam("@Address1", DataAccessParameterType.Text, this.Address1);
			data.AddParam("@Address2", DataAccessParameterType.Text, this.Address2);
			data.AddParam("@City", DataAccessParameterType.Text, this.City);
			data.AddParam("@State", DataAccessParameterType.Text, this.State);
			data.AddParam("@ZipCode", DataAccessParameterType.Text, this.ZipCode);
			data.AddParam("@Phone", DataAccessParameterType.Text, this.Phone);
			data.AddParam("@Fax", DataAccessParameterType.Text, this.Fax);
			data.AddParam("@Email", DataAccessParameterType.Text, this.Email);
			data.AddParam("@Contact", DataAccessParameterType.Text, this.Contact);
			data.AddParam("@Latitude", DataAccessParameterType.Numeric, this.Latitude.ToString());
			data.AddParam("@Longitude", DataAccessParameterType.Numeric, this.Longitude.ToString());
			data.AddParam("@CodeID_DealerCrmSystem", DataAccessParameterType.Numeric, this.CodeID_DealerCrmSystem.ToString());
			data.AddParam("@CrmDealershipID", DataAccessParameterType.Text, this.CrmDealershipID);
			data.AddParam("@CrmToEmail", DataAccessParameterType.Text, this.CrmToEmail);
			data.AddParam("@CodeID_DealerCrmSystem_Used", DataAccessParameterType.Numeric, this.CodeID_DealerCrmSystem_Used.ToString());
			data.AddParam("@CrmDealershipID_Used", DataAccessParameterType.Text, this.CrmDealershipID_Used);
			data.AddParam("@CrmToEmail_Used", DataAccessParameterType.Text, this.CrmToEmail_Used);
			data.AddParam("@LeadsMax", DataAccessParameterType.Numeric, this.LeadsMax.ToString());
			data.AddParam("@LeadsOldestAccepted", DataAccessParameterType.Text, this.LeadsOldestAccepted);
			data.AddParam("@EmplID_AcctMgr", DataAccessParameterType.Numeric, this.EmplID_AcctMgr.ToString());
			data.AddParam("@ActiveYN", DataAccessParameterType.Text, this.ActiveYN);
			data.AddParam("@salesforceid", DataAccessParameterType.Text, "AUTOLANDONLY");
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			bool isCARS = this.IsCARS;
			data.AddParam("@IsCARS", DataAccessParameterType.Bool, isCARS.ToString());
			isCARS = this.IsWholesaler;
			data.AddParam("@IsWholesaler", DataAccessParameterType.Bool, isCARS.ToString());
			data.ExecuteProcedure("DLRA_InsertUpdate");
			if (!data.EOF)
			{
				this.DlraID = data["DlraID"];
			}
			this.retrievedata();
		}
	}
}