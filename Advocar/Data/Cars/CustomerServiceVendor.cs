using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Cars
{
	public class CustomerServiceVendor : Transaction
	{
		private int cdcvID = 0;

		private int oldCode = 0;

		private string description = string.Empty;

		private string address = string.Empty;

		private string address2 = string.Empty;

		private string city = string.Empty;

		private string state = string.Empty;

		private string zipCode = string.Empty;

		private string phone = string.Empty;

		private string phoneOther = string.Empty;

		private string fax = string.Empty;

		private string contactName = string.Empty;

		private string contactEmail = string.Empty;

		public string Address
		{
			get
			{
				return this.address;
			}
			set
			{
				this.address = value;
			}
		}

		public string Address2
		{
			get
			{
				return this.address2;
			}
			set
			{
				this.address2 = value;
			}
		}

		public int CdcvID
		{
			get
			{
				return this.cdcvID;
			}
			set
			{
				this.cdcvID = value;
			}
		}

		public string City
		{
			get
			{
				return this.city;
			}
			set
			{
				this.city = value;
			}
		}

		public string ContactEmail
		{
			get
			{
				return this.contactEmail;
			}
			set
			{
				this.contactEmail = value;
			}
		}

		public string ContactName
		{
			get
			{
				return this.contactName;
			}
			set
			{
				this.contactName = value;
			}
		}

		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

		public string Fax
		{
			get
			{
				return this.fax;
			}
			set
			{
				this.fax = value;
			}
		}

		public int OldCode
		{
			get
			{
				return this.oldCode;
			}
			set
			{
				this.oldCode = value;
			}
		}

		public string Phone
		{
			get
			{
				return this.phone;
			}
			set
			{
				this.phone = value;
			}
		}

		public string PhoneOther
		{
			get
			{
				return this.phoneOther;
			}
			set
			{
				this.phoneOther = value;
			}
		}

		public string State
		{
			get
			{
				return this.state;
			}
			set
			{
				this.state = value;
			}
		}

		public string ZipCode
		{
			get
			{
				return this.zipCode;
			}
			set
			{
				this.zipCode = value;
			}
		}

		public CustomerServiceVendor(string connection, string modifiedUser, int cdcvID) : base(connection, modifiedUser)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUser;
			this.cdcvID = cdcvID;
			if (this.cdcvID > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess dataAccess = new DataAccess(this.connection);
			dataAccess.AddParam("@CdcvID", DataAccessParameterType.Numeric, this.cdcvID.ToString(), true);
			dataAccess.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID, true);
			dataAccess.ExecuteProcedure("CDCV_Delete");
		}

		protected override void retrievedata()
		{
			DataAccess dataAccess = new DataAccess(this.connection);
			dataAccess.AddParam("@CdcvID", DataAccessParameterType.Numeric, this.cdcvID.ToString(), true);
			dataAccess.ExecuteProcedure("CDCV_GetRecord");
			if (!dataAccess.EOF)
			{
				this.cdcvID = (Validation.IsNumeric(dataAccess["CdcvID"]) ? Convert.ToInt32(dataAccess["CdcvID"]) : 0);
				this.oldCode = (Validation.IsNumeric(dataAccess["OldCode"]) ? Convert.ToInt32(dataAccess["OldCode"]) : 0);
				this.description = dataAccess["Description"];
				this.address = dataAccess["Address"];
				this.address2 = dataAccess["Address2"];
				this.city = dataAccess["City"];
				this.state = dataAccess["State"];
				this.zipCode = dataAccess["ZipCode"];
				this.phone = dataAccess["Phone"];
				this.phoneOther = dataAccess["PhoneOther"];
				this.fax = dataAccess["Fax"];
				this.contactName = dataAccess["ContactName"];
				this.contactEmail = dataAccess["ContactEmail"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess dataAccess = new DataAccess(this.connection);
			dataAccess.AddParam("@CdcvID", DataAccessParameterType.Numeric, this.cdcvID.ToString(), true);
			dataAccess.AddParam("@OldCode", DataAccessParameterType.Numeric, this.oldCode.ToString(), true);
			dataAccess.AddParam("@Description", DataAccessParameterType.Text, this.description, true);
			dataAccess.AddParam("@Address", DataAccessParameterType.Text, this.address, true);
			dataAccess.AddParam("@Address2", DataAccessParameterType.Text, this.address2, true);
			dataAccess.AddParam("@City", DataAccessParameterType.Text, this.city, true);
			dataAccess.AddParam("@State", DataAccessParameterType.Text, this.state, true);
			dataAccess.AddParam("@ZipCode", DataAccessParameterType.Text, this.zipCode, true);
			dataAccess.AddParam("@Phone", DataAccessParameterType.Text, this.phone, true);
			dataAccess.AddParam("@PhoneOther", DataAccessParameterType.Text, this.phoneOther, true);
			dataAccess.AddParam("@Fax", DataAccessParameterType.Text, this.fax, true);
			dataAccess.AddParam("@ContactName", DataAccessParameterType.Text, this.contactName, true);
			dataAccess.AddParam("@ContactEmail", DataAccessParameterType.Text, this.contactEmail, true);
			dataAccess.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID, true);
			dataAccess.ExecuteProcedure("CDCV_InsertUpdate");
			if (!dataAccess.EOF)
			{
				this.cdcvID = (Validation.IsNumeric(dataAccess["CdcvID"]) ? Convert.ToInt32(dataAccess["CdcvID"]) : 0);
				this.retrievedata();
			}
		}
	}
}