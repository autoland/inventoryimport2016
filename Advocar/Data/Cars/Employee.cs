using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Cars
{
	public class Employee : Transaction
	{
		private int emplID = 0;

		private string userID_Employee = "";

		private int cmerID = 0;

		private int cmedID = 0;

		private int cmeoID = 0;

		private int cmeoID_2 = 0;

		private int cmeoID_3 = 0;

		private int cmeoID_4 = 0;

		private bool isConsultant = false;

		private string consultantStartDate = "";

		private int dlraID = 0;

		private string firstName = "";

		private string lastName = "";

		private string email = "";

		private string phone = "";

		private string fax = "";

		private int adpCode = 0;

		private string cmerDescription = "";

		private string cmedDescription = "";

		private string cmeoDescription = "";

		private string dlraDealerName = "";

		public string AdpCode
		{
			get
			{
				return this.adpCode.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.adpCode = 0;
				}
				else
				{
					this.adpCode = int.Parse(value);
				}
			}
		}

		public string CmedDescription
		{
			get
			{
				return this.cmedDescription;
			}
		}

		public string CmedID
		{
			get
			{
				return this.cmedID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.cmedID = 0;
				}
				else
				{
					this.cmedID = int.Parse(value);
				}
			}
		}

		public string CmeoDescription
		{
			get
			{
				return this.cmeoDescription;
			}
		}

		public string CmeoID
		{
			get
			{
				return this.cmeoID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.cmeoID = 0;
				}
				else
				{
					this.cmeoID = int.Parse(value);
				}
			}
		}

		public string CmeoID_2
		{
			get
			{
				return this.cmeoID_2.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.cmeoID_2 = 0;
				}
				else
				{
					this.cmeoID_2 = int.Parse(value);
				}
			}
		}

		public string CmeoID_3
		{
			get
			{
				return this.cmeoID_3.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.cmeoID_3 = 0;
				}
				else
				{
					this.cmeoID_3 = int.Parse(value);
				}
			}
		}

		public string CmeoID_4
		{
			get
			{
				return this.cmeoID_4.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.cmeoID_4 = 0;
				}
				else
				{
					this.cmeoID_4 = int.Parse(value);
				}
			}
		}

		public string CmerDescription
		{
			get
			{
				return this.cmerDescription;
			}
		}

		public string CmerID
		{
			get
			{
				return this.cmerID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.cmerID = 0;
				}
				else
				{
					this.cmerID = int.Parse(value);
				}
			}
		}

		public string ConsultantStartDate
		{
			get
			{
				return this.consultantStartDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.consultantStartDate = "";
				}
				else
				{
					this.consultantStartDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string DlraDealerName
		{
			get
			{
				return this.dlraDealerName;
			}
		}

		public string DlraID
		{
			get
			{
				return this.dlraID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.dlraID = 0;
				}
				else
				{
					this.dlraID = int.Parse(value);
				}
			}
		}

		public string Email
		{
			get
			{
				return this.email;
			}
			set
			{
				this.email = value;
			}
		}

		public string EmplID
		{
			get
			{
				return this.emplID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.emplID = 0;
				}
				else
				{
					this.emplID = int.Parse(value);
				}
			}
		}

		public string Fax
		{
			get
			{
				return this.fax;
			}
			set
			{
				this.fax = value;
			}
		}

		public string FirstName
		{
			get
			{
				return this.firstName;
			}
			set
			{
				this.firstName = value;
			}
		}

		public bool IsConsultant
		{
			get
			{
				return this.isConsultant;
			}
			set
			{
				this.isConsultant = value;
			}
		}

		public string LastName
		{
			get
			{
				return this.lastName;
			}
			set
			{
				this.lastName = value;
			}
		}

		public string Phone
		{
			get
			{
				return this.phone;
			}
			set
			{
				this.phone = value;
			}
		}

		public string UserID_Employee
		{
			get
			{
				return this.userID_Employee;
			}
			set
			{
				this.userID_Employee = value;
			}
		}

		public Employee()
		{
		}

		public Employee(string connection, string modifiedUserID, int emplID) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.emplID = emplID;
			if (this.emplID > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@EmplID", DataAccessParameterType.Numeric, this.EmplID);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("EMPL_Delete");
			this.wipeout();
		}

		public static int GetRecordId(string userId, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.ExecuteStatement(string.Concat("SELECT EMPL_ID FROM EMPL_Employees WHERE USER_ID_Employee = '", userId.ToUpper(), "' AND EMPL_DeletedYN = 'N'"));
			num = (data.EOF ? 0 : int.Parse(data["EMPL_ID"]));
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@EmplID", DataAccessParameterType.Numeric, this.emplID.ToString());
			data.ExecuteProcedure("EMPL_GetRecord");
			if (!data.EOF)
			{
				this.EmplID = data["EmplID"];
				this.UserID_Employee = data["UserID_Employee"];
				this.CmerID = data["CmerID"];
				this.CmedID = data["CmedID"];
				this.CmeoID = data["CmeoID"];
				this.CmeoID_2 = data["CmeoID_2"];
				this.CmeoID_3 = data["CmeoID_3"];
				this.CmeoID_4 = data["CmeoID_4"];
				this.DlraID = data["DlraID"];
				this.FirstName = data["FirstName"];
				this.LastName = data["LastName"];
				this.Email = data["Email"];
				this.Phone = data["Phone"];
				this.Fax = data["Fax"];
				this.AdpCode = data["AdpCode"];
				this.ConsultantStartDate = data["ConsultantStartDate"];
				this.IsConsultant = (data["IsConsultant"] == "1" ? true : false);
				this.cmerDescription = data["EmployeeRegion"];
				this.cmedDescription = data["EmployeeDivision"];
				this.cmeoDescription = data["EmployeeOffice"];
				this.dlraDealerName = data["DlraDealerName"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@EmplID", DataAccessParameterType.Numeric, this.EmplID.ToString());
			data.AddParam("@UserID_Employee", DataAccessParameterType.Text, this.UserID_Employee);
			data.AddParam("@CmerID", DataAccessParameterType.Numeric, this.CmerID.ToString());
			data.AddParam("@CmedID", DataAccessParameterType.Numeric, this.CmedID.ToString());
			data.AddParam("@CmeoID", DataAccessParameterType.Numeric, this.CmeoID.ToString());
			data.AddParam("@CmeoID_2", DataAccessParameterType.Numeric, this.CmeoID_2.ToString());
			data.AddParam("@CmeoID_3", DataAccessParameterType.Numeric, this.CmeoID_3.ToString());
			data.AddParam("@CmeoID_4", DataAccessParameterType.Numeric, this.CmeoID_4.ToString());
			data.AddParam("@ConsultantStartDate", DataAccessParameterType.DateTime, this.ConsultantStartDate);
			data.AddParam("@DlraID", DataAccessParameterType.Numeric, this.DlraID.ToString());
			data.AddParam("@FirstName", DataAccessParameterType.Text, this.FirstName);
			data.AddParam("@LastName", DataAccessParameterType.Text, this.LastName);
			data.AddParam("@Email", DataAccessParameterType.Text, this.Email);
			data.AddParam("@Phone", DataAccessParameterType.Text, this.Phone);
			data.AddParam("@Fax", DataAccessParameterType.Text, this.Fax);
			data.AddParam("@AdpCode", DataAccessParameterType.Numeric, this.AdpCode.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("EMPL_InsertUpdate");
			if (!data.EOF)
			{
				this.EmplID = data["EmplID"];
			}
			this.retrievedata();
		}
	}
}