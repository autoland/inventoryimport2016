using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Cars
{
	public class Activity : Transaction
	{
		private int actaID = 0;

		private int cmftID = 0;

		private int membID = 0;

		private int agraID = 0;

		private int coatID = 0;

		private string sectionShow = "";

		public string ActaID
		{
			get
			{
				return this.actaID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.actaID = 0;
				}
				else
				{
					this.actaID = int.Parse(value);
				}
			}
		}

		public string AgraID
		{
			get
			{
				return this.agraID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.agraID = 0;
				}
				else
				{
					this.agraID = int.Parse(value);
				}
			}
		}

		public string CmftID
		{
			get
			{
				return this.cmftID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.cmftID = 0;
				}
				else
				{
					this.cmftID = int.Parse(value);
				}
			}
		}

		public string CoatID
		{
			get
			{
				return this.coatID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.coatID = 0;
				}
				else
				{
					this.coatID = int.Parse(value);
				}
			}
		}

		public string MembID
		{
			get
			{
				return this.membID.ToString();
			}
			set
			{
				if (!Validation.IsNumeric(value))
				{
					this.membID = 0;
				}
				else
				{
					this.membID = int.Parse(value);
				}
			}
		}

		public string SectionShow
		{
			get
			{
				return this.sectionShow;
			}
			set
			{
				this.sectionShow = value;
			}
		}

		public Activity()
		{
		}

		public Activity(string connection, string modifiedUserID, int actaID) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.actaID = actaID;
			if (this.actaID > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ActaID", DataAccessParameterType.Numeric, this.ActaID);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("ACTA_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ActaID", DataAccessParameterType.Numeric, this.actaID.ToString());
			data.ExecuteProcedure("ACTA_GetRecord");
			if (!data.EOF)
			{
				this.ActaID = data["ActaID"];
				this.CmftID = data["CmftID"];
				this.MembID = data["MembID"];
				this.AgraID = data["AgraID"];
				this.CoatID = data["CoatID"];
				this.SectionShow = data["SectionShow"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@ActaID", DataAccessParameterType.Numeric, this.ActaID.ToString());
			data.AddParam("@CmftID", DataAccessParameterType.Numeric, this.CmftID.ToString());
			data.AddParam("@MembID", DataAccessParameterType.Numeric, this.MembID.ToString());
			data.AddParam("@AgraID", DataAccessParameterType.Numeric, this.AgraID.ToString());
			data.AddParam("@CoatID", DataAccessParameterType.Numeric, this.CoatID.ToString());
			data.AddParam("@SectionShow", DataAccessParameterType.Text, this.SectionShow);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("ACTA_InsertUpdate");
			if (!data.EOF)
			{
				this.ActaID = data["ActaID"];
			}
			this.retrievedata();
		}
	}
}