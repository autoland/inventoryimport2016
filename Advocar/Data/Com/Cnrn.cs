using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Com
{
	public class Cnrn : Transaction
	{
		private int cnrnId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int cntlId = 0;

		private string loanNewUsed = "";

		private string loanTerm = "";

		private string applicantType = "";

		private float requestedLoanAmount = 0f;

		private float downPayment = 0f;

		private int configurator_VehicleYearId = 0;

		private int configurator_VehicleMakeId = 0;

		private string configuratorMakeName = "";

		private int configurator_VehicleModelId = 0;

		private string configuratorModelName = "";

		private int vehicleMileage = 0;

		private string vIN = "";

		private string firstName = "";

		private string lastName = "";

		private string sSN = "";

		private string dateOfBirth = "";

		private string phoneHome = "";

		private string phoneWork = "";

		private string phoneCell = "";

		private string email = "";

		private string licenseNumber = "";

		private string licenseStateCode = "";

		private bool isBankruptcy = false;

		private bool isFelony = false;

		private string address1 = "";

		private string address2 = "";

		private string city = "";

		private string stateCode = "";

		private string zipCode = "";

		private string residenceStatus = "";

		private float monthlyRentMortgage = 0f;

		private int timeAtAddressYears = 0;

		private int timeAtAddressMonths = 0;

		private string workStatus = "";

		private string employerName = "";

		private string employerAddress = "";

		private string employerCity = "";

		private string employerStateCode = "";

		private string employerZipCode = "";

		private string workOccupation = "";

		private int lengthOfEmploymentYears = 0;

		private int lengthOfEmploymentMonths = 0;

		private float incomeMonthlyGross = 0f;

		private string incomeOtherSource = "";

		private float incomeOther = 0f;

		private string comments = "";

		public string Address1
		{
			get
			{
				return this.address1;
			}
			set
			{
				this.address1 = value;
			}
		}

		public string Address2
		{
			get
			{
				return this.address2;
			}
			set
			{
				this.address2 = value;
			}
		}

		public string ApplicantType
		{
			get
			{
				return this.applicantType;
			}
			set
			{
				this.applicantType = value;
			}
		}

		public string City
		{
			get
			{
				return this.city;
			}
			set
			{
				this.city = value;
			}
		}

		public int CnrnId
		{
			get
			{
				return this.cnrnId;
			}
			set
			{
				this.cnrnId = value;
			}
		}

		public int CntlId
		{
			get
			{
				return this.cntlId;
			}
			set
			{
				this.cntlId = value;
			}
		}

		public string Comments
		{
			get
			{
				return this.comments;
			}
			set
			{
				this.comments = value;
			}
		}

		public int Configurator_VehicleMakeId
		{
			get
			{
				return this.configurator_VehicleMakeId;
			}
			set
			{
				this.configurator_VehicleMakeId = value;
			}
		}

		public int Configurator_VehicleModelId
		{
			get
			{
				return this.configurator_VehicleModelId;
			}
			set
			{
				this.configurator_VehicleModelId = value;
			}
		}

		public int Configurator_VehicleYearId
		{
			get
			{
				return this.configurator_VehicleYearId;
			}
			set
			{
				this.configurator_VehicleYearId = value;
			}
		}

		public string ConfiguratorMakeName
		{
			get
			{
				return this.configuratorMakeName;
			}
			set
			{
				this.configuratorMakeName = value;
			}
		}

		public string ConfiguratorModelName
		{
			get
			{
				return this.configuratorModelName;
			}
			set
			{
				this.configuratorModelName = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string DateOfBirth
		{
			get
			{
				return this.dateOfBirth;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.dateOfBirth = "";
				}
				else
				{
					this.dateOfBirth = DateTime.Parse(value).ToString();
				}
			}
		}

		public float DownPayment
		{
			get
			{
				return this.downPayment;
			}
			set
			{
				this.downPayment = value;
			}
		}

		public string Email
		{
			get
			{
				return this.email;
			}
			set
			{
				this.email = value;
			}
		}

		public string EmployerAddress
		{
			get
			{
				return this.employerAddress;
			}
			set
			{
				this.employerAddress = value;
			}
		}

		public string EmployerCity
		{
			get
			{
				return this.employerCity;
			}
			set
			{
				this.employerCity = value;
			}
		}

		public string EmployerName
		{
			get
			{
				return this.employerName;
			}
			set
			{
				this.employerName = value;
			}
		}

		public string EmployerStateCode
		{
			get
			{
				return this.employerStateCode;
			}
			set
			{
				this.employerStateCode = value;
			}
		}

		public string EmployerZipCode
		{
			get
			{
				return this.employerZipCode;
			}
			set
			{
				this.employerZipCode = value;
			}
		}

		public string FirstName
		{
			get
			{
				return this.firstName;
			}
			set
			{
				this.firstName = value;
			}
		}

		public float IncomeMonthlyGross
		{
			get
			{
				return this.incomeMonthlyGross;
			}
			set
			{
				this.incomeMonthlyGross = value;
			}
		}

		public float IncomeOther
		{
			get
			{
				return this.incomeOther;
			}
			set
			{
				this.incomeOther = value;
			}
		}

		public string IncomeOtherSource
		{
			get
			{
				return this.incomeOtherSource;
			}
			set
			{
				this.incomeOtherSource = value;
			}
		}

		public bool IsBankruptcy
		{
			get
			{
				return this.isBankruptcy;
			}
			set
			{
				this.isBankruptcy = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public bool IsFelony
		{
			get
			{
				return this.isFelony;
			}
			set
			{
				this.isFelony = value;
			}
		}

		public string LastName
		{
			get
			{
				return this.lastName;
			}
			set
			{
				this.lastName = value;
			}
		}

		public int LengthOfEmploymentMonths
		{
			get
			{
				return this.lengthOfEmploymentMonths;
			}
			set
			{
				this.lengthOfEmploymentMonths = value;
			}
		}

		public int LengthOfEmploymentYears
		{
			get
			{
				return this.lengthOfEmploymentYears;
			}
			set
			{
				this.lengthOfEmploymentYears = value;
			}
		}

		public string LicenseNumber
		{
			get
			{
				return this.licenseNumber;
			}
			set
			{
				this.licenseNumber = value;
			}
		}

		public string LicenseStateCode
		{
			get
			{
				return this.licenseStateCode;
			}
			set
			{
				this.licenseStateCode = value;
			}
		}

		public string LoanNewUsed
		{
			get
			{
				return this.loanNewUsed;
			}
			set
			{
				this.loanNewUsed = value;
			}
		}

		public string LoanTerm
		{
			get
			{
				return this.loanTerm;
			}
			set
			{
				this.loanTerm = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public float MonthlyRentMortgage
		{
			get
			{
				return this.monthlyRentMortgage;
			}
			set
			{
				this.monthlyRentMortgage = value;
			}
		}

		public string PhoneCell
		{
			get
			{
				return this.phoneCell;
			}
			set
			{
				this.phoneCell = value;
			}
		}

		public string PhoneHome
		{
			get
			{
				return this.phoneHome;
			}
			set
			{
				this.phoneHome = value;
			}
		}

		public string PhoneWork
		{
			get
			{
				return this.phoneWork;
			}
			set
			{
				this.phoneWork = value;
			}
		}

		public float RequestedLoanAmount
		{
			get
			{
				return this.requestedLoanAmount;
			}
			set
			{
				this.requestedLoanAmount = value;
			}
		}

		public string ResidenceStatus
		{
			get
			{
				return this.residenceStatus;
			}
			set
			{
				this.residenceStatus = value;
			}
		}

		public string SSN
		{
			get
			{
				return this.sSN;
			}
			set
			{
				this.sSN = value;
			}
		}

		public string StateCode
		{
			get
			{
				return this.stateCode;
			}
			set
			{
				this.stateCode = value;
			}
		}

		public int TimeAtAddressMonths
		{
			get
			{
				return this.timeAtAddressMonths;
			}
			set
			{
				this.timeAtAddressMonths = value;
			}
		}

		public int TimeAtAddressYears
		{
			get
			{
				return this.timeAtAddressYears;
			}
			set
			{
				this.timeAtAddressYears = value;
			}
		}

		public int VehicleMileage
		{
			get
			{
				return this.vehicleMileage;
			}
			set
			{
				this.vehicleMileage = value;
			}
		}

		public string VIN
		{
			get
			{
				return this.vIN;
			}
			set
			{
				this.vIN = value;
			}
		}

		public string WorkOccupation
		{
			get
			{
				return this.workOccupation;
			}
			set
			{
				this.workOccupation = value;
			}
		}

		public string WorkStatus
		{
			get
			{
				return this.workStatus;
			}
			set
			{
				this.workStatus = value;
			}
		}

		public string ZipCode
		{
			get
			{
				return this.zipCode;
			}
			set
			{
				this.zipCode = value;
			}
		}

		public Cnrn()
		{
		}

		public Cnrn(string connection, string modifiedUserID, int cnrnId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "CNRN";
			this.CnrnId = cnrnId;
			if (this.CnrnId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@CnrnId", DataAccessParameterType.Numeric, this.CnrnId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("CNRN_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@CnrnId", DataAccessParameterType.Numeric, this.CnrnId.ToString());
			data.ExecuteProcedure("CNRN_GetRecord");
			if (!data.EOF)
			{
				this.CnrnId = int.Parse(data["CnrnId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.CntlId = int.Parse(data["CntlId"]);
				this.LoanNewUsed = data["LoanNewUsed"];
				this.LoanTerm = data["LoanTerm"];
				this.ApplicantType = data["ApplicantType"];
				this.RequestedLoanAmount = float.Parse(data["RequestedLoanAmount"]);
				this.DownPayment = float.Parse(data["DownPayment"]);
				this.Configurator_VehicleYearId = int.Parse(data["Configurator_VehicleYearId"]);
				this.Configurator_VehicleMakeId = int.Parse(data["Configurator_VehicleMakeId"]);
				this.ConfiguratorMakeName = data["ConfiguratorMakeName"];
				this.Configurator_VehicleModelId = int.Parse(data["Configurator_VehicleModelId"]);
				this.ConfiguratorModelName = data["ConfiguratorModelName"];
				this.VehicleMileage = int.Parse(data["VehicleMileage"]);
				this.VIN = data["VIN"];
				this.FirstName = data["FirstName"];
				this.LastName = data["LastName"];
				this.SSN = data["SSN"];
				this.DateOfBirth = data["DateOfBirth"];
				this.PhoneHome = data["PhoneHome"];
				this.PhoneWork = data["PhoneWork"];
				this.PhoneCell = data["PhoneCell"];
				this.Email = data["Email"];
				this.LicenseNumber = data["LicenseNumber"];
				this.LicenseStateCode = data["LicenseStateCode"];
				this.IsBankruptcy = bool.Parse(data["IsBankruptcy"]);
				this.IsFelony = bool.Parse(data["IsFelony"]);
				this.Address1 = data["Address1"];
				this.Address2 = data["Address2"];
				this.City = data["City"];
				this.StateCode = data["StateCode"];
				this.ZipCode = data["ZipCode"];
				this.ResidenceStatus = data["ResidenceStatus"];
				this.MonthlyRentMortgage = float.Parse(data["MonthlyRentMortgage"]);
				this.TimeAtAddressYears = int.Parse(data["TimeAtAddressYears"]);
				this.TimeAtAddressMonths = int.Parse(data["TimeAtAddressMonths"]);
				this.WorkStatus = data["WorkStatus"];
				this.EmployerName = data["EmployerName"];
				this.EmployerAddress = data["EmployerAddress"];
				this.EmployerCity = data["EmployerCity"];
				this.EmployerStateCode = data["EmployerStateCode"];
				this.EmployerZipCode = data["EmployerZipCode"];
				this.WorkOccupation = data["WorkOccupation"];
				this.LengthOfEmploymentYears = int.Parse(data["LengthOfEmploymentYears"]);
				this.LengthOfEmploymentMonths = int.Parse(data["LengthOfEmploymentMonths"]);
				this.IncomeMonthlyGross = float.Parse(data["IncomeMonthlyGross"]);
				this.IncomeOtherSource = data["IncomeOtherSource"];
				this.IncomeOther = float.Parse(data["IncomeOther"]);
				this.Comments = data["Comments"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int cnrnId = this.CnrnId;
			data.AddParam("@CnrnId", DataAccessParameterType.Numeric, cnrnId.ToString());
			cnrnId = this.CntlId;
			data.AddParam("@CntlId", DataAccessParameterType.Numeric, cnrnId.ToString());
			data.AddParam("@LoanNewUsed", DataAccessParameterType.Text, this.LoanNewUsed);
			data.AddParam("@LoanTerm", DataAccessParameterType.Text, this.LoanTerm);
			data.AddParam("@ApplicantType", DataAccessParameterType.Text, this.ApplicantType);
			float requestedLoanAmount = this.RequestedLoanAmount;
			data.AddParam("@RequestedLoanAmount", DataAccessParameterType.Numeric, requestedLoanAmount.ToString());
			requestedLoanAmount = this.DownPayment;
			data.AddParam("@DownPayment", DataAccessParameterType.Numeric, requestedLoanAmount.ToString());
			cnrnId = this.Configurator_VehicleYearId;
			data.AddParam("@Configurator_VehicleYearId", DataAccessParameterType.Numeric, cnrnId.ToString());
			cnrnId = this.Configurator_VehicleMakeId;
			data.AddParam("@Configurator_VehicleMakeId", DataAccessParameterType.Numeric, cnrnId.ToString());
			data.AddParam("@ConfiguratorMakeName", DataAccessParameterType.Text, this.ConfiguratorMakeName);
			cnrnId = this.Configurator_VehicleModelId;
			data.AddParam("@Configurator_VehicleModelId", DataAccessParameterType.Numeric, cnrnId.ToString());
			data.AddParam("@ConfiguratorModelName", DataAccessParameterType.Text, this.ConfiguratorModelName);
			cnrnId = this.VehicleMileage;
			data.AddParam("@VehicleMileage", DataAccessParameterType.Numeric, cnrnId.ToString());
			data.AddParam("@VIN", DataAccessParameterType.Text, this.VIN);
			data.AddParam("@FirstName", DataAccessParameterType.Text, this.FirstName);
			data.AddParam("@LastName", DataAccessParameterType.Text, this.LastName);
			data.AddParam("@SSN", DataAccessParameterType.Text, this.SSN);
			data.AddParam("@DateOfBirth", DataAccessParameterType.DateTime, this.DateOfBirth);
			data.AddParam("@PhoneHome", DataAccessParameterType.Text, this.PhoneHome);
			data.AddParam("@PhoneWork", DataAccessParameterType.Text, this.PhoneWork);
			data.AddParam("@PhoneCell", DataAccessParameterType.Text, this.PhoneCell);
			data.AddParam("@Email", DataAccessParameterType.Text, this.Email);
			data.AddParam("@LicenseNumber", DataAccessParameterType.Text, this.LicenseNumber);
			data.AddParam("@LicenseStateCode", DataAccessParameterType.Text, this.LicenseStateCode);
			bool isBankruptcy = this.IsBankruptcy;
			data.AddParam("@IsBankruptcy", DataAccessParameterType.Bool, isBankruptcy.ToString());
			isBankruptcy = this.IsFelony;
			data.AddParam("@IsFelony", DataAccessParameterType.Bool, isBankruptcy.ToString());
			data.AddParam("@Address1", DataAccessParameterType.Text, this.Address1);
			data.AddParam("@Address2", DataAccessParameterType.Text, this.Address2);
			data.AddParam("@City", DataAccessParameterType.Text, this.City);
			data.AddParam("@StateCode", DataAccessParameterType.Text, this.StateCode);
			data.AddParam("@ZipCode", DataAccessParameterType.Text, this.ZipCode);
			data.AddParam("@ResidenceStatus", DataAccessParameterType.Text, this.ResidenceStatus);
			requestedLoanAmount = this.MonthlyRentMortgage;
			data.AddParam("@MonthlyRentMortgage", DataAccessParameterType.Numeric, requestedLoanAmount.ToString());
			cnrnId = this.TimeAtAddressYears;
			data.AddParam("@TimeAtAddressYears", DataAccessParameterType.Numeric, cnrnId.ToString());
			cnrnId = this.TimeAtAddressMonths;
			data.AddParam("@TimeAtAddressMonths", DataAccessParameterType.Numeric, cnrnId.ToString());
			data.AddParam("@WorkStatus", DataAccessParameterType.Text, this.WorkStatus);
			data.AddParam("@EmployerName", DataAccessParameterType.Text, this.EmployerName);
			data.AddParam("@EmployerAddress", DataAccessParameterType.Text, this.EmployerAddress);
			data.AddParam("@EmployerCity", DataAccessParameterType.Text, this.EmployerCity);
			data.AddParam("@EmployerStateCode", DataAccessParameterType.Text, this.EmployerStateCode);
			data.AddParam("@EmployerZipCode", DataAccessParameterType.Text, this.EmployerZipCode);
			data.AddParam("@WorkOccupation", DataAccessParameterType.Text, this.WorkOccupation);
			cnrnId = this.LengthOfEmploymentYears;
			data.AddParam("@LengthOfEmploymentYears", DataAccessParameterType.Numeric, cnrnId.ToString());
			cnrnId = this.LengthOfEmploymentMonths;
			data.AddParam("@LengthOfEmploymentMonths", DataAccessParameterType.Numeric, cnrnId.ToString());
			requestedLoanAmount = this.IncomeMonthlyGross;
			data.AddParam("@IncomeMonthlyGross", DataAccessParameterType.Numeric, requestedLoanAmount.ToString());
			data.AddParam("@IncomeOtherSource", DataAccessParameterType.Text, this.IncomeOtherSource);
			requestedLoanAmount = this.IncomeOther;
			data.AddParam("@IncomeOther", DataAccessParameterType.Numeric, requestedLoanAmount.ToString());
			data.AddParam("@Comments", DataAccessParameterType.Text, this.Comments);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("CNRN_InsertUpdate");
			if (!data.EOF)
			{
				this.CnrnId = int.Parse(data["CnrnId"]);
			}
			this.retrievedata();
		}
	}
}