using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.Com
{
	public class VehicleAd : Transaction
	{
		private int uvadId = 0;

		private int agrpId = 0;

		private int membId = 0;

		private string addate = "";

		private string vehicleYear = "";

		private int uvmkId = 0;

		private int uvmdId = 0;

		private int uvenId = 0;

		private string engine = "";

		private int uvtrId = 0;

		private string transmission = "";

		private int uvftId = 0;

		private string fuelType = "";

		private int uvstId = 0;

		private string vehicleTrim = "";

		private string makeName = "";

		private string modelName = "";

		private string vehicleStyle = "";

		private string colorExterior = "";

		private string colorInterior = "";

		private int miles = 0;

		private float sellingPrice = 0f;

		private int uvprId = 0;

		private string notes = "";

		private string firstName = "";

		private string lastName = "";

		private string phone = "";

		private string phoneOther = "";

		private string email = "";

		private string city = "";

		private string stateCode = "";

		private string zipCode = "";

		private float latitude = 0f;

		private float longitude = 0f;

		private string status = "";

		private string userApprovedbyid = "";

		public string Addate
		{
			get
			{
				return this.addate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.addate = "";
				}
				else
				{
					this.addate = DateTime.Parse(value).ToString();
				}
			}
		}

		public int AgrpId
		{
			get
			{
				return this.agrpId;
			}
			set
			{
				this.agrpId = value;
			}
		}

		public string City
		{
			get
			{
				return this.city;
			}
			set
			{
				this.city = value;
			}
		}

		public string ColorExterior
		{
			get
			{
				return this.colorExterior;
			}
			set
			{
				this.colorExterior = value;
			}
		}

		public string ColorInterior
		{
			get
			{
				return this.colorInterior;
			}
			set
			{
				this.colorInterior = value;
			}
		}

		public string Email
		{
			get
			{
				return this.email;
			}
			set
			{
				this.email = value;
			}
		}

		public string Engine
		{
			get
			{
				return this.engine;
			}
			set
			{
				this.engine = value;
			}
		}

		public string FirstName
		{
			get
			{
				return this.firstName;
			}
			set
			{
				this.firstName = value;
			}
		}

		public string FuelType
		{
			get
			{
				return this.fuelType;
			}
			set
			{
				this.fuelType = value;
			}
		}

		public string LastName
		{
			get
			{
				return this.lastName;
			}
			set
			{
				this.lastName = value;
			}
		}

		public float Latitude
		{
			get
			{
				return this.latitude;
			}
			set
			{
				this.latitude = value;
			}
		}

		public float Longitude
		{
			get
			{
				return this.longitude;
			}
			set
			{
				this.longitude = value;
			}
		}

		public string MakeName
		{
			get
			{
				return this.makeName;
			}
			set
			{
				this.makeName = value;
			}
		}

		public int MembId
		{
			get
			{
				return this.membId;
			}
			set
			{
				this.membId = value;
			}
		}

		public int Miles
		{
			get
			{
				return this.miles;
			}
			set
			{
				this.miles = value;
			}
		}

		public string ModelName
		{
			get
			{
				return this.modelName;
			}
			set
			{
				this.modelName = value;
			}
		}

		public string Notes
		{
			get
			{
				return this.notes;
			}
			set
			{
				this.notes = value;
			}
		}

		public string Phone
		{
			get
			{
				return this.phone;
			}
			set
			{
				this.phone = value;
			}
		}

		public string PhoneOther
		{
			get
			{
				return this.phoneOther;
			}
			set
			{
				this.phoneOther = value;
			}
		}

		public float SellingPrice
		{
			get
			{
				return this.sellingPrice;
			}
			set
			{
				this.sellingPrice = value;
			}
		}

		public string StateCode
		{
			get
			{
				return this.stateCode;
			}
			set
			{
				this.stateCode = value;
			}
		}

		public string Status
		{
			get
			{
				return this.status;
			}
			set
			{
				this.status = value;
			}
		}

		public string Transmission
		{
			get
			{
				return this.transmission;
			}
			set
			{
				this.transmission = value;
			}
		}

		public string UserApprovedbyid
		{
			get
			{
				return this.userApprovedbyid;
			}
			set
			{
				this.userApprovedbyid = value;
			}
		}

		public int UvadId
		{
			get
			{
				return this.uvadId;
			}
			set
			{
				this.uvadId = value;
			}
		}

		public int UvenId
		{
			get
			{
				return this.uvenId;
			}
			set
			{
				this.uvenId = value;
			}
		}

		public int UvftId
		{
			get
			{
				return this.uvftId;
			}
			set
			{
				this.uvftId = value;
			}
		}

		public int UvmdId
		{
			get
			{
				return this.uvmdId;
			}
			set
			{
				this.uvmdId = value;
			}
		}

		public int UvmkId
		{
			get
			{
				return this.uvmkId;
			}
			set
			{
				this.uvmkId = value;
			}
		}

		public int UvprId
		{
			get
			{
				return this.uvprId;
			}
			set
			{
				this.uvprId = value;
			}
		}

		public int UvstId
		{
			get
			{
				return this.uvstId;
			}
			set
			{
				this.uvstId = value;
			}
		}

		public int UvtrId
		{
			get
			{
				return this.uvtrId;
			}
			set
			{
				this.uvtrId = value;
			}
		}

		public string VehicleStyle
		{
			get
			{
				return this.vehicleStyle;
			}
			set
			{
				this.vehicleStyle = value;
			}
		}

		public string VehicleTrim
		{
			get
			{
				return this.vehicleTrim;
			}
			set
			{
				this.vehicleTrim = value;
			}
		}

		public string VehicleYear
		{
			get
			{
				return this.vehicleYear;
			}
			set
			{
				this.vehicleYear = value;
			}
		}

		public string ZipCode
		{
			get
			{
				return this.zipCode;
			}
			set
			{
				this.zipCode = value;
			}
		}

		public VehicleAd()
		{
		}

		public VehicleAd(string connection, string modifiedUserID, int uvadId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.UvadId = uvadId;
			if (this.UvadId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@UvadId", DataAccessParameterType.Numeric, this.UvadId.ToString());
			data.ExecuteProcedure("UVAD_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@UvadId", DataAccessParameterType.Numeric, this.UvadId.ToString());
			data.ExecuteProcedure("UVAD_GetRecord");
			if (!data.EOF)
			{
				this.UvadId = int.Parse(data["UvadId"]);
				this.VehicleYear = data["VehicleYear"];
				this.MakeName = data["MakeName"];
				this.UvmkId = int.Parse(data["MakeID"]);
				this.ModelName = data["ModelName"];
				this.UvmdId = int.Parse(data["ModelID"]);
				this.UvstId = int.Parse(data["VehicleStyle"]);
				this.VehicleStyle = data["VehicleStyleDesc"];
				this.Miles = int.Parse(data["Miles"]);
				this.UvenId = int.Parse(data["Engine"]);
				this.Engine = data["EngineDesc"];
				this.UvtrId = int.Parse(data["Transmission"]);
				this.Transmission = data["TransmissionDesc"];
				this.ColorExterior = data["ColorExterior"];
				this.ColorInterior = data["ColorInterior"];
				this.UvftId = int.Parse(data["FuelType"]);
				this.FuelType = data["FuelTypeDesc"];
				this.SellingPrice = float.Parse(data["SellingPrice"]);
				this.Notes = data["Notes"];
				this.FirstName = data["ContactFirstName"];
				this.LastName = data["ContactLastName"];
				this.Phone = data["ContactPhone"];
				this.PhoneOther = data["ContactPhone2"];
				this.Email = data["ContactEmail"];
				this.City = data["ContactCity"];
				this.StateCode = data["ContactState"];
				this.ZipCode = data["ContactZipCode"];
				this.Addate = data["DateAdded"];
				this.Status = data["Status"];
				this.VehicleTrim = data["Trim"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int uvadId = this.UvadId;
			data.AddParam("@UvadId", DataAccessParameterType.Numeric, uvadId.ToString());
			uvadId = this.AgrpId;
			data.AddParam("@AgrpId", DataAccessParameterType.Numeric, uvadId.ToString());
			uvadId = this.MembId;
			data.AddParam("@MembId", DataAccessParameterType.Numeric, uvadId.ToString());
			data.AddParam("@Year", DataAccessParameterType.Text, this.VehicleYear);
			uvadId = this.UvmdId;
			data.AddParam("@UvmdId", DataAccessParameterType.Numeric, uvadId.ToString());
			uvadId = this.UvenId;
			data.AddParam("@UvenId", DataAccessParameterType.Numeric, uvadId.ToString());
			uvadId = this.UvtrId;
			data.AddParam("@UvtrId", DataAccessParameterType.Numeric, uvadId.ToString());
			uvadId = this.UvftId;
			data.AddParam("@UvftId", DataAccessParameterType.Numeric, uvadId.ToString());
			uvadId = this.UvstId;
			data.AddParam("@UvstId", DataAccessParameterType.Numeric, uvadId.ToString());
			data.AddParam("@ColorExterior", DataAccessParameterType.Text, this.ColorExterior);
			data.AddParam("@ColorInterior", DataAccessParameterType.Text, this.ColorInterior);
			uvadId = this.Miles;
			data.AddParam("@Miles", DataAccessParameterType.Numeric, uvadId.ToString());
			data.AddParam("@SellingPrice", DataAccessParameterType.Numeric, this.SellingPrice.ToString());
			data.AddParam("@Notes", DataAccessParameterType.Text, this.Notes);
			data.AddParam("@FirstName", DataAccessParameterType.Text, this.FirstName);
			data.AddParam("@LastName", DataAccessParameterType.Text, this.LastName);
			data.AddParam("@Phone", DataAccessParameterType.Text, this.Phone);
			data.AddParam("@PhoneOther", DataAccessParameterType.Text, this.PhoneOther);
			data.AddParam("@Email", DataAccessParameterType.Text, this.Email);
			data.AddParam("@City", DataAccessParameterType.Text, this.City);
			data.AddParam("@StatId", DataAccessParameterType.Text, this.StateCode);
			data.AddParam("@ZipcId", DataAccessParameterType.Text, this.ZipCode);
			data.ExecuteProcedure("UVAD_InsertUpdate");
			if (!data.EOF)
			{
				this.UvadId = int.Parse(data["UvadId"]);
			}
			this.retrievedata();
		}
	}
}