using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Data.Com
{
	public class Member : Transaction
	{
		private int membId = 0;

		private string code = "";

		private int agrpId = 0;

		private string firstName = "";

		private string lastName = "";

		private string middleInitial = "";

		private string address1 = "";

		private string address2 = "";

		private string city = "";

		private string state = "";

		private string zipCode = "";

		private string phonePrimary = "";

		private string phoneOther = "";

		private string email = "";

		private string userName = "";

		private string userPassword = "";

		private string challengeQuestion1Code = "";

		private string challengeQuestion2Code = "";

		private string challengeQuestion1Answer = "";

		private string challengeQuestion2Answer = "";

		private bool autoEmailSubscribe = false;

		public string Address1
		{
			get
			{
				return this.address1;
			}
			set
			{
				this.address1 = value;
			}
		}

		public string Address2
		{
			get
			{
				return this.address2;
			}
			set
			{
				this.address2 = value;
			}
		}

		public int AgrpId
		{
			get
			{
				return this.agrpId;
			}
			set
			{
				this.agrpId = value;
			}
		}

		public bool AutoEmailSubscribe
		{
			get
			{
				return this.autoEmailSubscribe;
			}
			set
			{
				this.autoEmailSubscribe = value;
			}
		}

		public string ChallengeQuestion1Answer
		{
			get
			{
				return this.challengeQuestion1Answer;
			}
			set
			{
				this.challengeQuestion1Answer = value;
			}
		}

		public string ChallengeQuestion1Code
		{
			get
			{
				return this.challengeQuestion1Code;
			}
			set
			{
				this.challengeQuestion1Code = value;
			}
		}

		public string ChallengeQuestion2Answer
		{
			get
			{
				return this.challengeQuestion2Answer;
			}
			set
			{
				this.challengeQuestion2Answer = value;
			}
		}

		public string ChallengeQuestion2Code
		{
			get
			{
				return this.challengeQuestion2Code;
			}
			set
			{
				this.challengeQuestion2Code = value;
			}
		}

		public string City
		{
			get
			{
				return this.city;
			}
			set
			{
				this.city = value;
			}
		}

		public string Code
		{
			get
			{
				return this.code;
			}
			set
			{
				this.code = value;
			}
		}

		public string Email
		{
			get
			{
				return this.email;
			}
			set
			{
				this.email = value;
			}
		}

		public string FirstName
		{
			get
			{
				return this.firstName;
			}
			set
			{
				this.firstName = value;
			}
		}

		public string LastName
		{
			get
			{
				return this.lastName;
			}
			set
			{
				this.lastName = value;
			}
		}

		public int MembId
		{
			get
			{
				return this.membId;
			}
			set
			{
				this.membId = value;
			}
		}

		public string MiddleInitial
		{
			get
			{
				return this.middleInitial;
			}
			set
			{
				this.middleInitial = value;
			}
		}

		public string PhoneOther
		{
			get
			{
				return this.phoneOther;
			}
			set
			{
				this.phoneOther = value;
			}
		}

		public string PhonePrimary
		{
			get
			{
				return this.phonePrimary;
			}
			set
			{
				this.phonePrimary = value;
			}
		}

		public string State
		{
			get
			{
				return this.state;
			}
			set
			{
				this.state = value;
			}
		}

		public string UserName
		{
			get
			{
				return this.userName;
			}
			set
			{
				this.userName = value;
			}
		}

		public string UserPassword
		{
			get
			{
				return this.userPassword;
			}
			set
			{
				this.userPassword = value;
			}
		}

		public string ZipCode
		{
			get
			{
				return this.zipCode;
			}
			set
			{
				this.zipCode = value;
			}
		}

		public Member()
		{
		}

		public Member(string connection, string modifiedUserID, int membId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.MembId = membId;
			if (this.MembId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@MembId", DataAccessParameterType.Numeric, this.MembId.ToString());
			data.ExecuteProcedure("MEMB_GetRecord");
			if (!data.EOF)
			{
				this.MembId = int.Parse(data["MembId"]);
				this.AgrpId = int.Parse(data["AgrpId"]);
				this.FirstName = data["FirstName"];
				this.LastName = data["LastName"];
				this.MiddleInitial = data["MiddleInitial"];
				this.Address1 = data["Address1"];
				this.Address2 = data["Address2"];
				this.City = data["City"];
				this.State = data["State"];
				this.ZipCode = data["ZipCode"];
				this.PhonePrimary = data["PhonePrimary"];
				this.PhoneOther = data["PhoneOther"];
				this.Email = data["Email"];
				this.UserName = data["UserName"];
				this.UserPassword = data["UserPassword"];
				this.ChallengeQuestion1Answer = data["ChallengeQuestion1Answer"];
				this.ChallengeQuestion1Code = data["ChallengeQuestion1Code"];
				this.ChallengeQuestion2Answer = data["ChallengeQuestion2Answer"];
				this.ChallengeQuestion2Code = data["ChallengeQuestion2Code"];
				this.AutoEmailSubscribe = data["AutoEmailSubscribe"].Trim().Equals("1");
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int membId = this.MembId;
			data.AddParam("@MembId", DataAccessParameterType.Numeric, membId.ToString());
			membId = this.AgrpId;
			data.AddParam("@AgrpID", DataAccessParameterType.Numeric, membId.ToString());
			data.AddParam("@LastName", DataAccessParameterType.Text, this.LastName);
			data.AddParam("@FirstName", DataAccessParameterType.Text, this.FirstName);
			data.AddParam("@MiddleInitial", DataAccessParameterType.Text, this.MiddleInitial);
			data.AddParam("@Address1", DataAccessParameterType.Text, this.Address1);
			data.AddParam("@Address2", DataAccessParameterType.Text, this.Address2);
			data.AddParam("@City", DataAccessParameterType.Text, this.City);
			data.AddParam("@State", DataAccessParameterType.Text, this.State);
			data.AddParam("@ZipCode", DataAccessParameterType.Text, this.ZipCode);
			data.AddParam("@PhonePrimary", DataAccessParameterType.Text, this.PhonePrimary);
			data.AddParam("@PhoneOther", DataAccessParameterType.Text, this.PhoneOther);
			data.AddParam("@Email", DataAccessParameterType.Text, this.Email);
			data.AddParam("@ChallengeQuestion1Code", DataAccessParameterType.Text, this.ChallengeQuestion1Code);
			data.AddParam("@ChallengeQuestion2Code", DataAccessParameterType.Text, this.ChallengeQuestion2Code);
			data.AddParam("@ChallengeQuestion1Answer", DataAccessParameterType.Text, this.ChallengeQuestion1Answer);
			data.AddParam("@ChallengeQuestion2Answer", DataAccessParameterType.Text, this.ChallengeQuestion2Answer);
			data.AddParam("@AutoEmailSubscribe", DataAccessParameterType.Numeric, (this.AutoEmailSubscribe ? "1" : "0"));
			data.ExecuteProcedure("MEMB_InsertUpdate");
			if (!data.EOF)
			{
				this.MembId = int.Parse(data["MembId"]);
			}
			this.retrievedata();
		}
	}
}