using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.AimeAd
{
	public class Lead : Transaction
	{
		private int leadId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int affinityGroupId = 0;

		private int memberId = 0;

		private string newUsed = "";

		private int dealerId = 0;

		private int vehicleYear = 0;

		private string vehicleYearRange = "";

		private string vehiclePriceRange = "";

		private bool isVehicleDecided = false;

		private bool isFinanced = false;

		private string makeName = "";

		private string modelName = "";

		private string trimName = "";

		private bool isAppointment = false;

		private string appointmentDate = "";

		private string memberComments = "";

		private string financeInstitution = "";

		private string financeContact = "";

		private string financeAddress = "";

		private string financePhone = "";

		private string financeFax = "";

		private string financeEmail = "";

		private string financeApplicationReference = "";

		private string userId_Msr = "";

		private int lookupId_DirectLeadStatus = 0;

		private string statusCode = "";

		private int noteId = 0;

		public int AffinityGroupId
		{
			get
			{
				return this.affinityGroupId;
			}
			set
			{
				this.affinityGroupId = value;
			}
		}

		public string AppointmentDate
		{
			get
			{
				return this.appointmentDate;
			}
			set
			{
				this.appointmentDate = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public int DealerId
		{
			get
			{
				return this.dealerId;
			}
			set
			{
				this.dealerId = value;
			}
		}

		public string FinanceAddress
		{
			get
			{
				return this.financeAddress;
			}
			set
			{
				this.financeAddress = value;
			}
		}

		public string FinanceApplicationReference
		{
			get
			{
				return this.financeApplicationReference;
			}
			set
			{
				this.financeApplicationReference = value;
			}
		}

		public string FinanceContact
		{
			get
			{
				return this.financeContact;
			}
			set
			{
				this.financeContact = value;
			}
		}

		public string FinanceEmail
		{
			get
			{
				return this.financeEmail;
			}
			set
			{
				this.financeEmail = value;
			}
		}

		public string FinanceFax
		{
			get
			{
				return this.financeFax;
			}
			set
			{
				this.financeFax = value;
			}
		}

		public string FinanceInstitution
		{
			get
			{
				return this.financeInstitution;
			}
			set
			{
				this.financeInstitution = value;
			}
		}

		public string FinancePhone
		{
			get
			{
				return this.financePhone;
			}
			set
			{
				this.financePhone = value;
			}
		}

		public bool IsAppointment
		{
			get
			{
				return this.isAppointment;
			}
			set
			{
				this.isAppointment = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public bool IsFinanced
		{
			get
			{
				return this.isFinanced;
			}
			set
			{
				this.isFinanced = value;
			}
		}

		public bool IsVehicleDecided
		{
			get
			{
				return this.isVehicleDecided;
			}
			set
			{
				this.isVehicleDecided = value;
			}
		}

		public int LeadId
		{
			get
			{
				return this.leadId;
			}
			set
			{
				this.leadId = value;
			}
		}

		public int LookupId_DirectLeadStatus
		{
			get
			{
				return this.lookupId_DirectLeadStatus;
			}
			set
			{
				this.lookupId_DirectLeadStatus = value;
			}
		}

		public string MakeName
		{
			get
			{
				return this.makeName;
			}
			set
			{
				this.makeName = value;
			}
		}

		public string MemberComments
		{
			get
			{
				return this.memberComments;
			}
			set
			{
				this.memberComments = value;
			}
		}

		public int MemberId
		{
			get
			{
				return this.memberId;
			}
			set
			{
				this.memberId = value;
			}
		}

		public string ModelName
		{
			get
			{
				return this.modelName;
			}
			set
			{
				this.modelName = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public string NewUsed
		{
			get
			{
				return this.newUsed;
			}
			set
			{
				this.newUsed = value;
			}
		}

		public int NoteId
		{
			get
			{
				return this.noteId;
			}
			set
			{
				this.noteId = value;
			}
		}

		public string StatusCode
		{
			get
			{
				return this.statusCode;
			}
			set
			{
				this.statusCode = value;
			}
		}

		public string TrimName
		{
			get
			{
				return this.trimName;
			}
			set
			{
				this.trimName = value;
			}
		}

		public string UserId_Msr
		{
			get
			{
				return this.userId_Msr;
			}
			set
			{
				this.userId_Msr = value;
			}
		}

		public string VehiclePriceRange
		{
			get
			{
				return this.vehiclePriceRange;
			}
			set
			{
				this.vehiclePriceRange = value;
			}
		}

		public int VehicleYear
		{
			get
			{
				return this.vehicleYear;
			}
			set
			{
				this.vehicleYear = value;
			}
		}

		public string VehicleYearRange
		{
			get
			{
				return this.vehicleYearRange;
			}
			set
			{
				this.vehicleYearRange = value;
			}
		}

		public Lead()
		{
		}

		public Lead(string connection, string modifiedUserID, int leadId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "LEAD";
			this.LeadId = leadId;
			if (this.LeadId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@LeadId", DataAccessParameterType.Numeric, this.LeadId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("LEAD_Delete");
			this.wipeout();
		}

		public int getRecordId_LookupLeadStatus(string code, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@Code", DataAccessParameterType.Numeric, code);
			data.ExecuteStatement(string.Concat("SELECT LOOKUP_ID FROM LOOKUP_DirectLeadStatus WHERE Code = '", code, "'"));
			num = (!data.EOF ? int.Parse(data["LOOKUP_ID"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@LeadId", DataAccessParameterType.Numeric, this.LeadId.ToString());
			data.ExecuteProcedure("LEAD_GetRecord");
			if (!data.EOF)
			{
				this.LeadId = int.Parse(data["LeadId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.AffinityGroupId = int.Parse(data["AffinityGroupId"]);
				this.MemberId = int.Parse(data["MemberId"]);
				this.NewUsed = data["NewUsed"];
				this.DealerId = int.Parse(data["DealerId"]);
				this.VehicleYear = int.Parse(data["VehicleYear"]);
				this.VehicleYearRange = data["VehicleYearRange"];
				this.VehiclePriceRange = data["VehiclePriceRange"];
				this.IsVehicleDecided = bool.Parse(data["IsVehicleDecided"]);
				this.IsFinanced = bool.Parse(data["IsFinanced"]);
				this.MakeName = data["MakeName"];
				this.ModelName = data["ModelName"];
				this.TrimName = data["TrimName"];
				this.IsAppointment = bool.Parse(data["IsAppointment"]);
				this.AppointmentDate = data["AppointmentDate"];
				this.MemberComments = data["MemberComments"];
				this.FinanceInstitution = data["FinanceInstitution"];
				this.FinanceContact = data["FinanceContact"];
				this.FinanceAddress = data["FinanceAddress"];
				this.FinancePhone = data["FinancePhone"];
				this.FinanceEmail = data["FinanceEmail"];
				this.FinanceFax = data["FinanceFax"];
				this.FinanceApplicationReference = data["FinanceApplicationReference"];
				this.UserId_Msr = data["UserId_Msr"];
				this.LookupId_DirectLeadStatus = int.Parse(data["LookupId_DirectLeadStatus"]);
				this.StatusCode = data["StatusCode"];
				this.NoteId = int.Parse(data["NoteId"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int leadId = this.LeadId;
			data.AddParam("@LeadId", DataAccessParameterType.Numeric, leadId.ToString());
			leadId = this.AffinityGroupId;
			data.AddParam("@AffinityGroupId", DataAccessParameterType.Numeric, leadId.ToString());
			leadId = this.MemberId;
			data.AddParam("@MemberId", DataAccessParameterType.Numeric, leadId.ToString());
			data.AddParam("@NewUsed", DataAccessParameterType.Text, this.NewUsed);
			leadId = this.DealerId;
			data.AddParam("@DealerId", DataAccessParameterType.Numeric, leadId.ToString());
			leadId = this.VehicleYear;
			data.AddParam("@VehicleYear", DataAccessParameterType.Numeric, leadId.ToString());
			data.AddParam("@VehicleYearRange", DataAccessParameterType.Text, this.VehicleYearRange.ToString());
			data.AddParam("@VehiclePriceRange", DataAccessParameterType.Text, this.VehiclePriceRange.ToString());
			bool isVehicleDecided = this.IsVehicleDecided;
			data.AddParam("@IsVehicleDecided", DataAccessParameterType.Bool, isVehicleDecided.ToString());
			isVehicleDecided = this.IsFinanced;
			data.AddParam("@IsFinanced", DataAccessParameterType.Bool, isVehicleDecided.ToString());
			data.AddParam("@MakeName", DataAccessParameterType.Text, this.MakeName);
			data.AddParam("@ModelName", DataAccessParameterType.Text, this.ModelName);
			data.AddParam("@TrimName", DataAccessParameterType.Text, this.TrimName);
			isVehicleDecided = this.IsAppointment;
			data.AddParam("@IsAppointment", DataAccessParameterType.Bool, isVehicleDecided.ToString());
			data.AddParam("@AppointmentDate", DataAccessParameterType.DateTime, this.AppointmentDate);
			data.AddParam("@MemberComments", DataAccessParameterType.Text, this.MemberComments);
			data.AddParam("@FinanceInstitution", DataAccessParameterType.Text, this.FinanceInstitution);
			data.AddParam("@FinanceContact", DataAccessParameterType.Text, this.FinanceContact);
			data.AddParam("@FinanceAddress", DataAccessParameterType.Text, this.FinanceAddress);
			data.AddParam("@FinancePhone", DataAccessParameterType.Text, this.FinancePhone);
			data.AddParam("@FinanceEmail", DataAccessParameterType.Text, this.FinanceEmail);
			data.AddParam("@FinanceFax", DataAccessParameterType.Text, this.FinanceFax);
			data.AddParam("@FinanceApplicationReference", DataAccessParameterType.Text, this.FinanceApplicationReference);
			data.AddParam("@UserId_Msr", DataAccessParameterType.Text, this.UserId_Msr);
			leadId = this.LookupId_DirectLeadStatus;
			data.AddParam("@LookupId_DirectLeadStatus", DataAccessParameterType.Numeric, leadId.ToString());
			leadId = this.NoteId;
			data.AddParam("@NoteId", DataAccessParameterType.Numeric, leadId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("LEAD_InsertUpdate");
			if (!data.EOF)
			{
				this.LeadId = int.Parse(data["LeadId"]);
			}
			this.retrievedata();
		}
	}
}