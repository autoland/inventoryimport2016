using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.AimeAd
{
	public class LeadLoan : Transaction
	{
		private int leadLoanId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int loanTerm = 0;

		private string applicantType = "";

		private string vin = "";

		private int vehicleMiles = 0;

		private float requestedLoanAmount = 0f;

		private float downPayment = 0f;

		private string firstName = "";

		private string lastName = "";

		private string sSN = "";

		private string dateOfBirth = "";

		private string phoneHome = "";

		private string phoneWork = "";

		private string phoneCell = "";

		private string email = "";

		private string driversLicenseNo = "";

		private string driversLicenseStateCode = "";

		private bool isBankruptcy = false;

		private bool isFelony = false;

		private string address1 = "";

		private string address2 = "";

		private string city = "";

		private string stateCode = "";

		private string zipCode = "";

		private string residenceStatus = "";

		private float monthlyRentMortgage = 0f;

		private int timeAtAddressYears = 0;

		private int timeAtAddressMonths = 0;

		private string workStatus = "";

		private string employerName = "";

		private string employerAddress = "";

		private string employerCity = "";

		private string employerStateCode = "";

		private string employerZipCode = "";

		private string workOccupation = "";

		private int lengthOfEmploymentYears = 0;

		private int lengthOfEmploymentMonths = 0;

		private float incomeMonthlyGross = 0f;

		private float incomeOtherSource = 0f;

		private float incomeOther = 0f;

		private string comments = "";

		private string secondaryFirstName = "";

		private string secondaryLastName = "";

		private string secondarySSN = "";

		private string secondaryDateOfBirth = "";

		private string secondaryPhoneHome = "";

		private string secondaryPhoneWork = "";

		private string secondaryPhoneCell = "";

		private string secondaryEmail = "";

		private string secondaryDriversLicenseNo = "";

		private string secondaryDriversLicenseStateCode = "";

		private bool secondaryIsBankruptcy = false;

		private bool secondaryIsFelony = false;

		private string secondaryAddress1 = "";

		private string secondaryAddress2 = "";

		private string secondaryCity = "";

		private string secondaryStateCode = "";

		private string secondaryZipCode = "";

		private string secondaryResidenceStatus = "";

		private float secondaryMonthlyRentMortgage = 0f;

		private int secondaryTimeAtAddressYears = 0;

		private int secondaryTimeAtAddressMonths = 0;

		private string secondaryWorkStatus = "";

		private string secondaryEmployerName = "";

		private string secondaryEmployerAddress = "";

		private string secondaryEmployerCity = "";

		private string secondaryEmployerStateCode = "";

		private string secondaryEmployerZipCode = "";

		private string secondaryWorkOccupation = "";

		private int secondaryLengthOfEmploymentYears = 0;

		private int secondaryLengthOfEmploymentMonths = 0;

		private float secondaryIncomeMonthlyGross = 0f;

		private float secondaryIncomeOtherSource = 0f;

		private float secondaryIncomeOther = 0f;

		private string secondaryComments = "";

		public string Address1
		{
			get
			{
				return this.address1;
			}
			set
			{
				this.address1 = value;
			}
		}

		public string Address2
		{
			get
			{
				return this.address2;
			}
			set
			{
				this.address2 = value;
			}
		}

		public string ApplicantType
		{
			get
			{
				return this.applicantType;
			}
			set
			{
				this.applicantType = value;
			}
		}

		public string City
		{
			get
			{
				return this.city;
			}
			set
			{
				this.city = value;
			}
		}

		public string Comments
		{
			get
			{
				return this.comments;
			}
			set
			{
				this.comments = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string DateOfBirth
		{
			get
			{
				return this.dateOfBirth;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.dateOfBirth = "";
				}
				else
				{
					this.dateOfBirth = DateTime.Parse(value).ToString();
				}
			}
		}

		public float DownPayment
		{
			get
			{
				return this.downPayment;
			}
			set
			{
				this.downPayment = value;
			}
		}

		public string DriversLicenseNo
		{
			get
			{
				return this.driversLicenseNo;
			}
			set
			{
				this.driversLicenseNo = value;
			}
		}

		public string DriversLicenseStateCode
		{
			get
			{
				return this.driversLicenseStateCode;
			}
			set
			{
				this.driversLicenseStateCode = value;
			}
		}

		public string Email
		{
			get
			{
				return this.email;
			}
			set
			{
				this.email = value;
			}
		}

		public string EmployerAddress
		{
			get
			{
				return this.employerAddress;
			}
			set
			{
				this.employerAddress = value;
			}
		}

		public string EmployerCity
		{
			get
			{
				return this.employerCity;
			}
			set
			{
				this.employerCity = value;
			}
		}

		public string EmployerName
		{
			get
			{
				return this.employerName;
			}
			set
			{
				this.employerName = value;
			}
		}

		public string EmployerStateCode
		{
			get
			{
				return this.employerStateCode;
			}
			set
			{
				this.employerStateCode = value;
			}
		}

		public string EmployerZipCode
		{
			get
			{
				return this.employerZipCode;
			}
			set
			{
				this.employerZipCode = value;
			}
		}

		public string FirstName
		{
			get
			{
				return this.firstName;
			}
			set
			{
				this.firstName = value;
			}
		}

		public float IncomeMonthlyGross
		{
			get
			{
				return this.incomeMonthlyGross;
			}
			set
			{
				this.incomeMonthlyGross = value;
			}
		}

		public float IncomeOther
		{
			get
			{
				return this.incomeOther;
			}
			set
			{
				this.incomeOther = value;
			}
		}

		public float IncomeOtherSource
		{
			get
			{
				return this.incomeOtherSource;
			}
			set
			{
				this.incomeOtherSource = value;
			}
		}

		public bool IsBankruptcy
		{
			get
			{
				return this.isBankruptcy;
			}
			set
			{
				this.isBankruptcy = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public bool IsFelony
		{
			get
			{
				return this.isFelony;
			}
			set
			{
				this.isFelony = value;
			}
		}

		public string LastName
		{
			get
			{
				return this.lastName;
			}
			set
			{
				this.lastName = value;
			}
		}

		public int LeadLoanId
		{
			get
			{
				return this.leadLoanId;
			}
			set
			{
				this.leadLoanId = value;
			}
		}

		public int LengthOfEmploymentMonths
		{
			get
			{
				return this.lengthOfEmploymentMonths;
			}
			set
			{
				this.lengthOfEmploymentMonths = value;
			}
		}

		public int LengthOfEmploymentYears
		{
			get
			{
				return this.lengthOfEmploymentYears;
			}
			set
			{
				this.lengthOfEmploymentYears = value;
			}
		}

		public int LoanTerm
		{
			get
			{
				return this.loanTerm;
			}
			set
			{
				this.loanTerm = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public float MonthlyRentMortgage
		{
			get
			{
				return this.monthlyRentMortgage;
			}
			set
			{
				this.monthlyRentMortgage = value;
			}
		}

		public string PhoneCell
		{
			get
			{
				return this.phoneCell;
			}
			set
			{
				this.phoneCell = value;
			}
		}

		public string PhoneHome
		{
			get
			{
				return this.phoneHome;
			}
			set
			{
				this.phoneHome = value;
			}
		}

		public string PhoneWork
		{
			get
			{
				return this.phoneWork;
			}
			set
			{
				this.phoneWork = value;
			}
		}

		public float RequestedLoanAmount
		{
			get
			{
				return this.requestedLoanAmount;
			}
			set
			{
				this.requestedLoanAmount = value;
			}
		}

		public string ResidenceStatus
		{
			get
			{
				return this.residenceStatus;
			}
			set
			{
				this.residenceStatus = value;
			}
		}

		public string SecondaryAddress1
		{
			get
			{
				return this.secondaryAddress1;
			}
			set
			{
				this.secondaryAddress1 = value;
			}
		}

		public string SecondaryAddress2
		{
			get
			{
				return this.secondaryAddress2;
			}
			set
			{
				this.secondaryAddress2 = value;
			}
		}

		public string SecondaryCity
		{
			get
			{
				return this.secondaryCity;
			}
			set
			{
				this.secondaryCity = value;
			}
		}

		public string SecondaryComments
		{
			get
			{
				return this.secondaryComments;
			}
			set
			{
				this.secondaryComments = value;
			}
		}

		public string SecondaryDateOfBirth
		{
			get
			{
				return this.secondaryDateOfBirth;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.secondaryDateOfBirth = "";
				}
				else
				{
					this.secondaryDateOfBirth = DateTime.Parse(value).ToString();
				}
			}
		}

		public string SecondaryDriversLicenseNo
		{
			get
			{
				return this.secondaryDriversLicenseNo;
			}
			set
			{
				this.secondaryDriversLicenseNo = value;
			}
		}

		public string SecondaryDriversLicenseStateCode
		{
			get
			{
				return this.secondaryDriversLicenseStateCode;
			}
			set
			{
				this.secondaryDriversLicenseStateCode = value;
			}
		}

		public string SecondaryEmail
		{
			get
			{
				return this.secondaryEmail;
			}
			set
			{
				this.secondaryEmail = value;
			}
		}

		public string SecondaryEmployerAddress
		{
			get
			{
				return this.secondaryEmployerAddress;
			}
			set
			{
				this.secondaryEmployerAddress = value;
			}
		}

		public string SecondaryEmployerCity
		{
			get
			{
				return this.secondaryEmployerCity;
			}
			set
			{
				this.secondaryEmployerCity = value;
			}
		}

		public string SecondaryEmployerName
		{
			get
			{
				return this.secondaryEmployerName;
			}
			set
			{
				this.secondaryEmployerName = value;
			}
		}

		public string SecondaryEmployerStateCode
		{
			get
			{
				return this.secondaryEmployerStateCode;
			}
			set
			{
				this.secondaryEmployerStateCode = value;
			}
		}

		public string SecondaryEmployerZipCode
		{
			get
			{
				return this.secondaryEmployerZipCode;
			}
			set
			{
				this.secondaryEmployerZipCode = value;
			}
		}

		public string SecondaryFirstName
		{
			get
			{
				return this.secondaryFirstName;
			}
			set
			{
				this.secondaryFirstName = value;
			}
		}

		public float SecondaryIncomeMonthlyGross
		{
			get
			{
				return this.secondaryIncomeMonthlyGross;
			}
			set
			{
				this.secondaryIncomeMonthlyGross = value;
			}
		}

		public float SecondaryIncomeOther
		{
			get
			{
				return this.secondaryIncomeOther;
			}
			set
			{
				this.secondaryIncomeOther = value;
			}
		}

		public float SecondaryIncomeOtherSource
		{
			get
			{
				return this.secondaryIncomeOtherSource;
			}
			set
			{
				this.secondaryIncomeOtherSource = value;
			}
		}

		public bool SecondaryIsBankruptcy
		{
			get
			{
				return this.secondaryIsBankruptcy;
			}
			set
			{
				this.secondaryIsBankruptcy = value;
			}
		}

		public bool SecondaryIsFelony
		{
			get
			{
				return this.secondaryIsFelony;
			}
			set
			{
				this.secondaryIsFelony = value;
			}
		}

		public string SecondaryLastName
		{
			get
			{
				return this.secondaryLastName;
			}
			set
			{
				this.secondaryLastName = value;
			}
		}

		public int SecondaryLengthOfEmploymentMonths
		{
			get
			{
				return this.secondaryLengthOfEmploymentMonths;
			}
			set
			{
				this.secondaryLengthOfEmploymentMonths = value;
			}
		}

		public int SecondaryLengthOfEmploymentYears
		{
			get
			{
				return this.secondaryLengthOfEmploymentYears;
			}
			set
			{
				this.secondaryLengthOfEmploymentYears = value;
			}
		}

		public float SecondaryMonthlyRentMortgage
		{
			get
			{
				return this.secondaryMonthlyRentMortgage;
			}
			set
			{
				this.secondaryMonthlyRentMortgage = value;
			}
		}

		public string SecondaryPhoneCell
		{
			get
			{
				return this.secondaryPhoneCell;
			}
			set
			{
				this.secondaryPhoneCell = value;
			}
		}

		public string SecondaryPhoneHome
		{
			get
			{
				return this.secondaryPhoneHome;
			}
			set
			{
				this.secondaryPhoneHome = value;
			}
		}

		public string SecondaryPhoneWork
		{
			get
			{
				return this.secondaryPhoneWork;
			}
			set
			{
				this.secondaryPhoneWork = value;
			}
		}

		public string SecondaryResidenceStatus
		{
			get
			{
				return this.secondaryResidenceStatus;
			}
			set
			{
				this.secondaryResidenceStatus = value;
			}
		}

		public string SecondarySSN
		{
			get
			{
				return this.secondarySSN;
			}
			set
			{
				this.secondarySSN = value;
			}
		}

		public string SecondaryStateCode
		{
			get
			{
				return this.secondaryStateCode;
			}
			set
			{
				this.secondaryStateCode = value;
			}
		}

		public int SecondaryTimeAtAddressMonths
		{
			get
			{
				return this.secondaryTimeAtAddressMonths;
			}
			set
			{
				this.secondaryTimeAtAddressMonths = value;
			}
		}

		public int SecondaryTimeAtAddressYears
		{
			get
			{
				return this.secondaryTimeAtAddressYears;
			}
			set
			{
				this.secondaryTimeAtAddressYears = value;
			}
		}

		public string SecondaryWorkOccupation
		{
			get
			{
				return this.secondaryWorkOccupation;
			}
			set
			{
				this.secondaryWorkOccupation = value;
			}
		}

		public string SecondaryWorkStatus
		{
			get
			{
				return this.secondaryWorkStatus;
			}
			set
			{
				this.secondaryWorkStatus = value;
			}
		}

		public string SecondaryZipCode
		{
			get
			{
				return this.secondaryZipCode;
			}
			set
			{
				this.secondaryZipCode = value;
			}
		}

		public string SSN
		{
			get
			{
				return this.sSN;
			}
			set
			{
				this.sSN = value;
			}
		}

		public string StateCode
		{
			get
			{
				return this.stateCode;
			}
			set
			{
				this.stateCode = value;
			}
		}

		public int TimeAtAddressMonths
		{
			get
			{
				return this.timeAtAddressMonths;
			}
			set
			{
				this.timeAtAddressMonths = value;
			}
		}

		public int TimeAtAddressYears
		{
			get
			{
				return this.timeAtAddressYears;
			}
			set
			{
				this.timeAtAddressYears = value;
			}
		}

		public int VehicleMiles
		{
			get
			{
				return this.vehicleMiles;
			}
			set
			{
				this.vehicleMiles = value;
			}
		}

		public string Vin
		{
			get
			{
				return this.vin;
			}
			set
			{
				this.vin = value;
			}
		}

		public string WorkOccupation
		{
			get
			{
				return this.workOccupation;
			}
			set
			{
				this.workOccupation = value;
			}
		}

		public string WorkStatus
		{
			get
			{
				return this.workStatus;
			}
			set
			{
				this.workStatus = value;
			}
		}

		public string ZipCode
		{
			get
			{
				return this.zipCode;
			}
			set
			{
				this.zipCode = value;
			}
		}

		public LeadLoan()
		{
		}

		public LeadLoan(string connection, string modifiedUserID, int leadLoanId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "LEAD_LOAN";
			this.LeadLoanId = leadLoanId;
			if (this.LeadLoanId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@LeadLoanId", DataAccessParameterType.Numeric, this.LeadLoanId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("LEAD_LOAN_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@LeadLoanId", DataAccessParameterType.Numeric, this.LeadLoanId.ToString());
			data.ExecuteProcedure("LEAD_LOAN_GetRecord");
			if (!data.EOF)
			{
				this.LeadLoanId = int.Parse(data["LeadLoanId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.LoanTerm = int.Parse(data["LoanTerm"]);
				this.ApplicantType = data["ApplicantType"];
				this.Vin = data["Vin"];
				this.VehicleMiles = int.Parse(data["VehicleMiles"]);
				this.RequestedLoanAmount = float.Parse(data["RequestedLoanAmount"]);
				this.DownPayment = float.Parse(data["DownPayment"]);
				this.FirstName = data["FirstName"];
				this.LastName = data["LastName"];
				this.SSN = data["SSN"];
				this.DateOfBirth = data["DateOfBirth"];
				this.PhoneHome = data["PhoneHome"];
				this.PhoneWork = data["PhoneWork"];
				this.PhoneCell = data["PhoneCell"];
				this.Email = data["Email"];
				this.DriversLicenseNo = data["DriversLicenseNo"];
				this.DriversLicenseStateCode = data["DriversLicenseStateCode"];
				this.IsBankruptcy = bool.Parse(data["IsBankruptcy"]);
				this.IsFelony = bool.Parse(data["IsFelony"]);
				this.Address1 = data["Address1"];
				this.Address2 = data["Address2"];
				this.City = data["City"];
				this.StateCode = data["StateCode"];
				this.ZipCode = data["ZipCode"];
				this.ResidenceStatus = data["ResidenceStatus"];
				this.MonthlyRentMortgage = float.Parse(data["MonthlyRentMortgage"]);
				this.TimeAtAddressYears = int.Parse(data["TimeAtAddressYears"]);
				this.TimeAtAddressMonths = int.Parse(data["TimeAtAddressMonths"]);
				this.WorkStatus = data["WorkStatus"];
				this.EmployerName = data["EmployerName"];
				this.EmployerAddress = data["EmployerAddress"];
				this.EmployerCity = data["EmployerCity"];
				this.EmployerStateCode = data["EmployerStateCode"];
				this.EmployerZipCode = data["EmployerZipCode"];
				this.WorkOccupation = data["WorkOccupation"];
				this.LengthOfEmploymentYears = int.Parse(data["LengthOfEmploymentYears"]);
				this.LengthOfEmploymentMonths = int.Parse(data["LengthOfEmploymentMonths"]);
				this.IncomeMonthlyGross = float.Parse(data["IncomeMonthlyGross"]);
				this.IncomeOtherSource = float.Parse(data["IncomeOtherSource"]);
				this.IncomeOther = float.Parse(data["IncomeOther"]);
				this.Comments = data["Comments"];
				this.SecondaryFirstName = data["SecondaryFirstName"];
				this.SecondaryLastName = data["SecondaryLastName"];
				this.SecondarySSN = data["SecondarySSN"];
				this.SecondaryDateOfBirth = data["SecondaryDateOfBirth"];
				this.SecondaryPhoneHome = data["SecondaryPhoneHome"];
				this.SecondaryPhoneWork = data["SecondaryPhoneWork"];
				this.SecondaryPhoneCell = data["SecondaryPhoneCell"];
				this.SecondaryEmail = data["SecondaryEmail"];
				this.SecondaryDriversLicenseNo = data["SecondaryDriversLicenseNo"];
				this.SecondaryDriversLicenseStateCode = data["SecondaryDriversLicenseStateCode"];
				this.SecondaryIsBankruptcy = bool.Parse(data["SecondaryIsBankruptcy"]);
				this.SecondaryIsFelony = bool.Parse(data["SecondaryIsFelony"]);
				this.SecondaryAddress1 = data["SecondaryAddress1"];
				this.SecondaryAddress2 = data["SecondaryAddress2"];
				this.SecondaryCity = data["SecondaryCity"];
				this.SecondaryStateCode = data["SecondaryStateCode"];
				this.SecondaryZipCode = data["SecondaryZipCode"];
				this.SecondaryResidenceStatus = data["SecondaryResidenceStatus"];
				this.SecondaryMonthlyRentMortgage = float.Parse(data["SecondaryMonthlyRentMortgage"]);
				this.SecondaryTimeAtAddressYears = int.Parse(data["SecondaryTimeAtAddressYears"]);
				this.SecondaryTimeAtAddressMonths = int.Parse(data["SecondaryTimeAtAddressMonths"]);
				this.SecondaryWorkStatus = data["SecondaryWorkStatus"];
				this.SecondaryEmployerName = data["SecondaryEmployerName"];
				this.SecondaryEmployerAddress = data["SecondaryEmployerAddress"];
				this.SecondaryEmployerCity = data["SecondaryEmployerCity"];
				this.SecondaryEmployerStateCode = data["SecondaryEmployerStateCode"];
				this.SecondaryEmployerZipCode = data["SecondaryEmployerZipCode"];
				this.SecondaryWorkOccupation = data["SecondaryWorkOccupation"];
				this.SecondaryLengthOfEmploymentYears = int.Parse(data["SecondaryLengthOfEmploymentYears"]);
				this.SecondaryLengthOfEmploymentMonths = int.Parse(data["SecondaryLengthOfEmploymentMonths"]);
				this.SecondaryIncomeMonthlyGross = float.Parse(data["SecondaryIncomeMonthlyGross"]);
				this.SecondaryIncomeOtherSource = float.Parse(data["SecondaryIncomeOtherSource"]);
				this.SecondaryIncomeOther = float.Parse(data["SecondaryIncomeOther"]);
				this.SecondaryComments = data["SecondaryComments"];
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int leadLoanId = this.LeadLoanId;
			data.AddParam("@LeadLoanId", DataAccessParameterType.Numeric, leadLoanId.ToString());
			leadLoanId = this.LoanTerm;
			data.AddParam("@LoanTerm", DataAccessParameterType.Numeric, leadLoanId.ToString());
			data.AddParam("@ApplicantType", DataAccessParameterType.Text, this.ApplicantType);
			data.AddParam("@Vin", DataAccessParameterType.Text, this.Vin);
			leadLoanId = this.VehicleMiles;
			data.AddParam("@VehicleMiles", DataAccessParameterType.Numeric, leadLoanId.ToString());
			float requestedLoanAmount = this.RequestedLoanAmount;
			data.AddParam("@RequestedLoanAmount", DataAccessParameterType.Numeric, requestedLoanAmount.ToString());
			requestedLoanAmount = this.DownPayment;
			data.AddParam("@DownPayment", DataAccessParameterType.Numeric, requestedLoanAmount.ToString());
			data.AddParam("@FirstName", DataAccessParameterType.Text, this.FirstName);
			data.AddParam("@LastName", DataAccessParameterType.Text, this.LastName);
			data.AddParam("@SSN", DataAccessParameterType.Text, this.SSN);
			data.AddParam("@DateOfBirth", DataAccessParameterType.DateTime, this.DateOfBirth);
			data.AddParam("@PhoneHome", DataAccessParameterType.Text, this.PhoneHome);
			data.AddParam("@PhoneWork", DataAccessParameterType.Text, this.PhoneWork);
			data.AddParam("@PhoneCell", DataAccessParameterType.Text, this.PhoneCell);
			data.AddParam("@Email", DataAccessParameterType.Text, this.Email);
			data.AddParam("@DriversLicenseNo", DataAccessParameterType.Text, this.DriversLicenseNo);
			data.AddParam("@DriversLicenseStateCode", DataAccessParameterType.Text, this.DriversLicenseStateCode);
			bool isBankruptcy = this.IsBankruptcy;
			data.AddParam("@IsBankruptcy", DataAccessParameterType.Bool, isBankruptcy.ToString());
			isBankruptcy = this.IsFelony;
			data.AddParam("@IsFelony", DataAccessParameterType.Bool, isBankruptcy.ToString());
			data.AddParam("@Address1", DataAccessParameterType.Text, this.Address1);
			data.AddParam("@Address2", DataAccessParameterType.Text, this.Address2);
			data.AddParam("@City", DataAccessParameterType.Text, this.City);
			data.AddParam("@StateCode", DataAccessParameterType.Text, this.StateCode);
			data.AddParam("@ZipCode", DataAccessParameterType.Text, this.ZipCode);
			data.AddParam("@ResidenceStatus", DataAccessParameterType.Text, this.ResidenceStatus);
			requestedLoanAmount = this.MonthlyRentMortgage;
			data.AddParam("@MonthlyRentMortgage", DataAccessParameterType.Numeric, requestedLoanAmount.ToString());
			leadLoanId = this.TimeAtAddressYears;
			data.AddParam("@TimeAtAddressYears", DataAccessParameterType.Numeric, leadLoanId.ToString());
			leadLoanId = this.TimeAtAddressMonths;
			data.AddParam("@TimeAtAddressMonths", DataAccessParameterType.Numeric, leadLoanId.ToString());
			data.AddParam("@WorkStatus", DataAccessParameterType.Text, this.WorkStatus);
			data.AddParam("@EmployerName", DataAccessParameterType.Text, this.EmployerName);
			data.AddParam("@EmployerAddress", DataAccessParameterType.Text, this.EmployerAddress);
			data.AddParam("@EmployerCity", DataAccessParameterType.Text, this.EmployerCity);
			data.AddParam("@EmployerStateCode", DataAccessParameterType.Text, this.EmployerStateCode);
			data.AddParam("@EmployerZipCode", DataAccessParameterType.Text, this.EmployerZipCode);
			data.AddParam("@WorkOccupation", DataAccessParameterType.Text, this.WorkOccupation);
			leadLoanId = this.LengthOfEmploymentYears;
			data.AddParam("@LengthOfEmploymentYears", DataAccessParameterType.Numeric, leadLoanId.ToString());
			leadLoanId = this.LengthOfEmploymentMonths;
			data.AddParam("@LengthOfEmploymentMonths", DataAccessParameterType.Numeric, leadLoanId.ToString());
			requestedLoanAmount = this.IncomeMonthlyGross;
			data.AddParam("@IncomeMonthlyGross", DataAccessParameterType.Numeric, requestedLoanAmount.ToString());
			requestedLoanAmount = this.IncomeOtherSource;
			data.AddParam("@IncomeOtherSource", DataAccessParameterType.Numeric, requestedLoanAmount.ToString());
			requestedLoanAmount = this.IncomeOther;
			data.AddParam("@IncomeOther", DataAccessParameterType.Numeric, requestedLoanAmount.ToString());
			data.AddParam("@Comments", DataAccessParameterType.Text, this.Comments);
			data.AddParam("@SecondaryFirstName", DataAccessParameterType.Text, this.SecondaryFirstName);
			data.AddParam("@SecondaryLastName", DataAccessParameterType.Text, this.SecondaryLastName);
			data.AddParam("@SecondarySSN", DataAccessParameterType.Text, this.SecondarySSN);
			data.AddParam("@SecondaryDateOfBirth", DataAccessParameterType.DateTime, this.SecondaryDateOfBirth);
			data.AddParam("@SecondaryPhoneHome", DataAccessParameterType.Text, this.SecondaryPhoneHome);
			data.AddParam("@SecondaryPhoneWork", DataAccessParameterType.Text, this.SecondaryPhoneWork);
			data.AddParam("@SecondaryPhoneCell", DataAccessParameterType.Text, this.SecondaryPhoneCell);
			data.AddParam("@SecondaryEmail", DataAccessParameterType.Text, this.SecondaryEmail);
			data.AddParam("@SecondaryDriversLicenseNo", DataAccessParameterType.Text, this.SecondaryDriversLicenseNo);
			data.AddParam("@SecondaryDriversLicenseStateCode", DataAccessParameterType.Text, this.SecondaryDriversLicenseStateCode);
			isBankruptcy = this.SecondaryIsBankruptcy;
			data.AddParam("@SecondaryIsBankruptcy", DataAccessParameterType.Bool, isBankruptcy.ToString());
			isBankruptcy = this.SecondaryIsFelony;
			data.AddParam("@SecondaryIsFelony", DataAccessParameterType.Bool, isBankruptcy.ToString());
			data.AddParam("@SecondaryAddress1", DataAccessParameterType.Text, this.SecondaryAddress1);
			data.AddParam("@SecondaryAddress2", DataAccessParameterType.Text, this.SecondaryAddress2);
			data.AddParam("@SecondaryCity", DataAccessParameterType.Text, this.SecondaryCity);
			data.AddParam("@SecondaryStateCode", DataAccessParameterType.Text, this.SecondaryStateCode);
			data.AddParam("@SecondaryZipCode", DataAccessParameterType.Text, this.SecondaryZipCode);
			data.AddParam("@SecondaryResidenceStatus", DataAccessParameterType.Text, this.SecondaryResidenceStatus);
			requestedLoanAmount = this.SecondaryMonthlyRentMortgage;
			data.AddParam("@SecondaryMonthlyRentMortgage", DataAccessParameterType.Numeric, requestedLoanAmount.ToString());
			leadLoanId = this.SecondaryTimeAtAddressYears;
			data.AddParam("@SecondaryTimeAtAddressYears", DataAccessParameterType.Numeric, leadLoanId.ToString());
			leadLoanId = this.SecondaryTimeAtAddressMonths;
			data.AddParam("@SecondaryTimeAtAddressMonths", DataAccessParameterType.Numeric, leadLoanId.ToString());
			data.AddParam("@SecondaryWorkStatus", DataAccessParameterType.Text, this.SecondaryWorkStatus);
			data.AddParam("@SecondaryEmployerName", DataAccessParameterType.Text, this.SecondaryEmployerName);
			data.AddParam("@SecondaryEmployerAddress", DataAccessParameterType.Text, this.SecondaryEmployerAddress);
			data.AddParam("@SecondaryEmployerCity", DataAccessParameterType.Text, this.SecondaryEmployerCity);
			data.AddParam("@SecondaryEmployerStateCode", DataAccessParameterType.Text, this.SecondaryEmployerStateCode);
			data.AddParam("@SecondaryEmployerZipCode", DataAccessParameterType.Text, this.SecondaryEmployerZipCode);
			data.AddParam("@SecondaryWorkOccupation", DataAccessParameterType.Text, this.SecondaryWorkOccupation);
			leadLoanId = this.SecondaryLengthOfEmploymentYears;
			data.AddParam("@SecondaryLengthOfEmploymentYears", DataAccessParameterType.Numeric, leadLoanId.ToString());
			leadLoanId = this.SecondaryLengthOfEmploymentMonths;
			data.AddParam("@SecondaryLengthOfEmploymentMonths", DataAccessParameterType.Numeric, leadLoanId.ToString());
			requestedLoanAmount = this.SecondaryIncomeMonthlyGross;
			data.AddParam("@SecondaryIncomeMonthlyGross", DataAccessParameterType.Numeric, requestedLoanAmount.ToString());
			requestedLoanAmount = this.SecondaryIncomeOtherSource;
			data.AddParam("@SecondaryIncomeOtherSource", DataAccessParameterType.Numeric, requestedLoanAmount.ToString());
			requestedLoanAmount = this.SecondaryIncomeOther;
			data.AddParam("@SecondaryIncomeOther", DataAccessParameterType.Numeric, requestedLoanAmount.ToString());
			data.AddParam("@SecondaryComments", DataAccessParameterType.Text, this.SecondaryComments);
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("LEAD_LOAN_InsertUpdate");
			if (!data.EOF)
			{
				this.LeadLoanId = int.Parse(data["LeadLoanId"]);
			}
			this.retrievedata();
		}
	}
}