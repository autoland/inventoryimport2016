using System;

namespace Advocar.Data
{
	public enum DataAccessParameterType
	{
		Text,
		Numeric,
		DateTime,
		Bool
	}
}