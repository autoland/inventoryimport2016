using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.ConfiguratorAd
{
	public class VehicleModelYear : Transaction
	{
		private int vehicleModelYearId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private bool isActive = false;

		private string autodataCode = "";

		private int vehicleYearId = 0;

		private int vehicleMakeId = 0;

		private string makeName = "";

		private int vehicleModelId = 0;

		private string modelName = "";

		private int vehicleImageId = 0;

		private int noColorInteriors = 0;

		private int noColorExteriors = 0;

		private int noColorImages = 0;

		private int noImageInteriors = 0;

		private int noImageExteriors = 0;

		private int noImageLogos = 0;

		public string AutodataCode
		{
			get
			{
				return this.autodataCode;
			}
			set
			{
				this.autodataCode = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public bool IsActive
		{
			get
			{
				return this.isActive;
			}
			set
			{
				this.isActive = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public string MakeName
		{
			get
			{
				return this.makeName;
			}
			set
			{
				this.makeName = value;
			}
		}

		public string ModelName
		{
			get
			{
				return this.modelName;
			}
			set
			{
				this.modelName = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public int NoColorExteriors
		{
			get
			{
				return this.noColorExteriors;
			}
			set
			{
				this.noColorExteriors = value;
			}
		}

		public int NoColorImages
		{
			get
			{
				return this.noColorImages;
			}
			set
			{
				this.noColorImages = value;
			}
		}

		public int NoColorInteriors
		{
			get
			{
				return this.noColorInteriors;
			}
			set
			{
				this.noColorInteriors = value;
			}
		}

		public int NoImageExteriors
		{
			get
			{
				return this.noImageExteriors;
			}
			set
			{
				this.noImageExteriors = value;
			}
		}

		public int NoImageInteriors
		{
			get
			{
				return this.noImageInteriors;
			}
			set
			{
				this.noImageInteriors = value;
			}
		}

		public int NoImageLogos
		{
			get
			{
				return this.noImageLogos;
			}
			set
			{
				this.noImageLogos = value;
			}
		}

		public int VehicleImageId
		{
			get
			{
				return this.vehicleImageId;
			}
			set
			{
				this.vehicleImageId = value;
			}
		}

		public int VehicleMakeId
		{
			get
			{
				return this.vehicleMakeId;
			}
			set
			{
				this.vehicleMakeId = value;
			}
		}

		public int VehicleModelId
		{
			get
			{
				return this.vehicleModelId;
			}
			set
			{
				this.vehicleModelId = value;
			}
		}

		public int VehicleModelYearId
		{
			get
			{
				return this.vehicleModelYearId;
			}
			set
			{
				this.vehicleModelYearId = value;
			}
		}

		public int VehicleYearId
		{
			get
			{
				return this.vehicleYearId;
			}
			set
			{
				this.vehicleYearId = value;
			}
		}

		public VehicleModelYear()
		{
		}

		public VehicleModelYear(string connection, string modifiedUserID, int vehicleModelYearId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "VEHICLE_MODEL_YEAR";
			this.VehicleModelYearId = vehicleModelYearId;
			if (this.VehicleModelYearId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleModelYearId", DataAccessParameterType.Numeric, this.VehicleModelYearId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_MODEL_YEAR_Delete");
			this.wipeout();
		}

		public static int GetRecordId(int vehicleModelId, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@VehicleModelId", DataAccessParameterType.Numeric, vehicleModelId.ToString());
			data.ExecuteProcedure("VEHICLE_MODEL_YEAR_GetRecord");
			num = (!data.EOF ? int.Parse(data["VehicleModelYearId"]) : 0);
			return num;
		}

		public static int GetRecordId(string autodataCode, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@AutodataCode", DataAccessParameterType.Numeric, autodataCode.ToString());
			data.ExecuteProcedure("VEHICLE_MODEL_YEAR_GetRecord");
			num = (!data.EOF ? int.Parse(data["VehicleModelYearId"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleModelYearId", DataAccessParameterType.Numeric, this.VehicleModelYearId.ToString());
			data.ExecuteProcedure("VEHICLE_MODEL_YEAR_GetRecord");
			if (!data.EOF)
			{
				this.VehicleModelYearId = int.Parse(data["VehicleModelYearId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.IsActive = bool.Parse(data["IsActive"]);
				this.AutodataCode = data["AutodataCode"];
				this.VehicleYearId = int.Parse(data["VehicleYearId"]);
				this.VehicleMakeId = int.Parse(data["VehicleMakeId"]);
				this.MakeName = data["MakeName"];
				this.VehicleModelId = int.Parse(data["VehicleModelId"]);
				this.ModelName = data["ModelName"];
				this.VehicleImageId = int.Parse(data["VehicleImageId"]);
				this.NoColorInteriors = int.Parse(data["NoColorInteriors"]);
				this.NoColorExteriors = int.Parse(data["NoColorExteriors"]);
				this.NoColorImages = int.Parse(data["NoColorImages"]);
				this.NoImageInteriors = int.Parse(data["NoImageInteriors"]);
				this.NoImageExteriors = int.Parse(data["NoImageExteriors"]);
				this.NoImageLogos = int.Parse(data["NoImageLogos"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int vehicleModelYearId = this.VehicleModelYearId;
			data.AddParam("@VehicleModelYearId", DataAccessParameterType.Numeric, vehicleModelYearId.ToString());
			data.AddParam("@IsActive", DataAccessParameterType.Bool, this.IsActive.ToString());
			data.AddParam("@AutodataCode", DataAccessParameterType.Text, this.AutodataCode);
			vehicleModelYearId = this.VehicleYearId;
			data.AddParam("@VehicleYearId", DataAccessParameterType.Numeric, vehicleModelYearId.ToString());
			vehicleModelYearId = this.VehicleMakeId;
			data.AddParam("@VehicleMakeId", DataAccessParameterType.Numeric, vehicleModelYearId.ToString());
			data.AddParam("@MakeName", DataAccessParameterType.Text, this.MakeName);
			vehicleModelYearId = this.VehicleModelId;
			data.AddParam("@VehicleModelId", DataAccessParameterType.Numeric, vehicleModelYearId.ToString());
			data.AddParam("@ModelName", DataAccessParameterType.Text, this.ModelName);
			vehicleModelYearId = this.VehicleImageId;
			data.AddParam("@VehicleImageId", DataAccessParameterType.Numeric, vehicleModelYearId.ToString());
			vehicleModelYearId = this.NoColorInteriors;
			data.AddParam("@NoColorInteriors", DataAccessParameterType.Numeric, vehicleModelYearId.ToString());
			vehicleModelYearId = this.NoColorExteriors;
			data.AddParam("@NoColorExteriors", DataAccessParameterType.Numeric, vehicleModelYearId.ToString());
			vehicleModelYearId = this.NoColorImages;
			data.AddParam("@NoColorImages", DataAccessParameterType.Numeric, vehicleModelYearId.ToString());
			vehicleModelYearId = this.NoImageInteriors;
			data.AddParam("@NoImageInteriors", DataAccessParameterType.Numeric, vehicleModelYearId.ToString());
			vehicleModelYearId = this.NoImageExteriors;
			data.AddParam("@NoImageExteriors", DataAccessParameterType.Numeric, vehicleModelYearId.ToString());
			vehicleModelYearId = this.NoImageLogos;
			data.AddParam("@NoImageLogos", DataAccessParameterType.Numeric, vehicleModelYearId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_MODEL_YEAR_InsertUpdate");
			if (!data.EOF)
			{
				this.VehicleModelYearId = int.Parse(data["VehicleModelYearId"]);
			}
			this.retrievedata();
		}
	}
}