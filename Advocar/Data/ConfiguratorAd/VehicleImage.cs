using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.ConfiguratorAd
{
	public class VehicleImage : Transaction
	{
		private int vehicleImageId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private string imageFile = "";

		private int imageWidth = 0;

		private int imageHeight = 0;

		private string interiorExteriorLogo = "";

		private bool isImageDownloaded = false;

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string ImageFile
		{
			get
			{
				return this.imageFile;
			}
			set
			{
				this.imageFile = value;
			}
		}

		public int ImageHeight
		{
			get
			{
				return this.imageHeight;
			}
			set
			{
				this.imageHeight = value;
			}
		}

		public int ImageWidth
		{
			get
			{
				return this.imageWidth;
			}
			set
			{
				this.imageWidth = value;
			}
		}

		public string InteriorExteriorLogo
		{
			get
			{
				return this.interiorExteriorLogo;
			}
			set
			{
				this.interiorExteriorLogo = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public bool IsImageDownloaded
		{
			get
			{
				return this.isImageDownloaded;
			}
			set
			{
				this.isImageDownloaded = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public int VehicleImageId
		{
			get
			{
				return this.vehicleImageId;
			}
			set
			{
				this.vehicleImageId = value;
			}
		}

		public VehicleImage()
		{
		}

		public VehicleImage(string connection, string modifiedUserID, int vehicleImageId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "VEHICLE_IMAGE";
			this.VehicleImageId = vehicleImageId;
			if (this.VehicleImageId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleImageId", DataAccessParameterType.Numeric, this.VehicleImageId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_IMAGE_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleImageId", DataAccessParameterType.Numeric, this.VehicleImageId.ToString());
			data.ExecuteProcedure("VEHICLE_IMAGE_GetRecord");
			if (!data.EOF)
			{
				this.VehicleImageId = int.Parse(data["VehicleImageId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.ImageFile = data["ImageFile"];
				this.ImageWidth = int.Parse(data["ImageWidth"]);
				this.ImageHeight = int.Parse(data["ImageHeight"]);
				this.InteriorExteriorLogo = data["InteriorExteriorLogo"];
				this.IsImageDownloaded = bool.Parse(data["IsImageDownloaded"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int vehicleImageId = this.VehicleImageId;
			data.AddParam("@VehicleImageId", DataAccessParameterType.Numeric, vehicleImageId.ToString());
			data.AddParam("@ImageFile", DataAccessParameterType.Text, this.ImageFile);
			vehicleImageId = this.ImageWidth;
			data.AddParam("@ImageWidth", DataAccessParameterType.Numeric, vehicleImageId.ToString());
			vehicleImageId = this.ImageHeight;
			data.AddParam("@ImageHeight", DataAccessParameterType.Numeric, vehicleImageId.ToString());
			data.AddParam("@InteriorExteriorLogo", DataAccessParameterType.Text, this.InteriorExteriorLogo);
			data.AddParam("@IsImageDownloaded", DataAccessParameterType.Bool, this.IsImageDownloaded.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_IMAGE_InsertUpdate");
			if (!data.EOF)
			{
				this.VehicleImageId = int.Parse(data["VehicleImageId"]);
			}
			this.retrievedata();
		}
	}
}