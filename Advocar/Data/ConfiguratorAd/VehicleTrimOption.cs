using System;

namespace Advocar.Data.ConfiguratorAd
{
	public class VehicleTrimOption
	{
		private int vehicleTrimOptionId = 0;

		private string optionCode = "";

		private string optionLevel = "";

		private string selectionState = "";

		private string details = "";

		private string description = "";

		private string notes = "";

		private string msrp = "";

		private string invoice = "";

		private string userPrice = "";

		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

		public string Details
		{
			get
			{
				return this.details;
			}
			set
			{
				this.details = value;
			}
		}

		public string Invoice
		{
			get
			{
				return this.invoice;
			}
			set
			{
				this.invoice = value;
			}
		}

		public string Msrp
		{
			get
			{
				return this.msrp;
			}
			set
			{
				this.msrp = value;
			}
		}

		public string Notes
		{
			get
			{
				return this.notes;
			}
			set
			{
				this.notes = value;
			}
		}

		public string OptionCode
		{
			get
			{
				return this.optionCode;
			}
			set
			{
				this.optionCode = value;
			}
		}

		public string OptionLevel
		{
			get
			{
				return this.optionLevel;
			}
			set
			{
				this.optionLevel = value;
			}
		}

		public string SelectionState
		{
			get
			{
				return this.selectionState;
			}
			set
			{
				this.selectionState = value;
			}
		}

		public string UserPrice
		{
			get
			{
				return this.userPrice;
			}
			set
			{
				this.userPrice = value;
			}
		}

		public int VehicleTrimOptionId
		{
			get
			{
				return this.vehicleTrimOptionId;
			}
			set
			{
				this.vehicleTrimOptionId = value;
			}
		}

		public VehicleTrimOption()
		{
		}
	}
}