using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.ConfiguratorAd
{
	public class VehicleTrimColor : Transaction
	{
		private int vehicleTrimColorId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private int vehicleTrimId = 0;

		private string autodataCode = "";

		private string colorCode = "";

		private string description = "";

		private string shortDescription = "";

		private int rgbRed = 0;

		private int rgbGreen = 0;

		private int rgbBlue = 0;

		private string interiorExterior = "";

		private int vehicleImageId = 0;

		public string AutodataCode
		{
			get
			{
				return this.autodataCode;
			}
			set
			{
				this.autodataCode = value;
			}
		}

		public string ColorCode
		{
			get
			{
				return this.colorCode;
			}
			set
			{
				this.colorCode = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

		public string InteriorExterior
		{
			get
			{
				return this.interiorExterior;
			}
			set
			{
				this.interiorExterior = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public int RgbBlue
		{
			get
			{
				return this.rgbBlue;
			}
			set
			{
				this.rgbBlue = value;
			}
		}

		public int RgbGreen
		{
			get
			{
				return this.rgbGreen;
			}
			set
			{
				this.rgbGreen = value;
			}
		}

		public int RgbRed
		{
			get
			{
				return this.rgbRed;
			}
			set
			{
				this.rgbRed = value;
			}
		}

		public string ShortDescription
		{
			get
			{
				return this.shortDescription;
			}
			set
			{
				this.shortDescription = value;
			}
		}

		public int VehicleImageId
		{
			get
			{
				return this.vehicleImageId;
			}
			set
			{
				this.vehicleImageId = value;
			}
		}

		public int VehicleTrimColorId
		{
			get
			{
				return this.vehicleTrimColorId;
			}
			set
			{
				this.vehicleTrimColorId = value;
			}
		}

		public int VehicleTrimId
		{
			get
			{
				return this.vehicleTrimId;
			}
			set
			{
				this.vehicleTrimId = value;
			}
		}

		public VehicleTrimColor()
		{
		}

		public VehicleTrimColor(string connection, string modifiedUserID, int vehicleTrimColorId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "VEHICLE_TRIM_COLOR";
			this.VehicleTrimColorId = vehicleTrimColorId;
			if (this.VehicleTrimColorId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleTrimColorId", DataAccessParameterType.Numeric, this.VehicleTrimColorId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_TRIM_COLOR_Delete");
			this.wipeout();
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleTrimColorId", DataAccessParameterType.Numeric, this.VehicleTrimColorId.ToString());
			data.ExecuteProcedure("VEHICLE_TRIM_COLOR_GetRecord");
			if (!data.EOF)
			{
				this.VehicleTrimColorId = int.Parse(data["VehicleTrimColorId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.VehicleTrimId = int.Parse(data["VehicleTrimId"]);
				this.AutodataCode = data["AutodataCode"];
				this.ColorCode = data["ColorCode"];
				this.Description = data["Description"];
				this.ShortDescription = data["ShortDescription"];
				this.RgbRed = int.Parse(data["RgbRed"]);
				this.RgbGreen = int.Parse(data["RgbGreen"]);
				this.RgbBlue = int.Parse(data["RgbBlue"]);
				this.InteriorExterior = data["InteriorExterior"];
				this.VehicleImageId = int.Parse(data["VehicleImageId"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int vehicleTrimColorId = this.VehicleTrimColorId;
			data.AddParam("@VehicleTrimColorId", DataAccessParameterType.Numeric, vehicleTrimColorId.ToString());
			vehicleTrimColorId = this.VehicleTrimId;
			data.AddParam("@VehicleTrimId", DataAccessParameterType.Numeric, vehicleTrimColorId.ToString());
			data.AddParam("@AutodataCode", DataAccessParameterType.Text, this.AutodataCode);
			data.AddParam("@ColorCode", DataAccessParameterType.Text, this.ColorCode);
			data.AddParam("@Description", DataAccessParameterType.Text, this.Description);
			data.AddParam("@ShortDescription", DataAccessParameterType.Text, this.ShortDescription);
			vehicleTrimColorId = this.RgbRed;
			data.AddParam("@RgbRed", DataAccessParameterType.Numeric, vehicleTrimColorId.ToString());
			vehicleTrimColorId = this.RgbGreen;
			data.AddParam("@RgbGreen", DataAccessParameterType.Numeric, vehicleTrimColorId.ToString());
			vehicleTrimColorId = this.RgbBlue;
			data.AddParam("@RgbBlue", DataAccessParameterType.Numeric, vehicleTrimColorId.ToString());
			data.AddParam("@InteriorExterior", DataAccessParameterType.Text, this.InteriorExterior);
			vehicleTrimColorId = this.VehicleImageId;
			data.AddParam("@VehicleImageId", DataAccessParameterType.Numeric, vehicleTrimColorId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_TRIM_COLOR_InsertUpdate");
			if (!data.EOF)
			{
				this.VehicleTrimColorId = int.Parse(data["VehicleTrimColorId"]);
			}
			this.retrievedata();
		}
	}
}