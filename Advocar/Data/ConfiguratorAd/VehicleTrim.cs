using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using System;

namespace Advocar.Data.ConfiguratorAd
{
	public class VehicleTrim : Transaction
	{
		private int vehicleTrimId = 0;

		private bool isDeleted = false;

		private string createdDate = "";

		private string createdUser = "";

		private string modifiedDate = "";

		private string modifiedUser = "";

		private string autodataCode = "";

		private int vehicleYearId = 0;

		private int vehicleMakeId = 0;

		private string makeName = "";

		private int vehicleModelId = 0;

		private string modelName = "";

		private int vehicleModelYearId = 0;

		private string trimName = "";

		private int noColorInteriors = 0;

		private int noColorExteriors = 0;

		private int noColorImages = 0;

		private int noImageInteriors = 0;

		private int noImageExteriors = 0;

		private int noImageLogos = 0;

		private int fuelEconomyHwy = 0;

		private int fuelEconomyCity = 0;

		public string AutodataCode
		{
			get
			{
				return this.autodataCode;
			}
			set
			{
				this.autodataCode = value;
			}
		}

		public string CreatedDate
		{
			get
			{
				return this.createdDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.createdDate = "";
				}
				else
				{
					this.createdDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string CreatedUser
		{
			get
			{
				return this.createdUser;
			}
			set
			{
				this.createdUser = value;
			}
		}

		public int FuelEconomyCity
		{
			get
			{
				return this.fuelEconomyCity;
			}
			set
			{
				this.fuelEconomyCity = value;
			}
		}

		public int FuelEconomyHwy
		{
			get
			{
				return this.fuelEconomyHwy;
			}
			set
			{
				this.fuelEconomyHwy = value;
			}
		}

		public bool IsDeleted
		{
			get
			{
				return this.isDeleted;
			}
			set
			{
				this.isDeleted = value;
			}
		}

		public string MakeName
		{
			get
			{
				return this.makeName;
			}
			set
			{
				this.makeName = value;
			}
		}

		public string ModelName
		{
			get
			{
				return this.modelName;
			}
			set
			{
				this.modelName = value;
			}
		}

		public string ModifiedDate
		{
			get
			{
				return this.modifiedDate;
			}
			set
			{
				if (!Validation.IsDate(value))
				{
					this.modifiedDate = "";
				}
				else
				{
					this.modifiedDate = DateTime.Parse(value).ToString();
				}
			}
		}

		public string ModifiedUser
		{
			get
			{
				return this.modifiedUser;
			}
			set
			{
				this.modifiedUser = value;
			}
		}

		public int NoColorExteriors
		{
			get
			{
				return this.noColorExteriors;
			}
			set
			{
				this.noColorExteriors = value;
			}
		}

		public int NoColorImages
		{
			get
			{
				return this.noColorImages;
			}
			set
			{
				this.noColorImages = value;
			}
		}

		public int NoColorInteriors
		{
			get
			{
				return this.noColorInteriors;
			}
			set
			{
				this.noColorInteriors = value;
			}
		}

		public int NoImageExteriors
		{
			get
			{
				return this.noImageExteriors;
			}
			set
			{
				this.noImageExteriors = value;
			}
		}

		public int NoImageInteriors
		{
			get
			{
				return this.noImageInteriors;
			}
			set
			{
				this.noImageInteriors = value;
			}
		}

		public int NoImageLogos
		{
			get
			{
				return this.noImageLogos;
			}
			set
			{
				this.noImageLogos = value;
			}
		}

		public string TrimName
		{
			get
			{
				return this.trimName;
			}
			set
			{
				this.trimName = value;
			}
		}

		public int VehicleMakeId
		{
			get
			{
				return this.vehicleMakeId;
			}
			set
			{
				this.vehicleMakeId = value;
			}
		}

		public int VehicleModelId
		{
			get
			{
				return this.vehicleModelId;
			}
			set
			{
				this.vehicleModelId = value;
			}
		}

		public int VehicleModelYearId
		{
			get
			{
				return this.vehicleModelYearId;
			}
			set
			{
				this.vehicleModelYearId = value;
			}
		}

		public int VehicleTrimId
		{
			get
			{
				return this.vehicleTrimId;
			}
			set
			{
				this.vehicleTrimId = value;
			}
		}

		public int VehicleYearId
		{
			get
			{
				return this.vehicleYearId;
			}
			set
			{
				this.vehicleYearId = value;
			}
		}

		public VehicleTrim()
		{
		}

		public VehicleTrim(string connection, string modifiedUserID, int vehicleTrimId) : base(connection, modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
			this.databaseObjectName = "VEHICLE_TRIM";
			this.VehicleTrimId = vehicleTrimId;
			if (this.VehicleTrimId > 0)
			{
				this.retrievedata();
			}
		}

		public override void Delete()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleTrimId", DataAccessParameterType.Numeric, this.VehicleTrimId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_TRIM_Delete");
			this.wipeout();
		}

		public static int GetRecordId(string autodataCode, string connection)
		{
			int num;
			DataAccess data = new DataAccess(connection);
			data.AddParam("@AutodataCode", DataAccessParameterType.Text, autodataCode);
			data.ExecuteProcedure("VEHICLE_TRIM_GetRecord");
			num = (!data.EOF ? int.Parse(data["VehicleTrimId"]) : 0);
			return num;
		}

		protected override void retrievedata()
		{
			DataAccess data = new DataAccess(this.connection);
			data.AddParam("@VehicleTrimId", DataAccessParameterType.Numeric, this.VehicleTrimId.ToString());
			data.ExecuteProcedure("VEHICLE_TRIM_GetRecord");
			if (!data.EOF)
			{
				this.VehicleTrimId = int.Parse(data["VehicleTrimId"]);
				this.IsDeleted = bool.Parse(data["IsDeleted"]);
				this.CreatedDate = data["CreatedDate"];
				this.CreatedUser = data["CreatedUser"];
				this.ModifiedDate = data["ModifiedDate"];
				this.ModifiedUser = data["ModifiedUser"];
				this.AutodataCode = data["AutodataCode"];
				this.VehicleYearId = int.Parse(data["VehicleYearId"]);
				this.VehicleMakeId = int.Parse(data["VehicleMakeId"]);
				this.MakeName = data["MakeName"];
				this.VehicleModelId = int.Parse(data["VehicleModelId"]);
				this.ModelName = data["ModelName"];
				this.VehicleModelYearId = int.Parse(data["VehicleModelYearId"]);
				this.TrimName = data["TrimName"];
				this.NoColorInteriors = int.Parse(data["NoColorInteriors"]);
				this.NoColorExteriors = int.Parse(data["NoColorExteriors"]);
				this.NoColorImages = int.Parse(data["NoColorImages"]);
				this.NoImageInteriors = int.Parse(data["NoImageInteriors"]);
				this.NoImageExteriors = int.Parse(data["NoImageExteriors"]);
				this.NoImageLogos = int.Parse(data["NoImageLogos"]);
				this.FuelEconomyCity = int.Parse(data["FuelEconomyCity"]);
				this.FuelEconomyHwy = int.Parse(data["FuelEconomyHwy"]);
			}
			else
			{
				this.wipeout();
			}
		}

		public override void Save()
		{
			DataAccess data = new DataAccess(this.connection);
			int vehicleTrimId = this.VehicleTrimId;
			data.AddParam("@VehicleTrimId", DataAccessParameterType.Numeric, vehicleTrimId.ToString());
			data.AddParam("@AutodataCode", DataAccessParameterType.Text, this.AutodataCode);
			vehicleTrimId = this.VehicleYearId;
			data.AddParam("@VehicleYearId", DataAccessParameterType.Numeric, vehicleTrimId.ToString());
			vehicleTrimId = this.VehicleMakeId;
			data.AddParam("@VehicleMakeId", DataAccessParameterType.Numeric, vehicleTrimId.ToString());
			data.AddParam("@MakeName", DataAccessParameterType.Text, this.MakeName);
			vehicleTrimId = this.VehicleModelId;
			data.AddParam("@VehicleModelId", DataAccessParameterType.Numeric, vehicleTrimId.ToString());
			data.AddParam("@ModelName", DataAccessParameterType.Text, this.ModelName);
			vehicleTrimId = this.VehicleModelYearId;
			data.AddParam("@VehicleModelYearId", DataAccessParameterType.Numeric, vehicleTrimId.ToString());
			data.AddParam("@TrimName", DataAccessParameterType.Text, this.TrimName);
			vehicleTrimId = this.NoColorInteriors;
			data.AddParam("@NoColorInteriors", DataAccessParameterType.Numeric, vehicleTrimId.ToString());
			vehicleTrimId = this.NoColorExteriors;
			data.AddParam("@NoColorExteriors", DataAccessParameterType.Numeric, vehicleTrimId.ToString());
			vehicleTrimId = this.NoColorImages;
			data.AddParam("@NoColorImages", DataAccessParameterType.Numeric, vehicleTrimId.ToString());
			vehicleTrimId = this.NoImageInteriors;
			data.AddParam("@NoImageInteriors", DataAccessParameterType.Numeric, vehicleTrimId.ToString());
			vehicleTrimId = this.NoImageExteriors;
			data.AddParam("@NoImageExteriors", DataAccessParameterType.Numeric, vehicleTrimId.ToString());
			vehicleTrimId = this.NoImageLogos;
			data.AddParam("@NoImageLogos", DataAccessParameterType.Numeric, vehicleTrimId.ToString());
			vehicleTrimId = this.FuelEconomyCity;
			data.AddParam("@FuelEconomyCity", DataAccessParameterType.Numeric, vehicleTrimId.ToString());
			vehicleTrimId = this.FuelEconomyHwy;
			data.AddParam("@FuelEconomyHwy", DataAccessParameterType.Numeric, vehicleTrimId.ToString());
			data.AddParam("@ModifiedUser", DataAccessParameterType.Text, this.modifiedUserID);
			data.ExecuteProcedure("VEHICLE_TRIM_InsertUpdate");
			if (!data.EOF)
			{
				this.VehicleTrimId = int.Parse(data["VehicleTrimId"]);
			}
			this.retrievedata();
		}
	}
}