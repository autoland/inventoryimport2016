using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Advocar;

namespace Advocar.Controls
{
    public class Report : CrystalDecisions.CrystalReports.Engine.ReportDocument
    {
        CrystalDecisions.Shared.ConnectionInfo connection = new CrystalDecisions.Shared.ConnectionInfo();

        public Report(string reportPath) : base() { this.Load(reportPath); }
        public Report(string reportPath, string server, string database, string user, string password)
            : base()
        {
            try { this.Load(reportPath); }
            catch (System.Exception err) { throw new Advocar.Tools.Exception(err, "Advocar.Controls.Report.Constructor", err.Message + " Path: " + reportPath); }

            this.connection.ServerName = server;
            this.connection.UserID = user;
            this.connection.Password = password;
            this.connection.DatabaseName = database;

            try
            {
                foreach (CrystalDecisions.CrystalReports.Engine.Table table in this.Database.Tables)
                {
                    table.LogOnInfo.ConnectionInfo = this.connection;
                    table.ApplyLogOnInfo(table.LogOnInfo);
                    table.Location = this.connection.DatabaseName + ".dbo." + table.Name;
                }

                foreach (CrystalDecisions.CrystalReports.Engine.Section section in this.ReportDefinition.Sections)
                {
                    foreach (CrystalDecisions.CrystalReports.Engine.ReportObject reportObject in section.ReportObjects)
                    {
                        if (reportObject.Kind == CrystalDecisions.Shared.ReportObjectKind.SubreportObject)
                        {
                            CrystalDecisions.CrystalReports.Engine.SubreportObject subreport = (CrystalDecisions.CrystalReports.Engine.SubreportObject)reportObject;
                            CrystalDecisions.CrystalReports.Engine.ReportDocument subreportDocument = subreport.OpenSubreport(subreport.SubreportName);

                            foreach (CrystalDecisions.CrystalReports.Engine.Table table in subreportDocument.Database.Tables)
                            {
                                table.LogOnInfo.ConnectionInfo = this.connection;
                                table.ApplyLogOnInfo(table.LogOnInfo);
                                table.Location = this.connection.DatabaseName + ".dbo." + table.Name;
                            }
                        }
                    }
                }
            }
            catch (System.Exception err) { throw new Advocar.Tools.Exception(err, "Advocar.Controls.Report.Constructor", err.Message + " Server: " + server + " DB: " + database); }
        }

        public string GetFieldName(int index)
        {
            CrystalDecisions.Shared.ParameterField field = this.ParameterFields[index];
            return field.Name;
        }

        public string GetFieldTitle(int index)
        {
            CrystalDecisions.Shared.ParameterField field = this.ParameterFields[index];
            return (Advocar.Tools.Validation.IsNull(field.PromptText) == false ? field.PromptText : field.Name);
        }

        public void SetParameterValue(int index, string assignedValue)
        {
            CrystalDecisions.Shared.ParameterField field = this.ParameterFields[index];
            field.DefaultValues.Clear();
            field.CurrentValues.Clear();
            field.EnableAllowMultipleValue = false;

            switch (field.ParameterValueType)
            {
                case CrystalDecisions.Shared.ParameterValueKind.BooleanParameter:
                    if (!(assignedValue == "true" || assignedValue == "false")) { assignedValue = "false"; }
                    field.CurrentValues.AddValue(bool.Parse(assignedValue));
                    break;

                case CrystalDecisions.Shared.ParameterValueKind.CurrencyParameter:
                    assignedValue = assignedValue.Replace("$", "");
                    if (Advocar.Tools.Validation.IsNumeric(assignedValue) == false) { assignedValue = "0"; }
                    field.CurrentValues.AddValue(float.Parse(assignedValue));
                    break;

                case CrystalDecisions.Shared.ParameterValueKind.DateParameter:
                    if (Advocar.Tools.Validation.IsDate(assignedValue) == false) { assignedValue = DateTime.Now.ToString("MM/dd/yyyy"); }
                    field.CurrentValues.AddValue(DateTime.Parse(assignedValue));
                    break;

                case CrystalDecisions.Shared.ParameterValueKind.DateTimeParameter:
                    if (Advocar.Tools.Validation.IsDate(assignedValue) == false) { assignedValue = DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss"); }
                    field.CurrentValues.AddValue(DateTime.Parse(assignedValue));
                    break;

                case CrystalDecisions.Shared.ParameterValueKind.NumberParameter:
                    assignedValue = assignedValue.Replace("$", "");
                    if (Advocar.Tools.Validation.IsNumeric(assignedValue) == false) { assignedValue = "0"; }
                    field.CurrentValues.AddValue(float.Parse(assignedValue));
                    break;

                case CrystalDecisions.Shared.ParameterValueKind.StringParameter:
                    field.CurrentValues.AddValue(assignedValue);
                    break;

                default:
                    field.CurrentValues.AddValue(assignedValue);
                    break;
            }
        }

        public string CreateWebInput(int index)
        {
            CrystalDecisions.Shared.ParameterField field = this.ParameterFields[index];
            string key = "parameter" + index.ToString("00"); string webInput = "";

            switch (field.ParameterValueType)
            {
                case CrystalDecisions.Shared.ParameterValueKind.BooleanParameter:
                    webInput = "" +
                        new Advocar.Web.Controls.Radio(key, "Yes", "Yes", "Yes").ToString() +
                        new Advocar.Web.Controls.Radio(key, "Yes", "No", "No").ToString();
                    break;

                case CrystalDecisions.Shared.ParameterValueKind.CurrencyParameter:
                    webInput = new Advocar.Web.Controls.Text(key, "", 100, 20).ToString();
                    break;

                case CrystalDecisions.Shared.ParameterValueKind.DateParameter:
                    webInput = new Advocar.Web.Controls.Date(key, "").ToString();
                    break;

                case CrystalDecisions.Shared.ParameterValueKind.DateTimeParameter:
                    webInput = new Advocar.Web.Controls.Date(key, "").ToString();
                    break;

                case CrystalDecisions.Shared.ParameterValueKind.NumberParameter:
                    webInput = new Advocar.Web.Controls.Text(key, "", 100, 20).ToString();
                    break;

                case CrystalDecisions.Shared.ParameterValueKind.StringParameter:
                    webInput = new Advocar.Web.Controls.Text(key, "", 250, 100).ToString();
                    break;

                default:
                    webInput = new Advocar.Web.Controls.Hidden(key, "").ToString();
                    break;
            }

            return webInput;
        }

        public string CreateWebValidation(int index)
        {
            CrystalDecisions.Shared.ParameterField field = this.ParameterFields[index];
            string key = "parameter" + index.ToString("00"); string webInput = "";

            switch (field.ParameterValueType)
            {
                case CrystalDecisions.Shared.ParameterValueKind.CurrencyParameter:
                    webInput = "checkAmount(document.form." + key + ", '" + field.PromptText + "', false)";
                    break;

                case CrystalDecisions.Shared.ParameterValueKind.DateParameter:
                    webInput = "checkDate(document.form." + key + "_year, document.form." + key + "_month, document.form." + key + "_day, '" + field.PromptText + " ', false)";
                    break;

                case CrystalDecisions.Shared.ParameterValueKind.DateTimeParameter:
                    webInput = "checkDate(document.form." + key + "_year, document.form." + key + "_month, document.form." + key + "_day, '" + field.PromptText + " ', false)";
                    break;

                case CrystalDecisions.Shared.ParameterValueKind.NumberParameter:
                    webInput = "checkAmount(document.form." + key + ", '" + field.PromptText + " ', false)";
                    break;

                case CrystalDecisions.Shared.ParameterValueKind.StringParameter:
                    webInput = "checkString(document.form." + key + ", '" + field.PromptText + " ', false)";
                    break;
            }

            return webInput;
        }

        public void ExportToPdf(string filePath)
        {
            CrystalDecisions.Shared.DiskFileDestinationOptions destination = new CrystalDecisions.Shared.DiskFileDestinationOptions();
            destination.DiskFileName = filePath;

            CrystalDecisions.Shared.ExportOptions options = new CrystalDecisions.Shared.ExportOptions();
            options.ExportDestinationOptions = destination;
            options.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
            options.ExportFormatType = ExportFormatType.PortableDocFormat;

            this.Export(options);
        }
    }
}
