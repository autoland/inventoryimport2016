using Advocar.Tools;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Advocar.Controls
{
	public class ListView : System.Windows.Forms.ListView
	{
		public bool isClickable = false;

		public int[] HighlightedIndexes = new int[0];

		[Browsable(true)]
		[Category("Appearance")]
		[DefaultValue(false)]
		[Description("Allows Row to be Highlighted")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool IsClickable
		{
			get
			{
				return this.isClickable;
			}
			set
			{
				this.isClickable = value;
			}
		}

		public ListView()
		{
			base.AllowColumnReorder = true;
			base.FullRowSelect = true;
			base.View = System.Windows.Forms.View.Details;
			base.MultiSelect = false;
			base.Clear();
			base.ColumnClick += new ColumnClickEventHandler(this.ListView_ColumnClick);
			base.DoubleClick += new EventHandler(this.ListView_DoubleClick);
		}

		public ListViewItem Add(params string[] items)
		{
			ListViewItem item = new ListViewItem(items);
			item = base.Items.Add(item);
			for (int index = 0; (int)items.Length > index; index++)
			{
				if (!Validation.IsNull(items[index]))
				{
					if ((items[index].IndexOf("COLOR#") != 0 || items[index].Length <= 8 ? false : items[index].IndexOf("#", 8) >= 0))
					{
						string color = items[index].Replace("COLOR#", "");
						color = color.Substring(0, color.IndexOf("#"));
						items[index] = items[index].Replace(string.Concat("COLOR#", color, "#"), "");
						item.SubItems[index].Text = items[index];
						item.SubItems[index].BackColor = Color.FromName(color);
						item.UseItemStyleForSubItems = false;
					}
				}
			}
			return item;
		}

		public void AddHighlitedIndex(int index)
		{
			base.Items[index].BackColor = Color.YellowGreen;
			base.Items[index].UseItemStyleForSubItems = true;
			int length = (int)this.HighlightedIndexes.Length + 1;
			int[] array = new int[length];
			this.HighlightedIndexes.CopyTo(array, 0);
			this.HighlightedIndexes = new int[length];
			array.CopyTo(this.HighlightedIndexes, 0);
			this.HighlightedIndexes[length - 1] = index;
		}

		public void ClearHighlightedIndex()
		{
			while ((int)this.HighlightedIndexes.Length > 0)
			{
				int row = this.HighlightedIndexes[(int)this.HighlightedIndexes.Length - 1];
				this.RemoveHighlightedIndex(row);
			}
			this.HighlightedIndexes = new int[0];
		}

		private void ListView_ColumnClick(object sender, ColumnClickEventArgs e)
		{
			if (e.Column == 0)
			{
				base.Sorting = (base.Sorting == SortOrder.Ascending ? SortOrder.Descending : SortOrder.Ascending);
				base.Sort();
			}
		}

		public void ListView_DoubleClick(object sender, EventArgs e)
		{
			if (this.IsClickable)
			{
				base.SelectedItems[0].BackColor = (base.SelectedItems[0].BackColor == this.BackColor ? Color.YellowGreen : this.BackColor);
				if (!(base.SelectedItems[0].BackColor == Color.YellowGreen))
				{
					this.RemoveHighlightedIndex(base.SelectedItems[0].Index);
				}
				else
				{
					this.AddHighlitedIndex(base.SelectedItems[0].Index);
				}
			}
		}

		public void Remove()
		{
			base.SelectedItems[0].Remove();
		}

		public void RemoveHighlightedIndex(int index)
		{
			base.Items[index].BackColor = this.BackColor;
			base.Items[index].UseItemStyleForSubItems = false;
			if ((int)this.HighlightedIndexes.Length != 0)
			{
				int[] array = new int[(int)this.HighlightedIndexes.Length - 1];
				int count = -1;
				for (int iter = 0; (int)this.HighlightedIndexes.Length > iter; iter++)
				{
					if (this.HighlightedIndexes[iter] != index)
					{
						count++;
						array[count] = this.HighlightedIndexes[iter];
					}
				}
				this.HighlightedIndexes = new int[(int)array.Length];
				array.CopyTo(this.HighlightedIndexes, 0);
			}
		}

		public void Update(ListViewItem item, params string[] items)
		{
			for (int index = 0; (int)items.Length > index; index++)
			{
				if (!Validation.IsNull(items[index]))
				{
					item.SubItems[index].Text = items[index];
					if ((items[index].IndexOf("COLOR#") != 0 || items[index].Length <= 8 ? false : items[index].IndexOf("#", 8) >= 0))
					{
						string color = items[index].Replace("COLOR#", "");
						color = color.Substring(0, color.IndexOf("#"));
						items[index] = items[index].Replace(string.Concat("COLOR#", color, "#"), "");
						item.SubItems[index].Text = items[index];
						item.SubItems[index].BackColor = Color.FromName(color);
						item.UseItemStyleForSubItems = false;
					}
				}
			}
		}
	}
}