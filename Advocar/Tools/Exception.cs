using System;

namespace Advocar.Tools
{
	public class Exception : System.Exception
	{
		public Exception(System.Exception err, string strSource, string Message) : base(Message)
		{
			this.Source = string.Concat(err.Source, "-{", strSource, "}");
		}

		public Exception(System.Exception err, string strSource)
		{
			this.Source = string.Concat(err.Source, "-{", strSource, "}");
		}
	}
}