using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Advocar.Tools
{
	public static class Encryption
	{
		private static string HashName;

		private static int KeySize;

		private static string Iv;

		private static int Iterations;

		private static string Password;

		private static string Salt;

		static Encryption()
		{
			Encryption.HashName = "SHA1";
			Encryption.KeySize = 256;
			Encryption.Iv = "@1B2c3D4e5F6g7H8";
			Encryption.Iterations = 2;
			Encryption.Password = "s0ftsm@rt";
			Encryption.Salt = "s^$tem$";
		}

		public static string Decrypt(string stringToDecript)
		{
			byte[] ivBytes = Encoding.UTF8.GetBytes(Encryption.Iv);
			byte[] saltBytes = Encoding.UTF8.GetBytes(Encryption.Salt);
			byte[] stringToDecriptBytes = Convert.FromBase64String(stringToDecript);
			PasswordDeriveBytes password = new PasswordDeriveBytes(Encryption.Password, saltBytes, Encryption.HashName, Encryption.Iterations);
			byte[] keyBytes = password.GetBytes(Encryption.KeySize / 8);
			RijndaelManaged symmetricKey = new RijndaelManaged()
			{
				Mode = CipherMode.CBC
			};
			ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, ivBytes);
			MemoryStream memoryStream = new MemoryStream(stringToDecriptBytes);
			CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
			byte[] stringToEncriptBytes = new byte[(int)stringToDecriptBytes.Length];
			int decryptedByteCount = cryptoStream.Read(stringToEncriptBytes, 0, (int)stringToEncriptBytes.Length);
			memoryStream.Close();
			cryptoStream.Close();
			return Encoding.UTF8.GetString(stringToEncriptBytes, 0, decryptedByteCount);
		}

		public static string Encrypt(string stringToEncript)
		{
			byte[] ivBytes = Encoding.UTF8.GetBytes(Encryption.Iv);
			byte[] saltBytes = Encoding.UTF8.GetBytes(Encryption.Salt);
			byte[] stringToEncriptBytes = Encoding.UTF8.GetBytes(stringToEncript);
			PasswordDeriveBytes password = new PasswordDeriveBytes(Encryption.Password, saltBytes, Encryption.HashName, Encryption.Iterations);
			byte[] keyBytes = password.GetBytes(Encryption.KeySize / 8);
			RijndaelManaged symmetricKey = new RijndaelManaged()
			{
				Mode = CipherMode.CBC
			};
			ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, ivBytes);
			MemoryStream memoryStream = new MemoryStream();
			CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
			cryptoStream.Write(stringToEncriptBytes, 0, (int)stringToEncriptBytes.Length);
			cryptoStream.FlushFinalBlock();
			byte[] stringToDecriptBytes = memoryStream.ToArray();
			memoryStream.Close();
			cryptoStream.Close();
			return Convert.ToBase64String(stringToDecriptBytes);
		}
	}
}