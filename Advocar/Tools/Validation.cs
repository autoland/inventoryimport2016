using System;
using System.Text.RegularExpressions;

namespace Advocar.Tools
{
	public class Validation
	{
		public Validation()
		{
		}

		public static string DeNullifyString(string myObject)
		{
			return (myObject != null ? myObject : "");
		}

		public static bool IsDate(string datevalue)
		{
			bool flag;
			try
			{
				DateTime.Parse(datevalue);
			}
			catch
			{
				flag = false;
				return flag;
			}
			flag = true;
			return flag;
		}

		public static bool IsNull(object boolvalue)
		{
			return (boolvalue != null ? false : true);
		}

		public static bool IsNumeric(string numericvalue)
		{
			bool flag;
			try
			{
				float.Parse(numericvalue);
			}
			catch
			{
				flag = false;
				return flag;
			}
			flag = true;
			return flag;
		}

		public static bool IsNumericPattern(string numericvalue)
		{
			Regex natural = new Regex("0*[1-9][0-9]*");
			return ((new Regex("[^0-9]")).IsMatch(numericvalue) ? false : natural.IsMatch(numericvalue));
		}

		public static string PhoneFormat(string phonevalue)
		{
			string str;
			if (Validation.IsNull(phonevalue))
			{
				phonevalue = "";
			}
			long stripedNumber = long.Parse(Validation.StripNumber(phonevalue));
			if (stripedNumber != (long)0)
			{
				string temp = "";
				temp = (stripedNumber.ToString().Length <= 10 ? stripedNumber.ToString("###-###-####") : stripedNumber.ToString("###-###-#### Ext. ####"));
				str = temp;
			}
			else
			{
				str = "";
			}
			return str;
		}

		public static string StripNumber(string number)
		{
			string temp = "";
			for (int row = 0; number.Length > row; row++)
			{
				if (Validation.IsNumeric(number.Substring(row, 1).ToString()))
				{
					temp = string.Concat(temp, number.Substring(row, 1).ToString());
				}
			}
			if (temp.Length == 0)
			{
				temp = "0";
			}
			return temp;
		}
	}
}