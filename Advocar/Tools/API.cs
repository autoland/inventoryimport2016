using System;
using System.Text;
using System.Runtime.InteropServices;

namespace Advocar.Tools
{
    public class API
    {
        public API() { }

        [DllImport("urlmon.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern Int32 URLDownloadToFile([MarshalAs(UnmanagedType.IUnknown)] object caller, [MarshalAs(UnmanagedType.LPWStr)] string url,
            [MarshalAs(UnmanagedType.LPWStr)] string fileName, Int32 reserved, IntPtr pointer);
        public static Int32 URLDownloadToFile(string url, string fileName) { return Advocar.Tools.API.URLDownloadToFile(null, url, fileName, 0, IntPtr.Zero); }

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindow(string className, string windowName);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr FindWindowEx(IntPtr parentHandle, IntPtr childHandle, string className, IntPtr windowTitle);
        public static IntPtr FindWindowEx(IntPtr parentHandle, string className) { return Advocar.Tools.API.FindWindowEx(parentHandle, IntPtr.Zero, className, IntPtr.Zero); }

        [DllImport("kernel32.dll")]
        public static extern uint GetProcessId(IntPtr handle);

        [DllImport("kernel32.dll")]
        private static extern int CreateProcess(int applicationName, string commandLine, int processAttributes, int threadAttributes, bool inheritHandles, int creationFlags,
            int environment, int currentDirectory, ref STARTUPINFO startupInfo, ref PROCESS_INFORMATION processInfo);
        public static int CreateProcess(string command, int processID)
        {
            STARTUPINFO startup = new STARTUPINFO(); startup.cb = (Int32)Marshal.SizeOf(startup);
            PROCESS_INFORMATION processinfo = new PROCESS_INFORMATION();
            processinfo.dwProcessId = 0; processinfo.dwThreadId = 0; processinfo.hProcess = IntPtr.Zero; processinfo.hThread = IntPtr.Zero;

            CreateProcess(processID, command, 0, 0, false, 32, 0, 0, ref startup, ref processinfo);
            return processinfo.dwProcessId;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct STARTUPINFO
        {
            public Int32 cb;
            public string lpReserved;
            public string lpDesktop;
            public string lpTitle;
            public Int32 dwX;
            public Int32 dwY;
            public Int32 dwXSize;
            public Int32 dwYSize;
            public Int32 dwXCountChars;
            public Int32 dwYCountChars;
            public Int32 dwFillAttribute;
            public Int32 dwFlags;
            public Int16 wShowWindow;
            public Int16 cbReserved2;
            public IntPtr lpReserved2;
            public IntPtr hStdInput;
            public IntPtr hStdOutput;
            public IntPtr hStdError;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct PROCESS_INFORMATION
        {
            public IntPtr hProcess;
            public IntPtr hThread;
            public int dwProcessId;
            public int dwThreadId;
        }

        [DllImport("user32.dll")]
        private static extern bool GetWindowInfo(IntPtr hwnd, ref WINDOWINFO pwi);
        public static WINDOWINFO GetWindowInfo(IntPtr hwnd)
        {
            WINDOWINFO info = new WINDOWINFO(); info.cbSize = (uint)Marshal.SizeOf(info);
            Advocar.Tools.API.GetWindowInfo(hwnd, ref info);
            return info;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct WINDOWINFO
        {
            public uint cbSize;
            public RECT rcWindow;
            public RECT rcClient;
            public uint dwStyle;
            public uint dwExStyle;
            public uint dwWindowStatus;
            public uint cxWindowBorders;
            public uint cyWindowBorders;
            public ushort atomWindowType;
            public ushort wCreatorVersion;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
        }

        [DllImport("user32.dll")]
        public static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, StringBuilder lParam);

        [DllImport("user32.dll")]
        public static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, int wParam, int lParam);

        #region Messages
        private const UInt32 WM_SETTEXT = 0x000C;
        public const UInt32 BM_SETCHECK = 0x00F1;
        public const UInt32 BM_CLICK = 0x00F5;
        #endregion

        public static void SetWindowText(IntPtr hwnd, string text)
        {
            StringBuilder mytext = new StringBuilder(text);
            Advocar.Tools.API.SendMessage(hwnd, Advocar.Tools.API.WM_SETTEXT, IntPtr.Zero, mytext);
        }

        [DllImport("user32.dll")]
        public static extern IntPtr GetWindow(IntPtr hWnd, Advocar.Tools.API.GetWindowCommand command);
        public enum GetWindowCommand { First = 0, Last = 1, Next = 2, Prev = 3, Owner = 4, Child = 5 }

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private static extern int GetWindowTextLength(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private static extern int GetWindowText(IntPtr hWnd, [Out] StringBuilder lpString, int nMaxCount);
        public static string GetWindowText(IntPtr hwnd)
        {
            int length = Advocar.Tools.API.GetWindowTextLength(hwnd);
            StringBuilder mytext = new StringBuilder("", 255);
            Advocar.Tools.API.GetWindowText(hwnd, mytext, mytext.Capacity);
            return mytext.ToString();
        }

        [DllImport("user32.dll")]
        public static extern IntPtr SetActiveWindow(IntPtr hWnd);
    }
}
