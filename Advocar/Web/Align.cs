using System;

namespace Advocar.Web
{
	public enum Align
	{
		left,
		center,
		right
	}
}