using System;

namespace Advocar.Web
{
	internal class Ajax
	{
		public const string ContentDelimiter = "@@@@";

		public const string ValuePairStart = "##VALUEPAIRSTART##";

		public const string ValuePairEnd = "##VALUEPAIREND##";

		public const string RenderStart = "##RENDERSTART##";

		public const string RenderEnd = "##RENDEREND##";

		public const string CustomStart = "##CUSTOMSTART##";

		public const string CustomEnd = "##CUSTOMEND##";

		private string renderItemId = "";

		private string preliminaryHtml = "";

		private string actionList = "";

		private string command = "";

		private bool useSpanNotDiv = false;

		private bool debug = false;

		public string ActionList
		{
			get
			{
				return this.actionList;
			}
			set
			{
				this.actionList = value;
			}
		}

		public string Command
		{
			get
			{
				return this.command;
			}
			set
			{
				this.command = value;
			}
		}

		public bool Debug
		{
			get
			{
				return this.debug;
			}
			set
			{
				this.debug = value;
			}
		}

		public string PreliminaryHtml
		{
			get
			{
				return this.preliminaryHtml;
			}
			set
			{
				this.preliminaryHtml = value;
			}
		}

		public string RenderItemId
		{
			get
			{
				return this.renderItemId;
			}
			set
			{
				this.renderItemId = value;
			}
		}

		public bool UseSpanNotDiv
		{
			get
			{
				return this.useSpanNotDiv;
			}
			set
			{
				this.useSpanNotDiv = value;
			}
		}

		public Ajax()
		{
		}

		public Ajax(string RenderItemId, string PreliminaryHtml, string ActionList, string Command, bool UseSpanNotDiv)
		{
			this.RenderItemId = RenderItemId;
			this.PreliminaryHtml = PreliminaryHtml;
			this.ActionList = ActionList;
			this.Command = Command;
			this.UseSpanNotDiv = UseSpanNotDiv;
		}

		public override string ToString()
		{
			string element = (this.useSpanNotDiv ? "span" : "div");
			string str = this.actionList;
			char[] charArray = new char[] { ",".ToCharArray()[0] };
			string[] action = str.Split(charArray);
			string[] renderItemId = new string[] { "<", element, " id=\"", this.renderItemId, "\"", null, null, null, null, null, null, null, null };
			renderItemId[5] = (this.command.Length > 0 ? string.Concat(" ", this.command) : "");
			renderItemId[6] = ">";
			renderItemId[7] = this.preliminaryHtml;
			renderItemId[8] = "</";
			renderItemId[9] = element;
			renderItemId[10] = ">\n\n<script language=\"javascript\">\nvar AjaxObject_";
			renderItemId[11] = this.renderItemId;
			renderItemId[12] = ";\n\n";
			string html = string.Concat(renderItemId);
			for (int index = 0; (int)action.Length > index; index++)
			{
				renderItemId = new string[] { html, "function Advocar_Ajax_Action_", action[index], "(){\n    var url = document.URL;\n    if (url.indexOf('?') >= 0){url = url + '&ajax=", action[index], "';} else {url = url + '?ajax=", action[index], "';}\n\n", (this.debug ? "alert('url=' + url);\n" : ""), "    AjaxObject_", this.renderItemId, " = Advocar_Ajax_Start();\n", (this.debug ? "alert('Advocar_Ajax_Start Passed');\n" : ""), "    AjaxObject_", this.renderItemId, ".onreadystatechange = Advocar_Ajax_", this.RenderItemId, "Postback;\n", (this.debug ? "alert('Postback passed');\n" : ""), "    AjaxObject_", this.renderItemId, ".open('post', url, true);\n", (this.debug ? "alert('Open Post URL Passed');\n" : ""), "    AjaxObject_", this.renderItemId, ".send(Advocar_Ajax_GetFormValues());\n", (this.debug ? "alert('Send Passed: ' + Advocar_Ajax_GetFormValues());\n" : ""), "    document.body.style.cursor = 'wait';\n", (this.debug ? "document.body.style.cursor = 'auto';\n" : ""), "}\n\n" };
				html = string.Concat(renderItemId);
			}
			renderItemId = new string[] { html, "function Advocar_Ajax_", this.renderItemId, "Postback(){\n\ttry{\n\t\tif (AjaxObject_", this.renderItemId, ".readyState == 4", (this.debug ? "){\n" : string.Concat(" && AjaxObject_", this.renderItemId, ".status == 200){\n")), "\t\t\tdocument.body.style.cursor = 'auto';\n\n\t\t\tvar result = AjaxObject_", this.renderItemId, ".responseText;\n", (this.debug ? "alert(result);" : ""), "\t\t\tvar start = 0; end = 0;\n\t\t\tvar myResults = result.split('@@@@');\n\n\t\t\tfor (var index = 0; myResults.length > index; index++){\n\t\t\t\tresult = myResults[index];\n\t\t\t\tif (result.indexOf('##VALUEPAIRSTART##') >=0 && result.indexOf('##VALUEPAIREND##') >=0){\n\t\t\t\t\tstart = eval(result.indexOf('##VALUEPAIRSTART##') + '##VALUEPAIRSTART##'.length);\n\t\t\t\t\tend = result.indexOf('##VALUEPAIREND##');\n\t\t\t\t\tresult = result.substring(start, end);\n", (this.debug ? "alert('Value Pair Results: ' + result);" : ""), "\t\t\t\t\tif (result.length){Advocar_Ajax_SetFormValues(result);}\n\n", (this.debug ? "alert('Processed Value Pairs');" : ""), "\t\t\t\t} else if (result.indexOf('##RENDERSTART##') >=0 && result.indexOf('##RENDEREND##') >=0){\n\t\t\t\t\tstart = eval(result.indexOf('##RENDERSTART##') + '##RENDERSTART##'.length);\n\t\t\t\t\tend = result.indexOf('##RENDEREND##');\n\t\t\t\t\tresult = result.substring(start, end);\n", (this.debug ? "alert('Render Result: ' + result);" : ""), "\t\t\t\t\tdocument.getElementById('", this.renderItemId, "').innerHTML = result;\n\n", (this.debug ? "alert('Processed Render Item');" : ""), "\t\t\t\t} else if (result.indexOf('##CUSTOMSTART##') >=0 && result.indexOf('##CUSTOMEND##') >=0){\n\t\t\t\t\tstart = eval(result.indexOf('##CUSTOMSTART##') + '##CUSTOMSTART##'.length);\n\t\t\t\t\tend = result.indexOf('##CUSTOMEND##');\n\t\t\t\t\tresult = result.substring(start, end);\n", (this.debug ? "alert('Custom Function Results: ' + result);" : ""), "\t\t\t\t\tif (result.length > 0){eval(result);}\n", (this.debug ? "alert('Process Custom Functions');" : ""), "\t\t\t\t}\n\t\t\t}\n\t\t}\n\t} catch(e){alert('Sorry, there was a communication Error, please try again!'); my", this.renderItemId, " = null;}\n\n\tdocument.body.style.cursor = 'auto';\n}\n\ndocument.getElementById('", this.renderItemId, "').style.margin = '0px';\n</script>\n" };
			html = string.Concat(renderItemId);
			return html;
		}
	}
}