using System;

namespace Advocar.Web
{
	public class TableSorting
	{
		private string sorting = "";

		private Advocar.Web.SortOrder sortOrder = Advocar.Web.SortOrder.@ascending;

		public string Sorting
		{
			get
			{
				return this.sorting;
			}
			set
			{
				this.sorting = value;
			}
		}

		public Advocar.Web.SortOrder SortOrder
		{
			get
			{
				return this.sortOrder;
			}
		}

		public TableSorting()
		{
		}
	}
}