using Advocar.Data;
using Advocar.Interface;
using System;

namespace Advocar.Web
{
	public class TableSettings : Xml
	{
		public TableColumn[] Columns = new TableColumn[0];

		public string ProcedureName = "";

		public string KeyField = "";

		public string Title = "";

		public int Rows = 25;

		public int Width = 750;

		public Advocar.Web.Align Align = Advocar.Web.Align.left;

		public Advocar.Web.Sorting Sorting = Advocar.Web.Sorting.normal;

		public bool RowNumbers = false;

		public string TableId = "";

		public DataAccessParameter[] Parameters = new DataAccessParameter[0];

		public string Connection = "";

		public TableSettings()
		{
		}
	}
}