using System;

namespace Advocar.Web.Controls
{
	public class Radio : Control
	{
		private string radioLabel = "";

		private string radioChecked = "";

		private string radioValue = "";

		public string RadioChecked
		{
			get
			{
				return this.radioChecked;
			}
			set
			{
				this.radioChecked = value;
			}
		}

		public string RadioLabel
		{
			get
			{
				return this.radioLabel;
			}
			set
			{
				this.radioLabel = value;
			}
		}

		public string RadioValue
		{
			get
			{
				return this.radioValue;
			}
			set
			{
				this.radioValue = value;
			}
		}

		public Radio() : base("inputRadio")
		{
		}

		public Radio(string key, string assignedValue, string radioValue, string radioLabel, string command, string stylesheet) : base(key, assignedValue, command, false, stylesheet)
		{
			this.initialize(radioLabel, radioValue);
		}

		public Radio(string key, string assignedValue, string radioValue, string radioLabel, string command) : base(key, assignedValue, command, false, "inputRadio")
		{
			this.initialize(radioLabel, radioValue);
		}

		public Radio(string key, string assignedValue, string radioValue, string radioLabel) : base(key, assignedValue, "", false, "inputRadio")
		{
			this.initialize(radioLabel, radioValue);
		}

		private void initialize(string label, string radioValue)
		{
			base.Stylesheet = (base.Stylesheet.Length > 0 ? base.Stylesheet : "inputRadio");
			this.radioChecked = (base.AssignedValue == radioValue ? " checked" : "");
			this.radioLabel = label;
			this.radioValue = radioValue;
		}

		public override string ToString()
		{
			string str;
			string[] stylesheet = new string[] { "<input type=\"radio\" class=\"", base.Stylesheet, "\" id=\"", base.Key, this.radioValue, "\" name=\"", base.Key, "\" value=\"", this.radioValue, "\"", this.radioChecked, base.Command, ">", null };
			string[] strArrays = stylesheet;
			if (this.radioLabel.Length > 0)
			{
				string[] stylesheet1 = new string[] { "<label class=\"", base.Stylesheet, "\" for=\"", base.Key, this.radioValue, "\">", this.radioLabel, "</label>" };
				str = string.Concat(stylesheet1);
			}
			else
			{
				str = "";
			}
			strArrays[13] = str;
			return string.Concat(stylesheet);
		}
	}
}