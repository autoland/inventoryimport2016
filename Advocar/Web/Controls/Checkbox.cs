using System;

namespace Advocar.Web.Controls
{
	public class Checkbox : Control
	{
		private string checkboxLabel = "";

		private string checkboxChecked = "";

		private string checkboxValue = "";

		public string CheckboxChecked
		{
			get
			{
				return this.checkboxChecked;
			}
		}

		public string CheckboxLabel
		{
			get
			{
				return this.checkboxLabel;
			}
			set
			{
				this.checkboxLabel = value;
			}
		}

		public string CheckboxValue
		{
			get
			{
				return this.checkboxValue;
			}
			set
			{
				this.checkboxValue = value;
			}
		}

		public Checkbox() : base("inputCheckbox")
		{
		}

		public Checkbox(string key, string assignedValue, string checkboxValue, string checkboxLabel) : base(key, assignedValue, "", false, "inputCheckbox")
		{
			this.initialize(checkboxLabel, checkboxValue);
		}

		public Checkbox(string key, string assignedValue, string checkboxValue, string checkboxLabel, string command, string stylesheet) : base(key, assignedValue, command, false, stylesheet)
		{
			this.initialize(checkboxLabel, checkboxValue);
		}

		public Checkbox(string key, string assignedValue, string checkboxValue, string checkboxLabel, string command) : base(key, assignedValue, command, false, "inputCheckbox")
		{
			this.initialize(checkboxLabel, checkboxValue);
		}

		private void initialize(string label, string checkboxValue)
		{
			base.Stylesheet = (base.Stylesheet.Length > 0 ? base.Stylesheet : "inputCheckbox");
			this.checkboxChecked = (base.AssignedValue == checkboxValue ? " checked" : "");
			this.checkboxValue = checkboxValue;
			this.checkboxLabel = label;
		}

		public override string ToString()
		{
			string str;
			string[] stylesheet = new string[] { "<input type=\"checkbox\" class=\"", base.Stylesheet, "\" id=\"", base.Key, "\" name=\"", base.Key, "\" value=\"", this.checkboxValue, "\"", this.checkboxChecked, base.Command, ">", null };
			string[] strArrays = stylesheet;
			if (this.checkboxLabel.Length > 0)
			{
				string[] stylesheet1 = new string[] { "<label class=\"", base.Stylesheet, "\" for=\"", base.Key, "\">", this.checkboxLabel, "</label>" };
				str = string.Concat(stylesheet1);
			}
			else
			{
				str = "";
			}
			strArrays[12] = str;
			return string.Concat(stylesheet);
		}
	}
}