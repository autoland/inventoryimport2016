using System;

namespace Advocar.Web.Controls
{
	public class File : Control
	{
		private int width = 0;

		public int Width
		{
			get
			{
				return this.width;
			}
			set
			{
				this.width = value;
			}
		}

		public File() : base("inputFile")
		{
		}

		public File(string key, string assignedValue, int width) : base(key, assignedValue, "", false, "inputFile")
		{
			this.initialize(width);
		}

		public File(string key, string assignedValue, int width, string command) : base(key, assignedValue, command, false, "inputFile")
		{
			this.initialize(width);
		}

		public File(string key, string assignedValue, int width, string command, bool required) : base(key, assignedValue, command, required, "inputFile")
		{
			this.initialize(width);
		}

		private void initialize(int width)
		{
			this.width = width;
		}

		public override string ToString()
		{
			string[] stylesheet = new string[] { "<input type=\"file\" class=\"", base.Stylesheet, "\" name=\"", base.Key, "\" style=\"width:", this.width.ToString(), "px;\"", base.Command, ">" };
			string temp = string.Concat(stylesheet);
			if (base.Required)
			{
				temp = string.Concat(temp, base.RequiredStyle);
			}
			return temp;
		}
	}
}