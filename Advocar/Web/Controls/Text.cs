using System;

namespace Advocar.Web.Controls
{
	public class Text : Control
	{
		private int width = 0;

		private int maxLength = 0;

		public int MaxLength
		{
			get
			{
				return this.maxLength;
			}
			set
			{
				this.maxLength = value;
			}
		}

		public int Width
		{
			get
			{
				return this.width;
			}
			set
			{
				this.width = value;
			}
		}

		public Text() : base("inputText")
		{
		}

		public Text(string key, string assignedValue, int width, int maxlength) : base(key, assignedValue, "", false, "inputText")
		{
			this.initialize(width, maxlength);
		}

		public Text(string key, string assignedValue, int width, int maxlength, string command) : base(key, assignedValue, command, false, "inputText")
		{
			this.initialize(width, maxlength);
		}

		public Text(string key, string assignedValue, int width, int maxlength, string command, bool required) : base(key, assignedValue, command, required, "inputText")
		{
			this.initialize(width, maxlength);
		}

		public Text(string key, string assignedValue, int width, int maxlength, string command, bool required, string stylesheet) : base(key, assignedValue, command, required, stylesheet)
		{
			this.initialize(width, maxlength);
		}

		private void initialize(int width, int maxlength)
		{
			this.width = (width == 0 ? 200 : width);
			this.maxLength = (maxlength == 0 ? 500 : maxlength);
			base.Stylesheet = (base.Stylesheet.Length > 0 ? base.Stylesheet : "inputText");
		}

		public override string ToString()
		{
			string temp = base.ToString();
			string str = temp;
			string[] stylesheet = new string[] { str, "<input type=\"text\" class=\"", base.Stylesheet, "\" id=\"", base.Key, "\" name=\"", base.Key, "\" value=\"", base.AssignedValue, "\" style=\"width:", this.width.ToString(), "px;\" maxlength=\"", this.maxLength.ToString(), "\"", base.Command, ">" };
			temp = string.Concat(stylesheet);
			if (base.Required)
			{
				temp = string.Concat(temp, base.RequiredStyle);
			}
			return temp;
		}
	}
}