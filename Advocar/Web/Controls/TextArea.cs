using System;

namespace Advocar.Web.Controls
{
	public class TextArea : Control
	{
		private int width = 0;

		private int height = 0;

		private int maxLength = 0;

		public int Height
		{
			get
			{
				return this.height;
			}
			set
			{
				this.height = value;
			}
		}

		public int MaxLength
		{
			get
			{
				return this.maxLength;
			}
			set
			{
				this.maxLength = value;
			}
		}

		public int Width
		{
			get
			{
				return this.width;
			}
			set
			{
				this.width = value;
			}
		}

		public TextArea() : base("inputText")
		{
		}

		public TextArea(string key, string assignedValue, int width, int height, int maxlength) : base(key, assignedValue, "", false, "inputText")
		{
			this.initialize(width, maxlength, height);
		}

		public TextArea(string key, string assignedValue, int width, int height, int maxlength, string command) : base(key, assignedValue, command, false, "inputText")
		{
			this.initialize(width, maxlength, height);
		}

		public TextArea(string key, string assignedValue, int width, int height, int maxlength, string command, bool required) : base(key, assignedValue, command, required, "inputText")
		{
			this.initialize(width, maxlength, height);
		}

		private void initialize(int width, int maxlength, int height)
		{
			this.width = (width == 0 ? 200 : width);
			this.maxLength = (maxlength == 0 ? 500 : maxlength);
			this.height = (height == 0 ? 100 : height);
		}

		public override string ToString()
		{
			string[] key = new string[] { "<textarea class=\"inputText\" name=\"", base.Key, "\" style=\"width:", this.width.ToString(), "px; height:", this.height.ToString(), "px;\" maxlength=\"", this.maxLength.ToString(), "\"", base.Command, ">", base.AssignedValue, "</textarea>" };
			string temp = string.Concat(key);
			if (base.Required)
			{
				temp = string.Concat(temp, base.RequiredStyle);
			}
			return temp;
		}
	}
}