using Advocar.Tools;
using System;

namespace Advocar.Web.Controls
{
	public class Date : Control
	{
		private DateTime datevalue = DateTime.Now;

		private int month = DateTime.Now.Month;

		private int day = DateTime.Now.Day;

		private int year = DateTime.Now.Year;

		public DateTime DateValue
		{
			get
			{
				return this.datevalue;
			}
		}

		public int Day
		{
			get
			{
				return this.day;
			}
			set
			{
				this.day = value;
			}
		}

		public int Month
		{
			get
			{
				return this.month;
			}
			set
			{
				this.month = value;
			}
		}

		public int Year
		{
			get
			{
				return this.year;
			}
			set
			{
				this.year = value;
			}
		}

		public Date() : base("inputText")
		{
		}

		public Date(string key, string assignedValue) : base(key, assignedValue, "", false, "inputText")
		{
			this.initialize();
		}

		public Date(string key, string assignedValue, string command) : base(key, assignedValue, command, false, "inputText")
		{
			this.initialize();
		}

		public Date(string key, string assignedValue, string command, bool required) : base(key, assignedValue, command, required, "inputText")
		{
			this.initialize();
		}

		public Date(string key, string assignedValue, string command, bool required, string stylesheet) : base(key, assignedValue, command, required, stylesheet)
		{
			this.initialize();
		}

		private void initialize()
		{
			if (!Validation.IsDate(base.AssignedValue))
			{
				this.month = 0;
				this.day = 0;
				this.year = 0;
			}
			else
			{
				DateTime date = DateTime.Parse(base.AssignedValue);
				this.month = date.Month;
				this.day = date.Day;
				this.year = date.Year;
			}
			base.Stylesheet = (base.Stylesheet.Length > 0 ? base.Stylesheet : "inputText");
		}

		public override string ToString()
		{
			string[] key = new string[] { "document.form.", base.Key, ".value = document.form.", base.Key, "_month.value + '/' + document.form.", base.Key, "_day.value + '/' + document.form.", base.Key, "_year.value;" };
			string onchange = string.Concat(key);
			key = new string[] { "<input onchange=\"", onchange, "\" type=\"text\" class=\"", base.Stylesheet, "\" name=\"", base.Key, "_month\" value=\"", (this.month > 0 ? this.month.ToString("00") : ""), "\" style=\"width:30px;\" maxlength=\"2\"", base.Command, "> / <input onchange=\"", onchange, "\" type=\"text\" class=\"", base.Stylesheet, "\" name=\"", base.Key, "_day\" value=\"", (this.day > 0 ? this.day.ToString("00") : ""), "\" style=\"width:30px;\" maxlength=\"2\"", base.Command, "> / <input onchange=\"", onchange, "\" type=\"text\" class=\"", base.Stylesheet, "\" name=\"", base.Key, "_year\" value=\"", (this.year > 0 ? this.year.ToString("0000") : ""), "\" style=\"width:40px;\" maxlength=\"4\"", base.Command, "><input type=\"hidden\" id=\"", base.Key, "\" name=\"", base.Key, "\" value=\"", base.AssignedValue, "\">" };
			string temp = string.Concat(key);
			if (base.Required)
			{
				temp = string.Concat(temp, base.RequiredStyle);
			}
			return temp;
		}
	}
}