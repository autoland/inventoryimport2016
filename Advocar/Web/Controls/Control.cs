using Advocar.Tools;
using System;
using System.Text;

namespace Advocar.Web.Controls
{
	public abstract class Control
	{
		private string requiredStyle = "<span class=\"inputRequired\">&nbsp;*</span>";

		private string indent = "\t\t\t\t";

		private string key = "";

		private string assignedValue = "";

		private string command = "";

		private bool required = false;

		private string stylesheet = "";

		public string AssignedValue
		{
			get
			{
				return this.assignedValue;
			}
			set
			{
				this.assignedValue = value;
			}
		}

		public string Command
		{
			get
			{
				return this.command;
			}
			set
			{
				this.command = value;
			}
		}

		public string Indent
		{
			get
			{
				return this.indent;
			}
		}

		public string Key
		{
			get
			{
				return this.key;
			}
		}

		public bool Required
		{
			get
			{
				return this.required;
			}
			set
			{
				this.required = value;
			}
		}

		public string RequiredStyle
		{
			get
			{
				return this.requiredStyle;
			}
		}

		public string Stylesheet
		{
			get
			{
				return this.stylesheet;
			}
			set
			{
				this.stylesheet = value;
			}
		}

		public Control()
		{
		}

		public Control(string stylesheet)
		{
			this.stylesheet = stylesheet;
		}

		public Control(string key, string assignedValue, string command, bool required, string stylesheet)
		{
			string str;
			this.key = key.Trim();
			this.assignedValue = (Validation.IsNull(assignedValue) ? "" : assignedValue.Trim());
			if (command.Length > 0)
			{
				str = string.Concat(" ", command);
				command = str;
			}
			else
			{
				str = "";
			}
			this.command = str;
			this.required = required;
			this.stylesheet = stylesheet;
		}

		public override string ToString()
		{
			return (new StringBuilder()).ToString();
		}
	}
}