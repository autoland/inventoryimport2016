using System;

namespace Advocar.Web.Controls
{
	public class Button : Control
	{
		private string action = "";

		private int width = 0;

		public string Action
		{
			get
			{
				return this.action;
			}
		}

		public int Width
		{
			get
			{
				return this.width;
			}
			set
			{
				this.width = value;
			}
		}

		public Button() : base("inputButton")
		{
		}

		public Button(string key, string action, int width) : base(key, "", "", false, "inputButton")
		{
			this.initialize(action, width);
		}

		public Button(string key, string action, int width, string command, string stylesheet) : base(key, "", command, false, stylesheet)
		{
			this.initialize(action, width);
		}

		public Button(string key, string action, int width, string command) : base(key, "", command, false, "inputButton")
		{
			this.initialize(action, width);
		}

		private void initialize(string action, int width)
		{
			string str;
			if (action.Length > 0)
			{
				str = string.Concat(" onclick=\"", action, ";\"");
				action = str;
			}
			else
			{
				str = "";
			}
			this.action = str;
			this.width = width;
		}

		public override string ToString()
		{
			string[] stylesheet = new string[] { "<input class=\"", base.Stylesheet, "\" type=\"button\" value=\"", base.Key, "\"", this.action, base.Command, null, null };
			stylesheet[7] = (this.width > 0 ? string.Concat(" style=\"width: ", this.width, "px;\"") : "");
			stylesheet[8] = ">";
			return string.Concat(stylesheet);
		}
	}
}