using Advocar.Data;
using System;

namespace Advocar.Web.Controls
{
	public class Select : Control
	{
		private int width = 0;

		private DataAccess data = null;

		private string fieldCode = "";

		private string fieldDesc = "";

		private string query = "";

		private string options = "";

		public DataAccess Data
		{
			get
			{
				return this.data;
			}
			set
			{
				this.data = value;
			}
		}

		public string FieldCode
		{
			get
			{
				return this.fieldCode;
			}
			set
			{
				this.fieldCode = value;
			}
		}

		public string FieldDesc
		{
			get
			{
				return this.fieldDesc;
			}
			set
			{
				this.fieldDesc = value;
			}
		}

		public string Query
		{
			get
			{
				return this.query;
			}
			set
			{
				this.query = value;
			}
		}

		public int Width
		{
			get
			{
				return this.width;
			}
			set
			{
				this.width = value;
			}
		}

		public Select() : base("inputSelect")
		{
		}

		public Select(string key, string assignedValue, int width, DataAccess data, string fieldCode, string fieldDesc, string command, bool required) : base(key, assignedValue, command, required, "inputSelect")
		{
			this.initialize(width, data, fieldCode, fieldDesc);
		}

		public Select(string key, string assignedValue, int width, DataAccess data, string fieldCode, string fieldDesc) : base(key, assignedValue, "", false, "inputSelect")
		{
			this.initialize(width, data, fieldCode, fieldDesc);
		}

		public Select(string key, string assignedValue, int width, DataAccess data) : base(key, assignedValue, "", false, "inputSelect")
		{
			this.initialize(width, data, "", "");
		}

		public Select(string key, string assignedValue, int width, string optionList, string command) : base(key, assignedValue, command, false, "inputSelect")
		{
			this.width = (width == 0 ? 200 : width);
			this.options = optionList;
		}

		private void initialize(int width, DataAccess data, string fieldCode, string fieldDesc)
		{
			this.width = (width == 0 ? 200 : width);
			this.data = data;
			this.fieldCode = (fieldCode.Length == 0 ? "CodeID" : fieldCode);
			this.fieldDesc = (fieldDesc.Length == 0 ? "Description" : fieldDesc);
			string temp = "<option value=\"\"></option>";
			while (!data.EOF)
			{
				string selected = (base.AssignedValue == data.AssignValue(this.fieldCode) ? " selected" : "");
				string[] strArrays = new string[] { temp, "<option value=\"", data.AssignValue(this.fieldCode), "\"", selected, ">", data.AssignValue(this.fieldDesc), "</option>\n" };
				temp = string.Concat(strArrays);
				data.MoveNext();
			}
			this.options = temp;
		}

		public override string ToString()
		{
			string[] key = new string[] { "<select class=\"inputSelect\" name=\"", base.Key, "\" style=\"width:", this.width.ToString(), "px;\"", base.Command, ">", this.options, "</select>" };
			string temp = string.Concat(key);
			if (base.Required)
			{
				temp = string.Concat(temp, base.RequiredStyle);
			}
			return temp;
		}
	}
}