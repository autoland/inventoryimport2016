using System;

namespace Advocar.Web.Controls
{
	public class Hidden : Control
	{
		public Hidden()
		{
		}

		public Hidden(string key, string assignedValue) : base(key, assignedValue, "", false, "")
		{
		}

		public override string ToString()
		{
			string[] key = new string[] { "<input type=\"hidden\" id=\"", base.Key, "\" name=\"", base.Key, "\" value=\"", base.AssignedValue, "\">" };
			return string.Concat(key);
		}
	}
}