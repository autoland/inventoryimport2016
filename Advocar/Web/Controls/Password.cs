using System;

namespace Advocar.Web.Controls
{
	public class Password : Control
	{
		private int width = 0;

		private int maxLength = 0;

		public int MaxLength
		{
			get
			{
				return this.maxLength;
			}
			set
			{
				this.maxLength = value;
			}
		}

		public int Width
		{
			get
			{
				return this.width;
			}
			set
			{
				this.width = value;
			}
		}

		public Password() : base("inputText")
		{
		}

		public Password(string key, string assignedValue) : base(key, assignedValue, "", false, "")
		{
			this.initialize(0, 0);
		}

		public Password(string key, string assignedValue, int width, int maxlength) : base(key, assignedValue, "", false, "inputText")
		{
			this.initialize(width, maxlength);
		}

		public Password(string key, string assignedValue, int width, int maxlength, string command) : base(key, assignedValue, command, false, "inputText")
		{
			this.initialize(width, maxlength);
		}

		public Password(string key, string assignedValue, int width, int maxlength, string command, bool required) : base(key, assignedValue, command, required, "inputText")
		{
			this.initialize(width, maxlength);
		}

		private void initialize(int width, int maxlength)
		{
			this.width = (width == 0 ? 150 : width);
			this.maxLength = (maxlength == 0 ? 20 : maxlength);
		}

		public override string ToString()
		{
			string[] stylesheet = new string[] { "<input type=\"password\" class=\"", base.Stylesheet, "\" id=\"", base.Key, "\" name=\"", base.Key, "\" value=\"", base.AssignedValue, "\" style=\"width:", this.width.ToString(), "px;\" maxlength=\"", this.maxLength.ToString(), "\"", base.Command, ">" };
			string temp = string.Concat(stylesheet);
			if (base.Required)
			{
				temp = string.Concat(temp, base.RequiredStyle);
			}
			return temp;
		}
	}
}