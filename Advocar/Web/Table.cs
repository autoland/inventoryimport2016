using Advocar.Data;
using Advocar.Interface;
using Advocar.Tools;
using Advocar.Web.Controls;
using System;

namespace Advocar.Web
{
	public class Table : DataAccess
	{
		private string keyField = "";

		private string title = "";

		private int rows = 25;

		private int width = 750;

		private Advocar.Web.Align align = Advocar.Web.Align.center;

		private Advocar.Web.Sorting sorting = Advocar.Web.Sorting.normal;

		private string sortBy = "";

		private bool rowNumbers = false;

		private int pageNo = 1;

		private string tableID = "";

		private string openRows = "";

		private string detail = "";

		private Dictionary dict = null;

		private TableColumn[] columns = new TableColumn[0];

		private string indent = "\t\t\t\t";

		private bool isAjax = false;

		private bool debug = false;

		public Advocar.Web.Align Align
		{
			get
			{
				return this.align;
			}
			set
			{
				this.align = value;
			}
		}

		public bool Debug
		{
			get
			{
				return this.debug;
			}
			set
			{
				this.debug = value;
			}
		}

		public string Detail
		{
			get
			{
				return this.detail;
			}
			set
			{
				this.detail = value;
			}
		}

		public string Indent
		{
			get
			{
				return this.indent;
			}
			set
			{
				this.indent = value;
			}
		}

		public bool IsAjax
		{
			get
			{
				return this.isAjax;
			}
			set
			{
				this.isAjax = value;
			}
		}

		public string KeyField
		{
			get
			{
				return this.keyField;
			}
		}

		public string OpenRows
		{
			get
			{
				return this.openRows;
			}
		}

		public int PageNo
		{
			get
			{
				return this.pageNo;
			}
		}

		public bool RowNumbers
		{
			get
			{
				return this.rowNumbers;
			}
		}

		public int Rows
		{
			get
			{
				return this.rows;
			}
			set
			{
				this.rows = value;
			}
		}

		public string SortBy
		{
			get
			{
				return this.sortBy;
			}
		}

		public Advocar.Web.Sorting Sorting
		{
			get
			{
				return this.sorting;
			}
			set
			{
				this.sorting = value;
			}
		}

		public string TableID
		{
			get
			{
				return this.tableID;
			}
			set
			{
				this.tableID = value;
			}
		}

		public string Title
		{
			get
			{
				return this.title;
			}
			set
			{
				this.title = value;
			}
		}

		public int Width
		{
			get
			{
				return this.width;
			}
			set
			{
				this.width = value;
			}
		}

		public Table()
		{
		}

		public Table(string connection, TableSettings settings, Dictionary dict) : base(connection)
		{
			this.rows = settings.Rows;
			this.width = settings.Width;
			this.align = settings.Align;
			this.sorting = settings.Sorting;
			this.rowNumbers = settings.RowNumbers;
			this.tableID = settings.TableId;
			this.Title = settings.Title;
			this.initialize(settings.KeyField, settings.Title, dict);
			this.columns = settings.Columns;
			this.Parameters = settings.Parameters;
			this.IsAjax = true;
		}

		public Table(string tableId, string connection, string keyField, string title, Dictionary dict) : base(connection)
		{
			this.TableID = tableId;
			this.initialize(keyField, title, dict);
		}

		public Table(string tableId, string server, string database, string user, string password, string keyField, string title, Dictionary dict) : base(server, database, user, password)
		{
			this.TableID = tableId;
			this.initialize(keyField, title, dict);
		}

		public Table(string tableId, string connection, string keyField, string title, Dictionary dict, int rows, int width, Advocar.Web.Align align, Advocar.Web.Sorting sorting, bool rowNumbers) : base(connection)
		{
			this.rows = rows;
			this.width = width;
			this.align = align;
			this.sorting = sorting;
			this.rowNumbers = rowNumbers;
			this.tableID = tableId;
			this.initialize(keyField, title, dict);
		}

		public void AddButton(string buttonName, string command, int width)
		{
			this.AddButton(buttonName, command, width, Conditional.none, "", "");
		}

		public void AddButton(string buttonName, string command, int width, Conditional conditional, string conditionalField, string conditionalValue)
		{
			int length = (int)this.columns.Length + 1;
			TableColumn[] array = new TableColumn[length];
			this.columns.CopyTo(array, 0);
			this.columns = new TableColumn[length];
			array.CopyTo(this.columns, 0);
			TableColumn column = new TableColumn(command, buttonName, width, "", Advocar.Web.Align.center, ColumnType.button, conditional, conditionalField, conditionalValue);
			this.columns.SetValue(column, length - 1);
		}

		public void AddColumn(string fieldName, string title, int width, Advocar.Web.Align alignment)
		{
			this.AddColumn(fieldName, title, width, "", alignment, ColumnType.label, Conditional.none, "", "");
		}

		public void AddColumn(string fieldName, string title, int width, Advocar.Web.Align alignment, ColumnType columnType)
		{
			this.AddColumn(fieldName, title, width, "", alignment, columnType, Conditional.none, "", "");
		}

		public void AddColumn(string fieldName, string title, int width, string command, Advocar.Web.Align alignment, ColumnType columnType, Conditional conditional, string conditionalField, string conditionalValue)
		{
			int length = (int)this.columns.Length + 1;
			TableColumn[] array = new TableColumn[length];
			this.columns.CopyTo(array, 0);
			this.columns = new TableColumn[length];
			array.CopyTo(this.columns, 0);
			TableColumn column = new TableColumn(fieldName, title, width, command, alignment, columnType, conditional, conditionalField, conditionalValue);
			this.columns.SetValue(column, length - 1);
		}

		private void addSorting(string sortby, string sortbys)
		{
			if (Validation.IsNull(sortby))
			{
				sortby = "";
			}
			if (Validation.IsNull(sortbys))
			{
				sortbys = "";
			}
			if (this.sorting == Advocar.Web.Sorting.none)
			{
				this.sortBy = "";
			}
			else if (this.sorting == Advocar.Web.Sorting.normal)
			{
				string previousSort = sortbys.Replace(" DESC", "").Trim().ToLower();
				string currentSort = sortby.Trim().ToLower();
				if ((currentSort == previousSort) & sortby.Length > 0)
				{
					this.sortBy = (sortbys.IndexOf("DESC") > 0 ? sortbys.Replace("DESC", "").Trim() : string.Concat(sortby, " DESC"));
				}
				else if (currentSort.Length != 0)
				{
					this.sortBy = sortby;
				}
				else
				{
					this.sortBy = sortbys;
				}
			}
		}

		private void initialize(string keyField, string title, Dictionary dict)
		{
			if (Validation.IsNull(dict[string.Concat("tableItemOpen", this.tableID)]))
			{
				dict[string.Concat("tableItemOpen", this.tableID)] = "";
			}
			if (Validation.IsNull(dict[string.Concat("tableItemClose", this.tableID)]))
			{
				dict[string.Concat("tableItemClose", this.tableID)] = "";
			}
			if (Validation.IsNull(dict[string.Concat("tableOpenItems", this.tableID)]))
			{
				dict[string.Concat("tableOpenItems", this.tableID)] = "";
			}
			if (Validation.IsNull(dict[string.Concat("tablePageNo", this.tableID)]))
			{
				dict[string.Concat("tablePageNo", this.tableID)] = "";
			}
			this.keyField = keyField;
			this.title = title;
			this.dict = dict;
			string sortField = (Validation.IsNull(dict[string.Concat("tableSortBy", this.tableID)]) ? "" : dict[string.Concat("tableSortBy", this.tableID)]);
			if (sortField.IndexOf("{") >= 0)
			{
				int start = sortField.IndexOf("{");
				int end = sortField.IndexOf("}");
				sortField = sortField.Substring(start + 1, end - start - 1);
				end = sortField.IndexOf("(");
				if (end >= 0)
				{
					sortField = sortField.Remove(end, sortField.Length - end);
				}
			}
			this.addSorting(sortField, dict[string.Concat("tableSortBys", this.tableID)]);
			this.pageNo = (dict[string.Concat("tablePageNo", this.tableID)].Length > 0 ? int.Parse(dict[string.Concat("tablePageNo", this.tableID)]) : 1);
			string openRow = dict[string.Concat("tableItemOpen", this.tableID)];
			this.openRows = this.openRows.Replace(string.Concat("#", dict[string.Concat("tableItemOpen", this.tableID)].Trim(), "#"), "");
			this.openRows = string.Concat(dict[string.Concat("tableOpenItems", this.tableID)], (dict[string.Concat("tableItemOpen", this.tableID)].Length > 0 ? string.Concat("#", dict[string.Concat("tableItemOpen", this.tableID)].Trim(), "#") : ""));
			string closeRow = dict[string.Concat("tableItemClose", this.tableID)];
			this.openRows = (closeRow.Length == 0 ? this.OpenRows : this.openRows.Replace(string.Concat("#", closeRow, "#"), ""));
		}

		private string renderBody()
		{
			int col;
			string[] item;
			string body = "";
			int columnCount = (int)this.columns.Length + (this.rowNumbers ? 1 : 0);
			if (base.RecordCount == 0)
			{
				body = string.Concat("<tr class=\"tableRowOdd\"><td colspan=\"", columnCount.ToString(), "\" align=\"center\" class=\"tableBodyFont\">No Records Returned</td></tr>\n");
			}
			int rowStart = (this.rows == 0 ? 1 : this.pageNo * this.rows - this.rows + 1);
			if (rowStart > base.RecordCount)
			{
				rowStart = 1;
				this.pageNo = 1;
			}
			if (rowStart < 1)
			{
				rowStart = 1;
				this.pageNo = 1;
			}
			if (rowStart > 1)
			{
				base.MoveNext(rowStart - 1);
			}
			int rowEnd = (this.rows == 0 ? base.RecordCount : this.pageNo * this.rows);
			if (rowEnd > base.RecordCount)
			{
				rowEnd = base.RecordCount;
			}
			string tablerow = "";
			string cell = "";
			bool isRedFlagdAvailable = (base.FieldID("IsRedFlag") >= 0 ? true : false);
			string redFlag = "";
			string tablerowstyle = "";
			int row = rowStart;
			while (rowEnd >= row)
			{
				if (!base.EOF)
				{
					tablerow = "";
					cell = "";
					redFlag = "";
					if (isRedFlagdAvailable)
					{
						redFlag = (base["IsRedFlag"] == "True" ? "Red" : "");
					}
					tablerowstyle = (row % 2 == 0 ? "tableRowEven" : "tableRowOdd");
					item = new string[] { this.indent, "<tr id = \"Row", base[this.keyField], "\" class=\"", tablerowstyle, redFlag, "\" onmouseover=\"this.className='tableRowHighlight", redFlag, "';\" onmouseout=\"this.className='", tablerowstyle, redFlag, "';\">\n" };
					tablerow = string.Concat(item);
					tablerow = string.Concat(tablerow, cell);
					if (this.rowNumbers)
					{
						item = new string[] { tablerow, this.indent, "\t<td align=\"right\" class=\"tableBodyFont\">", row.ToString(), ")</td>\n" };
						tablerow = string.Concat(item);
					}
					for (col = 0; (int)this.columns.Length > col; col++)
					{
						string conditional = "";
						bool display = true;
						string command = "";
						if (this.columns[col].Conditional == Conditional.enable)
						{
							conditional = (this.columns[col].ConditionalValue == base.AssignValue(this.columns[col].ConditionalField) ? "" : " disabled");
						}
						if (this.columns[col].Conditional == Conditional.display)
						{
							display = (this.columns[col].ConditionalValue == base.AssignValue(this.columns[col].ConditionalField) ? true : false);
						}
						Input inpt = new Input();
						switch (this.columns[col].ColumnType)
						{
							case ColumnType.label:
							{
								cell = base.AssignValue(this.columns[col].FieldProcess);
								break;
							}
							case ColumnType.button:
							{
								string action = base.AssignValue(this.columns[col].FieldProcess);
								command = string.Concat(base.AssignValue(this.columns[col].Command), conditional);
								Button button = new Button(this.columns[col].Title, action, this.columns[col].Width, command)
								{
									Stylesheet = "tableHeadingButton"
								};
								cell = button.ToString();
								break;
							}
							case ColumnType.checkbox:
							{
								command = string.Concat(base.AssignValue(this.columns[col].Command), conditional);
								Checkbox checkbox = new Checkbox(string.Concat(this.columns[col].FieldName, base[this.keyField]), "", "True", base.AssignValue(this.columns[col].FieldName), command)
								{
									CheckboxLabel = ""
								};
								cell = checkbox.ToString();
								break;
							}
							case ColumnType.textbox:
							{
								command = string.Concat(base.AssignValue(this.columns[col].Command), conditional);
								Text text = new Text(string.Concat(this.columns[col].FieldName, base[this.keyField]), base.AssignValue(this.columns[col].FieldProcess), this.columns[col].Width, this.columns[col].MaxLength, command);
								cell = text.ToString();
								break;
							}
						}
						item = new string[] { tablerow, this.indent, "\t<td valign=\"top\" class=\"tableBodyFont\" align=\"", this.columns[col].Alignment.ToString(), "\">", null, null };
						item[5] = (display ? cell : "");
						item[6] = "\n";
						tablerow = string.Concat(item);
					}
					string detail = "";
					if (this.detail.Length > 0)
					{
						object[] objArray = new object[] { "\n", this.indent, "<tr class=\"tableRowDetail\" style=\"display: inline-block;\" id=expRow", base[this.keyField], ">\n", this.indent, "\t<td class=\"tableRowDetail\" class=\"tableBodyFont\" colspan=\"", columnCount, "\">\n", this.indent, "\t\t", base.AssignValue(this.detail), "\n", this.indent, "\t</td>\n", this.indent, "</tr>" };
						detail = string.Concat(objArray);
					}
					item = new string[] { body, tablerow, this.indent, "</tr>", detail, "\n" };
					body = string.Concat(item);
					base.MoveNext();
					row++;
				}
				else
				{
					break;
				}
			}
			int widthAdjust = this.width;
			string rowheader = "";
			if (this.rowNumbers)
			{
				rowheader = string.Concat(rowheader, this.indent, "\t<td width=\"30\" align=\"right\"></td>\n");
				widthAdjust = widthAdjust - 30;
			}
			widthAdjust = widthAdjust - columnCount * 4;
			int widthLeft = widthAdjust;
			int width = 0;
			col = 0;
			while ((int)this.columns.Length > col)
			{
				if (widthLeft != 0)
				{
					if (widthLeft - this.columns[col].Width >= 0)
					{
						float newwidth = (float)widthAdjust * ((float)this.columns[col].Width / (float)widthAdjust);
						width = (int)newwidth;
						widthLeft = widthLeft - (int)newwidth;
					}
					else
					{
						width = widthLeft;
						widthLeft = 0;
					}
					item = new string[] { rowheader, this.indent, "\t<td width=\"", width.ToString(), "\" align=\"", this.columns[col].Alignment.ToString(), "\"></td>\n" };
					rowheader = string.Concat(item);
					col++;
				}
				else
				{
					break;
				}
			}
			rowheader = string.Concat(rowheader, this.indent, "</tr>\n");
			item = new string[] { "\n", this.indent, "<!--Table: Body for Table ID ", this.tableID, "-->\n", this.indent, "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" width=\"", this.width.ToString(), "\" style=\"table-layout:fixed;\" align=\"", null, null, null, null, null, null };
			item[9] = (this.align == Advocar.Web.Align.left ? "" : this.align.ToString());
			item[10] = "\">\n";
			item[11] = rowheader;
			item[12] = body;
			item[13] = this.indent;
			item[14] = "</table>\n";
			string str = string.Concat(item);
			body = str;
			return str;
		}

		public string renderFooter()
		{
			object[] objArray = new object[] { "\n", this.indent, "<!--Table: Footer for Table ID ", this.tableID, "-->\n", this.indent, "<table width=\"", this.width, "\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\"", null, null, null, null, null, null, null, null, null, null };
			objArray[9] = (this.align == Advocar.Web.Align.center ? " align = \"center\"" : "");
			objArray[10] = ">\n";
			objArray[11] = this.indent;
			objArray[12] = "<tr class=\"tableTitle\">\n";
			objArray[13] = this.indent;
			objArray[14] = "\t<td class=\"tableFooter\" align=\"left\" height=\"2\"></td>\n";
			objArray[15] = this.indent;
			objArray[16] = "</tr>\n";
			objArray[17] = this.indent;
			objArray[18] = "</table>";
			return string.Concat(objArray);
		}

		private string renderHeader()
		{
			string[] str;
			string str1;
			int rowStart = (this.rows == 0 ? 1 : this.pageNo * this.rows - this.rows + 1);
			if (rowStart > base.RecordCount)
			{
				rowStart = 1;
				this.pageNo = 1;
			}
			if (rowStart < 1)
			{
				rowStart = 1;
				this.pageNo = 1;
			}
			int rowEnd = (this.rows == 0 ? base.RecordCount : this.pageNo * this.rows);
			if (rowEnd > base.RecordCount)
			{
				rowEnd = base.RecordCount;
			}
			object[] objArray = new object[] { "\n", this.indent, "<!--Table: Heading for Table ID ", this.tableID, "-->\n", this.indent, "<table width=\"", this.width, "\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\"", null, null, null, null, null, null, null, null };
			objArray[9] = (this.align == Advocar.Web.Align.center ? " align = \"center\"" : "");
			objArray[10] = ">\n";
			objArray[11] = this.indent;
			objArray[12] = "<tr class=\"tableTitle\">\n";
			objArray[13] = this.indent;
			objArray[14] = "\t<td class=\"tableTitle\" align=\"left\">";
			objArray[15] = this.title;
			objArray[16] = "</td>";
			string navigation = string.Concat(objArray);
			if (this.rows > 0)
			{
				str = new string[20];
				str[0] = navigation;
				str[1] = "\n";
				str[2] = this.indent;
				str[3] = "\t<td align=\"right\" width=\"100\"><span class=\"tableTitle\" style=\"font-style:normal; font-weight:bold; font-size:7pt;\">";
				string[] strArrays = str;
				if (rowEnd == 0)
				{
					str1 = "";
				}
				else
				{
					string[] strArrays1 = new string[] { rowStart.ToString(), " - ", rowEnd.ToString(), " / ", null };
					strArrays1[4] = base.RecordCount.ToString();
					str1 = string.Concat(strArrays1);
				}
				strArrays[4] = str1;
				str[5] = "</span></td>\n";
				str[6] = this.indent;
				str[7] = "\t<td align=\"right\" width=\"50\" style=\"font-size: 7pt;\"><input class=\"tableHeadingButton\" type=\"button\" value=\"<\" onclick=\"tablePagePrevious";
				str[8] = this.tableID;
				str[9] = "();\" ";
				str[10] = (this.pageNo <= 1 ? "disabled=\"true\"" : "");
				str[11] = " style=\"width:15px;\">";
				str[12] = this.indent;
				str[13] = "<input class=\"tableHeadingButton\" type=\"button\" value=\">\" onclick=\"tablePageNext";
				str[14] = this.tableID;
				str[15] = "();\" ";
				str[16] = (rowEnd == base.RecordCount ? "disabled=\"true\"" : "");
				str[17] = " style=\"width:15px;\">\n";
				str[18] = this.indent;
				str[19] = "\t</td>";
				navigation = string.Concat(str);
			}
			str = new string[] { navigation, "\n", this.indent, "</tr>\n", this.indent, "</table>\n\n" };
			navigation = string.Concat(str);
			Input inpt = new Input();
			str = new string[] { "\n", this.indent, "<!--Table: Navigation Control Values for Table ", this.tableID, "-->\n", this.indent, inpt.Hidden(string.Concat("tableSortBy", this.tableID), ""), "\n", this.indent, inpt.Hidden(string.Concat("tableSortBys", this.tableID), this.sortBy), "\n", this.indent, inpt.Hidden(string.Concat("tablePageNo", this.tableID), this.pageNo.ToString()), "\n", this.indent, inpt.Hidden(string.Concat("tableItemOpen", this.tableID), ""), "\n", this.indent, inpt.Hidden(string.Concat("tableItemClose", this.tableID), ""), "\n", this.indent, inpt.Hidden(string.Concat("tableOpenItems", this.tableID), this.openRows), "\n" };
			string navigationControl = string.Concat(str);
			str = new string[] { "myAjaxTable = Advocar_Ajax_Start(); myAjaxTable.onreadystatechange = Advocar_Ajax_Action_TableResult", this.tableID, "; var url = document.URL; if (url.indexOf('?') >= 0){url = url + '&ajax=WebTableResult&WebTableSettingsId=", this.tableID, "';} else {url = url + '?ajax=WebTableResult&WebTableSettingsId=", this.tableID, "';}\n\n", null, null, null, null, null, null };
			str[7] = (this.debug ? "alert('url=' + url);\n" : "");
			str[8] = "myAjaxTable.open('POST', url, true); ";
			str[9] = (this.debug ? "alert('Post Passed!' + 'Url: ' + url); " : " ");
			str[10] = "myAjaxTable.send(Advocar_Ajax_GetFormValues()); ";
			str[11] = (this.debug ? "alert('Send Passed! ' + Advocar_Ajax_GetFormValues()); " : " ");
			str[12] = "document.body.style.cursor = 'wait';";
			string ajax = string.Concat(str);
			str = new string[] { navigationControl, "\n", this.indent, "<!--Table: Navigation Control Functions for Table ", this.tableID, "-->\n", this.indent, "<script language=\"javascript\">\n", this.indent, "function tablePageNext", this.tableID, "(){document.form.tablePageNo", this.tableID, ".value = eval(parseInt(document.form.tablePageNo", this.tableID, ".value) + 1); ", ajax, "}\n" };
			navigationControl = string.Concat(str);
			str = new string[] { navigationControl, this.indent, "function tablePagePrevious", this.tableID, "(){document.form.tablePageNo", this.tableID, ".value = eval(parseInt(document.form.tablePageNo", this.tableID, ".value) - 1); ", ajax, "}\n" };
			navigationControl = string.Concat(str);
			if (this.Sorting != Advocar.Web.Sorting.normal)
			{
				str = new string[] { navigationControl, this.indent, "function tableColumnSort", this.tableID, "(columnName){}\n" };
				navigationControl = string.Concat(str);
			}
			else
			{
				str = new string[] { navigationControl, this.indent, "function tableColumnSort", this.tableID, "(columnName){document.form.tableSortBy", this.tableID, ".value = columnName; ", ajax, "}\n" };
				navigationControl = string.Concat(str);
			}
			str = new string[] { navigationControl, this.indent, "function commandRowClose", this.tableID, "(keyID){document.form.tableItemClose", this.tableID, ".value = keyID; document.getElementById('expRow' + keyID).style.display = 'none'; ", ajax, "}\n" };
			navigationControl = string.Concat(str);
			str = new string[] { navigationControl, this.indent, "function commandRowOpen", this.tableID, "(keyID){document.form.tableItemOpen", this.tableID, ".value = keyID; document.getElementById('expRow' + keyID).style.display = 'inline-block'; document.getElementById('Row' + keyID).className = 'tableRowOpen'; ", ajax, "}\n" };
			navigationControl = string.Concat(str);
			navigationControl = string.Concat(navigationControl, "\n", this.indent, "</script>\n");
			str = new string[] { navigationControl, "\n", navigation, "\n", this.indent, "<!--Table: Table Heading ", this.tableID, "-->\n", this.indent, "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" width=\"", this.width.ToString(), "\" style=\"table-layout:fixed;\" align=\"", null };
			str[12] = (this.align == Advocar.Web.Align.left ? "" : this.align.ToString());
			string heading = string.Concat(str);
			heading = string.Concat(heading, "\">\n", this.indent, "<tr>\n");
			int widthAdjust = this.width;
			int columnCount = (int)this.columns.Length;
			if (this.rowNumbers)
			{
				heading = string.Concat(heading, this.indent, "\t<td width=\"30\" align=\"right\" class=\"tableHeading\"></td>\n");
				widthAdjust = widthAdjust - 30;
				columnCount++;
			}
			widthAdjust = widthAdjust - columnCount * 4;
			int widthLeft = widthAdjust;
			int width = 0;
			int col = 0;
			while ((int)this.columns.Length > col)
			{
				if (widthLeft != 0)
				{
					if (widthLeft - this.columns[col].Width >= 0)
					{
						float newwidth = (float)widthAdjust * ((float)this.columns[col].Width / (float)widthAdjust);
						width = (int)newwidth;
						widthLeft = widthLeft - (int)newwidth;
					}
					else
					{
						width = widthLeft;
						widthLeft = 0;
					}
					switch (this.columns[col].ColumnType)
					{
						case ColumnType.label:
						{
							str = new string[] { heading, this.indent, "\t<td width=\"", width.ToString(), "\" style=\"cursor:hand;\"  class=\"tableHeading\" onclick=\"tableColumnSort", this.tableID, "('", this.columns[col].FieldName, "');\" onmouseover=\"this.className='tableHeadingHighlight'; this.style.cursor='hand';\" onmouseout=\"this.className='tableHeading';\" align=\"", this.columns[col].Alignment.ToString(), "\">", this.columns[col].Title, "</td>\n" };
							heading = string.Concat(str);
							break;
						}
						case ColumnType.button:
						{
							str = new string[] { heading, this.indent, "\t<td width=\"", width.ToString(), "\" align=\"", this.columns[col].Alignment.ToString(), "\" class=\"tableHeading\"></td>\n" };
							heading = string.Concat(str);
							break;
						}
						default:
						{
							str = new string[] { heading, this.indent, "\t<td width=\"", width.ToString(), "\" align=\"", this.columns[col].Alignment.ToString(), "\" class=\"tableHeading\">", this.columns[col].Title, "</td>\n" };
							heading = string.Concat(str);
							break;
						}
					}
					col++;
				}
				else
				{
					break;
				}
			}
			str = new string[] { heading, this.indent, "</tr>\n", this.indent, "</table>\n" };
			return string.Concat(str);
		}

		public string ToString(string procedure)
		{
			string[] tableID;
			if (this.sorting != Advocar.Web.Sorting.none)
			{
				base.AddParam("@OrderBy", this.sortBy);
			}
			if (procedure.IndexOf("SELECT") <= 0)
			{
				base.ExecuteProcedure(procedure, false);
			}
			else
			{
				base.ExecuteStatement(procedure);
			}
			TableSettings settings = new TableSettings()
			{
				Parameters = this.Parameters,
				Columns = this.columns,
				ProcedureName = base.Procedure,
				KeyField = this.keyField,
				Title = this.Title,
				Rows = this.rows,
				Width = this.width,
				Align = this.Align,
				Sorting = this.Sorting,
				RowNumbers = this.rowNumbers,
				TableId = this.TableID,
				Connection = base.ConnectionString
			};
			string script = "";
			if (!this.IsAjax)
			{
				tableID = new string[] { "<script language=\"javascript\">var myAjaxTable; function Advocar_Ajax_Action_TableResult", this.tableID, "(){if (myAjaxTable.readyState == 4 && myAjaxTable.status == 200){", null, null, null, null, null, null };
				tableID[3] = (this.debug ? "alert('Response' + myAjaxTable.responseText); " : " ");
				tableID[4] = "var html = myAjaxTable.responseText; document.getElementById('WebTable";
				tableID[5] = this.tableID;
				tableID[6] = "').innerHTML = html;";
				tableID[7] = (this.debug ? "alert('Web Table Reloaded!'); " : " ");
				tableID[8] = "myAjaxTable = null; document.body.style.cursor = 'auto';}}</script>";
				script = string.Concat(tableID);
			}
			string tableSettings = settings.Serialize();
			tableSettings = tableSettings.Replace("<?xml version=\"1.0\" encoding=\"us-ascii\"?>", "");
			tableSettings = tableSettings.Replace(" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", "");
			tableSettings = Encryption.Encrypt(tableSettings);
			tableID = new string[] { "<textarea id=\"WebTableSettings", this.TableID, "\" name=\"WebTableSettings", this.TableID, "\" style=\"display: none;\" maxlength=\"10000\">", tableSettings, "</textarea>" };
			tableSettings = string.Concat(tableID);
			string tableResult = "";
			if (!this.IsAjax)
			{
				tableID = new string[] { script, "<div id=\"WebTable", this.TableID, "\" style=\"margin: 0px;\" name=\"WebTable", this.tableID, "\">" };
				tableResult = string.Concat(tableID);
			}
			tableID = new string[] { tableResult, this.renderHeader(), this.renderBody(), this.renderFooter(), tableSettings };
			tableResult = string.Concat(tableID);
			if (!this.IsAjax)
			{
				tableResult = string.Concat(tableResult, "</div>\n");
			}
			return tableResult;
		}
	}
}