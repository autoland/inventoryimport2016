using System;

namespace Advocar.Web
{
	public class TableColumn
	{
		private string fieldName = "";

		private string fieldProcess = "";

		private string title = "";

		private int width = 0;

		private string command = "";

		private Align alignment = Align.left;

		private Advocar.Web.ColumnType columnType = Advocar.Web.ColumnType.label;

		private Advocar.Web.Conditional conditional = Advocar.Web.Conditional.none;

		private string conditionalField = "";

		private string conditionalValue = "";

		private int maxLength = 0;

		public Align Alignment
		{
			get
			{
				return this.alignment;
			}
			set
			{
				this.alignment = value;
			}
		}

		public Advocar.Web.ColumnType ColumnType
		{
			get
			{
				return this.columnType;
			}
			set
			{
				this.columnType = value;
			}
		}

		public string Command
		{
			get
			{
				return this.command;
			}
			set
			{
				this.command = value;
			}
		}

		public Advocar.Web.Conditional Conditional
		{
			get
			{
				return this.conditional;
			}
			set
			{
				this.conditional = value;
			}
		}

		public string ConditionalField
		{
			get
			{
				return this.conditionalField;
			}
			set
			{
				this.conditionalField = value;
			}
		}

		public string ConditionalValue
		{
			get
			{
				return this.conditionalValue;
			}
			set
			{
				this.conditionalValue = value;
			}
		}

		public string FieldName
		{
			get
			{
				return this.fieldName;
			}
			set
			{
				this.fieldName = value;
			}
		}

		public string FieldProcess
		{
			get
			{
				return this.fieldProcess;
			}
			set
			{
				this.fieldProcess = value;
			}
		}

		public int MaxLength
		{
			get
			{
				return this.maxLength;
			}
			set
			{
				this.maxLength = value;
			}
		}

		public string Title
		{
			get
			{
				return this.title;
			}
			set
			{
				this.title = value;
			}
		}

		public int Width
		{
			get
			{
				return this.width;
			}
			set
			{
				this.width = value;
			}
		}

		public TableColumn()
		{
		}

		public TableColumn(string fieldName, string title, int width, string command, Align alignment, Advocar.Web.ColumnType columnType, Advocar.Web.Conditional conditional, string conditionalField, string conditionalValue)
		{
			this.initialize(fieldName, title, width, command, alignment, columnType, conditional, conditionalField, conditionalValue, 0);
		}

		public TableColumn(string fieldName, string title, int width, string command, Align alignment, Advocar.Web.ColumnType columnType, Advocar.Web.Conditional conditional, string conditionalField, string conditionalValue, int maxLength)
		{
			this.initialize(fieldName, title, width, command, alignment, columnType, conditional, conditionalField, conditionalValue, maxLength);
		}

		private void initialize(string fieldName, string title, int width, string command, Align alignment, Advocar.Web.ColumnType columnType, Advocar.Web.Conditional conditional, string conditionalField, string conditionalValue, int maxLength)
		{
			if (fieldName.IndexOf("{") <= 0)
			{
				this.fieldName = fieldName;
			}
			else
			{
				int start = fieldName.IndexOf("{");
				int end = fieldName.IndexOf("}");
				if ((end <= 0 || start <= 0 ? false : end > start))
				{
					this.fieldName = fieldName.Substring(start, end - start + 1);
				}
				this.fieldName = this.fieldName.Replace("{", "").Replace("}", "");
				start = this.fieldName.IndexOf("(");
				if (start > 0)
				{
					this.fieldName = this.fieldName.Remove(start, this.fieldName.Length - start);
				}
			}
			this.maxLength = maxLength;
			this.fieldProcess = fieldName;
			this.title = title;
			this.width = width;
			this.command = command;
			this.alignment = alignment;
			this.columnType = columnType;
			this.conditional = conditional;
			this.conditionalField = conditionalField;
			this.conditionalValue = conditionalValue;
		}
	}
}