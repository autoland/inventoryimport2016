using System;

namespace Advocar.Web
{
	public enum ColumnType
	{
		label,
		button,
		checkbox,
		textbox,
		combobox
	}
}