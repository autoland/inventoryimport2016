using System;

namespace Advocar.Web
{
	public enum SortOrder
	{
		@ascending,
		@descending
	}
}