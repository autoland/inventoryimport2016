using Advocar.Interface;
using Advocar.Tools;
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Web;
using System.Web.UI;

namespace Advocar.Web
{
	public class Page : System.Web.UI.Page
	{
		public Input input = new Input();

		public Dictionary dict = null;

		private string website = "";

		private bool isSecure = false;

		private string serverName = Environment.MachineName;

		private string pageTitle = "";

		private int contentWidth = 730;

		private string http = "";

		private bool isHttps = false;

		public int ContentWidth
		{
			get
			{
				return this.contentWidth;
			}
			set
			{
				this.contentWidth = value;
			}
		}

		public string Http
		{
			get
			{
				return this.http;
			}
			set
			{
				this.http = value;
			}
		}

		public bool IsHttps
		{
			get
			{
				return this.isHttps;
			}
			set
			{
				this.isHttps = value;
			}
		}

		public bool IsSecure
		{
			get
			{
				return this.isSecure;
			}
			set
			{
				this.isSecure = value;
			}
		}

		public string PageTitle
		{
			get
			{
				return this.pageTitle;
			}
			set
			{
				this.pageTitle = value;
			}
		}

		public string ServerName
		{
			get
			{
				return this.serverName;
			}
			set
			{
				this.serverName = value;
			}
		}

		public string Website
		{
			get
			{
				return this.website;
			}
			set
			{
				this.website = value;
			}
		}

		public Page()
		{
		}

		public string GetCookieValue(string website, string field)
		{
			string str;
			bool found = false;
			foreach (string cookieDomain in base.Request.Cookies.Keys)
			{
				if (cookieDomain == string.Concat(website, field))
				{
					found = true;
					break;
				}
			}
			str = (found ? base.Request.Cookies[string.Concat(website, field)].Value : "");
			return str;
		}

		public bool IsTimestampValid(ref Dictionary dict)
		{
			bool flag;
			flag = (!(dict["TimeStamp"] == base.Request.Cookies["SessionAimeCom2.0"].Values["Timestamp"]) ? false : true);
			return flag;
		}

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			this.ProcessAjax();
		}

		protected override void OnPreInit(EventArgs e)
		{
			base.OnPreInit(e);
			this.dict = new Dictionary(base.Request);
			this.Website = base.Request.Url.AbsoluteUri.Replace(base.Request.Url.AbsolutePath, "");
			if (base.Request.Url.Query.Length > 0)
			{
				this.Website = this.Website.Replace(base.Request.Url.Query, "");
			}
			string machine = Environment.MachineName.ToUpper();
			if ((machine == "DEVTEAM01" || machine == "STGWEB01" || machine == "WEB01" ? false : !(machine == "WEB02")))
			{
				for (int index = 0; 2 > index; index++)
				{
					this.Website = string.Concat(this.Website, base.Request.Url.Segments[index]);
				}
			}
			else
			{
				this.Website = string.Concat(this.Website, "/");
			}
		}

		protected override void OnUnload(EventArgs e)
		{
			base.OnUnload(e);
			GC.Collect();
		}

		public virtual void ProcessAjax()
		{
			string item = this.dict["ajax"];
			if (item != null)
			{
				if (item != "WebTableResult")
				{
					return;
				}
				string webTableSettings = this.dict[string.Concat("WebTableSettings", this.dict["WebTableSettingsId"])];
				webTableSettings = Encryption.Decrypt(webTableSettings);
				TableSettings settings = new TableSettings();
				settings = (TableSettings)settings.Deserialize(webTableSettings);
				Table table = new Table(settings.Connection, settings, this.dict);
				base.Response.Write(table.ToString(settings.ProcedureName));
				base.Response.End();
			}
		}

		public void SetCookieValue(string website, string field, string value)
		{
			base.Response.Cookies[string.Concat(website, field)].Expires = DateTime.Now.AddDays(7);
			base.Response.Cookies[string.Concat(website, field)].Value = value;
		}

		public void SetTimetamp()
		{
			Input input = new Input();
			string timestamp = DateTime.Now.ToString();
			base.Response.Cookies["SessionAimeCom2.0"].Values["Timestamp"] = timestamp;
			base.Response.Write(input.Hidden("TimeStamp", timestamp));
		}
	}
}