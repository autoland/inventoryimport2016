using System;
using System.Collections.Specialized;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Advocar.Web
{
	public class Dictionary : NameValueCollection
	{
		private string FieldDelimiter = "#@#";

		private string FieldEqual = "===";

		public new string this[int index]
		{
			get
			{
				return base[index];
			}
		}

		public new string this[string name]
		{
			get
			{
				string str;
				str = (this.Exists(name) ? base[name.ToLower()] : "");
				return str;
			}
			set
			{
				this.Remove(name);
				this.Add(name.ToLower(), value);
			}
		}

		public Dictionary(HttpRequest request)
		{
			int row;
			bool isAjax = false;
			bool isForm = false;
			NameValueCollection fields = request.Form;
			for (row = 0; row <= fields.Count - 1; row++)
			{
				this.Add(fields.Keys[row], fields[row]);
				isForm = true;
			}
			fields = request.QueryString;
			for (row = 0; row <= fields.Count - 1; row++)
			{
				this.Add(fields.Keys[row], fields[row]);
				try
				{
					if ((fields.Keys[row].ToLower() != "ajax" ? false : !isForm))
					{
						isAjax = true;
					}
				}
				catch
				{
				}
			}
			if (isAjax)
			{
				byte[] byteArray = new byte[request.ContentLength];
				byteArray = request.BinaryRead(request.ContentLength);
				string ajaxValuePairs = Encoding.UTF8.GetString(byteArray);
				int ajaxStart = ajaxValuePairs.IndexOf("##VALUEPAIRSTART##");
				int ajaxEnd = ajaxValuePairs.IndexOf("##VALUEPAIREND##");
				if (ajaxStart >= 0)
				{
					ajaxStart = ajaxStart + "##VALUEPAIRSTART##".Length;
				}
				if ((ajaxEnd < ajaxStart ? false : ajaxStart >= 0))
				{
					ajaxValuePairs = ajaxValuePairs.Substring(ajaxStart, ajaxEnd - ajaxStart);
					string[] stringArray = Regex.Split(ajaxValuePairs, this.FieldDelimiter);
					for (row = 0; row <= (int)stringArray.Length - 1; row++)
					{
						if (stringArray[row].Length > 0)
						{
							string[] field = Regex.Split(stringArray[row], this.FieldEqual);
							this.Add(field[0], field[1]);
						}
					}
				}
			}
		}

		public override void Add(string name, string value)
		{
			if ((!this.Exists(name) ? true : value.Length <= 0))
			{
				base.Add(name, value);
			}
			else
			{
				this[name] = value;
			}
		}

		public bool Exists(string name)
		{
			bool found = false;
			for (int row = 0; row < this.Keys.Count; row++)
			{
				try
				{
					if (this.Keys[row].ToLower() == name.ToLower())
					{
						found = true;
						break;
					}
				}
				catch
				{
				}
			}
			return found;
		}

		public string GetListValuePairs()
		{
			string valuepairs = "";
			for (int index = 0; this.Count > index; index++)
			{
				string[] item = new string[] { valuepairs, this.Keys[index], this.FieldEqual, this[index], this.FieldDelimiter };
				valuepairs = string.Concat(item);
			}
			return valuepairs;
		}
	}
}