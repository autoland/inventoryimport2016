using System;

namespace Advocar.Web
{
	public class MenuItem
	{
		private bool selected = false;

		private string code = "";

		public string title = "";

		public string link = "";

		public int width = 0;

		public string codeParent = "";

		public string Code
		{
			get
			{
				return this.code;
			}
		}

		public string CodeParent
		{
			get
			{
				return this.codeParent;
			}
		}

		public string Link
		{
			get
			{
				return this.link;
			}
		}

		public bool Selected
		{
			get
			{
				return this.selected;
			}
			set
			{
				this.selected = value;
			}
		}

		public string Title
		{
			get
			{
				return this.title;
			}
		}

		public int Width
		{
			get
			{
				return this.width;
			}
		}

		public MenuItem(string code, string codeParent, string title, string link, int width, bool selected)
		{
			this.code = code;
			this.codeParent = codeParent;
			this.title = title;
			this.link = link;
			this.width = width;
			this.selected = selected;
		}
	}
}