using System;

namespace Advocar.Web
{
	public class Menu
	{
		private MenuItem[] arrayItem = new MenuItem[0];

		private MenuItem[] arraySub = new MenuItem[0];

		private double multiplier = 0;

		private int additive = 0;

		private string selection = "";

		private string align = "";

		private string indent = "\t\t\t\t";

		private int height = 19;

		public int Additive
		{
			get
			{
				return this.additive;
			}
			set
			{
				this.additive = value;
			}
		}

		public string Align
		{
			get
			{
				return this.align;
			}
			set
			{
				this.align = value;
			}
		}

		public int Height
		{
			get
			{
				return this.height;
			}
			set
			{
				this.height = value;
			}
		}

		public string Indent
		{
			get
			{
				return this.indent;
			}
			set
			{
				this.indent = value;
			}
		}

		public double Multiplier
		{
			get
			{
				return this.multiplier;
			}
			set
			{
				this.multiplier = value;
			}
		}

		public Menu(string codeSelected)
		{
			this.multiplier = 7.5;
			this.additive = 10;
			this.align = "center";
			this.selection = codeSelected;
		}

		public Menu(string codeSelected, int height)
		{
			this.multiplier = 7.5;
			this.additive = 10;
			this.align = "center";
			this.selection = codeSelected;
			this.height = height;
		}

		public bool AddMenuItem(string code, string title, string link)
		{
			bool flag;
			if (!this.SearchMenuItem(code))
			{
				int length = (int)this.arrayItem.Length + 1;
				MenuItem[] array = new MenuItem[length];
				this.arrayItem.CopyTo(array, 0);
				this.arrayItem = new MenuItem[length];
				array.CopyTo(this.arrayItem, 0);
				int width = (title.Length <= 5 ? 50 : (int)((double)title.Length * this.multiplier) + this.additive);
				MenuItem menu = new MenuItem(code, "", title, link, width, (code == this.selection ? true : false));
				this.arrayItem.SetValue(menu, length - 1);
				flag = true;
			}
			else
			{
				flag = false;
			}
			return flag;
		}

		public bool AddMenuItemSub(string codeParent, string code, string title, string link)
		{
			bool flag;
			if (this.SearchMenuItemSub(code, codeParent))
			{
				flag = false;
			}
			else if (this.SearchMenuItem(codeParent))
			{
				int length = (int)this.arraySub.Length + 1;
				MenuItem[] array = new MenuItem[(int)this.arraySub.Length + 1];
				this.arraySub.CopyTo(array, 0);
				this.arraySub = new MenuItem[length];
				array.CopyTo(this.arraySub, 0);
				MenuItem menu = new MenuItem(code, codeParent, title, link, (title.Length <= 5 ? 50 : (int)((double)title.Length * this.multiplier) + this.additive), false);
				this.arraySub.SetValue(menu, length - 1);
				flag = true;
			}
			else
			{
				flag = false;
			}
			return flag;
		}

		public string CreateMenu()
		{
			int row;
			string[] str;
			string mainMenu = "";
			string mainMenuTop = "";
			string mainMenuBottom = "";
			string subMenu = "";
			int width = 0;
			int widthLeft = 0;
			int menuSelected = 0;
			this.SelectSubMenu(this.selection);
			string className = "";
			width++;
			object[] height = new object[] { mainMenu, this.indent, "\t<td width=\"1\" bgcolor=\"white\" height=\"", this.Height, "\"></td>\n" };
			mainMenu = string.Concat(height);
			mainMenuTop = string.Concat(mainMenuTop, this.indent, "\t<td width=\"1\" height=\"3\"></td>\n");
			mainMenuBottom = string.Concat(mainMenuBottom, this.indent, "\t<td height=\"1\" width=\"1\"></td>\n");
			for (row = 0; row <= (int)this.arrayItem.Length - 1; row++)
			{
				width = width + this.arrayItem[row].width;
				className = "menuMain";
				if (!this.arrayItem[row].Selected)
				{
					height = new object[] { mainMenu, this.indent, "\t<td width=\"", this.arrayItem[row].width, "\" onmouseover=\"this.style.cursor='hand'; this.className='menuMainOver';\" onmouseout=\"this.className='", className, "';\" onclick=\"commandGo('", this.arrayItem[row].link, "');\" valign=\"center\" align=\"center\" class=\"", className, "\">", this.arrayItem[row].title, "</td>\n" };
					mainMenu = string.Concat(height);
					height = new object[] { mainMenuTop, this.indent, "\t<td height=\"3\" width=\"", this.arrayItem[row].width, "\"></td>\n" };
					mainMenuTop = string.Concat(height);
					height = new object[] { mainMenuBottom, this.indent, "\t<td height=\"1\" width=\"", this.arrayItem[row].width, "\"></td>\n" };
					mainMenuBottom = string.Concat(height);
				}
				else
				{
					className = "menuMainSelected";
					menuSelected = row;
					height = new object[] { mainMenuTop, this.indent, "\t<td rowspan=\"2\" width=\"", this.arrayItem[row].width, "\" onmouseover=\"this.style.cursor='hand';\" onclick=\"window.location='", this.arrayItem[row].link, "';\" valign=\"center\" align=\"center\" class=\"", className, "\">", this.arrayItem[row].title, "</td>\n" };
					mainMenuTop = string.Concat(height);
					height = new object[] { mainMenuBottom, this.indent, "\t<td class=\"", className, "\" height=\"1\" width=\"", this.arrayItem[row].width, "\"></td>\n" };
					mainMenuBottom = string.Concat(height);
				}
				width++;
				height = new object[] { mainMenu, this.indent, "\t<td width=\"1\" bgcolor=\"white\" height=\"", this.Height, "\"></td>\n" };
				mainMenu = string.Concat(height);
				mainMenuTop = string.Concat(mainMenuTop, this.indent, "\t<td height=\"3\" width=\"1\"></td>\n");
				mainMenuBottom = string.Concat(mainMenuBottom, this.indent, "\t<td height=\"1\" width=\"1\"></td>\n");
			}
			widthLeft = 750 - width;
			height = new object[] { this.indent, "<!--Menu: Main Menu Section-->\n", this.indent, "<script language=\"javascript\">function commandGo(strLink){window.open(strLink, '_top');}</script>\n\n", this.indent, "<table cellpadding=\"0\" cellspacing=\"0\" width=\"750\" border=\"0\"", (this.align == "center" ? " align=\"center\"" : ""), ">\n", this.indent, "<tr>\n", this.indent, "\t<td width=\"5\" height=\"5\"></td>\n", mainMenuTop, this.indent, "\t<td width=\"", widthLeft.ToString(), "\"></td>\n", this.indent, "</tr>\n", this.indent, "<tr class=\"menuMain\">\n", this.indent, "\t<td width=\"5\" class=\"menuMain\" height=\"", this.Height, "\"></td>\n", mainMenu, this.indent, "\t<td width=\"", widthLeft.ToString(), "\"></td>\n", this.indent, "</tr>\n", this.indent, "<tr>\n", this.indent, "\t<td width=\"5\" height=\"1\"></td>\n", mainMenuBottom, this.indent, "\t<td width=\"", widthLeft.ToString(), "\"></td>\n", this.indent, "</tr>\n", this.indent, "</table>\n" };
			mainMenu = string.Concat(height);
			width = 0;
			widthLeft = 0;
			height = new object[] { this.indent, "<!--Menu: Sub Menu Section-->\n", this.indent, "<table cellpadding=\"0\" cellspacing=\"0\" width=\"750\" border=\"0\" class=\"menuSub\"", null, null, null, null, null, null, null, null, null, null };
			height[4] = (this.align == "center" ? " align=\"center\"" : "");
			height[5] = ">\n";
			height[6] = this.indent;
			height[7] = "<tr class=\"menuSub\">\n";
			height[8] = this.indent;
			height[9] = "\t<td width=\"10\" class=\"menuSub\" height=\"";
			height[10] = this.Height + 3;
			height[11] = "\"></td>\n";
			height[12] = this.indent;
			height[13] = "\t<td>\n";
			subMenu = string.Concat(height);
			string subMenuSeparator = "";
			int count = 0;
			for (row = 0; row <= (int)this.arraySub.Length - 1; row++)
			{
				if (this.arraySub[row].codeParent == this.arrayItem[menuSelected].Code)
				{
					widthLeft = 750 - width;
					if (count > 0)
					{
						subMenuSeparator = string.Concat("\n", this.indent, "\t\t<b class=\"menuSub\">&nbsp;|&nbsp;</b>\n");
						width = width + 3;
						widthLeft = widthLeft - 3;
					}
					if (widthLeft < this.arraySub[row].width)
					{
						height = new object[] { subMenu, "\n\t<td width=\"", widthLeft, "\"></td>\n</tr>\n</table>\n<table cellpadding=\"0\" cellspacing=\"0\" width=\"750\" border=\"0\" class=\"menuSub\"", null, null, null, null, null, null, null, null, null, null };
						height[4] = (this.align == "center" ? " align=\"center\"" : "");
						height[5] = ">\n<tr class=\"menuSub\">\n";
						height[6] = this.indent;
						height[7] = "\t\t<td width=\"10\" class=\"menuSub\" height=\"";
						height[8] = this.Height + 3;
						height[9] = "\">\n";
						height[10] = this.indent;
						height[11] = "\t\t</td>\n";
						height[12] = this.indent;
						height[13] = "\t\t<td>";
						subMenu = string.Concat(height);
						subMenuSeparator = "";
						width = 10;
					}
					width = width + this.arraySub[row].width;
					count++;
					className = "menuSub";
					if (this.arraySub[row].Selected)
					{
						className = "menuSubSelected";
					}
					str = new string[] { subMenu, subMenuSeparator, this.indent, "\t\t<a onmouseover=\"this.style.cursor='hand'; this.className='", null, null, null, null, null, null, null, null, null, null };
					str[4] = (this.arraySub[row].Selected ? className : string.Concat(className, "Over"));
					str[5] = "';\" onmouseout=\"this.className='";
					str[6] = className;
					str[7] = "';\" onclick=\"commandGo('";
					str[8] = this.arraySub[row].link;
					str[9] = "');\" class=\"";
					str[10] = className;
					str[11] = "\">";
					str[12] = this.arraySub[row].title;
					str[13] = "</a>";
					subMenu = string.Concat(str);
				}
			}
			subMenu = string.Concat(subMenu, "\n", this.indent, "\t</td>");
			widthLeft = 750 - width;
			str = new string[] { subMenu, "\n", this.indent, "\t<td width=\"", widthLeft.ToString(), "\"></td>\n", this.indent, "</tr>\n", this.indent, "</table>" };
			subMenu = string.Concat(str);
			return string.Concat(mainMenu, "\n", subMenu);
		}

		public bool SearchMenuItem(string code)
		{
			bool flag;
			bool result = false;
			if ((int)this.arrayItem.Length != 0)
			{
				int row = 0;
				while (row <= (int)this.arrayItem.Length - 1)
				{
					if (!(this.arrayItem[row].Code == code))
					{
						row++;
					}
					else
					{
						result = true;
						break;
					}
				}
				flag = result;
			}
			else
			{
				flag = result;
			}
			return flag;
		}

		public bool SearchMenuItemSub(string code, string codeParent)
		{
			bool flag;
			bool result = false;
			if ((int)this.arraySub.Length != 0)
			{
				int row = 0;
				while (row <= (int)this.arraySub.Length - 1)
				{
					if ((this.arraySub[row].Code != code ? true : !(this.arraySub[row].CodeParent == codeParent)))
					{
						row++;
					}
					else
					{
						result = true;
						break;
					}
				}
				flag = result;
			}
			else
			{
				flag = result;
			}
			return flag;
		}

		public void SelectSubMenu(string code)
		{
			int row;
			for (row = 0; row <= (int)this.arrayItem.Length - 1; row++)
			{
				this.arrayItem[row].Selected = false;
			}
			for (row = 0; row <= (int)this.arraySub.Length - 1; row++)
			{
				this.arraySub[row].Selected = false;
			}
			if ((int)this.arraySub.Length != 0)
			{
				int item = 0;
				row = 0;
				while (row <= (int)this.arraySub.Length - 1)
				{
					if (!(this.arraySub[row].Code == code))
					{
						row++;
					}
					else
					{
						this.arraySub[row].Selected = true;
						item = row;
						break;
					}
				}
				string codeParent = this.arraySub[item].codeParent;
				row = 0;
				while (row <= (int)this.arrayItem.Length - 1)
				{
					if (!(this.arrayItem[row].Code == codeParent))
					{
						row++;
					}
					else
					{
						this.arrayItem[row].Selected = true;
						break;
					}
				}
			}
		}
	}
}