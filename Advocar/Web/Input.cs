using Advocar.Data;
using Advocar.Tools;
using Advocar.Web.Controls;
using System;

namespace Advocar.Web
{
	public class Input
	{
		private string indent = "\t\t\t\t";

		public string Indent
		{
			get
			{
				return this.indent;
			}
			set
			{
				this.indent = value;
			}
		}

		public Input()
		{
		}

		public string Button(string key, string action, int width)
		{
			return (new Button(key, action, width)).ToString();
		}

		public string Button(string key, string action, int width, string command, string stylesheet)
		{
			Button button = new Button(key, action, width, command, stylesheet);
			return button.ToString();
		}

		public string Button(string key, string action, int width, string command)
		{
			return (new Button(key, action, width, command)).ToString();
		}

		public string Checkbox(string key, string assignedValue, string checkboxValue, string checkboxLabel)
		{
			return (new Checkbox(key, assignedValue, checkboxValue, checkboxLabel)).ToString();
		}

		public string Checkbox(string key, string assignedValue, string checkboxValue, string checkboxLabel, string command)
		{
			Checkbox checkbox = new Checkbox(key, assignedValue, checkboxValue, checkboxLabel, command);
			return checkbox.ToString();
		}

		public string Checkbox(string key, string assignedValue, string checkboxValue, string checkboxLabel, string command, string stylesheet)
		{
			Checkbox checkbox = new Checkbox(key, assignedValue, checkboxValue, checkboxLabel, command, stylesheet);
			return checkbox.ToString();
		}

		public string Date(string key, string assignedValue)
		{
			return (new Date(key, assignedValue)).ToString();
		}

		public string Date(string key, string assignedValue, string command)
		{
			return (new Date(key, assignedValue, command)).ToString();
		}

		public string Date(string key, string assignedValue, string command, bool required)
		{
			return (new Date(key, assignedValue, command, required)).ToString();
		}

		public string Date(string key, string assignedValue, string command, bool required, string stylesheet)
		{
			Date datetime = new Date(key, assignedValue, command, required, stylesheet);
			return datetime.ToString();
		}

		public string File(string key, string assignedValue, int width)
		{
			return (new File(key, assignedValue, width)).ToString();
		}

		public string File(string key, string assignedValue, int width, string command)
		{
			return (new File(key, assignedValue, width, command)).ToString();
		}

		public string Hidden(string key, string assignedValue)
		{
			return (new Hidden(key, assignedValue)).ToString();
		}

		public string MessageBox(string messagePage)
		{
			Button buttonClose = new Button("Close", "Advocar_MessageBoxClose();", 50);
			string[] strArrays = new string[] { "<div id=\"Advocar_Page_MessageBoxBackground\" class=\"Page_MessageBoxDisabled\"></div><div id=\"Page_MessageBox\"><iframe id=\"Advocar_Page_MessageBoxContent\" src=\"", messagePage, "\"></iframe><center>", buttonClose.ToString(), "</center></div>" };
			return string.Concat(strArrays);
		}

		public string Password(string key, string assignedValue)
		{
			return (new Password(key, assignedValue)).ToString();
		}

		public string Password(string key, string assignedValue, int width, int maxlength)
		{
			return (new Password(key, assignedValue, width, maxlength)).ToString();
		}

		public string Password(string key, string assignedValue, int width, int maxlength, string command)
		{
			Password password = new Password(key, assignedValue, width, maxlength, command);
			return password.ToString();
		}

		public string Password(string key, string assignedValue, int width, int maxlength, string command, bool required)
		{
			Password password = new Password(key, assignedValue, width, maxlength, command, required);
			return password.ToString();
		}

		public string Radio(string key, string assignedValue, string radioValue, string radioLabel, string command, string stylesheet)
		{
			Radio radio = new Radio(key, assignedValue, radioValue, radioLabel, command, stylesheet);
			return radio.ToString();
		}

		public string Radio(string key, string assignedValue, string radioValue, string radioLabel, string command)
		{
			Radio radio = new Radio(key, assignedValue, radioValue, radioLabel, command);
			return radio.ToString();
		}

		public string Radio(string key, string assignedValue, string radioValue, string radioLabel)
		{
			return (new Radio(key, assignedValue, radioValue, radioLabel)).ToString();
		}

		public string RenderAjax(string RenderItemId, string PreliminaryHtml, string ActionList)
		{
			Ajax ajax = new Ajax(RenderItemId, PreliminaryHtml, ActionList, "", false);
			return ajax.ToString();
		}

		public string RenderAjax(string RenderItemId, string PreliminaryHtml, string ActionList, bool Debug)
		{
			Ajax ajax = new Ajax(RenderItemId, PreliminaryHtml, ActionList, "", false)
			{
				Debug = Debug
			};
			return ajax.ToString();
		}

		public string RenderAjax(string RenderItemId, string PreliminaryHtml, string ActionList, string Command, bool UseSpanNotDiv)
		{
			Ajax ajax = new Ajax(RenderItemId, PreliminaryHtml, ActionList, Command, UseSpanNotDiv);
			return ajax.ToString();
		}

		public string RenderAjax(string RenderItemId, string PreliminaryHtml, string ActionList, string Command, bool UseSpanNotDiv, bool Debug)
		{
			Ajax ajax = new Ajax(RenderItemId, PreliminaryHtml, ActionList, Command, UseSpanNotDiv)
			{
				Debug = Debug
			};
			return ajax.ToString();
		}

		public string RenderAjaxResponse(string valuePair, string render, string javascriptFunction)
		{
			string[] strArrays = new string[] { "##VALUEPAIRSTART##", valuePair, "##VALUEPAIREND##@@@@##RENDERSTART##", render, "##RENDEREND##@@@@##CUSTOMSTART##", javascriptFunction, "##CUSTOMEND##" };
			return string.Concat(strArrays);
		}

		public string ScrollPosition(Dictionary dict, string onLoadCommand)
		{
			string temp = "";
			Input inpt = new Input();
			string scrollID = "";
			if (Validation.IsNull(dict["scrollTopID"]))
			{
				int num = (new Random()).Next(1, 99);
				dict["scrollTopID"] = num.ToString("00");
			}
			scrollID = dict["scrollTopID"];
			if (Validation.IsNull(dict[string.Concat("scrollTop", scrollID)]))
			{
				dict[string.Concat("scrollTop", scrollID)] = "0";
			}
			string[] strArrays = new string[] { "\n", this.indent, "<!--Page: Page Positioning Code-->\n", this.indent, inpt.Hidden("scrollTopID", dict["scrollTopID"]), "\n", this.indent, inpt.Hidden(string.Concat("scrollTop", scrollID), dict[string.Concat("scrollTop", scrollID)]), "\n\n", this.indent, "<script language=\"javascript\">\n", this.indent, "function scrollPosition(){document.forms[0].scrollTop", scrollID, ".value = document.body.scrollTop;}\n\n", this.indent, "function scrollLoad(){\n", this.indent, "\tvar scrollTop = parseInt(document.forms[0].scrollTop", scrollID, ".value);\n\n", this.indent, "\tif (scrollTop > document.body.scrollHeight){\n", this.indent, "\t\tscrollTop = document.body.scrollHeight;\n", this.indent, "\t\tdocument.forms[0].scrollTop", scrollID, ".value = scrollTop;\n", this.indent, "\t}\n\n", this.indent, "\tif (document.readyState == 'complete'){\n", this.indent, "\t\twindow.scrollTo(0, scrollTop);\n", this.indent, "\t\tdocument.forms[0].scrollTop", scrollID, ".value = document.body.scrollTop;\n" };
			temp = string.Concat(strArrays);
			if (onLoadCommand.Length > 0)
			{
				strArrays = new string[] { temp, this.indent, "\t\t\t", onLoadCommand, "\n" };
				temp = string.Concat(strArrays);
			}
			strArrays = new string[] { temp, this.indent, "\t}\n", this.indent, "}\n\n", this.indent, "var myBody = document.body;\n", this.indent, "myBody.onload = scrollLoad;\n", this.indent, "myBody.onscroll = scrollPosition;\n", this.indent, "myBody.onunload = scrollPosition;\n", this.indent, "</script>\n" };
			temp = string.Concat(strArrays);
			return temp;
		}

		public string Select(string key, string assignedValue, int width, DataAccess data, string fieldCode, string fieldDesc, string command, bool required)
		{
			Select select = new Select(key, assignedValue, width, data, fieldCode, fieldDesc, command, required);
			return select.ToString();
		}

		public string Select(string key, string assignedValue, int width, DataAccess data, string fieldCode, string fieldDesc)
		{
			Select select = new Select(key, assignedValue, width, data, fieldCode, fieldDesc);
			return select.ToString();
		}

		public string Select(string key, string assignedValue, int width, DataAccess data)
		{
			return (new Select(key, assignedValue, width, data)).ToString();
		}

		public string Select(string key, string assignedValue, int width, string optionList, string command)
		{
			Select select = new Select(key, assignedValue, width, optionList, command);
			return select.ToString();
		}

		public string Text(string key, string assignedValue, int width, int maxlength)
		{
			return (new Text(key, assignedValue, width, maxlength)).ToString();
		}

		public string Text(string key, string assignedValue, int width, int maxlength, string command)
		{
			Text text = new Text(key, assignedValue, width, maxlength, command);
			return text.ToString();
		}

		public string Text(string key, string assignedValue, int width, int maxlength, string command, bool required)
		{
			Text text = new Text(key, assignedValue, width, maxlength, command, required);
			return text.ToString();
		}

		public string Text(string key, string assignedValue, int width, int maxlength, string command, bool required, string stylesheet)
		{
			Text text = new Text(key, assignedValue, width, maxlength, command, required, stylesheet);
			return text.ToString();
		}

		public string TextArea(string key, string assignedValue, int width, int height, int maxlength)
		{
			TextArea textarea = new TextArea(key, assignedValue, width, height, maxlength);
			return textarea.ToString();
		}

		public string TextArea(string key, string assignedValue, int width, int height, int maxlength, string command)
		{
			TextArea textarea = new TextArea(key, assignedValue, width, height, maxlength, command);
			return textarea.ToString();
		}

		public string TextArea(string key, string assignedValue, int width, int height, int maxlength, string command, bool required)
		{
			TextArea textarea = new TextArea(key, assignedValue, width, height, maxlength, command, required);
			return textarea.ToString();
		}
	}
}