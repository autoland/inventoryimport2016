using System;

namespace Advocar.Web
{
	public enum Conditional
	{
		none,
		display,
		enable
	}
}