using System;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Advocar.Interface
{
	public abstract class Xml : IXml
	{
		protected Xml()
		{
		}

		public virtual object Deserialize(string xmlContent)
		{
			XmlSerializer xml = new XmlSerializer(this.GetType());
			MemoryStream stream = new MemoryStream((new UTF8Encoding()).GetBytes(xmlContent));
			return xml.Deserialize(stream);
		}

		public virtual object Deserialize(Stream stream)
		{
			XmlSerializer xml = new XmlSerializer(this.GetType());
			return xml.Deserialize(new XmlTextReader(stream));
		}

		public virtual object DeserializeFromFile(string fileName)
		{
			FileStream stream = null;
			stream = (File.Exists(fileName) ? File.Open(fileName, FileMode.Open, FileAccess.ReadWrite, FileShare.None) : File.Create(fileName));
			return (new XmlSerializer(this.GetType())).Deserialize(stream);
		}

		public virtual object DeserializeFromUrl(string url)
		{
			XmlSerializer xml = new XmlSerializer(this.GetType());
			return xml.Deserialize((new WebClient()).OpenRead(url));
		}

		public virtual string Serialize()
		{
			MemoryStream stream = new MemoryStream();
			XmlSerializer xml = new XmlSerializer(this.GetType());
			XmlTextWriter writer = new XmlTextWriter(stream, Encoding.ASCII);
			xml.Serialize(writer, this);
			stream = (MemoryStream)writer.BaseStream;
			return Encoding.ASCII.GetString(stream.ToArray());
		}

		public virtual void SerializeToFile(string fileName)
		{
			if (File.Exists(fileName))
			{
				File.Delete(fileName);
			}
			FileStream stream = File.Create(fileName);
			(new XmlSerializer(this.GetType())).Serialize(stream, this);
			stream.Close();
		}
	}
}