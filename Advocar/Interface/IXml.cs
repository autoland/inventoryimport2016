using System;

namespace Advocar.Interface
{
	public interface IXml
	{
		object Deserialize(string xmlContent);

		object DeserializeFromFile(string fileName);

		object DeserializeFromUrl(string url);

		string Serialize();

		void SerializeToFile(string fileName);
	}
}