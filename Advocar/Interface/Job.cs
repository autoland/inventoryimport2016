using System;

namespace Advocar.Interface
{
	public abstract class Job : IJob
	{
		public Job()
		{
		}

		public abstract void Execute();
	}
}