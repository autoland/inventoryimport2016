using Advocar.Web;
using System;

namespace Advocar.Interface
{
	public interface ITransaction
	{
		void Delete();

		void Load(ref Dictionary dict);

		void Populate(ref Dictionary dict);

		void Save();
	}
}