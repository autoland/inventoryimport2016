using System;

namespace Advocar.Interface
{
	public interface IJob
	{
		void Execute();
	}
}