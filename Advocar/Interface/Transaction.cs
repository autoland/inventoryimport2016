using Advocar.Data;
using Advocar.Tools;
using Advocar.Web;
using System;
using System.Reflection;

namespace Advocar.Interface
{
	public abstract class Transaction : Xml, ITransaction
	{
		protected string connection = "";

		protected string modifiedUserID = "";

		protected string databaseObjectName = "";

		public Transaction()
		{
		}

		public Transaction(string connection, string modifiedUserID)
		{
			this.connection = connection;
			this.modifiedUserID = modifiedUserID;
		}

		public abstract void Delete();

		public virtual void Load(ref Dictionary dict)
		{
			PropertyInfo[] properties = base.GetType().GetProperties();
			for (int i = 0; i < (int)properties.Length; i++)
			{
				PropertyInfo property = properties[i];
				if ((!property.CanWrite ? false : dict.Exists(property.Name)))
				{
					if (property.PropertyType.Name == "Int32")
					{
						if (!Validation.IsNumeric(dict[property.Name]))
						{
							dict[property.Name] = "0";
						}
						property.SetValue(this, int.Parse(dict[property.Name]), null);
					}
					else if (!(property.PropertyType.Name == "Float" ? false : !(property.PropertyType.Name == "Single")))
					{
						if (!Validation.IsNumeric(dict[property.Name]))
						{
							dict[property.Name] = "0";
						}
						property.SetValue(this, float.Parse(dict[property.Name]), null);
					}
					else if (!(property.PropertyType.Name == "Boolean"))
					{
						property.SetValue(this, dict[property.Name], null);
					}
					else
					{
						string boolValue = dict[property.Name];
						boolValue = boolValue.ToLower();
						if (boolValue == "1")
						{
							boolValue = "true";
						}
						if (boolValue != "true")
						{
							boolValue = "false";
						}
						property.SetValue(this, bool.Parse(boolValue), null);
					}
				}
			}
		}

		public virtual void Populate(ref Dictionary dict)
		{
			PropertyInfo[] properties = base.GetType().GetProperties();
			for (int i = 0; i < (int)properties.Length; i++)
			{
				PropertyInfo property = properties[i];
				if (property.CanWrite)
				{
					dict[property.Name] = property.GetValue(this, null).ToString();
				}
			}
		}

		protected abstract void retrievedata();

		public abstract void Save();

		public virtual string SaveQuery()
		{
			Type type = base.GetType();
			DataAccess data = new DataAccess(this.connection);
			PropertyInfo[] properties = type.GetProperties();
			for (int i = 0; i < (int)properties.Length; i++)
			{
				PropertyInfo property = properties[i];
				if (property.CanWrite)
				{
					if (property.PropertyType.Name == "Int32")
					{
						data.AddParam(string.Concat("@", property.Name), DataAccessParameterType.Numeric, property.GetValue(this, null).ToString());
					}
					else if (!(property.PropertyType.Name == "Float" ? false : !(property.PropertyType.Name == "Single")))
					{
						data.AddParam(string.Concat("@", property.Name), DataAccessParameterType.Numeric, property.GetValue(this, null).ToString());
					}
					else if (!(property.PropertyType.Name == "Boolean"))
					{
						data.AddParam(string.Concat("@", property.Name), DataAccessParameterType.Text, property.GetValue(this, null).ToString());
					}
					else
					{
						data.AddParam(string.Concat("@", property.Name), DataAccessParameterType.Bool, property.GetValue(this, null).ToString());
					}
				}
			}
			string str = data.CreateQueryString(string.Concat(this.databaseObjectName, "_InsertUpdate"));
			return str;
		}

		public virtual void wipeout()
		{
			FieldInfo[] fields = base.GetType().GetFields();
			for (int i = 0; i < (int)fields.Length; i++)
			{
				fields[i].SetValue(this, null);
			}
		}
	}
}