﻿using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyCompany("Autoland Inc.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCopyright("Copyright © AUTOLAND 2006, 2007")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyProduct("Advocar")]
[assembly: AssemblyTitle("Advocar")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: CompilationRelaxations(8)]
[assembly: ComVisible(false)]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.Default | DebuggableAttribute.DebuggingModes.DisableOptimizations | DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints | DebuggableAttribute.DebuggingModes.EnableEditAndContinue)]
[assembly: Guid("b1ef4d9d-3664-4c1c-b8d8-80ff7bcf1091")]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows=true)]
